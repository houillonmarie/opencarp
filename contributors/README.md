## Instructions to sign the Contributor Agreement

To make the Contributor Agreement effective, please follow these steps:
* Read the [Contributor Agreement](https://opencarp.org/cla)
* Create an account on [our GitLab instance](http://git.opencarp.org)
* Fork off a [new branch](https://git.opencarp.org/openCARP/openCARP/-/branches/new) from the master of [the openCARP project](https://git.opencarp.org/openCARP/openCARP)
* Add a single file to the [contributors](contributors) folder named with your username and the extension ``.md``, e.g., ``JohnDoe.md``.
* Put the following in the file:

```
[date]

I hereby agree to the terms of the Contributor Agreement, version 1.0, with
MD5 checksum 424c55023a9c38c9c8ce96e50d86770b.

I furthermore declare that I am authorized and able to make this
agreement and sign this declaration.

Signed,

[your name]
https://git.opencarp.org/[your username]
```

Replace the bracketed text as follows:
* `[date]` with today's date, in the unambiguous numeric form YYYY-MM-DD.
* `[your name]` with your name.
* `[your username]` with your GitLab username.

You can confirm the MD5 checksum of the Contributor Agreement by running the
md5 program over CLA-1.0.md:

```
md5 CLA-1.0.md
MD5 (CLA-1.0.md) = 424c55023a9c38c9c8ce96e50d86770b
```

If the output is different from above, do not sign the Contributor Agreement
and let us know.

* Open a merge request to integrate your signed Contributor Agreement from
your cla-branch into the master branch.


That's it!
