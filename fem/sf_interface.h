// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file sf_interface.h
* @brief Interface to SlimFem.
* @author Aurel Neic
* @version 
* @date 2019-10-25
*/


#ifndef SF_INTERFACE_H
#define SF_INTERFACE_H

#include <map>

#include "fem_types.h"
#include "basics.h"

#ifndef CARP_PARAMS
#define CARP_PARAMS
#include "openCARP_p.h"
#include "openCARP_d.h"
#endif

#include "SF_base.h"

namespace opencarp {

typedef SF::meshdata<mesh_int_t,mesh_real_t>     sf_mesh;
typedef SF::petsc_vector<mesh_int_t,mesh_real_t> sf_petsc_vec;
typedef SF::petsc_matrix<mesh_int_t,mesh_real_t> sf_petsc_mat;
typedef SF::petsc_solver<mesh_int_t,mesh_real_t> sf_petsc_sol;

typedef SF::abstract_matrix<mesh_int_t,mesh_real_t>        sf_mat;
typedef SF::abstract_linear_solver<mesh_int_t,mesh_real_t> sf_sol;

/**
* @brief The enum identifying the different meshes we might want to load.
*/
enum mesh_t {
  intra_elec_msh = 0,
  extra_elec_msh,
  eikonal_msh,
  elasticity_msh,
  fluid_msh,
  reference_msh,
  phie_recv_msh,
  unset_msh,
  num_msh
};

// Special gridIDs used in for identifying scatterers
/// Scatter algebraic to nodal
#define ALG_TO_NODAL (num_msh+5)
/// Permute algebraic data from PETSC to canonical ordering
#define PETSC_TO_CANONICAL (num_msh+6)
/// Permute algebraic element data from PETSC to canonical ordering
#define ELEM_PETSC_TO_CANONICAL (num_msh+7)

// The physics. Has to match with carp.prm
#define PHYSREG_INTRA_ELEC   0
#define PHYSREG_EXTRA_ELEC   1
#define PHYSREG_EIKONAL      2
#define PHYSREG_MECH         3
#define PHYSREG_FLUID        4
#define PHYSREG_NUM_PHYSICS  5


namespace user_globals {
  extern SF::scatter_registry<mesh_int_t>   scatter_reg;
  extern std::map<mesh_t, sf_mesh>          mesh_reg;

  extern std::map<SF::quadruple<int>, SF::index_mapping<mesh_int_t> > map_reg;
}  // namespace user_globals

/**
* @brief Get a mesh by specifying the gridID
*
* @param gt The grid ID.
*
* @return The SF meshdata
*/
sf_mesh & get_mesh(const mesh_t gt);

/// get a char* to the name of a mesh type
const char* get_mesh_type_name(mesh_t t);

/// check wheter a SF mesh is set
bool mesh_is_registered(const mesh_t gt);

/**
* @brief Register a scattering between to grids, or between algebraic and nodal representation
*        of data on the same grid.
*
* @param from  The mesh ID we scatter from.
* @param to    The mesh ID we scatter to, or ALG_TO_NODAL.
* @param nbr   The used numbering.
* @param dpn   The degrees of freedom per node.
*/
SF::scattering*
register_scattering(const int from, const int to, const SF::SF_nbr nbr,
                    const int dpn);

inline SF::scattering*
register_scattering(const int from, const int to, const int dpn) {
  return register_scattering(from, to, SF::NBR_PETSC, dpn);
}

/**
* @brief Register a permutation between two orderings for a mesh.
*
* @param mesh_id  The mesh we register the permutation for.
* @param perm_id  The permutation specifier index (see sf_interface.h).
* @param dpn      Degrees of freedom per node.
*
* @return The registered permutation scattering pointer.
*/
SF::scattering*
register_permutation(const int mesh_id, const int perm_id, const int dpn);

/**
* @brief Get a scattering from the global scatter registry.
*
* @param [in]  from  The mesh ID we scatter from.
* @param [in]  to    The mesh ID we scatter to, or ALG_TO_NODAL.
* @param [in]  nbr   The used numbering.
* @param [in]  dpn   The degrees of freedom per node.
*
* @return The requested scattering.
*/
SF::scattering*
get_scattering(const int from, const int to, const SF::SF_nbr nbr, const int dpn);

inline SF::scattering*
get_scattering(const int from, const int to, const int dpn) {
  return get_scattering(from, to, SF::NBR_PETSC, dpn);
}

/**
* @brief Get the PETSC to canonical permutation scattering for a given mesh and number of dpn
*
* @param mesh_id  The mesh id.
* @param perm_id  The permutation specifier index (see sf_interface.h).
* @param dpn      Degrees of freedom per node.
*
* @return The scattering.
*/
SF::scattering*
get_permutation(const int mesh_id, const int perm_id, const int dpn);

}  // namespace opencarp

#endif
