// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file solver_utils.cc
* @brief Utilities for linear solvers.
* @author Aurel Neic, Gernot Plank, Edward Vigmond
* @version 
* @date 2019-10-25
*/

#include "solver_utils.h"

namespace opencarp {

void set_stopping_criterion(sf_petsc_sol & s,
                            const sf_petsc_sol::norm_t normtype,
                            const double tol,
                            const int max_it,
                            FILE_SPEC logger,
                            bool verbose)
{
  s.norm   = normtype;
  s.max_it = max_it;

  double absTol = tol;
  double relTol = absTol;

  switch(normtype)
  {
    case sf_petsc_sol::absPreResidual:
      // make sure that we do not converge due to relative stopping criterion
      relTol = 1e-50;
      KSPSetNormType(s.ksp, COMPAT_KSP_NORM_PRECONDITIONED);
      if (verbose)
        log_msg(logger, 0, 0,"Solving %s system using absolute tolerance (%g) of preconditioned residual\n"
                           "as stopping criterion", s.name, absTol);
      break;

    case sf_petsc_sol::absUnpreResidual:
      // make sure that we do not converge due to relative stopping criterion
      relTol = 1e-50;
      KSPSetNormType(s.ksp, COMPAT_KSP_NORM_UNPRECONDITIONED);
      // this is not possible with all iterative methods, ideally we would issue
      // an error message here to inform the user
      if (verbose)
        log_msg(logger, 0, 0,"Solving %s system using absolute tolerance (%g) of unpreconditioned residual\n"
                             "as stopping criterion", s.name, absTol);
      break;

    case sf_petsc_sol::relResidual:
      // make sure that we do not converge due to absolute stopping criterion
      absTol = 0.0;
      if (verbose)
        log_msg(logger, 0, 0,"Solving %s system using relative tolerance (%g)\n"
                             "as stopping criterion", s.name, relTol);
      break;

    case sf_petsc_sol::absPreRelResidual:
      if(verbose)
        log_msg(logger, 0, 0,"Solving %s system using combined relative and absolute tolerance (%g)\n"
                             "as stopping criterion (preconditioned L2 is used)",s.name, absTol);
      break;

    default:
      log_msg(logger, 2, 0,"Chosen stopping criterion invalid, defaulting to absolute tolerance of preconditioned residual");
      relTol = 1e-50;
  }

  KSPSetTolerances(s.ksp, relTol, absTol, PETSC_DEFAULT, s.max_it);
}


int insert_petsc_solver_opts(sf_petsc_sol & s, const char* default_opts_str, FILE_SPEC logger, bool verbose)
{
  int ierr = 0;

  // in either case we use command line options
  // we have to be careful with that though, we should not set any solver specific
  // options here. Typically, we would set options such as
  // -ksp_monitor -ksp_view -ksp_norm_type etc
  ierr = KSPSetFromOptions(s.ksp);
  CHKERRQ(ierr);

  if ( (s.options_file == NULL) || (strcmp(s.options_file, "") == 0) )
  {
    log_msg(logger, 0, 0, "%s solver: switching to default settings \"%s\".",
            s.name, default_opts_str);

    COMPAT_PetscOptionsInsertString(default_opts_str);
    ierr = KSPSetFromOptions(s.ksp); CHKERRQ(ierr);

    ierr = PetscOptionsClearFromString(default_opts_str);
    CHKERRQ(ierr);

    // check whether solver is direct
    PC     pc;
    PCType type;
    KSPGetPC  (s.ksp, &pc);
    PCGetType (pc, &type);
    if(!strcmp(type, PCLU) || !strcmp(type, PCCHOLESKY))  {
      KSPSetType(s.ksp, "preonly");
      KSPSetInitialGuessNonzero(s.ksp, PETSC_FALSE);
      KSPSetNormType(s.ksp, KSP_NORM_NONE);
    }
  }
  else if(file_can_be_opened(s.options_file)) {
    if (verbose)
      log_msg(logger, 0, 0, "%s solver: switching to user-provided settings given in %s.",
              s.name, s.options_file);

    // insert options file
    COMPAT_PetscOptionsInsertFile(PETSC_COMM_WORLD, s.options_file, PETSC_FALSE);
    ierr = KSPSetFromOptions(s.ksp); CHKERRQ(ierr);

    ierr = PetscOptionsClearFromFile(PETSC_COMM_WORLD, s.options_file);
    CHKERRQ(ierr);

    // check whether solver is direct
    PC     pc;
    PCType type;
    KSPGetPC  (s.ksp, &pc);
    PCGetType (pc, &type);
    if(!strcmp(type, PCLU) || !strcmp(type, PCCHOLESKY))  {
      KSPSetType(s.ksp, "preonly");
      KSPSetInitialGuessNonzero(s.ksp, PETSC_FALSE);
      KSPSetNormType(s.ksp, KSP_NORM_NONE);
    }
  }
  else {
    log_msg(0, 4, 0,"%s solver: user-provided options file %s could not be read.", s.name, s.options_file);
    ierr = -1;
  }

  return ierr;
}

void lin_solver_stats::init_logger(const char* filename)
{
  logger = f_open(filename, "w");

  const char* h1 = "          ----- ----- -------  -----  ----- | ---- ----- --------- --------- --------- |";
  const char* h2 = "          mn it mx it  avg it    its  ttits |   ss    ts        st      stps     stpit |";

  if(logger == NULL)
    log_msg(NULL, 3, 0,"%s error: Could not open file %s in %s. Turning off logging.\n",
            __func__, filename);
  else {
    log_msg(logger, 0, 0, "%s", h1);
    log_msg(logger, 0, 0, "%s", h2);
  }
}

void lin_solver_stats::log_stats(double tm, bool cflg)
{
  // make sure this->solves is > 0
  if(!this->solves) this->solves++;

  char itbuf[256];
  char stbuf[64];

  // iterations in this period
  float its = (this->tot-this->last_tot);

  // total solver time spent in this period
  float tstm    = this->slvtime-this->lastSlvtime;

  // solver time per solve
  float stm     = tstm/this->solves;

  // solver time per iteration
  float itm     = its?tstm/its:0.;

  if(!its)
    this->min = 0;
  this->totsolves  += this->solves;

  sprintf(itbuf,"%5d %5d %7.1f %6d %6d",
          this->min, this->max, (float)(its/this->solves),
          this->tot - this->last_tot, this->tot );
  sprintf(stbuf,"%4d %5d %9.4f %9.4f %9.4f",this->solves,this->totsolves,
                                                 (float)tstm,stm,itm);

  unsigned char flag = cflg? ECHO : 0;
  log_msg(this->logger, 0, flag|FLUSH|NONL, "%9.3f %s | %s |\n", tm, itbuf, stbuf);

  this->min         = INT_MAX;
  this->max         = 0;
  this->last_tot    = this->tot;
  this->solves      = 0;
  this->lastSlvtime = this->slvtime;
}

void lin_solver_stats::update_iter(const int curiter)
{
  if (curiter > max) max = curiter;
  if (curiter < min) min = curiter;
  tot += curiter;
  solves++;
}

}  // namespace opencarp

