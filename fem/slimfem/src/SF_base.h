// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file SF_base.h
* @brief The base header file.
*
* Include this file to use all SlimFem functionality.
*
* @author Aurel Neic
* @version 
* @date 2017-02-14
*/


#ifndef _SF_BASE_H
#define _SF_BASE_H

// C includes
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <mpi.h>

// C++ includes
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <algorithm>

// petsc includes
#include "petsc.h"
#include "petscvec.h"
#include "petscsys.h"
#include "petscksp.h"

// external includes
#include "progress.hpp"
#include "asciiPlotter.hpp"
#include "dense_mat.hpp"

// This defines are used in multiple header files so have to put them here
/// the default SlimFem MPI communicator
#define SF_COMM MPI_COMM_WORLD
/// the MPI tag when communicating
#define SF_MPITAG 100

namespace SF {
/**
* @brief Clamp a value into an interval [start, end]
*
* @tparam V   Value type
* @tparam W   Interval boundary type
* @param val    The value we clamp
* @param start  The interval start value
* @param end    The interval end value
*
* @return The clamped value
*/
template<typename V, typename W>
V clamp(const V val, const W start, const W end) {
  if(val < start) return start;
  if(val > end) return end;
  return val;
}

}

// this are the mesh management modules
#include "SF_vector.h"
#include "SF_sort.h"
#include "SF_container.h"
#include "SF_network.h"
#include "SF_mesh_io.h"
#include "SF_parallel_utils.h"
#include "SF_linalg_utils.h"
#include "SF_fem_utils.h"
#include "SF_partitioning.h"
#include "SF_parallel_layout.h"
#include "SF_numbering.h"
#include "SF_mesh_utils.h"


#endif
