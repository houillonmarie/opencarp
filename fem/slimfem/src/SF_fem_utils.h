// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file SF_fem_utils.h
* @brief FEM utilities.
*
* Some of this funcitons (num dof, integration points, shape
* funcs) have been copied over from the carpentry source base.
*
* @author Aurel Neic, Christoph Augustin
* @version
* @date 2017-11-15
*/


#ifndef _SF_FEM_H
#define _SF_FEM_H

#include<cassert>

#define SF_MAX_ELEM_NODES 10  //!< max \#nodes defining an element

namespace SF {

/**
* @brief Get number of d.o.f. for an element typa and an Ansatz function order.
*
* @param type   Element type.
* @param order  Ansatz function order
*
* @return Number of d.o.f.
*/
inline short num_dof(elem_t type, short order)
{
  short ndof = -1;
  switch(type) {
    case Line:
      if (order == 1)      ndof = 2;
      else if (order == 2) ndof = 3;
      else {
        fprintf(stderr, "Line element order %d not implemented yet.\n", order);
        exit(1);
      }
      break;
    case Tri:
      if (order == 1)      ndof = 3;
      else if (order == 2) ndof = 6;
      else {
        fprintf(stderr, "Tri element order %d not implemented yet.\n", order);
        exit(1);
      }
      break;
    case Quad:
      if (order == 1)      ndof = 4;
      else if (order == 2) ndof = 8;
      else {
        fprintf(stderr, "Quad element order %d not implemented yet.\n", order);
        exit(1);
      }
      break;
    case Tetra:
      if (order == 1)      ndof = 4;
      else if (order == 2) ndof = 10;
      else {
        fprintf(stderr, "Tetra element order %d not implemented yet.\n", order);
        exit(1);
      }
      break;
    case Hexa:
      if (order == 1)      ndof = 8;
      else if (order == 2) ndof = 20; // serendipity element
      else {
        fprintf(stderr, "Hexa element order %d not implemented yet.\n", order);
        exit(1);
      }
      break;

    default:
        fprintf(stderr, "%s error: Unsupported element type.\n", __func__);
        exit(1);
  }

  return ndof;
}


/**
* @brief Compute the integration point locations and weights.
*
* @param type   Element type.
* @param order  Integration order.
* @param ip     Integration point locations.
* @param w      Integration weights.
* @param nint   Number of integration points.
*/
inline void general_integration_points(const elem_t type,
                                const short order,
                                Point* ip,
                                double* w,
                                int & nint)
{
  switch (type) {
    case Line:
      if (order == 1) {
        ip[0].x = 0.; ip[0].y = 0.; ip[0].z = 0.;
        w[0] = 1.; nint = 1;
      } else if (order == 2) {
        const double sqrt3 = 0.577350269189626;  //=sqrt(1./3.)
        ip[0].x = -sqrt3; ip[0].y = 0.; ip[0].z = 0.;
        ip[1].x = +sqrt3; ip[1].y = 0.; ip[1].z = 0.;
        w[0] = 1.; w[1] = 1.; nint = 2;
      }
      break;

    case Tri:
      if (order == 1) {
        ip[0].x = 1. / 3.; ip[0].y = 1. / 3.; ip[0].z = 0.;
        w[0] = 1. / 2.;
        nint = 1;
      } else if (order == 2) {
        ip[0].x = 1. / 6.; ip[0].y = 1. / 6.; ip[0].z = 0.;
        ip[1].x = 4. / 6.; ip[1].y = 1. / 6.; ip[1].z = 0.;
        ip[2].x = 1. / 6.; ip[2].y = 4. / 6.; ip[2].z = 0.;
        w[0] = 1. / 6.; w[1] = 1. / 6.; w[2] = 1. / 6.;
        nint = 3;
      } else if (order == 3 || order == 4) {
        // Gauss computed
        ip[0].x = 0.188409405952072339650, ip[0].y = 0.787659461760847001700, ip[0].z = 0;
        ip[1].x = 0.523979067720100721850, ip[1].y = 0.409466864440734712450, ip[1].z = 0;
        ip[2].x = 0.808694385677669824730, ip[2].y = 0.088587959512703928766, ip[2].z = 0;
        ip[3].x = 0.106170269119576471390, ip[3].y = 0.787659461760847001700, ip[3].z = 0;
        ip[4].x = 0.295266567779632616020, ip[4].y = 0.409466864440734712450, ip[4].z = 0;
        ip[5].x = 0.455706020243648035620, ip[5].y = 0.088587959512703928766, ip[5].z = 0;
        ip[6].x = 0.023931132287080617016, ip[6].y = 0.787659461760847001700, ip[6].z = 0;
        ip[7].x = 0.066554067839164496312, ip[7].y = 0.409466864440734712450, ip[7].z = 0;
        ip[8].x = 0.102717654809626260380, ip[8].y = 0.088587959512703928766, ip[8].z = 0;

        w[0] = 0.019396383305959434551;
        w[1] = 0.063678085099884929043;
        w[2] = 0.055814420483044288601;
        w[3] = 0.03103421328953510222;
        w[4] = 0.1018849361598159059;
        w[5] = 0.089303072772870889517;
        w[6] = 0.019396383305959434551;
        w[7] = 0.063678085099884929043;
        w[8] = 0.055814420483044288601;
        nint = 9;
      }
      break;

    case Quad:
      if (order == 1) {
        ip[0].x = 0.; ip[0].y = 0.; ip[0].z = 0.;
        w[0] = 4.;
        nint = 1;
      } else if (order == 2 || order == 3) {
        const double sqrt3 = 0.577350269189626;  //=sqrt(1./3.)
        ip[0].x = -sqrt3; ip[0].y = -sqrt3; ip[0].z = 0.;
        ip[1].x = +sqrt3; ip[1].y = -sqrt3; ip[1].z = 0.;
        ip[2].x = +sqrt3; ip[2].y = +sqrt3; ip[2].z = 0.;
        ip[3].x = -sqrt3; ip[3].y = +sqrt3; ip[3].z = 0.;
        w[0] = 1.; w[1] = 1.; w[2] = 1.; w[3] = 1.;
        nint = 4;
      } else if (order == 4) {
        // Gauss computed
        ip[0].x = 0.88729833462074170214, ip[0].y = 0.88729833462074170214, ip[0].z = 0;
        ip[1].x = 0.88729833462074170214, ip[1].y = 0.5,                    ip[1].z = 0;
        ip[2].x = 0.88729833462074170214, ip[2].y = 0.11270166537925829786, ip[2].z = 0;
        ip[3].x = 0.5,                    ip[3].y = 0.88729833462074170214, ip[3].z = 0;
        ip[4].x = 0.5,                    ip[4].y = 0.5,                    ip[4].z = 0;
        ip[5].x = 0.5,                    ip[5].y = 0.11270166537925829786, ip[5].z = 0;
        ip[6].x = 0.11270166537925829786, ip[6].y = 0.88729833462074170214, ip[6].z = 0;
        ip[7].x = 0.11270166537925829786, ip[7].y = 0.5,                    ip[7].z = 0;
        ip[8].x = 0.11270166537925829786, ip[8].y = 0.11270166537925829786, ip[8].z = 0;

        w[0] = 0.077160493827160225866;
        w[1] = 0.12345679012345638081;
        w[2] = 0.077160493827160225866;
        w[3] = 0.12345679012345638081;
        w[4] = 0.19753086419753024261;
        w[5] = 0.12345679012345638081;
        w[6] = 0.077160493827160225866;
        w[7] = 0.12345679012345638081;
        w[8] = 0.077160493827160225866;

        nint = 9;
      }
      break;

    case Tetra:
      if (order == 0 || order == 1) {
        // exact for linears
        ip[0].x = 0.25; ip[0].y = 0.25; ip[0].z = 0.25;
        w[0] = .16666666666666666666;
        nint = 1;
      } else if (order == 2) {
        // exact for quadratics
        double gauss1 = 0.13819660112501051518; // (5-sqrt(5))/20
        double gauss2 = 0.58541019662496845446; // (5+3sqrt(5))/20
        double weight = 0.0416666666666666666666666666666666; // 1/24
        ip[0].x = gauss1; ip[0].y = gauss1; ip[0].z = gauss1;
        ip[1].x = gauss2; ip[1].y = gauss1; ip[1].z = gauss1;
        ip[2].x = gauss1; ip[2].y = gauss2; ip[2].z = gauss1;
        ip[3].x = gauss1; ip[3].y = gauss1; ip[3].z = gauss2;
        w[0] = weight; w[1] = weight; w[2] = weight; w[3] = weight;
        nint = 4;
      } else if (order == 3 || order == 4) {
        // Gauss computed
        ip[0].x = 0.12764656212038541505,  ip[0].y = 0.29399880063162286969,  ip[0].z = 0.54415184401122529412;
        ip[1].x = 0.2457133252117133515,   ip[1].y = 0.56593316507280100325,  ip[1].z = 0.12251482265544139105;
        ip[2].x = 0.30377276481470755209,  ip[2].y = 0.070679724159396897787, ip[2].z = 0.54415184401122529412;
        ip[3].x = 0.58474756320489440498,  ip[3].y = 0.13605497680284600603,  ip[3].z = 0.12251482265544139105;
        ip[4].x = 0.034202793236766414198, ip[4].y = 0.29399880063162286969,  ip[4].z = 0.54415184401122529412;
        ip[5].x = 0.065838687060044420729, ip[5].y = 0.56593316507280100325,  ip[5].z = 0.12251482265544139105;
        ip[6].x = 0.081395667014670256001, ip[6].y = 0.070679724159396897787, ip[6].z = 0.54415184401122529412;
        ip[7].x = 0.15668263733681833672,  ip[7].y = 0.13605497680284600603,  ip[7].z = 0.12251482265544139105;

        w[0] = 0.0091694299214797256314;
        w[1] = 0.0211570064545240181520;
        w[2] = 0.0160270405984766287080;
        w[3] = 0.0369798563588529458080;
        w[4] = 0.0091694299214797291009;
        w[5] = 0.0211570064545240285600;
        w[6] = 0.0160270405984766356470;
        w[7] = 0.0369798563588529666250;

        nint = 8;
      }
      break;

    case Hexa:
      if (order == 0) {
        ip[0].x = 0.; ip[0].y = 0.; ip[0].z = 0.;
        w[0] = 8.;
        nint = 1;
      } else if (order == 1 || order == 2) {
        const double sqrt3 = 0.577350269189626;  //=sqrt(1./3.)
        ip[0].x = -sqrt3; ip[0].y = -sqrt3; ip[0].z = -sqrt3;
        ip[1].x = +sqrt3; ip[1].y = -sqrt3; ip[1].z = -sqrt3;
        ip[2].x = +sqrt3; ip[2].y = +sqrt3; ip[2].z = -sqrt3;
        ip[3].x = -sqrt3; ip[3].y = +sqrt3; ip[3].z = -sqrt3;
        ip[4].x = -sqrt3; ip[4].y = -sqrt3; ip[4].z = +sqrt3;
        ip[5].x = +sqrt3; ip[5].y = -sqrt3; ip[5].z = +sqrt3;
        ip[6].x = +sqrt3; ip[6].y = +sqrt3; ip[6].z = +sqrt3;
        ip[7].x = -sqrt3; ip[7].y = +sqrt3; ip[7].z = +sqrt3;
        w[0] = 1.; w[1] = 1.; w[2] = 1.; w[3] = 1.;
        w[4] = 1.; w[5] = 1.; w[6] = 1.; w[7] = 1.;
        nint = 8;
      }
      break;

    case Prism:
      if (order == 0) {
        ip[0].x = 0.33333333333333331, ip[0].y = 0.33333333333333337, ip[0].z = 0.5;
        w[0] = 0.5;
        nint = 1;
      } else if(order == 1 || order == 2) {
#if 1
        ip[0].x = 0.8168475628; ip[0].y = 0.0915762112; ip[0].z = 0.2113248706;
        ip[1].x = 0.8168475628; ip[1].y = 0.0915762112; ip[1].z = 0.7886751294;
        ip[2].x = 0.0915762112; ip[2].y = 0.8168475628; ip[2].z = 0.2113248706;
        ip[3].x = 0.0915762112; ip[3].y = 0.8168475628; ip[3].z = 0.7886751294;
        ip[4].x = 0.0915762112; ip[4].y = 0.0915762112; ip[4].z = 0.2113248706;
        ip[5].x = 0.0915762112; ip[5].y = 0.0915762112; ip[5].z = 0.7886751294;
        w[0] = 0.0833333358;
        w[1] = 0.0833333358;
        w[2] = 0.0833333358;
        w[3] = 0.0833333358;
        w[4] = 0.0833333358;
        w[5] = 0.0833333358;
        nint = 6;
#else
        ip[0].x = 0.28001991549907407,  ip[0].y = 0.64494897427831788, ip[0].z = 0.78867513459481287;
        ip[1].x = 0.28001991549907407,  ip[1].y = 0.64494897427831788, ip[1].z = 0.21132486540518713;
        ip[2].x = 0.66639024601470143,  ip[2].y = 0.15505102572168217, ip[2].z = 0.78867513459481287;
        ip[3].x = 0.66639024601470143,  ip[3].y = 0.15505102572168217, ip[3].z = 0.21132486540518713;
        ip[4].x = 0.075031110222608124, ip[4].y = 0.64494897427831788, ip[4].z = 0.78867513459481287;
        ip[5].x = 0.075031110222608124, ip[5].y = 0.64494897427831788, ip[5].z = 0.21132486540518713;
        ip[6].x = 0.17855872826361643,  ip[6].y = 0.15505102572168217, ip[6].z = 0.78867513459481287;
        ip[7].x = 0.17855872826361643,  ip[7].y = 0.15505102572168217, ip[7].z = 0.21132486540518713;
        w[0] = 0.045489654564005534;
        w[1] = 0.045489654564005548;
        w[2] = 0.079510345435994237;
        w[3] = 0.079510345435994265;
        w[4] = 0.045489654564005548;
        w[5] = 0.045489654564005562;
        w[6] = 0.079510345435994265;
        w[7] = 0.079510345435994292;
        nint = 8;
#endif
      }
      break;

    case Pyramid:
      if (order == 1) {
        ip[0].x = -0.433013; ip[0].y = -0.433013; ip[0].z = 0.25;
        ip[1].x = 0.433013;  ip[1].y = -0.433013; ip[1].z = 0.25;
        ip[2].x = 0.433013;  ip[2].y = 0.433013;  ip[2].z = 0.25;
        ip[3].x = -0.433013; ip[3].y = 0.433013;  ip[3].z = 0.25;
        w[0] = 1. / 3;
        w[1] = 1. / 3;
        w[2] = 1. / 3;
        w[3] = 1. / 3;
        nint = 4;
      } else if (order == 2) {
        ip[0 ].x = 0.040086493940919059,  ip[0 ].y = 0.040086493940919059,  ip[0 ].z = 0.93056815579702623;
        ip[1 ].x = 0.19053106107826956,   ip[1 ].y = 0.19053106107826956,   ip[1 ].z = 0.66999052179242813;
        ip[2 ].x = 0.38681920811135617,   ip[2 ].y = 0.38681920811135617,   ip[2 ].z = 0.33000947820757187;
        ip[3 ].x = 0.53726377524870672,   ip[3 ].y = 0.53726377524870672,   ip[3 ].z = 0.069431844202973714;
        ip[4 ].x = 0.040086493940919059,  ip[4 ].y = -0.040086493940919059, ip[4 ].z = 0.93056815579702623;
        ip[5 ].x = 0.19053106107826956,   ip[5 ].y = -0.19053106107826956,  ip[5 ].z = 0.66999052179242813;
        ip[6 ].x = 0.38681920811135617,   ip[6 ].y = -0.38681920811135617,  ip[6 ].z = 0.33000947820757187;
        ip[7 ].x = 0.53726377524870672,   ip[7 ].y = -0.53726377524870672,  ip[7 ].z = 0.069431844202973714;
        ip[8 ].x = -0.040086493940919059, ip[8 ].y = 0.040086493940919059,  ip[8 ].z = 0.93056815579702623;
        ip[9 ].x = -0.19053106107826956,  ip[9 ].y = 0.19053106107826956,   ip[9 ].z = 0.66999052179242813;
        ip[10].x = -0.38681920811135617,  ip[10].y = 0.38681920811135617,   ip[10].z = 0.33000947820757187;
        ip[11].x = -0.53726377524870672,  ip[11].y = 0.53726377524870672,   ip[11].z = 0.069431844202973714;
        ip[12].x = -0.040086493940919059, ip[12].y = -0.040086493940919059, ip[12].z = 0.93056815579702623;
        ip[13].x = -0.19053106107826956,  ip[13].y = -0.19053106107826956,  ip[13].z = 0.66999052179242813;
        ip[14].x = -0.38681920811135617,  ip[14].y = -0.38681920811135617,  ip[14].z = 0.33000947820757187;
        ip[15].x = -0.53726377524870672,  ip[15].y = -0.53726377524870672,  ip[15].z = 0.069431844202973714;

        w[0 ] = 0.000838466012258970;
        w[1 ] = 0.035511343496716564;
        w[2 ] = 0.14636983865620462;
        w[3 ] = 0.15061368516815288;
        w[4 ] = 0.000838466012258971;
        w[5 ] = 0.035511343496716585;
        w[6 ] = 0.1463698386562047;
        w[7 ] = 0.15061368516815296;
        w[8 ] = 0.000838466012258971;
        w[9 ] = 0.035511343496716585;
        w[10] = 0.1463698386562047;
        w[11] = 0.15061368516815296;
        w[12] = 0.000838466012258971;
        w[13] = 0.035511343496716599;
        w[14] = 0.14636983865620476;
        w[15] = 0.15061368516815302;
        nint = 16;
      }
      break;

    default:
      fprintf(stderr, "%s error: Unsupported element type.\n", __func__);
      exit(1);
  }
}


/**
* @brief Compute shape function and its derivatives on a reference element.
*
* @param type    Element type.
* @param order   Order of the Ansatz functions.
* @param ip      Integration point.
* @param rshape  Reference shape and deriv. matrix.
*/
inline void reference_shape(const elem_t type,
                            const Point ip,
                            dmat<double> & rshape)
{
  switch (type) {
    case Line:
    {
      rshape[0][0] = 1.0 - ip.x;
      rshape[0][1] = ip.x;

      rshape[1][0] = -1.0;
      rshape[1][1] = 1.0;

      rshape[2][0] = 0.;
      rshape[2][1] = 0.;

      rshape[3][0] = 0.;
      rshape[3][1] = 0.;
      break;
    }

    case Tri:
    {
      double lam0 = (1.0 - ip.x - ip.y);
      double lam1 = ip.x;
      double lam2 = ip.y;

      rshape[0][0] = lam0;
      rshape[0][1] = lam1;
      rshape[0][2] = lam2;

      // derivative w.r.t. xi
      rshape[1][0] = -1.0;
      rshape[1][1] =  1.0;
      rshape[1][2] =  0.0;

      // derivative w.r.t. eta
      rshape[2][0] = -1.0;
      rshape[2][1] =  0.0;
      rshape[2][2] =  1.0;

      // derivative w.r.t. zeta (is zero)
      rshape[3][0] = 0.;
      rshape[3][1] = 0.;
      rshape[3][2] = 0.;
      break;
    }

    case Quad: {
      double qrtr = 0.25;
      const double node[4][2] =
      {
        { -1.0, -1.0},
        { 1.0, -1.0},
        { 1.0,  1.0},
        { -1.0,  1.0}
      };

      // adjust to ordering given in carp manual
      int v[4] = {0, 1, 2, 3};

      for (int i = 0; i < 4; i++) {
        // shape function
        rshape[0][i] = qrtr * (1. + ip.x * node[v[i]][0]) * (1. + ip.y * node[v[i]][1]);
        // derivative w.r.t. xi
        rshape[1][i] = node[v[i]][0] * qrtr * (1. + ip.y * node[v[i]][1]);
        // derivative w.r.t. eta
        rshape[2][i] = node[v[i]][1] * qrtr * (1. + ip.x * node[v[i]][0]);
        // derivative w.r.t. zeta (is zero)
        rshape[3][i] = 0.;
      }
      break;
    }

    case Tetra:
    {
      double lam0 = 1.0 - ip.x - ip.y - ip.z;
      double lam1 = ip.x;
      double lam2 = ip.y;
      double lam3 = ip.z;
      //static int v[10] = {0,3,1,2,7,8,4,6,9,5};

      // shape function
      rshape[0][0] = lam0;
      rshape[0][1] = lam1;
      rshape[0][2] = lam2;
      rshape[0][3] = lam3;

      // derivative w.r.t. xi
      rshape[1][0] = -1.0;
      rshape[1][1] =  1.0;
      rshape[1][2] =  0.0;
      rshape[1][3] =  0.0;

      // derivative w.r.t. eta
      rshape[2][0] = -1.0;
      rshape[2][1] =  0.0;
      rshape[2][2] =  1.0;
      rshape[2][3] =  0.0;

      // derivative w.r.t. zeta
      rshape[3][0] = -1.0;
      rshape[3][1] =  0.0;
      rshape[3][2] =  0.0;
      rshape[3][3] =  1.0;
      break;
    }

    case Hexa:
    {
      const double oito = 1.0 / 8.0;
      static const double node[8][3] =
      {
        { -1.0, -1.0, -1.0},
        { 1.0, -1.0, -1.0},
        { 1.0,  1.0, -1.0},
        { -1.0,  1.0, -1.0},

        { -1.0, -1.0,  1.0},
        { 1.0, -1.0,  1.0},
        { 1.0,  1.0,  1.0},
        { -1.0,  1.0,  1.0}
      };

      // adjust to ordering given in carp manual
      static int v[8] = {4, 7, 6, 5, 0, 1, 2, 3};

      for (int i = 0; i < 8; i++) {
        // shape function
        rshape[0][i] = oito * (1. + ip.x * node[v[i]][0]) * (1. + ip.y * node[v[i]][1]) * (1. + ip.z * node[v[i]][2]);
        // derivative w.r.t. xi
        rshape[1][i] = node[v[i]][0] * oito * (1. + ip.y * node[v[i]][1]) * (1. + ip.z * node[v[i]][2]);
        // derivative w.r.t. eta
        rshape[2][i] = node[v[i]][1] * oito * (1. + ip.x * node[v[i]][0]) * (1. + ip.z * node[v[i]][2]);
        // derivative w.r.t zeta
        rshape[3][i] = node[v[i]][2] * oito * (1. + ip.x * node[v[i]][0]) * (1. + ip.y * node[v[i]][1]);
      }

      break;
    }

    case Prism:
    {
      rshape[0][0] = (1.0 - ip.x - ip.y) * ip.z;
      rshape[0][1] = ip.y * ip.z;
      rshape[0][2] = ip.x * ip.z;
      rshape[0][3] = (1.0 - ip.x - ip.y) * (1. - ip.z);
      rshape[0][4] = ip.x * (1. - ip.z);
      rshape[0][5] = ip.y * (1. - ip.z);

      // derivative w.r.t. x
      rshape[1][0] = -ip.z;
      rshape[1][1] = 0.0;
      rshape[1][2] = ip.z;
      rshape[1][3] = ip.z - 1.0;
      rshape[1][4] = 1. - ip.z;
      rshape[1][5] = 0.0;

      // derivative w.r.t. y
      rshape[2][0] = -ip.z;
      rshape[2][1] = ip.z;
      rshape[2][2] = 0.0;
      rshape[2][3] = ip.z - 1.0;
      rshape[2][4] = 0.0;
      rshape[2][5] = 1. - ip.z;

      // derivative w.r.t. z
      rshape[3][0] = 1.0 - ip.x - ip.y;
      rshape[3][1] = ip.y;
      rshape[3][2] = ip.x;
      rshape[3][3] = ip.x + ip.y - 1.0;
      rshape[3][4] = -ip.x;
      rshape[3][5] = -ip.y;
      break;
    }

    case Pyramid:
    {
      const double qrtr = 0.25;
      // shape function
      const double lterm0 = ip.x * ip.y * ip.z / (1.0 - ip.z);
      rshape[0][0] = qrtr * ( (1.0 + ip.x) * (1.0 + ip.y) - ip.z + lterm0 );
      rshape[0][1] = qrtr * ( (1.0 - ip.x) * (1.0 + ip.y) - ip.z - lterm0 );
      rshape[0][2] = qrtr * ( (1.0 - ip.x) * (1.0 - ip.y) - ip.z + lterm0 );
      rshape[0][3] = qrtr * ( (1.0 + ip.x) * (1.0 - ip.y) - ip.z - lterm0 );
      rshape[0][4] = ip.z;

      // derivative w.r.t. xi
      const double lterm1 = (ip.y * ip.z) / (1.0 - ip.z);
      rshape[1][0] = qrtr * (  (1.0 + ip.y) + lterm1 );
      rshape[1][1] = qrtr * ( -(1.0 + ip.y) - lterm1 );
      rshape[1][2] = qrtr * ( -(1.0 - ip.y) + lterm1 );
      rshape[1][3] = qrtr * (  (1.0 - ip.y) - lterm1 );
      rshape[1][4] = 0.0;

      // derivative w.r.t. eta
      const double lterm2 = (ip.x * ip.z) / (1.0 - ip.z);
      rshape[2][0] = qrtr * (  (1.0 + ip.x) + lterm2 );
      rshape[2][1] = qrtr * (  (1.0 - ip.x) - lterm2 );
      rshape[2][2] = qrtr * ( -(1.0 - ip.x) + lterm2 );
      rshape[2][3] = qrtr * ( -(1.0 + ip.x) - lterm2 );
      rshape[2][4] = 0.0;

      // derivative w.r.t zeta
      const double lterm3 = ((ip.x * ip.y * ip.z) / (1.0 - ip.z) * (1.0 - ip.z)) + (ip.x * ip.y / (1.0 - ip.z));
      rshape[3][0] = qrtr * ( -1.0 + lterm3 );
      rshape[3][1] = qrtr * ( -1.0 - lterm3 );
      rshape[3][2] = qrtr * ( -1.0 + lterm3 );
      rshape[3][3] = qrtr * ( -1.0 - lterm3 );
      rshape[3][4] = 1.0;
      break;
    }

    default:
      fprintf(stderr, "%s: Unimplemented element type! Aborting!\n", __func__);
      exit(1);
  }
}

/**
* @brief Compute Jacobian matrix from the real element to the reference element.
*
* @param rshape  Reference element shape derivatives.
* @param npts    Number of points.
* @param pts     Points array.
* @param J       Jacobian matrix.
*/
inline void jacobian_matrix(const dmat<double> & rshape,
                        const int npts,
                        const Point* pts,
                        double *J)
{
  // zero the 3x3 jacobian matrix
  memset (J, 0, 9 * sizeof(double) );
  // note that ref_shape hold shape functions in [0][i] and derivatives in [k][i]
  // if element is 2 dimensional then [3][i] is set to 0!
  for (int i = 0; i < npts; i++)
  {
    J[0] += rshape[1][i] * pts[i].x;
    J[1] += rshape[1][i] * pts[i].y;
    J[2] += rshape[1][i] * pts[i].z;
    J[3] += rshape[2][i] * pts[i].x;
    J[4] += rshape[2][i] * pts[i].y;
    J[5] += rshape[2][i] * pts[i].z;
    J[6] += rshape[3][i] * pts[i].x;
    J[7] += rshape[3][i] * pts[i].y;
    J[8] += rshape[3][i] * pts[i].z;
  }
}

inline void invert_jacobian_matrix(const elem_t type, double* J, double & detJ)
{
  switch(type)
  {
    // all 3D elems
    default:
    case Tetra:
      invert_3x3(J, detJ);
      break;

    // 2D elems
    case Quad:
    case Tri: {
      double J2[4] = {J[0], J[1], J[3], J[4]};

      invert_2x2(J2, detJ);
      detJ = fabs(detJ);

      J[0] = J2[0]; J[1] = J2[1]; J[2] = 0.0;
      J[3] = J2[2]; J[4] = J2[3]; J[5] = 0.0;
      J[6] = 0.0;   J[7] = 0.0;   J[8] = 0.0;
      break;
    }

    // 1D elem
    case Line:
      detJ = J[0];
      J[0] = 1.0 / detJ;
      break;
  }
}

/**
* @brief Compute shape derivatives for an element, based
* on the shape derivatives of the associated reference element.
*
* @param iJ      Inverse Jacobian from the reference element to the real element.
* @param rshape  Reference shape funciton and derivatives.
* @param ndof    Number of d.o.f.
* @param shape   Shape derivatives.
*/
inline void shape_deriv(const double *iJ,
                    const dmat<double> & rshape,
                    const int ndof,
                    dmat<double> & shape)
{
  for (int in = 0; in < ndof; in++)
  {
    shape[1][in] = iJ[0] * rshape[1][in] +
                   iJ[1] * rshape[2][in] +
                   iJ[2] * rshape[3][in];

    shape[2][in] = iJ[3] * rshape[1][in] +
                   iJ[4] * rshape[2][in] +
                   iJ[5] * rshape[3][in];

    shape[3][in] = iJ[6] * rshape[1][in] +
                   iJ[7] * rshape[2][in] +
                   iJ[8] * rshape[3][in];
  }
}


/**
* @brief Comfort class. Provides getter functions to access the mesh member variables
* more comfortably.
*
* @tparam T  Integer type.
* @tparam S  Floating point type.
*/
template<class T, class S>
class element_view
{
  private:
  const meshdata<T, S> & _mesh;    ///< Mesh reference.
  const vector<T> & _glob_numbr;   ///< the global numbering
  T        _esize;                 ///< The number of connectivity indices of that element.
  const T* _offset_con;            ///< A pointer to the start of the element in the mesh connectivity.
  size_t   _eidx;                  ///< The element index of the view.
  int      _rank;                  ///< The rank of the process. Used to access layouts.

  public:
  /**
  * @brief Constructor. Initializes to element index 0.
  *
  * @param [in] mesh The mesh which should be linked to the element view.
  */
  element_view(const meshdata<T, S> & mesh, const SF_nbr nbr) :
               _mesh(mesh), _glob_numbr(_mesh.get_numbering(nbr))
  {
    this->set_elem(0);
    MPI_Comm_rank(_mesh.comm, &_rank);
  }

  /**
  * @brief Set the view to a new element.
  *
  * @param [in] eidx An element index.
  */
  inline void set_elem(size_t eidx)
  {
    _eidx = eidx;

    T offset    = _mesh.dsp[_eidx];
    _esize      = _mesh.dsp[_eidx+1] - offset;
    _offset_con = _mesh.con.data()   + offset;
  }

  /**
  * @brief Select next element if possible.
  *
  * @return Whether there was a next element.
  */
  inline bool next()
  {
    if(_eidx < (_mesh.l_numelem - 1) )
    {
      this->set_elem(_eidx + 1);
      return true;
    }
    else
      return false;
  }

  /**
  * @brief Getter function for the number of nodes.
  *
  * @return The number of nodes.
  */
  inline T num_nodes() const
  {
    return _esize;
  }

  /**
  * @brief Getter function for the element type.
  *
  * @return The element type.
  */
  inline elem_t type() const
  {
    return _mesh.type[_eidx];
  }

  /**
  * @brief Getter function for the element tag.
  *
  * @return The element tag.
  */
  inline T tag() const
  {
    return _mesh.tag[_eidx];
  }

  /**
  * @brief Access the connectivity information.
  *
  * @param [in] nidx Connectivity index w.r.t. the current element (e.g. 0-3 for a Tetra).
  *
  * @return Local index w.r.t. to the local domain. Use the mesh numberings to access global indices.
  */
  inline const T & node(short nidx) const
  {
    return _offset_con[nidx];
  }

  /**
  * @brief Access the connectivity information.
  *
  * @param [in] nidx Connectivity index w.r.t. the current element (e.g. 0-3 for a Tetra).
  *
  * @return Local index w.r.t. to the local domain. Use the mesh numberings to access global indices.
  */
  inline const T & global_node(short nidx) const
  {
    return _glob_numbr[_offset_con[nidx]];
  }


  /**
  * @brief Access the connectivity information.
  *
  * @return Local connectivity indices.
  */
  inline const T* nodes() const
  {
    return _offset_con;
  }
  /**
  * @brief Access vertex coordinates
  *
  * @param [in] nidx Connectivity index w.r.t. the current element (e.g. 0-3 for a Tetra).
  *
  * @return Vertex coordinate.
  */
  inline Point coord(short nidx) const
  {
    T idx = _offset_con[nidx];
    return {_mesh.xyz[idx*3+0], _mesh.xyz[idx*3+1], _mesh.xyz[idx*3+2]};
  }

  /**
  * @brief Get element fiber direction.
  *
  * @return Fiber direction.
  */
  inline Point fiber() const
  {
    return {_mesh.fib[_eidx*3+0], _mesh.fib[_eidx*3+1], _mesh.fib[_eidx*3+2]};
  }
  /**
  * @brief Get element sheet direction.
  *
  * @return Sheet direction.
  */
  inline Point sheet() const
  {
    if(_mesh.she.size())
      return {_mesh.she[_eidx*3+0], _mesh.she[_eidx*3+1], _mesh.she[_eidx*3+2]};
    else
      return {0,0,0};
  }

  /**
  * @brief Check if a sheet direction is present.
  *
  * @return Whether sheet direction is present.
  */
  bool has_sheet() const
  {
    return _mesh.she.size() > 0;
  }

  /**
  * @brief Get currently selected element index.
  *
  * @return Element index.
  */
  inline size_t element_index() const
  {
    return _eidx;
  }

  /**
  * @brief Get currently selected element index.
  *
  * @return Element index.
  */
  inline size_t global_element_index() const
  {
    return _mesh.epl.algebraic_layout()[_rank] + _eidx;
  }

  inline short num_dof(short order) const
  {
    return SF::num_dof(_mesh.type[_eidx], order);
  }

  inline void integration_points(const short order, Point* ip, double* w, int & nint) const
  {
    general_integration_points(_mesh.type[_eidx], order, ip, w, nint);
  }

  inline short dimension() const
  {
    switch(_mesh.type[_eidx])
    {
      default:
      case Tetra:
        return 3;

      case Tri:
      case Quad:
        return 2;

      case Line:
        return 1;
    }
  }
};

/**
* @brief Abstract matrix integration base class.
*
* @tparam T  Integer type used in associated element_view (thus in the mesh)
* @tparam S  Floating point type used in associated element_view (thus in the mesh)
*/
template<class T, class S>
class matrix_integrator
{
  public:
  /// compute the element matrix for a given element.
  virtual void operator() (const element_view<T,S> & elem, dmat<double> & buff) = 0;
  /// return (by referrence) the row and column dimensions
  virtual void dpn(T & row_dpn, T & col_dpn) = 0;
};

/**
* @brief Abstract vector integration base class.
*
* @tparam T  Integer type used in associated element_view (thus in the mesh)
* @tparam S  Floating point type used in associated element_view (thus in the mesh)
*/
template<class T, class S>
class vector_integrator
{
  protected:
  void zero_buff(double* buff, T nrows)
  {
    for(T i=0; i<nrows; i++) buff[i] = 0.0;
  }

  public:
  /// compute the element matrix for a given element.
  virtual void operator() (const element_view<T,S> & elem, double* buff) = 0;
  /// return (by referrence) the row and column dimensions
  virtual void dpn(T & dpn) = 0;
};

/**
* @brief Compute the node-to-node connectivity.
*
* @param mesh     The mesh.
* @param n2n_cnt  Counts of the connectivity matrix rows.
* @param n2n_con  Column indices of the connectivity matrix.
*/
template<class T, class S>
inline void nodal_connectivity_graph(const meshdata<T, S> & mesh,
                              vector<T> & n2n_cnt,
                              vector<T> & n2n_con)
{
  vector<T> e2n_cnt, n2e_cnt, n2e_con;
  const vector<T> & e2n_con = mesh.con;

  cnt_from_dsp(mesh.dsp, e2n_cnt);

  transpose_connectivity(e2n_cnt, e2n_con, n2e_cnt, n2e_con);
  multiply_connectivities(n2e_cnt, n2e_con,
                          e2n_cnt, e2n_con,
                          n2n_cnt, n2n_con);
}

/**
* @brief Compute the maximum number of node-to-node edges for a mesh.
*
* @tparam T    Integer type.
* @tparam S    Floating point type.
* @param mesh  The mesh.
*
* @return The computed maximum.
*/
template<class T, class S>
int max_nodal_edgecount(const meshdata<T,S> & mesh)
{
  vector<T> n2n_cnt, n2n_con;
  nodal_connectivity_graph(mesh, n2n_cnt, n2n_con);

  int nmax = 0;
  for(size_t nidx=0; nidx < n2n_cnt.size(); nidx++)
    if(nmax < n2n_cnt[nidx]) nmax = n2n_cnt[nidx];

  MPI_Allreduce(MPI_IN_PLACE, &nmax, 1, MPI_INT, MPI_MAX, mesh.comm);

  return nmax;
}


/**
* @brief Compute canonical indices from nodal indices and dpn.
*
* @param nidx  Nodal indices in local indexing.
* @param nbr   Numbering to use.
* @param esize Element size.
* @param dpn   Degrees of freedom per node.
* @param cidx  Canonical indices.
*/
template<class T, class V>
inline void canonic_indices(const T* nidx,
                     const T* nbr,
                     const T esize,
                     const short dpn,
                     V* cidx)
{
  for(T i=0; i<esize; i++)
    for(short j=0; j<dpn; j++)
      cidx[i*dpn + j] = nbr[nidx[i]]*dpn + j;
}

// forward declaration
template<class T, class S> class petsc_vector;


/**
* @brief FEM matrix class.
*
* It connects a mesh with a PETSc matrix.
*
* @tparam T  Integer type.
* @tparam S  Floating point type.
*/
template<class T, class S>
class abstract_matrix
{
  public:
  /**
  * @brief init from mesh
  *
  * @param imesh      mesh we associated to matrix
  * @param irow_dpn   row dpn
  * @param icol_dpn   col dpn
  * @param max_edges  max number of edges, if -1 max edges will be computed on the fly
  */
  virtual void init(const meshdata<T, S> & imesh,
                   const PetscInt irow_dpn,
                   const PetscInt icol_dpn,
                   const PetscInt max_edges) = 0;

  virtual int dpn_row() const = 0;
  virtual int dpn_col() const = 0;
  virtual const meshdata<T,S>* mesh_ptr() const = 0;

  /// zero matrix valse
  virtual void zero() = 0;

  /// multiply matrix with x into b
  virtual void mult(petsc_vector<T,S> & x, petsc_vector<T,S> & b) const = 0;

  /// compute M := L M R, with diagonal matrices L and R
  virtual void mult_LR(petsc_vector<T,S> & L, petsc_vector<T,S> & R) = 0;

  /// add values to diagonal
  virtual void diag_add(petsc_vector<T,S> & diag) = 0;

  /// store matrix diagonal in vector
  virtual void get_diagonal(petsc_vector<T,S> & vec) = 0;

  /// mark matrix assembly as finished
  virtual void finish_assembly() = 0;

  /// scale matrix
  virtual void scale(PetscReal s) = 0;

  virtual void add_scaled_matrix(const abstract_matrix<T,S>* A, const PetscReal s,
                                 const bool same_nnz) = 0;

  virtual void duplicate (const abstract_matrix<T,S>* M) = 0;

  virtual void set_values(vector<PetscInt> & row_idx, vector<PetscInt> & col_idx,
                         vector<PetscReal> & vals, bool add) = 0;
  virtual void set_values(vector<PetscInt> & row_idx, vector<PetscInt> & col_idx,
                         PetscReal* vals, bool add) = 0;
  virtual void set_value(PetscInt row_idx, PetscInt col_idx,
                         PetscReal val, bool add) = 0;
  /// write matrx to disk
  virtual void write(const char* filename) = 0;
};



/**
* @brief FEM matrix class.
*
* It connects a mesh with a PETSc matrix.
*
* @tparam T  Integer type.
* @tparam S  Floating point type.
*/
template<class T, class S>
class petsc_matrix : public abstract_matrix<T,S>
{
  public:
  int  NRows;         ///< global number of rows
  int  NCols;         ///< global number of cols
  int  row_dpn;       ///< row dpn
  int  col_dpn;       ///< col dpn

  int  lsize;         //!< size of local matrix (\#locally stored rows)

  /// the petsc Mat object
  mutable Mat data;

  /// pointer to the associated mesh
  const meshdata<T, S> * mesh;

  // empty constructor
  petsc_matrix() :
  NRows(0), NCols(0),
  row_dpn(0), col_dpn(0),
  lsize(0),
  data(NULL),
  mesh(NULL)
  {}

  // destructor
  ~petsc_matrix()
  {
    if(data) MatDestroy(&data);
  }

  // We need this getter functions to be able to access this members even through
  // the abstract_matrix interface.
  inline int dpn_row() const {return row_dpn;}
  inline int dpn_col() const {return col_dpn;}
  inline const meshdata<T,S>* mesh_ptr() const {return mesh;}

  /**
  * @brief algebraic init
  *
  * @param iNRows       global number of rows
  * @param iNCols       global number of cols
  * @param ilrows       local number of rows
  * @param ilcols       local number of cols
  * @param mxent        maximum expected number of entries per row
  */
  inline void init(PetscInt iNRows,
                   PetscInt iNCols,
                   PetscInt ilrows,
                   PetscInt ilcols,
                   PetscInt mxent)
  {
    NRows = iNRows;
    NCols = iNCols;
    lsize = ilrows;
    row_dpn = 1;
    col_dpn = 1;

    MatCreateAIJ(PETSC_COMM_WORLD, ilrows, ilcols, NRows, NCols, mxent, PETSC_NULL, mxent, PETSC_NULL, &data);
    MatSetFromOptions(data);
    MatMPIAIJSetPreallocation(data, mxent, PETSC_NULL, mxent, PETSC_NULL);

    MatSetOption(data, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    this->zero();
  }

  /**
  * @brief init from mesh
  *
  * @param imesh      mesh we associated to matrix
  * @param irow_dpn   row dpn
  * @param icol_dpn   col dpn
  * @param max_edges  max number of edges, if -1 max edges will be computed on the fly
  */
  inline void init(const meshdata<T, S> & imesh,
                   const PetscInt irow_dpn,
                   const PetscInt icol_dpn,
                   const PetscInt max_edges = -1)
  {
    mesh = &imesh;
    row_dpn = irow_dpn;
    col_dpn = icol_dpn;

    int rank;
    MPI_Comm_rank(mesh->comm, &rank);

    PetscInt M = mesh->pl.num_global_idx()*row_dpn;
    PetscInt N = mesh->pl.num_global_idx()*col_dpn;
    PetscInt m = mesh->pl.num_algebraic_idx()*row_dpn;
    PetscInt n = mesh->pl.num_algebraic_idx()*col_dpn;

    PetscInt nmax = max_edges;
    if(max_edges < 0) nmax = max_nodal_edgecount(*mesh);

    init(M, N, m, n, nmax*col_dpn);
  }

  /// zero matrix valse
  inline void zero()
  {
    MatZeroEntries(data);
  }

  /// multiply matrix with x into b
  inline void mult(petsc_vector<T,S> & x, petsc_vector<T,S> & b) const
  {
    MatMult(data, x.data, b.data);
  }

  /// compute M := L M R, with diagonal matrices L and R
  inline void mult_LR(petsc_vector<T,S> & L, petsc_vector<T,S> & R)
  {
    MatDiagonalScale(data, L.data, R.data);
  }

  /// compute M += sA
  inline void add_scaled_matrix(const abstract_matrix<T,S>* A, const PetscReal s,
                                const bool same_nnz)
  {
    // we require that the abstract_matrix is actually a petsc_matrix
    petsc_matrix<T,S>* pA = (petsc_matrix<T,S>*)A;

    MatStructure NNZ = same_nnz ? SAME_NONZERO_PATTERN : DIFFERENT_NONZERO_PATTERN;
    MatAXPY(data, s, pA->data, NNZ);
  }

  /// add values to diagonal
  inline void diag_add(petsc_vector<T,S> & diag)
  {
    MatDiagonalSet(data, diag.data, ADD_VALUES);
  }

  /// store matrix diagonal in vector
  inline void get_diagonal(petsc_vector<T,S> & vec)
  {
    MatGetDiagonal(data, vec.data);
  }

  /// mark matrix assembly as finished
  inline void finish_assembly()
  {
    MatAssemblyBegin(data, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd  (data, MAT_FINAL_ASSEMBLY);
  }

  /// scale matrix
  inline void scale(PetscReal s)
  {
    MatScale(data, s);
  }

  inline void set_values(vector<PetscInt> & row_idx, vector<PetscInt> & col_idx,
                         vector<PetscReal> & vals, bool add)
  {
    // add values into system matrix
    MatSetValues(data, row_idx.size(), row_idx.data(), col_idx.size(), col_idx.data(),
                 vals.data(), add ? ADD_VALUES : INSERT_VALUES);
  }
  inline void set_values(vector<PetscInt> & row_idx, vector<PetscInt> & col_idx,
                         PetscReal* vals, bool add)
  {
    // add values into system matrix
    MatSetValues(data, row_idx.size(), row_idx.data(), col_idx.size(), col_idx.data(),
                 vals, add ? ADD_VALUES : INSERT_VALUES);
  }

  inline void set_value(PetscInt row_idx, PetscInt col_idx, PetscReal val, bool add)
  {
    MatSetValue(data, row_idx, col_idx, val, add ? ADD_VALUES : INSERT_VALUES);
  }

  /// matrix deep copy
  // inline void operator= (const petsc_matrix<T,S> & inp_mat)
  inline void duplicate (const abstract_matrix<T,S>* M)
  {
    // we require that the abstract_matrix is actually a petsc_matrix
    petsc_matrix<T,S> & inp_mat = *((petsc_matrix<T,S>*)M);

    NRows   = inp_mat.NRows;
    NCols   = inp_mat.NCols;
    row_dpn = inp_mat.row_dpn;
    col_dpn = inp_mat.col_dpn;
    lsize   = inp_mat.lsize;
    mesh    = inp_mat.mesh;

    if(data) MatDestroy(&data);
    MatDuplicate(inp_mat.data, MAT_COPY_VALUES, &data);
  }

  /// write matrx to disk
  inline void write(const char* filename)
  {
    PetscViewer viewer;
    PetscViewerBinaryOpen(mesh ? mesh->comm : PETSC_COMM_WORLD, filename, FILE_MODE_WRITE, &viewer);
    MatView(data, viewer);
    PetscViewerDestroy(&viewer);
  }
};

/**
* @brief A class encapsulating a petsc vector.
*
* It is connected to a mesh and offers some convenience features
* w.r.t. setup and function calls.
*
*/
template<class T, class S>
class petsc_vector {

  public:

  enum ltype {algebraic, nodal, elemwise, unset};

  Vec                   data   = NULL;    ///< the PETSc vector pointer
  const meshdata<T,S> * mesh   = NULL;    ///< the connected mesh
  int                   dpn    = 0;       ///< d.o.f. per mesh vertex.
  ltype                 layout = unset;   ///< used vector layout (nodal, algebraic, unset)

  petsc_vector()
  {}

  ~petsc_vector()
  {
    if(data) VecDestroy(&data);
  }

  petsc_vector(const meshdata<T,S> & imesh, int idpn, ltype inp_layout) {
    init(imesh, idpn, inp_layout);
  }

  petsc_vector(int igsize, int ilsize, int idpn, ltype ilayout) {
    init(igsize, ilsize, idpn, ilayout);
  }

  petsc_vector(const petsc_vector<T,S> & vec) {
    init(vec);
  }

  /**
  * @brief Init the vector dimensions based on a give mesh.
  *
  * @param imesh  The mesh defining the parallel layout of the vector.
  * @param idpn   The number of d.o.f. per mesh vertex.
  */
  inline void init(const meshdata<T,S> & imesh, int idpn, ltype inp_layout)
  {
    mesh = &imesh;
    dpn = idpn;
    int N = 0, n = 0;

    switch(inp_layout) {
      case algebraic:
        N = mesh->pl.num_global_idx()*dpn;
        n = mesh->pl.num_algebraic_idx()*dpn;
        layout = algebraic;
        break;

      case nodal:
        N = mesh->l_numpts*dpn;
        MPI_Allreduce(MPI_IN_PLACE, &N, 1, MPI_INT, MPI_SUM, mesh->comm);
        n = mesh->l_numpts*dpn;
        layout = nodal;
        break;

      case elemwise:
        N = mesh->g_numelem;
        n = mesh->l_numelem;
        layout = elemwise;
        break;

      default: break;
    }

    if(data) VecDestroy(&data);
    VecCreateMPI(PETSC_COMM_WORLD, n, N, &data);
  }

  /**
  * @brief Init parallel petsc vector directly with sizes.
  *
  * @param igsize  Global size
  * @param ilsize  Local size
  * @param idpn    Number of d.o.f. used per mesh node
  * @param ilayout Vector layout w.r.t. used mesh.
  */
  inline void init(int igsize, int ilsize, int idpn = 1, ltype ilayout = unset) {
    if(data) VecDestroy(&data);

    int ierr;
    if (ilsize == igsize && igsize >= 0) {
      ierr = VecCreateSeq(PETSC_COMM_SELF, igsize, &data);
    }
    else if (ilsize >= 0) {
      ierr = VecCreateMPI(PETSC_COMM_WORLD, ilsize, abs(igsize), &data);
    }
    else {
      ierr = VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, abs(igsize), &data);
    }
    // ierr = VecSetFromOptions (v);
    // ierr = VecSetBlockSize(v, bs);

    mesh = NULL;
    dpn = idpn;
    layout = ilayout;
    set(0.0);
  }

  /**
  * @brief Initialize a vector from the setup of a given vector.
  *
  * @param vec Vector to replicate the setup from.
  */
  inline void init(const petsc_vector<T,S> & vec) {
    if(data) VecDestroy(&data);

    VecDuplicate(vec.data, &data);

    mesh   = vec.mesh;
    dpn    = vec.dpn;
    layout = vec.layout;
    set(0.0);
  }

  /**
  * @brief Set vector values.
  *
  * @param idx  The global indices where to set.
  * @param vals The values to set.
  */
  inline void set(const vector<PetscInt> & idx, const vector<PetscReal> & vals, const bool additive = false)
  {
    InsertMode mode = additive ? ADD_VALUES : INSERT_VALUES;
    VecSetValues(data, idx.size(), idx.data(), vals.data(), mode);
    VecAssemblyBegin(data);
    VecAssemblyEnd(data);
  }
  /**
  * @brief Set vector indices to one value.
  *
  * @param idx  The indices where to set.
  * @param val  The value to set.
  */
  inline void set(const vector<PetscInt> & idx, const PetscReal val)
  {
    vector<PetscReal> vals(idx.size(), val);
    set(idx, vals);
  }
  /// set whole vector to one val
  inline void set(const PetscReal val)
  {
    VecSet(data, val);
  }

  /// scale vector
  inline void operator *= (const PetscReal sca)
  {
    // VecScale(data, sca);
    PetscReal* w = this->ptr();
    for(PetscInt i=0; i<this->lsize(); i++) w[i] *= sca;
    this->release_ptr(w);
  }
  /// scale vector
  inline void operator /= (const PetscReal sca)
  {
    (*this) *= 1.0 / sca;
  }

  inline void operator *= (petsc_vector<T,S> & vec)
  {
    PetscInt mysize = this->lsize();
    assert(mysize == vec.lsize());

    PetscReal* w = this->ptr();
    PetscReal* v = vec.ptr();

    for(PetscInt i=0; i < mysize; i++) w[i] *= v[i];

    this->release_ptr(w);
    this->release_ptr(v);
  }

  void add_scaled(petsc_vector<T,S> & vec, PetscReal k)
  {
    PetscInt mysize = this->lsize();
    assert(mysize == vec.lsize());

    PetscReal* w = this->ptr(), *r = vec.ptr();

    for(PetscInt i=0; i<mysize; i++)
      w[i] += r[i] * k;

    this->release_ptr(w);
    vec.release_ptr(r);
  }

  /// vector add
  inline void operator += (petsc_vector<T,S> & vec)
  {
    this->add_scaled(vec, 1.0);
  }
  /// vector sub
  inline void operator -= (petsc_vector<T,S> & vec)
  {
    this->add_scaled(vec, -1.0);
  }

  /// scalar add, equivalent to VecShift
  inline void operator += (PetscReal c)
  {
    PetscReal* w = this->ptr();

    for(PetscInt i=0; i<this->lsize(); i++) w[i] += c;

    this->release_ptr(w);
  }

  /// deep copy vector
  inline void operator= (const vector<PetscReal> & rhs)
  {
    assert(this->lsize() == rhs.size());

    PetscReal *w = this->ptr();
    const PetscReal *r = rhs.data();

    for(size_t i=0; i<rhs.size(); i++)
      w[i] = r[i];

    this->release_ptr(w);
  }
  /// deep copy petsc vector
  inline void operator= (petsc_vector<T,S> & rhs)
  {
    int mylsize = this->lsize();

    assert(mylsize == rhs.lsize());
    PetscReal* w = this->ptr(), *r = rhs.ptr();

    for(PetscInt i=0; i<mylsize; i++) w[i] = r[i];

    this->release_ptr(w);
    rhs.release_ptr(r);
  }

  inline void shallow_copy(petsc_vector<T,S> & v)
  {
    data   = v.data;
    mesh   = v.mesh;
    dpn    = v.dpn;
    layout = v.layout;
  }

  /** create a subvector which is defined on a superset of procs and has the same layout
   *
   * Processes not part of the original owner set will have no data
   *
   * \param sub    the vector on the subnodes
   * \param member true if the process is part of the original vector
   * \param offset offset of new vector into original
   * \param sz     number of entries to extract from original, -1=all
   * \param share  use the same memory
   */
  inline void overshadow(petsc_vector<T,S> & sub, bool member, int offset, int sz, bool share)
  {
    // RVector super;
    int        loc_size = 0;
    PetscReal* databuff = NULL;

    if(member) {
      PetscInt start, stop;
      sub.get_ownership_range(start, stop);

      if(sz == -1)
        sz = sub.gsize();

      PetscInt end    = offset + sz;
      PetscInt lstart = stop - start;

      if(start >= offset)
        lstart = 0;
      else if(offset < end)
        lstart = offset - start;

      int lstop  = 0;
      if(end >= stop)
        lstop = stop - start;
      else if(end >= start)
        lstop = end-start;

      loc_size = lstop - lstart;

      if(loc_size < 0)
        loc_size = 0;

      databuff = sub.ptr() + lstart;
    }

    if(share)
      VecCreateMPIWithArray(PETSC_COMM_WORLD, 1, loc_size, PETSC_DECIDE, databuff, &data);
    else
      VecCreateMPI(PETSC_COMM_WORLD, loc_size, PETSC_DECIDE, &data);

    if(member)
      sub.release_ptr(databuff);
  }

  /// get local size
  inline PetscInt lsize() const
  {
    if(!data) return 0;
    int loc_size; VecGetLocalSize(data, &loc_size);
    return loc_size;
  }

  /// get global size
  inline PetscInt gsize() const
  {
    if(!data) return 0;
    int glb_size; VecGetSize(data, &glb_size);
    return glb_size;
  }

  inline void get_ownership_range(PetscInt & start, PetscInt & stop)
  {
    VecGetOwnershipRange(data, &start, &stop);
  }

  /// get pointer to local data
  inline PetscReal* ptr()
  {
    PetscReal* p;
    // get pointer to local data
    VecGetArray(data, &p);
    return p;
  }

  /// release pointer to local data
  inline void release_ptr(PetscReal* & p)
  {
    // release local data
    VecRestoreArray(data, &p);
  }
  /// L2 norm
  inline PetscReal mag()
  {
    PetscReal ret;
    VecNorm(data, NORM_2, &ret);
    return ret;
  }
  inline PetscReal sum()
  {
    PetscReal ret;
    VecSum(data, &ret);
    return ret;
  }
  inline PetscReal dot(petsc_vector<T,S> & v)
  {
    PetscReal ret;
    VecDot(this->data, v.data, &ret);
    return ret;
  }

  /// return whether the vector is initialized
  bool is_init()
  {
    return this->data != NULL;
  }

  /**
  * @brief Write a vector to HD in ascii
  *
  * @param file           The file name.
  * @param write_header   Whether to write the vector size as a header.
  */
  inline size_t write_ascii(const char* file, bool write_header)
  {
    int size, rank;
    MPI_Comm comm = mesh != NULL ? mesh->comm : PETSC_COMM_WORLD;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);

    int glb_size = this->gsize();
    int loc_size = this->lsize();

    int err = 0;
    FILE* fd = NULL;
    long int nwr = 0;

    if(rank == 0) {
      fd = fopen(file, "w");
      if(!fd) err = 1;
    }

    MPI_Allreduce(MPI_IN_PLACE, &err, 1, MPI_INT, MPI_MAX, comm);
    if(err) {
      treat_file_open_error(file, __func__, errno, false, rank);
      return nwr;
    }

    PetscReal* p = this->ptr();

    if(rank == 0) {
      if(write_header)
        fprintf(fd, "%d\n", glb_size);

      for(int i=0; i<loc_size/dpn; i++) {
        for(int j=0; j<dpn; j++)
          nwr += fprintf(fd, "%f ", p[i*dpn+j]);
        nwr += fprintf(fd, "\n");
      }

      vector<PetscReal> wbuff;
      for(int pid=1; pid < size; pid++)
      {
        int rsize;
        MPI_Status stat;

        MPI_Recv(&rsize, 1, MPI_INT, pid, SF_MPITAG, comm, &stat);
        wbuff.resize(rsize);
        MPI_Recv(wbuff.data(), rsize*sizeof(PetscReal), MPI_BYTE, pid, SF_MPITAG, comm, &stat);

        for(int i=0; i<rsize/dpn; i++) {
          for(int j=0; j<dpn; j++)
            nwr += fprintf(fd, "%f ", wbuff[i*dpn+j]);
          nwr += fprintf(fd, "\n");
        }
      }
      fclose(fd);
    }
    else {
      MPI_Send(&loc_size, 1, MPI_INT, 0, SF_MPITAG, comm);
      MPI_Send(p, loc_size*sizeof(PetscReal), MPI_BYTE, 0, SF_MPITAG, comm);
    }

    this->release_ptr(p);
    MPI_Bcast(&nwr, 1, MPI_LONG, 0, comm);

    return nwr;
  }

  /**
  * @brief Write a vector to HD in binary. File descriptor is already set up.
  *
  * File descriptor is not closed by this function.
  *
  * @param fd   The already set up file descriptor.
  */
  template<typename V>
  inline size_t write_binary(FILE* fd)
  {
    int size, rank;
    MPI_Comm comm = mesh != NULL ? mesh->comm : PETSC_COMM_WORLD;
    MPI_Comm_rank(comm, &rank); MPI_Comm_size(comm, &size);

    long int loc_size = this->lsize();
    PetscReal* p = this->ptr();

    vector<V> buff(loc_size);
    for(long int i=0; i<loc_size; i++) buff[i] = p[i];

    long int nwr = root_write(fd, buff, comm);

    this->release_ptr(p);
    return nwr;
  }

  /// write binary. Open file descriptor myself.
  template<typename V>
  inline size_t write_binary(std::string file)
  {
    size_t nwr = 0;
    MPI_Comm comm = mesh != NULL ? mesh->comm : PETSC_COMM_WORLD;
    int rank; MPI_Comm_rank(comm, &rank);

    FILE* fd = NULL;
    int error = 0;

    if(rank == 0) {
      fd = fopen(file.c_str(), "w");
      if(fd == NULL)
        error++;
    }

    MPI_Allreduce(MPI_IN_PLACE, &error, 1, MPI_INT, MPI_SUM, comm);

    if(error == 0) {
      nwr = this->write_binary<V>(fd);
      fclose(fd);
    }
    else {
      treat_file_open_error(file.c_str(), __func__, errno, false, rank);
    }

    return nwr;
  }

  template<typename V>
  inline size_t read_binary(FILE* fd)
  {
    MPI_Comm comm = mesh != NULL ? mesh->comm : PETSC_COMM_WORLD;

    size_t loc_size = this->lsize();
    PetscReal* p = this->ptr();
    vector<V> buff(loc_size);

    size_t nrd = root_read(fd, buff, comm);

    for(size_t i=0; i<loc_size; i++)
      p[i] = buff[i];

    this->release_ptr(p);

    return nrd;
  }

  template<typename V>
  inline size_t read_binary(std::string file)
  {
    MPI_Comm comm = mesh != NULL ? mesh->comm : PETSC_COMM_WORLD;

    size_t nrd = 0;
    int rank; MPI_Comm_rank(comm, &rank);

    FILE* fd = NULL;
    int error = 0;

    if(rank == 0) {
      fd = fopen(file.c_str(), "r");
      if(fd == NULL)
        error++;
    }

    MPI_Allreduce(MPI_IN_PLACE, &error, 1, MPI_INT, MPI_SUM, comm);

    if(error == 0) {
      nrd = read_binary<V>(fd);
      fclose(fd);
    }
    else {
      treat_file_open_error(file.c_str(), __func__, errno, false, rank);
    }

    return nrd;
  }

  inline size_t read_ascii(FILE* fd)
  {
    MPI_Comm comm = mesh != NULL ? mesh->comm : PETSC_COMM_WORLD;

    size_t loc_size = this->lsize();
    PetscReal* p = this->ptr();

    size_t nrd = root_read_ascii(fd, p, loc_size, comm, false);
    return nrd;
  }

  inline size_t read_ascii(std::string file)
  {
    MPI_Comm comm = mesh != NULL ? mesh->comm : PETSC_COMM_WORLD;
    int rank; MPI_Comm_rank(comm, &rank);

    size_t nrd = 0;

    FILE* fd = NULL;
    int error = 0;

    if(rank == 0) {
      fd = fopen(file.c_str(), "r");
      if(fd == NULL)
        error++;
    }

    MPI_Allreduce(MPI_IN_PLACE, &error, 1, MPI_INT, MPI_SUM, comm);

    if(error == 0) {
      nrd = read_ascii(fd);
      if(fd) fclose(fd);
    }
    else {
      treat_file_open_error(file.c_str(), __func__, errno, false, rank);
    }

    return nrd;
  }
};



/**
* @brief Generalized matrix assembly.
*
* All the assembly details -- both the computation as well as the parameters handling --
* are encapsulated in the used integrator.
*
* @param mat         matrix to add values into
* @param domain      The domain we compute the integral on
* @param integrator  The integral computation functor
*/
template<class T, class S>
inline void assemble_matrix(abstract_matrix<T,S> & mat,
                            meshdata<T,S> & domain,
                            matrix_integrator<T,S> & integrator)
{
  int rank;
  MPI_Comm_rank(domain.comm, &rank);

  // we want to make sure that the element integrator fits the matrix
  T row_dpn, col_dpn;
  integrator.dpn(row_dpn, col_dpn);
  assert(mat.dpn_row() == row_dpn && mat.dpn_col() == col_dpn);

  // allocate row / col index buffers
  vector<PetscInt> row_idx(SF_MAX_ELEM_NODES * row_dpn), col_idx(SF_MAX_ELEM_NODES * col_dpn);

  // allocate elem index buffer
  dmat<PetscReal> ebuff(SF_MAX_ELEM_NODES * row_dpn, SF_MAX_ELEM_NODES * col_dpn);

  const vector<PetscInt> & petsc_nbr = domain.get_numbering(NBR_PETSC);

  // start with assembly
  element_view<T, S> view(domain, NBR_PETSC);
  for(size_t eidx=0; eidx < domain.l_numelem; eidx++)
  {
    // set element view to current element
    view.set_elem(eidx);

    // calculate row/col indices of entries
    PetscInt nnodes = view.num_nodes();

    row_idx.resize(nnodes*row_dpn);
    col_idx.resize(nnodes*col_dpn);
    canonic_indices(view.nodes(), petsc_nbr.data(), nnodes, row_dpn, row_idx.data());
    canonic_indices(view.nodes(), petsc_nbr.data(), nnodes, col_dpn, col_idx.data());

    // call integrator
    integrator(view, ebuff);

    // add values into system matrix
    const bool add = true;
    mat.set_values(row_idx, col_idx, ebuff.data(), add);
  }
  // finish assembly and progress output
  mat.finish_assembly();
}

template<class T, class S>
inline void assemble_lumped_matrix(abstract_matrix<T,S> & mat,
                                   meshdata<T,S> & domain,
                                   matrix_integrator<T,S> & integrator)
{
  int rank;
  MPI_Comm_rank(domain.comm, &rank);

  // we want to make sure that the element integrator fits the matrix
  T row_dpn, col_dpn;
  integrator.dpn(row_dpn, col_dpn);

  assert(row_dpn == 1 && row_dpn == mat.dpn_row());

  // allocate row / col index buffers
  vector<PetscInt> row_idx(SF_MAX_ELEM_NODES * row_dpn);

  // allocate elem index buffer
  dmat<PetscReal> ebuff(SF_MAX_ELEM_NODES * row_dpn, SF_MAX_ELEM_NODES * col_dpn);
  const vector<PetscInt> & petsc_nbr = domain.get_numbering(NBR_PETSC);

  // start with assembly
  element_view<T, S> view(domain, NBR_PETSC);
  for(size_t eidx=0; eidx < domain.l_numelem; eidx++)
  {
    // set element view to current element
    view.set_elem(eidx);

    // calculate row/col indices of entries
    PetscInt nnodes = view.num_nodes();
    row_idx.resize(nnodes*row_dpn);
    canonic_indices(view.nodes(), petsc_nbr.data(), nnodes, row_dpn, row_idx.data());

    // call integrator
    integrator(view, ebuff);

    // do lumping
    for(int i=0; i<nnodes; i++) {
      PetscReal rowsum = 0.0;

      for(int j=0; j<nnodes; j++)
          rowsum += ebuff[i][j];

      // add values into system matrix
      mat.set_value(row_idx[i], row_idx[i], rowsum, true);
    }
  }

  // finish assembly and progress output
  mat.finish_assembly();
}

/**
* @brief Generalized vector assembly.
*
* All the assembly details -- both the computation as well as the parameters handling --
* are encapsulated in the used integrator.
*
* @param vec         vector to add values into
* @param domain      The domain we compute the integral on
* @param integrator  The integral computation functor
* @param prg         Progress display class
*/
template<class T, class S>
inline void assemble_vector(petsc_vector<T,S> & vec,
                     meshdata<T,S> & domain,
                     vector_integrator<T,S> & integrator)
{
  int rank;
  MPI_Comm_rank(vec.mesh->comm, &rank);

  // we want to make sure that the element integrator fits the matrix
  T dpn;
  integrator.dpn(dpn);
  assert(vec.dpn == dpn);

  // allocate row / col index buffers
  vector<PetscInt> idx(SF_MAX_ELEM_NODES * dpn);

  // allocate elem index buffer
  vector<PetscReal> ebuff(SF_MAX_ELEM_NODES * dpn);
  const vector<PetscInt> & petsc_nbr = domain.get_numbering(NBR_PETSC);

  // start with assembly
  element_view<T, S> view(domain, NBR_PETSC);
  for(size_t eidx=0; eidx < domain.l_numelem; eidx++)
  {
    // set element view to current element
    view.set_elem(eidx);

    // calculate row/col indices of entries
    PetscInt nnodes = view.num_nodes();
    canonic_indices(view.nodes(), petsc_nbr.data(), nnodes, dpn, idx.data());

    // call integrator
    integrator(view, ebuff.data());

    // add values into system matrix
    VecSetValues(vec.data, nnodes*dpn, idx.data(), ebuff.data(), ADD_VALUES);
  }
  // finish assembly and progress output
  VecAssemblyBegin(vec.data);
  VecAssemblyEnd(vec.data);
}

template<class T, class S>
struct abstract_linear_solver
{
  const char*         name         = "unnamed";
  const char*         options_file = NULL;

  // solver statistics
  double       final_residual    = -1.0;                 //!< Holds the residual after convergence
  int          niter             = -1;                   //!< number of iterations
  int          reason            = 0;                    //!< number of iterations

  virtual void operator() (petsc_vector<T,S> & x, petsc_vector<T,S> & b) = 0;
};

/*
 * A basic structure to hold the most common datastructs that one might need for a PETSC
 * solver. The initialization of solvers varies hugely, as such we do not present an
 * initialization workflow on this code level. We rather expect the user to set up the
 * linear solvers in the context of the PDE type that is adressed. -Aurel
 *
 */
template<class T, class S>
struct petsc_solver : abstract_linear_solver<T,S>
{
  enum norm_t {absPreResidual, absUnpreResidual, relResidual, absPreRelResidual, norm_unset};

  petsc_matrix<T,S> * matrix           = NULL;
  norm_t              norm             = norm_unset;
  int                 max_it           = -1;
  bool                check_start_norm = false;

  KSP          ksp       = NULL;
  MatNullSpace nullspace = NULL;

  void operator() (petsc_vector<T,S> & x, petsc_vector<T,S> & b)
  {
    assert (x.data != b.data);
    assert (ksp != NULL);

    const double solve_zero = 1e-16;
    double NORMB = check_start_norm ? b.mag() : 1.0;

    if(NORMB > solve_zero)
    {
      KSPSolve(ksp, b.data, x.data);
      KSPGetIterationNumber(ksp, &(abstract_linear_solver<T,S>::niter));

      KSPConvergedReason r; KSPGetConvergedReason(ksp, &r);
      abstract_linear_solver<T,S>::reason = int(r);

      KSPGetResidualNorm(ksp, &(abstract_linear_solver<T,S>::final_residual));
    }
    else {
      abstract_linear_solver<T,S>::niter = 0;
      abstract_linear_solver<T,S>::final_residual = NORMB;
    }
  }
};

template<class T, class S> inline
void extract_element_data(const element_view<T,S> & view, petsc_vector<T,S> & vec, PetscReal* buffer)
{
  // the dpn has to be set for this to work
  int dpn = vec.dpn;
  typename petsc_vector<T,S>::ltype nodaltype = petsc_vector<T,S>::nodal;
  assert(dpn > 0);
  assert(vec.layout == nodaltype);

  PetscReal* pvec = vec.ptr();

  for(int i=0; i<view.num_nodes(); i++)
    for(int j=0; j<dpn; j++) {
      int idx = view.node(i)*dpn+j;
      buffer[i*dpn+j] = pvec[idx];
    }

  vec.release_ptr(pvec);
}

template<class T, class S> inline
void set_element_data(const element_view<T,S> & view, PetscReal* buffer, petsc_vector<T,S> & vec)
{
  // the dpn has to be set for this to work
  int dpn = vec.dpn;
  typename petsc_vector<T,S>::ltype nodaltype = petsc_vector<T,S>::nodal;
  assert(dpn > 0);
  assert(vec.layout == nodaltype);

  PetscReal* pvec = vec.ptr();

  for(int i=0; i<view.num_nodes(); i++)
    for(int j=0; j<dpn; j++) {
      int idx = view.node(i)*dpn+j;
      pvec[idx] = buffer[i*dpn+j];
    }

  vec.release_ptr(pvec);
}

template<class T, class S> inline
void get_transformed_pts(const element_view<T,S> & view, Point* loc_pts, Point & trsf_fibre)
{
  const elem_t type = view.type();

  switch(type) {
    case Line: {
      Point p0 = view.coord(0), p1 = view.coord(1);

      loc_pts[0] = {0, 0, 0};
      loc_pts[1] = {mag(p1 - p0), 0, 0};
      trsf_fibre = {1, 0, 0};
      break;
    }

    case Tri: {
      Point p0 = view.coord(0), p1 = view.coord(1), p2 = view.coord(2);
      Point f = trsf_fibre;

      Point p01 = p1 - p0, p02 = p2 - p0;

      Point x = normalize(p01);
      Point z = normalize(cross(p01,p02));
      Point y = cross(x, z);

      loc_pts[0] = {0, 0, 0};
      loc_pts[1] = {mag(p01), 0, 0};
      loc_pts[2] = {inner_prod(p02, x), inner_prod(p02, y), 0};
      trsf_fibre = {inner_prod(f, x), inner_prod(f, y), 0};

      if((fabs(trsf_fibre.x) + fabs(trsf_fibre.y)) < 1e-8) {
        fprintf(stderr, "Fibre direction is orthogonal to triangle. Assigning (1,0,0) fiber direction.\n");
        trsf_fibre = {1, 0, 0};
      }
      else trsf_fibre = normalize(trsf_fibre);
      break;
    }

    case Quad: {
      Point p0 = view.coord(0), p1 = view.coord(1), p2 = view.coord(2), p3 = view.coord(3);
      Point f  = trsf_fibre;

      Point p01 = p1 - p0, p02 = p2 - p0, p03 = p3 - p0;

      Point x = normalize(p01);
      Point z = normalize(cross(p01, p02));
      Point y = cross(x, z);

      loc_pts[0] = {0, 0, 0};
      loc_pts[1] = {mag(p01), 0, 0};
      loc_pts[2] = {inner_prod(p02, x), inner_prod(p02, y), 0};
      loc_pts[3] = {inner_prod(p03, x), inner_prod(p03, y), 0};

      trsf_fibre = {inner_prod(f, x), inner_prod(f, y), 0};

      if((fabs(trsf_fibre.x) + fabs(trsf_fibre.y)) < 1e-8) {
        fprintf(stderr, "Fibre direction is orthogonal to quad. Assigning (1,0,0) fiber direction.\n");
        trsf_fibre = {1, 0, 0};
      }
      else trsf_fibre = normalize(trsf_fibre);
      break;
    }

    // for 3D elements we just copy over the coords
    default:
      for(int i=0; i<view.num_nodes(); i++)
        loc_pts[i] = view.coord(i);

      break;
  }
}

}

#endif

