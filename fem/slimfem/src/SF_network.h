// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file SF_network.h
* @brief Functions related to network communication.
* @author Aurel Neic
* @version
* @date 2017-02-14
*/

#ifndef _SF_NETWORK_H
#define _SF_NETWORK_H

namespace SF {

/**
 * \brief Exchange data in parallel over MPI.
 *
 * \param [in]  grph  The communications graph.
 * \param [in]  send  The send buffer.
 * \param [out] revc  The receive buffer.
 *
 */
template<class T, class S>
inline void MPI_Exchange(commgraph<T> & grph,
                         vector<S> & send,
                         vector<S> & recv,
                         MPI_Comm comm)
{
  int size, rank;
  MPI_Comm_size(comm, &size); MPI_Comm_rank(comm, &rank);

  size_t numsend = 0, numrecv = 0;
  for(int i=0; i<size; i++) {
    if(grph.scnt[i]) numsend++;
    if(grph.rcnt[i]) numrecv++;
  }

  vector<MPI_Request> sreq(numsend), rreq(numrecv);
  vector<MPI_Status> stat(numsend > numrecv ? numsend : numrecv);

  size_t sidx = 0, ridx = 0;
  for(int i=0; i<size; i++) {
    if(grph.rcnt[i])
      MPI_Irecv(recv.data() + grph.rdsp[i], grph.rcnt[i]*sizeof(S), MPI_BYTE, i, SF_MPITAG, comm, rreq.data()+ridx++);
    if(grph.scnt[i])
      MPI_Isend(send.data() + grph.sdsp[i], grph.scnt[i]*sizeof(S), MPI_BYTE, i, SF_MPITAG, comm, sreq.data()+sidx++);
  }

  if( (ridx != numrecv) || (sidx != numsend) )
    fprintf(stderr, "Rank %d: MPI_Exchange error! \n", rank);

  MPI_Waitall(ridx, rreq.data(), stat.data());
  MPI_Waitall(sidx, sreq.data(), stat.data());
}


/**
* @brief Get a fallback value for operations on parallel vectors if the local vector is of 0 size.
*
* The fallback value is the first value of the first rank with a nonzero local size.
*
* @tparam T    Vector data type
* @param vec   the vector.
* @param comm  MPI communicator used.
*
* @return The fallback value.
*/
template<class T>
inline T parallel_fallback_value(const vector<T> & vec, MPI_Comm comm)
{
  int rank, size;
  MPI_Comm_rank(comm, &rank); MPI_Comm_size(comm, &size);

  vector<size_t> vec_sizes(size);
  size_t lsize = vec.size();

  MPI_Allgather(&lsize, sizeof(size_t), MPI_BYTE, vec_sizes.data(), sizeof(size_t), MPI_BYTE, comm);

  int fallback_rank = 0;
  while(fallback_rank < size && vec_sizes[fallback_rank] == 0) fallback_rank++;

  assert(vec_sizes[fallback_rank]);

  T fallback_value = T();
  if(rank == fallback_rank)
    fallback_value = vec[0];

  MPI_Bcast(&fallback_value, sizeof(T), MPI_BYTE, fallback_rank, comm);

  return fallback_value;
}


/**
* @brief Compute the global minimum of a distributed vecor.
*
* @param [in] vec
* @param [in] comm
*
* @return The global minimum.
*/
template<class T>
inline T global_min(const vector<T> & vec, MPI_Comm comm)
{
  int size;
  MPI_Comm_size(comm, &size);

  int zero_size_err = 0;
  if(vec.size() == 0) zero_size_err++;
  MPI_Allreduce(MPI_IN_PLACE, &zero_size_err, 1, MPI_INT, MPI_SUM, comm);

  T fb = T();
  if(zero_size_err)
    fb = parallel_fallback_value(vec, comm);

  vector<T> mins(size);

  T lmin = vec.size() ? *std::min_element(vec.begin(), vec.end()) : fb;
  MPI_Allgather(&lmin, sizeof(T), MPI_BYTE, mins.data(), sizeof(T), MPI_BYTE, comm);
  T gmin = *std::min_element(mins.begin(), mins.end());

  return gmin;
}
/**
* @brief Compute the global maximum of a distributed vecor.
*
* @param [in] vec
* @param [in] comm
*
* @return The global maximum.
*/
template<class T>
inline T global_max(const vector<T> & vec, MPI_Comm comm)
{
  int size;
  MPI_Comm_size(comm, &size);

  int zero_size_err = 0;
  if(vec.size() == 0) zero_size_err++;
  MPI_Allreduce(MPI_IN_PLACE, &zero_size_err, 1, MPI_INT, MPI_SUM, comm);

  T fb = T();
  if(zero_size_err)
    fb = parallel_fallback_value(vec, comm);

  vector<T> max(size);

  T lmax = vec.size() ? *std::max_element(vec.begin(), vec.end()) : fb;
  MPI_Allgather(&lmax, sizeof(T), MPI_BYTE, max.data(), sizeof(T), MPI_BYTE, comm);
  T gmax = *std::max_element(max.begin(), max.end());

  return gmax;
}
/**
* @brief Compute the global maximum of a distributed vecor.
*
* @param [in] vec
* @param [in] comm
*
* @return The global maximum.
*/
template<class T>
inline T global_max(const T val, MPI_Comm comm)
{
  int size;
  MPI_Comm_size(comm, &size);
  vector<T> max(size);

  MPI_Allgather(&val, sizeof(T), MPI_BYTE, max.data(), sizeof(T), MPI_BYTE, comm);
  T gmax = *std::max_element(max.begin(), max.end());

  return gmax;
}

}
#endif
