To build the tests for slimfem, first do a developer build by running
 
`make`
 
in the main openCARP directory. This will generate the static libraries for every module in openCARP. Then navigate to fem/slimfem, and run
 
`make test_integrators`
 
This will compile a binary `test_integrators` that reads a mesh, computes a mass matrix for it and outputs the mass matrix to disk. The usage is:
 
`mpirun -np N ./test_integrators /path/to/mesh`
 
Note that the `/path/to/mesh` must not contain the mesh file extension (e.g. `.elem` or `.belem).
