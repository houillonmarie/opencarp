// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file solver_utils.h
* @brief Utilities for linear solvers.
* @author Aurel Neic, Gernot Plank, Edward Vigmond
* @version 
* @date 2019-10-25
*/

#ifndef _SOLVER_UTILS_H
#define _SOLVER_UTILS_H

#include "petsc_utils.h"
#include "basics.h"
#include "sf_interface.h"

namespace opencarp {

void set_stopping_criterion(sf_petsc_sol & s,
                            const sf_petsc_sol::norm_t normtype,
                            const double tol,
                            const int max_it,
                            FILE_SPEC logger,
                            bool verbose);

/** insert petsc solver options file
 *
 * \param s        solver context
 * \param default_opts_str  string with default options. used if no options file provided.
 * \param logger   The file descriptor we write to if we are verbose.
 * \param verbose  Whether to be verbose or not.
 *
 * \return  0, if successful
 *         -1, otherwise, i.e. options file not found
 */
int insert_petsc_solver_opts(sf_petsc_sol & s, const char* default_opts_str, FILE_SPEC logger, bool verbose);

//! for analysis of the \#iterations to solve CG
struct lin_solver_stats {
  int    min          = INT_MAX; //!< minimum \#interations
  int    max          = 0;     //!< maximum \#iterations
  int    tot          = 0;     //!< total \#
  int    last_tot     = 0;     //!< previous total \#
  int    iter         = 0;     //!< its previous solver step
  int    solves       = 0;     //!< \#solutions performed
  int    totsolves    = 0;     //!< total \# of solutions
  double slvtime      = 0.0;   //!< total solver time
  double lastSlvtime  = 0.0;   //!< total solver time
  FILE_SPEC  logger   = NULL;  //!< file in which to write stats

  ~lin_solver_stats()
  {
    f_close(logger);
  }

  void init_logger(const char* filename);
  void update_iter(const int curiter);
  void log_stats(double tm, bool cflg);
};

}  // namespace opencarp

#endif
