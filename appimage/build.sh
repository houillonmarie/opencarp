#!/bin/bash
# build openCARP and generate its appimage

BUILD_DIR=_build

PARENT_PATH=$( cd $(dirname $BASH_SOURCE) ; pwd -P )
OPENCARP_DIR=$PARENT_PATH/..
BUILD_APP_DIR=$OPENCARP_DIR/$BUILD_DIR

# build opencarp and install to AppDir
mkdir -p $BUILD_APP_DIR
cd $BUILD_APP_DIR
cmake .. -DDLOPEN=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr
make install DESTDIR=AppDir

# copy petsc header folder for make_dynamiic_model.sh
mkdir -p AppDir/usr/lib/petsc
if [ -d "${PETSC_DIR}/include" ]; then
    echo Copying PETSc header folder from ${PETSC_DIR}/include
    cp -r ${PETSC_DIR}/include AppDir/usr/lib/petsc
elif [ -d "${PETSC_DIR}/${PETSC_ARCH}/include" ]; then
    echo Copying PETSc header folder from ${PETSC_DIR}/${PETSC_ARCH}/include
    cp -r ${PETSC_DIR}/${PETSC_ARCH}/include AppDir/usr/lib/petsc
else
    echo Missing PETSc header folder
    exit 1
fi

# run linuxdeploy to generate AppImage
export VERSION=latest
chmod +x $OPENCARP_DIR/appimage/linuxdeploy-x86_64.AppImage
$OPENCARP_DIR/appimage/linuxdeploy-x86_64.AppImage --appimage-extract
./squashfs-root/AppRun --appdir AppDir \
                       --desktop-file $OPENCARP_DIR/appimage/opencarp.desktop \
                       --icon-file $OPENCARP_DIR/appimage/opencarp.png \
                       --custom-apprun $OPENCARP_DIR/appimage/AppRun \
                       --output appimage
