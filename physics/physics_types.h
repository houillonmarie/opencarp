// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file physics_types.h
* @brief Basic physics types.
* @author Aurel Neic
* @version 
* @date 2019-10-25
*/

#ifndef PHYSICS_TYPES_H
#define PHYSICS_TYPES_H

#include "basics.h"

namespace opencarp {

// unit conversion
#define UM2_to_CM2                1.0e-8              //!< convert um^2 to cm^2
#define UM_to_CM                  1.0e-4              //!< convert um to cm
#define UM_to_MM                  1.0e-3              //!< convert um to mm
#define MM_to_UM                  1.0e3               //!< convert um to mm


/**
* @brief Identifier for the different physics we want to set up.
*
*  The order of the physics defines the order the in which initialize(), compute_step()
*  and output_step() will be called.
*
*  The physic_t does not correspond directly to mesh_t, since we have multiple
*  physics defined on one mesh (electrics).
*
*/
enum physic_t {
  ion_phys=0, elec_phys, mech_phys, diffK_phys, magnet_phys, opt_phys,
  fluid_phys, NUM_PHYSICS
};

/**
* @brief The abstract physics interface we can use to trigger all physics
*/
class Basic_physic {
public:
  /// The name of the physic, each physic should have one.
  const char* name = NULL;
  /// The logger of the physic, each physic should have one.
  FILE_SPEC logger = NULL;
  /// the timer index received from the timer manager
  int timer_idx = -1;

  // For very heavy objects like the physics, we might not want to use the
  // constructor / destructor mechanisms, to avoid expensive operations when
  // someone (accidentaly) creates a local copy
  virtual void initialize() = 0;
  virtual void destroy()    = 0;
  virtual void compute_step() = 0;
  virtual void output_step() = 0;

  virtual inline void output_timings()
  {
    if(name) log_msg(0,0,0, "%s:", name);
    else     log_msg(0,0,0, "<unnamed>:");
    log_msg(0,0,0, "  Init:    %.2lf sec", initialize_time);
    log_msg(0,0,0, "  Compute: %.2lf sec", compute_time);
    log_msg(0,0,0, "  Output:  %.2lf sec", output_time);
    log_msg(0,0,0, "");
  }

  // we also store timings for the main Basic_physic API
  double initialize_time = 0.0;
  double compute_time    = 0.0;
  double output_time     = 0.0;

  virtual std::string timer_unit(const int timer_id) = 0;
  virtual double timer_val(const int timer_id) = 0;
};


/**
* @brief Enum used to adress the different data vectors stored in the data registry
*/
enum datavec_t {
  vm_vec, iion_vec
};

}  // namespace opencarp

#endif
