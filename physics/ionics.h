// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file ionics.h
* @brief Electrical ionics functions and LIMPET wrappers.
* @author Aurel Neic, Edward Vigmond
* @version 
* @date 2019-10-25
*/

#ifndef _IONICS_H
#define _IONICS_H

#include "physics_types.h"
#include "sim_utils.h"
#include "solver_utils.h"
#include "stimulate.h"
#include "IGBheader.h"

#include "sf_interface.h"
#include "MULTI_ION_IF.h"
#include "sv_init.h"

namespace opencarp {


/**
* @brief classify elements/points as belonging to a region
*
*  Element tags in the elem files are grouped to construct regions.
*  If points are to be assigned to a region, points on the boundary
*  between regions belong to several regions. In this case, the point
*  is assigned to the region with the highest number
*
* @param meshspec    Identifier for the mesh we work on.
* @param regspec     Vector or region spec structs
* @param regionIDs   Vector of element/nodal IDs (mapping into the region spec vec).
* @param mask_elem   Whether we mask elements or points.
* @param reglist     A string naming to region we work on for output.
*
 * @post  The array holding the element region identifiers is allocated
 *        and filled. If there is only 1 region, regionIDs is set to NULL
*/
void region_mask(mesh_t meshspec, SF::vector<RegionSpecs> & regspec,
                 SF::vector<int> & regionIDs, bool mask_elem, const char* reglist);

class Ionics : public Basic_physic
{
  public:
  limpet::MULTI_IF* miif = NULL;
  mesh_t    ion_domain;

  Ionics(mesh_t gid) : ion_domain(gid)
  {
    switch(ion_domain) {
      case intra_elec_msh:
        name = "Myocard Ionics"; break;
      default:
        name = "Ionics"; break;
    }
  }

  void initialize();
  void destroy();
  void compute_step();
  void output_step();
  ~Ionics() = default;

  /// figure out current value of a signal linked to a given timer
  double timer_val(const int timer_id);
  /// figure out units of a signal linked to a given timer
  std::string timer_unit(const int timer_id);

  private:
  /** set up MIIFs
   *
   * \param miif      MIIF to set up
   * \param nnodes    number of nodes
   * \param nreg      number of regions
   * \param impreg    region descriptions
   * \param mask      array of length \p nnodes indicating region for each node
   * \param start_fn  file name to read in state variables
   * \param numadjust adjust MIIF on nodal basis
   * \param adjust    adjustments
   * \param time_step time step
   * \param map       mapping extra- to intracellular grid
   * \param close     true to leave start_fn open after reading
   *
   * if \p start_fn==NULL, use already opened file
   *
   * \pre  \p miif low and high are set
   * \post mask is free'd
   */
  double setup_MIIF(int nnodes, int nreg, IMPregion* impreg, int* mask,
             const char *start_fn, int numadjust, IMPVariableAdjustment *adjust,
             double time_step, bool close);

};

/** simple routine to run ionic models for prepacing
 *
 * \parama pIF     ionic model
 * \param  impdata data for ionic model
 * \param  n       node to compute
 *
 * \note Only one node is run
 */
void compute_IIF(limpet::ION_IF* pIF, limpet::GlobalData_t** impdata, int n);

//! Main purpose is to store information that allows to assemble
//! a global vector based on state variables which are defined
//! in different regions of a domain.
struct sv_data {
  char*        name;        //!< Name of global composite sv vector
  char**       imps;        //!< Name of imp to which sv belongs
  char**       svNames;     //!< sv names of components forming global vector
  int*         svInds;      //!< store sv dump indices for each global vector
  int*         svSizes;     //!< sv size in bytes
  int*         svOff;       //!< sv size in bytes
  FILE_SPEC    outf;        //!< output file
  void**       getsv;       //!< functions to retrieve sv
  sf_petsc_vec ordered;     //!< vector in which to place ordered data
  char*        units;       //!< units of sv
  float        bogus;       //!< value indicating sv not in region
};

struct gvec_data {
  unsigned int           nRegs;        //!< number of imp regions
  bool                   inclPS;       //!< include PS if exists
  SF::vector<sv_data>    vecs;         //!< store sv dump indices for global vectors
  bool                   rm_dumps;     //!< remove intermediate state variable dump files
};

void init_sv_gvec(gvec_data& GVs, limpet::MULTI_IF* miif, sf_petsc_vec & tmpl,
                  igb_output_manager & output_manager);
void assemble_sv_gvec(gvec_data & gvecs, limpet::MULTI_IF *miif);
}
#endif
