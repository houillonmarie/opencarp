#!/bin/bash
#
# use this script if you do not use cmake. It is a wrapper for
# make_dynamic_model.py.
#
# We try to be smart but if that is not the case,
# just hard code your CARP_DIR and CC


#this function is needed since OSX does not implement "readlink -f"
function actualpath {
    echo $(python3 -c 'import os;print(os.path.realpath("'$1'"))') 
}

CUR_DIR=$(dirname $(actualpath ${BASH_SOURCE}))
CARP_DIR=$(actualpath ${CUR_DIR}/../../..)

INCS="-I${CARP_DIR}/physics/limpet/src \
-I${CARP_DIR}/numerics \
-I${CARP_DIR}/simulator \
-I${CARP_DIR}/fem \
-I${CARP_DIR}/fem/slimfem/src \
-I${CARP_DIR}/param/include \
-I${PETSC_DIR}/include \
-I${PETSC_DIR}/${PETSC_ARCH}/include"

while getopts "kh" option
do
        case "${option}"
        in
            k) keep="--keep";;
            h) echo "Usage: $0 [-k] base_IMP_name" 1>&2
               exit 1;;
            \?) exit 1;;
        esac
done
shift $((${OPTIND}-1))

#try to determine the compiler from environment
if [[ -e ${PETSC_DIR}/${PETSC_ARCH}/lib/petsc/conf/petscvariables ]]
then
    CC=$(grep '^CC ' ${PETSC_DIR}/${PETSC_ARCH}/lib/petsc/conf/petscvariables | awk '{print $3;}')
elif [[ -e ${PETSC_DIR}/lib/petsc/conf/petscvariables ]]
then
    CC=$(grep '^CC ' ${PETSC_DIR}/lib/petsc/conf/petscvariables | awk '{print $3;}')
elif [[ -e ${PETSC_DIR}/conf/variables ]]
then
    CC=$(grep '^CC ' ${PETSC_DIR}/conf/variables | awk '{print $3;}')
else #no petsc configuration found, try naive gcc
    CC=$(which gcc)
fi

if [[ -z ${CC} ]]
then
    echo Cannot find C compiler --- bailing
    exit 1
fi

if ${CC} --version 2>&1 >/dev/null; then
    compiler=$(${CC} --version 2>&1)
else
    compiler=$(${CC} -V 2>&1)
fi

#set compiler specific flags
if [[ $compiler =~ "GCC" ||  $compiler =~ "gcc" || $compiler =~ "clang" ]]; then
    cc="GCC"
    DYN_LIB_OPTS="-fPIC -shared -std=c++11 -lm"
    if [[ $(uname -s) = "Darwin" ]]
    then
        DYN_LIB_OPTS+=" -Wl,-undefined,dynamic_lookup"
    fi    
elif [[ $compiler =~ "ICC" ]]; then
    cc="ICC"
    DYN_LIB_OPTS="-fPIC -shared -std=c++11"
elif [[ $compiler =~ "pgc" ]]; then
    cc="PGI"
    DYN_LIB_OPTS="-fPIC -shared -std=c++11"
elif [[ $compiler =~ "Cray" ]]; then
    cc="CRAYCC"
    DYN_LIB_OPTS="-fPIC -shared -h c++11"
fi

if [ -f ${CARP_DIR}/my_switches.def ]; then
    #try to determine if we are debugging
    DEBUG=`sed 's/#.*//' ${CARP_DIR}/my_switches.def | grep -c 'DEBUG.*=.*[1-9]'`
    if [ $DEBUG -ne 0 ] ; then
        echo DEBUGGING
        DYN_LIB_OPTS="${DYN_LIB_OPTS} -g -O0 "
    fi

    #try to determine if we are using CVODE
    CVODE=$(sed 's/#.*//' ${CARP_DIR}/my_switches.def | grep -c 'CVODE\s*=\s*[1-9]')
    if [ $CVODE -ge 1 ] ; then
        echo "using CVODE"
        INCS+=" -DUSE_CVODE"
    fi
fi

DYN_PREFIX=''

# model name can be with or without .model extension. here we make the input consisitent
MODEL_NAME=${1}
if [[ ! $MODEL_NAME == *".model"* ]]; then
  echo "appending .model to input model name.."
  MODEL_NAME="${1}.model"
  echo "using model name: "$MODEL_NAME
fi

# first call limpet_fe.py
echo ${CARP_DIR}/physics/limpet/src/python/limpet_fe.py ${MODEL_NAME} ${CARP_DIR}/physics/limpet/models/imp_list.txt "$(dirname ${MODEL_NAME})"
${CARP_DIR}/physics/limpet/src/python/limpet_fe.py ${MODEL_NAME} ${CARP_DIR}/physics/limpet/models/imp_list.txt "$(dirname ${MODEL_NAME})"

# then we call make_dynamic_model.py
echo ${CARP_DIR}/physics/limpet/src/python/make_dynamic_model.py --compiler=${CC} --cflags="${DYN_LIB_OPTS} ${INCS}" --dynamic_prefix=${DYN_PREFIX} ${1} ${keep}
${CARP_DIR}/physics/limpet/src/python/make_dynamic_model.py --compiler=${CC} --cflags="${DYN_LIB_OPTS} ${INCS}" --dynamic_prefix=${DYN_PREFIX} $1 ${keep}

