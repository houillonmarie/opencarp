// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef ROSENBROCK_H
#define ROSENBROCK_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

namespace limpet {

#define RS_ORDER 4
#define RS_MAX_N 36
#define RM(rowlength,row,col) (((rowlength)*(row))+(col))

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void rbStepX ( float *X, void (*calcDX)(float*, float*, void*),
                void (*calcJ)(float**, float*, void*, int ),
                void *params, float h, int N );

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

#endif
