#!/usr/bin/env python3

# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

##
# \todo Please add documentation to this file
#

from copy import *
from basic import *
from functools import cmp_to_key
from unit import si
from sys import version_info
if version_info >= (2,4,0):
    Set = set
else:
    from sets import Set
import parser

class Blank:
    pass

def AST(*args):
    return parser.AST(*args)

def order(set):
    var_list = [var for var in set]
    var_list.sort(key=lambda a: a.name)
    return var_list

class Var:
    def __init__(self, name):
        self.name = name
        self.attr = {}

    def is_a(self, type):
        return type in self.attr
    def make_a(self, type, info):
        self.attr[type] = info
    def info(self, type):
        return self.attr[type]

    def __str__(self):
        return self.name

    def __repr__(self):
        return 'Var("'+self.name+'")'

class Equation:
    def __init__(self, lhs, type, rhs):
        self.lhs = lhs
        self.type = type
        self.rhs= rhs
        self.depend = Set()

        for node in rhs.depth_walk():
            if node.type == 'var':
                self.depend.add(node[0])

    def formatRHS(self, rhs_format=lambda v: v.name):
        return self.rhs.toString(rhs_format)

    def __repr__(self):
        return ('Equation({}, "%s", "%s", "%s")' 
                % (self.lhs.name,
                   self.type,
                   self.formatRHS()
                   ))

    def countFunctions(self):
        return len([n for n in self.rhs.depth_walk() if n.type == 'func'])

class SymbolTable(MapClass):
    def __init__(self):
        MapClass.__init__(self)
        
    def assureSym(self, name):
        if name not in self:
            self[name] = Var(name)

    def newVar(self, name):
        self.assureSym(name)
        return self[name]

    def getVars(self, type=''):
        if type:
            result = Set()
            for var in list(self.values()):
                if var.is_a(type):
                    result.add(var)
            return result
        else:
            return Set(list(self.values()))


def assure_no_duplicate_equations(eqn_list):
    vars = Set()
    duplicate_eqns = {}
    for eqn in eqn_list:
        if eqn.lhs in vars:
            duplicate_eqns[eqn.lhs] = Set()
        else:
            vars.add(eqn.lhs)
    if duplicate_eqns:
        print("Error!  Asked to create a depend list, but multiple equations had the same lhs.")
        for eqn in eqn_list:
            if eqn.lhs in duplicate_eqns:
                duplicate_eqns[eqn.lhs].add(eqn)
        for lhs in list(duplicate_eqns.keys()):
            for eqn in duplicate_eqns[lhs]:
                print(eqn)
        raise DependError(duplicate_eqns)     

## Used by the Depend class to throw exceptions.
#
class DependError(Exception):
    pass

class EqnMap(MapClass):
    def __init__(self, eqn_list=[]):
        MapClass.__init__(self)
        assure_no_duplicate_equations(eqn_list)
        for eqn in eqn_list:
            self.add(eqn)
    
    def add(self, eqn):
        self[eqn.lhs] = eqn

    def extend(self, other_eqn_map):
        assure_no_duplicate_equations(list(self.keys()) + list(other_eqn_map.keys()))
        override(other_eqn_map)

    def override(self, other_eqn_map):
        for var in list(other_eqn_map.keys()):
            self[var] = other_eqn_map[var]

    def supplement(self, other_eqn_map):
        for var in list(other_eqn_map.keys()):
            if var not in self:
                self[var] = other_eqn_map[var]

    def used(self):
        all_used = Set()
        for var in list(self.keys()):
            all_used |= self[var].depend
        return all_used

    def dependencies_of(self, target_set, valid=Set()):
        unprocessed = copy(target_set)
        processed = Set()
        result = Set()

        while unprocessed:
            new_symbols = Set()
            for sym in unprocessed:
                if sym in self and sym not in processed:
                    new_symbols |= self[sym].depend
            processed |= unprocessed
            processed |= valid
            result |= unprocessed
            unprocessed = new_symbols - processed
        return result

    def reachable_from(self, other_valid, exclude=Set()):
        valid = copy(other_valid)
        reachable = Set()

        go_on = 1
        while go_on:
            new_valid_symbols = Set()
            for sym in list(self.keys()):
                for d in self[sym].depend:
                    if d not in valid:
                        break
                else:
                    if sym not in reachable and sym not in exclude:
                        new_valid_symbols.add(sym)
            reachable |= new_valid_symbols
            valid |= new_valid_symbols
            go_on = new_valid_symbols
        return reachable

    def influences_of(self, base, valid=Set()):
        unchecked = copy(base)
        depends_on = Set()

        while unchecked:
            new_symbols = Set()
            for sym in list(self.keys()):
                for dsym in self[sym].depend:
                    if dsym in unchecked:
                        new_symbols.add(sym)
                        break
            depends_on |= unchecked
            unchecked = new_symbols - depends_on - valid
        return depends_on
                        
    def extract_external(self, covering, targets):
        externals = Set()
        unprocessed = copy(targets)
        processed = Set()

        while unprocessed:
            new_symbols = Set()
            for sym in unprocessed:
                if sym in covering:
                    externals.add(sym)
                elif sym in self:
                    new_symbols |= self[sym].depend
            processed |= unprocessed
            unprocessed = new_symbols - processed
        return externals

    def getList(self, targets, orig_valid):
        result=[]

        valid = copy(orig_valid)
        symbols = (self.dependencies_of(targets, valid) - valid) | targets

        while symbols:
            discard_list=[]
            for sym in symbols:
                for d in self[sym].depend:
                    if d not in valid:
                        break
                else:
                    discard_list.append(sym)
            if not discard_list:
                #no longer improving, need to exit
                print("================= Resolved Variables")
                for sym in valid:
                    print(sym)
                print("================= Unresolved equations")
                for sym in symbols:
                    print(self[sym])
                print("================= Cycles")
                
                cycles = self.cycleEnumerate(symbols, valid)
                if cycles:
                    for cycle in cycles:
                        print(join(" <- ", [str(c) for c in cycle]))
                    raise DependError("a circular dependancy exists in the model.")
                else:
                    raise DependError("error in code to getList.")
            else:
                discard_list.sort(key=lambda a: a.name)
                for sym in discard_list:
                    symbols.discard(sym)
                    result.append(sym)
                    valid.add(sym)

        return result

    def cycleEnumerate(self, symbols, valid):
        globals = Valid()
        globals.point_stack = []
        globals.mark_set = Set()
        globals.mark_stack = []
        globals.valid = valid
        globals.order = {}
        globals.cycles = []
        globals.debug = 0
        index = 0
        for sym in symbols:
            globals.order[sym] = index
            index += 1
        
        for sym in symbols:
            globals.start = sym
            globals.mark_set = Set()
            globals.mark_stack = []
            if globals.debug:
                print("Starting Node %s" % sym)
            self.cycleEnumerateBacktrack(sym, globals)
        
        return globals.cycles

    def cycleEnumerateBacktrack(self, node, globals):
        f = 0
        if globals.debug:
            print("Level %s: %s = %s" % (len(globals.point_stack), node, globals.order[node]))
        globals.point_stack.append(node)
        globals.mark_set.add(node)
        globals.mark_stack.append(node)
        if globals.debug:
            print("Point stack: %s" % globals.point_stack)
            print("Mark set: %s" % globals.mark_set)
            print("Mark_stack: %s" % globals.mark_stack)
            print(self[node].depend)
        for dsym in self[node].depend:
            if globals.debug:
                print("%s -> %s" % (node, dsym))
            if dsym in globals.valid:
                continue
            if globals.order[dsym] < globals.order[globals.start]:
                continue
            elif dsym == globals.start:
                cycle = globals.point_stack+[dsym]
                globals.cycles.append(cycle)
                f = 1
            elif dsym not in globals.mark_set:
                g = self.cycleEnumerateBacktrack(dsym, globals)
                f = f or g
        if f:
            while globals.mark_stack[-1] != node:
                globals.mark_set.remove(globals.mark_stack.pop())
            globals.mark_set.remove(globals.mark_stack.pop())
        globals.point_stack.pop()
        return f

class IonicModel:
    def __init__(self, text):
        self.table = SymbolTable()
        self.parser = parser.Eqn_Parser(self.table)
        self.parse(text)
        self.cleanup()

    def init_system(self):
        '''Returns a copy of the ionic model object with the initialization for all variables.'''
        other = copy(self)
        other.main = copy(self.main)
        other.main._map = copy(self.main._map)
        for var in other.getVars('init'):
            other.main.add(Equation(var, "=", self.expression(var.info('init').name)))
        return other

    def parse(self, text):
        self.parser.parse(text)
        self.main = self.parser.main.eqn_map
        self.lookup = self.parser.lookup

    def expression(self, expression):
        p = parser.Expr_Parser(self.table)
        return p.parse(expression)

    def findVars(self):
        self.findGates()
        self.findDiffVars()
        self.findInitVars()

    def findGates(self):
        gate_regex = { 'alpha': re.compile(r'(?:alpha_|a_)(.*)'),
                       'beta':  re.compile(r'(?:beta_|b_)(.*)'),
                       'tau':   re.compile(r'(?:t|tau)_(.*)'),
                       'inf':   re.compile(r'(.*)_(?:inf|infinity|st)$'),
                       }
        gates = {}
        for type in gate_regex:
            for sym in list(self.table.keys()):
                m = gate_regex[type].match(sym)
                if m:
                    gate_name = m.group(1)
                    gate_depend = self.table[sym]
                    if gate_name not in gates:
                        gates[gate_name] = Valid()
                    setattr(gates[gate_name], type, gate_depend)

        for name in list(gates.keys()):
            if name in self.table:
                if gates[name].tau and gates[name].inf and gates[name].alpha and gates[name].beta:
                    print("Warning, you should only use alpha/beta OR tau/infinity while formulating gates!\n")
                    print("Using the tau infinity values.\n")
                    print("Error with " + name + "\n")
                    gates[name].alpha = Void()
                    gates[name].beta = Void()
                if ((gates[name].tau and gates[name].inf) or
                    (gates[name].alpha and gates[name].beta)):
                    self.table.newVar(name).make_a('gate', gates[name])

    def findDiffVars(self):
        regex = re.compile(r'(?:diff_(\w+)|d_(\w+)_dt)$')
        for var in list(self.table.values()):
            m=regex.match(var.name)
            if m:
                if m.group(1):
                    name = m.group(1)
                else:
                    name = m.group(2)
                self.table.newVar(name).make_a('diff', var)

    def findInitVars(self):
        regex = re.compile(r'(?:(\w+)_init)|(?:init_(\w+))')
        for var in list(self.table.values()):
            m=regex.match(var.name)
            if m:
                if m.group(1):
                    name = m.group(1)
                else:
                    name = m.group(2)
                self.table.newVar(name).make_a('init', var)

    def createGateInit(self):
        for var in list(self.table.values()):
            if var.is_a('gate'):
                gate = var.info('gate')
                if gate.inf:
                    init = self.expression(gate.inf.name)
                else:
                    init = self.expression('%s/(%s+%s)' % (gate.alpha, gate.alpha, gate.beta))
                newvar = self.table.newVar(var.name+"_init")
                if newvar not in self.main:
                    self.main.add(Equation(newvar, "=", init))
                    var.make_a('init', newvar)

    def createGateDiff(self):
        for var in list(self.table.values()):
            if var.is_a('gate'):
                gate = var.info('gate')
                if gate.alpha and gate.beta:
                    diff = "%s - (%s+%s)*%s" % (gate.alpha,
                                                gate.alpha,
                                                gate.beta,
                                                var)
                elif gate.inf and gate.tau:
                    diff = "(%s - %s)/%s" % (gate.inf,
                                             var,
                                             gate.tau)
                diff_name = "diff_"+var.name
                newvar = self.table.newVar(diff_name)
                self.main.add(Equation(newvar,
                                       "=",
                                       self.expression(diff)))
                var.make_a('diff', newvar)

    def findRushLarsenCanidates(self):
        for var in self.getVars('diff'):
            diff_var = var.info('diff')
            linear_coeff = self.partial(diff_var, var)
            if var not in self.main.dependencies_of(Set([linear_coeff])) and not var.is_a('method'):
                var.make_a('method', 'rush_larsen')

    def setDefaultInt(self, default):
        for var in self.getVars('diff'):
            if not var.is_a('method'):
                var.make_a('method', default)

    def markRushLarsenGates(self):
        for var in self.getVars('gate'):
            if not var.is_a('method'):
                var.make_a('method', 'rush_larsen')

    def createGateRushLarsenInt(self):
        eqns = self.main
        for var in self.getVars('gate'):
            if var.is_a('method') and var.info('method') == 'rush_larsen':
                a = self.table.newVar(var.name+"_rush_larsen_A")
                b = self.table.newVar(var.name+"_rush_larsen_B")
                if var.info('gate').alpha and var.info('gate').beta:
                    eqns.add(Equation(a, "=", 
                                      self.expression("-%(alpha)s/(%(alpha)s+%(beta)s)*expm1(-dt*(%(alpha)s+%(beta)s))" 
                                                      % {
                                    "alpha": var.info('gate').alpha,
                                    "beta" : var.info('gate').beta
                                    })))
                    eqns.add(Equation(b, "=",
                                      self.expression("exp(-dt*(%(alpha)s+%(beta)s))" 
                                                      % {
                                    "alpha": var.info('gate').alpha,
                                    "beta" : var.info('gate').beta
                                    })))
                else:
                    c = self.table.newVar(var.name+"_rush_larsen_C")
                    eqns.add(Equation(c, "=", 
                                      self.expression("expm1(-dt/%(tau)s)" 
                                                      % {
                                        "inf": var.info('gate').inf,
                                        "tau": var.info('gate').tau
                                        })))
                    eqns.add(Equation(a, "=", 
                                      self.expression("-%(inf)s*%(c)s" 
                                                      % {
                                        "inf": var.info('gate').inf,
                                        "c": c,
                                        })))
                    
                    eqns.add(Equation(b, "=",
                                      self.expression("exp(-dt/%s)" 
                                                      % var.info('gate').tau)))
                var.make_a('rush_larsen', (a, b))

    def createRushLarsenInt(self):
        var_list = Set([var for var in list(self.table.values()) if var.is_a('diff') and var.is_a('method') and var.info('method') == 'rush_larsen' and not var.is_a('rush_larsen')])
        for var in var_list:
            diff_var = var.info('diff')
            linear_coeff = self.partial(diff_var, var)
            a = self.table.newVar(var.name+"_rush_larsen_A")
            b = self.table.newVar(var.name+"_rush_larsen_B")
            
            if var not in self.main.dependencies_of(Set([linear_coeff])):
                #special case: linear like a gate equation.  Redo the exp_int so that both
                # the RLA and RLB terms can both go in lookup tables.
                self.main.add(Equation(a, '=',
                                       self.expression("%(constant)s/%(partial)s*expm1(dt*%(partial)s)"
                                                       % {
                                "partial" : linear_coeff,
                                "constant" : self.var_to_zero(diff_var, var)
                                })))
                self.main.add(Equation(b, '=',
                                       self.expression("exp(dt*%(partial)s)"
                                                       % {
                                "partial" : linear_coeff,
                                })))
            else:
                #Not a special case, so do the best we can.
                self.main.add(Equation(a, '=',
                                       self.expression("expm1(%(partial)s*dt)*(%(diff)s/%(partial)s)"
                                                       % {
                                "partial" : linear_coeff,
                                "var"     : var,
                                "diff"    : diff_var
                                })))
                self.main.add(Equation(b, '=',
                                       self.expression("1")))
            var.make_a('rush_larsen', (a, b))

    def createSundnesInt(self):
        var_list = Set([var for var in list(self.table.values()) if var.is_a('diff') and var.is_a('method') and var.info('method') == 'sundnes' and not var.is_a('sundnes')])
        for var in var_list:
            diff_var = var.info('diff')
            linear_coeff = self.partial(diff_var, var)
            half = self.table.newVar(var.name+"_sundnes_half")
            update_eqn = """
            %(var)s + %(diff)s*
               ((fabs(%(partial)s) < 1e-8) 
                ? %(dt)s
                : expm1(%(partial)s*%(dt)s)/%(partial)s)
            """
            self.main.add(Equation(half, '=',
                                   self.expression(update_eqn
                                                   % {
                            "partial" : linear_coeff,
                            "var"     : var,
                            "diff"    : diff_var,
                            "dt"      : "dt/2",
                            })))
            full = self.table.newVar(var.name+"_sundnes_full")
            self.main.add(Equation(full, '=',
                                   self.expression(update_eqn
                                                   % {
                            "partial" : linear_coeff,
                            "var"     : var,
                            "diff"    : diff_var,
                            "dt"      : "dt",
                            })))
            sundnes = Blank()
            sundnes.half = half
            sundnes.full = full
            var.make_a('sundnes', sundnes)

    def createImplicitInt(self, var_list):
        for var in var_list:
            implicit = Blank()
            implicit.partial = Set()
            implicit.diff = var.info('diff')
            for wrt_var in var_list:
                implicit.partial.add(self.partial(implicit.diff, wrt_var))
            var.make_a('implicit_method', implicit)


    def createRosenbrockInt(self):
        var_list = Set([var for var in self.getVars('diff') & self.getVars('method') if var.info('method') == 'rosenbrock'])
        for var in var_list:
            var.make_a('rosenbrock', 1)
        self.createImplicitInt(var_list)

    def createMarkovBackwardEulerInt(self):
        var_list = Set([var for var in self.getVars('diff') & self.getVars('method') if var.info('method') == 'markov_be'])
        self.createImplicitInt(var_list)
        for var in var_list:
            gauss_seidel_list = []
            for y_var in order(var_list):
                if y_var == var:
                    continue
                gauss_seidel_list.append("%s*%s" % (self.partial(var.info('diff'), y_var), y_var))
            equation_text = "(%s+dt*(%s))/(1-dt*%s)" % (
                var.info('diff'),
                "+".join(gauss_seidel_list),
                self.partial(var.info('diff'),var),
                )
            solve_var = self.table.newVar(var.name+"_markov_solve")
            self.main.add(Equation(solve_var, '=', self.expression(equation_text)))
            var.make_a('markov_be', solve_var)
            
    def cleanupOtherMethods(self, methods):
        for method in methods:
            for var in self.getVars("method"):
                if var.info("method") == method:
                    var.make_a(method, 1)

    def cleanup(self):

        self.lookup = order(self.lookup)

        self.table.newVar("dt").make_a('time', 1)
        self.table.newVar("t").make_a('time', 1)

        self.findVars()
        self.createGateInit()
        self.createGateDiff()
        #self.findRushLarsenCanidates()
        self.markRushLarsenGates()
        self.setDefaultInt('fe')
        self.createGateRushLarsenInt()
        self.createRushLarsenInt()
        self.createSundnesInt()
        self.createRosenbrockInt()
        self.createMarkovBackwardEulerInt()
                          
        self.cleanupOtherMethods(['fe','rk2','rk4','cvode'])        

        self.assigned = Set(list(self.main.keys()))
        self.used = self.main.used()
        self.statevars = self.getVars('diff') | self.getVars('store')
        for var in self.statevars:
            var.make_a('state', 1)
            var.make_a('nodal', 1)


        test = self.getVars('method') - self.getVars('diff')
        if test:
            print("Error, the method qualifier only applies to differential variables.")
            print("The following variables need to have their methods removed: ")
            print(test)
            assert(0)
        methods = ['fe','rk2','rk4','rush_larsen','sundnes','rosenbrock','cvode','markov_be']
        test = True
        for var in self.getVars('method'):
            if var.info('method') not in methods:
                print("Error, didn't recognize method %s on var %s" % (var.info('method'), var))
                test = False
        assert(test)

        test = self.getVars('diff') & self.assigned
        if test:
            print("Error, don't assign to variables marked as differential variables")
            print(test)
            assert(0)
        test = self.getVars('diff') - self.getVars('init')
        if test:
            print("Error, the following differential variables are missing initializations.")
            print(test)
            assert(0)
        test = self.getVars('store') - self.getVars('init')
        if test:
            print("Error, the following store variables are missing initializations.")
            print(test)
            assert(0)
        test = self.getVars('store') - self.assigned
        if test:
            print("Error, the following variables are marked as store, but they are not assigned to.")
            print(test)
            assert(0)

        #in the case of reading models from cellml, it helps to move the units from the _init
        #over to the actual variables.
        for var in self.getVars('init'):
            if not var.is_a('unit') and var.info('init').is_a('unit'):
                var.make_a('unit', var.info('init').info('unit'))
        
        # try and get the units of time.
        time_guesses = dict([(var,var.info('unit')/var.info('diff').info('unit'))
                        for var in self.getVars('diff') & self.getVars('unit')
                        if var.info('diff').is_a('unit')])
        t = self.table.newVar('t')
        if t.is_a('unit'):
            time_guesses[t] = t.info('unit')
        dt = self.table.newVar('dt')
        if dt.is_a('unit'):
            time_guesses[dt] = dt.info('unit')

        if not time_guesses:
            self.time_unit = si.ms
        else:
            first = list(time_guesses.keys())[0]
            test = Set()
            for key in list(time_guesses.keys()):
                if time_guesses[first] != time_guesses[key]:
                    test.add(first)
                    test.add(key)
            if test:
                print("Error, inconsistent units with the following variables:")
                print(test)
                assert(0)
            self.time_unit = time_guesses[first]

    def getVars(self, type=''):
        return self.table.getVars(type)

    def partial(self, diff_var, wrt_var):
        self._rec_partial_var(diff_var, wrt_var)
        return self.partial_var(diff_var, wrt_var)

    def partial_var(self, diff_var, wrt_var):
        partial_name = 'partial_'+diff_var.name+"_del_"+wrt_var.name
        return self.table.newVar(partial_name)        

    def _rec_partial_var(self, diff_var, wrt_var):
        if diff_var not in self.main:
            return AST('const', '0')

        partial_var = self.partial_var(diff_var, wrt_var)
        if partial_var not in self.main:
            #Create the differential variable
            self.main.add(Equation(partial_var,
                                   '=',
                                   self._rec_partial_ast(self.main[diff_var].rhs, wrt_var)))
        return self.main[partial_var].rhs

    def _rec_partial_ast(self, node, wrt_var):
        t = node.type

        if t in ('-.', 'paren'):
            ret = AST(t, self._rec_partial_ast(node[0], wrt_var))
        elif t in ('.+.', '.-.'):
            ret = AST(t, self._rec_partial_ast(node[0], wrt_var), self._rec_partial_ast(node[1], wrt_var))
        elif t == 'ifelse':
            ret = AST(t, node[0], self._rec_partial_ast(node[1], wrt_var), self._rec_partial_ast(node[2], wrt_var))
        elif t == '.*.':
            ret = AST('.+.', 
                      AST('.*.', self._rec_partial_ast(node[0], wrt_var), node[1]),
                      AST('.*.', node[0], self._rec_partial_ast(node[1], wrt_var)))
        elif t == './.':
            ret = AST('./.',
                      AST('.-.',
                          AST('.*.', node[1], self._rec_partial_ast(node[0], wrt_var)),
                          AST('.*.', node[0], self._rec_partial_ast(node[1], wrt_var))),
                      AST('.*.', node[1], node[1]))
        elif t == 'var':
            if node[0] == wrt_var:
                ret = AST('const', '1')
            else:
                ret = self._rec_partial_var(node[0], wrt_var)
        elif t == 'var' or t == 'const':
            ret = AST('const', '0')
        elif t == 'func':
            func = node[0]
            v1 = node[1][0]
            partial1 = self._rec_partial_ast(v1, wrt_var)
            if len(node[1]) >= 2:
                v2 = node[1][1]
                partial2 = self._rec_partial_ast(v2, wrt_var)
            if func == 'expm1' or func == 'exp':
                ret = AST('.*.',
                          AST('func', 'exp', [v1]),
                          partial1)
            elif func == 'sqrt':
                ret = AST('./.',
                          AST('.*.',
                              AST('const', '0.5'),
                              partial1),
                          node)
            elif func == 'log':
                ret = AST('./.',
                          partial1,
                          v1)
            elif func == 'log10':
                ret = AST('./.',
                          partial1,
                          AST('.*.',
                              v1,
                              AST('func', 'log', [AST('const','10')])))
            elif func == 'pow':
                if v2.isZero():
                    ret = AST('const', '0')
                if partial2.isZero():
                    if v2.type == 'const':
                        if float(v2[0])==1:
                            ret = partial1
                        else:
                            ret = AST('.*.',
                                      v2,
                                      AST('.*.',
                                          partial1,
                                          AST('func', 'pow', [v1, AST('const', str(float(v2[0])-1))])))
                    else:
                        ret = AST('.*.',
                                  v2,
                                  AST('.*.',
                                      partial1,
                                      AST('func', 'pow', [v1,
                                                          AST('.-.', v2,
                                                              AST('const', '1'))])))
                        
                else:
                    # most general version.
                    ret = AST('.*.',
                              node,
                              AST('.+.',
                                  AST('./.',
                                      AST('.*.', v2, partial1),
                                      v1),
                                  AST('.*.',
                                      AST('func', 'log', [v1]),
                                      partial2)))
            elif func == "acos":
                ret = AST('./.',
                          AST('-.', partial1),
                          AST('func', 'sqrt',
                              [AST('.-.',
                                  AST('const', '1'),
                                  AST('.*.', v1, v1))]))
            elif func == "atan2":
                ret = AST('./.',
                          AST('.-.',
                              AST('.*.', partial1, v2),
                              AST('.*.', v1, partial2)),
                          AST('.+.',
                              AST('.*.', v1, v1),
                              AST('.*.', v2, v2)))
            elif func == "fabs":
                ret = AST('.*.', partial1, AST('func', 'sign', [v1]))
            elif func == "sign":
                ret = AST('const', '0')
            elif func == "square":
                ret = AST('.*.',
                        AST('.*.', 
                            AST( 'const', '2' ),
                            v1 ),
                        partial1 )
            elif func == "cube":
                ret = AST( '.*.',
                        AST('.*.',
                            AST('const','3'),
                            AST('.*.', v1, v1 )),
                        partial1 )
            else:
                assert(0)
        return ret

    def var_to_zero(self, var, zero_var):
        self._rec_var_to_zero(var, zero_var)
        return self.reserve_var_to_zero(var, zero_var)
        
    def reserve_var_to_zero(self, var, zero_var):
        reserve_name = "set_"+zero_var.name+"_tozero_in_"+var.name
        return self.table.newVar(reserve_name)

    def _rec_var_to_zero(self, var, zero_var):
        if var not in self.main:
            return AST('var', var)
        
        reserved_var = self.reserve_var_to_zero(var, zero_var)
        if reserved_var not in self.main:
            #Create the differential variable
            self.main.add(Equation(reserved_var,
                                   '=',
                                   self._rec_var_to_zero_ast(self.main[var].rhs, zero_var)))
        return self.main[reserved_var].rhs
    
    def _rec_var_to_zero_ast(self, node, zero_var):
        if node.type == 'var':
            if node[0] == zero_var:
                ret = AST('const', '0')
            else:
                ret = self._rec_var_to_zero(node[0], zero_var)
        else:
            #do nothing, recurse into children.
            ret = node.recurse(lambda n : self._rec_var_to_zero_ast(n, zero_var))

        return ret

if (__name__=='__main__'):
    import sys
    import re
    import im
    eqn_test = '''
// This is a comment.
// a = b;
/* a = b
//*/

a_m= (0.32*(V+47.13))/ // more comments
 (1.-exp(-0.1*(V+47.13)));
b_m= 0.08*exp(-V/11.);

init_Cansr = 0;
diff_Cansr = -50*a_m+m;
'''
    imm = IonicModel(eqn_test)
    for name in list(imm.table.keys()):
        print("table: "+name)
    for eqn in list(imm.main.values()):
        print(eqn)
    print("===")
    
    eqn_test = '''
m=m;
'''
    imm = IonicModel(eqn_test)
    try:
        imm.main.getList(Set([imm.table['m']]), Set())
    except im.DependError:
        pass
    
    eqn_test = '''
a = b;
b = c;
c = a;
'''
    imm = IonicModel(eqn_test)
    try:
        imm.main.getList(Set([imm.table['a']]), Set())
    except im.DependError:
        pass 
    
    eqn_test = '''
a = b;
b = c+R;
c = d;
R = d;
d = b+f;
f = 50;
'''
    imm = IonicModel(eqn_test)
    try:
        imm.main.getList(Set([imm.table['a']]), Set())
    except im.DependError:
        pass 
    
    sample_model = '''

V_init = -88.5;

//V is lookup: lb=-800 ub=800 step=0.01;
V; .external(Vm); .nodal();

h = g + 30;
g = 30;
.param();

f = 40;
diff_V = 50 + blah;
blah = sqrt(m);
temp = (V+f);

a_m= (g*temp)/(1.-exp(-0.1*(V+47.13)));
b_m= 0.08*exp(-V/11.) + a_m;
m;
.lookup(0,1,0.001);

q = V+m;
.lookup(0,1,0.1);

a_x = q;
b_x = q;

'''
    im = IonicModel(sample_model)
    partial = im.partial(im.table['a_m'], im.table['V'])
    print(im.main[partial])
    print(im.main[im.table['a_m']])

    sample_model = '''
z = 3.7;
y = x*x;

A01 = -(5*x);
A02 = 5*x+y;
A03 = (x > 0) ? 5*x : 3*y;
A04 = x*y;
A05 = y/x;
A06 = y;
A07 = x;
A08 = z;
A09 = exp(5*x);
A10 = expm1(5*x);
A11 = sqrt(5*x);
A12 = log(5*x);
A13 = pow(y,0);
A14 = pow(y,1);
A15 = pow(y,5);
A16 = pow(y,z);
A17 = pow(y,x);
A18 = acos(y);
A19 = atan2(y,x);
'''
    im = IonicModel(sample_model)
    partial = im.partial(im.table['A01'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A02'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A03'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A04'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A05'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A06'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A07'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A08'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A09'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A10'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A11'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A12'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A13'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A14'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A15'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A16'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A17'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A18'], im.table['x'])
    print(im.main[partial])
    partial = im.partial(im.table['A19'], im.table['x'])
    print(im.main[partial])
