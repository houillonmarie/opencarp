# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'model_main.ui'
#
# Created: Thu Apr  7 13:19:13 2011
#      by: PyQt4 UI code generator 4.7.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_ModelMain(object):
    def setupUi(self, ModelMain):
        ModelMain.setObjectName("ModelMain")
        ModelMain.resize(800, 600)
        self.centralWidget = QtGui.QWidget(ModelMain)
        self.centralWidget.setObjectName("centralWidget")
        ModelMain.setCentralWidget(self.centralWidget)
        self.menubar = QtGui.QMenuBar(ModelMain)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 26))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        ModelMain.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(ModelMain)
        self.statusbar.setObjectName("statusbar")
        ModelMain.setStatusBar(self.statusbar)
        self.toolBar = QtGui.QToolBar(ModelMain)
        self.toolBar.setMovable(False)
        self.toolBar.setAllowedAreas(QtCore.Qt.TopToolBarArea)
        self.toolBar.setObjectName("toolBar")
        ModelMain.addToolBar(QtCore.Qt.ToolBarArea(QtCore.Qt.TopToolBarArea), self.toolBar)
        ModelMain.insertToolBarBreak(self.toolBar)
        self.actionRead_Text_File = QtGui.QAction(ModelMain)
        self.actionRead_Text_File.setObjectName("actionRead_Text_File")
        self.actionRead_HDF5_File = QtGui.QAction(ModelMain)
        self.actionRead_HDF5_File.setObjectName("actionRead_HDF5_File")
        self.actionSolve_System = QtGui.QAction(ModelMain)
        self.actionSolve_System.setObjectName("actionSolve_System")
        self.actionReplot = QtGui.QAction(ModelMain)
        self.actionReplot.setObjectName("actionReplot")
        self.actionUse_Model_File = QtGui.QAction(ModelMain)
        self.actionUse_Model_File.setObjectName("actionUse_Model_File")
        self.menuFile.addAction(self.actionRead_Text_File)
        self.menuFile.addAction(self.actionRead_HDF5_File)
        self.menuFile.addAction(self.actionUse_Model_File)
        self.menubar.addAction(self.menuFile.menuAction())
        self.toolBar.addAction(self.actionSolve_System)
        self.toolBar.addAction(self.actionReplot)

        self.retranslateUi(ModelMain)
        QtCore.QMetaObject.connectSlotsByName(ModelMain)

    def retranslateUi(self, ModelMain):
        ModelMain.setWindowTitle(QtGui.QApplication.translate("ModelMain", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.menuFile.setTitle(QtGui.QApplication.translate("ModelMain", "File", None, QtGui.QApplication.UnicodeUTF8))
        self.toolBar.setWindowTitle(QtGui.QApplication.translate("ModelMain", "toolBar", None, QtGui.QApplication.UnicodeUTF8))
        self.actionRead_Text_File.setText(QtGui.QApplication.translate("ModelMain", "Read Text File...", None, QtGui.QApplication.UnicodeUTF8))
        self.actionRead_HDF5_File.setText(QtGui.QApplication.translate("ModelMain", "Read HDF5 File...", None, QtGui.QApplication.UnicodeUTF8))
        self.actionSolve_System.setText(QtGui.QApplication.translate("ModelMain", "Solve System", None, QtGui.QApplication.UnicodeUTF8))
        self.actionReplot.setText(QtGui.QApplication.translate("ModelMain", "Replot", None, QtGui.QApplication.UnicodeUTF8))
        self.actionUse_Model_File.setText(QtGui.QApplication.translate("ModelMain", "Use Model File...", None, QtGui.QApplication.UnicodeUTF8))

