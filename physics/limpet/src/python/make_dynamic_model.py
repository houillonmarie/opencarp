#!/usr/bin/env python3

# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

import optparse
from limpetcommon import *

class DictOptionParser(optparse.OptionParser):
    def parse_args(self, *args, **keywords):
        (options, args) = optparse.OptionParser.parse_args(self, *args, **keywords)
        return (vars(options), args)
class _blank:
    pass

if __name__=="__main__":
    import sys
    import os
    import os.path
    import tempfile

    print("Dynamic model generator")

    program_dir = os.path.abspath(os.path.dirname(sys.argv[0]))

    parser = DictOptionParser(usage="""Usage: %prog [options] modelname.cc
Produces a modelname.so library that CARP can load dynamically at run time.
""")
    parser.add_option("", "--compiler",
                      dest="compiler",
                      help="Set the compiler used to compile the external LIMPET module",
                      default="@CMAKE_C_COMPILER@",
                      )
    parser.add_option("", "--cflags",
                      dest="cflags",
                      help="Set the C_FLAGS used to compile the external LIMPET module",
                      default="@CMAKE_SHARED_LIBRARY_C_FLAGS@ @CMAKE_C_FLAGS@ @CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS@ -I%(prog)s/../include -I%(prog)s/../include/carp" % {'prog': program_dir},
                      )
    parser.add_option("", "--dynamic_prefix",
                      dest="prefix",
                      help="Set the prefix rpath to append when compiling a dynamic model.",
                      default="@CMAKE_SHARED_LIBRARY_SONAME_C_FLAG@",
                      )
    parser.add_option("-k", "--keep_tmp_file",
                      dest="del_tmp_file",
                      help="Save temporary c file for debugging.",
                      action="store_false",
                      default="True",
                      )
    (options, args) = parser.parse_args()
    if len(args)<1:
        print("Need to supply an source code file")
        print(parser.print_help())
        sys.exit(1)
    elif len(args)>1:
        print("Too many non-flag arguments")
        print(parser.print_help())
    else:
        modelfile = args[0]
        (directory, modelname) = os.path.split(modelfile)
        modelname = re.sub(r'\.[^.]*$','',modelname)
        directory = os.path.abspath(directory)
        modelbase = os.path.join(directory,modelname)

    #f = tempfile.NamedTemporaryFile(suffix=".cc",delete=options["del_tmp_file"])
    full_model_filename = os.path.join(directory, modelname) + '_dyn.cc'
    print('Generating: ' + modelname + ' -> ' + full_model_filename)
    f = open(full_model_filename, "w")
    stdout = sys.stdout
    sys.stdout = f
    print( """
#ifdef _AIX
#include <stdlib.h>
#endif
#include <string.h>
#include <dlfcn.h>

#include "ION_IF.h"
#include "MULTI_ION_IF.h"

namespace limpet {
using ::opencarp::dupstr;
extern opencarp::FILE_SPEC _nc_logf;
}  // namespace limpet

""")
    print_model_functions(modelbase, directory)
    print_dynamic_model_decl(modelname, 0)
    sys.stdout = stdout
    f.close()

    options["modelbase"] = modelbase
    options["file"] = f.name
    options["cflags"] += " -I"+directory

    build_command = "%(compiler)s %(cflags)s %(prefix)s -o %(modelbase)s.so %(modelbase)s.cc %(file)s"
    build_command = build_command % options
    print(build_command)
    os.system(build_command)
