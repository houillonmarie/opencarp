#!/usr/bin/env python3

# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

##
# \todo Please add documentation to this file
#

from im import *
from unit import si,log_diff
import re
import parser
import os
import sys

# Auto-prints a string using the new (as of 2.4) string-template
# based substitution.
import copy, inspect
try:
    from string import Template
except ImportError:
    from stringtemplate import Template # backport for 2.3

def find_file(filename):
    imp_list = ['']
    # (bindir, f) = os.path.split(sys.argv[0])
    # if (len(bindir) > 0):
    #     imp_list.append(bindir+"/../include/")
    #     imp_list.append(bindir+"/")
    # We append here potential locations of imp_list.txt relative to Makefile dir
    imp_list.append("./models/")
    location = False
    for imp_list_file in imp_list:
        try:
            trial_location = imp_list_file+filename
            open(trial_location,'r')
            location = trial_location
            break
        except IOError:
            pass
    if not location:
        print("Error: Can't open "+filename+"!  Perhaps you should use the full path name to the file?")
        sys.exit(1)
    return location

def parse_external_units(filename):
    '''Takes in a filename, extracts all the external
    variables from that filename, and records their units.
    '''
    var_external = {}

    f = open(filename, "r")
    for line in f:
        #remove comments.
        line = re.sub(r'#.*$','',line)
        #remove blank lines.
        line = line.strip()
        if not line:
            continue
        m = re.match(r'variable\s+(\w+)\s+(\w+)\s+(.*?)$',line)
        if m:
            class Blank: pass
            b = Blank();
            b.type = m.group(1)
            b.name = m.group(2)
            b.unit = parser.parse_unit(m.group(3))
            var_external[b.name] = b
    return var_external

def basic(var):
    return var.name

def lookup_format(var, f=basic):
    if var.is_a('lookup'):
        return "%s_row[%s_idx]" % (var.info('lookup').name, var.name)
    else:
        return f(var)

class Limpet:
    def __init__(self, root, name, external_units):
        self.root = root
        (self.directory, self.filebase) = os.path.split(name)

        if len(self.directory) == 0:
            self.directory = '.'

        self.external_units = external_units
        self.private = Set()
        self.public = Set()
        for var in self.root.getVars('external'):
            for e in list(external_units.values()):
                if e.name == var.info('external'):
                    if e.type == "private":
                        self.private.add(var)
                        break
                    else:
                        self.public.add(var)
                        break
            else:
                self.private.add(var)
        self.ext_regional = self.root.getVars('external') & self.root.getVars('regional')
        self.ext_nodal = self.root.getVars('external') & self.root.getVars('nodal')
        #the code below will incorrectly set a variable as a reqvar if
        #it is assigned before it is used.  I can fix this by doing
        #dependency analysis, but it's complicated and the need hasn't
        #come up yet.
        self.reqvars = self.ext_nodal & (self.root.getVars('store') | self.root.used)
        self.modvars = self.ext_nodal & (self.root.assigned | self.root.statevars)

        self.model_name = basename(self.filebase)
        self.value_hash = {
            'header' : self.model_name.upper(),
            'name'  : self.model_name,
            'private' : self.model_name+"_Private",
            'time_factor' : "1e%d" % log_diff(si.ms,self.root.time_unit),
            }

    def type(self, var):
        if var.is_a('gate'):
            return "Gatetype"
        else:
            return "GlobalData_t"

    def printExternalDef(self, out):
        out("//Prepare all the public arrays.")
        for var in order((self.reqvars | self.modvars) & self.public):
            out("GlobalData_t *%s_ext = impdata[%s];" % (var.name, var.info('external')))
        out("//Prepare all the private functions.")
        for var in order((self.reqvars | self.modvars) & self.private):
            out('''
int __%(var)s_sizeof;
int __%(var)s_offset;
SVgetfcn __%(var)s_SVgetfcn = get_sv_offset( IF->parent->type, "%(ext)s", &__%(var)s_offset, &__%(var)s_sizeof );
SVputfcn __%(var)s_SVputfcn = __%(var)s_SVgetfcn ? getPutSV(__%(var)s_SVgetfcn) : NULL;
''' % { "var" : var.name, "ext": var.info('external') })


    def printGetExternal(self, out):
        out("//Initialize the external vars to their current values")
        for var in order((self.reqvars | self.modvars) & self.public):
            out("GlobalData_t %(var)s = %(var)s_ext[__i];" % {"var" : var})
        for var in order((self.reqvars | self.modvars) & self.private):
            out('GlobalData_t {0} = __{0}_SVgetfcn ? __{0}_SVgetfcn(IF->parent, __i, __{0}_offset) :'
                                                                         'sv->__{0}_local;'.format(var) )

    def printSetExternal(self, out):
        out("//Save all external vars")
        for var in order((self.reqvars | self.modvars) & self.public):
            out("%(var)s_ext[__i] = %(var)s;" % {"var" : var})
        for var in order((self.reqvars | self.modvars) & self.private):
            out('if( __{0}_SVputfcn ) \n\t__{0}_SVputfcn(IF->parent, __i, __{0}_offset, {0});'
                 '\nelse\n\tsv->__{0}_local={0};'.format(var))


    def printLookupTableRow(self, out, var, rhs):
        out('''LUT_data_t %(row)s[NROWS_%(var)s];
LUT_interpRow(&IF->tables[%(table)s], %(value)s, __i, %(row)s);'''
            % { 'row' : var.name + "_row",
                'table' : var.name + "_TAB",
                'value' : rhs(var),
                'var'   : var.name,
                'model' : self.model_name,
                })

    def changeExternalUnits(self, factor, out, complete_format):
        out("//Change the units of external variables as appropriate.")
        for var in order(self.root.statevars | self.modvars | self.reqvars):
            if var.is_a('unit'):
                current_unit = var.info('unit')
                if var.is_a('external'):
                    name = var.info('external')
                else:
                    name = var.name
                if name in self.external_units:
                    desired_unit = self.external_units[name].unit
                    if current_unit != desired_unit:
                        #print(var,desired_unit,current_unit)
                        out("%s *= 1e%d;" % (complete_format(var), 
                                             factor*log_diff(desired_unit,current_unit)))
        out("\n\n")


    def printEquations(self,
                       out,
                       eqnmap,
                       valid,
                       target,
                       declared=Set(),
                       lhs_format=basic,
                       rhs_format=basic,
                       format="%(decl)s%(lhs)s = %(rhs)s;",
                       lookup=Set(),
                       post_print=lambda var : '',
                       declare_type="GlobalData_t "
                       ) :
        lookup_dependencies = Set([self.root.table[var.info('lookup').name]
                                   for var in lookup
                                   ])
        all_lookup = eqnmap.dependencies_of(lookup, valid | lookup_dependencies)
        def specific_lookup_format(var, f, lookup=lookup):
            if var in lookup:
                return lookup_format(var)
            else:
                return f(var)
        rhs = lambda v: specific_lookup_format(v, rhs_format)
        lhs = lambda v: specific_lookup_format(v, lhs_format)
        #for var in order(lookup_dependencies & valid):
        #    self.printLookupTableRow(out, var, rhs)
        lookup_dependencies -= valid
        functions = 0
        func_vars = Set()
        var_list = []
        for var in eqnmap.getList(target, valid):
            if var not in declared:
                declare = declare_type
            else:
                declare = ""
            write = 1
            if var in target and var in all_lookup:
                lhs_text = lhs_format(var)
                rhs_text = rhs(var)
            elif var not in all_lookup:
                eqn = eqnmap[var]
                lhs_text = lhs(var)
                rhs_text = eqn.formatRHS(rhs)
                new_funcs = eqn.countFunctions()
                if new_funcs > 0:
                    functions += new_funcs
                    func_vars.add(eqn.lhs)
            else:
                write = 0
            if write:
                var_list.append(var)
                out(format
                    % {
                        "decl" : declare,
                        "lhs" : lhs_text,
                        "rhs" : rhs_text
                        })
                post = post_print(var)
                if post:
                    out(post)
            if var in lookup_dependencies:
                self.printLookupTableRow(out, var, rhs)
        return (functions, func_vars, var_list)

    def license(self):
        return '''// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

'''

    def reference(self):
        with open(self.directory+'/'+self.model_name+'.model', 'r', encoding="utf-8") as file:
            lines = file.readlines()

        keywords=[
        "Authors",
        "Year",
        "Title",
        "Journal",
        "DOI",
        "Comment"
        ]

        reference=[]        
        for line in lines:
            if line == "\n" or not line.lstrip() or line.lstrip()[0] != "#":
                continue
            for keyword in keywords:
                if re.search(rf"{keyword}", line, flags=re.IGNORECASE) is not None: 
                    reference.append(keyword + ": " + line.split("::=")[1].strip())
            
        ref_vars = { 'reference' : '\n'.join('*  ' + x for x in reference) }

        ref_source = Template("""
/*
*
${reference}
*
*/
        """)

        ref = ref_source.safe_substitute(ref_vars)
        return ref

    ###################################################################333
    def printHeader(self, out_dir):
        out = Indenter(open(os.path.join(out_dir, self.model_name+".h"), "w"))
        out(self.license())
        out(self.reference())
        out('''//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __%(header)s_H__
#define __%(header)s_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

''' % self.value_hash)

        req_flags = [ var.info('external')+"_DATA_FLAG"
                      for var
                      in order(self.reqvars & self.public)
                      ]
        out("#define "+self.model_name+"_REQDAT "+"|".join(req_flags))

        mod_flags = [ var.info('external')+"_DATA_FLAG"
                      for var
                      in order(self.modvars & self.public)
                      ]
        out("#define "+self.model_name+"_MODDAT "+"|".join(mod_flags))

        out('''
struct %(name)s_Params {
''' % self.value_hash)
        out.inc()
        for var in order(self.root.getVars('param')):
            out("%s %s;" % (self.type(var), var.name))
        flags = self.root.getVars('flag')
        if flags:
            out("char* flags;")
        out.dec()
        out('''
};
''' % self.value_hash)

        if flags:
            valid_flags = []
            for flag in order(flags):
                if not flag.info('flag'):
                    valid_flags.append(str(flag))
                    valid_flags.append('no_'+str(flag))
                else:
                    for enum in flag.info('flag'):
                        valid_flags.append(str(enum))
            out(('static const char* %(name)s_flags = "' % self.value_hash)
                +"|".join(valid_flags)
                +'";\n\n')

        out('''
struct %(name)s_state {
''' % self.value_hash)
        out.inc()
        statevars = self.root.getVars('nodal') - (self.root.getVars('external'))
        for var in order(statevars):
            out("%s %s;" % (self.type(var), var.name))
        for var in order(self.private):
            out("{} __{}_local;".format(self.type(var), var.name))
        if len(statevars)==0:
            out("char dummy; //PGI doesn't allow a structure of 0 bytes.")
        out.dec()
        out('''
};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_%(name)s(ION_IF *);
void construct_tables_%(name)s(ION_IF *);
void destroy_%(name)s(ION_IF *);
void initialize_sv_%(name)s(ION_IF *, GlobalData_t**);
void compute_%(name)s(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
''' % self.value_hash)

    ###################################################################333

    def printSource(self, out_dir):
        out = Indenter(open(os.path.join(out_dir, self.model_name+".cc"), "w"))
        out(self.license())
        out(self.reference())
        out.indent_amount = 2

        # Define all the necessary variable sets here
        #
        # constants - variables that are constant no matter what.
        # param_vars - variables that are defined as parameters but are otherwise
        #    constant
        # param_nodal_vars - variables that are parameters and nodal vars.
        # regional_constants - variables that are constant for a region
        # recomputed_regional_constants - regional constants that need to be recomputed
        #     once every timestep for each region
        # nodal_constants - variables that are nodal yet constant

        out('''
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "%(name)s.h"

#ifdef _OPENMP
#include <omp.h>
#endif

''' % self.value_hash)
        if self.root.getVars('rosenbrock'):
            out('''
#include "Rosenbrock.h"
''' % self.value_hash)
        if self.root.getVars('cvode'):
            out('''
#ifdef USE_CVODE
#include <nvector/nvector_serial.h>
#include <cvode/cvode.h>
#include <cvode/cvode_diag.h>
#endif
''' % self.value_hash)
        out('''

namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_%(name)s(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_%(name)s( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
''' % self.value_hash)

        init = self.root.init_system()

        good_vars = Set()
        nodal_vars = self.root.getVars('nodal')
        nodal_constants = self.root.getVars('nodal') - self.public - self.root.statevars
        nodal_influences = self.root.main.influences_of(nodal_constants)

        param_vars = self.root.getVars('param') | self.ext_regional
        param_constants = param_vars - nodal_vars
        param_init = param_vars - param_constants
        param_influences = self.root.main.influences_of(param_constants)

        time_vars = self.root.getVars('time') - Set([self.root.table['dt']])
        time_influences = self.root.main.influences_of(time_vars)
        dt = Set([self.root.table['dt']])
        dt_influences = self.root.main.influences_of(dt)

        constants = self.root.main.reachable_from(Set())
        just_dt = self.root.main.reachable_from(dt) - constants

        global_constants = constants - param_influences - time_influences - dt_influences - nodal_influences
        regional_constants = ((constants & param_influences) | just_dt ) - nodal_influences
        recomputed_regional_constants = regional_constants - param_constants

        self.printEquations(out=out,
                            eqnmap=self.root.main,
                            valid=good_vars,
                            target=global_constants,
                            format="#define %(lhs)s (GlobalData_t)(%(rhs)s)"
                            )
        good_vars |= global_constants

        out("\n\n")

        out('''
void initialize_params_%(name)s( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  %(name)s_Params *p = (%(name)s_Params *)IF->params;

  // Compute the regional constants
  {
'''
% self.value_hash)
        if self.root.getVars('cvode') :
            out('''
#ifndef USE_CVODE
        //log_msg(NULL,5,0,"%(name)s needs CVODE. Recompilation needed.");
        exit(1);
#endif
'''
%self.value_hash)
        out.inc()
        out.inc()

        def basic(var):
            if var.is_a('param'):
                return "p->%s" % var.name
            elif var.is_a('external') and var.is_a('regional'):
                return "region->%s" % var.info('external')
            else:
                return var.name

        def post_flag_behavior(var):
            ret = []
            if var.is_a('flag'):
                ret.append('if (0) ;')
                if not var.info('flag'):
                    ret.append('else if (flag_set(p->flags, "%s")) %s = 1;' % (var, basic(var)))
                    ret.append('else if (flag_set(p->flags, "no_%s")) %s = 0;' % (var, basic(var)))
                else:
                    for enum in var.info('flag'):
                        ret.append('else if (flag_set(p->flags, "%s")) %s = %s;' % (enum, basic(var), basic(enum)))
            return "\n".join(ret)

        self.printEquations(out=out,
                            eqnmap=init.main,
                            valid=good_vars,
                            target=param_constants - (self.ext_regional - self.root.assigned),
                            declared=param_constants,
                            lhs_format=basic,
                            rhs_format=basic,
                            post_print=post_flag_behavior,
                            )
        good_vars = good_vars | param_constants

        out.dec()
        out("}")
        out("// Compute the regional initialization")
        out("{")
        out.inc()

        self.printEquations(out=out,
                            eqnmap=init.main,
                            valid=good_vars,
                            target=param_init,
                            declared=param_init,
                            lhs_format=basic,
                            rhs_format=basic,
                            )

        out.dec()
        out("}")
        if self.root.getVars('trace'):
            out("IF->type->trace = trace_%(name)s;" % self.value_hash);
        out.dec()
        out('''
}


// Define the parameters for the lookup tables
enum Tables {
''' % self.value_hash)
        out.inc()
        for lookup in self.root.lookup:
            out("%s_TAB," % lookup.name)
        out.dec()
        out('''
  N_TABS
};

// Define the indices into the lookup tables.
''' % self.value_hash)

        def printRegionalConstants(
            self=self,
            out=out,
            eqnmap=self.root.main,
            target = recomputed_regional_constants,
            valid = global_constants | param_constants | dt,
            format = basic,
            declare_type = "GlobalData_t "
            ):
            out("// Define the constants that depend on the parameters.")
            self.printEquations(out=out,
                                eqnmap=eqnmap,
                                valid=valid,
                                target=target,
                                lhs_format=format,
                                rhs_format=format,
                                declare_type=declare_type,
                                )

        good_vars |= Set([self.root.table['dt']]) | recomputed_regional_constants
        update_inputs = self.root.statevars | self.reqvars | nodal_constants

        all_targets = Set()
        external_update_targets = self.root.getVars('store') | (self.modvars - self.root.getVars('state'))
        all_targets |= external_update_targets

        rush_larsen_targets = Set()
        for var in self.root.getVars('rush_larsen'):
            rush_larsen_targets |= Set(var.info('rush_larsen'))
        all_targets |= rush_larsen_targets

        sundnes_half_targets = Set()
        for var in self.root.getVars('sundnes'):
            sundnes_half_targets.add(var.info('sundnes').half)
            all_targets.add(var.info('sundnes').full)
        all_targets |= sundnes_half_targets

        rosenbrock = self.root.getVars('rosenbrock')
        rosenbrock_diff_targets = Set()
        rosenbrock_partial_targets = Set()
        for var in rosenbrock:
            rosenbrock_diff_targets.add(var.info('implicit_method').diff)
            rosenbrock_partial_targets |= var.info('implicit_method').partial
        all_targets |= rosenbrock_diff_targets | rosenbrock_partial_targets
        rosenbrock_nodal_req = self.reqvars - rosenbrock

        cvode = self.root.getVars('cvode')
        cvode_targets = Set([var.info('diff') for var in self.root.getVars('cvode')])
        all_targets |= cvode_targets
        cvode_nodal_req = self.reqvars - cvode

        fe_targets = Set([var.info('diff') for var in self.root.getVars('fe')])
        all_targets |= fe_targets

        rk2_targets = Set([var.info('diff') for var in self.root.getVars('rk2')])
        all_targets |= rk2_targets

        rk4_targets = Set([var.info('diff') for var in self.root.getVars('rk4')])
        all_targets |= rk4_targets

        markov_be_targets = Set()
        markov_be_solve = Set()
        for var in self.root.getVars('markov_be'):
            markov_be_targets.add(var.info('implicit_method').diff)
            markov_be_targets |= var.info('implicit_method').partial
            markov_be_solve.add(var.info('markov_be'))
        all_targets |= markov_be_targets | markov_be_solve

        for lookup in self.root.lookup:
            lookup_var = Set([self.root.table[lookup.name]])
            lookup_canidates = self.root.main.influences_of(lookup_var, update_inputs)
            lookup_canidates &= self.root.main.reachable_from(good_vars | lookup_var, update_inputs)
            lookup.set = self.root.main.extract_external(lookup_canidates, all_targets)
            for var in lookup.set:
                var.make_a('lookup', lookup)

        for lookup in self.root.lookup:
            out("enum %s_TableIndex {" % lookup.name)
            out.inc()
            for var in order(lookup.set):
                out("%s_idx," % var.name)
            out("NROWS_%s" % lookup.name)
            out.dec()
            out("};")
            out("\n")

        out('''

void construct_tables_%(name)s( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*%(time_factor)s;
  cell_geom *region = &IF->cgeom;
  %(name)s_Params *p = (%(name)s_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

''' % self.value_hash)
        out.inc()
        printRegionalConstants(declare_type="double ")


        for lookup in self.root.lookup:
            if not lookup.set:
                continue
            lookup_hash = { 'name' : lookup.name,
                            'min' : lookup.lb,
                            'max' : lookup.ub,
                            'step' : lookup.step,
                            'model' : self.model_name,
                            }
            out("""
// Create the %(name)s lookup table
LUT* %(name)s_tab = &IF->tables[%(name)s_TAB];
LUT_alloc(%(name)s_tab, NROWS_%(name)s, %(min)s, %(max)s, %(step)s, "%(model)s %(name)s");
for (int __i=%(name)s_tab->mn_ind; __i<=%(name)s_tab->mx_ind; __i++) {
  double %(name)s = %(name)s_tab->res*__i;
  LUT_data_t* %(name)s_row = %(name)s_tab->tab[__i];
"""
                % lookup_hash)
            out.inc()

            def construct_lookup_format(var, base, f=basic):
                if var == base:
                    return var.name
                else:
                    return lookup_format(var, f)

            lookup_var = self.root.table[lookup.name]
            lookup_var_set = Set([lookup_var])
            self.printEquations(out=out,
                                eqnmap=self.root.main,
                                target=lookup.set,
                                valid=good_vars | lookup_var_set,
                                declared=lookup.set,
                                lhs_format=lambda v: construct_lookup_format(v, lookup_var),
                                rhs_format=lambda v: construct_lookup_format(v, lookup_var),
                                declare_type="double "
                                )
            out.close()
            out('check_LUT(%(name)s_tab);\n\n' % lookup_hash)

        out.dec()
        out('''
}
''' % self.value_hash)
        if rosenbrock | cvode:
            out('''
struct %(name)s_Regional_Constants {
''' % self.value_hash)
            out.inc()
            for var in order(recomputed_regional_constants):
                out("GlobalData_t %s;" % var.name)
            out.dec()
            out('''
};

struct %(name)s_Nodal_Req {
''' % self.value_hash)
            out.inc()
            for var in order(rosenbrock_nodal_req | cvode_nodal_req):
                out("GlobalData_t %s;" % var.name)
            out.dec()
            out('''
};

struct %(private)s {
  ION_IF *IF;
  int   node_number;
  void* cvode_mem;
  bool  trace_init;
  %(name)s_Regional_Constants rc;
  %(name)s_Nodal_Req nr;
};
''' % self.value_hash)

        if rosenbrock:
            out('''
//Printing out the Rosenbrock declarations
void %(name)s_rosenbrock_f(float*, float*, void*);
void %(name)s_rosenbrock_jacobian(float**, float*, void*, int);

enum Rosenbrock {
''' % self.value_hash)
            out.inc()
            for var in order(rosenbrock):
                out("ROSEN_%s," % var)
            out.dec()
            out('''
  N_ROSEN
};

void rbStepX  ( float *X,
                void (*calcDX) (float*,  float*, void*),
                void (*calcJ)  (float**, float*, void*, int ),
                void *params, float h, int N );

''' % self.value_hash)
        if cvode:
            out('''
enum Cvode_Vars {
''' % self.value_hash)
            out.inc()
            for var in order(cvode):
                out("CVODE_%s," % var)
            out.dec()
            out('''
  N_CVODE
};

#ifdef USE_CVODE
int %(name)s_internal_rhs_function(realtype t, N_Vector CVODE, N_Vector CVODE_dot, void* user_data);
#endif

#ifndef LIMPET_CVODE_RTOL
#define LIMPET_CVODE_RTOL 1e-5
#endif

//disable absolute tolerances for all gates.
#ifndef LIMPET_CVODE_ATOL
#define LIMPET_CVODE_ATOL 1e-16
#endif

''' % self.value_hash)
        out('''


void    initialize_sv_%(name)s( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*%(time_factor)s;
  cell_geom *region = &IF->cgeom;
  %(name)s_Params *p = (%(name)s_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(%(name)s_state) );
  %(name)s_state *sv_base = (%(name)s_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
''' % self.value_hash)
        out.inc()
        printRegionalConstants(declare_type="double ")

        self.printExternalDef(out)

        out.dec()
        out('''
  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    %(name)s_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
''' % self.value_hash)
        good_vars |= Set([self.root.table['t']])

        out.inc()
        out.inc()

        def complete_format(var, f=basic):
            if (var.is_a('state') or var.is_a('nodal')) and not var.is_a('external'):
                return "sv->%s" % var.name
            else:
                return f(var)

        self.printGetExternal(out)
        self.changeExternalUnits(1, out, complete_format)

        for var in order(param_init):
            out("%s = %s;" % (complete_format(var), basic(var)))

        out("// Initialize the rest of the nodal variables")
        init_ex_mod = self.root.getVars('init') & (self.ext_nodal)
        init_ex_req = self.reqvars - init_ex_mod

        self.printEquations(out=out,
                            eqnmap=init.main,
                            valid=good_vars | init_ex_req | param_vars,
                            target=((self.root.getVars('nodal')-init_ex_req) | init_ex_mod) - param_vars,
                            declared = self.root.getVars('nodal') | self.root.getVars('external'),
                            lhs_format = complete_format,
                            rhs_format = complete_format,
                            declare_type = "double ",
                            )

        self.changeExternalUnits(-1, out, complete_format)
        self.printSetExternal(out)

        out.dec()
        out.dec()
        out('''
  }
''' % self.value_hash )
        if rosenbrock | cvode:
           out('''

  int num_thread = 1;
#ifdef _OPENMP
  num_thread = omp_get_max_threads();
#endif
  %(private)s* userdata = (%(private)s*)IMP_malloc(num_thread,sizeof(%(private)s));
  for( int j=0; j<num_thread; j++ ){
    userdata[j].IF = IF;
    userdata[j].node_number = 0;
  }
  IF->ion_private = userdata;
  IF->private_sz  = sizeof(userdata[0]);

''' % self.value_hash)
        if cvode:
            out('''
  //setup CVODE
  {
#ifdef USE_CVODE
#if SUNDIALS_VERSION_MAJOR >= 4
    void* cvode_mem = CVodeCreate(CV_BDF);
#else
    void* cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);
#endif
    assert(cvode_mem != NULL);
    N_Vector cvode_y = N_VNew_Serial(N_CVODE);
    int flag = CVodeInit(cvode_mem, %(name)s_internal_rhs_function, 0, cvode_y);
    assert(flag == CV_SUCCESS);
    N_VDestroy(cvode_y);

    flag = CVodeSStolerances(cvode_mem, LIMPET_CVODE_RTOL, LIMPET_CVODE_ATOL);
    assert(flag == CV_SUCCESS);
    flag = CVodeSetMaxStep(cvode_mem, dt);
    assert(flag == CV_SUCCESS);
    userdata->cvode_mem = cvode_mem;
    flag = CVodeSetUserData(cvode_mem, userdata);
    assert(flag == CV_SUCCESS);
    flag = CVDiag(cvode_mem);
    assert(flag == CV_SUCCESS);
#endif
  }
''' % self.value_hash)
        out('''
}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_%(name)s(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*%(time_factor)s;
  cell_geom *region = &IF->cgeom;
  %(name)s_Params *p  = (%(name)s_Params *)IF->params;
  %(name)s_state *sv_base = (%(name)s_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

''' % self.value_hash)

        out.inc()
        printRegionalConstants()
        if rosenbrock | cvode:
            out('''
//Initializing the userdata structures.
%(private)s* ion_private = (%(private)s*) IF->ion_private;
int nthread = 1;
#ifdef _OPENMP
nthread = omp_get_max_threads();
#endif
for( int j=0; j<nthread; j++ ) {
''' % self.value_hash)
            out.inc()
            for var in order(recomputed_regional_constants):
                out("ion_private[j].rc.%s = %s;" % (var, complete_format(var)))
            out.dec()
            out("}")
        if cvode:
            out('''
#ifdef USE_CVODE
realtype CVODE_array[N_CVODE];
N_Vector CVODE = N_VMake_Serial(N_CVODE, CVODE_array);
#endif
void* cvode_mem = ((%(private)s*)(IF->ion_private))->cvode_mem;
''' % self.value_hash)

        self.printExternalDef(out)

        out.dec()
        out('''
#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    %(name)s_state *sv = sv_base+__i;

''' % self.value_hash)
        out.inc()
        out.inc()

        all_lookup = Set()
        for lookup in self.root.lookup:
            all_lookup |= lookup.set

        class lhs_wrapper:
            def __init__(self, f):
                self.f = f;
            def __call__(self, var):
                if var.is_a('state'):
                    return var.name + "_new"
                else:
                    return self.f(var)

        lhs_complete = lhs_wrapper(complete_format)

        def dont_set_modvars(var, f=complete_format, modvars=self.modvars):
            if (var in modvars
                and not var.is_a('state')):
                return basic(var)
            else:
                return f(var)
        lhs_dont = lhs_wrapper(dont_set_modvars)

        functions = 0
        func_vars = Set()
        already_computed = Set()

        self.printGetExternal(out)
        self.changeExternalUnits(1, out, complete_format)

        out("//Compute lookup tables for things that have already been defined.")
        specified_lookup_variables = Set([self.root.table[lookup.name] for lookup in self.root.lookup])
        for var in order(specified_lookup_variables & update_inputs):
            self.printLookupTableRow(out, var, complete_format)
        out("\n\n")

        out("//Compute storevars and external modvars")
        (this_func_count, this_func_vars, this_var_list) \
            = self.printEquations(out=out,
                                  eqnmap=self.root.main,
                                  valid=good_vars | update_inputs,
                                  target=external_update_targets,
                                  lhs_format = lhs_dont,
                                  rhs_format = dont_set_modvars,
                                  lookup=all_lookup,
                                  declared=self.modvars-self.root.statevars
                                  )
        functions += this_func_count; func_vars |= this_func_vars
        already_computed |= Set(this_var_list)



        ####################################################################
        out("\n\n")
        out("//Complete Forward Euler Update")
        (this_func_count, this_func_vars, this_var_list) \
            = self.printEquations(out=out,
                                  eqnmap=self.root.main,
                                  valid=good_vars | update_inputs | already_computed,
                                  target=fe_targets-good_vars,
                                  lhs_format = lhs_dont,
                                  rhs_format = dont_set_modvars,
                                  lookup=all_lookup,
                                  declared=self.modvars
                                  )
        functions += this_func_count; func_vars |= this_func_vars
        already_computed |= Set(this_var_list)
        for var in order(self.root.getVars('fe')):
            diff = var.info('diff')
            out("GlobalData_t %(lhs)s = %(rhs)s+%(diff)s*dt;" % {
                    'lhs' : lhs_complete(var),
                    'rhs' : complete_format(var),
                    'diff' : complete_format(diff)})

        ########################################################################
        out("\n\n")
        out("//Complete Rush Larsen Update")
        (this_func_count, this_func_vars, this_var_list) \
            = self.printEquations(out=out,
                                  eqnmap=self.root.main,
                                  valid=good_vars | update_inputs | already_computed,
                                  target=rush_larsen_targets-good_vars,
                                  lhs_format = lhs_dont,
                                  rhs_format = dont_set_modvars,
                                  lookup=all_lookup,
                                  declared=self.modvars
                                  )
        functions += this_func_count; func_vars |= this_func_vars
        already_computed |= Set(this_var_list)
        for var in order(self.root.getVars('rush_larsen')):
            (a, b) = var.info('rush_larsen')
            out("GlobalData_t %(lhs)s = %(a)s+%(b)s*%(rhs)s;" % {
                    'a': complete_format(a),
                    'b': complete_format(b),
                    'lhs': lhs_complete(var),
                    'rhs': complete_format(var)})

        ########################################################################
        out("\n\n")
        out("//Complete RK2 Update")

        (this_func_count, this_func_vars, this_var_list) \
            = self.printEquations(out=out,
                                  eqnmap=self.root.main,
                                  valid=good_vars | update_inputs | already_computed,
                                  target=rk2_targets-good_vars,
                                  lhs_format = lhs_dont,
                                  rhs_format = dont_set_modvars,
                                  lookup=all_lookup,
                                  declared=self.modvars
                                  )
        functions += this_func_count; func_vars |= this_func_vars
        already_computed |= Set(this_var_list)

        class intermediate_wrapper:
            def __init__(self, f, vars_to_wrap):
                self.f = f
                self.vars_to_wrap = vars_to_wrap
            def __call__(self, var):
                if var in self.vars_to_wrap:
                    return "sv_intermediate_%s" % var.name
                else:
                    return self.f(var)

        if self.root.getVars('rk2'):
            for var in order(self.root.getVars('rk2')):
                out("GlobalData_t %s;" % lhs_dont(var))

            rk2_dont = intermediate_wrapper(dont_set_modvars, self.root.getVars('rk2'))

            out("{"); out.inc()
            out("GlobalData_t t = t + dt/2;")
            for var in order(self.root.getVars('rk2')):
                out("GlobalData_t %s = %s+dt/2*%s;" % (
                        rk2_dont(var),
                        complete_format(var),
                        complete_format(var.info('diff'))))
            for var in order(specified_lookup_variables & self.root.getVars('rk2')):
                self.printLookupTableRow(out, var, rk2_dont)
            useable_from_previous = already_computed - self.root.main.influences_of(self.root.getVars('rk2'))
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs | useable_from_previous,
                                      target=rk2_targets-good_vars,
                                      lhs_format = lhs_wrapper(rk2_dont),
                                      rhs_format = rk2_dont,
                                      lookup=all_lookup
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            for var in order(self.root.getVars('rk2')):
                out("%s = %s+dt*%s;" % (
                        lhs_dont(var), dont_set_modvars(var), dont_set_modvars(var.info('diff'))))
            out.dec(); out('}')

        ########################################################################
        out("\n\n")
        out("//Complete RK4 Update")
        (this_func_count, this_func_vars, this_var_list) \
            = self.printEquations(out=out,
                                  eqnmap=self.root.main,
                                  valid=good_vars | update_inputs | already_computed,
                                  target=rk4_targets-good_vars,
                                  lhs_format = lhs_dont,
                                  rhs_format = dont_set_modvars,
                                  lookup=all_lookup,
                                  declared=self.modvars
                                  )
        functions += this_func_count; func_vars |= this_func_vars
        already_computed |= Set(this_var_list)

        if self.root.getVars('rk4'):
            for var in order(self.root.getVars('rk4')):
                out("GlobalData_t %s;" % lhs_dont(var))
                out("GlobalData_t rk4_k4_%s;" % var)
                out("GlobalData_t rk4_k3_%s;" % var)
                out("GlobalData_t rk4_k2_%s;" % var)
                out("GlobalData_t rk4_k1_%s = %s*dt;" % (var, var.info('diff')))

            rk4_dont = intermediate_wrapper(dont_set_modvars, self.root.getVars('rk4'))

            out("{"); out.inc()
            out("GlobalData_t t = t + dt/2;")
            for var in order(self.root.getVars('rk4')):
                out("GlobalData_t %s = %s+rk4_k1_%s/2;" % (
                        rk4_dont(var),
                        complete_format(var),
                        var))
            for var in order(specified_lookup_variables & self.root.getVars('rk4')):
                self.printLookupTableRow(out, var, rk4_dont)
            useable_from_previous = already_computed - self.root.main.influences_of(self.root.getVars('rk4'))
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs | useable_from_previous,
                                      target=rk4_targets-good_vars,
                                      lhs_format = lhs_wrapper(rk4_dont),
                                      rhs_format = rk4_dont,
                                      lookup=all_lookup
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            for var in order(self.root.getVars('rk4')):
                out("rk4_k2_%s = dt*%s;" % (
                        var, dont_set_modvars(var.info('diff'))))
            out.dec(); out('}')
            out("{"); out.inc()
            out("GlobalData_t t = t + dt/2;")
            for var in order(self.root.getVars('rk4')):
                out("GlobalData_t %s = %s+rk4_k2_%s/2;" % (
                        rk4_dont(var),
                        complete_format(var),
                        var))
            for var in order(specified_lookup_variables & self.root.getVars('rk4')):
                self.printLookupTableRow(out, var, rk4_dont)
            useable_from_previous = already_computed - self.root.main.influences_of(self.root.getVars('rk4'))
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs | useable_from_previous,
                                      target=rk4_targets-good_vars,
                                      lhs_format = lhs_wrapper(rk4_dont),
                                      rhs_format = rk4_dont,
                                      lookup=all_lookup
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            for var in order(self.root.getVars('rk4')):
                out("rk4_k3_%s = dt*%s;" % (
                        var, dont_set_modvars(var.info('diff'))))
            out.dec(); out('}')
            out("{"); out.inc()
            out("GlobalData_t t = t + dt;")
            for var in order(self.root.getVars('rk4')):
                out("GlobalData_t %s = %s+rk4_k3_%s;" % (
                        rk4_dont(var),
                        complete_format(var),
                        var))
            for var in order(specified_lookup_variables & self.root.getVars('rk4')):
                self.printLookupTableRow(out, var, rk4_dont)
            useable_from_previous = already_computed - self.root.main.influences_of(self.root.getVars('rk4'))
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs | useable_from_previous,
                                      target=rk4_targets-good_vars,
                                      lhs_format = lhs_wrapper(rk4_dont),
                                      rhs_format = rk4_dont,
                                      lookup=all_lookup
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            for var in order(self.root.getVars('rk4')):
                out("rk4_k4_%s = dt*%s;" % (
                        var, dont_set_modvars(var.info('diff'))))
            out.dec(); out('}')

            for var in order(self.root.getVars('rk4')):
                out("%s = %s+(rk4_k1_%s + 2*rk4_k2_%s + 2*rk4_k3_%s + rk4_k4_%s)/6;" % (
                        lhs_dont(var), dont_set_modvars(var), var,var,var,var))


        ###########################################################
        out("\n\n")
        out("//Complete Sundnes Update")
        (this_func_count, this_func_vars, this_var_list) \
            = self.printEquations(out=out,
                                  eqnmap=self.root.main,
                                  valid=good_vars | update_inputs | already_computed,
                                  target=sundnes_half_targets-good_vars,
                                  lhs_format = lhs_dont,
                                  rhs_format = dont_set_modvars,
                                  lookup=all_lookup,
                                  declared=self.modvars
                                  )
        functions += this_func_count; func_vars |= this_func_vars
        already_computed |= Set(this_var_list)

        sundnes = self.root.getVars('sundnes')
        for var in order(sundnes):
            out("GlobalData_t %s;" % lhs_dont(var))
        sundnes_dont = intermediate_wrapper(dont_set_modvars, sundnes)

        for var in order(sundnes):
            full = var.info('sundnes').full
            out("{"); out.inc()
            for other_var in order(sundnes):
                if var != other_var:
                    half = other_var.info('sundnes').half
                    out("GlobalData_t %s = %s;" % (sundnes_dont(other_var), dont_set_modvars(half)))
                else:
                    out("GlobalData_t %s = %s;" % (sundnes_dont(var), dont_set_modvars(var)))
            for other_var in specified_lookup_variables & sundnes:
                self.printLookupTableRow(out, other_var, sundnes_dont)
            useable_from_previous = already_computed - self.root.main.influences_of(sundnes)
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs | useable_from_previous,
                                      target = Set([full])-good_vars,
                                      lhs_format = lhs_wrapper(sundnes_dont),
                                      rhs_format = sundnes_dont,
                                      lookup=all_lookup
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            out("%s = %s;" % (lhs_dont(var), sundnes_dont(full)))
            out.dec(); out("}")

        ###########################################################
        out("\n\n")
        out("//Complete Markov Backward Euler method")
        if self.root.getVars('markov_be'):
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs | already_computed,
                                      target=markov_be_targets-good_vars,
                                      lhs_format = lhs_dont,
                                      rhs_format = dont_set_modvars,
                                      lookup=all_lookup,
                                      declared=self.modvars
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            already_computed |= Set(this_var_list)

            for var in order(self.root.getVars('markov_be')):
                out("GlobalData_t %s = %s;" % (lhs_dont(var), dont_set_modvars(var)))

            def solve_format(var, f=lhs_dont):
                if var.is_a('markov_be'):
                    return f(var.info('markov_be'))
                else:
                    return f(var)

            out('''
int __count=0;
GlobalData_t __error=1;
while (__error > 1e-100 && __count < 50) {
'''); out.inc()
            for var in order(self.root.getVars('markov_be')):
                out("GlobalData_t %s = %s;" % (solve_format(var), lhs_dont(var)))

            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                  eqnmap=self.root.main,
                                  valid=good_vars | update_inputs | already_computed,
                                  target=markov_be_solve-good_vars,
                                  lhs_format = solve_format,
                                  rhs_format = solve_format,
                                  lookup=all_lookup,
                                  declared=markov_be_solve
                                  )
            functions += this_func_count; func_vars |= this_func_vars
            already_computed |= Set(this_var_list)
            out("__error=0;")
            for var in order(self.root.getVars('markov_be')):
                out("__error += fabs(%s-%s);" % (solve_format(var),lhs_dont(var)))
                out("%s = %s;" % (lhs_dont(var),solve_format(var)))
            out("__count++;")
            out.dec(); out("}")
            for var in order(self.root.getVars('markov_be')):
                out("%s = %s+dt*%s;" % (lhs_dont(var), dont_set_modvars(var), lhs_dont(var)))
        ###########################################################
        out("\n\n")
        out("//Complete Rosenbrock Update")
        if rosenbrock:
            out('''
int thread=0;
#ifdef _OPENMP
thread = omp_get_thread_num();
#endif
float Rosenbrock_X[N_ROSEN];
''' % self.value_hash)
            for var in order(rosenbrock):
                out("Rosenbrock_X[ROSEN_%s] = %s;" % (var, complete_format(var)))
            for var in order(rosenbrock_nodal_req):
                out("ion_private[thread].nr.%s = %s;" % (var, complete_format(var)))
            out('''ion_private[thread].node_number = __i;

rbStepX(Rosenbrock_X, %(name)s_rosenbrock_f, %(name)s_rosenbrock_jacobian, (void*)(ion_private+thread), dt, N_ROSEN);
''' % self.value_hash)
            for var in order(rosenbrock):
                out("GlobalData_t %s = Rosenbrock_X[ROSEN_%s];" % (lhs_dont(var), var))

        ###########################################################
        out("\n\n")
        out("//Complete Cvode Update")
        if cvode:
            def cvode_format(var,
                             f=dont_set_modvars,
                             nr=cvode_nodal_req,
                             rc=recomputed_regional_constants,
                             cvode=cvode):
                if var in nr:
                    return "nr->"+var.name
                elif var in rc:
                    return "rc->"+var.name
                elif var in cvode:
                    return "NV_Ith_S(CVODE,CVODE_%s)" % var
                else:
                    return f(var)
            out('#ifdef USE_CVODE')
            for var in order(cvode):
                out("%s = %s;" % (cvode_format(var), dont_set_modvars(var)))
            for var in order(cvode_nodal_req):
                out("ion_private->nr.%s = %s;" % (var, dont_set_modvars(var)))
            out('''

//Do the solve
int CVODE_flag;
CVODE_flag = CVodeReInit(cvode_mem, t, CVODE);
assert(CVODE_flag == CV_SUCCESS);
CVODE_flag = CVodeSetInitStep(cvode_mem, dt);
assert(CVODE_flag == CV_SUCCESS);
realtype tret;
ion_private->node_number = __i;
CVODE_flag = CVode(cvode_mem, t+dt, CVODE, &tret, CV_NORMAL);
assert(CVODE_flag == CV_SUCCESS);
#endif

//Unload the data back into LIMPET structures
''' % self.value_hash)
            out('#ifdef USE_CVODE')
            for var in order(cvode):
                out("GlobalData_t %s = %s;" % (lhs_dont(var), cvode_format(var)))
            out('#else')
            for var in order(cvode):
                out("GlobalData_t %s = 0;" % (lhs_dont(var)))
            out('#endif')


        out("\n\n")
        out("//Finish the update")
        for var in order(self.modvars | self.root.statevars):
            lower_bound = var.is_a('lower_bound')
            upper_bound = var.is_a('upper_bound')

            if lower_bound or upper_bound:
                if lower_bound:
                    out('if (%(new)s < %(val)s) { IIF_warn(__i, "%(var)s < %(val)s, clamping to %(val)s"); %(actual)s = %(val)s; }' % {
                            "var" : var.name,
                            "actual" : complete_format(var),
                            "new" : lhs_dont(var),
                            "val" : var.info('lower_bound'),
                            })
                if upper_bound:
                    if lower_bound:
                        cond_str = 'else if'
                    else:
                        cond_str = 'if'

                    out(cond_str+' (%(new)s > %(val)s) { IIF_warn(__i, "%(var)s > %(val)s, clamping to %(val)s"); %(actual)s = %(val)s; }' % {
                            "var" : var.name,
                            "actual" : complete_format(var),
                            "new" : lhs_dont(var),
                            "val" : var.info('upper_bound'),
                            })
                out("else {%s = %s;}" % (complete_format(var), lhs_dont(var)))
            else:
                out("%s = %s;" % (complete_format(var), lhs_dont(var)))

        self.changeExternalUnits(-1, out, complete_format)
        self.printSetExternal(out)

        # if functions:
        #     print("Function %s invocations involving the following variables:" % functions)
        #     for var in order(func_vars):
        #         print(var)
        out.dec()
        out.dec()
        out('''
  }
''' % self.value_hash)
        if cvode:
            out('''
  //free y
#ifdef USE_CVODE
  N_VDestroy(CVODE);
#endif
''' % self.value_hash)
        out('''

}
''' % self.value_hash)
        if rosenbrock:
            def rosen_format(var,
                             f=dont_set_modvars,
                             nr=rosenbrock_nodal_req,
                             rc=recomputed_regional_constants,
                             rosenbrock=rosenbrock):
                if var in nr:
                    return "nr->"+var.name
                elif var in rc:
                    return "rc->"+var.name
                elif var in rosenbrock:
                    return "Rosenbrock_X[ROSEN_%s]" % var
                else:
                    return f(var)
            out('''
void %(name)s_rosenbrock_f(float* Rosenbrock_DX, float* Rosenbrock_X, void* user_data) {
  %(private)s* ion_private = (%(private)s*)user_data;
  %(name)s_Regional_Constants* rc = &ion_private->rc;
  ION_IF* IF = ion_private->IF;
  int __i = ((%(private)s*)user_data)->node_number;
  %(name)s_Nodal_Req* nr = &ion_private->nr;
  cell_geom *region = &IF->cgeom;
  %(name)s_Params *p = (%(name)s_Params *)IF->params;
  %(name)s_state *sv_base = (%(name)s_state *)IF->sv_tab.y;
  %(name)s_state *sv = sv_base+__i;

''' % self.value_hash)
            out.inc()
            for var in order(specified_lookup_variables & update_inputs):
                self.printLookupTableRow(out, var, rosen_format)
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs,
                                      target=rosenbrock_diff_targets-good_vars,
                                      lhs_format = lhs_wrapper(rosen_format),
                                      rhs_format = rosen_format,
                                      lookup=all_lookup
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            for var in order(rosenbrock):
                out("Rosenbrock_DX[ROSEN_%s] = %s;" % (var, rosen_format(var.info('diff'))))
            out.dec()
            out('''
}

void %(name)s_rosenbrock_jacobian(float** Rosenbrock_jacobian, float* Rosenbrock_X, void* user_data, int dddd) {
  %(private)s* ion_private = (%(private)s*)user_data;
  %(name)s_Regional_Constants* rc = &ion_private->rc;
  ION_IF* IF = ion_private->IF;
  int __i = ((%(private)s*)user_data)->node_number;
  %(name)s_Nodal_Req* nr = &ion_private->nr;
  cell_geom *region = &IF->cgeom;
  %(name)s_Params *p = (%(name)s_Params *)IF->params;
  %(name)s_state *sv_base = (%(name)s_state *)IF->sv_tab.y;
  %(name)s_state *sv = sv_base+__i;
''' % self.value_hash)
            out.inc()
            for var in order(specified_lookup_variables & update_inputs):
                self.printLookupTableRow(out, var, rosen_format)
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs,
                                      target=rosenbrock_partial_targets - good_vars,
                                      lhs_format = lhs_wrapper(rosen_format),
                                      rhs_format = rosen_format,
                                      lookup=all_lookup
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            for row_var in order(rosenbrock):
                for col_var in order(rosenbrock):
                    out("Rosenbrock_jacobian[ROSEN_%s][ROSEN_%s] = %s;" %
                        (row_var,
                         col_var,
                         rosen_format(self.root.partial(row_var.info('diff'), col_var))))
            out.dec()
            out('''
}
''' % self.value_hash)
        if cvode:
            out('''
#ifdef USE_CVODE
int %(name)s_internal_rhs_function(realtype t, N_Vector CVODE, N_Vector CVODE_dot, void* user_data) {

  %(private)s* ion_private = (%(private)s*)user_data;
  %(name)s_Regional_Constants* rc = &ion_private->rc;
  ION_IF* IF = ion_private->IF;
  %(name)s_Nodal_Req* nr = &ion_private->nr;
  int __i = ((%(private)s*)user_data)->node_number;
  cell_geom *region = &IF->cgeom;
  %(name)s_Params *p = (%(name)s_Params *)IF->params;
  %(name)s_state *sv_base = (%(name)s_state *)IF->sv_tab.y;
  %(name)s_state *sv = sv_base+__i;

''' % self.value_hash)
            out.inc()
            out("//Check to see if input is valid")
            for var in order(cvode):
                if var.is_a('lower_bound'):
                    out("if (%(var)s < %(bound)s) { return 1; }"
                        % {"var" : cvode_format(var), "bound" : var.info('lower_bound')})
                if var.is_a('upper_bound'):
                    out("if (%(var)s > %(bound)s) { return 1; }"
                        % {"var" : cvode_format(var), "bound" : var.info('upper_bound')})
            out("\n//Compute the equations")
            for var in order(specified_lookup_variables & update_inputs):
                self.printLookupTableRow(out, var, cvode_format)
            (this_func_count, this_func_vars, this_var_list) \
                = self.printEquations(out=out,
                                      eqnmap=self.root.main,
                                      valid=good_vars | update_inputs,
                                      target=cvode_targets-good_vars,
                                      lhs_format = lhs_wrapper(cvode_format),
                                      rhs_format = cvode_format,
                                      lookup=all_lookup
                                      )
            functions += this_func_count; func_vars |= this_func_vars
            out("\n\n")
            out("//Complete the update")
            for var in order(cvode):
                out("NV_Ith_S(CVODE_dot,CVODE_%s) = %s;" % (var.name, cvode_format(var.info('diff'))))
            out.dec()
            out('''
  return 0;
}
#endif
''' % self.value_hash)

        if self.root.getVars('trace'):
            out('''

void trace_%(name)s(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("%(name)s_trace_header.txt","wt");
    fprintf(theader->fd,
''' % self.value_hash)
            out.inc()
            out.inc()
            out.inc()
            out.inc()
            for var in order(self.root.getVars('trace')):
                out('\"%s\\n\"' % dont_set_modvars(var))
            out.dec()
            out(");")
            out.dec()
            out.dec()
            out.dec()
            out('''
    f_close(theader);
  }

  GlobalData_t dt = IF->dt*%(time_factor)s;
  cell_geom *region = &IF->cgeom;
  %(name)s_Params *p  = (%(name)s_Params *)IF->params;

  %(name)s_state *sv_base = (%(name)s_state *)IF->sv_tab.y;
  %(name)s_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

''' % self.value_hash)

            out.inc()
            printRegionalConstants()
            self.printExternalDef(out)

            self.printGetExternal(out)
            self.changeExternalUnits(1, out, complete_format)

            self.printEquations(out=out,
                                eqnmap=self.root.main,
                                valid=good_vars | update_inputs,
                                target=self.root.getVars('trace') - good_vars - update_inputs,
                                declared=(self.root.statevars | self.modvars) - self.root.getVars('store'),
                                lhs_format=lhs_dont,
                                rhs_format=dont_set_modvars,
                                )
            out("")
            out("//Output the desired variables")
            for var in order(self.root.getVars('trace')):
                out('fprintf(file, "%%4.12f\\t", %s);' % dont_set_modvars(var))
            self.changeExternalUnits(-1, out, complete_format)
            out.dec()
            out('''
}
''' % self.value_hash)

    def append(self, out_dir):
        File = open(os.path.join(out_dir, self.model_name+".cc"), "a+")

        File.write('''
#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        ''')
        File.close()

if __name__=='__main__':
    try:
        import val
    except ImportError as e:
        if e.args[0] == "No module named val" or "No module named 'val'":
            #running from the directory, import fake credentials.
            import fake_val as val
        else:
            raise e

    if val.isLicensed(sys.argv,'limpet_translator'):

        if len(sys.argv) < 3:
            print('Use '+sys.argv[0]+' <.model file> <path of imp_list.txt> <imp source dir>')
            print('Build openCARP (LIMPET) C++ code from EasyML .model files')
            sys.exit(1)

        filename = sys.argv[1]
        imp_list = sys.argv[2]
        out_dir  = sys.argv[3]

        external_units = parse_external_units(imp_list)

        try :
            im = IonicModel(open(filename, "r",  encoding="utf-8").read());
        except IOError :
            print('\nFile not found: '+filename+'\n')
            exit(1)

        obj = Limpet(im, filename, external_units)
        obj.printHeader(out_dir)
        obj.printSource(out_dir)
        obj.append(out_dir)

