#!/usr/bin/env python

# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

from copy import *
import math

class Unit:
    def __init__(self, system, scale, dict):
        self.system = system
        self.scale = scale
        self.unit = dict
        for base in self.system.bases:
            if base not in self.unit:
                self.unit[base] = 0

    def __mul__(self, other):
        new_unit = copy(self.unit)
        for key in other.unit.keys():
            new_unit[key] += other.unit[key]

        return Unit(self.system, 
                    self.scale+other.scale,
                    new_unit
                    )

    def __truediv__(self, other):
        new_unit = copy(self.unit)
        for key in other.unit.keys():
            new_unit[key] -= other.unit[key]
        return Unit(self.system,
                    self.scale-other.scale,
                    new_unit)

    def __div__(self, other):
        new_unit = copy(self.unit)
        for key in other.unit.keys():
            new_unit[key] -= other.unit[key]
        return Unit(self.system,
                    self.scale-other.scale,
                    new_unit)

    def __pow__(self, amount):
        new_unit = copy(self.unit)
        for key in new_unit.keys():
            new_unit[key] *= amount
        return Unit(self.system,
                    self.scale*amount,
                    new_unit)

    def __str__(self):
        return self.system.display_unit(self)

    def length(self):
        acc = 0
        for val in self.unit.values():
            acc += val**2
        return math.sqrt(acc)

    def reduce_by(self, other):
        # compute the dot product of the units.
        dot = 0
        for key in self.unit.keys():
            dot += self.unit[key]*other.unit[key]
        #              |\
        #  x = a*other | \ y = self                                  
        #              |  \                            
        #              -----
        #              z = result
        # z^2 = x^2 + y^2 - 2xy*cos(theta=angle between x,y)
        # d(z^2)/dx = 2x - 2y*cos(theta)
        # crit point (minimum) at x = y*cos(theta)
        # a = self.length()*cos(theta)/other.length()
        #   = dot/(other.length()^2)
        a = dot/(other.length()**2);
        # round a to an integer:
        amount = int(round(a))
        return (self/(other**amount), amount)

    def is_like(self, other):
        for key in list(self.unit.keys()) + list(other.unit.keys()):
            if key not in self.unit or key not in other.unit:
                return 0
        for key in self.unit.keys():
            if self.unit[key] != other.unit[key]:
                return 0
        return 1

    def __eq__(self, other):
        return self.is_like(other) and self.scale==other.scale
    def __ne__(self, other):
        return not self == other

def log_diff(a,b):
    assert a.is_like(b)
    return a.scale-b.scale

class UnitSystem:
    def __init__(self, bases):
        self.bases = bases;
        self.prefixes = {
            'y' : -24,
            'z' : -21,
            'a' : -18,
            'f' : -15,
            'p' : -12,
            'n' : -9,
            'u' : -6,
            'm' : -3,
            'c' : -2,
            'h' : 2,
            'k' : 3,
            'M' : 6,
            'G' : 9,
            'T' : 12,
            'P' : 15,
            'E' : 18,
            'Z' : 21,
            'Y' : 24,
            }
        self.units = {}
        for unit in bases:
            self.add(unit, self.unit(0, (unit, 1)))
        self.unitless = self.unit(0)

    def unit(self, scale, *args):
        dict = {}
        for tuple in args:
            dict[tuple[0]] = tuple[1]
        return Unit(self, scale, dict)

    def add(self, name, unit):
        self.units[name] = unit

    def get_unit(self, name):
        if name in self.units:
            return copy(self.units[name])
        elif name[0] in self.prefixes.keys() and name[1:] in self.units:
            return self.units[name[1:]]*self.unit(self.prefixes[name[0]])
        else:
            assert(False)
            return 0

    def __getattr__(self, name):
        unit = self.get_unit(name)
        if unit:
            return unit
        else:
            raise AttributeError(name)

    def display_unit(self, unit):
        # ok, see if we can clean up the display names.
        pos = []
        neg = []
        remaining_unit = unit
        while remaining_unit.length() > 0:
            min = remaining_unit
            min_name = None
            min_amount = 0
            for unit_name in self.units.keys():
                (new_unit, amount) = remaining_unit.reduce_by(self.units[unit_name])
                if (new_unit.length() < min.length()):
                    min_name = unit_name
                    min = new_unit
                    min_amount = amount
            remaining_unit = min
            if min_amount > 0:
                pos.append((min_name, min_amount))
            else:
                neg.append((min_name, -min_amount))

        ret = "10^%d * " % remaining_unit.scale
        if len(neg) == 1:
            neg_format = "%s"
        else:
            neg_format = "(%s)"
        if not len(pos) and not len(neg):
            ret += "unitless"
        elif not len(pos) and len(neg):
            ret += ("1/"+neg_format) % self.format(neg)
        elif len(pos) and not len(neg):
            ret += self.format(pos)
        else:
            ret += ("%s/"+neg_format) % (self.format(pos), self.format(neg))
        return ret

    def format(self, list):
        ret = []
        for item in list:
            amount = item[1]
            if amount == 1:
                ret.append(item[0])
            else:
                ret.append("%s^%d" % (item[0], amount))
        return "*".join(ret)

class Si(UnitSystem):
    def __init__(self):
        UnitSystem.__init__(self, ['mol', 'm', 's', 'A', 'g', 'cd', 'K'])

        self.add('L', self.m**3 *self.unit(-3))
        self.add('M', self.mol / self.L)
        self.add('C', self.s*self.A)
        self.add('V', self.m**2 * self.kg / (self.s**3 * self.A))
        self.add('ohm', self.V/self.A)
        self.add('S', self.unit(0)/self.ohm)
        self.add('F', self.C/self.V)
        self.add('N', self.m*self.kg/(self.s**2))
        self.add('J', self.N*self.m)
        self.add('Pa', self.N/self.m**2)
        self.add('W', self.J/self.s)


si = Si()

if __name__=='__main__':
    print(si.mV/si.ms)
    print(si.uA/si.uF)
    print(si.uA/si.uA)
    print(si.uS*si.ms)
    print((si.V/si.s).is_like(si.uA/si.uF))
    print(si.mol/si.cm)
