#!/usr/bin/python

# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

from im import IonicModel
from basic import Indenter
from StringIO import StringIO
from sys import version_info
if version_info >= (2,4,0):
    Set = set
else:
    from sets import Set
from scipy import weave
from scipy.integrate import odeint
from scipy import ndarray
from array import array
import scipy as S
import numpy as N
import re


class VarModel:
    """
    Keeps track of the state of a single variable.  Holds all the associated
    traces, whether or not the varaible is being used, and so on.
    """
    def __init__(self, name="", trace=0):
        self.gen_trace = 0
        self.held_trace = 0
        self.used = False
        self.change_name(name)
        if trace:
            self.set_held_trace(trace)

    def generated_trace(self, trace):
        self.gen_trace = trace
    def discard_gen(self):
        self.gen_trace = 0
    def save_gen(self):
        self.held_trace = self.gen_trace
        self.gen_trace = 0
    def use(self, switch=True):
        assert not self.gen_trace and self.held_trace
        self.used = switch
    def set_held_trace(self, trace):
        self.held_trace = trace
    def get_held_trace(self):
        return self.held_trace
    def change_name(self, string):
        if not string:
            self.name = "noname"
        else:
            self.name = string
    def state(self):
        result = ""
        if not self.gen_trace:
            if not self.held_trace:
                result = "empty"
            elif self.held_trace and self.used:
                result = "used"
            elif self.held_trace and not self.used:
                result = "held"
            else:
                assert 0
        else:
            if not self.held_trace:
                result = "gen"
            elif self.held_trace:
                result = "compare"
        return result
    def has_held(self):
        return self.state() != "gen" and self.state() != "empty"

class Trace:
    """
    Stores a function y_i(t_i) from tabular data.  Lets you get y(t) for any t
    using linear interpolation.
    """
    def __init__(self, y, t):
        self.t = t
        self.y = y

    def lookup(self, new_time):
        y = self.y
        t = self.t
        i = N.searchsorted(t, new_time) - 1
        if i == -1:
            result = y[0]
        elif i == len(self.t)-1:
            result = y[i]
        else:
            result = (y[i]*(t[i+1]-new_time)+ y[i+1]*(new_time-t[i]))/(t[i+1]-t[i])
        return float(result)

    def tstart(self):
        return self.t[0]

    def tstop(self):
        return self.t[len(self.t)-1]

    def dump(self):
        for ii in xrange(0, len(self.t)):
            print "%g %g" % (self.t[ii], self.y[ii])
    

class SimulationModel:
    """
    Class to hold the data model for my GUI program to display and
    work on ionic models.
    """
    def __init__(self):
        self.im_filename = 0
        self.var_list = []
        self.tstart = 0
        self.tstop = 0

    def set_ionic_model(self, filename):
        self.im_filename = filename

    def generate(self):
        #find all variables that are used.
        #find all variables that are not used
        input_vars = {}
        init_vars = {}
        output_names = []
        myvars = self.get_var_hash()
        for name in myvars.keys():
            if myvars[name].state() == "used":
                input_vars[name] = myvars[name].get_held_trace()
            else:
                myvars[name].discard_gen()
                output_names.append(name)
            init_vars[name] = myvars[name].get_held_trace()

        #do the solve
        out_traces = solve(IonicModel(open(self.im_filename, "r").read()), 
                           input_vars, output_names, self.tstart, self.tstop, init_vars)
        #put the results back into the variables.
        for name in out_traces.keys():
            myvars[name].generated_trace(out_traces[name])
        
    def read_text_file(self, file):
        file = open(file, "r")
        line = file.readline().strip()
        column_headers = re.split(r'\s+', line)
        columns = {}
        for header in column_headers:
            columns[header] = array('d')
        assert columns.has_key("t")
        while 1:
            line = file.readline().strip()
            if not line:
                break
            values = [float(v) for v in re.split(r'\s+', line)]
            if len(values) != len(column_headers):
                assert "# of columns needs to be the same throughout the file"
            for ii in xrange(0,len(column_headers)):
                columns[column_headers[ii]].append(values[ii])
        for name in columns.keys():
            if name == "t":
                continue
            self.add_var(VarModel(name, Trace(columns[name], columns['t'])))

    def get_var_hash(self):
        result = {}
        for var in self.var_list:
            result[var.name] = var
        return result

    def add_var(self, var_model):
        if (var_model.has_held()
            and (var_model.get_held_trace().tstart() != self.tstart 
                 or
                 var_model.get_held_trace().tstop() != self.tstop
                 )):
            self.var_list = []
            self.tstart = var_model.get_held_trace().tstart()
            self.tstop = var_model.get_held_trace().tstop()
        self.var_list.append(var_model)

def is_float(string):
    """
    Returns true if a string is a floating point number.
    """
    result = 0
    try:
        float(string)
    except ValueError:
        result = 1
    return result

def solve(im, used, output_names, tstart, tstop, init={}):
    '''
    Solves an ionic model.  Returns a dictionary of traces.
    '''
    # in_dynamic - hash of name -> traces
    # in_constant - hash of name -> values
    # initial_conditions - hash of name -> values
    # output_names - list of names of things we want to output.

    #find out which of the input names are actually variables.
    valid = find_vars(im, used.keys())
    restrictions = {}
    for var in valid:
        restrictions[var] = used[var.name]

    init_restrictions = restrictions.copy()
    for var in find_vars(im, init.keys()):
        init_restrictions[var] = init[var.name]

    # figure out all the differential variables.
    diff_in = to_list(im.getVars('diff') - valid)
    diff_out = [var.info('diff') for var in diff_in]

    t = S.linspace(tstart,tstop,max(5000,int(10*(tstop-tstart))))
    print max(5000,int(10*(tstop-tstart)))

    # generate the initialization code
    func = IonicFunction(im.init_system().main, [], diff_in, init_restrictions)
    init = func(ndarray((0)), t[0])

    # generate the differential update code
    func = IonicFunction(im.main, diff_in, diff_out, restrictions)
    #solve the differential system
    y = odeint(func, init, t)

    # generate the trace variable code
    targets = to_list(find_vars(im, output_names))
    func = IonicFunction(im.main, diff_in, targets, restrictions)

    #return all the relevant traces.
    return func.unpack_output(y, t)


def find_vars(im, var_names):
    results = Set()
    for var_name in var_names:
        if im.table.has_key(var_name):
            results.add(im.table[var_name])
    return results

def reverse_list_hash(list):
    results = {}
    for ii in xrange(0,len(list)):
        results[list[ii]] = ii
    return results

def to_list(input):
    return [item for item in input]

class IonicFunction:
    def __init__(self, eqnmap, input, output, restrictions):
        self.input = reverse_list_hash(input)
        self.output = reverse_list_hash(output)
        self.restrictions = restrictions
        #the restrictions will be handled in the calling function
        out = Indenter(StringIO())
        
        # read in the input
        for ii in xrange(0,len(input)):
            var = input[ii]
            out("double %s = INPUT1(%d);" % (var.name, ii))

        #print the model run code.
        valid = Set(input + restrictions.keys())
        target = Set(output) - valid
        def basic(var):
            return var.name
        for var in eqnmap.getList(target, valid):
            eqn = eqnmap[var]
            out("double %s = %s;" % (basic(var), eqn.formatRHS(basic)))
            
        #put the output back into an array.
        #out("py::list items(%d);" % len(output))
        #for ii in xrange(0,len(output)):
        #    out("{py::object obj(%s); items.set_item(%d, obj);}" 
        #        % (output[ii].name, ii))
        #out("return_val = items;")
        for ii in xrange(0,len(output)):
            out("OUTPUT1(%d) = %s;" 
                % (ii, output[ii].name))
        
        self.code = out.file.getvalue()

    def __call__(self, y, t):
        dict = {}
        dict['t'] = t
        dict['input'] = y
        dict['output'] = ndarray(len(self.output))
        for var in self.restrictions.keys():
            dict[var.name] = self.restrictions[var].lookup(t)
        weave.inline(self.code,dict.keys(),local_dict=dict)
        return dict['output']
        
    def unpack_output(self, input_array, t):
        result = {}
        for var in self.output.keys():
            result[var.name] = N.empty_like(t)
        for ii  in xrange(0,len(t)):
            out = self(input_array[ii,:], t[ii])
            for var in self.output.keys():
                result[var.name][ii] = out[self.output[var]]
        for name in result.keys():
            result[name] = Trace(result[name], t)
        
        return result

if __name__=="__main__":
    import sys
    sim = SimulationModel()
    sim.set_ionic_model("converted_UCLA_RAB.model")
    sim.read_text_file("blah.txt")
    hash = sim.get_var_hash()
    hash["V"].use(True)
    hash["m"].use(True)
    sim.generate()
    hash['h'].gen_trace.dump()

