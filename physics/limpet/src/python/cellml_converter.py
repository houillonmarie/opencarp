#!/usr/bin/env python3

# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

from   xml.etree import ElementTree as ET
import re
from   copy import copy
import argparse
import sys,os.path

cellml='{http://www.cellml.org/cellml/1.0#}'
mathml='{http://www.w3.org/1998/Math/MathML}'

ops = {"times" : "*",
       "divide" : "/",
       "plus" : "+",
       "minus" : " - ",
       "lt" : "<",
       "gt" : ">",
       "leq" : "<=",
       "geq" : ">=",
       "eq" : "==",
       "and" : " and ",
       "or" : " or ",
       "neq" : "!="
       }
needs_parens = set(['plus', 'or', 'divide', 'times','minus'])

functions = {'exp' : 'exp',
             'ln' : 'log',
             'log' : 'log10',
             'abs' : 'fabs',
             'sin' : 'sin',
             'cos' : 'cos',
             'tan' : 'tan',
             'seq' : 'seq',
             'csc' : 'csc',
             'cot' : 'cot',
             'sinh' : 'sinh',
             'cosh' : 'cosh',
             'tanh' : 'tanh',
             'sech' : 'sech',
             'csch' : 'csch',
             'coth' : 'coth',
             'arcsin' : 'asin',
             'arccos' : 'acos',
             'atan' : 'atan',
             'arccosh' : 'arccosh',
             'arccot' : 'arccot',
             'arccoth' : 'arccoth',
             'arccsc' : 'arccsc',
             'arccsch' : 'arccsch',
             'arcsec' : 'arcsec',
             'arcsech' : 'arcsech',
             'arcsinh' : 'arcsinh',
             'arctanh' : 'arctanh',
             'msup' : 'pow',
             'power' : 'pow',
             'msqrt' : 'sqrt',
             'mroot' : 'sqrt',
             'root' : 'sqrt',
             'floor' : 'floor',
             'ceil' : 'ceil'
             }

unit_desc = {
        'metre' : {"m":1},
        'meter' : {"m":1},
        'volt' : {"V":1},
        'siemens' : {"S":1},
        'second' : {"s":1},
        'farad' : {"F":1},
        'ampere' : {"A":1},
        'mole' : {"mol":1},
        'litre' : {"L":1},
        'liter' : {"L":1},
        'joule' : {"J":1},
        'kelvin' : {"K":1},
        'newton' : {"N":1},
        'coulomb' : {"C":1},
        'dimensionless' : {"unitless":1},
}

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

prefixes = {
        'yotta' : "Y",
        'zetta' : "Z",
        'exa' : "E",
        'peta' : "P",
        'tera' : "T",
        'giga' : "G",
        'mega' : "M",
        'kilo' : "k",
        'hecto' : "h",
        'deci' : "d",
        'centi' : "c",
        'milli' : "m",
        'micro' : "u",
        'nano' : "n",
        'pico' : 'p',
        'femto' : "f",
        'atto' : "a",
        'zepto': "z",
        'yocto': "y",
	'-3'   : "m",
	'-6'   : "u",
        '-9'   : "n",
        '-12'  : "p",
        '3'    : 'k',
        '6'    : 'M',
}


def unit_to_text(unit):
    a = []
    for key in unit:
        if unit[key] == 1:
            a.append(key)
        elif unit[key] > 1:
            a.append("%s^%d" % (key,unit[key]))
    if a:
        unit_text = "*".join(a)
    else:
        unit_text = "unitless"
    for key in unit:
        if unit[key] == -1:
            unit_text += "/"+key
        elif unit[key] < -1:
            unit_text += "/%s^%d" % (key,-unit[key])
    return unit_text


def repeat(s, times):
    return [s for i in range(0,times)]

def make_int(s):
    trial=s
    if trial[-1] == ".":
        trial = trial[0:-1]
    return int(trial)

def is_int(s):
    try:
        make_int(s)
        return True
    except ValueError:
        return False

def is_tag(tag, element):
    return re.search(r"\}%s$" % tag, element.tag)

def process_equation(element):
    assert is_tag("apply", element)
    children = copy(element.getchildren())
    op = children.pop(0)
    processed = [process_mathml(e) for e in children]
    assert is_tag("eq", op)
    return processed

def unique_name(name, namelist, component='cp'):
    new_name = name+"_"+component
    assert new_name not in namelist 
    return new_name

# convert local component names to global names
# replacements is a dictionary of local to global names
def globalize_diff_var( old, new, text ):
    var = re.sub(r"(diff_)%s(\W|$)"%old, r"\1%s\2"%new, text )
    var = re.sub(r"(\W|^)%s(\W|$)"%old, r"\1%s\2"%new, var )
    return var

def globalize( text, replacements ) :
    for rep in replacements :
        while re.search(r"(\W|^)%s(\W|$)"%rep, text ):
            text = re.sub(r"(\W|^)%s(\W|$)"%rep, r"\1%s\2"%replacements[rep], text )
    return text

constants = {}

def process_mathml(element):
    if is_tag("ci", element):
        return element.text.strip()
    elif is_tag("cn", element):
        constant=element.text.strip()
        if not re.search("\.", constant):
            constant += "."
        if element.get('type') == "e-notation":
            a = element.find(mathml+'sep')
            if a != None:
                constant += 'e'+a.tail.strip()
        return constant
    elif is_tag("piece", element):
        return [process_mathml(e) for e in element.getchildren()]
    elif is_tag("otherwise", element):
        return [process_mathml(e) for e in element.getchildren()]
    elif is_tag("piecewise", element):
        ret = ""
        indent = 0
        processed = [process_mathml(e) for e in element.getchildren()]
        for child in processed:
            if len(child) > 1:
                indent += 1
                ret += "((%s) ? %s : " % (child[1], child[0])
            else:
                ret += child[0] + ")"*indent
                indent=0
        return ret
    elif is_tag("apply", element):

        children = copy(element.getchildren())
        op = children.pop(0)
        processed = [process_mathml(e) for e in children]

        if 0:
            pass
        elif is_tag("diff", op):
            return "diff_"+processed[1]
        elif is_tag("minus",op) and len(processed)==1:
            return "-"+processed[0]
        elif is_tag("not",op):
            return "!("+processed[0]+")"
        elif is_tag("power",op) and is_int(processed[1]):
            power = make_int(processed[1])
            if power == 0:
                return "1"
            elif power > 0:
                return "("+"*".join(repeat(processed[0],power))+")"
            else:
                return "(1/"+"/".join(repeat(processed[0],-power))+")"
        else:
            for key in list(functions.keys()):
                if is_tag(key, op):
                    return functions[key]+"("+",".join(processed)+")"
            else:
                for key in list(ops.keys()):
                    if is_tag(key, op):
                        ret = ops[key].join(processed)
                        if key in needs_parens:
                            ret = "(%s)" % ret
                        return ret
                else:
                    return "("+op.tag+":"+",".join(processed)+")"
    elif is_tag("pi", element):
        constants["pi"] = "3.141592653589793238462643383279502884";
        return "pi"
    else:
        return str(element.tag)

# determine the units in the model
def solve_units( root ):
    global unit_desc
    unsolved_units = set(root.findall(cellml+'units'))
    prefixed_units = set()
    while unsolved_units:
        solved_units = set()
        for unitxml in unsolved_units:
            name = unitxml.get('name')
            ready_to_define = True
            for subxml in unitxml.getchildren():
                ready_to_define &= subxml.get('units') in unit_desc
            if ready_to_define:
                unit = {}
                for subxml in unitxml.getchildren():
                    subunit = copy(unit_desc[subxml.get('units')])
                    if subxml.get('prefix'):
                        prefix = prefixes[subxml.get('prefix')]
                        #find one of the keys with a positive exponent
                        prefix_canidates = [key for key in subunit if subunit[key] == 1 and key not in prefixed_units]
                        if prefix_canidates:
                            del subunit[prefix_canidates[0]]
                            new_unit = prefix+prefix_canidates[0]
                            prefixed_units.add(new_unit)
                            subunit[new_unit] = 1
                        else:
                            new_unit = prefix+"unitless"
                            prefixed_units.add(new_unit)
                            subunit[new_unit] = 1
                    exponent = 1
                    if subxml.get('exponent'):
                        exponent = int(subxml.get('exponent'))
                    #find one of the keys with a positive exponent.
                    for key in list(subunit.keys()):
                        if key not in unit:
                            unit[key] = 0
                        unit[key] += exponent*subunit[key]
                unit_desc[name] = unit
                solved_units.add(unitxml)
        unsolved_units -= solved_units
        
#initial common header
def write_header( outf, Vm, filename ) :
    outf.write( '#generated from: '+os.path.basename(filename)+'\n' )
    outf.write( Vm+'; .external(Vm); .nodal();\n' )
    outf.write( 'Iion; .external(); .nodal();\n' )
    outf.write( '\n' )


# the differential equation for Vm is the ionic current
def replace_diff_Vm( eqns, Vm, Istim, varlist ) :
    i_found = False
    v_found = False

    #remove references to Istim
    for ceqns in eqns :
        for var in eqns[ceqns] :
            if re.search(r'(\W)%s(\W|$)'%Istim, eqns[ceqns][var] ):
                 a = re.sub(r'(\W)%s(\W|$)'%Istim, r'\g<1>0\2', eqns[ceqns][var] )
                 eqns[ceqns][var] = a
                 i_found = True
                 sys.stderr.write( 'ATTN: stimulus current ('+color.BOLD+Istim+color.END+') removed from '+var+'\n' )
    if not i_found :
        sys.stderr.write( 'WARNING: stimulus current not found\n' )

    # remove Istim update
    for ceqns in eqns :
        if Istim in eqns[ceqns] :
            del eqns[ceqns][Istim]
    
    # replace diff_Vm with Iion
    for ceqns in eqns :
        if 'diff_'+Vm in eqns[ceqns] :
            rhs = '-('+eqns[ceqns].pop('diff_'+Vm)+')'
            eqns[ceqns]['Iion'] = rhs
            v_found = True
            var_list[ceqns].remove('diff_'+Vm)
            break
    if not v_found :
        sys.stderr.write( 'WARNING: transmembrane voltage not found\n' )
    




# remove redundant differential equations, eg, if alpha_ and beta_ are defined,
# the diff_ is automatically generated and need not be specified
# gvars: global name list
# eqns: equations
def rm_redundant_diff( gvars, eqns, vl, justwarn ) :
    for gname in gvars :
        for cname in eqns :
            if gname in vl[cname] :
                found_ab = 'a_'+gname in vl[cname] or 'alpha_'+gname in vl[cname] 
                found_ab = found_ab and ('b_'+gname in vl[cname] or 'beta_'+gname in vl[cname] )
                found_ti = 'tau_'+gname in vl[cname] 
                found_ti = found_ti and ( gname+'_inf' in vl[cname] or gname+'_infinity' in vl[cname] )
                found_diff = 'diff_'+gname in vl[cname]
                if found_diff and (found_ab or found_ti) :
                    sys.stderr.write('ATTN: For '+color.BOLD+gname+color.END+' in '+color.UNDERLINE+cname+color.END+' found diff'+
                            ('+alpha+beta' if found_ab else '') +
                            ('+tau+inf'    if found_ti else '') )
                    if justwarn :
                        sys.stderr.write('.\n')
                    else :
                        sys.stderr.write(' -> Removing diff: '+ color.BOLD+eqns[cname]['diff_'+gname]+color.END+'\n')
                        del eqns[cname]['diff_'+gname]
                elif found_ab and found_ti:
                    sys.stderr.write('ATTN: For '+color.BOLD+gname+color.END+' in '+color.UNDERLINE+cname+color.END+
                                   ' found alpha+beta+tau+infinity\n')

# add trace for all variables
def trace( out, varlist ) :
    out.write('\ngroup{\n')
    for cn in varlist :
        for v in varlist[cn] :
            if not v.startswith('diff_' ) :out.write( '\t'+v+';\n')
    out.write('}.trace();\n')


# set default integration method for non-gate variables
def default_int_method( out, method, varlist ) :
    out.write('group {\n')
    for cn in varlist :
        for v in varlist[cn] :
            if v.startswith('diff_' ) : 
                out.write( '\t'+re.sub('diff_','',v)+';\n' )
    out.write('}}.method({});\n'.format(method))

            
# output all constants as parameters
def print_params( out, pars ) :
    out.write('\ngroup{\n')
    for p in pars :
        out.write( '\t'+p+';\n')
    out.write('}.param();\n')


if __name__=="__main__":
    
    parser = argparse.ArgumentParser(description='Convert CellML 1.0 to model format')
    parser.add_argument('--Vm', default='V',  help='transmembrane voltage variable [%(default)s]' )
    parser.add_argument('--Istim', default='i_Stim',  help='stimulus current variable [%(default)s]' )
    parser.add_argument('--out',   help='output file' )
    parser.add_argument('--trace-all', action='store_true', help='trace all variables')
    parser.add_argument('--int-method', default='cvode', 
            help='default integration method for non-gate vars [%(default)s]')
    parser.add_argument('--plugin', action='store_true', help='is a plug-in, not a whole cell model')
    parser.add_argument('--params', action='store_true', help='parameterize all constants')
    parser.add_argument('--keep-redundant', action='store_true', 
            help='keep redundant differential expressions, eg alpha, beta and diff)' )
    parser.add_argument('CMLfile', help='CellML file' )
    opts = parser.parse_args()

    root = ET.parse(opts.CMLfile)
    solve_units( root )

    outf = open( opts.out, 'w' ) if opts.out else sys.stdout
    write_header( outf,  opts.Vm, opts.CMLfile )
    
    # render all the differential equations assigning unque global names
    var_eqns    = {}     # equations in the model by component, global names
    var_list    = {}     # global differential variable list by component
    var_map     = {}     # var_map[component_name][local_name] = global_name
    global_vars = set()  # global list of variable names
    
    for component in root.findall(cellml+'component'):
        
        compname = component.attrib['name']

        # make local lists since names may not be unique
        var_list[compname] = []
        var_eqns[compname] = {}
        var_map[compname]  = {}
        assigned_vars = set()   # must be local to component

        # make set of all variables assigned to in component
        for math in component.findall(mathml+'math/'+mathml+'apply'):
            eqn = process_equation(math)
            diffv = eqn[0].startswith('diff_') 
            var = eqn[0][5 if diffv else 0 :]
            assigned_vars.add(var)

        # build dictionary of component to global names
        for variable in component.findall(cellml+'variable'):
            lvar = variable.get('name')
            #print compname, lvar, 1 if variable.get('initial_value') else 0
            if variable.get('initial_value') or lvar in assigned_vars:
                if lvar in global_vars :
                    new_const =  unique_name( lvar, global_vars, compname )
                    global_vars.add(new_const)
                    var_map[compname][lvar] = new_const
                    sys.stderr.write( 'ATTN: In '+compname+', '+color.BOLD+lvar+color.END+' -> '+
                                         color.BOLD+new_const+color.END+'\n')
                else :
                    global_vars.add(lvar)
        
        # find all the equations and map local to global names
        for math in component.findall(mathml+'math/'+mathml+'apply'):
            eqn = process_equation(math)
            diffv = eqn[0].startswith('diff_') 
            var = eqn[0][5 if diffv else 0 :]
            if var in var_map[compname] :
                cvar = globalize_diff_var(var, var_map[compname][var], eqn[0])
            else :
                cvar = eqn[0]
            var_list[compname].append(cvar)
            if diffv : var_list[compname].append(cvar[5:])
            equation =  globalize( eqn[1], var_map[compname] )
            if cvar == equation :    #yes, this can happen 
                sys.stderr.write( 'ATTN: ignoring identity assignment for '+color.BOLD+str(cvar)+color.END+'\n' )
            else:
                var_eqns[compname][cvar] = equation

    # find the units of every variable, including time
    var_unit = {}
    for component in root.findall(cellml+'component'):
        compmap = var_map[component.attrib['name']]
        for variable in component.findall(cellml+'variable'):
            cname = variable.get('name')
            gname = compmap.get(cname,cname)
            var_unit[gname] = unit_desc[variable.get('units')]
        
    # find all the differential variables and figure out units
    for component in root.findall(cellml+'component'):
        compmap  = var_map[component.attrib['name']]
        compname = component.attrib['name']
        for var in var_eqns[compname] :
            if var.startswith('diff_') :
                unit = copy(var_unit[var[5:]])
                for key in var_unit['time']:
                    if key not in unit:
                        unit[key] = 0
                    unit[key] -= var_unit['time'][key]    
                var_unit[var] = unit
    var_unit['Iion'] = { 'uA': 1, 'uF': -1 } #this should really be determined dynamically

    # go through all the initial variables and determine
    # if the var is a constant or an initial condition
    init_vals  = []
    params = {}
    for component in root.findall(cellml+'component'):
        compname = component.attrib['name']
        compmap = var_map[compname]
        for variable in component.findall(cellml+'variable'):
            if variable.get('initial_value'):
                cname = variable.get('name')
                gname = compmap.get(cname,cname)
                if 'diff_'+gname in var_eqns[compname] :
                    init_vals.append("%s = %s; .units(%s);" % (gname+'_init', 
                                            variable.get('initial_value'),
                                            unit_to_text(var_unit[gname])))
                else:
                    params[gname] = "%s; .units(%s);" % (variable.get('initial_value'),
                                            unit_to_text(var_unit[gname]))

    outf.write("# Constants\n")
    for variable in list(params.keys()):
        outf.write( "%s = %s\n" % (variable, params[variable]) )

    outf.write("# Constants\n")
    for variable in list(constants.keys()):
        outf.write( "%s = %s; .units(unitless);\n" % (variable, constants[variable]) )

    outf.write( "\n# Initial values\n" )
    for initval in init_vals:
        outf.write( initval+'\n' )

    # print all the equations
    replace_diff_Vm( var_eqns, opts.Vm, opts.Istim, var_list )
    rm_redundant_diff( global_vars, var_eqns, var_list, opts.keep_redundant )
    for component in root.findall(cellml+'component'):
        compname = component.attrib['name']
        compeqns = var_eqns[compname]
        if len(compeqns) : outf.write("\n#"+compname+'\n')
        for var in compeqns :
            outf.write( "%s %s= %s; .units(%s);\n" % (var, 
                        '+' if opts.plugin and var == 'Iion' else '',
                           compeqns[var], unit_to_text(var_unit[var])) )
    outf.write('\n')

    if opts.int_method is not None: default_int_method( outf, opts.int_method, var_list )  
    if opts.params: print_params( outf, params )
    if opts.trace_all: trace( outf, var_list )
