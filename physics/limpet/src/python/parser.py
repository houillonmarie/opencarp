#!/usr/bin/env python3
# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

import ply.lex as lex
import ply.yacc as yacc
import sys
import os
import unit
import im
import re
from sys import version_info
if version_info >= (2,4,0):
    Set = set
else:
    from sets import Set

# This object is a stand-in for a logging object created by the 
# logging module.   PLY will use this by default to create things
# such as the parser.out file.  If a user wants more detailed
# information, they can create their own logging object and pass
# it into PLY.

class MyLogger(object):
    def __init__(self,f):
        self.f = f
        self.warnings = []
    def debug(self,msg,*args,**kwargs):
        pass
    info     = debug

    def warning(self,msg,*args,**kwargs):
        self.warnings.append("WARNING: "+ (msg % args) + "\n")

    def error(self,msg,*args,**kwargs):
        self.f.write("ERROR: " + (msg % args) + "\n")
    critical = debug


class Parser:
    """
    Base class for a lexer/parser that has the rules defined as methods
    """
    tokens = ()
    precedence = ()

    def __init__(self, start_symbol=None, **kw):
        self.debug = kw.get('debug', 0)
        self.names = { }
        try:
            modname = os.path.split(os.path.splitext(__file__)[0])[1] + "_" + self.__class__.__name__
        except:
            modname = "parser"+"_"+self.__class__.__name__
        self.debugfile = modname + ".dbg"
        self.tabmodule = modname + "_" + "parsetab"
        #print self.debugfile, self.tabmodule

        # Build the lexer and parser
        self.lexer = lex.lex(module=self, debug=self.debug)
        kwargs = {}
        if float(lex.__version__) >= 3.0:            
            kwargs['errorlog'] = MyLogger(sys.stderr)
        if start_symbol:
            self.parser = yacc.yacc(module=self,
                                    debug=self.debug,
                                    debugfile=self.debugfile,
                                    tabmodule=self.tabmodule,
                                    write_tables=0,
                                    start=start_symbol,
                                    **kwargs
                                    )
        else:
            self.parser = yacc.yacc(module=self,
                                    debug=self.debug,
                                    write_tables=0,
                                    debugfile=self.debugfile,
                                    tabmodule=self.tabmodule,
                                    **kwargs
                                    )
            
    t_ignore = " \t\r"

    def parse(self, s):
        return self.parser.parse(s)

class Scope:
    def __init__(self, parent):
        self.parent = parent
        self.vars = Set()
        self.eqn_map = im.EqnMap()
    def mention_variable(self, var):
        self.vars.add(var)
    def add_equation(self, eqn):
        if eqn.lhs in self.eqn_map:
            print("duplicate definition of variable",eqn.lhs)
            assert False
        self.replace_equation(eqn)
    def replace_equation(self, eqn):
        self.eqn_map.add(eqn)
        self.vars.add(eqn.lhs)
    def override(self, scope):
        self.vars |= scope.vars;
        self.eqn_map.override(scope.eqn_map)
    def mentioned(self):
        return self.vars | Set(list(self.eqn_map.keys()))
    def immediate(self):
        return list(self.eqn_map.keys())
    def get_expr(self, var):
        if var in list(self.eqn_map.keys()):
            return self.eqn_map[var].rhs
        elif self.parent != None:
            return self.parent.get_expr(var)
        else:
            assert(0 and var)
    def has_expr(self, var):
        if var in list(self.eqn_map.keys()):
            return True
        elif self.parent != None:
            return self.parent.get_expr(var)
        else:
            return False

class Lookup:
    def __init__(self, name, lb, ub, step):
        self.name = name
        self.lb = lb
        self.ub = ub
        self.step = step

class Eqn_Parser(Parser):
    def __init__(self, table, start_symbol=None):
        self.table = table
        for key in list(self.token_map.keys()):
            setattr(self, "t_"+self.token_map[key], key)
        Parser.__init__(self, start_symbol)
        self.main = Scope(None)
        self.scope_stack = [self.main]
        self.lookup = []

    def current_scope(self):
        return self.scope_stack[-1]

    keywords = {
        "and",
        "or",
        "not",
        "true",
        "false",
        "unitless",
        "group",
        "if",
        "elif",
        "else",
        }
    
    functions = {
        "expm1",
        "exp",
        "sqrt",
        "log",
        "pow",
        "acos",
        "cos",
        "atan2",
        "fabs",
        "log10",
        "min",
        "max",
        "sign",
        "heav",
        "acosh",
        "asinh",
        "atanh",
        "cosh",
        "ctanh",
        "sinh",
        "tanh",
        "square",
        "cube"
        }

    # functions without arguments
    functions_na = {
        'rand01'
        }

    literals = "()+-*/|;,{}<>?:^"
    token_map = {
        "==" : "EQUALTO",
        ">=" : "GTEQUAL",
        "<=" : "LTEQUAL",
        "=" : "ASSIGN",
        "!=" : "NOTEQUAL",
        "\+=" : "PLUSEQUAL",
        "-=" : "MINUSEQUAL",
        "\*=" : "TIMESEQUAL",
        "/=" : "DIVIDEEQUAL",
        "\.units" : "UNITS",
        "\.param" : "PARAM",
        "\.external" : "EXTERNAL",
        "\.nodal" : "NODAL",
        "\.lookup" : "LOOKUP",
        "\.regional" : "REGIONAL",
        "\.method" : "METHOD",
        "\.store" : "STORE",
        "\.trace" : "TRACE",
        "\.flag" : "FLAG",
        "\.lb" : "LB",
        "\.ub" : "UB",
        "/" : "DIVIDE",
        "\.reference" : "REFERENCE",
        }

    tokens = [
        'NAME', 'FUNCTION', 'NUMBER', 'FUNCTION_NA'
        ] + [k.upper() for k in keywords] + list(token_map.values())
    # Tokens

    def t_NUMBER(self, t):
        r'((\d+(\.\d*)?)|(\.\d+))((e|E)(\+|-)?\d+)?'
        return t

    def t_NAME(self, t):
        r'[a-zA-Z_][a-zA-Z0-9_]*'
        if t.value in self.keywords:
            t.type = t.value.upper()
        if t.value in self.functions:
            t.type = "FUNCTION"
        if t.value in self.functions_na:
            t.type = "FUNCTION_NA"
        return t

    def t_c_comment(self, t):
        r'/\*(.|\n)*?\*/'
        t.lexer.lineno += t.value.count('\n')

    def t_eol_comment(self, t):
        r'((//)|(\#))[^\n]*\n'
        t.lexer.lineno += 1

    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += t.value.count("\n")
    
    def t_error(self, t):
        print(("Illegal character '%s' on line %d" % (t.value[0], t.lexer.lineno)))
        t.lexer.skip(1)


    # Parsing rules

    precedence = (
        ('left',','),
        ('left','OR'),
        ('left','AND'),
        ('left','+','-'),
        ('left','*','DIVIDE'),
        ('left','|'),
        ('right','UMINUS'),
        ('right','NOT'),
        ('left', '^'),
        )

    #################
    def p_all(self, p):
        'all : statement_list'
        p[0] = self.main

    ###################################
    def p_unit_name(self, p):
        'unit : NAME'
        p[0] = getattr(unit.si, p[1])
    def p_unit_unitless(self, p):
        'unit : UNITLESS'
        p[0] = unit.si.unitless
    def p_unit_paren(self, p):
        "unit : '(' unit ')'"
        p[0] = p[2]
    def p_unit_binop(self, p):
        """
        unit : unit '*' unit
             | unit DIVIDE unit
        """
        if p[2] == '*': p[0] = p[1] * p[3]
        elif p[2] == '/': p[0] = p[1] / p[3]
        elif p[2] == '|': p[0] = int(p[1]) | int(p[3])
    def p_unit_exp(self, p):
        "unit : unit '^' signed_number"
        p[0] = p[1]**(int(p[3]))
        

    ##########################################
    def p_signed_number_nosign(self, p):
        '''signed_number : NUMBER'''
        p[0] = p[1]
    def p_signed_number_signed(self, p):
        '''signed_number : '-' NUMBER
                         | '+' NUMBER
        '''
        p[0] = p[1] + p[2]

    ##################################
    def p_expr_binop(self, p):
        '''
        expr : expr '+' expr
             | expr '-' expr
             | expr '*' expr
             | expr DIVIDE expr
             | expr '|' expr
        '''
        if p[2] == '+': p[0] = AST('.+.', p[1], p[3])
        elif p[2] == '-': p[0] = AST(".-.", p[1], p[3])
        elif p[2] == '*': p[0] = AST(".*.", p[1], p[3])
        elif p[2] == '/': p[0] = AST("./.", p[1], p[3])
        elif p[2] == '|': p[0] = AST(".|.", p[1], p[3])
        
    def p_expr_uminus(self, p):
        'expr : "-" expr %prec UMINUS'
        p[0] = AST('-.', p[2])
    def p_expr_paren(self, p):
        "expr : '(' expr ')'"
        #p[0] = AST('paren', p[2])
        p[0] = p[2]
    def p_expr_number(self, p):
        "expr : NUMBER"
        p[0] = AST('const', p[1])
    def p_expr_var(self, p):
        "expr : var"
        p[0] = AST('var', p[1])
    def p_expr_function(self, p):
        "expr : FUNCTION '(' arg_list ')'"
        p[0] = AST('func', p[1], p[3])
    def p_expr_function_na(self, p):
        "expr : FUNCTION_NA '(' ')'"
        p[0] = AST('func_na', p[1])
    def p_expr_ternary(self, p):
        "expr : bexpr '?' expr ':' expr"
        p[0] = AST('ifelse', p[1], p[3], p[5])

    ##########################################
    def p_arg_list_binop(self, p):
        "arg_list : arg_list ',' arg_list"
        p[0] = p[1] + p[3]
    def p_arg_list_base(self, p):
        "arg_list : expr"
        p[0] = [p[1]]

    ##########################################
    def p_bexpr_binop(self, p):
        '''
        bexpr : expr EQUALTO expr
              | expr NOTEQUAL expr
              | expr GTEQUAL expr
              | expr LTEQUAL expr
              | expr '>' expr
              | expr '<' expr
        '''
        if p[2] == '=='  : p[0] = AST(".==.", p[1], p[3])
        elif p[2] == '<=': p[0] = AST(".<=.", p[1], p[3])
        elif p[2] == '>=': p[0] = AST(".>=.", p[1], p[3])
        elif p[2] == '>': p[0] = AST(".>.", p[1], p[3])
        elif p[2] == '<': p[0] = AST(".<.", p[1], p[3])
        elif p[2] == '!=': p[0] = AST(".!=.", p[1], p[3]) 
    def p_bexpr_paren(self, p):
        "bexpr : '(' bexpr ')'"
        p[0] = p[2]
    def p_bexpr_base(self, p):
        """
         bexpr : TRUE
               | FALSE
        """
        p[0] = AST('logical_const', p[1])
    def p_bexpr_short_circuit(self, p):
        """
        bexpr : bexpr AND bexpr
              | bexpr OR bexpr
        """
        if p[2] == 'and': p[0] = AST('.and.', p[1], p[3])
        elif p[2] == 'or': p[0] = AST('.or.', p[1], p[3])
    def p_bexpr_not(self, p):
        "bexpr : NOT bexpr"
        p[0] = AST('not.', p[2])

    ##########################################
    def p_var(self, p):
        "var : NAME"
        p[0] = self.table.newVar(p[1])

    ##########################################
    def p_scope(self, p):
        "scope : scope_begin statement_list scope_end"
        p[0] = p[3]
    def p_scope_begin(self, p):
        "scope_begin : '{'"
        self.scope_stack.append(Scope(self.current_scope()))
    def p_scope_end(self, p):
        "scope_end : '}'"
        p[0] = self.scope_stack.pop()

    def p_statement_group(self, p):
        "statement : GROUP scope"
        self.current_scope().override(p[2])
        self.last_statement = p[2].mentioned()

    def p_if_list(self, p):
        "if_list : IF bexpr scope elif_list"
        p[0] = [(p[2], p[3])] + p[4]
    def p_elif_list_empty(self, p):
        "elif_list : "
        p[0] = []
    def p_elif_list_item(self, p):
        "elif_list : ELIF bexpr scope elif_list"
        p[0] = [(p[2], p[3])] + p[4]

    def p_if_statement_no_default(self, p):
        "statement : if_list"
        self.process_if(p[1], Scope(self.current_scope()))
    def p_if_statement_default(self, p):
        "statement : if_list ELSE scope"
        self.process_if(p[1], p[3])

    def process_if(self, if_list, else_scope):
        modified_vars = Set(else_scope.immediate())
        for (cond, scope) in if_list:
            modified_vars |= Set(scope.immediate())
        if_list.reverse()
        for var in modified_vars:
            expr = else_scope.get_expr(var)
            for (cond, scope) in if_list:
                expr = AST('ifelse', cond, scope.get_expr(var), expr);
            else_scope.replace_equation(im.Equation(var, "=", expr))
        self.current_scope().override(else_scope)

        mentioned = Set()
        mentioned |= else_scope.mentioned()
        for (cond, scope) in if_list:
            mentioned |= scope.mentioned()
        self.last_statement = mentioned

    def p_statement_eqn(self, p):
        "statement : var ASSIGN expr ';'"
        eqn = im.Equation(p[1], "=", p[3])
        self.current_scope().add_equation(eqn)
        self.last_statement = Set([p[1]])
    def p_statement_var(self, p):
        "statement : var ';'"
        self.current_scope().mention_variable(p[1])
        self.last_statement = Set([p[1]])

    def p_modassign(self, p):
        """
        modassign : PLUSEQUAL
                  | MINUSEQUAL
                  | DIVIDEEQUAL
                  | TIMESEQUAL
        """
        p[0] = p[1]
    def p_statement_modeqn(self, p):
        "statement : var modassign expr ';'"
        scope = self.current_scope()
        var = p[1]
        if not scope.has_expr(var) : 
            raise Exception('Problem with variable: '+str(var)) 
        expr = scope.get_expr(var)
        if p[2] == "+=":
            expr = AST(".+.", expr, p[3])
        elif p[2] == "-=":
            expr = AST(".-.", expr, p[3])
        elif p[2] == "*=":
            expr = AST(".*.", expr, p[3])
        elif p[2] == "/=":
            expr = AST("./.", expr, p[3])
        scope.replace_equation(im.Equation(var, "=", expr))
        self.last_statement = Set([var])

    def p_statement_units(self, p):
        "statement : UNITS '(' unit ')' ';'"
        for var in self.last_statement:
            var.make_a('unit', p[3])
    def p_statement_param(self, p):
        '''
        statement : PARAM '(' ')' ';'
                  | PARAM ';'
        '''
        for var in self.last_statement:
            var.make_a('param', 1);
    def p_statement_external_named(self, p):
        "statement : EXTERNAL '(' NAME ')' ';'"
        for var in self.last_statement:
            var.make_a('external', p[3])
    def p_statement_external_unnamed(self, p):
        "statement : EXTERNAL '(' ')' ';'"
        for var in self.last_statement:
            var.make_a('external', var.name)
    def p_statement_nodal(self, p):
        '''
        statement : NODAL '(' ')' ';'
        '''
        for var in self.last_statement:
            var.make_a('nodal', 1)
    def p_statement_lookup(self, p):
        "statement : LOOKUP '(' signed_number ',' signed_number ',' signed_number ')' ';'"
        for var in self.last_statement:
            self.lookup.append(Lookup(var.name, p[3],p[5],p[7]))
    def p_statement_lower_bound(self, p):
        "statement : LB '(' signed_number ')' ';'"
        for var in self.last_statement:
            var.make_a('lower_bound', p[3])
    def p_statement_upper_bound(self, p):
        "statement : UB '(' signed_number ')' ';'"
        for var in self.last_statement:
            var.make_a('upper_bound', p[3])
    def p_statement_regional(self, p):
        """
        statement : REGIONAL '(' ')' ';'
        """
        for var in self.last_statement:
            var.make_a('regional', 1)
    def p_statement_method(self, p):
        "statement : METHOD '(' NAME ')' ';'"
        for var in self.last_statement:
            var.make_a('method', p[3])
    def p_statement_store(self, p):
        "statement : STORE '(' ')' ';'"
        for var in self.last_statement:
            var.make_a('store', 1)
    def p_statement_trace(self, p):
        "statement : TRACE '(' ')' ';'"
        for var in self.last_statement:
            var.make_a('trace', 1)
    def p_statement_flag_bare(self, p):
        "statement : FLAG '(' ')' ';'"
        for var in self.last_statement:
            var.make_a('flag', [])
            var.make_a('param', 1)
    def p_statement_flag_param(self,p):
        "statement : FLAG '(' var_list ')' ';'"
        for var in self.last_statement:
            var.make_a('flag', p[3]);
            var.make_a('param', 1);
    def p_var_list_base(self,p):
        "var_list : var"
        p[0] = [p[1]]
    def p_var_list_recursive(self,p):
        "var_list : var ',' var_list"
        p[0] = [p[1]] + p[3]

    def p_statement_empty(self, p):
        "statement : ';'"
    def p_statement_list_empty(self, p):
        "statement_list : "
    def p_statement_list(self, p):
        "statement_list : statement statement_list"

    def p_statement_reference(self, p):
        "statement : REFERENCE '(' ')' ';'"
        for var in self.last_statement:
            var.make_a('reference', 1)

    #########################################
    # do if statements

    def p_error(self, p):
        if p:
            print(("Syntax error at line %d, token '%s'" % (self.lexer.lineno, p.value)))
        else:
            print("Syntax error at EOF")

class Expr_Parser(Eqn_Parser):
    def __init__(self, table):
        Eqn_Parser.__init__(self, table, start_symbol='expr')

class Unit_Parser(Eqn_Parser):
    def __init__(self):
        Eqn_Parser.__init__(self, None, start_symbol='unit')
def parse_unit(text):
    p = Unit_Parser()
    return p.parse(text)

class AST:
    def __init__(self, type, *args):
        self.type = type
        self.args = args
        if self.type == 'paren':
            if args[0].type == 'var' or args[0].type == 'const':
                self.copy(args[0])
        elif self.type == '-.':
            if args[0].isZero():
                self.copy(args[0])
            elif args[0].type == 'const':
                if args[0][0][0] == '-': #first character of first arg to const
                    # remove negative sign.
                    self.copy(AST('const',args[0][0][1:]))
                else:
                    # add negative sign.
                    self.copy(AST('const','-' + args[0][0]))
        elif self.type == '.*.':
            for arg in args:
                if arg.isZero():
                    self.copy(arg)
                    break
            else:
                if self[0].isOne():
                    self.copy(self[1])
                elif self[1].isOne():
                    self.copy(self[0])
        elif self.type == './.':
            if self[0].isZero() or self[1].isOne():
                self.copy(self[0])
        elif self.type in ('.+.', '.-.'):
            if self[0].isZero() and self[1].isZero():
                self.copy(self[0])
            elif self[1].isZero():
                self.copy(self[0])
            elif self[0].isZero():
                if self.type == '.+.':
                    self.copy(self[1])
                else:
                    self.copy(AST('-.', self[1]))
        elif self.type == 'ifelse':
            for child in self.args[1:]:
                if not child.isZero():
                    break
            else:
                self.copy(self[1])
        elif self.type == 'const':
            if not re.search(r'\.', self.args[0]) and not re.search(r'[eE]', self.args[0]):
                self.args = (args[0]+'.',args[1:])

    def isZero(self):
        return self.type == 'const' and float(self.args[0]) == 0
    def isOne(self):
        return self.type == 'const' and float(self.args[0]) == 1
    def copy(self, other):
        self.type = other.type
        self.args = other.args

    def children(self):
        t = self.type
        if t in ('.+.'
                 ,'.*.'
                 ,'./.'
                 ,'.-.'
                 ,'.|.'
                 ,'-.'
                 ,'paren'
                 ,'ifelse'
                 ,'.==.'
                 ,'.!=.'
                 ,".<=."
                 ,".>=."
                 ,".>."
                 ,".<."
                 ,".and."
                 ,".or."
                 ,"not."):
            for arg in self.args:
                yield arg
        elif t == 'func':
            for arg in self.args[1]:
                yield arg
        else:
            pass

    def depth_walk(self):
        yield self
        for child in self.children():
            for ast in child.depth_walk():
                yield ast

    def recurse(self, function):
        new_args = []
        for child in self.children():
            new_args.append(function(child))
        if self.type == 'func':
            return AST('func', self.args[0], new_args)
        elif new_args:
            return AST(self.type, *new_args)
        else:
            return self

    def toString(self, format=lambda v: v.name):
        children = [child.toString(format) for child in self.children()]
        t = self.type
        
        if t == '.+.':
            ret = '+'.join(children)
        elif t == '.-.':
            ret = children[0]+'-('+children[1]+')' # to prevent a - -3 = a--3, different c semantics . 
        elif t == '.*.':
            ret = '*'.join(children)
        elif t == './.':
            ret = '/'.join(children)
        elif t == '.|.':
            ret = '|'.join(children)
        elif t == 'paren':
            ret = ''.join(children)
        elif t == '-.':
            ret = "-" + ''.join(children)
        elif t == 'func':
            ret = ''.join((self[0],'(',','.join(children),')'))
        elif t == 'func_na':
            ret = self[0]+'()'
        elif t == 'ifelse':
            ret = '%s ? %s : %s' % tuple(children) 
        elif t == '.==.':
            ret = "==".join(children)
        elif t == '.!=.':
            ret = "!=".join(children)
        elif t == ".<=.":
            ret = "<=".join(children)
        elif t == ".>=.":
            ret = ">=".join(children)
        elif t == ".>.":
            ret = ">".join(children)
        elif t == ".<.":
            ret = "<".join(children)
        elif t == ".and.":
            ret = "&&".join(children)
        elif t == ".or.":
            ret = "||".join(children)
        elif t == "not.":
            ret = "!"+''.join(children)
        elif t == 'var':
            return format(self[0])
        elif t == 'const':
            return self[0]
        elif t == 'logical_const':
            return self[0]
        else:
            return "Blah "+str(self.type)+str(self.args)
        return '('+ret+')'

    def __getitem__(self, i):
        return self.args[i]
