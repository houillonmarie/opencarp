#!/usr/bin/env python

# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

from integrate import Trace, solve
from im import IonicModel
from var_control import Ui_VarControl
from model_main import Ui_ModelMain
from PyQt4 import QtGui, QtCore
from array import array
import numpy as np
import PyQt4.Qwt5 as Qwt
import re

class VarControl(Ui_VarControl,QtGui.QWidget):
    def __init__(self, parent=None, name="", trace=False):
        # set up the GUI
        QtGui.QWidget.__init__(self, parent)
        self.setupUi(self)
        self.parent=parent
        
        # define all internal model variables
        self.gen_trace = None
        self.held_trace = None
        self.used = False
        self.viewed = True

        # make the buttons functional.
        self.refresh_state()
        self.connect(self.wClamp, QtCore.SIGNAL('toggled(bool)'), self.use)
        self.connect(self.wView, QtCore.SIGNAL('toggled(bool)'), self.view)
        self.connect(self.wName, QtCore.SIGNAL('textChanged(QString)'), self.gui_change_name)
        self.connect(self.wClose, QtCore.SIGNAL('clicked()'), self.delete)

        # set the initial values
        self.change_name(name)
        if trace:
            self.set_held_trace(trace)


    def generated_trace(self, trace):
        self.gen_trace = trace
        self.refresh_state()
    def discard_gen(self):
        self.gen_trace = None
        self.refresh_state()
    def save_gen(self):
        self.held_trace = self.gen_trace
        self.gen_trace = None
        self.refresh_state()
    def use(self, switch=True):
        #assert not self.gen_trace and self.held_trace
        self.used = switch
        self.refresh_state()
    def view(self, switch=True):
        self.viewed = switch
        self.refresh_state()
    def set_held_trace(self, trace):
        self.held_trace = trace
        self.refresh_state()
    def get_held_trace(self):
        return self.held_trace
    def change_name(self, string):
        self.wName.setText(string)
    def gui_change_name(self, string):
        if not string:
            self.name = "noname"
        else:
            self.name = str(string)
    def refresh_state(self):
        state = self.state()
        if state in ["used", "held", "compare"]:
            self.wClamp.setEnabled(True)
        else:
            self.wClamp.setEnabled(False)
        self.wClamp.setChecked(self.used)
        self.wView.setChecked(self.viewed)
    def state(self):
        result = ""
        if self.held_trace and self.used:
            result = "used"
        elif not self.gen_trace:
            if not self.held_trace:
                result = "empty"
            elif self.held_trace and not self.used:
                result = "held"
            else:
                assert 0
        else:
            if not self.held_trace:
                result = "gen"
            elif self.held_trace:
                result = "compare"
        return result
    def has_held(self):
        return self.state() != "gen" and self.state() != "empty"
    def delete(self):
        # tell the parent that we need to get rid of this variable.
        if self.parent:
            self.parent.remove(self)

class VarList(QtGui.QWidget):
    def __init__(self, parent=None):
        # set up the GUI
        QtGui.QWidget.__init__(self, parent)
        self.list = []

        #prepare the widget
        vb = QtGui.QVBoxLayout(self)
        vb.setMargin(0)

        scroll = QtGui.QScrollArea(self)
        scroll.setWidgetResizable(True)
        scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff) # no horizontal scrolling.
        vb.addWidget(scroll)
        self.wAdd = QtGui.QPushButton(self)
        self.wAdd.setText(QtGui.QApplication.translate("VarList", "Add", None, QtGui.QApplication.UnicodeUTF8))
        vb.addWidget(self.wAdd)

        wList = QtGui.QWidget(scroll)
        scroll.setWidget(wList)
        
        self.layout = QtGui.QVBoxLayout(wList)
        self.layout.setMargin(0)
        self.layout.setSpacing(0)
        spacerItem = QtGui.QSpacerItem(20,40,QtGui.QSizePolicy.Minimum,QtGui.QSizePolicy.Expanding)
        self.layout.addItem(spacerItem)

        #connect up all the buttons
        self.connect(self.wAdd, QtCore.SIGNAL('clicked()'), self.add)

    def add(self, name="", trace=False):
        vc = VarControl(self, name, trace)
        self.list.append(vc)
        self.layout.addWidget(vc)

    def remove(self, child):
        self.layout.removeWidget(child)
        self.list.remove(child)
        child.close()
        del child

    def clear(self):
        while self.list:
            self.remove(self.list[0])

    def get_var_hash(self):
        result = {}
        for var in self.list:
            result[var.name] = var
        return result

class TracePlot(Qwt.QwtPlot):
    """
    Graphs one or more traces.
    """
    def __init__(self, parent=None):
        Qwt.QwtPlot.__init__(self, parent)
        
        # create a plot with a white canvas
        self.setCanvasBackground(QtCore.Qt.white)
        
        self.plotLayout().setCanvasMargin(0)
        self.plotLayout().setAlignCanvasToScales(True)
        
        # set plot layout
        self.plotLayout().setMargin(0)
        self.plotLayout().setCanvasMargin(0)        
        self.plotLayout().setAlignCanvasToScales(True)

        # adjust the min size so it doesn't get squashed.
        self.setMinimumHeight(200)

        # attach a grid
        grid = Qwt.QwtPlotGrid()
        grid.attach(self)
        grid.setPen(QtGui.QPen(QtCore.Qt.black, 0, QtCore.Qt.DotLine))

        axFont  = QtGui.QFont("Helvetica [Cronyx]", 9)
        axTitle = Qwt.QwtText('Time (ms)')
        axTitle.setFont(axFont)
        self.setAxisTitle(Qwt.QwtPlot.xBottom, axTitle)

        # set up other internal variables
        self.traces = []
        self.colors = [QtCore.Qt.blue,QtCore.Qt.red]

    def add_trace(self, trace, type):
        if type == 'held':
            type_index = 0
        else:
            type_index = 1
        self.traces.append(trace)
        curve = Qwt.QwtPlotCurve("Trace %d" % (len(self.traces)+1))
        curve.setPen(QtGui.QPen(self.colors[type_index], 2))
        curve.setData(trace.t, trace.y)
        curve.attach(self)

    def change_name(self, name):
        axFont  = QtGui.QFont("Helvetica [Cronyx]", 9)
        axTitle = Qwt.QwtText(name)
        axTitle.setFont(axFont)
        self.setAxisTitle(Qwt.QwtPlot.yLeft, axTitle)

class ModelMain(QtGui.QMainWindow,Ui_ModelMain):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)
        
        #set up the widget
        hb = QtGui.QHBoxLayout(self.centralWidget)
        hb.setMargin(0)
        splitter = QtGui.QSplitter(QtCore.Qt.Horizontal, self.centralWidget)
        hb.addWidget(splitter)

        self.var_list = VarList(splitter)
        splitter.addWidget(self.var_list)

        #set up the plotting widget
        scroll = QtGui.QScrollArea(splitter)
        splitter.addWidget(scroll)
        scroll.setWidgetResizable(True)
        scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff) # no horizontal scrolling.
        self.plotWidget = QtGui.QWidget(scroll)
        vb = QtGui.QVBoxLayout(self.plotWidget)
        vb.setMargin(0)
        self.plotLayout = vb
        scroll.setWidget(self.plotWidget)

        #set up all the internal variables
        self.im_filename = 0
        self.tstart = 0
        self.tstop = 0

        #Make the buttons do things
        self.connect(self.actionRead_Text_File, QtCore.SIGNAL("triggered()"),
                     self.read_text_file)
        self.connect(self.actionSolve_System, QtCore.SIGNAL("triggered()"),
                     self.solve_system)
        self.connect(self.actionReplot, QtCore.SIGNAL("triggered()"),
                     self.replot)
        self.connect(self.actionUse_Model_File, QtCore.SIGNAL("triggered()"),
                     self.use_model_file)

    def use_model_file(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, "Open a model file", "", "Model Files (*.model)")
        if filename:
            self.im_filename = filename
        
    def read_text_file(self):
        file = QtGui.QFileDialog.getOpenFileName(self, "Open a trace text file", "", "Text trace (*.txt)")
        if not file:
            return
        file = open(file, "r")
        line = file.readline().strip()
        column_headers = re.split(r'\s+', line)
        columns = {}
        for header in column_headers:
            columns[header] = array('d')
        assert columns.has_key("t")
        while 1:
            line = file.readline().strip()
            if not line:
                break
            values = [float(v) for v in re.split(r'\s+', line)]
            if len(values) != len(column_headers):
                assert "# of columns needs to be the same throughout the file"
            for ii in xrange(0,len(column_headers)):
                columns[column_headers[ii]].append(values[ii])
        for name in column_headers:
            if name == "t":
                continue
            self.add_var(name, Trace(np.asanyarray(columns[name]), np.asanyarray(columns['t'])))

    def checkTrace(self, trace):
        if (trace
            and (trace.tstart() != self.tstart
                 or
                 trace.tstop() != self.tstop
                 )):
            ret = QtGui.QMessageBox.warning(self, "Trace time mismatch",
                                            "You're inserting a trace that has a\n"
                                            +"different start time and end time.  I want to delete\n"
                                            +"all your traces and start over.",
                                            QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
            if ret == QtGui.QMessageBox.Ok:
                self.var_list.clear()
                self.tstart = trace.tstart()
                self.tstop = trace.tstop()
                return True
            else:
                return False
        else:
            return True

    def add_var(self, name, trace=False):
        if self.checkTrace(trace):
            self.var_list.add(name, trace)

    def solve_system(self):
        if not self.im_filename:
            QtGui.QMessageBox.warning(self, "Need to set model", "I need you to set 'Use Model File...' first.")
            return
        #find all variables that are used.
        #find all variables that are not used
        input_vars = {}
        init_vars = {}
        output_names = []
        vars = self.var_list.get_var_hash()
        for name in vars.keys():
            if vars[name].state() == "used":
                input_vars[name] = vars[name].get_held_trace()
            else:
                output_names.append(name)
            vars[name].discard_gen()
            if vars[name].state() != "empty":
                init_vars[name] = vars[name].get_held_trace()

        #do the solve
        out_traces = solve(IonicModel(open(self.im_filename, "r").read()), 
                           input_vars, output_names, self.tstart, self.tstop, init_vars)
        #put the results back into the variables.
        for name in out_traces.keys():
            vars[name].generated_trace(out_traces[name])
            if vars[name].state() == "gen":
                vars[name].save_gen()
        self.replot()

    def replot(self):
        #clear the plot widget
        while self.plotLayout.itemAt(0):
            item = self.plotLayout.itemAt(0)
            self.plotLayout.removeItem(item)
            widget = item.widget()
            if widget:
                widget.close()
            del item

        #fill the plot widget
        for vc in self.var_list.list:
            if vc.viewed and vc.state() != "empty":
                plot = TracePlot(self.plotWidget)
                plot.change_name(vc.name)
                if vc.held_trace:
                    plot.add_trace(vc.held_trace, "held")
                if vc.gen_trace:
                    plot.add_trace(vc.gen_trace, "gen")
                self.plotLayout.addWidget(plot)
        


if __name__=="__main__":
    import sys
    app = QtGui.QApplication(sys.argv)

    widget = ModelMain()
    widget.setWindowTitle('simple')
    widget.show()

    sys.exit(app.exec_())
