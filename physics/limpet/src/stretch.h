// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef STRETCH_H
#define STRETCH_H

#include "MULTI_ION_IF.h"

namespace limpet {

// define passive and active strain protocols
typedef enum _stretch_protocol {STRAIN_PULSE,ISOMETRIC,ISOSARCOMETRIC,AUXOTONIC} s_prtcl;

struct pulseStretch {
  s_prtcl  prtcl;
  float    sr;        //< stretch ratio
  float    onset;
  float    duration;
  float    rise;
  float    fall;
};

struct isoStretch {
  s_prtcl  prtcl;
};

struct isoSarcoStretch {
  s_prtcl  prtcl;
} ;

struct auxotonicStretch {
  s_prtcl  prtcl;
};

typedef union _stretch {
  s_prtcl          prtcl;
  pulseStretch     pulse;
  isoStretch       iso;
  isoSarcoStretch  isoSarco;
  auxotonicStretch auxo;
} stretch;


void initializePulseStretch(float strain, float onset, float duration, 
                                  float rise, float fall, stretch *s);
void apply_stretch(MULTI_IF *miif, stretch *s, opencarp::timer_manager *tm);

}  // namespace limpet

#endif
