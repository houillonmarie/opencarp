// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef LUT_H
#define LUT_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include "limpet_types.h"

// define so that LUT exist only in device memory
#define LUT_DEV_ONLY

namespace limpet {

typedef double LUT_data_t;

/** \brief lookup table structure

   Instead of using defines in the header files, variables are used
   to allow the adjustment of the table size without recompilation.
   This is useful, for instance, when much bigger tables are needed
   during a shock.

   Two convenience functions are provided for dealing with the tables:
   LUT_index() and LUT_constrain().
 */
struct LUT {
  char       *name;   /**< name of the table                  */
  int         rows;   /**< number of rows in lut              */
  int	      cols;   /**< number of cols in lut              */
  float	        mn;   /**< start value of lut                 */
  float	        mx;   /**< end value of lut                   */
  float	       res;	  /**< resolution of lut                  */
  float	      step;   /**< step size of lut                   */
  int	    mn_ind;   /**< minimum index of lut               */
  int       mx_ind;   /**< maximum index of lut               */
  float         dt;   /**< time step used for lut generation  */
  LUT_data_t **tab;	  /**< pointer to the table               */
};


void         LUT_alloc( LUT *psv, int cols, float mn, float mx, float res,
                        const char* );
int          LUT_dump( LUT *plut, const char * );
int          check_LUT( LUT* );
LUT_data_t   LUT_interp( LUT *t, int i, int j, GlobalData_t x );
LUT_data_t   LUT_derror( LUT *t, int idx, GlobalData_t x );
LUT_data_t   LUT_interpRow( LUT *tab, GlobalData_t val, int i, LUT_data_t*);
void         LUT_problem( LUT *, double, int, const char * );
void         IIF_warn(const int wv, const char* error);
void         destroy_lut( LUT *plut );
void       **build_matrix_ns ( int, int, int, size_t );
LUT_data_t*  LUT_row( LUT *lut, GlobalData_t val, int locind );

/** access an entry in the table
 *  \param A table
 *  \param I row
 *  \param J column
 */
#define LUT_entry(A, I, J) (A)->tab[I][J]

#ifdef IMP_FAST
/** \def LUT_index
 *   Fast index lookup with no checking.
 *
 *   This gets implemented if <code>IMP_FAST<\code> is defined.
 *   Arguments are the same as for LUT_index()
 */
#define LUT_index( T, V, W )  (int)((V)*T->step)
#define LUT_constrain( T, V, W )

#else

/** do a safe table lookup with error reporting
*
* \param tab    pointer to lookup table
* \param val    value to lookup.
* \param locind index of value in its local array
*
* \return the index into the table
*
* \note An out-of-bounds value (including infinity) will be brought to
*         the nearest edge. NaN will die.
*/
int LUT_index( LUT *tab, GlobalData_t val, int locind );

/** constrain value to table bounds.
*
*  If a value lies outside the table, return it to the closest edge.
*  It calls LUT_index().
*
* \param T pointer to lookup table
* \param V value to check
* \param W index of value in its array
* \param N string for table type
*
* \note If <code>IMP_FAST</code> is defined, this is ignored
*/
#define LUT_constrain(T, V, W, N )  \
  if( V>=T->mx){/*fprintf(stderr, "table overbound:  %g\n", V );*/V=T->mx;}\
  if( V<=T->mn){/*fprintf(stderr, "table underbound: %g\n", V );*/V=T->mn;}
#endif

}  // namespace limpet

#endif
