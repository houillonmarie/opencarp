// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef AP_ANALYZER_H
#define AP_ANALYZER_H

#include <stdio.h>

#include "timer_utils.h"

namespace limpet {

// define which beat to use for calibration
#define CALIBRATION_BEAT     3

// assuming a resting potential of -86 mV and an AP amplitude of 120 mV
#define def_APD90_thresh   -40.
#define def_APD30_thresh   -40.

typedef enum _event_meth { X_THRESHOLD, MX_RATE, MX_VAL } event_meth;

struct event_x_threshold {
  event_meth        m;
  char             *name;
  float             threshold;
  bool              p_slope;
};

struct event_mx_rate {
  event_meth         m;
  char              *name;
  float              threshold;
  bool               p_slope;
};

struct event_mx_val {
  event_meth         m;         //!< method is MX_VAL
  char              *name;      //!< name of event
  bool               p_slope;   //!< toggle max min search
};

typedef union _event_class  {
  event_meth         m;
  event_x_threshold  x_thresh;
  event_mx_rate      mx_rate;
  event_mx_val       mx_val;
} event_class;

struct ap_event {
  double             s;         //!< value of signal when event occurred
  double             t;         //!< time when event was observed
  double            *trace;     //!< short trace of signal to analyze
  event_class        c;
  bool               trg;
};

struct ap_events {
  int                num_events;
  ap_event          *events;
};

struct steady_state_ap {
  int                num_APs;    //!< minimum number of APs for steady state
  int                num_params; //!< number of parameters we use to decide steady state
  int                cnt;        //!< count number of beats in steady state
  float             *c;          //!< array of parameters at current AP
  float             *p;          //!< array of parameters at previous AP
};

typedef enum _ap_states {AP_STATE,DI_STATE} ap_states;

#define VM_HIST_LEN 4
struct action_potential {
  int                beat;     //!< beat counter
  int                calBeat;  //!< beat number used to calibrate thresholds
  bool               pmat;     //!< indiacte premature AP
  ap_events          acts;     //!< activation events
  ap_events          repols;   //!< repolarization events
  ap_events          ampls;    //!< amplitude events (mx or mn found in a signal)
  float              APD;      //!< standard APD as measured by APD90
  float              DI;       //!< BCL-APD90
  float              triang;   //!< AP triangulation, APD90-APD30
  float              DIp;      //!< DI of the previous AP
  ap_states          state;    //!< current state of AP, ongoing AP or diastole
  double             vm_trc [VM_HIST_LEN];
  double             dvm_trc[VM_HIST_LEN];
  steady_state_ap    ss;       //!< steady state detection
  FILE              *stats;    //!< output statistics for each AP
  FILE              *rstats;   //!< output restitution statistics only
};


// AP analyzer
int  initialize_AP_analysis(action_potential *AP);
bool check_events(double vm, action_potential *AP, opencarp::timer_manager *tm);
void calibrate_thresholds(action_potential *AP);
bool check_steady_state(steady_state_ap *ss);
void cleanup_AP_analysis(action_potential *AP);
void print_AP_stats_header(action_potential *AP, FILE *outbuf);

}  // namespace limpet

#endif
