// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef CLAMP_H
#define CLAMP_H

#include "trace.h"
#include "timer_utils.h"
#include "sf_interface.h"

namespace limpet {

struct Clamp {
  double  val_pre;
  double  val;
  double  val_post;
  double  start;
  double  dur;
  double  tau;
  char   *file;
  trace   tr;
  int     transient;
  char   *sv;
  int     impDataID;
};


bool initialize_clamp(Clamp *cl, double ini_val, double cl_val, double dur, double start, const char *f,int,float*);
void clamp_signal(MULTI_IF *pMIIF, Clamp *cl, opencarp::timer_manager *tm );
void initialize_sv_clamp(Clamp *cl, const char *sv, char *f, double);
void sv_clamp( Clamp *cl, opencarp::timer_manager *tm, MULTI_IF*, bool trigger );
void initialize_AP_clamp(Clamp *cl, char *f, double);
void AP_clamp( Clamp *cl, opencarp::timer_manager *tm, opencarp::sf_petsc_vec *v, bool trigger );
int  process_sv_clamps( char *SVs, char *files, Clamp **clamps, double dt );

}  // namespace limpet

#endif
