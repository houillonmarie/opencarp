// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*
 * header for ODEint
 *
 * Formalized ODE integration following an approach similar to CVODE etc.
 */
#ifndef ODEint_H
#define ODEint_H

#include "limpet_types.h"

namespace limpet {

#define ODEint_MaxVecLen 100       // Do not allow any ODE systems with more than 100 variables

struct d_OdeVec {
  int     len;
  GlobalData_t *vec;
};

void ODEint_RK(GlobalData_t *,GlobalData_t *, void f(GlobalData_t*,GlobalData_t*,void*), void *, int, GlobalData_t);
void ODEint_FE(GlobalData_t *y, GlobalData_t *ydot, void *, void *f_data, int len, GlobalData_t dt);

void d_ODEint_FE(d_OdeVec *y, d_OdeVec *ydot, void *, void *f_data, GlobalData_t dt);
void d_ODEint_RK(d_OdeVec *y, d_OdeVec *ydot, void f(GlobalData_t*,d_OdeVec*,void*),
                 void *f_data, GlobalData_t dt);

}  // namespace limpet

#endif


