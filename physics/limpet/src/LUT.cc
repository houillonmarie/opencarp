// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#include "LUT.h"
#include "basics.h"
#include "MULTI_ION_IF.h"

#include <string.h>
#include <assert.h>
#include <cmath>

namespace limpet {

using ::opencarp::dupstr;
using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;
using ::opencarp::is_big_endian;
using ::opencarp::log_msg;

/*  create and initialize LUT
 *
 * \param p pointer to lookup table structure
 * \param rows  number of lookup variables
 * \param mn  lower boundary of LUT
 * \param mx  upper boundary of LUT
 * \param res resolution of LUT
 * \param name  table name
 *
 */
void LUT_alloc(LUT *plut,int cols,float mn,float mx,float res,const char *name)
{
  plut->name = dupstr(name);

  // table bounds
  plut->cols   = cols;
  plut->mn     = mn;
  plut->mx     = mx;
  plut->res    = res;
  plut->step   = 1/res;

  // in terms of indices
  plut->mn_ind = (int)(mn * plut->step);
  plut->mx_ind = (int)(mx * plut->step);
  plut->rows   = plut->mx_ind - plut->mn_ind + 1;

  int target = 0;  // host memory
#ifndef LUT_DEV_ONLY
  target = 2;      // unified memory
#endif

  // build a table and offset it
  plut->tab  = (LUT_data_t **)build_matrix_ns( plut->rows, plut->cols,
               sizeof(LUT_data_t), target );
  plut->tab -= plut->mn_ind;
}


/*  dump LUT to file
 *
 * \param p pointer to lookup table structure
 * \param fname filename to store LUT dump
 *
 * \return 0 if successull, -1 otherwise
 */
int LUT_dump( LUT *plut, const char *fname )
{
  FILE_SPEC fdump = f_open(fname, "wb");

  if (!fdump)
    return -1;

  // write endianness and table dimensions first
  int endian_flg = is_big_endian();
  fwrite(&endian_flg, sizeof(int), 1, fdump->fd);
  fwrite(&plut->rows, sizeof(plut->rows), 1, fdump->fd);
  fwrite(&plut->cols, sizeof(plut->cols), 1, fdump->fd);

  for (int i=plut->mn_ind; i<=plut->mx_ind; i++)
    for (int j=0; j<plut->cols; j++)
      fwrite(&plut->tab[i][j], sizeof(LUT_data_t), 1, fdump->fd);

  f_close(fdump);

  return 0;
}


/**  destroy  LUT
 *
 * \param plut  pointer to lut
 */
void destroy_lut( LUT *plut )
{
  if ( plut != NULL && plut->tab != NULL )  {
    if (plut->tab != NULL) {
      plut->tab += plut->mn_ind;
      IMP_free( plut->tab[0] );
      IMP_free( plut->tab );
    }
    IMP_free(plut->name);
  }
  if( plut )
    memset( plut, 0, sizeof(LUT) );
}


/** build an mXn matrix
 *
 *  \param m      number of rows
 *  \param n      number of columns
 *  \param size   size in bytes of each entry
 *  \param device 0=host,1=device,2=unified
 *
 *  \return pointer to an array of pointers to the matrix rows
 */
void **build_matrix_ns( int m, int n, int size, size_t device )
{
  void **ptr = NULL;
  char *buf;

  buf = (char*)calloc( m*n, size );
  if( buf==NULL ) {
    log_msg(0, 5, 0, "Not enough memory for matrix\n" );
    exit(1);
  }
  ptr = (void **)calloc( m, sizeof(void *) );
  for ( int i=0; i<m; i++ )
    ptr[i] = buf + i*n*size;

  return ptr;
}


/** make sure all entries in a lookup table are valid
 *
 *  \param IF   IMP
 *  \param lut  LUT
 *  \param name name of table
 *
 *  \retval 0 all entries finite
 *  \retval otherwise
 */
#define MAX_LUT_NUMINF 10       //!< maximum \# non-finite warnings to print

int check_LUT( LUT* lut )
{
  int retval = 0;
  int numinf = 0;

  for ( int i=lut->mn_ind; i<=lut->mx_ind; i++ )
    for ( int j=0; j<lut->cols; j++ )
      if ( ! std::isfinite(lut->tab[i][j])) {
        log_msg(0, 2, 0, "LUT WARNING: %s=%g produces %g in entry number %d!\n",lut->name,i*lut->res,lut->tab[i][j],j);
        retval = 1;
    if( numinf++ > MAX_LUT_NUMINF ) {
          log_msg(0, 3, 0, "suppressing further errors!!!\n" );
      return retval;
    }
      }

  return retval;
}

/** print an error message to the terminaldetermine why index into a lookup table failed
 *
 * \param error   string error message.
 *
 * \note if <code>IMP_FAST</code> is defined, this function is implemented
 *       as a macro which does no bounds checking
 */
void IIF_warn(const int wv, const char error[]) {
  // log_msg(_nc_logf, 2, 0, "danger [tm = %f]: %s : local node = %d, global node = %d\n",
  //         current_global_time(), error, wv, current_global_node(wv) ); //FIXME
}

/** determine why index into a lookup table failed
 *
 * \param lt      lookup table
 * \param val     bad value for computing look up
 * \param wv      local index associated with \em val
 * \param tabname name of table being accessed
 * \param newval  table edge boundary
 *
 * \note if <code>IMP_FAST</code> is defined, this function is implemented
 *       as a macro which does no bounds checking
 */
void LUT_problem( LUT *lt, double val, int wv, const char *tabname )
{
  // struct exception_type except =  { .exit=0 };
  char error[5000]; //FIXME, replace me with a C++ string.

  if ( std::isfinite(val) ) {
#ifdef CHATTY_LUT
    sprintf(error, "bounds exceeded for %s-table = %g (limits: %g - %g)",
            tabname, val, lt->mn, lt->mx);
    IIF_warn(wv, error);
#endif
    return;
  } else if (std::isinf(val)) {
    sprintf(error, "inf passed to LUT_index() for %s-table", tabname);
    IIF_warn(wv, error);
  } else if ( std::isnan(val) ) {
    sprintf(error, "NaN passed to LUT_index() for %s-table", tabname);
    IIF_warn(wv, error);
  }
}

#ifndef IMP_FAST
int LUT_index( LUT *tab, GlobalData_t val, int locind )
{
  int indx =  (int)(tab->step*val);

  if (indx < tab->mn_ind) {
    indx = tab->mn_ind;
  } else if (indx > tab->mx_ind) {
    indx = tab->mx_ind;
  } else {
    return indx;
  }

  LUT_problem(tab, val, locind, tab->name );
  return indx;
}
#endif

int LUT_out_of_bounds(LUT *tab, GlobalData_t val) {
  return (val < tab->mn || val > tab->mx) ;
}

/** interpolate in a lookup table
 *
 *  \param t the table
 *  \param i the row in the table
 *  \param j the column in the table
 *  \param x normalized discretizaton error
 *
 *  \return a linear interpolation of table values
 */
inline LUT_data_t LUT_interp( LUT *t, int i, int j, GlobalData_t x )
{
  return (1.-x)*t->tab[i][j] + x*t->tab[i+1][j];
}


/** compute normalized disretization error
 *
 * This is defined as the distance of the lookup value from the closest
 * lower value used by the table lookup. It is computed as
 * \f$ e = (X-x[i])/\Delta x\f$ where X is the value used in the lookup up, x[i] is
 * the closest table value and \f$\Delta x\f$ is the table resolution
 *
 *  \param t   table
 *  \param idx closest lower index
 *  \param x   value to lookup
 *
 *  \return the normalized error
 */
inline LUT_data_t LUT_derror( LUT *t, int idx, GlobalData_t x )
{
  return  (x-idx*t->res)/t->res;
}

/** interpolate a row of a lookup table
 *
 *  \param tab the table
 *  \param val current value for which we are looking up the dependent values
 *  \param i   local index of the vertex for which we are doing the lookup
 *  \param row interpolated values
 *
 *  \return  discretization error
 *  \pre     row is allocated
 *  \note    This is now threadsafe
 */
LUT_data_t LUT_interpRow(LUT *const tab,GlobalData_t val,int i,LUT_data_t* row)
{
  int    idx  = LUT_index(tab, val, i);
  GlobalData_t derr = LUT_derror(tab, idx, val);
  if (LUT_out_of_bounds(tab, val)) {
    for (int j=0;j<tab->cols;j++)
      row[j] = tab->tab[idx][j];
  } else {
    for (int j=0;j<tab->cols;j++)
      row[j] = LUT_interp(tab, idx, j, derr );
  }
  return derr;
}


/** return the row of a lookup table
 *
 *  \param lut     the lookup table
 *  \param val     the val to index
 *  \param locind  local index of val
 */
LUT_data_t*
LUT_row( LUT *lut, GlobalData_t val, int locind )
{
  return lut->tab[LUT_index( lut, val, locind )];
}

}  // namespace limpet
