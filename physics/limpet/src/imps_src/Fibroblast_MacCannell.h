// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: MacCannell KA, Bazzazi H, Chilton L, Shibukawa Y, Clark RB, Giles WR
*  Year: 2007
*  Title: A mathematical model of electrotonic interactions between ventricular myocytes and fibroblasts
*  Journal: Biophys J., 92(11), 4121-32
*  DOI: 10.1529/biophysj.106.101410
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __FIBROBLAST_MACCANNELL_H__
#define __FIBROBLAST_MACCANNELL_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Fibroblast_MacCannell_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG
#define Fibroblast_MacCannell_MODDAT Iion_DATA_FLAG

struct Fibroblast_MacCannell_Params {
    GlobalData_t GK1;
    GlobalData_t GKv;
    GlobalData_t GbNa;
    GlobalData_t Ggj;
    GlobalData_t Ki_init;
    GlobalData_t Ko;
    GlobalData_t Nai_init;
    GlobalData_t Nao;
    GlobalData_t fb_myo_surf_ratio;
    GlobalData_t fb_surf;
    GlobalData_t maxINaK;
    GlobalData_t num_fb;

};

struct Fibroblast_MacCannell_state {
    GlobalData_t Ki;
    GlobalData_t Nai;
    GlobalData_t V_fb;
    Gatetype r_Kv;
    Gatetype s_Kv;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Fibroblast_MacCannell(ION_IF *);
void construct_tables_Fibroblast_MacCannell(ION_IF *);
void destroy_Fibroblast_MacCannell(ION_IF *);
void initialize_sv_Fibroblast_MacCannell(ION_IF *, GlobalData_t**);
void compute_Fibroblast_MacCannell(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
