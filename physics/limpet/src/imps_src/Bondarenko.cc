// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Vladimir E. Bondarenko, Gyula P. Szigeti, Glenna C. L. Bett, Song-Jung Kim, and Randall L. Rasmusson
*  Year: 2004
*  Title: Computer model of action potential of mouse ventricular myocytes
*  Journal: American Journal of Physiology-Heart and Circulatory Physiology, 287(3), 1378-1403
*  DOI: 10.1152/ajpheart.00185.2003
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Bondarenko.h"

#ifdef _OPENMP
#include <omp.h>
#endif


#include "Rosenbrock.h"


namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Bondarenko(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Bondarenko( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define partial_P_C1_del_P_C2 (GlobalData_t)(-1.)
#define partial_P_C1_del_P_O1 (GlobalData_t)(-1.)
#define partial_P_C1_del_P_O2 (GlobalData_t)(-1.)
#define partial_diff_P_C2_del_P_O2 (GlobalData_t)(0.)
#define partial_diff_P_O2_del_P_C2 (GlobalData_t)(0.)
#define partial_k_minus_a_del_P_C2 (GlobalData_t)(0.)
#define partial_k_minus_a_del_P_O1 (GlobalData_t)(0.)
#define partial_k_minus_a_del_P_O2 (GlobalData_t)(0.)
#define partial_k_minus_b_del_P_C2 (GlobalData_t)(0.)
#define partial_k_minus_b_del_P_O1 (GlobalData_t)(0.)
#define partial_k_minus_b_del_P_O2 (GlobalData_t)(0.)
#define partial_k_minus_c_del_P_C2 (GlobalData_t)(0.)
#define partial_k_minus_c_del_P_O1 (GlobalData_t)(0.)
#define partial_k_minus_c_del_P_O2 (GlobalData_t)(0.)
#define partial_k_plus_a_del_P_C2 (GlobalData_t)(0.)
#define partial_k_plus_a_del_P_O1 (GlobalData_t)(0.)
#define partial_k_plus_a_del_P_O2 (GlobalData_t)(0.)
#define partial_k_plus_b_del_P_C2 (GlobalData_t)(0.)
#define partial_k_plus_b_del_P_O1 (GlobalData_t)(0.)
#define partial_k_plus_b_del_P_O2 (GlobalData_t)(0.)
#define partial_k_plus_c_del_P_C2 (GlobalData_t)(0.)
#define partial_k_plus_c_del_P_O1 (GlobalData_t)(0.)
#define partial_k_plus_c_del_P_O2 (GlobalData_t)(0.)
#define partial_m_del_P_C2 (GlobalData_t)(0.)
#define partial_m_del_P_O1 (GlobalData_t)(0.)
#define partial_m_del_P_O2 (GlobalData_t)(0.)
#define partial_n_del_P_C2 (GlobalData_t)(0.)
#define partial_n_del_P_O1 (GlobalData_t)(0.)
#define partial_n_del_P_O2 (GlobalData_t)(0.)



void initialize_params_Bondarenko( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Bondarenko_Params *p = (Bondarenko_Params *)IF->params;

  // Compute the regional constants
  {
    p->Acap = 0.00013866;
    p->Bmax = 109.;
    p->CSQN_tot = 50000.;
    p->C_K1_init = 0.0011733433957123;
    p->C_K2_init = 0.00105586824723736;
    p->C_Na1_init = 0.000420472760277688;
    p->C_Na2_init = 0.0240114508843199;
    p->CaJSR_init = 171.167969039613;
    p->CaMKt_init = 0.729027738385079;
    p->CaNSR_init = 404.825013216286;
    p->Cai_init = 0.0949915068139801;
    p->Cao = 1400.;
    p->Cass_init = 0.0954184301907784;
    p->Cm = 1.;
    p->E_Cl = -40.;
    p->F = 96.5;
    p->GClCa = 10.;
    p->GK1 = 0.35;
    p->GKr = 0.0165;
    p->GKs = 0.00575;
    p->GKss = 0.0595;
    p->GKto_f = 0.5347;
    p->GKto_s = 0.;
    p->GKur = 0.25;
    p->GNa = 16.;
    p->GNab = 0.0026;
    p->GbCa = 0.0007;
    p->I1_Na_init = 0.000517471697712382;
    p->I2_Na_init = 2.45406116958509e-5;
    p->IC_Na2_init = 0.0174528857380179;
    p->IC_Na3_init = 0.402980726914811;
    p->ICaLmax = 7.;
    p->IF_Na_init = 0.000306123648969581;
    p->I_K_init = 0.00140618453684944;
    p->I_init = 0.427651445872853;
    p->IpCa_max = 0.0955;
    p->K_L = 0.3;
    p->K_mAllo = 0.;
    p->K_mCai = 3.6;
    p->K_mCao = 1400.;
    p->K_mNai = 12000.;
    p->K_mNao = 88000.;
    p->Kd = 0.6;
    p->Ki_init = 115599.50642567;
    p->Km_CSQN = 630.;
    p->Km_Cl = 10.;
    p->Km_Ko = 1500.;
    p->Km_Nai = 16600.;
    p->Km_pCa = 0.2885;
    p->Km_up = 0.412;
    p->Ko = 5400.;
    p->Nai_init = 12364.7482121793;
    p->Nao = 134000.;
    p->O_K_init = 0.0131742086125972;
    p->O_Na_init = 1.46826771436314e-6;
    p->O_init = 1.23713515513533e-6;
    p->P_C2_init = 0.565182571165673;
    p->P_CaL = 2.5;
    p->P_O1_init = 0.00571393383393735;
    p->P_O2_init = 2.09864618235341e-8;
    p->P_RyR_init = 0.000280539508743811;
    p->P_ryr_const1 = -0.01;
    p->P_ryr_const2 = -2.;
    p->R = 8.314;
    p->T = 308.;
    p->VJSR = 7.7e-8;
    p->VNSR = 2.31e-7;
    p->V_L = 0.;
    p->V_init = -78.945215785979;
    p->V_max_NCX = 3.939;
    p->Vmyo = 2.2e-5;
    p->Vss = 2.2e-8;
    p->a = 0.0625;
    p->aKss_init = 0.287585636847048;
    p->ato_f_init = 0.0142335908879204;
    p->ato_s_init = 0.0443263407760382;
    p->aur_init = 0.00346258606821817;
    p->b = 0.4;
    p->const5 = 8.2;
    p->delta_V_L = 6.4489;
    p->eta = 0.35;
    p->ito_f_init = 0.996989882138174;
    p->ito_s_init = 0.887568880831388;
    p->iur_init = 0.955684946168062;
    p->k_minus_a = 0.07125;
    p->k_minus_b = 0.965;
    p->k_minus_c = 0.0008;
    p->k_plus_a = 0.006075;
    p->k_plus_b = 0.00405;
    p->k_plus_c = 0.009;
    p->k_sat = 0.27;
    p->kb = 0.036778;
    p->kf = 0.023761;
    p->m = 3.;
    p->maxINaK = 2.486;
    p->n = 4.;
    p->nKs_init = 0.00336735013094628;
    p->off_rate = 0.0002;
    p->on_rate = 0.05;
    p->phi_L = 1.798;
    p->t_L = 1.5;
    p->tau_L = 1150.;
    p->tau_i_const = 643.;
    p->tau_tr = 20.;
    p->tau_xfer = 8.;
    p->v1 = 4.5;
    p->v1_caff = 10.;
    p->v2 = 3e-5;
    p->v2_caff = 0.1;
    p->vmup_init = 0.5059;
    p->y_gate_init = 0.845044436980163;
    p->y_gate_tau_const1 = 8.;
    p->y_gate_tau_const2 = 315.;
    GlobalData_t V = p->V_init;
    GlobalData_t Alpha_Na3 = (7e-7*(exp(((-(V+7.))/7.7))));
    p->Beta_Na4 = Alpha_Na3;
    p->Beta_Na5 = (Alpha_Na3/50.);
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_Bondarenko;

}


// Define the parameters for the lookup tables
enum Tables {
  V_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum V_TableIndex {
  Alpha_Na11_idx,
  Alpha_Na12_idx,
  Alpha_Na13_idx,
  Alpha_Na2_idx,
  Alpha_Na3_idx,
  Alpha_Na4_idx,
  Alpha_Na5_idx,
  Alpha_a0_idx,
  Alpha_a1_idx,
  Alpha_i_idx,
  Alpha_p_idx,
  Beta_Na11_idx,
  Beta_Na12_idx,
  Beta_Na13_idx,
  Beta_Na2_idx,
  Beta_Na3_idx,
  Beta_Na4_idx,
  Beta_Na5_idx,
  Beta_a0_idx,
  Beta_a1_idx,
  Beta_i_idx,
  FVRT_Ca_idx,
  O_ClCa_idx,
  aKss_rush_larsen_A_idx,
  aKss_rush_larsen_B_idx,
  ato_f_rush_larsen_A_idx,
  ato_f_rush_larsen_B_idx,
  ato_s_rush_larsen_A_idx,
  ato_s_rush_larsen_B_idx,
  aur_rush_larsen_A_idx,
  aur_rush_larsen_B_idx,
  epsilon_m_idx,
  epsilon_p_idx,
  f_NaK_idx,
  ito_f_rush_larsen_A_idx,
  ito_f_rush_larsen_B_idx,
  ito_s_rush_larsen_A_idx,
  ito_s_rush_larsen_B_idx,
  iur_rush_larsen_A_idx,
  iur_rush_larsen_B_idx,
  nKs_rush_larsen_A_idx,
  nKs_rush_larsen_B_idx,
  y_gate_rush_larsen_A_idx,
  y_gate_rush_larsen_B_idx,
  NROWS_V
};



void construct_tables_Bondarenko( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Bondarenko_Params *p = (Bondarenko_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double Alpha_m = (p->phi_L/p->t_L);
  double partial_diff_P_C2_del_P_C2 = (-p->k_minus_c);
  double partial_diff_P_C2_del_P_O1 = p->k_plus_c;
  double partial_diff_P_O2_del_P_O2 = (-p->k_minus_b);
  double sigma = ((1./7.)*((exp((p->Nao/67300.)))-(1.)));
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -1000, 1000, 0.01, "Bondarenko V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    V_row[Alpha_Na11_idx] = (3.802/((0.1027*(exp(((-(V+2.5))/17.))))+(0.2*(exp(((-(V+2.5))/150.))))));
    V_row[Alpha_Na12_idx] = (3.802/((0.1027*(exp(((-(V+2.5))/15.))))+(0.23*(exp(((-(V+2.5))/150.))))));
    V_row[Alpha_Na13_idx] = (3.802/((0.1027*(exp(((-(V+2.5))/12.))))+(0.25*(exp(((-(V+2.5))/150.))))));
    V_row[Alpha_Na2_idx] = (1./((0.188495*(exp(((-(V+7.))/16.6))))+0.393956));
    V_row[Alpha_Na3_idx] = (7e-7*(exp(((-(V+7.))/7.7))));
    V_row[Alpha_a0_idx] = (0.022348*(exp((0.01176*V))));
    V_row[Alpha_a1_idx] = (0.0335*(exp((0.0109*V))));
    V_row[Alpha_i_idx] = (0.0703*(exp((0.0287*(V+5.)))));
    V_row[Beta_Na11_idx] = (0.1917*(exp(((-(V+2.5))/20.3))));
    V_row[Beta_Na12_idx] = (0.2*(exp(((-(V-(2.5)))/20.3))));
    V_row[Beta_Na13_idx] = (0.22*(exp(((-(V-(7.5)))/20.3))));
    V_row[Beta_Na3_idx] = (0.0084+(0.00002*(V+7.)));
    V_row[Beta_a0_idx] = (0.047002*(exp((-0.0631*V))));
    V_row[Beta_a1_idx] = (0.0000689*(exp((-0.04178*V))));
    V_row[Beta_i_idx] = (0.006497*(exp((-0.03268*(V+5.)))));
    double FVRT = ((p->F*V)/(p->R*p->T));
    V_row[O_ClCa_idx] = (0.2/(1.+(exp(((-(V-(46.7)))/7.8)))));
    double alpha_ato_f = (0.18064*(exp((0.03577*(V+45.)))));
    double alpha_nKs = ((V==-26.5) ? (0.00000481333/0.128) : ((0.00000481333*(V+26.5))/(1.-((exp((-0.128*(V+26.5))))))));
    double ass = (1./(1.+(exp(((-(V+6.19))/9.6)))));
    double beta_ato_f = (0.3956*(exp((-0.06237*(V+45.)))));
    double beta_nKs = (0.0000953333*(exp((-0.038*(V+26.5)))));
    double expVL = (exp(((V-(p->V_L))/p->delta_V_L)));
    V_row[f_NaK_idx] = (1./((1.+(0.1245*(exp((((-0.1*V)*p->F)/(p->R*p->T))))))+((0.0365*sigma)*(exp((((-V)*p->F)/(p->R*p->T)))))));
    double iss = (1./(1.+(exp(((V+42.1)/5.4)))));
    double ito_f_inf = (1./(1.+(exp(((V+51.4)/5.)))));
    double tau_aKss = ((39.3*(exp((-0.05*V))))+13.17);
    double tau_ato_s = ((0.493*(exp((-0.0629*V))))+2.058);
    double tau_aur = ((0.493*(exp((-0.0629*V))))+2.058);
    double tau_ito_f = (9.6645+(10.9362/(1.+(exp(((V+51.4)/5.))))));
    double tau_ito_s = (270.+(1050./(1.+(exp(((V+45.2)/5.7))))));
    double tau_iur = (p->tau_i_const+(1000./(1.+(exp(((V+42.1)/5.4))))));
    double tau_y_gate = (p->y_gate_tau_const1+(p->y_gate_tau_const2/(1.+(exp(((V+30.)/4.))))));
    double y_gate_inf = ((1./(1.+(exp(((V+33.)/p->const5)))))+(0.1/(1.+(exp((((-V)+40.)/6.))))));
    V_row[Alpha_Na4_idx] = (V_row[Alpha_Na2_idx]/1000.);
    V_row[Alpha_Na5_idx] = (V_row[Alpha_Na2_idx]/95000.);
    V_row[Alpha_p_idx] = (expVL/(p->t_L*(expVL+1.)));
    V_row[Beta_Na2_idx] = (((V_row[Alpha_Na13_idx]*V_row[Alpha_Na2_idx])*V_row[Alpha_Na3_idx])/(V_row[Beta_Na13_idx]*V_row[Beta_Na3_idx]));
    V_row[Beta_Na4_idx] = V_row[Alpha_Na3_idx];
    V_row[Beta_Na5_idx] = (V_row[Alpha_Na3_idx]/50.);
    V_row[FVRT_Ca_idx] = (2.*FVRT);
    double aKss_inf = ass;
    V_row[aKss_rush_larsen_B_idx] = (exp(((-dt)/tau_aKss)));
    double aKss_rush_larsen_C = (expm1(((-dt)/tau_aKss)));
    V_row[ato_f_rush_larsen_A_idx] = (((-alpha_ato_f)/(alpha_ato_f+beta_ato_f))*(expm1(((-dt)*(alpha_ato_f+beta_ato_f)))));
    V_row[ato_f_rush_larsen_B_idx] = (exp(((-dt)*(alpha_ato_f+beta_ato_f))));
    double ato_s_inf = ass;
    V_row[ato_s_rush_larsen_B_idx] = (exp(((-dt)/tau_ato_s)));
    double ato_s_rush_larsen_C = (expm1(((-dt)/tau_ato_s)));
    double aur_inf = ass;
    V_row[aur_rush_larsen_B_idx] = (exp(((-dt)/tau_aur)));
    double aur_rush_larsen_C = (expm1(((-dt)/tau_aur)));
    V_row[epsilon_m_idx] = ((p->b*(expVL+p->a))/(p->tau_L*((p->b*expVL)+p->a)));
    V_row[epsilon_p_idx] = ((expVL+p->a)/((p->tau_L*p->K_L)*(expVL+1.)));
    V_row[ito_f_rush_larsen_B_idx] = (exp(((-dt)/tau_ito_f)));
    double ito_f_rush_larsen_C = (expm1(((-dt)/tau_ito_f)));
    double ito_s_inf = iss;
    V_row[ito_s_rush_larsen_B_idx] = (exp(((-dt)/tau_ito_s)));
    double ito_s_rush_larsen_C = (expm1(((-dt)/tau_ito_s)));
    double iur_inf = iss;
    V_row[iur_rush_larsen_B_idx] = (exp(((-dt)/tau_iur)));
    double iur_rush_larsen_C = (expm1(((-dt)/tau_iur)));
    V_row[nKs_rush_larsen_A_idx] = (((-alpha_nKs)/(alpha_nKs+beta_nKs))*(expm1(((-dt)*(alpha_nKs+beta_nKs)))));
    V_row[nKs_rush_larsen_B_idx] = (exp(((-dt)*(alpha_nKs+beta_nKs))));
    V_row[y_gate_rush_larsen_B_idx] = (exp(((-dt)/tau_y_gate)));
    double y_gate_rush_larsen_C = (expm1(((-dt)/tau_y_gate)));
    V_row[aKss_rush_larsen_A_idx] = ((-aKss_inf)*aKss_rush_larsen_C);
    V_row[ato_s_rush_larsen_A_idx] = ((-ato_s_inf)*ato_s_rush_larsen_C);
    V_row[aur_rush_larsen_A_idx] = ((-aur_inf)*aur_rush_larsen_C);
    V_row[ito_f_rush_larsen_A_idx] = ((-ito_f_inf)*ito_f_rush_larsen_C);
    V_row[ito_s_rush_larsen_A_idx] = ((-ito_s_inf)*ito_s_rush_larsen_C);
    V_row[iur_rush_larsen_A_idx] = ((-iur_inf)*iur_rush_larsen_C);
    V_row[y_gate_rush_larsen_A_idx] = ((-y_gate_inf)*y_gate_rush_larsen_C);
  }
  check_LUT(V_tab);
  

}

struct Bondarenko_Regional_Constants {
  GlobalData_t Alpha_m;
  GlobalData_t partial_diff_P_C2_del_P_C2;
  GlobalData_t partial_diff_P_C2_del_P_O1;
  GlobalData_t partial_diff_P_O2_del_P_O2;
  GlobalData_t sigma;

};

struct Bondarenko_Nodal_Req {
  GlobalData_t V;

};

struct Bondarenko_Private {
  ION_IF *IF;
  int   node_number;
  void* cvode_mem;
  bool  trace_init;
  Bondarenko_Regional_Constants rc;
  Bondarenko_Nodal_Req nr;
};

//Printing out the Rosenbrock declarations
void Bondarenko_rosenbrock_f(float*, float*, void*);
void Bondarenko_rosenbrock_jacobian(float**, float*, void*, int);

enum Rosenbrock {
  ROSEN_P_C2,
  ROSEN_P_O1,
  ROSEN_P_O2,

  N_ROSEN
};

void rbStepX  ( float *X,
                void (*calcDX) (float*,  float*, void*),
                void (*calcJ)  (float**, float*, void*, int ),
                void *params, float h, int N );




void    initialize_sv_Bondarenko( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Bondarenko_Params *p = (Bondarenko_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Bondarenko_state) );
  Bondarenko_state *sv_base = (Bondarenko_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double Alpha_m = (p->phi_L/p->t_L);
  double partial_diff_P_C2_del_P_C2 = (-p->k_minus_c);
  double partial_diff_P_C2_del_P_O1 = p->k_plus_c;
  double partial_diff_P_O2_del_P_O2 = (-p->k_minus_b);
  double sigma = ((1./7.)*((exp((p->Nao/67300.)))-(1.)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Bondarenko_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Ki *= 1e3;
    sv->Nai *= 1e3;
    
    
    // Initialize the rest of the nodal variables
    sv->C_K1 = p->C_K1_init;
    sv->C_K2 = p->C_K2_init;
    sv->C_Na1 = p->C_Na1_init;
    sv->C_Na2 = p->C_Na2_init;
    sv->CaJSR = p->CaJSR_init;
    sv->CaMKt = p->CaMKt_init;
    sv->CaNSR = p->CaNSR_init;
    sv->Cai = p->Cai_init;
    sv->Cass = p->Cass_init;
    sv->I = p->I_init;
    sv->I1_Na = p->I1_Na_init;
    sv->I2_Na = p->I2_Na_init;
    sv->IC_Na2 = p->IC_Na2_init;
    sv->IC_Na3 = p->IC_Na3_init;
    sv->IF_Na = p->IF_Na_init;
    sv->I_K = p->I_K_init;
    sv->Ki = p->Ki_init;
    sv->Nai = p->Nai_init;
    sv->O = p->O_init;
    sv->O_K = p->O_K_init;
    sv->O_Na = p->O_Na_init;
    sv->P_C2 = p->P_C2_init;
    sv->P_O1 = p->P_O1_init;
    sv->P_O2 = p->P_O2_init;
    sv->P_RyR = p->P_RyR_init;
    V = p->V_init;
    sv->aKss = p->aKss_init;
    sv->ato_f = p->ato_f_init;
    sv->ato_s = p->ato_s_init;
    sv->aur = p->aur_init;
    sv->ito_f = p->ito_f_init;
    sv->ito_s = p->ito_s_init;
    sv->iur = p->iur_init;
    sv->nKs = p->nKs_init;
    sv->y_gate = p->y_gate_init;
    double E_CaN = (((p->R*p->T)/(2.*p->F))*(log((p->Cao/sv->Cai))));
    double E_K = (((p->R*p->T)/p->F)*(log((p->Ko/sv->Ki))));
    double E_Na = (((p->R*p->T)/p->F)*(log((((0.9*p->Nao)+(0.1*p->Ko))/((0.9*sv->Nai)+(0.1*sv->Ki))))));
    double FVRT = ((p->F*V)/(p->R*p->T));
    double IKr = ((p->GKr*sv->O_K)*(V-((((p->R*p->T)/p->F)*(log((((0.98*p->Ko)+(0.02*p->Nao))/((0.98*sv->Ki)+(0.02*sv->Nai)))))))));
    double INaCa = (((p->V_max_NCX/(1.+(square((p->K_mAllo/sv->Cai)))))*((((cube(sv->Nai))*p->Cao)*(exp((((p->eta*V)*p->F)/(p->R*p->T)))))-((((cube(p->Nao))*sv->Cai)*(exp(((((p->eta-(1.))*V)*p->F)/(p->R*p->T))))))))/(((((((p->K_mCao*(cube(sv->Nai)))+((cube(p->K_mNao))*sv->Cai))+(((cube(p->K_mNai))*p->Cao)*(1.+(sv->Cai/p->K_mCai))))+((p->K_mCai*(cube(p->Nao)))*(1.+(cube((sv->Nai/p->K_mNai))))))+((cube(sv->Nai))*p->Cao))+((cube(p->Nao))*sv->Cai))*(1.+(p->k_sat*(exp(((((p->eta-(1.))*V)*p->F)/(p->R*p->T))))))));
    double IpCa = ((p->IpCa_max*(square(sv->Cai)))/((square(p->Km_pCa))+(square(sv->Cai))));
    double O_ClCa = (0.2/(1.+(exp(((-(V-(46.7)))/7.8)))));
    double f_NaK = (1./((1.+(0.1245*(exp((((-0.1*V)*p->F)/(p->R*p->T))))))+((0.0365*sigma)*(exp((((-V)*p->F)/(p->R*p->T)))))));
    double FVRT_Ca = (2.*FVRT);
    double IClCa = ((((p->GClCa*O_ClCa)*sv->Cai)/(sv->Cai+p->Km_Cl))*(V-(p->E_Cl)));
    double IK1 = ((((p->GK1*p->Ko)/(p->Ko+210.))*(V-(E_K)))/(1.+(exp((0.0896*(V-(E_K)))))));
    double IKs = ((p->GKs*(square(sv->nKs)))*(V-(E_K)));
    double IKss = ((p->GKss*sv->aKss)*(V-(E_K)));
    double IKtof = (((p->GKto_f*(cube(sv->ato_f)))*sv->ito_f)*(V-(E_K)));
    double IKtos = (((p->GKto_s*sv->ato_s)*sv->ito_s)*(V-(E_K)));
    double IKur = (((p->GKur*sv->aur)*sv->iur)*(V-(E_K)));
    double INa = ((p->GNa*sv->O_Na)*(V-(E_Na)));
    double INaK = ((((p->maxINaK*f_NaK)/(1.+(pow((p->Km_Nai/sv->Nai),2.4))))*p->Ko)/(p->Ko+p->Km_Ko));
    double IbCa = (p->GbCa*(V-(E_CaN)));
    double IbNa = (p->GNab*(V-(E_Na)));
    double i_CaL = (((fabs(FVRT_Ca))>0.00001) ? ((((((((((-p->P_CaL)*2.)*p->Vss)*p->F)/(p->Acap*p->Cm))*sv->O)*sv->y_gate)*FVRT_Ca)/(1.-((exp((-FVRT_Ca))))))*((p->Cao*(exp((-FVRT_Ca))))-(sv->Cass))) : ((((((((((-p->P_CaL)*2.)*p->Vss)*p->F)/(p->Acap*p->Cm))*sv->O)*sv->y_gate)*0.00001)/(1.-((exp(-0.00001)))))*((p->Cao*(exp(-0.00001)))-(sv->Cass))));
    Iion = ((((((((((((((i_CaL+IpCa)+INaCa)+IbCa)+INa)+IbNa)+INaK)+IKtof)+IKtos)+IK1)+IKs)+IKur)+IKss)+IKr)+IClCa);
    //Change the units of external variables as appropriate.
    sv->Ki *= 1e-3;
    sv->Nai *= 1e-3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


  int num_thread = 1;
#ifdef _OPENMP
  num_thread = omp_get_max_threads();
#endif
  Bondarenko_Private* userdata = (Bondarenko_Private*)IMP_malloc(num_thread,sizeof(Bondarenko_Private));
  for( int j=0; j<num_thread; j++ ){
    userdata[j].IF = IF;
    userdata[j].node_number = 0;
  }
  IF->ion_private = userdata;
  IF->private_sz  = sizeof(userdata[0]);


}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Bondarenko(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Bondarenko_Params *p  = (Bondarenko_Params *)IF->params;
  Bondarenko_state *sv_base = (Bondarenko_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Alpha_m = (p->phi_L/p->t_L);
  GlobalData_t partial_diff_P_C2_del_P_C2 = (-p->k_minus_c);
  GlobalData_t partial_diff_P_C2_del_P_O1 = p->k_plus_c;
  GlobalData_t partial_diff_P_O2_del_P_O2 = (-p->k_minus_b);
  GlobalData_t sigma = ((1./7.)*((exp((p->Nao/67300.)))-(1.)));
  
  //Initializing the userdata structures.
  Bondarenko_Private* ion_private = (Bondarenko_Private*) IF->ion_private;
  int nthread = 1;
  #ifdef _OPENMP
  nthread = omp_get_max_threads();
  #endif
  for( int j=0; j<nthread; j++ ) {
    ion_private[j].rc.Alpha_m = Alpha_m;
    ion_private[j].rc.partial_diff_P_C2_del_P_C2 = partial_diff_P_C2_del_P_C2;
    ion_private[j].rc.partial_diff_P_C2_del_P_O1 = partial_diff_P_C2_del_P_O1;
    ion_private[j].rc.partial_diff_P_O2_del_P_O2 = partial_diff_P_O2_del_P_O2;
    ion_private[j].rc.sigma = sigma;
  }
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Bondarenko_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Ki *= 1e3;
    sv->Nai *= 1e3;
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t E_CaN = (((p->R*p->T)/(2.*p->F))*(log((p->Cao/sv->Cai))));
    GlobalData_t E_K = (((p->R*p->T)/p->F)*(log((p->Ko/sv->Ki))));
    GlobalData_t E_Na = (((p->R*p->T)/p->F)*(log((((0.9*p->Nao)+(0.1*p->Ko))/((0.9*sv->Nai)+(0.1*sv->Ki))))));
    GlobalData_t IKr = ((p->GKr*sv->O_K)*(V-((((p->R*p->T)/p->F)*(log((((0.98*p->Ko)+(0.02*p->Nao))/((0.98*sv->Ki)+(0.02*sv->Nai)))))))));
    GlobalData_t INaCa = (((p->V_max_NCX/(1.+(square((p->K_mAllo/sv->Cai)))))*((((cube(sv->Nai))*p->Cao)*(exp((((p->eta*V)*p->F)/(p->R*p->T)))))-((((cube(p->Nao))*sv->Cai)*(exp(((((p->eta-(1.))*V)*p->F)/(p->R*p->T))))))))/(((((((p->K_mCao*(cube(sv->Nai)))+((cube(p->K_mNao))*sv->Cai))+(((cube(p->K_mNai))*p->Cao)*(1.+(sv->Cai/p->K_mCai))))+((p->K_mCai*(cube(p->Nao)))*(1.+(cube((sv->Nai/p->K_mNai))))))+((cube(sv->Nai))*p->Cao))+((cube(p->Nao))*sv->Cai))*(1.+(p->k_sat*(exp(((((p->eta-(1.))*V)*p->F)/(p->R*p->T))))))));
    GlobalData_t IpCa = ((p->IpCa_max*(square(sv->Cai)))/((square(p->Km_pCa))+(square(sv->Cai))));
    GlobalData_t IClCa = ((((p->GClCa*V_row[O_ClCa_idx])*sv->Cai)/(sv->Cai+p->Km_Cl))*(V-(p->E_Cl)));
    GlobalData_t IK1 = ((((p->GK1*p->Ko)/(p->Ko+210.))*(V-(E_K)))/(1.+(exp((0.0896*(V-(E_K)))))));
    GlobalData_t IKs = ((p->GKs*(square(sv->nKs)))*(V-(E_K)));
    GlobalData_t IKss = ((p->GKss*sv->aKss)*(V-(E_K)));
    GlobalData_t IKtof = (((p->GKto_f*(cube(sv->ato_f)))*sv->ito_f)*(V-(E_K)));
    GlobalData_t IKtos = (((p->GKto_s*sv->ato_s)*sv->ito_s)*(V-(E_K)));
    GlobalData_t IKur = (((p->GKur*sv->aur)*sv->iur)*(V-(E_K)));
    GlobalData_t INa = ((p->GNa*sv->O_Na)*(V-(E_Na)));
    GlobalData_t INaK = ((((p->maxINaK*V_row[f_NaK_idx])/(1.+(pow((p->Km_Nai/sv->Nai),2.4))))*p->Ko)/(p->Ko+p->Km_Ko));
    GlobalData_t IbCa = (p->GbCa*(V-(E_CaN)));
    GlobalData_t IbNa = (p->GNab*(V-(E_Na)));
    GlobalData_t i_CaL = (((fabs(V_row[FVRT_Ca_idx]))>0.00001) ? ((((((((((-p->P_CaL)*2.)*p->Vss)*p->F)/(p->Acap*p->Cm))*sv->O)*sv->y_gate)*V_row[FVRT_Ca_idx])/(1.-((exp((-V_row[FVRT_Ca_idx]))))))*((p->Cao*(exp((-V_row[FVRT_Ca_idx]))))-(sv->Cass))) : ((((((((((-p->P_CaL)*2.)*p->Vss)*p->F)/(p->Acap*p->Cm))*sv->O)*sv->y_gate)*0.00001)/(1.-((exp(-0.00001)))))*((p->Cao*(exp(-0.00001)))-(sv->Cass))));
    Iion = ((((((((((((((i_CaL+IpCa)+INaCa)+IbCa)+INa)+IbNa)+INaK)+IKtof)+IKtos)+IK1)+IKs)+IKur)+IKss)+IKr)+IClCa);
    
    
    //Complete Forward Euler Update
    GlobalData_t BJSR = (1./(1.+((p->CSQN_tot*p->Km_CSQN)/(square((p->Km_CSQN+sv->CaJSR))))));
    GlobalData_t Bi = (1./(1.+((p->Bmax*p->Kd)/(square((p->Kd+sv->Cai))))));
    GlobalData_t Bss = (1./(1.+((p->Bmax*p->Kd)/(square((p->Kd+sv->Cass))))));
    GlobalData_t C = ((1.-(sv->O))-(sv->I));
    GlobalData_t C_K0 = (1.-((((sv->C_K1+sv->C_K2)+sv->O_K)+sv->I_K)));
    GlobalData_t C_Na3 = (1.-((((((((sv->O_Na+sv->C_Na1)+sv->C_Na2)+sv->IF_Na)+sv->I1_Na)+sv->I2_Na)+sv->IC_Na2)+sv->IC_Na3)));
    GlobalData_t CaMKb = ((0.05*(1.-(sv->CaMKt)))/(1.+(0.7/sv->Cass)));
    GlobalData_t J_leak = (p->v2*(sv->CaNSR-(sv->Cai)));
    GlobalData_t J_rel = (((p->v1*(sv->P_O1+sv->P_O2))*(sv->CaJSR-(sv->Cass)))*sv->P_RyR);
    GlobalData_t J_tr = ((sv->CaNSR-(sv->CaJSR))/p->tau_tr);
    GlobalData_t J_xfer = ((sv->Cass-(sv->Cai))/p->tau_xfer);
    GlobalData_t diff_Ki = ((((-(((((((IKtof+IKtos)+IK1)+IKs)+IKss)+IKur)+IKr)-((2.*INaK))))*p->Acap)*p->Cm)/(p->Vmyo*p->F));
    GlobalData_t diff_Nai = ((((-(((INa+IbNa)+(3.*INaK))+(3.*INaCa)))*p->Acap)*p->Cm)/(p->Vmyo*p->F));
    GlobalData_t diff_P_RyR = ((p->P_ryr_const1*sv->P_RyR)+(((p->P_ryr_const2*i_CaL)/p->ICaLmax)*(exp(((-(square((V-(5.)))))/648.)))));
    GlobalData_t CaMKa = (CaMKb+sv->CaMKt);
    GlobalData_t diff_C_K1 = (((V_row[Alpha_a0_idx]*C_K0)+(p->kb*sv->C_K2))-(((V_row[Beta_a0_idx]*sv->C_K1)+(p->kf*sv->C_K1))));
    GlobalData_t diff_C_K2 = (((p->kf*sv->C_K1)+(V_row[Beta_a1_idx]*sv->O_K))-(((p->kb*sv->C_K2)+(V_row[Alpha_a1_idx]*sv->C_K2))));
    GlobalData_t diff_C_Na1 = ((((V_row[Alpha_Na12_idx]*sv->C_Na2)+(V_row[Beta_Na13_idx]*sv->O_Na))+(V_row[Alpha_Na3_idx]*sv->IF_Na))-((((V_row[Beta_Na12_idx]*sv->C_Na1)+(V_row[Alpha_Na13_idx]*sv->C_Na1))+(V_row[Beta_Na3_idx]*sv->C_Na1))));
    GlobalData_t diff_C_Na2 = ((((V_row[Alpha_Na11_idx]*C_Na3)+(V_row[Beta_Na12_idx]*sv->C_Na1))+(V_row[Alpha_Na3_idx]*sv->IC_Na2))-((((V_row[Beta_Na11_idx]*sv->C_Na2)+(V_row[Alpha_Na12_idx]*sv->C_Na2))+(V_row[Beta_Na3_idx]*sv->C_Na2))));
    GlobalData_t diff_CaJSR = (BJSR*(J_tr-(J_rel)));
    GlobalData_t diff_CaMKt = (((p->on_rate*CaMKb)*(CaMKb+sv->CaMKt))-((p->off_rate*sv->CaMKt)));
    GlobalData_t diff_Cass = (Bss*(((J_rel*p->VJSR)/p->Vss)-((((J_xfer*p->Vmyo)/p->Vss)+(((i_CaL*p->Acap)*p->Cm)/((2.*p->Vss)*p->F))))));
    GlobalData_t diff_IC_Na2 = ((((V_row[Alpha_Na11_idx]*sv->IC_Na3)+(V_row[Beta_Na12_idx]*sv->IF_Na))+(V_row[Beta_Na3_idx]*sv->C_Na2))-((((V_row[Beta_Na11_idx]*sv->IC_Na2)+(V_row[Alpha_Na12_idx]*sv->IC_Na2))+(V_row[Alpha_Na3_idx]*sv->IC_Na2))));
    GlobalData_t diff_IC_Na3 = (((V_row[Beta_Na11_idx]*sv->IC_Na2)+(V_row[Beta_Na3_idx]*C_Na3))-(((V_row[Alpha_Na11_idx]*sv->IC_Na3)+(V_row[Alpha_Na3_idx]*sv->IC_Na3))));
    GlobalData_t diff_I_K = ((V_row[Alpha_i_idx]*sv->O_K)-((V_row[Beta_i_idx]*sv->I_K)));
    GlobalData_t diff_O_K = (((V_row[Alpha_a1_idx]*sv->C_K2)+(V_row[Beta_i_idx]*sv->I_K))-(((V_row[Beta_a1_idx]*sv->O_K)+(V_row[Alpha_i_idx]*sv->O_K))));
    GlobalData_t diff_I = (((V_row[epsilon_p_idx]*sv->Cass)*C)-((V_row[epsilon_m_idx]*sv->I)));
    GlobalData_t diff_I1_Na = (((V_row[Alpha_Na4_idx]*sv->IF_Na)+(V_row[Beta_Na5_idx]*sv->I2_Na))-(((V_row[Beta_Na4_idx]*sv->I1_Na)+(V_row[Alpha_Na5_idx]*sv->I1_Na))));
    GlobalData_t diff_I2_Na = ((V_row[Alpha_Na5_idx]*sv->I1_Na)-((V_row[Beta_Na5_idx]*sv->I2_Na)));
    GlobalData_t diff_IF_Na = (((((V_row[Alpha_Na2_idx]*sv->O_Na)+(V_row[Beta_Na3_idx]*sv->C_Na1))+(V_row[Beta_Na4_idx]*sv->I1_Na))+(V_row[Alpha_Na12_idx]*sv->IC_Na2))-(((((V_row[Beta_Na2_idx]*sv->IF_Na)+(V_row[Alpha_Na3_idx]*sv->IF_Na))+(V_row[Alpha_Na4_idx]*sv->IF_Na))+(V_row[Beta_Na12_idx]*sv->IF_Na))));
    GlobalData_t diff_O = ((V_row[Alpha_p_idx]*C)-((Alpha_m*sv->O)));
    GlobalData_t diff_O_Na = (((V_row[Alpha_Na13_idx]*sv->C_Na1)+(V_row[Beta_Na2_idx]*sv->IF_Na))-(((V_row[Beta_Na13_idx]*sv->O_Na)+(V_row[Alpha_Na2_idx]*sv->O_Na))));
    GlobalData_t vmup = ((((2.9982*(pow(CaMKa,2.583)))/((pow(1.2444,2.583))+(pow(CaMKa,2.583))))+1.)*p->vmup_init);
    GlobalData_t J_serca = ((vmup*(square(sv->Cai)))/((square(p->Km_up))+(square(sv->Cai))));
    GlobalData_t diff_CaNSR = ((((J_serca-(J_leak))*p->Vmyo)/p->VNSR)-(((J_tr*p->VJSR)/p->VNSR)));
    GlobalData_t diff_Cai = (Bi*(((J_leak+J_xfer)-((((((IbCa+IpCa)-((2.*INaCa)))*p->Acap)*p->Cm)/((2.*p->Vmyo)*p->F))))-(J_serca)));
    GlobalData_t C_K1_new = sv->C_K1+diff_C_K1*dt;
    GlobalData_t C_K2_new = sv->C_K2+diff_C_K2*dt;
    GlobalData_t C_Na1_new = sv->C_Na1+diff_C_Na1*dt;
    GlobalData_t C_Na2_new = sv->C_Na2+diff_C_Na2*dt;
    GlobalData_t CaJSR_new = sv->CaJSR+diff_CaJSR*dt;
    GlobalData_t CaMKt_new = sv->CaMKt+diff_CaMKt*dt;
    GlobalData_t CaNSR_new = sv->CaNSR+diff_CaNSR*dt;
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    GlobalData_t Cass_new = sv->Cass+diff_Cass*dt;
    GlobalData_t I_new = sv->I+diff_I*dt;
    GlobalData_t I1_Na_new = sv->I1_Na+diff_I1_Na*dt;
    GlobalData_t I2_Na_new = sv->I2_Na+diff_I2_Na*dt;
    GlobalData_t IC_Na2_new = sv->IC_Na2+diff_IC_Na2*dt;
    GlobalData_t IC_Na3_new = sv->IC_Na3+diff_IC_Na3*dt;
    GlobalData_t IF_Na_new = sv->IF_Na+diff_IF_Na*dt;
    GlobalData_t I_K_new = sv->I_K+diff_I_K*dt;
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t O_new = sv->O+diff_O*dt;
    GlobalData_t O_K_new = sv->O_K+diff_O_K*dt;
    GlobalData_t O_Na_new = sv->O_Na+diff_O_Na*dt;
    GlobalData_t P_RyR_new = sv->P_RyR+diff_P_RyR*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t aKss_rush_larsen_B = V_row[aKss_rush_larsen_B_idx];
    GlobalData_t ato_f_rush_larsen_A = V_row[ato_f_rush_larsen_A_idx];
    GlobalData_t ato_f_rush_larsen_B = V_row[ato_f_rush_larsen_B_idx];
    GlobalData_t ato_s_rush_larsen_B = V_row[ato_s_rush_larsen_B_idx];
    GlobalData_t aur_rush_larsen_B = V_row[aur_rush_larsen_B_idx];
    GlobalData_t ito_f_rush_larsen_B = V_row[ito_f_rush_larsen_B_idx];
    GlobalData_t ito_s_rush_larsen_B = V_row[ito_s_rush_larsen_B_idx];
    GlobalData_t iur_rush_larsen_B = V_row[iur_rush_larsen_B_idx];
    GlobalData_t nKs_rush_larsen_A = V_row[nKs_rush_larsen_A_idx];
    GlobalData_t nKs_rush_larsen_B = V_row[nKs_rush_larsen_B_idx];
    GlobalData_t y_gate_rush_larsen_B = V_row[y_gate_rush_larsen_B_idx];
    GlobalData_t aKss_rush_larsen_A = V_row[aKss_rush_larsen_A_idx];
    GlobalData_t ato_s_rush_larsen_A = V_row[ato_s_rush_larsen_A_idx];
    GlobalData_t aur_rush_larsen_A = V_row[aur_rush_larsen_A_idx];
    GlobalData_t ito_f_rush_larsen_A = V_row[ito_f_rush_larsen_A_idx];
    GlobalData_t ito_s_rush_larsen_A = V_row[ito_s_rush_larsen_A_idx];
    GlobalData_t iur_rush_larsen_A = V_row[iur_rush_larsen_A_idx];
    GlobalData_t y_gate_rush_larsen_A = V_row[y_gate_rush_larsen_A_idx];
    GlobalData_t aKss_new = aKss_rush_larsen_A+aKss_rush_larsen_B*sv->aKss;
    GlobalData_t ato_f_new = ato_f_rush_larsen_A+ato_f_rush_larsen_B*sv->ato_f;
    GlobalData_t ato_s_new = ato_s_rush_larsen_A+ato_s_rush_larsen_B*sv->ato_s;
    GlobalData_t aur_new = aur_rush_larsen_A+aur_rush_larsen_B*sv->aur;
    GlobalData_t ito_f_new = ito_f_rush_larsen_A+ito_f_rush_larsen_B*sv->ito_f;
    GlobalData_t ito_s_new = ito_s_rush_larsen_A+ito_s_rush_larsen_B*sv->ito_s;
    GlobalData_t iur_new = iur_rush_larsen_A+iur_rush_larsen_B*sv->iur;
    GlobalData_t nKs_new = nKs_rush_larsen_A+nKs_rush_larsen_B*sv->nKs;
    GlobalData_t y_gate_new = y_gate_rush_larsen_A+y_gate_rush_larsen_B*sv->y_gate;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    int thread=0;
    #ifdef _OPENMP
    thread = omp_get_thread_num();
    #endif
    float Rosenbrock_X[N_ROSEN];
    Rosenbrock_X[ROSEN_P_C2] = sv->P_C2;
    Rosenbrock_X[ROSEN_P_O1] = sv->P_O1;
    Rosenbrock_X[ROSEN_P_O2] = sv->P_O2;
    ion_private[thread].nr.V = V;
    ion_private[thread].node_number = __i;
    
    rbStepX(Rosenbrock_X, Bondarenko_rosenbrock_f, Bondarenko_rosenbrock_jacobian, (void*)(ion_private+thread), dt, N_ROSEN);
    GlobalData_t P_C2_new = Rosenbrock_X[ROSEN_P_C2];
    GlobalData_t P_O1_new = Rosenbrock_X[ROSEN_P_O1];
    GlobalData_t P_O2_new = Rosenbrock_X[ROSEN_P_O2];
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->C_K1 = C_K1_new;
    sv->C_K2 = C_K2_new;
    sv->C_Na1 = C_Na1_new;
    sv->C_Na2 = C_Na2_new;
    sv->CaJSR = CaJSR_new;
    sv->CaMKt = CaMKt_new;
    sv->CaNSR = CaNSR_new;
    sv->Cai = Cai_new;
    sv->Cass = Cass_new;
    sv->I = I_new;
    sv->I1_Na = I1_Na_new;
    sv->I2_Na = I2_Na_new;
    sv->IC_Na2 = IC_Na2_new;
    sv->IC_Na3 = IC_Na3_new;
    sv->IF_Na = IF_Na_new;
    sv->I_K = I_K_new;
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->O = O_new;
    sv->O_K = O_K_new;
    sv->O_Na = O_Na_new;
    sv->P_C2 = P_C2_new;
    sv->P_O1 = P_O1_new;
    sv->P_O2 = P_O2_new;
    sv->P_RyR = P_RyR_new;
    sv->aKss = aKss_new;
    sv->ato_f = ato_f_new;
    sv->ato_s = ato_s_new;
    sv->aur = aur_new;
    sv->ito_f = ito_f_new;
    sv->ito_s = ito_s_new;
    sv->iur = iur_new;
    sv->nKs = nKs_new;
    sv->y_gate = y_gate_new;
    //Change the units of external variables as appropriate.
    sv->Ki *= 1e-3;
    sv->Nai *= 1e-3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}

void Bondarenko_rosenbrock_f(float* Rosenbrock_DX, float* Rosenbrock_X, void* user_data) {
  Bondarenko_Private* ion_private = (Bondarenko_Private*)user_data;
  Bondarenko_Regional_Constants* rc = &ion_private->rc;
  ION_IF* IF = ion_private->IF;
  int __i = ((Bondarenko_Private*)user_data)->node_number;
  Bondarenko_Nodal_Req* nr = &ion_private->nr;
  cell_geom *region = &IF->cgeom;
  Bondarenko_Params *p = (Bondarenko_Params *)IF->params;
  Bondarenko_state *sv_base = (Bondarenko_state *)IF->sv_tab.y;
  Bondarenko_state *sv = sv_base+__i;

  LUT_data_t V_row[NROWS_V];
  LUT_interpRow(&IF->tables[V_TAB], nr->V, __i, V_row);
  GlobalData_t P_C1 = (1.-(((Rosenbrock_X[ROSEN_P_C2]+Rosenbrock_X[ROSEN_P_O1])+Rosenbrock_X[ROSEN_P_O2])));
  GlobalData_t diff_P_C2 = ((p->k_plus_c*Rosenbrock_X[ROSEN_P_O1])-((p->k_minus_c*Rosenbrock_X[ROSEN_P_C2])));
  GlobalData_t diff_P_O2 = (((p->k_plus_b*(pow(sv->Cass,p->m)))*Rosenbrock_X[ROSEN_P_O1])-((p->k_minus_b*Rosenbrock_X[ROSEN_P_O2])));
  GlobalData_t diff_P_O1 = (((((p->k_plus_a*(pow(sv->Cass,p->n)))*P_C1)+(p->k_minus_b*Rosenbrock_X[ROSEN_P_O2]))+(p->k_minus_c*Rosenbrock_X[ROSEN_P_C2]))-((((p->k_minus_a*Rosenbrock_X[ROSEN_P_O1])+((p->k_plus_b*(pow(sv->Cass,p->m)))*Rosenbrock_X[ROSEN_P_O1]))+(p->k_plus_c*Rosenbrock_X[ROSEN_P_O1]))));
  Rosenbrock_DX[ROSEN_P_C2] = diff_P_C2;
  Rosenbrock_DX[ROSEN_P_O1] = diff_P_O1;
  Rosenbrock_DX[ROSEN_P_O2] = diff_P_O2;

}

void Bondarenko_rosenbrock_jacobian(float** Rosenbrock_jacobian, float* Rosenbrock_X, void* user_data, int dddd) {
  Bondarenko_Private* ion_private = (Bondarenko_Private*)user_data;
  Bondarenko_Regional_Constants* rc = &ion_private->rc;
  ION_IF* IF = ion_private->IF;
  int __i = ((Bondarenko_Private*)user_data)->node_number;
  Bondarenko_Nodal_Req* nr = &ion_private->nr;
  cell_geom *region = &IF->cgeom;
  Bondarenko_Params *p = (Bondarenko_Params *)IF->params;
  Bondarenko_state *sv_base = (Bondarenko_state *)IF->sv_tab.y;
  Bondarenko_state *sv = sv_base+__i;
  LUT_data_t V_row[NROWS_V];
  LUT_interpRow(&IF->tables[V_TAB], nr->V, __i, V_row);
  GlobalData_t partial_diff_P_O1_del_P_C2 = (((p->k_plus_a*(pow(sv->Cass,p->n)))*-1.)+p->k_minus_c);
  GlobalData_t partial_diff_P_O1_del_P_O1 = (((p->k_plus_a*(pow(sv->Cass,p->n)))*-1.)-(((p->k_minus_a+(p->k_plus_b*(pow(sv->Cass,p->m))))+p->k_plus_c)));
  GlobalData_t partial_diff_P_O1_del_P_O2 = (((p->k_plus_a*(pow(sv->Cass,p->n)))*-1.)+p->k_minus_b);
  GlobalData_t partial_diff_P_O2_del_P_O1 = (p->k_plus_b*(pow(sv->Cass,p->m)));
  Rosenbrock_jacobian[ROSEN_P_C2][ROSEN_P_C2] = rc->partial_diff_P_C2_del_P_C2;
  Rosenbrock_jacobian[ROSEN_P_C2][ROSEN_P_O1] = rc->partial_diff_P_C2_del_P_O1;
  Rosenbrock_jacobian[ROSEN_P_C2][ROSEN_P_O2] = partial_diff_P_C2_del_P_O2;
  Rosenbrock_jacobian[ROSEN_P_O1][ROSEN_P_C2] = partial_diff_P_O1_del_P_C2;
  Rosenbrock_jacobian[ROSEN_P_O1][ROSEN_P_O1] = partial_diff_P_O1_del_P_O1;
  Rosenbrock_jacobian[ROSEN_P_O1][ROSEN_P_O2] = partial_diff_P_O1_del_P_O2;
  Rosenbrock_jacobian[ROSEN_P_O2][ROSEN_P_C2] = partial_diff_P_O2_del_P_C2;
  Rosenbrock_jacobian[ROSEN_P_O2][ROSEN_P_O1] = partial_diff_P_O2_del_P_O1;
  Rosenbrock_jacobian[ROSEN_P_O2][ROSEN_P_O2] = rc->partial_diff_P_O2_del_P_O2;

}


void trace_Bondarenko(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Bondarenko_trace_header.txt","wt");
    fprintf(theader->fd,
        "IClCa\n"
        "IK1\n"
        "IKr\n"
        "IKs\n"
        "IKss\n"
        "IKtof\n"
        "IKtos\n"
        "IKur\n"
        "INa\n"
        "INaCa\n"
        "INaK\n"
        "IbCa\n"
        "IbNa\n"
        "IpCa\n"
        "V\n"
        "i_CaL\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Bondarenko_Params *p  = (Bondarenko_Params *)IF->params;

  Bondarenko_state *sv_base = (Bondarenko_state *)IF->sv_tab.y;
  Bondarenko_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Alpha_m = (p->phi_L/p->t_L);
  GlobalData_t partial_diff_P_C2_del_P_C2 = (-p->k_minus_c);
  GlobalData_t partial_diff_P_C2_del_P_O1 = p->k_plus_c;
  GlobalData_t partial_diff_P_O2_del_P_O2 = (-p->k_minus_b);
  GlobalData_t sigma = ((1./7.)*((exp((p->Nao/67300.)))-(1.)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  sv->Ki *= 1e3;
  sv->Nai *= 1e3;
  
  
  GlobalData_t E_CaN = (((p->R*p->T)/(2.*p->F))*(log((p->Cao/sv->Cai))));
  GlobalData_t E_K = (((p->R*p->T)/p->F)*(log((p->Ko/sv->Ki))));
  GlobalData_t E_Na = (((p->R*p->T)/p->F)*(log((((0.9*p->Nao)+(0.1*p->Ko))/((0.9*sv->Nai)+(0.1*sv->Ki))))));
  GlobalData_t FVRT = ((p->F*V)/(p->R*p->T));
  GlobalData_t IKr = ((p->GKr*sv->O_K)*(V-((((p->R*p->T)/p->F)*(log((((0.98*p->Ko)+(0.02*p->Nao))/((0.98*sv->Ki)+(0.02*sv->Nai)))))))));
  GlobalData_t INaCa = (((p->V_max_NCX/(1.+(square((p->K_mAllo/sv->Cai)))))*((((cube(sv->Nai))*p->Cao)*(exp((((p->eta*V)*p->F)/(p->R*p->T)))))-((((cube(p->Nao))*sv->Cai)*(exp(((((p->eta-(1.))*V)*p->F)/(p->R*p->T))))))))/(((((((p->K_mCao*(cube(sv->Nai)))+((cube(p->K_mNao))*sv->Cai))+(((cube(p->K_mNai))*p->Cao)*(1.+(sv->Cai/p->K_mCai))))+((p->K_mCai*(cube(p->Nao)))*(1.+(cube((sv->Nai/p->K_mNai))))))+((cube(sv->Nai))*p->Cao))+((cube(p->Nao))*sv->Cai))*(1.+(p->k_sat*(exp(((((p->eta-(1.))*V)*p->F)/(p->R*p->T))))))));
  GlobalData_t IpCa = ((p->IpCa_max*(square(sv->Cai)))/((square(p->Km_pCa))+(square(sv->Cai))));
  GlobalData_t O_ClCa = (0.2/(1.+(exp(((-(V-(46.7)))/7.8)))));
  GlobalData_t f_NaK = (1./((1.+(0.1245*(exp((((-0.1*V)*p->F)/(p->R*p->T))))))+((0.0365*sigma)*(exp((((-V)*p->F)/(p->R*p->T)))))));
  GlobalData_t FVRT_Ca = (2.*FVRT);
  GlobalData_t IClCa = ((((p->GClCa*O_ClCa)*sv->Cai)/(sv->Cai+p->Km_Cl))*(V-(p->E_Cl)));
  GlobalData_t IK1 = ((((p->GK1*p->Ko)/(p->Ko+210.))*(V-(E_K)))/(1.+(exp((0.0896*(V-(E_K)))))));
  GlobalData_t IKs = ((p->GKs*(square(sv->nKs)))*(V-(E_K)));
  GlobalData_t IKss = ((p->GKss*sv->aKss)*(V-(E_K)));
  GlobalData_t IKtof = (((p->GKto_f*(cube(sv->ato_f)))*sv->ito_f)*(V-(E_K)));
  GlobalData_t IKtos = (((p->GKto_s*sv->ato_s)*sv->ito_s)*(V-(E_K)));
  GlobalData_t IKur = (((p->GKur*sv->aur)*sv->iur)*(V-(E_K)));
  GlobalData_t INa = ((p->GNa*sv->O_Na)*(V-(E_Na)));
  GlobalData_t INaK = ((((p->maxINaK*f_NaK)/(1.+(pow((p->Km_Nai/sv->Nai),2.4))))*p->Ko)/(p->Ko+p->Km_Ko));
  GlobalData_t IbCa = (p->GbCa*(V-(E_CaN)));
  GlobalData_t IbNa = (p->GNab*(V-(E_Na)));
  GlobalData_t i_CaL = (((fabs(FVRT_Ca))>0.00001) ? ((((((((((-p->P_CaL)*2.)*p->Vss)*p->F)/(p->Acap*p->Cm))*sv->O)*sv->y_gate)*FVRT_Ca)/(1.-((exp((-FVRT_Ca))))))*((p->Cao*(exp((-FVRT_Ca))))-(sv->Cass))) : ((((((((((-p->P_CaL)*2.)*p->Vss)*p->F)/(p->Acap*p->Cm))*sv->O)*sv->y_gate)*0.00001)/(1.-((exp(-0.00001)))))*((p->Cao*(exp(-0.00001)))-(sv->Cass))));
  //Output the desired variables
  fprintf(file, "%4.12f\t", IClCa);
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", IKss);
  fprintf(file, "%4.12f\t", IKtof);
  fprintf(file, "%4.12f\t", IKtos);
  fprintf(file, "%4.12f\t", IKur);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", IbCa);
  fprintf(file, "%4.12f\t", IbNa);
  fprintf(file, "%4.12f\t", IpCa);
  fprintf(file, "%4.12f\t", V);
  fprintf(file, "%4.12f\t", i_CaL);
  //Change the units of external variables as appropriate.
  sv->Ki *= 1e-3;
  sv->Nai *= 1e-3;
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        