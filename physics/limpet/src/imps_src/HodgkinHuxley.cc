// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Hodgkin, A. L., Huxley, A. F.
*  Year: 1952
*  Title: A quantitative description of membrane current and its application to conduction and excitation in nerve
*  Journal: The Journal of Physiology, 117
*  DOI: 10.1113/jphysiol.1952.sp004764
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "HodgkinHuxley.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_HodgkinHuxley(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_HodgkinHuxley( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define E_R (GlobalData_t)(-60.0)
#define V_init (GlobalData_t)(-60.0)
#define h_init (GlobalData_t)(0.6)
#define m_init (GlobalData_t)(0.05)
#define n_init (GlobalData_t)(0.325)
#define E_K (GlobalData_t)((E_R-(12.0)))
#define E_L (GlobalData_t)((E_R+10.613))
#define E_Na (GlobalData_t)((E_R+115.0))



void initialize_params_HodgkinHuxley( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  HodgkinHuxley_Params *p = (HodgkinHuxley_Params *)IF->params;

  // Compute the regional constants
  {
    p->GK = 36.0;
    p->GNa = 120.0;
    p->g_L = 0.3;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_HodgkinHuxley( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  HodgkinHuxley_Params *p = (HodgkinHuxley_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.

}



void    initialize_sv_HodgkinHuxley( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  HodgkinHuxley_Params *p = (HodgkinHuxley_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(HodgkinHuxley_state) );
  HodgkinHuxley_state *sv_base = (HodgkinHuxley_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    HodgkinHuxley_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    V = V_init;
    sv->h = h_init;
    sv->m = m_init;
    sv->n = n_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_HodgkinHuxley(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  HodgkinHuxley_Params *p  = (HodgkinHuxley_Params *)IF->params;
  HodgkinHuxley_state *sv_base = (HodgkinHuxley_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    HodgkinHuxley_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t IK = (((((p->GK*sv->n)*sv->n)*sv->n)*sv->n)*(V-(E_K)));
    GlobalData_t IL = (p->g_L*(V-(E_L)));
    GlobalData_t INa = (((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*(V-(E_Na)));
    Iion = ((INa+IK)+IL);
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    GlobalData_t alpha_h = (0.07*(exp((V/20.0))));
    GlobalData_t alpha_m = ((0.1*(V+25.0))/((exp((0.1*(V+25.0))))-(1.0)));
    GlobalData_t alpha_n = ((0.01*(V+10.0))/((exp((0.1*(V+10.0))))-(1.0)));
    GlobalData_t beta_h = (1.0/((exp((0.1*(V+30.0))))+1.0));
    GlobalData_t beta_m = (4.0*(exp((V/18.0))));
    GlobalData_t beta_n = (0.125*(exp((V/80.0))));
    GlobalData_t h_rush_larsen_A = (((-alpha_h)/(alpha_h+beta_h))*(expm1(((-dt)*(alpha_h+beta_h)))));
    GlobalData_t h_rush_larsen_B = (exp(((-dt)*(alpha_h+beta_h))));
    GlobalData_t m_rush_larsen_A = (((-alpha_m)/(alpha_m+beta_m))*(expm1(((-dt)*(alpha_m+beta_m)))));
    GlobalData_t m_rush_larsen_B = (exp(((-dt)*(alpha_m+beta_m))));
    GlobalData_t n_rush_larsen_A = (((-alpha_n)/(alpha_n+beta_n))*(expm1(((-dt)*(alpha_n+beta_n)))));
    GlobalData_t n_rush_larsen_B = (exp(((-dt)*(alpha_n+beta_n))));
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t n_new = n_rush_larsen_A+n_rush_larsen_B*sv->n;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    sv->h = h_new;
    sv->m = m_new;
    sv->n = n_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        