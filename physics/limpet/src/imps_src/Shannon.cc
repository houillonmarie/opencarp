// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Thomas R. Shannon, Fei Wang, Jose Puglisi, Christopher Weber, Donald M. Bers
*  Year: 2004
*  Title: A Mathematical Treatment of Integrated Ca Dynamics within the Ventricular Myocyte
*  Journal: Biophysical Journal, 87(5), 3351-3371
*  DOI: 10.1529/biophysj.104.047449
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Shannon.h"

#ifdef _OPENMP
#include <omp.h>
#endif


#include "Rosenbrock.h"


namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Shannon(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Shannon( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define Bmax_Calmodulin (GlobalData_t)(0.024)
#define Bmax_Calsequestrin (GlobalData_t)(0.14)
#define Bmax_Myosin_Ca (GlobalData_t)(0.14)
#define Bmax_Myosin_Mg (GlobalData_t)(0.14)
#define Bmax_SL (GlobalData_t)(1.65)
#define Bmax_SLB_SL (GlobalData_t)(0.0374)
#define Bmax_SLB_jct (GlobalData_t)(0.0046)
#define Bmax_SLHigh_SL (GlobalData_t)(0.0134)
#define Bmax_SLHigh_jct (GlobalData_t)(0.00165)
#define Bmax_SRB (GlobalData_t)(0.0171)
#define Bmax_TroponinC (GlobalData_t)(0.07)
#define Bmax_TroponinC_Ca_MGCa (GlobalData_t)(0.14)
#define Bmax_TroponinC_Ca_MGMg (GlobalData_t)(0.14)
#define Bmax_jct (GlobalData_t)(7.561)
#define Ca_Calmodulin_init (GlobalData_t)(2.911916e-4)
#define Ca_Calsequestrin_init (GlobalData_t)(1.242988)
#define Ca_Myosin_init (GlobalData_t)(1.298754e-3)
#define Ca_SLB_SL_init (GlobalData_t)(1.110363e-1)
#define Ca_SLB_jct_init (GlobalData_t)(9.566355e-3)
#define Ca_SLHigh_SL_init (GlobalData_t)(7.297378e-2)
#define Ca_SLHigh_jct_init (GlobalData_t)(7.347888e-3)
#define Ca_SL_init (GlobalData_t)(1.031812e-4)
#define Ca_SRB_init (GlobalData_t)(2.143165e-3)
#define Ca_SR_init (GlobalData_t)(5.545201e-1)
#define Ca_TroponinC_Ca_Mg_init (GlobalData_t)(1.078283e-1)
#define Ca_TroponinC_init (GlobalData_t)(8.773191e-3)
#define Ca_jct_init (GlobalData_t)(1.737475e-4)
#define Cai_init (GlobalData_t)(8.597401e-5)
#define Cao (GlobalData_t)(1.8)
#define Cli (GlobalData_t)(15.)
#define Clo (GlobalData_t)(150.)
#define Cm (GlobalData_t)(1.381e-10)
#define EC50_SR (GlobalData_t)(0.45)
#define F (GlobalData_t)(96485.)
#define Fx_CaBk_SL (GlobalData_t)(0.89)
#define Fx_CaBk_jct (GlobalData_t)(0.11)
#define Fx_Cl_SL (GlobalData_t)(0.89)
#define Fx_Cl_jct (GlobalData_t)(0.11)
#define Fx_ICaL_SL (GlobalData_t)(0.1)
#define Fx_ICaL_jct (GlobalData_t)(0.9)
#define Fx_Ks_SL (GlobalData_t)(0.89)
#define Fx_Ks_jct (GlobalData_t)(0.11)
#define Fx_NCX_SL (GlobalData_t)(0.89)
#define Fx_NCX_jct (GlobalData_t)(0.11)
#define Fx_NaBk_SL (GlobalData_t)(0.89)
#define Fx_NaBk_jct (GlobalData_t)(0.11)
#define Fx_NaK_SL (GlobalData_t)(0.89)
#define Fx_NaK_jct (GlobalData_t)(0.11)
#define Fx_Na_SL (GlobalData_t)(0.89)
#define Fx_Na_jct (GlobalData_t)(0.11)
#define Fx_SLCaP_SL (GlobalData_t)(0.89)
#define Fx_SLCaP_jct (GlobalData_t)(0.11)
#define GCaBk (GlobalData_t)(0.0002513)
#define GCl (GlobalData_t)(0.109625)
#define GClBk (GlobalData_t)(0.009)
#define GINa (GlobalData_t)(16.)
#define GKp (GlobalData_t)(0.001)
#define GNaBk (GlobalData_t)(0.297e-3)
#define Gtof (GlobalData_t)(0.02)
#define Gtos (GlobalData_t)(0.06)
#define H (GlobalData_t)(1.6)
#define HNa (GlobalData_t)(3.)
#define HSR (GlobalData_t)(2.5)
#define H_NaK (GlobalData_t)(4.)
#define H_cp0 (GlobalData_t)(1.787)
#define INaK_max (GlobalData_t)(1.90719)
#define I_init (GlobalData_t)(1.024274e-7)
#define KSRleak (GlobalData_t)(5.348e-6)
#define K_mCai (GlobalData_t)(0.00359)
#define K_mCao (GlobalData_t)(1.3)
#define K_mNai (GlobalData_t)(12.29)
#define K_mNao (GlobalData_t)(87.5)
#define Kd_ClCa (GlobalData_t)(0.1)
#define Kd_act (GlobalData_t)(0.000256)
#define Ki (GlobalData_t)(135.)
#define Km (GlobalData_t)(0.0005)
#define Km_Ko (GlobalData_t)(1.5)
#define Km_Nai (GlobalData_t)(11.)
#define Kmf (GlobalData_t)(0.000246)
#define Kmr (GlobalData_t)(1.7)
#define Ko (GlobalData_t)(5.4)
#define MGMyosin_init (GlobalData_t)(1.381982e-1)
#define MGTroponinC_Ca_Mg_init (GlobalData_t)(1.524002e-2)
#define Max_SR (GlobalData_t)(15.)
#define Mgi (GlobalData_t)(1.)
#define Min_SR (GlobalData_t)(1.)
#define Na_SL_buf_init (GlobalData_t)(7.720854e-1)
#define Na_SL_init (GlobalData_t)(8.80733)
#define Na_jct_buf_init (GlobalData_t)(3.539892)
#define Na_jct_init (GlobalData_t)(8.80329)
#define Nai_init (GlobalData_t)(8.80853)
#define Nao (GlobalData_t)(140.)
#define O_init (GlobalData_t)(8.156628e-7)
#define PCa (GlobalData_t)(5.4e-4)
#define PK (GlobalData_t)(2.7e-7)
#define PNa (GlobalData_t)(1.5e-8)
#define Q10_CaL (GlobalData_t)(1.8)
#define Q10_NCX (GlobalData_t)(1.57)
#define Q10_SLCaP (GlobalData_t)(2.35)
#define Q10_SRCaP (GlobalData_t)(2.6)
#define R (GlobalData_t)(8314.3)
#define R_cp0_init (GlobalData_t)(8.884332e-1)
#define R_tos_init (GlobalData_t)(0.9946)
#define T (GlobalData_t)(310.)
#define V_init (GlobalData_t)(-8.556885e1)
#define V_max (GlobalData_t)(9.)
#define V_maxAF (GlobalData_t)(0.0673)
#define V_max_cp0 (GlobalData_t)(5.3114e-3)
#define X_tof_init (GlobalData_t)(4.051574e-3)
#define X_tos_init (GlobalData_t)(4.051574e-3)
#define Xr_init (GlobalData_t)(8.641386e-3)
#define Xs_init (GlobalData_t)(5.412034e-3)
#define Y_tof_init (GlobalData_t)(9.945511e-1)
#define Y_tos_init (GlobalData_t)(9.945511e-1)
#define cell_length (GlobalData_t)(100.)
#define cell_radius (GlobalData_t)(10.25)
#define d_init (GlobalData_t)(7.175662e-6)
#define eta (GlobalData_t)(0.35)
#define fCaB_SL_init (GlobalData_t)(1.452605e-2)
#define fCaB_jct_init (GlobalData_t)(2.421991e-2)
#define f_init (GlobalData_t)(1.000681)
#define gamma_Cai (GlobalData_t)(0.341)
#define gamma_Cao (GlobalData_t)(0.341)
#define gamma_Ki (GlobalData_t)(0.75)
#define gamma_Ko (GlobalData_t)(0.75)
#define gamma_Nai (GlobalData_t)(0.75)
#define gamma_Nao (GlobalData_t)(0.75)
#define h_init (GlobalData_t)(9.867005e-1)
#define j_init (GlobalData_t)(9.91562e-1)
#define kiCa (GlobalData_t)(0.5)
#define kim (GlobalData_t)(0.005)
#define koCa (GlobalData_t)(10.)
#define koff (GlobalData_t)(1e-3)
#define koff_Calmodulin (GlobalData_t)(238e-3)
#define koff_Calsequestrin (GlobalData_t)(65.)
#define koff_Myosin_Ca (GlobalData_t)(0.46e-3)
#define koff_Myosin_Mg (GlobalData_t)(0.057e-3)
#define koff_SLB (GlobalData_t)(1.3)
#define koff_SLHigh (GlobalData_t)(30e-3)
#define koff_SRB (GlobalData_t)(60e-3)
#define koff_TroponinC (GlobalData_t)(19.6e-3)
#define koff_TroponinC_Ca_MGCa (GlobalData_t)(0.032e-3)
#define koff_TroponinC_Ca_MGMg (GlobalData_t)(3.33e-3)
#define kom (GlobalData_t)(0.06)
#define kon (GlobalData_t)(0.0001)
#define kon_Calmodulin (GlobalData_t)(34.)
#define kon_Calsequestrin (GlobalData_t)(100.)
#define kon_Myosin_Ca (GlobalData_t)(13.8)
#define kon_Myosin_Mg (GlobalData_t)(15.7e-3)
#define kon_SL (GlobalData_t)(100.)
#define kon_SRB (GlobalData_t)(100.)
#define kon_TroponinC (GlobalData_t)(32.7)
#define kon_TroponinC_Ca_MGCa (GlobalData_t)(2.37)
#define kon_TroponinC_Ca_MGMg (GlobalData_t)(3e-3)
#define ks (GlobalData_t)(25.)
#define ksat (GlobalData_t)(0.27)
#define m_init (GlobalData_t)(1.405627e-3)
#define pKNa (GlobalData_t)(0.01833)
#define partial_Bmax_Calmodulin_del_Ca_SL (GlobalData_t)(0.)
#define partial_Bmax_Calmodulin_del_Ca_SR (GlobalData_t)(0.)
#define partial_Bmax_Calmodulin_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Bmax_Calmodulin_del_Ca_jct (GlobalData_t)(0.)
#define partial_Bmax_Calmodulin_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_Calsequestrin_del_Ca_SL (GlobalData_t)(0.)
#define partial_Bmax_Calsequestrin_del_Ca_SR (GlobalData_t)(0.)
#define partial_Bmax_Calsequestrin_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Bmax_Calsequestrin_del_Ca_jct (GlobalData_t)(0.)
#define partial_Bmax_Calsequestrin_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_Myosin_Ca_del_Ca_SL (GlobalData_t)(0.)
#define partial_Bmax_Myosin_Ca_del_Ca_SR (GlobalData_t)(0.)
#define partial_Bmax_Myosin_Ca_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Bmax_Myosin_Ca_del_Ca_jct (GlobalData_t)(0.)
#define partial_Bmax_Myosin_Ca_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_Myosin_Mg_del_Ca_SL (GlobalData_t)(0.)
#define partial_Bmax_Myosin_Mg_del_Ca_SR (GlobalData_t)(0.)
#define partial_Bmax_Myosin_Mg_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Bmax_Myosin_Mg_del_Ca_jct (GlobalData_t)(0.)
#define partial_Bmax_Myosin_Mg_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_SLB_SL_del_Ca_SL (GlobalData_t)(0.)
#define partial_Bmax_SLB_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_Bmax_SLB_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Bmax_SLB_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_Bmax_SLB_SL_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_SLB_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_Bmax_SLB_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_Bmax_SLB_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Bmax_SLB_jct_del_Ca_jct (GlobalData_t)(0.)
#define partial_Bmax_SLB_jct_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_SLHigh_SL_del_Ca_SL (GlobalData_t)(0.)
#define partial_Bmax_SLHigh_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_Bmax_SLHigh_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Bmax_SLHigh_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_Bmax_SLHigh_SL_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_SLHigh_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_Bmax_SLHigh_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_Bmax_SLHigh_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Bmax_SLHigh_jct_del_Ca_jct (GlobalData_t)(0.)
#define partial_Bmax_SLHigh_jct_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_SRB_del_Ca_SL (GlobalData_t)(0.)
#define partial_Bmax_SRB_del_Ca_SR (GlobalData_t)(0.)
#define partial_Bmax_SRB_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Bmax_SRB_del_Ca_jct (GlobalData_t)(0.)
#define partial_Bmax_SRB_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_Ca_MGCa_del_Ca_SL (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_Ca_MGCa_del_Ca_SR (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_Ca_MGCa_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_Ca_MGCa_del_Ca_jct (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_Ca_MGCa_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_Ca_MGMg_del_Ca_SL (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_Ca_MGMg_del_Ca_SR (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_Ca_MGMg_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_Ca_MGMg_del_Ca_jct (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_Ca_MGMg_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_del_Ca_SL (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_del_Ca_SR (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_del_Ca_jct (GlobalData_t)(0.)
#define partial_Bmax_TroponinC_del_Cai (GlobalData_t)(0.)
#define partial_Cao_del_Ca_SL (GlobalData_t)(0.)
#define partial_Cao_del_Ca_SR (GlobalData_t)(0.)
#define partial_Cao_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Cao_del_Ca_jct (GlobalData_t)(0.)
#define partial_Cao_del_Cai (GlobalData_t)(0.)
#define partial_Cm_del_Ca_SL (GlobalData_t)(0.)
#define partial_Cm_del_Ca_SR (GlobalData_t)(0.)
#define partial_Cm_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Cm_del_Ca_jct (GlobalData_t)(0.)
#define partial_Cm_del_Cai (GlobalData_t)(0.)
#define partial_E_Ca_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_E_Ca_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_E_Ca_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_E_Ca_SL_del_Cai (GlobalData_t)(0.)
#define partial_E_Ca_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_E_Ca_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_E_Ca_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_E_Ca_jct_del_Cai (GlobalData_t)(0.)
#define partial_F_del_Ca_SL (GlobalData_t)(0.)
#define partial_F_del_Ca_SR (GlobalData_t)(0.)
#define partial_F_del_Ca_SRB (GlobalData_t)(0.)
#define partial_F_del_Ca_jct (GlobalData_t)(0.)
#define partial_F_del_Cai (GlobalData_t)(0.)
#define partial_Fx_CaBk_SL_del_Ca_SL (GlobalData_t)(0.)
#define partial_Fx_CaBk_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_Fx_CaBk_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Fx_CaBk_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_Fx_CaBk_SL_del_Cai (GlobalData_t)(0.)
#define partial_Fx_CaBk_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_Fx_CaBk_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_Fx_CaBk_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Fx_CaBk_jct_del_Ca_jct (GlobalData_t)(0.)
#define partial_Fx_CaBk_jct_del_Cai (GlobalData_t)(0.)
#define partial_Fx_ICaL_SL_del_Ca_SL (GlobalData_t)(0.)
#define partial_Fx_ICaL_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_Fx_ICaL_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Fx_ICaL_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_Fx_ICaL_SL_del_Cai (GlobalData_t)(0.)
#define partial_Fx_ICaL_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_Fx_ICaL_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_Fx_ICaL_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Fx_ICaL_jct_del_Ca_jct (GlobalData_t)(0.)
#define partial_Fx_ICaL_jct_del_Cai (GlobalData_t)(0.)
#define partial_Fx_NCX_SL_del_Ca_SL (GlobalData_t)(0.)
#define partial_Fx_NCX_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_Fx_NCX_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Fx_NCX_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_Fx_NCX_SL_del_Cai (GlobalData_t)(0.)
#define partial_Fx_NCX_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_Fx_NCX_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_Fx_NCX_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Fx_NCX_jct_del_Ca_jct (GlobalData_t)(0.)
#define partial_Fx_NCX_jct_del_Cai (GlobalData_t)(0.)
#define partial_Fx_SLCaP_SL_del_Ca_SL (GlobalData_t)(0.)
#define partial_Fx_SLCaP_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_Fx_SLCaP_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Fx_SLCaP_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_Fx_SLCaP_SL_del_Cai (GlobalData_t)(0.)
#define partial_Fx_SLCaP_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_Fx_SLCaP_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_Fx_SLCaP_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Fx_SLCaP_jct_del_Ca_jct (GlobalData_t)(0.)
#define partial_Fx_SLCaP_jct_del_Cai (GlobalData_t)(0.)
#define partial_GCaBk_del_Ca_SL (GlobalData_t)(0.)
#define partial_GCaBk_del_Ca_SR (GlobalData_t)(0.)
#define partial_GCaBk_del_Ca_SRB (GlobalData_t)(0.)
#define partial_GCaBk_del_Ca_jct (GlobalData_t)(0.)
#define partial_GCaBk_del_Cai (GlobalData_t)(0.)
#define partial_HNa_del_Ca_SL (GlobalData_t)(0.)
#define partial_HNa_del_Ca_SR (GlobalData_t)(0.)
#define partial_HNa_del_Ca_SRB (GlobalData_t)(0.)
#define partial_HNa_del_Ca_jct (GlobalData_t)(0.)
#define partial_HNa_del_Cai (GlobalData_t)(0.)
#define partial_H_cp0_del_Ca_SL (GlobalData_t)(0.)
#define partial_H_cp0_del_Ca_SR (GlobalData_t)(0.)
#define partial_H_cp0_del_Ca_SRB (GlobalData_t)(0.)
#define partial_H_cp0_del_Ca_jct (GlobalData_t)(0.)
#define partial_H_cp0_del_Cai (GlobalData_t)(0.)
#define partial_H_del_Ca_SL (GlobalData_t)(0.)
#define partial_H_del_Ca_SR (GlobalData_t)(0.)
#define partial_H_del_Ca_SRB (GlobalData_t)(0.)
#define partial_H_del_Ca_jct (GlobalData_t)(0.)
#define partial_H_del_Cai (GlobalData_t)(0.)
#define partial_ICaL_Ca_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_ICaL_Ca_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_ICaL_Ca_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_ICaL_Ca_SL_del_Cai (GlobalData_t)(0.)
#define partial_ICaL_Ca_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_ICaL_Ca_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_ICaL_Ca_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_ICaL_Ca_jct_del_Cai (GlobalData_t)(0.)
#define partial_INaCa_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_INaCa_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_INaCa_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_INaCa_SL_del_Cai (GlobalData_t)(0.)
#define partial_INaCa_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_INaCa_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_INaCa_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_INaCa_jct_del_Cai (GlobalData_t)(0.)
#define partial_IbCa_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_IbCa_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_IbCa_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_IbCa_SL_del_Cai (GlobalData_t)(0.)
#define partial_IbCa_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_IbCa_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_IbCa_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_IbCa_jct_del_Cai (GlobalData_t)(0.)
#define partial_IpCa_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_IpCa_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_IpCa_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_IpCa_SL_del_Cai (GlobalData_t)(0.)
#define partial_IpCa_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_IpCa_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_IpCa_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_IpCa_jct_del_Cai (GlobalData_t)(0.)
#define partial_J_Ca_SL_myo_del_Ca_SL (GlobalData_t)(3.7243e-12)
#define partial_J_Ca_SL_myo_del_Ca_SR (GlobalData_t)(0.)
#define partial_J_Ca_SL_myo_del_Ca_SRB (GlobalData_t)(0.)
#define partial_J_Ca_SL_myo_del_Ca_jct (GlobalData_t)(0.)
#define partial_J_Ca_SL_myo_del_Cai (GlobalData_t)((-1.*3.7243e-12))
#define partial_J_Ca_jct_SL_del_Ca_SL (GlobalData_t)((-1.*8.2413e-13))
#define partial_J_Ca_jct_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_J_Ca_jct_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_J_Ca_jct_SL_del_Ca_jct (GlobalData_t)(8.2413e-13)
#define partial_J_Ca_jct_SL_del_Cai (GlobalData_t)(0.)
#define partial_KSRleak_del_Ca_SL (GlobalData_t)(0.)
#define partial_KSRleak_del_Ca_SR (GlobalData_t)(0.)
#define partial_KSRleak_del_Ca_SRB (GlobalData_t)(0.)
#define partial_KSRleak_del_Ca_jct (GlobalData_t)(0.)
#define partial_KSRleak_del_Cai (GlobalData_t)(0.)
#define partial_K_mCai_del_Ca_SL (GlobalData_t)(0.)
#define partial_K_mCai_del_Ca_SR (GlobalData_t)(0.)
#define partial_K_mCai_del_Ca_SRB (GlobalData_t)(0.)
#define partial_K_mCai_del_Ca_jct (GlobalData_t)(0.)
#define partial_K_mCai_del_Cai (GlobalData_t)(0.)
#define partial_K_mCao_del_Ca_SL (GlobalData_t)(0.)
#define partial_K_mCao_del_Ca_SR (GlobalData_t)(0.)
#define partial_K_mCao_del_Ca_SRB (GlobalData_t)(0.)
#define partial_K_mCao_del_Ca_jct (GlobalData_t)(0.)
#define partial_K_mCao_del_Cai (GlobalData_t)(0.)
#define partial_K_mNai_del_Ca_SL (GlobalData_t)(0.)
#define partial_K_mNai_del_Ca_SR (GlobalData_t)(0.)
#define partial_K_mNai_del_Ca_SRB (GlobalData_t)(0.)
#define partial_K_mNai_del_Ca_jct (GlobalData_t)(0.)
#define partial_K_mNai_del_Cai (GlobalData_t)(0.)
#define partial_K_mNao_del_Ca_SL (GlobalData_t)(0.)
#define partial_K_mNao_del_Ca_SR (GlobalData_t)(0.)
#define partial_K_mNao_del_Ca_SRB (GlobalData_t)(0.)
#define partial_K_mNao_del_Ca_jct (GlobalData_t)(0.)
#define partial_K_mNao_del_Cai (GlobalData_t)(0.)
#define partial_Ka_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_Ka_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Ka_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_Ka_SL_del_Cai (GlobalData_t)(0.)
#define partial_Ka_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_Ka_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_Ka_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Ka_jct_del_Cai (GlobalData_t)(0.)
#define partial_Kd_act_del_Ca_SL (GlobalData_t)(0.)
#define partial_Kd_act_del_Ca_SR (GlobalData_t)(0.)
#define partial_Kd_act_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Kd_act_del_Ca_jct (GlobalData_t)(0.)
#define partial_Kd_act_del_Cai (GlobalData_t)(0.)
#define partial_Km_del_Ca_SL (GlobalData_t)(0.)
#define partial_Km_del_Ca_SR (GlobalData_t)(0.)
#define partial_Km_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Km_del_Ca_jct (GlobalData_t)(0.)
#define partial_Km_del_Cai (GlobalData_t)(0.)
#define partial_Kmf_del_Ca_SL (GlobalData_t)(0.)
#define partial_Kmf_del_Ca_SR (GlobalData_t)(0.)
#define partial_Kmf_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Kmf_del_Ca_jct (GlobalData_t)(0.)
#define partial_Kmf_del_Cai (GlobalData_t)(0.)
#define partial_Kmr_del_Ca_SL (GlobalData_t)(0.)
#define partial_Kmr_del_Ca_SR (GlobalData_t)(0.)
#define partial_Kmr_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Kmr_del_Ca_jct (GlobalData_t)(0.)
#define partial_Kmr_del_Cai (GlobalData_t)(0.)
#define partial_Mgi_del_Ca_SL (GlobalData_t)(0.)
#define partial_Mgi_del_Ca_SR (GlobalData_t)(0.)
#define partial_Mgi_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Mgi_del_Ca_jct (GlobalData_t)(0.)
#define partial_Mgi_del_Cai (GlobalData_t)(0.)
#define partial_Nao_del_Ca_SL (GlobalData_t)(0.)
#define partial_Nao_del_Ca_SR (GlobalData_t)(0.)
#define partial_Nao_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Nao_del_Ca_jct (GlobalData_t)(0.)
#define partial_Nao_del_Cai (GlobalData_t)(0.)
#define partial_PCa_del_Ca_SL (GlobalData_t)(0.)
#define partial_PCa_del_Ca_SR (GlobalData_t)(0.)
#define partial_PCa_del_Ca_SRB (GlobalData_t)(0.)
#define partial_PCa_del_Ca_jct (GlobalData_t)(0.)
#define partial_PCa_del_Cai (GlobalData_t)(0.)
#define partial_Q10_CaL_del_Ca_SL (GlobalData_t)(0.)
#define partial_Q10_CaL_del_Ca_SR (GlobalData_t)(0.)
#define partial_Q10_CaL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Q10_CaL_del_Ca_jct (GlobalData_t)(0.)
#define partial_Q10_CaL_del_Cai (GlobalData_t)(0.)
#define partial_Q10_NCX_del_Ca_SL (GlobalData_t)(0.)
#define partial_Q10_NCX_del_Ca_SR (GlobalData_t)(0.)
#define partial_Q10_NCX_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Q10_NCX_del_Ca_jct (GlobalData_t)(0.)
#define partial_Q10_NCX_del_Cai (GlobalData_t)(0.)
#define partial_Q10_SLCaP_del_Ca_SL (GlobalData_t)(0.)
#define partial_Q10_SLCaP_del_Ca_SR (GlobalData_t)(0.)
#define partial_Q10_SLCaP_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Q10_SLCaP_del_Ca_jct (GlobalData_t)(0.)
#define partial_Q10_SLCaP_del_Cai (GlobalData_t)(0.)
#define partial_Q10_SRCaP_del_Ca_SL (GlobalData_t)(0.)
#define partial_Q10_SRCaP_del_Ca_SR (GlobalData_t)(0.)
#define partial_Q10_SRCaP_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Q10_SRCaP_del_Ca_jct (GlobalData_t)(0.)
#define partial_Q10_SRCaP_del_Cai (GlobalData_t)(0.)
#define partial_Q_CaL_del_Ca_SL (GlobalData_t)(0.)
#define partial_Q_CaL_del_Ca_SR (GlobalData_t)(0.)
#define partial_Q_CaL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Q_CaL_del_Ca_jct (GlobalData_t)(0.)
#define partial_Q_CaL_del_Cai (GlobalData_t)(0.)
#define partial_Q_NCX_del_Ca_SL (GlobalData_t)(0.)
#define partial_Q_NCX_del_Ca_SR (GlobalData_t)(0.)
#define partial_Q_NCX_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Q_NCX_del_Ca_jct (GlobalData_t)(0.)
#define partial_Q_NCX_del_Cai (GlobalData_t)(0.)
#define partial_Q_SLCaP_del_Ca_SL (GlobalData_t)(0.)
#define partial_Q_SLCaP_del_Ca_SR (GlobalData_t)(0.)
#define partial_Q_SLCaP_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Q_SLCaP_del_Ca_jct (GlobalData_t)(0.)
#define partial_Q_SLCaP_del_Cai (GlobalData_t)(0.)
#define partial_Q_SRCaP_del_Ca_SL (GlobalData_t)(0.)
#define partial_Q_SRCaP_del_Ca_SR (GlobalData_t)(0.)
#define partial_Q_SRCaP_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Q_SRCaP_del_Ca_jct (GlobalData_t)(0.)
#define partial_Q_SRCaP_del_Cai (GlobalData_t)(0.)
#define partial_R_del_Ca_SL (GlobalData_t)(0.)
#define partial_R_del_Ca_SR (GlobalData_t)(0.)
#define partial_R_del_Ca_SRB (GlobalData_t)(0.)
#define partial_R_del_Ca_jct (GlobalData_t)(0.)
#define partial_R_del_Cai (GlobalData_t)(0.)
#define partial_T_del_Ca_SL (GlobalData_t)(0.)
#define partial_T_del_Ca_SR (GlobalData_t)(0.)
#define partial_T_del_Ca_SRB (GlobalData_t)(0.)
#define partial_T_del_Ca_jct (GlobalData_t)(0.)
#define partial_T_del_Cai (GlobalData_t)(0.)
#define partial_V_maxAF_del_Ca_SL (GlobalData_t)(0.)
#define partial_V_maxAF_del_Ca_SR (GlobalData_t)(0.)
#define partial_V_maxAF_del_Ca_SRB (GlobalData_t)(0.)
#define partial_V_maxAF_del_Ca_jct (GlobalData_t)(0.)
#define partial_V_maxAF_del_Cai (GlobalData_t)(0.)
#define partial_V_max_cp0_del_Ca_SL (GlobalData_t)(0.)
#define partial_V_max_cp0_del_Ca_SR (GlobalData_t)(0.)
#define partial_V_max_cp0_del_Ca_SRB (GlobalData_t)(0.)
#define partial_V_max_cp0_del_Ca_jct (GlobalData_t)(0.)
#define partial_V_max_cp0_del_Cai (GlobalData_t)(0.)
#define partial_V_max_del_Ca_SL (GlobalData_t)(0.)
#define partial_V_max_del_Ca_SR (GlobalData_t)(0.)
#define partial_V_max_del_Ca_SRB (GlobalData_t)(0.)
#define partial_V_max_del_Ca_jct (GlobalData_t)(0.)
#define partial_V_max_del_Cai (GlobalData_t)(0.)
#define partial_VolCell_del_Ca_SL (GlobalData_t)(0.)
#define partial_VolCell_del_Ca_SR (GlobalData_t)(0.)
#define partial_VolCell_del_Ca_SRB (GlobalData_t)(0.)
#define partial_VolCell_del_Ca_jct (GlobalData_t)(0.)
#define partial_VolCell_del_Cai (GlobalData_t)(0.)
#define partial_VolSL_del_Ca_SL (GlobalData_t)(0.)
#define partial_VolSL_del_Ca_SR (GlobalData_t)(0.)
#define partial_VolSL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_VolSL_del_Ca_jct (GlobalData_t)(0.)
#define partial_VolSL_del_Cai (GlobalData_t)(0.)
#define partial_VolSR_del_Ca_SL (GlobalData_t)(0.)
#define partial_VolSR_del_Ca_SR (GlobalData_t)(0.)
#define partial_VolSR_del_Ca_SRB (GlobalData_t)(0.)
#define partial_VolSR_del_Ca_jct (GlobalData_t)(0.)
#define partial_VolSR_del_Cai (GlobalData_t)(0.)
#define partial_Voljct_del_Ca_SL (GlobalData_t)(0.)
#define partial_Voljct_del_Ca_SR (GlobalData_t)(0.)
#define partial_Voljct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Voljct_del_Ca_jct (GlobalData_t)(0.)
#define partial_Voljct_del_Cai (GlobalData_t)(0.)
#define partial_Volmyo_del_Ca_SL (GlobalData_t)(0.)
#define partial_Volmyo_del_Ca_SR (GlobalData_t)(0.)
#define partial_Volmyo_del_Ca_SRB (GlobalData_t)(0.)
#define partial_Volmyo_del_Ca_jct (GlobalData_t)(0.)
#define partial_Volmyo_del_Cai (GlobalData_t)(0.)
#define partial_cell_length_del_Ca_SL (GlobalData_t)(0.)
#define partial_cell_length_del_Ca_SR (GlobalData_t)(0.)
#define partial_cell_length_del_Ca_SRB (GlobalData_t)(0.)
#define partial_cell_length_del_Ca_jct (GlobalData_t)(0.)
#define partial_cell_length_del_Cai (GlobalData_t)(0.)
#define partial_cell_radius_del_Ca_SL (GlobalData_t)(0.)
#define partial_cell_radius_del_Ca_SR (GlobalData_t)(0.)
#define partial_cell_radius_del_Ca_SRB (GlobalData_t)(0.)
#define partial_cell_radius_del_Ca_jct (GlobalData_t)(0.)
#define partial_cell_radius_del_Cai (GlobalData_t)(0.)
#define partial_dCa_Calmodulin_del_Ca_SL (GlobalData_t)(0.)
#define partial_dCa_Calmodulin_del_Ca_SR (GlobalData_t)(0.)
#define partial_dCa_Calmodulin_del_Ca_SRB (GlobalData_t)(0.)
#define partial_dCa_Calmodulin_del_Ca_jct (GlobalData_t)(0.)
#define partial_dCa_Myosin_del_Ca_SL (GlobalData_t)(0.)
#define partial_dCa_Myosin_del_Ca_SR (GlobalData_t)(0.)
#define partial_dCa_Myosin_del_Ca_SRB (GlobalData_t)(0.)
#define partial_dCa_Myosin_del_Ca_jct (GlobalData_t)(0.)
#define partial_dCa_SLB_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_dCa_SLB_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_dCa_SLB_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_dCa_SLB_SL_del_Cai (GlobalData_t)(0.)
#define partial_dCa_SLB_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_dCa_SLB_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_dCa_SLB_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_dCa_SLB_jct_del_Cai (GlobalData_t)(0.)
#define partial_dCa_SLHigh_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_dCa_SLHigh_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_dCa_SLHigh_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_dCa_SLHigh_SL_del_Cai (GlobalData_t)(0.)
#define partial_dCa_SLHigh_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_dCa_SLHigh_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_dCa_SLHigh_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_dCa_SLHigh_jct_del_Cai (GlobalData_t)(0.)
#define partial_dCa_SL_tot_bound_del_Ca_SR (GlobalData_t)(0.)
#define partial_dCa_SL_tot_bound_del_Ca_SRB (GlobalData_t)(0.)
#define partial_dCa_SL_tot_bound_del_Ca_jct (GlobalData_t)(0.)
#define partial_dCa_SL_tot_bound_del_Cai (GlobalData_t)(0.)
#define partial_dCa_SRB_del_Ca_SL (GlobalData_t)(0.)
#define partial_dCa_SRB_del_Ca_SR (GlobalData_t)(0.)
#define partial_dCa_SRB_del_Ca_jct (GlobalData_t)(0.)
#define partial_dCa_TroponinC_Ca_Mg_del_Ca_SL (GlobalData_t)(0.)
#define partial_dCa_TroponinC_Ca_Mg_del_Ca_SR (GlobalData_t)(0.)
#define partial_dCa_TroponinC_Ca_Mg_del_Ca_SRB (GlobalData_t)(0.)
#define partial_dCa_TroponinC_Ca_Mg_del_Ca_jct (GlobalData_t)(0.)
#define partial_dCa_TroponinC_del_Ca_SL (GlobalData_t)(0.)
#define partial_dCa_TroponinC_del_Ca_SR (GlobalData_t)(0.)
#define partial_dCa_TroponinC_del_Ca_SRB (GlobalData_t)(0.)
#define partial_dCa_TroponinC_del_Ca_jct (GlobalData_t)(0.)
#define partial_dCa_cytosol_tot_bound_del_Ca_SL (GlobalData_t)(0.)
#define partial_dCa_cytosol_tot_bound_del_Ca_SR (GlobalData_t)(0.)
#define partial_dCa_cytosol_tot_bound_del_Ca_jct (GlobalData_t)(0.)
#define partial_dCa_jct_tot_bound_del_Ca_SL (GlobalData_t)(0.)
#define partial_dCa_jct_tot_bound_del_Ca_SR (GlobalData_t)(0.)
#define partial_dCa_jct_tot_bound_del_Ca_SRB (GlobalData_t)(0.)
#define partial_dCa_jct_tot_bound_del_Cai (GlobalData_t)(0.)
#define partial_dCalsequestrin_del_Ca_SL (GlobalData_t)(0.)
#define partial_dCalsequestrin_del_Ca_SRB (GlobalData_t)(0.)
#define partial_dCalsequestrin_del_Ca_jct (GlobalData_t)(0.)
#define partial_dCalsequestrin_del_Cai (GlobalData_t)(0.)
#define partial_dMGMyosin_del_Ca_SL (GlobalData_t)(0.)
#define partial_dMGMyosin_del_Ca_SR (GlobalData_t)(0.)
#define partial_dMGMyosin_del_Ca_SRB (GlobalData_t)(0.)
#define partial_dMGMyosin_del_Ca_jct (GlobalData_t)(0.)
#define partial_dMGMyosin_del_Cai (GlobalData_t)(0.)
#define partial_dMGTroponinC_Ca_Mg_del_Ca_SL (GlobalData_t)(0.)
#define partial_dMGTroponinC_Ca_Mg_del_Ca_SR (GlobalData_t)(0.)
#define partial_dMGTroponinC_Ca_Mg_del_Ca_SRB (GlobalData_t)(0.)
#define partial_dMGTroponinC_Ca_Mg_del_Ca_jct (GlobalData_t)(0.)
#define partial_dMGTroponinC_Ca_Mg_del_Cai (GlobalData_t)(0.)
#define partial_diff_Ca_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_diff_Ca_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_diff_Ca_SRB_del_Ca_SL (GlobalData_t)(0.)
#define partial_diff_Ca_SRB_del_Ca_SR (GlobalData_t)(0.)
#define partial_diff_Ca_SRB_del_Ca_jct (GlobalData_t)(0.)
#define partial_diff_Ca_SR_del_Ca_SL (GlobalData_t)(0.)
#define partial_diff_Ca_SR_del_Ca_SRB (GlobalData_t)(0.)
#define partial_diff_Ca_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_diff_Ca_jct_del_Cai (GlobalData_t)(0.)
#define partial_diff_Cai_del_Ca_jct (GlobalData_t)(0.)
#define partial_eta_del_Ca_SL (GlobalData_t)(0.)
#define partial_eta_del_Ca_SR (GlobalData_t)(0.)
#define partial_eta_del_Ca_SRB (GlobalData_t)(0.)
#define partial_eta_del_Ca_jct (GlobalData_t)(0.)
#define partial_eta_del_Cai (GlobalData_t)(0.)
#define partial_fCa_SL_del_Ca_SL (GlobalData_t)(0.)
#define partial_fCa_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_fCa_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_fCa_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_fCa_SL_del_Cai (GlobalData_t)(0.)
#define partial_fCa_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_fCa_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_fCa_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_fCa_jct_del_Ca_jct (GlobalData_t)(0.)
#define partial_fCa_jct_del_Cai (GlobalData_t)(0.)
#define partial_gamma_Cai_del_Ca_SL (GlobalData_t)(0.)
#define partial_gamma_Cai_del_Ca_SR (GlobalData_t)(0.)
#define partial_gamma_Cai_del_Ca_SRB (GlobalData_t)(0.)
#define partial_gamma_Cai_del_Ca_jct (GlobalData_t)(0.)
#define partial_gamma_Cai_del_Cai (GlobalData_t)(0.)
#define partial_gamma_Cao_del_Ca_SL (GlobalData_t)(0.)
#define partial_gamma_Cao_del_Ca_SR (GlobalData_t)(0.)
#define partial_gamma_Cao_del_Ca_SRB (GlobalData_t)(0.)
#define partial_gamma_Cao_del_Ca_jct (GlobalData_t)(0.)
#define partial_gamma_Cao_del_Cai (GlobalData_t)(0.)
#define partial_i_Ca_SL_tot_del_Ca_SR (GlobalData_t)(0.)
#define partial_i_Ca_SL_tot_del_Ca_SRB (GlobalData_t)(0.)
#define partial_i_Ca_SL_tot_del_Ca_jct (GlobalData_t)(0.)
#define partial_i_Ca_SL_tot_del_Cai (GlobalData_t)(0.)
#define partial_i_Ca_jct_tot_del_Ca_SL (GlobalData_t)(0.)
#define partial_i_Ca_jct_tot_del_Ca_SR (GlobalData_t)(0.)
#define partial_i_Ca_jct_tot_del_Ca_SRB (GlobalData_t)(0.)
#define partial_i_Ca_jct_tot_del_Cai (GlobalData_t)(0.)
#define partial_j_leak_SR_del_Ca_SL (GlobalData_t)(0.)
#define partial_j_leak_SR_del_Ca_SRB (GlobalData_t)(0.)
#define partial_j_leak_SR_del_Cai (GlobalData_t)(0.)
#define partial_j_pump_SR_del_Ca_SL (GlobalData_t)(0.)
#define partial_j_pump_SR_del_Ca_SRB (GlobalData_t)(0.)
#define partial_j_pump_SR_del_Ca_jct (GlobalData_t)(0.)
#define partial_j_rel_SR_del_Ca_SL (GlobalData_t)(0.)
#define partial_j_rel_SR_del_Ca_SRB (GlobalData_t)(0.)
#define partial_j_rel_SR_del_Cai (GlobalData_t)(0.)
#define partial_koff_Calmodulin_del_Ca_SL (GlobalData_t)(0.)
#define partial_koff_Calmodulin_del_Ca_SR (GlobalData_t)(0.)
#define partial_koff_Calmodulin_del_Ca_SRB (GlobalData_t)(0.)
#define partial_koff_Calmodulin_del_Ca_jct (GlobalData_t)(0.)
#define partial_koff_Calmodulin_del_Cai (GlobalData_t)(0.)
#define partial_koff_Calsequestrin_del_Ca_SL (GlobalData_t)(0.)
#define partial_koff_Calsequestrin_del_Ca_SR (GlobalData_t)(0.)
#define partial_koff_Calsequestrin_del_Ca_SRB (GlobalData_t)(0.)
#define partial_koff_Calsequestrin_del_Ca_jct (GlobalData_t)(0.)
#define partial_koff_Calsequestrin_del_Cai (GlobalData_t)(0.)
#define partial_koff_Myosin_Ca_del_Ca_SL (GlobalData_t)(0.)
#define partial_koff_Myosin_Ca_del_Ca_SR (GlobalData_t)(0.)
#define partial_koff_Myosin_Ca_del_Ca_SRB (GlobalData_t)(0.)
#define partial_koff_Myosin_Ca_del_Ca_jct (GlobalData_t)(0.)
#define partial_koff_Myosin_Ca_del_Cai (GlobalData_t)(0.)
#define partial_koff_Myosin_Mg_del_Ca_SL (GlobalData_t)(0.)
#define partial_koff_Myosin_Mg_del_Ca_SR (GlobalData_t)(0.)
#define partial_koff_Myosin_Mg_del_Ca_SRB (GlobalData_t)(0.)
#define partial_koff_Myosin_Mg_del_Ca_jct (GlobalData_t)(0.)
#define partial_koff_Myosin_Mg_del_Cai (GlobalData_t)(0.)
#define partial_koff_SLB_del_Ca_SL (GlobalData_t)(0.)
#define partial_koff_SLB_del_Ca_SR (GlobalData_t)(0.)
#define partial_koff_SLB_del_Ca_SRB (GlobalData_t)(0.)
#define partial_koff_SLB_del_Ca_jct (GlobalData_t)(0.)
#define partial_koff_SLB_del_Cai (GlobalData_t)(0.)
#define partial_koff_SLHigh_del_Ca_SL (GlobalData_t)(0.)
#define partial_koff_SLHigh_del_Ca_SR (GlobalData_t)(0.)
#define partial_koff_SLHigh_del_Ca_SRB (GlobalData_t)(0.)
#define partial_koff_SLHigh_del_Ca_jct (GlobalData_t)(0.)
#define partial_koff_SLHigh_del_Cai (GlobalData_t)(0.)
#define partial_koff_SRB_del_Ca_SL (GlobalData_t)(0.)
#define partial_koff_SRB_del_Ca_SR (GlobalData_t)(0.)
#define partial_koff_SRB_del_Ca_SRB (GlobalData_t)(0.)
#define partial_koff_SRB_del_Ca_jct (GlobalData_t)(0.)
#define partial_koff_SRB_del_Cai (GlobalData_t)(0.)
#define partial_koff_TroponinC_Ca_MGCa_del_Ca_SL (GlobalData_t)(0.)
#define partial_koff_TroponinC_Ca_MGCa_del_Ca_SR (GlobalData_t)(0.)
#define partial_koff_TroponinC_Ca_MGCa_del_Ca_SRB (GlobalData_t)(0.)
#define partial_koff_TroponinC_Ca_MGCa_del_Ca_jct (GlobalData_t)(0.)
#define partial_koff_TroponinC_Ca_MGCa_del_Cai (GlobalData_t)(0.)
#define partial_koff_TroponinC_Ca_MGMg_del_Ca_SL (GlobalData_t)(0.)
#define partial_koff_TroponinC_Ca_MGMg_del_Ca_SR (GlobalData_t)(0.)
#define partial_koff_TroponinC_Ca_MGMg_del_Ca_SRB (GlobalData_t)(0.)
#define partial_koff_TroponinC_Ca_MGMg_del_Ca_jct (GlobalData_t)(0.)
#define partial_koff_TroponinC_Ca_MGMg_del_Cai (GlobalData_t)(0.)
#define partial_koff_TroponinC_del_Ca_SL (GlobalData_t)(0.)
#define partial_koff_TroponinC_del_Ca_SR (GlobalData_t)(0.)
#define partial_koff_TroponinC_del_Ca_SRB (GlobalData_t)(0.)
#define partial_koff_TroponinC_del_Ca_jct (GlobalData_t)(0.)
#define partial_koff_TroponinC_del_Cai (GlobalData_t)(0.)
#define partial_kon_Calmodulin_del_Ca_SL (GlobalData_t)(0.)
#define partial_kon_Calmodulin_del_Ca_SR (GlobalData_t)(0.)
#define partial_kon_Calmodulin_del_Ca_SRB (GlobalData_t)(0.)
#define partial_kon_Calmodulin_del_Ca_jct (GlobalData_t)(0.)
#define partial_kon_Calmodulin_del_Cai (GlobalData_t)(0.)
#define partial_kon_Calsequestrin_del_Ca_SL (GlobalData_t)(0.)
#define partial_kon_Calsequestrin_del_Ca_SR (GlobalData_t)(0.)
#define partial_kon_Calsequestrin_del_Ca_SRB (GlobalData_t)(0.)
#define partial_kon_Calsequestrin_del_Ca_jct (GlobalData_t)(0.)
#define partial_kon_Calsequestrin_del_Cai (GlobalData_t)(0.)
#define partial_kon_Myosin_Ca_del_Ca_SL (GlobalData_t)(0.)
#define partial_kon_Myosin_Ca_del_Ca_SR (GlobalData_t)(0.)
#define partial_kon_Myosin_Ca_del_Ca_SRB (GlobalData_t)(0.)
#define partial_kon_Myosin_Ca_del_Ca_jct (GlobalData_t)(0.)
#define partial_kon_Myosin_Ca_del_Cai (GlobalData_t)(0.)
#define partial_kon_Myosin_Mg_del_Ca_SL (GlobalData_t)(0.)
#define partial_kon_Myosin_Mg_del_Ca_SR (GlobalData_t)(0.)
#define partial_kon_Myosin_Mg_del_Ca_SRB (GlobalData_t)(0.)
#define partial_kon_Myosin_Mg_del_Ca_jct (GlobalData_t)(0.)
#define partial_kon_Myosin_Mg_del_Cai (GlobalData_t)(0.)
#define partial_kon_SL_del_Ca_SL (GlobalData_t)(0.)
#define partial_kon_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_kon_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_kon_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_kon_SL_del_Cai (GlobalData_t)(0.)
#define partial_kon_SRB_del_Ca_SL (GlobalData_t)(0.)
#define partial_kon_SRB_del_Ca_SR (GlobalData_t)(0.)
#define partial_kon_SRB_del_Ca_SRB (GlobalData_t)(0.)
#define partial_kon_SRB_del_Ca_jct (GlobalData_t)(0.)
#define partial_kon_SRB_del_Cai (GlobalData_t)(0.)
#define partial_kon_TroponinC_Ca_MGCa_del_Ca_SL (GlobalData_t)(0.)
#define partial_kon_TroponinC_Ca_MGCa_del_Ca_SR (GlobalData_t)(0.)
#define partial_kon_TroponinC_Ca_MGCa_del_Ca_SRB (GlobalData_t)(0.)
#define partial_kon_TroponinC_Ca_MGCa_del_Ca_jct (GlobalData_t)(0.)
#define partial_kon_TroponinC_Ca_MGCa_del_Cai (GlobalData_t)(0.)
#define partial_kon_TroponinC_Ca_MGMg_del_Ca_SL (GlobalData_t)(0.)
#define partial_kon_TroponinC_Ca_MGMg_del_Ca_SR (GlobalData_t)(0.)
#define partial_kon_TroponinC_Ca_MGMg_del_Ca_SRB (GlobalData_t)(0.)
#define partial_kon_TroponinC_Ca_MGMg_del_Ca_jct (GlobalData_t)(0.)
#define partial_kon_TroponinC_Ca_MGMg_del_Cai (GlobalData_t)(0.)
#define partial_kon_TroponinC_del_Ca_SL (GlobalData_t)(0.)
#define partial_kon_TroponinC_del_Ca_SR (GlobalData_t)(0.)
#define partial_kon_TroponinC_del_Ca_SRB (GlobalData_t)(0.)
#define partial_kon_TroponinC_del_Ca_jct (GlobalData_t)(0.)
#define partial_kon_TroponinC_del_Cai (GlobalData_t)(0.)
#define partial_ks_del_Ca_SL (GlobalData_t)(0.)
#define partial_ks_del_Ca_SR (GlobalData_t)(0.)
#define partial_ks_del_Ca_SRB (GlobalData_t)(0.)
#define partial_ks_del_Ca_jct (GlobalData_t)(0.)
#define partial_ks_del_Cai (GlobalData_t)(0.)
#define partial_ksat_del_Ca_SL (GlobalData_t)(0.)
#define partial_ksat_del_Ca_SR (GlobalData_t)(0.)
#define partial_ksat_del_Ca_SRB (GlobalData_t)(0.)
#define partial_ksat_del_Ca_jct (GlobalData_t)(0.)
#define partial_ksat_del_Cai (GlobalData_t)(0.)
#define partial_temp_SL_del_Ca_SR (GlobalData_t)(0.)
#define partial_temp_SL_del_Ca_SRB (GlobalData_t)(0.)
#define partial_temp_SL_del_Ca_jct (GlobalData_t)(0.)
#define partial_temp_SL_del_Cai (GlobalData_t)(0.)
#define partial_temp_del_Ca_SL (GlobalData_t)(0.)
#define partial_temp_del_Ca_SR (GlobalData_t)(0.)
#define partial_temp_del_Ca_SRB (GlobalData_t)(0.)
#define partial_temp_del_Ca_jct (GlobalData_t)(0.)
#define partial_temp_del_Cai (GlobalData_t)(0.)
#define partial_temp_jct_del_Ca_SL (GlobalData_t)(0.)
#define partial_temp_jct_del_Ca_SR (GlobalData_t)(0.)
#define partial_temp_jct_del_Ca_SRB (GlobalData_t)(0.)
#define partial_temp_jct_del_Cai (GlobalData_t)(0.)
#define E_Cl (GlobalData_t)((((R*T)/F)*(log((Cli/Clo)))))
#define E_K (GlobalData_t)((((R*T)/F)*(log((Ko/Ki)))))
#define GIKr (GlobalData_t)((0.03*(sqrt((Ko/5.4)))))
#define GK1 (GlobalData_t)((0.9*(sqrt((Ko/5.4)))))
#define Q_CaL (GlobalData_t)((pow(Q10_CaL,((T-(310.))/10.))))
#define Q_NCX (GlobalData_t)((pow(Q10_NCX,((T-(310.))/10.))))
#define Q_SLCaP (GlobalData_t)((pow(Q10_SLCaP,((T-(310.))/10.))))
#define Q_SRCaP (GlobalData_t)((pow(Q10_SRCaP,((T-(310.))/10.))))
#define VolCell (GlobalData_t)((((3.141592654*((cell_radius/1000.)*(cell_radius/1000.)))*cell_length)/((1000.*1000.)*1000.)))
#define partial_j_leak_SR_del_Ca_SR (GlobalData_t)(KSRleak)
#define partial_j_leak_SR_del_Ca_jct (GlobalData_t)((KSRleak*-1.))
#define sigma (GlobalData_t)((((exp((Nao/67.3)))-(1.))/7.))
#define VolSL (GlobalData_t)((0.02*VolCell))
#define VolSR (GlobalData_t)((0.035*VolCell))
#define Voljct (GlobalData_t)(((0.0539*0.01)*VolCell))
#define Volmyo (GlobalData_t)((0.65*VolCell))
#define partial_diff_Ca_SL_del_Ca_jct (GlobalData_t)(((VolSL*8.2413e-13)/(VolSL*VolSL)))
#define partial_diff_Ca_SL_del_Cai (GlobalData_t)(((VolSL*(-(-1.*3.7243e-12)))/(VolSL*VolSL)))
#define partial_diff_Ca_jct_del_Ca_SL (GlobalData_t)((-((Voljct*(-1.*8.2413e-13))/(Voljct*Voljct))))
#define partial_diff_Cai_del_Ca_SL (GlobalData_t)(((Volmyo*3.7243e-12)/(Volmyo*Volmyo)))



void initialize_params_Shannon( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Shannon_Params *p = (Shannon_Params *)IF->params;

  // Compute the regional constants
  {
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_Shannon;

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Shannon( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Shannon_Params *p = (Shannon_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.

}

struct Shannon_Regional_Constants {

};

struct Shannon_Nodal_Req {
  GlobalData_t V;

};

struct Shannon_Private {
  ION_IF *IF;
  int   node_number;
  void* cvode_mem;
  bool  trace_init;
  Shannon_Regional_Constants rc;
  Shannon_Nodal_Req nr;
};

//Printing out the Rosenbrock declarations
void Shannon_rosenbrock_f(float*, float*, void*);
void Shannon_rosenbrock_jacobian(float**, float*, void*, int);

enum Rosenbrock {
  ROSEN_Ca_SL,
  ROSEN_Ca_SR,
  ROSEN_Ca_SRB,
  ROSEN_Ca_jct,
  ROSEN_Cai,

  N_ROSEN
};

void rbStepX  ( float *X,
                void (*calcDX) (float*,  float*, void*),
                void (*calcJ)  (float**, float*, void*, int ),
                void *params, float h, int N );




void    initialize_sv_Shannon( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Shannon_Params *p = (Shannon_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Shannon_state) );
  Shannon_state *sv_base = (Shannon_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Shannon_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    // Initialize the rest of the nodal variables
    sv->Ca_Calmodulin = Ca_Calmodulin_init;
    sv->Ca_Calsequestrin = Ca_Calsequestrin_init;
    sv->Ca_Myosin = Ca_Myosin_init;
    sv->Ca_SL = Ca_SL_init;
    sv->Ca_SLB_SL = Ca_SLB_SL_init;
    sv->Ca_SLB_jct = Ca_SLB_jct_init;
    sv->Ca_SLHigh_SL = Ca_SLHigh_SL_init;
    sv->Ca_SLHigh_jct = Ca_SLHigh_jct_init;
    sv->Ca_SR = Ca_SR_init;
    sv->Ca_SRB = Ca_SRB_init;
    sv->Ca_TroponinC = Ca_TroponinC_init;
    sv->Ca_TroponinC_Ca_Mg = Ca_TroponinC_Ca_Mg_init;
    sv->Ca_jct = Ca_jct_init;
    sv->Cai = Cai_init;
    sv->I = I_init;
    sv->MGMyosin = MGMyosin_init;
    sv->MGTroponinC_Ca_Mg = MGTroponinC_Ca_Mg_init;
    sv->Na_SL = Na_SL_init;
    sv->Na_SL_buf = Na_SL_buf_init;
    sv->Na_jct = Na_jct_init;
    sv->Na_jct_buf = Na_jct_buf_init;
    sv->Nai = Nai_init;
    sv->O = O_init;
    sv->R_cp0 = R_cp0_init;
    sv->R_tos = R_tos_init;
    V = V_init;
    sv->X_tof = X_tof_init;
    sv->X_tos = X_tos_init;
    sv->Xr = Xr_init;
    sv->Xs = Xs_init;
    sv->Y_tof = Y_tof_init;
    sv->Y_tos = Y_tos_init;
    sv->d = d_init;
    sv->f = f_init;
    sv->fCaB_SL = fCaB_SL_init;
    sv->fCaB_jct = fCaB_jct_init;
    sv->h = h_init;
    sv->j = j_init;
    sv->m = m_init;
    double E_Ca_SL = (((R*T)/(2.*F))*(log((Cao/sv->Ca_SL))));
    double E_Ca_jct = (((R*T)/(2.*F))*(log((Cao/sv->Ca_jct))));
    double E_Ks = (((R*T)/F)*(log(((Ko+(pKNa*Nao))/(Ki+(pKNa*sv->Nai))))));
    double E_Na_SL = (((R*T)/F)*(log((Nao/sv->Na_SL))));
    double E_Na_jct = (((R*T)/F)*(log((Nao/sv->Na_jct))));
    double IClCa = ((GCl*(V-(E_Cl)))*((Fx_Cl_jct/(1.+(Kd_ClCa/sv->Ca_jct)))+(Fx_Cl_SL/(1.+(Kd_ClCa/sv->Ca_SL)))));
    double IKp = ((GKp*(V-(E_K)))/(1.+(exp((7.488-((V/5.98)))))));
    double IbCl = (GClBk*(V-(E_Cl)));
    double IpCa_SL = (((Q_SLCaP*V_maxAF)*Fx_SLCaP_SL)/(1.+(pow((Km/sv->Ca_SL),H))));
    double IpCa_jct = (((Q_SLCaP*V_maxAF)*Fx_SLCaP_jct)/(1.+(pow((Km/sv->Ca_jct),H))));
    double Itof = (((Gtof*sv->X_tof)*sv->Y_tof)*(V-(E_K)));
    double Itos = (((Gtos*sv->X_tos)*(sv->Y_tos+(0.5*sv->R_tos)))*(V-(E_K)));
    double Ka_SL = (1./(1.+(((Kd_act/sv->Ca_SL)*(Kd_act/sv->Ca_SL))*(Kd_act/sv->Ca_SL))));
    double Ka_jct = (1./(1.+(((Kd_act/sv->Ca_jct)*(Kd_act/sv->Ca_jct))*(Kd_act/sv->Ca_jct))));
    double Rr = (1./(1.+(exp(((33.+V)/22.4)))));
    double alpha_K1 = (1.02/(1.+(exp((0.2385*(V-((E_K+59.215))))))));
    double beta_K1 = (((0.49124*(exp((0.08032*((V-(E_K))+5.476)))))+(exp((0.06175*(V-((E_K+594.31)))))))/(1.+(exp((-0.5143*((V-(E_K))+4.753))))));
    double fCa_SL = (1.-(sv->fCaB_SL));
    double fCa_jct = (1.-(sv->fCaB_jct));
    double f_NaK = (1./((1.+(0.1245*(exp((((-0.1*V)*F)/(R*T))))))+((0.0365*sigma)*(exp((((-V)*F)/(R*T)))))));
    double openProb = ((((sv->m*sv->m)*sv->m)*sv->h)*sv->j);
    double pCa_SL = ((-(log10(sv->Ca_SL)))+3.);
    double pCa_jct = ((-(log10(sv->Ca_jct)))+3.);
    double temp = ((((((0.45*sv->d)*sv->f)*Q_CaL)*V)*(F*F))/(R*T));
    double temp_SL = (((((exp((((eta*V)*F)/(R*T))))*(pow(sv->Na_SL,HNa)))*Cao)-((((exp(((((eta-(1.))*V)*F)/(R*T))))*(pow(Nao,HNa)))*sv->Ca_SL)))/(1.+(ksat*(exp(((((eta-(1.))*V)*F)/(R*T)))))));
    double temp_jct = (((((exp((((eta*V)*F)/(R*T))))*(pow(sv->Na_jct,HNa)))*Cao)-((((exp(((((eta-(1.))*V)*F)/(R*T))))*(pow(Nao,HNa)))*sv->Ca_jct)))/(1.+(ksat*(exp(((((eta-(1.))*V)*F)/(R*T)))))));
    double GKs_SL = (0.07*(0.057+(0.19/(1.+(exp(((-7.2+pCa_SL)/0.6)))))));
    double GKs_jct = (0.07*(0.057+(0.19/(1.+(exp(((-7.2+pCa_jct)/0.6)))))));
    double ICaL_Ca_SL = ((((((temp*fCa_SL)*Fx_ICaL_SL)*PCa)*4.)*(((gamma_Cai*sv->Ca_SL)*(exp((((2.*V)*F)/(R*T)))))-((gamma_Cao*Cao))))/((exp((((2.*V)*F)/(R*T))))-(1.)));
    double ICaL_Ca_jct = ((((((temp*fCa_jct)*Fx_ICaL_jct)*PCa)*4.)*(((gamma_Cai*sv->Ca_jct)*(exp((((2.*V)*F)/(R*T)))))-((gamma_Cao*Cao))))/((exp((((2.*V)*F)/(R*T))))-(1.)));
    double ICaL_K = ((((temp*((fCa_SL*Fx_ICaL_SL)+(fCa_jct*Fx_ICaL_jct)))*PK)*(((gamma_Ki*Ki)*(exp(((V*F)/(R*T)))))-((gamma_Ko*Ko))))/((exp(((V*F)/(R*T))))-(1.)));
    double ICaL_Na_SL = (((((temp*fCa_SL)*Fx_ICaL_SL)*PNa)*(((gamma_Nai*sv->Na_SL)*(exp(((V*F)/(R*T)))))-((gamma_Nao*Nao))))/((exp(((V*F)/(R*T))))-(1.)));
    double ICaL_Na_jct = (((((temp*fCa_jct)*Fx_ICaL_jct)*PNa)*(((gamma_Nai*sv->Na_jct)*(exp(((V*F)/(R*T)))))-((gamma_Nao*Nao))))/((exp(((V*F)/(R*T))))-(1.)));
    double IKr = (((GIKr*sv->Xr)*Rr)*(V-(E_K)));
    double INaCa_SL = (((((Fx_NCX_SL*V_max)*Ka_SL)*Q_NCX)*temp_SL)/((((((K_mCai*(pow(Nao,HNa)))*(1.+(pow((sv->Na_SL/K_mNai),HNa))))+(((pow(K_mNao,HNa))*sv->Ca_SL)*(1.+(sv->Ca_SL/K_mCai))))+(K_mCao*(pow(sv->Na_SL,HNa))))+((pow(sv->Na_SL,HNa))*Cao))+((pow(Nao,HNa))*sv->Ca_SL)));
    double INaCa_jct = (((((Fx_NCX_jct*V_max)*Ka_jct)*Q_NCX)*temp_jct)/((((((K_mCai*(pow(Nao,HNa)))*(1.+(pow((sv->Na_jct/K_mNai),HNa))))+(((pow(K_mNao,HNa))*sv->Ca_jct)*(1.+(sv->Ca_jct/K_mCai))))+(K_mCao*(pow(sv->Na_jct,HNa))))+((pow(sv->Na_jct,HNa))*Cao))+((pow(Nao,HNa))*sv->Ca_jct)));
    double INaK_SL = (((((Fx_NaK_SL*INaK_max)*f_NaK)/(1.+(pow((Km_Nai/sv->Na_SL),H_NaK))))*Ko)/(Ko+Km_Ko));
    double INaK_jct = (((((Fx_NaK_jct*INaK_max)*f_NaK)/(1.+(pow((Km_Nai/sv->Na_jct),H_NaK))))*Ko)/(Ko+Km_Ko));
    double INa_SL = (((Fx_Na_SL*GINa)*openProb)*(V-(E_Na_SL)));
    double INa_jct = (((Fx_Na_jct*GINa)*openProb)*(V-(E_Na_jct)));
    double IbCa_SL = ((GCaBk*Fx_CaBk_SL)*(V-(E_Ca_SL)));
    double IbCa_jct = ((GCaBk*Fx_CaBk_jct)*(V-(E_Ca_jct)));
    double IbNa_SL = ((Fx_NaBk_SL*GNaBk)*(V-(E_Na_SL)));
    double IbNa_jct = ((Fx_NaBk_jct*GNaBk)*(V-(E_Na_jct)));
    double IpCa = (IpCa_jct+IpCa_SL);
    double K1_infinity = (alpha_K1/(alpha_K1+beta_K1));
    double ICaL = ((((ICaL_Ca_SL+ICaL_Ca_jct)+ICaL_Na_SL)+ICaL_Na_jct)+ICaL_K);
    double IK1 = ((GK1*K1_infinity)*(V-(E_K)));
    double IKs_SL = (((Fx_Ks_SL*GKs_SL)*(sv->Xs*sv->Xs))*(V-(E_Ks)));
    double IKs_jct = (((Fx_Ks_jct*GKs_jct)*(sv->Xs*sv->Xs))*(V-(E_Ks)));
    double INa = (INa_jct+INa_SL);
    double INaCa = (INaCa_jct+INaCa_SL);
    double INaK = (INaK_jct+INaK_SL);
    double IbCa = (IbCa_SL+IbCa_jct);
    double IbNa = (IbNa_jct+IbNa_SL);
    double IKs = (IKs_jct+IKs_SL);
    Iion = ((((((((((((((INa+IbNa)+INaK)+IKr)+IKs)+Itos)+Itof)+IK1)+INaCa)+IClCa)+IbCl)+ICaL)+IbCa)+IpCa)+IKp);
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


  int num_thread = 1;
#ifdef _OPENMP
  num_thread = omp_get_max_threads();
#endif
  Shannon_Private* userdata = (Shannon_Private*)IMP_malloc(num_thread,sizeof(Shannon_Private));
  for( int j=0; j<num_thread; j++ ){
    userdata[j].IF = IF;
    userdata[j].node_number = 0;
  }
  IF->ion_private = userdata;
  IF->private_sz  = sizeof(userdata[0]);


}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Shannon(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Shannon_Params *p  = (Shannon_Params *)IF->params;
  Shannon_state *sv_base = (Shannon_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  
  //Initializing the userdata structures.
  Shannon_Private* ion_private = (Shannon_Private*) IF->ion_private;
  int nthread = 1;
  #ifdef _OPENMP
  nthread = omp_get_max_threads();
  #endif
  for( int j=0; j<nthread; j++ ) {
  }
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Shannon_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t E_Ca_SL = (((R*T)/(2.*F))*(log((Cao/sv->Ca_SL))));
    GlobalData_t E_Ca_jct = (((R*T)/(2.*F))*(log((Cao/sv->Ca_jct))));
    GlobalData_t E_Ks = (((R*T)/F)*(log(((Ko+(pKNa*Nao))/(Ki+(pKNa*sv->Nai))))));
    GlobalData_t E_Na_SL = (((R*T)/F)*(log((Nao/sv->Na_SL))));
    GlobalData_t E_Na_jct = (((R*T)/F)*(log((Nao/sv->Na_jct))));
    GlobalData_t IClCa = ((GCl*(V-(E_Cl)))*((Fx_Cl_jct/(1.+(Kd_ClCa/sv->Ca_jct)))+(Fx_Cl_SL/(1.+(Kd_ClCa/sv->Ca_SL)))));
    GlobalData_t IKp = ((GKp*(V-(E_K)))/(1.+(exp((7.488-((V/5.98)))))));
    GlobalData_t IbCl = (GClBk*(V-(E_Cl)));
    GlobalData_t IpCa_SL = (((Q_SLCaP*V_maxAF)*Fx_SLCaP_SL)/(1.+(pow((Km/sv->Ca_SL),H))));
    GlobalData_t IpCa_jct = (((Q_SLCaP*V_maxAF)*Fx_SLCaP_jct)/(1.+(pow((Km/sv->Ca_jct),H))));
    GlobalData_t Itof = (((Gtof*sv->X_tof)*sv->Y_tof)*(V-(E_K)));
    GlobalData_t Itos = (((Gtos*sv->X_tos)*(sv->Y_tos+(0.5*sv->R_tos)))*(V-(E_K)));
    GlobalData_t Ka_SL = (1./(1.+(((Kd_act/sv->Ca_SL)*(Kd_act/sv->Ca_SL))*(Kd_act/sv->Ca_SL))));
    GlobalData_t Ka_jct = (1./(1.+(((Kd_act/sv->Ca_jct)*(Kd_act/sv->Ca_jct))*(Kd_act/sv->Ca_jct))));
    GlobalData_t Rr = (1./(1.+(exp(((33.+V)/22.4)))));
    GlobalData_t alpha_K1 = (1.02/(1.+(exp((0.2385*(V-((E_K+59.215))))))));
    GlobalData_t beta_K1 = (((0.49124*(exp((0.08032*((V-(E_K))+5.476)))))+(exp((0.06175*(V-((E_K+594.31)))))))/(1.+(exp((-0.5143*((V-(E_K))+4.753))))));
    GlobalData_t fCa_SL = (1.-(sv->fCaB_SL));
    GlobalData_t fCa_jct = (1.-(sv->fCaB_jct));
    GlobalData_t f_NaK = (1./((1.+(0.1245*(exp((((-0.1*V)*F)/(R*T))))))+((0.0365*sigma)*(exp((((-V)*F)/(R*T)))))));
    GlobalData_t openProb = ((((sv->m*sv->m)*sv->m)*sv->h)*sv->j);
    GlobalData_t pCa_SL = ((-(log10(sv->Ca_SL)))+3.);
    GlobalData_t pCa_jct = ((-(log10(sv->Ca_jct)))+3.);
    GlobalData_t temp = ((((((0.45*sv->d)*sv->f)*Q_CaL)*V)*(F*F))/(R*T));
    GlobalData_t temp_SL = (((((exp((((eta*V)*F)/(R*T))))*(pow(sv->Na_SL,HNa)))*Cao)-((((exp(((((eta-(1.))*V)*F)/(R*T))))*(pow(Nao,HNa)))*sv->Ca_SL)))/(1.+(ksat*(exp(((((eta-(1.))*V)*F)/(R*T)))))));
    GlobalData_t temp_jct = (((((exp((((eta*V)*F)/(R*T))))*(pow(sv->Na_jct,HNa)))*Cao)-((((exp(((((eta-(1.))*V)*F)/(R*T))))*(pow(Nao,HNa)))*sv->Ca_jct)))/(1.+(ksat*(exp(((((eta-(1.))*V)*F)/(R*T)))))));
    GlobalData_t GKs_SL = (0.07*(0.057+(0.19/(1.+(exp(((-7.2+pCa_SL)/0.6)))))));
    GlobalData_t GKs_jct = (0.07*(0.057+(0.19/(1.+(exp(((-7.2+pCa_jct)/0.6)))))));
    GlobalData_t ICaL_Ca_SL = ((((((temp*fCa_SL)*Fx_ICaL_SL)*PCa)*4.)*(((gamma_Cai*sv->Ca_SL)*(exp((((2.*V)*F)/(R*T)))))-((gamma_Cao*Cao))))/((exp((((2.*V)*F)/(R*T))))-(1.)));
    GlobalData_t ICaL_Ca_jct = ((((((temp*fCa_jct)*Fx_ICaL_jct)*PCa)*4.)*(((gamma_Cai*sv->Ca_jct)*(exp((((2.*V)*F)/(R*T)))))-((gamma_Cao*Cao))))/((exp((((2.*V)*F)/(R*T))))-(1.)));
    GlobalData_t ICaL_K = ((((temp*((fCa_SL*Fx_ICaL_SL)+(fCa_jct*Fx_ICaL_jct)))*PK)*(((gamma_Ki*Ki)*(exp(((V*F)/(R*T)))))-((gamma_Ko*Ko))))/((exp(((V*F)/(R*T))))-(1.)));
    GlobalData_t ICaL_Na_SL = (((((temp*fCa_SL)*Fx_ICaL_SL)*PNa)*(((gamma_Nai*sv->Na_SL)*(exp(((V*F)/(R*T)))))-((gamma_Nao*Nao))))/((exp(((V*F)/(R*T))))-(1.)));
    GlobalData_t ICaL_Na_jct = (((((temp*fCa_jct)*Fx_ICaL_jct)*PNa)*(((gamma_Nai*sv->Na_jct)*(exp(((V*F)/(R*T)))))-((gamma_Nao*Nao))))/((exp(((V*F)/(R*T))))-(1.)));
    GlobalData_t IKr = (((GIKr*sv->Xr)*Rr)*(V-(E_K)));
    GlobalData_t INaCa_SL = (((((Fx_NCX_SL*V_max)*Ka_SL)*Q_NCX)*temp_SL)/((((((K_mCai*(pow(Nao,HNa)))*(1.+(pow((sv->Na_SL/K_mNai),HNa))))+(((pow(K_mNao,HNa))*sv->Ca_SL)*(1.+(sv->Ca_SL/K_mCai))))+(K_mCao*(pow(sv->Na_SL,HNa))))+((pow(sv->Na_SL,HNa))*Cao))+((pow(Nao,HNa))*sv->Ca_SL)));
    GlobalData_t INaCa_jct = (((((Fx_NCX_jct*V_max)*Ka_jct)*Q_NCX)*temp_jct)/((((((K_mCai*(pow(Nao,HNa)))*(1.+(pow((sv->Na_jct/K_mNai),HNa))))+(((pow(K_mNao,HNa))*sv->Ca_jct)*(1.+(sv->Ca_jct/K_mCai))))+(K_mCao*(pow(sv->Na_jct,HNa))))+((pow(sv->Na_jct,HNa))*Cao))+((pow(Nao,HNa))*sv->Ca_jct)));
    GlobalData_t INaK_SL = (((((Fx_NaK_SL*INaK_max)*f_NaK)/(1.+(pow((Km_Nai/sv->Na_SL),H_NaK))))*Ko)/(Ko+Km_Ko));
    GlobalData_t INaK_jct = (((((Fx_NaK_jct*INaK_max)*f_NaK)/(1.+(pow((Km_Nai/sv->Na_jct),H_NaK))))*Ko)/(Ko+Km_Ko));
    GlobalData_t INa_SL = (((Fx_Na_SL*GINa)*openProb)*(V-(E_Na_SL)));
    GlobalData_t INa_jct = (((Fx_Na_jct*GINa)*openProb)*(V-(E_Na_jct)));
    GlobalData_t IbCa_SL = ((GCaBk*Fx_CaBk_SL)*(V-(E_Ca_SL)));
    GlobalData_t IbCa_jct = ((GCaBk*Fx_CaBk_jct)*(V-(E_Ca_jct)));
    GlobalData_t IbNa_SL = ((Fx_NaBk_SL*GNaBk)*(V-(E_Na_SL)));
    GlobalData_t IbNa_jct = ((Fx_NaBk_jct*GNaBk)*(V-(E_Na_jct)));
    GlobalData_t IpCa = (IpCa_jct+IpCa_SL);
    GlobalData_t K1_infinity = (alpha_K1/(alpha_K1+beta_K1));
    GlobalData_t ICaL = ((((ICaL_Ca_SL+ICaL_Ca_jct)+ICaL_Na_SL)+ICaL_Na_jct)+ICaL_K);
    GlobalData_t IK1 = ((GK1*K1_infinity)*(V-(E_K)));
    GlobalData_t IKs_SL = (((Fx_Ks_SL*GKs_SL)*(sv->Xs*sv->Xs))*(V-(E_Ks)));
    GlobalData_t IKs_jct = (((Fx_Ks_jct*GKs_jct)*(sv->Xs*sv->Xs))*(V-(E_Ks)));
    GlobalData_t INa = (INa_jct+INa_SL);
    GlobalData_t INaCa = (INaCa_jct+INaCa_SL);
    GlobalData_t INaK = (INaK_jct+INaK_SL);
    GlobalData_t IbCa = (IbCa_SL+IbCa_jct);
    GlobalData_t IbNa = (IbNa_jct+IbNa_SL);
    GlobalData_t IKs = (IKs_jct+IKs_SL);
    Iion = ((((((((((((((INa+IbNa)+INaK)+IKr)+IKs)+Itos)+Itof)+IK1)+INaCa)+IClCa)+IbCl)+ICaL)+IbCa)+IpCa)+IKp);
    
    
    //Complete Forward Euler Update
    GlobalData_t J_Na_SL_myo = ((sv->Na_SL-(sv->Nai))*1.6386e-12);
    GlobalData_t J_Na_jct_SL = ((sv->Na_jct-(sv->Na_SL))*1.8313e-14);
    GlobalData_t RI = (((1.-(sv->R_cp0))-(sv->O))-(sv->I));
    GlobalData_t dCa_Calmodulin = (((kon_Calmodulin*sv->Cai)*(Bmax_Calmodulin-(sv->Ca_Calmodulin)))-((koff_Calmodulin*sv->Ca_Calmodulin)));
    GlobalData_t dCa_Myosin = (((kon_Myosin_Ca*sv->Cai)*(Bmax_Myosin_Ca-((sv->Ca_Myosin+sv->MGMyosin))))-((koff_Myosin_Ca*sv->Ca_Myosin)));
    GlobalData_t dCa_SLB_SL = (((kon_SL*sv->Ca_SL)*(((Bmax_SLB_SL*Volmyo)/VolSL)-(sv->Ca_SLB_SL)))-((koff_SLB*sv->Ca_SLB_SL)));
    GlobalData_t dCa_SLB_jct = (((kon_SL*sv->Ca_jct)*((((Bmax_SLB_jct*0.1)*Volmyo)/Voljct)-(sv->Ca_SLB_jct)))-((koff_SLB*sv->Ca_SLB_jct)));
    GlobalData_t dCa_SLHigh_SL = (((kon_SL*sv->Ca_SL)*(((Bmax_SLHigh_SL*Volmyo)/VolSL)-(sv->Ca_SLHigh_SL)))-((koff_SLHigh*sv->Ca_SLHigh_SL)));
    GlobalData_t dCa_SLHigh_jct = (((kon_SL*sv->Ca_jct)*((((Bmax_SLHigh_jct*0.1)*Volmyo)/Voljct)-(sv->Ca_SLHigh_jct)))-((koff_SLHigh*sv->Ca_SLHigh_jct)));
    GlobalData_t dCa_TroponinC = (((kon_TroponinC*sv->Cai)*(Bmax_TroponinC-(sv->Ca_TroponinC)))-((koff_TroponinC*sv->Ca_TroponinC)));
    GlobalData_t dCa_TroponinC_Ca_Mg = (((kon_TroponinC_Ca_MGCa*sv->Cai)*(Bmax_TroponinC_Ca_MGCa-((sv->Ca_TroponinC_Ca_Mg+sv->MGTroponinC_Ca_Mg))))-((koff_TroponinC_Ca_MGCa*sv->Ca_TroponinC_Ca_Mg)));
    GlobalData_t dCalsequestrin = (((kon_Calsequestrin*sv->Ca_SR)*(((Bmax_Calsequestrin*Volmyo)/VolSR)-(sv->Ca_Calsequestrin)))-((koff_Calsequestrin*sv->Ca_Calsequestrin)));
    GlobalData_t dMGMyosin = (((kon_Myosin_Mg*Mgi)*(Bmax_Myosin_Mg-((sv->Ca_Myosin+sv->MGMyosin))))-((koff_Myosin_Mg*sv->MGMyosin)));
    GlobalData_t dMGTroponinC_Ca_Mg = (((kon_TroponinC_Ca_MGMg*Mgi)*(Bmax_TroponinC_Ca_MGMg-((sv->Ca_TroponinC_Ca_Mg+sv->MGTroponinC_Ca_Mg))))-((koff_TroponinC_Ca_MGMg*sv->MGTroponinC_Ca_Mg)));
    GlobalData_t dNa_SL_buf = (((kon*sv->Na_SL)*(Bmax_SL-(sv->Na_SL_buf)))-((koff*sv->Na_SL_buf)));
    GlobalData_t dNa_jct_buf = (((kon*sv->Na_jct)*(Bmax_jct-(sv->Na_jct_buf)))-((koff*sv->Na_jct_buf)));
    GlobalData_t diff_fCaB_SL = (((1.7*sv->Ca_SL)*(1.-(sv->fCaB_SL)))-((11.9e-3*sv->fCaB_SL)));
    GlobalData_t diff_fCaB_jct = (((1.7*sv->Ca_jct)*(1.-(sv->fCaB_jct)))-((11.9e-3*sv->fCaB_jct)));
    GlobalData_t kCaSR = (Max_SR-(((Max_SR-(Min_SR))/(1.+(pow((EC50_SR/sv->Ca_SR),HSR))))));
    GlobalData_t diff_Ca_Calmodulin = dCa_Calmodulin;
    GlobalData_t diff_Ca_Calsequestrin = dCalsequestrin;
    GlobalData_t diff_Ca_Myosin = dCa_Myosin;
    GlobalData_t diff_Ca_SLB_SL = dCa_SLB_SL;
    GlobalData_t diff_Ca_SLB_jct = dCa_SLB_jct;
    GlobalData_t diff_Ca_SLHigh_SL = dCa_SLHigh_SL;
    GlobalData_t diff_Ca_SLHigh_jct = dCa_SLHigh_jct;
    GlobalData_t diff_Ca_TroponinC = dCa_TroponinC;
    GlobalData_t diff_Ca_TroponinC_Ca_Mg = dCa_TroponinC_Ca_Mg;
    GlobalData_t diff_MGMyosin = dMGMyosin;
    GlobalData_t diff_MGTroponinC_Ca_Mg = dMGTroponinC_Ca_Mg;
    GlobalData_t diff_Na_SL = (((((-Cm)*((((INa_SL+(3.*INaCa_SL))+IbNa_SL)+(3.*INaK_SL))+ICaL_Na_SL))/(VolSL*F))+((J_Na_jct_SL-(J_Na_SL_myo))/VolSL))-(dNa_SL_buf));
    GlobalData_t diff_Na_SL_buf = dNa_SL_buf;
    GlobalData_t diff_Na_jct = (((((-Cm)*((((INa_jct+(3.*INaCa_jct))+IbNa_jct)+(3.*INaK_jct))+ICaL_Na_jct))/(Voljct*F))-((J_Na_jct_SL/Voljct)))-(dNa_jct_buf));
    GlobalData_t diff_Na_jct_buf = dNa_jct_buf;
    GlobalData_t diff_Nai = (J_Na_SL_myo/Volmyo);
    GlobalData_t kiSRCa = (kiCa*kCaSR);
    GlobalData_t koSRCa = (koCa/kCaSR);
    GlobalData_t diff_I = ((((kiSRCa*sv->Ca_jct)*sv->O)-((kim*sv->I)))-(((kom*sv->I)-(((koSRCa*(sv->Ca_jct*sv->Ca_jct))*RI)))));
    GlobalData_t diff_O = ((((koSRCa*(sv->Ca_jct*sv->Ca_jct))*sv->R_cp0)-((kom*sv->O)))-((((kiSRCa*sv->Ca_jct)*sv->O)-((kim*sv->I)))));
    GlobalData_t diff_R_cp0 = (((kim*RI)-(((kiSRCa*sv->Ca_jct)*sv->R_cp0)))-((((koSRCa*(sv->Ca_jct*sv->Ca_jct))*sv->R_cp0)-((kom*sv->O)))));
    GlobalData_t Ca_Calmodulin_new = sv->Ca_Calmodulin+diff_Ca_Calmodulin*dt;
    GlobalData_t Ca_Calsequestrin_new = sv->Ca_Calsequestrin+diff_Ca_Calsequestrin*dt;
    GlobalData_t Ca_Myosin_new = sv->Ca_Myosin+diff_Ca_Myosin*dt;
    GlobalData_t Ca_SLB_SL_new = sv->Ca_SLB_SL+diff_Ca_SLB_SL*dt;
    GlobalData_t Ca_SLB_jct_new = sv->Ca_SLB_jct+diff_Ca_SLB_jct*dt;
    GlobalData_t Ca_SLHigh_SL_new = sv->Ca_SLHigh_SL+diff_Ca_SLHigh_SL*dt;
    GlobalData_t Ca_SLHigh_jct_new = sv->Ca_SLHigh_jct+diff_Ca_SLHigh_jct*dt;
    GlobalData_t Ca_TroponinC_new = sv->Ca_TroponinC+diff_Ca_TroponinC*dt;
    GlobalData_t Ca_TroponinC_Ca_Mg_new = sv->Ca_TroponinC_Ca_Mg+diff_Ca_TroponinC_Ca_Mg*dt;
    GlobalData_t I_new = sv->I+diff_I*dt;
    GlobalData_t MGMyosin_new = sv->MGMyosin+diff_MGMyosin*dt;
    GlobalData_t MGTroponinC_Ca_Mg_new = sv->MGTroponinC_Ca_Mg+diff_MGTroponinC_Ca_Mg*dt;
    GlobalData_t Na_SL_new = sv->Na_SL+diff_Na_SL*dt;
    GlobalData_t Na_SL_buf_new = sv->Na_SL_buf+diff_Na_SL_buf*dt;
    GlobalData_t Na_jct_new = sv->Na_jct+diff_Na_jct*dt;
    GlobalData_t Na_jct_buf_new = sv->Na_jct_buf+diff_Na_jct_buf*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t O_new = sv->O+diff_O*dt;
    GlobalData_t R_cp0_new = sv->R_cp0+diff_R_cp0*dt;
    GlobalData_t fCaB_SL_new = sv->fCaB_SL+diff_fCaB_SL*dt;
    GlobalData_t fCaB_jct_new = sv->fCaB_jct+diff_fCaB_jct*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t R_tos_infinity = (1./(1.+(exp(((V+33.5)/10.)))));
    GlobalData_t X_tof_infinity = (1./(1.+(exp(((-(V+3.))/15.)))));
    GlobalData_t X_tos_infinity = (1./(1.+(exp(((-(V+3.))/15.)))));
    GlobalData_t Xr_infinity = (1./(1.+(exp(((-(50.+V))/7.5)))));
    GlobalData_t Xs_infinity = (1./(1.+(exp(((-(V-(1.5)))/16.7)))));
    GlobalData_t Y_tof_infinity = (1./(1.+(exp(((V+33.5)/10.)))));
    GlobalData_t Y_tos_infinity = (1./(1.+(exp(((V+33.5)/10.)))));
    GlobalData_t alpha_h = ((V<-40.) ? (0.135*(exp(((80.+V)/-6.8)))) : 0.);
    GlobalData_t alpha_j = ((V<-40.) ? ((((-1.2714e5*(exp((0.2444*V))))-((3.474e-5*(exp((-0.04391*V))))))*(V+37.78))/(1.+(exp((0.311*(V+79.23)))))) : 0.);
    GlobalData_t alpha_m = ((0.32*(V+47.13))/(1.-((exp((-0.1*(V+47.13)))))));
    GlobalData_t beta_h = ((V<-40.) ? ((3.56*(exp((0.079*V))))+(3.1e5*(exp((0.35*V))))) : (1./(0.13*(1.+(exp(((V+10.66)/-11.1)))))));
    GlobalData_t beta_j = ((V<-40.) ? ((0.1212*(exp((-0.01052*V))))/(1.+(exp((-0.1378*(V+40.14)))))) : ((0.3*(exp((-2.535e-7*V))))/(1.+(exp((-0.1*(V+32.)))))));
    GlobalData_t beta_m = (0.08*(exp(((-V)/11.))));
    GlobalData_t d_infinity = (1./(1.+(exp(((-(V+14.5))/6.)))));
    GlobalData_t f_infinity = ((1./(1.+(exp(((V+35.06)/3.6)))))+(0.6/(1.+(exp(((50.-(V))/20.))))));
    GlobalData_t tau_R_tos = ((2.8e3/(1.+(exp(((V+60.)/10.)))))+220.);
    GlobalData_t tau_X_tof = ((3.5*(exp((-((V/30.)*(V/30.))))))+1.5);
    GlobalData_t tau_X_tos = ((9./(1.+(exp(((V+3.)/15.)))))+0.5);
    GlobalData_t tau_Xr = (1./(((0.00138*(V+7.))/(1.-((exp((-0.123*(V+7.)))))))+((0.00061*(V+10.))/((exp((0.145*(V+10.))))-(1.)))));
    GlobalData_t tau_Xs = (1./(((7.19e-5*(V+30.))/(1.-((exp((-0.148*(V+30.)))))))+((1.31e-4*(V+30.))/(-1.+(exp((0.0687*(V+30.))))))));
    GlobalData_t tau_Y_tof = ((20./(1.+(exp(((V+33.5)/10.)))))+20.);
    GlobalData_t tau_Y_tos = ((3000./(1.+(exp(((V+60.)/10.)))))+30.);
    GlobalData_t tau_f = (1./((0.0197*(exp((-((0.0337*(V+14.5))*(0.0337*(V+14.5)))))))+0.02));
    GlobalData_t R_tos_rush_larsen_B = (exp(((-dt)/tau_R_tos)));
    GlobalData_t R_tos_rush_larsen_C = (expm1(((-dt)/tau_R_tos)));
    GlobalData_t X_tof_rush_larsen_B = (exp(((-dt)/tau_X_tof)));
    GlobalData_t X_tof_rush_larsen_C = (expm1(((-dt)/tau_X_tof)));
    GlobalData_t X_tos_rush_larsen_B = (exp(((-dt)/tau_X_tos)));
    GlobalData_t X_tos_rush_larsen_C = (expm1(((-dt)/tau_X_tos)));
    GlobalData_t Xr_rush_larsen_B = (exp(((-dt)/tau_Xr)));
    GlobalData_t Xr_rush_larsen_C = (expm1(((-dt)/tau_Xr)));
    GlobalData_t Xs_rush_larsen_B = (exp(((-dt)/tau_Xs)));
    GlobalData_t Xs_rush_larsen_C = (expm1(((-dt)/tau_Xs)));
    GlobalData_t Y_tof_rush_larsen_B = (exp(((-dt)/tau_Y_tof)));
    GlobalData_t Y_tof_rush_larsen_C = (expm1(((-dt)/tau_Y_tof)));
    GlobalData_t Y_tos_rush_larsen_B = (exp(((-dt)/tau_Y_tos)));
    GlobalData_t Y_tos_rush_larsen_C = (expm1(((-dt)/tau_Y_tos)));
    GlobalData_t f_rush_larsen_B = (exp(((-dt)/tau_f)));
    GlobalData_t f_rush_larsen_C = (expm1(((-dt)/tau_f)));
    GlobalData_t h_rush_larsen_A = (((-alpha_h)/(alpha_h+beta_h))*(expm1(((-dt)*(alpha_h+beta_h)))));
    GlobalData_t h_rush_larsen_B = (exp(((-dt)*(alpha_h+beta_h))));
    GlobalData_t j_rush_larsen_A = (((-alpha_j)/(alpha_j+beta_j))*(expm1(((-dt)*(alpha_j+beta_j)))));
    GlobalData_t j_rush_larsen_B = (exp(((-dt)*(alpha_j+beta_j))));
    GlobalData_t m_rush_larsen_A = (((-alpha_m)/(alpha_m+beta_m))*(expm1(((-dt)*(alpha_m+beta_m)))));
    GlobalData_t m_rush_larsen_B = (exp(((-dt)*(alpha_m+beta_m))));
    GlobalData_t tau_d = ((d_infinity*(1.-((exp(((-(V+14.5))/6.))))))/(0.035*(V+14.5)));
    GlobalData_t R_tos_rush_larsen_A = ((-R_tos_infinity)*R_tos_rush_larsen_C);
    GlobalData_t X_tof_rush_larsen_A = ((-X_tof_infinity)*X_tof_rush_larsen_C);
    GlobalData_t X_tos_rush_larsen_A = ((-X_tos_infinity)*X_tos_rush_larsen_C);
    GlobalData_t Xr_rush_larsen_A = ((-Xr_infinity)*Xr_rush_larsen_C);
    GlobalData_t Xs_rush_larsen_A = ((-Xs_infinity)*Xs_rush_larsen_C);
    GlobalData_t Y_tof_rush_larsen_A = ((-Y_tof_infinity)*Y_tof_rush_larsen_C);
    GlobalData_t Y_tos_rush_larsen_A = ((-Y_tos_infinity)*Y_tos_rush_larsen_C);
    GlobalData_t d_rush_larsen_B = (exp(((-dt)/tau_d)));
    GlobalData_t d_rush_larsen_C = (expm1(((-dt)/tau_d)));
    GlobalData_t f_rush_larsen_A = ((-f_infinity)*f_rush_larsen_C);
    GlobalData_t d_rush_larsen_A = ((-d_infinity)*d_rush_larsen_C);
    GlobalData_t R_tos_new = R_tos_rush_larsen_A+R_tos_rush_larsen_B*sv->R_tos;
    GlobalData_t X_tof_new = X_tof_rush_larsen_A+X_tof_rush_larsen_B*sv->X_tof;
    GlobalData_t X_tos_new = X_tos_rush_larsen_A+X_tos_rush_larsen_B*sv->X_tos;
    GlobalData_t Xr_new = Xr_rush_larsen_A+Xr_rush_larsen_B*sv->Xr;
    GlobalData_t Xs_new = Xs_rush_larsen_A+Xs_rush_larsen_B*sv->Xs;
    GlobalData_t Y_tof_new = Y_tof_rush_larsen_A+Y_tof_rush_larsen_B*sv->Y_tof;
    GlobalData_t Y_tos_new = Y_tos_rush_larsen_A+Y_tos_rush_larsen_B*sv->Y_tos;
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f_new = f_rush_larsen_A+f_rush_larsen_B*sv->f;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    int thread=0;
    #ifdef _OPENMP
    thread = omp_get_thread_num();
    #endif
    float Rosenbrock_X[N_ROSEN];
    Rosenbrock_X[ROSEN_Ca_SL] = sv->Ca_SL;
    Rosenbrock_X[ROSEN_Ca_SR] = sv->Ca_SR;
    Rosenbrock_X[ROSEN_Ca_SRB] = sv->Ca_SRB;
    Rosenbrock_X[ROSEN_Ca_jct] = sv->Ca_jct;
    Rosenbrock_X[ROSEN_Cai] = sv->Cai;
    ion_private[thread].nr.V = V;
    ion_private[thread].node_number = __i;
    
    rbStepX(Rosenbrock_X, Shannon_rosenbrock_f, Shannon_rosenbrock_jacobian, (void*)(ion_private+thread), dt, N_ROSEN);
    GlobalData_t Ca_SL_new = Rosenbrock_X[ROSEN_Ca_SL];
    GlobalData_t Ca_SR_new = Rosenbrock_X[ROSEN_Ca_SR];
    GlobalData_t Ca_SRB_new = Rosenbrock_X[ROSEN_Ca_SRB];
    GlobalData_t Ca_jct_new = Rosenbrock_X[ROSEN_Ca_jct];
    GlobalData_t Cai_new = Rosenbrock_X[ROSEN_Cai];
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Ca_Calmodulin = Ca_Calmodulin_new;
    sv->Ca_Calsequestrin = Ca_Calsequestrin_new;
    sv->Ca_Myosin = Ca_Myosin_new;
    sv->Ca_SL = Ca_SL_new;
    sv->Ca_SLB_SL = Ca_SLB_SL_new;
    sv->Ca_SLB_jct = Ca_SLB_jct_new;
    sv->Ca_SLHigh_SL = Ca_SLHigh_SL_new;
    sv->Ca_SLHigh_jct = Ca_SLHigh_jct_new;
    sv->Ca_SR = Ca_SR_new;
    sv->Ca_SRB = Ca_SRB_new;
    sv->Ca_TroponinC = Ca_TroponinC_new;
    sv->Ca_TroponinC_Ca_Mg = Ca_TroponinC_Ca_Mg_new;
    sv->Ca_jct = Ca_jct_new;
    sv->Cai = Cai_new;
    sv->I = I_new;
    Iion = Iion;
    sv->MGMyosin = MGMyosin_new;
    sv->MGTroponinC_Ca_Mg = MGTroponinC_Ca_Mg_new;
    sv->Na_SL = Na_SL_new;
    sv->Na_SL_buf = Na_SL_buf_new;
    sv->Na_jct = Na_jct_new;
    sv->Na_jct_buf = Na_jct_buf_new;
    sv->Nai = Nai_new;
    sv->O = O_new;
    sv->R_cp0 = R_cp0_new;
    sv->R_tos = R_tos_new;
    sv->X_tof = X_tof_new;
    sv->X_tos = X_tos_new;
    sv->Xr = Xr_new;
    sv->Xs = Xs_new;
    sv->Y_tof = Y_tof_new;
    sv->Y_tos = Y_tos_new;
    sv->d = d_new;
    sv->f = f_new;
    sv->fCaB_SL = fCaB_SL_new;
    sv->fCaB_jct = fCaB_jct_new;
    sv->h = h_new;
    sv->j = j_new;
    sv->m = m_new;
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}

void Shannon_rosenbrock_f(float* Rosenbrock_DX, float* Rosenbrock_X, void* user_data) {
  Shannon_Private* ion_private = (Shannon_Private*)user_data;
  Shannon_Regional_Constants* rc = &ion_private->rc;
  ION_IF* IF = ion_private->IF;
  int __i = ((Shannon_Private*)user_data)->node_number;
  Shannon_Nodal_Req* nr = &ion_private->nr;
  cell_geom *region = &IF->cgeom;
  Shannon_Params *p = (Shannon_Params *)IF->params;
  Shannon_state *sv_base = (Shannon_state *)IF->sv_tab.y;
  Shannon_state *sv = sv_base+__i;

  GlobalData_t E_Ca_SL = (((R*T)/(2.*F))*(log((Cao/Rosenbrock_X[ROSEN_Ca_SL]))));
  GlobalData_t E_Ca_jct = (((R*T)/(2.*F))*(log((Cao/Rosenbrock_X[ROSEN_Ca_jct]))));
  GlobalData_t IpCa_SL = (((Q_SLCaP*V_maxAF)*Fx_SLCaP_SL)/(1.+(pow((Km/Rosenbrock_X[ROSEN_Ca_SL]),H))));
  GlobalData_t IpCa_jct = (((Q_SLCaP*V_maxAF)*Fx_SLCaP_jct)/(1.+(pow((Km/Rosenbrock_X[ROSEN_Ca_jct]),H))));
  GlobalData_t J_Ca_SL_myo = ((Rosenbrock_X[ROSEN_Ca_SL]-(Rosenbrock_X[ROSEN_Cai]))*3.7243e-12);
  GlobalData_t J_Ca_jct_SL = ((Rosenbrock_X[ROSEN_Ca_jct]-(Rosenbrock_X[ROSEN_Ca_SL]))*8.2413e-13);
  GlobalData_t Ka_SL = (1./(1.+(((Kd_act/Rosenbrock_X[ROSEN_Ca_SL])*(Kd_act/Rosenbrock_X[ROSEN_Ca_SL]))*(Kd_act/Rosenbrock_X[ROSEN_Ca_SL]))));
  GlobalData_t Ka_jct = (1./(1.+(((Kd_act/Rosenbrock_X[ROSEN_Ca_jct])*(Kd_act/Rosenbrock_X[ROSEN_Ca_jct]))*(Kd_act/Rosenbrock_X[ROSEN_Ca_jct]))));
  GlobalData_t dCa_Calmodulin = (((kon_Calmodulin*Rosenbrock_X[ROSEN_Cai])*(Bmax_Calmodulin-(sv->Ca_Calmodulin)))-((koff_Calmodulin*sv->Ca_Calmodulin)));
  GlobalData_t dCa_Myosin = (((kon_Myosin_Ca*Rosenbrock_X[ROSEN_Cai])*(Bmax_Myosin_Ca-((sv->Ca_Myosin+sv->MGMyosin))))-((koff_Myosin_Ca*sv->Ca_Myosin)));
  GlobalData_t dCa_SLB_SL = (((kon_SL*Rosenbrock_X[ROSEN_Ca_SL])*(((Bmax_SLB_SL*Volmyo)/VolSL)-(sv->Ca_SLB_SL)))-((koff_SLB*sv->Ca_SLB_SL)));
  GlobalData_t dCa_SLB_jct = (((kon_SL*Rosenbrock_X[ROSEN_Ca_jct])*((((Bmax_SLB_jct*0.1)*Volmyo)/Voljct)-(sv->Ca_SLB_jct)))-((koff_SLB*sv->Ca_SLB_jct)));
  GlobalData_t dCa_SLHigh_SL = (((kon_SL*Rosenbrock_X[ROSEN_Ca_SL])*(((Bmax_SLHigh_SL*Volmyo)/VolSL)-(sv->Ca_SLHigh_SL)))-((koff_SLHigh*sv->Ca_SLHigh_SL)));
  GlobalData_t dCa_SLHigh_jct = (((kon_SL*Rosenbrock_X[ROSEN_Ca_jct])*((((Bmax_SLHigh_jct*0.1)*Volmyo)/Voljct)-(sv->Ca_SLHigh_jct)))-((koff_SLHigh*sv->Ca_SLHigh_jct)));
  GlobalData_t dCa_SRB = (((kon_SRB*Rosenbrock_X[ROSEN_Cai])*(Bmax_SRB-(Rosenbrock_X[ROSEN_Ca_SRB])))-((koff_SRB*Rosenbrock_X[ROSEN_Ca_SRB])));
  GlobalData_t dCa_TroponinC = (((kon_TroponinC*Rosenbrock_X[ROSEN_Cai])*(Bmax_TroponinC-(sv->Ca_TroponinC)))-((koff_TroponinC*sv->Ca_TroponinC)));
  GlobalData_t dCa_TroponinC_Ca_Mg = (((kon_TroponinC_Ca_MGCa*Rosenbrock_X[ROSEN_Cai])*(Bmax_TroponinC_Ca_MGCa-((sv->Ca_TroponinC_Ca_Mg+sv->MGTroponinC_Ca_Mg))))-((koff_TroponinC_Ca_MGCa*sv->Ca_TroponinC_Ca_Mg)));
  GlobalData_t dCalsequestrin = (((kon_Calsequestrin*Rosenbrock_X[ROSEN_Ca_SR])*(((Bmax_Calsequestrin*Volmyo)/VolSR)-(sv->Ca_Calsequestrin)))-((koff_Calsequestrin*sv->Ca_Calsequestrin)));
  GlobalData_t dMGMyosin = (((kon_Myosin_Mg*Mgi)*(Bmax_Myosin_Mg-((sv->Ca_Myosin+sv->MGMyosin))))-((koff_Myosin_Mg*sv->MGMyosin)));
  GlobalData_t dMGTroponinC_Ca_Mg = (((kon_TroponinC_Ca_MGMg*Mgi)*(Bmax_TroponinC_Ca_MGMg-((sv->Ca_TroponinC_Ca_Mg+sv->MGTroponinC_Ca_Mg))))-((koff_TroponinC_Ca_MGMg*sv->MGTroponinC_Ca_Mg)));
  GlobalData_t fCa_SL = (1.-(sv->fCaB_SL));
  GlobalData_t fCa_jct = (1.-(sv->fCaB_jct));
  GlobalData_t j_leak_SR = (KSRleak*(Rosenbrock_X[ROSEN_Ca_SR]-(Rosenbrock_X[ROSEN_Ca_jct])));
  GlobalData_t j_pump_SR = (((Q_SRCaP*V_max_cp0)*((pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0))-((pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))))/((1.+(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0)))+(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0))));
  GlobalData_t j_rel_SR = ((ks*sv->O)*(Rosenbrock_X[ROSEN_Ca_SR]-(Rosenbrock_X[ROSEN_Ca_jct])));
  GlobalData_t temp = ((((((0.45*sv->d)*sv->f)*Q_CaL)*nr->V)*(F*F))/(R*T));
  GlobalData_t temp_SL = (((((exp((((eta*nr->V)*F)/(R*T))))*(pow(sv->Na_SL,HNa)))*Cao)-((((exp(((((eta-(1.))*nr->V)*F)/(R*T))))*(pow(Nao,HNa)))*Rosenbrock_X[ROSEN_Ca_SL])))/(1.+(ksat*(exp(((((eta-(1.))*nr->V)*F)/(R*T)))))));
  GlobalData_t temp_jct = (((((exp((((eta*nr->V)*F)/(R*T))))*(pow(sv->Na_jct,HNa)))*Cao)-((((exp(((((eta-(1.))*nr->V)*F)/(R*T))))*(pow(Nao,HNa)))*Rosenbrock_X[ROSEN_Ca_jct])))/(1.+(ksat*(exp(((((eta-(1.))*nr->V)*F)/(R*T)))))));
  GlobalData_t ICaL_Ca_SL = ((((((temp*fCa_SL)*Fx_ICaL_SL)*PCa)*4.)*(((gamma_Cai*Rosenbrock_X[ROSEN_Ca_SL])*(exp((((2.*nr->V)*F)/(R*T)))))-((gamma_Cao*Cao))))/((exp((((2.*nr->V)*F)/(R*T))))-(1.)));
  GlobalData_t ICaL_Ca_jct = ((((((temp*fCa_jct)*Fx_ICaL_jct)*PCa)*4.)*(((gamma_Cai*Rosenbrock_X[ROSEN_Ca_jct])*(exp((((2.*nr->V)*F)/(R*T)))))-((gamma_Cao*Cao))))/((exp((((2.*nr->V)*F)/(R*T))))-(1.)));
  GlobalData_t INaCa_SL = (((((Fx_NCX_SL*V_max)*Ka_SL)*Q_NCX)*temp_SL)/((((((K_mCai*(pow(Nao,HNa)))*(1.+(pow((sv->Na_SL/K_mNai),HNa))))+(((pow(K_mNao,HNa))*Rosenbrock_X[ROSEN_Ca_SL])*(1.+(Rosenbrock_X[ROSEN_Ca_SL]/K_mCai))))+(K_mCao*(pow(sv->Na_SL,HNa))))+((pow(sv->Na_SL,HNa))*Cao))+((pow(Nao,HNa))*Rosenbrock_X[ROSEN_Ca_SL])));
  GlobalData_t INaCa_jct = (((((Fx_NCX_jct*V_max)*Ka_jct)*Q_NCX)*temp_jct)/((((((K_mCai*(pow(Nao,HNa)))*(1.+(pow((sv->Na_jct/K_mNai),HNa))))+(((pow(K_mNao,HNa))*Rosenbrock_X[ROSEN_Ca_jct])*(1.+(Rosenbrock_X[ROSEN_Ca_jct]/K_mCai))))+(K_mCao*(pow(sv->Na_jct,HNa))))+((pow(sv->Na_jct,HNa))*Cao))+((pow(Nao,HNa))*Rosenbrock_X[ROSEN_Ca_jct])));
  GlobalData_t IbCa_SL = ((GCaBk*Fx_CaBk_SL)*(nr->V-(E_Ca_SL)));
  GlobalData_t IbCa_jct = ((GCaBk*Fx_CaBk_jct)*(nr->V-(E_Ca_jct)));
  GlobalData_t dCa_SL_tot_bound = (dCa_SLB_SL+dCa_SLHigh_SL);
  GlobalData_t dCa_cytosol_tot_bound = ((((((dCa_TroponinC+dCa_TroponinC_Ca_Mg)+dMGTroponinC_Ca_Mg)+dCa_Calmodulin)+dCa_Myosin)+dMGMyosin)+dCa_SRB);
  GlobalData_t dCa_jct_tot_bound = (dCa_SLB_jct+dCa_SLHigh_jct);
  GlobalData_t diff_Ca_SR = ((j_pump_SR-((((j_leak_SR*Volmyo)/VolSR)+j_rel_SR)))-(dCalsequestrin));
  GlobalData_t diff_Ca_SRB = dCa_SRB;
  GlobalData_t diff_Cai = (((((-j_pump_SR)*VolSR)/Volmyo)+(J_Ca_SL_myo/Volmyo))-(dCa_cytosol_tot_bound));
  GlobalData_t i_Ca_SL_tot = (((ICaL_Ca_SL-((2.*INaCa_SL)))+IbCa_SL)+IpCa_SL);
  GlobalData_t i_Ca_jct_tot = (((ICaL_Ca_jct-((2.*INaCa_jct)))+IbCa_jct)+IpCa_jct);
  GlobalData_t diff_Ca_SL = (((((-i_Ca_SL_tot)*Cm)/((VolSL*2.)*F))+((J_Ca_jct_SL-(J_Ca_SL_myo))/VolSL))-(dCa_SL_tot_bound));
  GlobalData_t diff_Ca_jct = (((((((-i_Ca_jct_tot)*Cm)/((Voljct*2.)*F))-((J_Ca_jct_SL/Voljct)))+((j_rel_SR*VolSR)/Voljct))+((j_leak_SR*Volmyo)/Voljct))-(dCa_jct_tot_bound));
  Rosenbrock_DX[ROSEN_Ca_SL] = diff_Ca_SL;
  Rosenbrock_DX[ROSEN_Ca_SR] = diff_Ca_SR;
  Rosenbrock_DX[ROSEN_Ca_SRB] = diff_Ca_SRB;
  Rosenbrock_DX[ROSEN_Ca_jct] = diff_Ca_jct;
  Rosenbrock_DX[ROSEN_Cai] = diff_Cai;

}

void Shannon_rosenbrock_jacobian(float** Rosenbrock_jacobian, float* Rosenbrock_X, void* user_data, int dddd) {
  Shannon_Private* ion_private = (Shannon_Private*)user_data;
  Shannon_Regional_Constants* rc = &ion_private->rc;
  ION_IF* IF = ion_private->IF;
  int __i = ((Shannon_Private*)user_data)->node_number;
  Shannon_Nodal_Req* nr = &ion_private->nr;
  cell_geom *region = &IF->cgeom;
  Shannon_Params *p = (Shannon_Params *)IF->params;
  Shannon_state *sv_base = (Shannon_state *)IF->sv_tab.y;
  Shannon_state *sv = sv_base+__i;
  GlobalData_t Ka_SL = (1./(1.+(((Kd_act/Rosenbrock_X[ROSEN_Ca_SL])*(Kd_act/Rosenbrock_X[ROSEN_Ca_SL]))*(Kd_act/Rosenbrock_X[ROSEN_Ca_SL]))));
  GlobalData_t Ka_jct = (1./(1.+(((Kd_act/Rosenbrock_X[ROSEN_Ca_jct])*(Kd_act/Rosenbrock_X[ROSEN_Ca_jct]))*(Kd_act/Rosenbrock_X[ROSEN_Ca_jct]))));
  GlobalData_t fCa_SL = (1.-(sv->fCaB_SL));
  GlobalData_t fCa_jct = (1.-(sv->fCaB_jct));
  GlobalData_t partial_diff_Ca_SRB_del_Ca_SRB = (((kon_SRB*Rosenbrock_X[ROSEN_Cai])*-1.)-(koff_SRB));
  GlobalData_t partial_diff_Ca_SRB_del_Cai = (kon_SRB*(Bmax_SRB-(Rosenbrock_X[ROSEN_Ca_SRB])));
  GlobalData_t partial_diff_Ca_SR_del_Ca_SR = (((((((1.+(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0)))+(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))*((Q_SRCaP*V_max_cp0)*(-(H_cp0*((Kmr/(Kmr*Kmr))*(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),(H_cp0-(1.)))))))))-((((Q_SRCaP*V_max_cp0)*((pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0))-((pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))))*(H_cp0*((Kmr/(Kmr*Kmr))*(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),(H_cp0-(1.)))))))))/(((1.+(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0)))+(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))*((1.+(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0)))+(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))))-((((VolSR*(KSRleak*Volmyo))/(VolSR*VolSR))+(ks*sv->O))))-((kon_Calsequestrin*(((Bmax_Calsequestrin*Volmyo)/VolSR)-(sv->Ca_Calsequestrin)))));
  GlobalData_t partial_diff_Ca_SR_del_Ca_jct = (-(((VolSR*((KSRleak*-1.)*Volmyo))/(VolSR*VolSR))+((ks*sv->O)*-1.)));
  GlobalData_t partial_diff_Ca_SR_del_Cai = (((((1.+(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0)))+(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))*((Q_SRCaP*V_max_cp0)*(H_cp0*((Kmf/(Kmf*Kmf))*(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),(H_cp0-(1.))))))))-((((Q_SRCaP*V_max_cp0)*((pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0))-((pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))))*(H_cp0*((Kmf/(Kmf*Kmf))*(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),(H_cp0-(1.)))))))))/(((1.+(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0)))+(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))*((1.+(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0)))+(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))));
  GlobalData_t partial_diff_Ca_jct_del_Ca_SR = (((Voljct*((ks*sv->O)*VolSR))/(Voljct*Voljct))+((Voljct*(KSRleak*Volmyo))/(Voljct*Voljct)));
  GlobalData_t partial_diff_Cai_del_Ca_SR = ((Volmyo*((-(((((1.+(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0)))+(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))*((Q_SRCaP*V_max_cp0)*(-(H_cp0*((Kmr/(Kmr*Kmr))*(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),(H_cp0-(1.)))))))))-((((Q_SRCaP*V_max_cp0)*((pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0))-((pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))))*(H_cp0*((Kmr/(Kmr*Kmr))*(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),(H_cp0-(1.)))))))))/(((1.+(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0)))+(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))*((1.+(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0)))+(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0))))))*VolSR))/(Volmyo*Volmyo));
  GlobalData_t partial_diff_Cai_del_Ca_SRB = (-(((kon_SRB*Rosenbrock_X[ROSEN_Cai])*-1.)-(koff_SRB)));
  GlobalData_t partial_diff_Cai_del_Cai = ((((Volmyo*((-(((((1.+(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0)))+(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))*((Q_SRCaP*V_max_cp0)*(H_cp0*((Kmf/(Kmf*Kmf))*(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),(H_cp0-(1.))))))))-((((Q_SRCaP*V_max_cp0)*((pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0))-((pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))))*(H_cp0*((Kmf/(Kmf*Kmf))*(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),(H_cp0-(1.)))))))))/(((1.+(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0)))+(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0)))*((1.+(pow((Rosenbrock_X[ROSEN_Cai]/Kmf),H_cp0)))+(pow((Rosenbrock_X[ROSEN_Ca_SR]/Kmr),H_cp0))))))*VolSR))/(Volmyo*Volmyo))+((Volmyo*(-1.*3.7243e-12))/(Volmyo*Volmyo)))-((((((kon_TroponinC*(Bmax_TroponinC-(sv->Ca_TroponinC)))+(kon_TroponinC_Ca_MGCa*(Bmax_TroponinC_Ca_MGCa-((sv->Ca_TroponinC_Ca_Mg+sv->MGTroponinC_Ca_Mg)))))+(kon_Calmodulin*(Bmax_Calmodulin-(sv->Ca_Calmodulin))))+(kon_Myosin_Ca*(Bmax_Myosin_Ca-((sv->Ca_Myosin+sv->MGMyosin)))))+(kon_SRB*(Bmax_SRB-(Rosenbrock_X[ROSEN_Ca_SRB]))))));
  GlobalData_t temp = ((((((0.45*sv->d)*sv->f)*Q_CaL)*nr->V)*(F*F))/(R*T));
  GlobalData_t temp_SL = (((((exp((((eta*nr->V)*F)/(R*T))))*(pow(sv->Na_SL,HNa)))*Cao)-((((exp(((((eta-(1.))*nr->V)*F)/(R*T))))*(pow(Nao,HNa)))*Rosenbrock_X[ROSEN_Ca_SL])))/(1.+(ksat*(exp(((((eta-(1.))*nr->V)*F)/(R*T)))))));
  GlobalData_t temp_jct = (((((exp((((eta*nr->V)*F)/(R*T))))*(pow(sv->Na_jct,HNa)))*Cao)-((((exp(((((eta-(1.))*nr->V)*F)/(R*T))))*(pow(Nao,HNa)))*Rosenbrock_X[ROSEN_Ca_jct])))/(1.+(ksat*(exp(((((eta-(1.))*nr->V)*F)/(R*T)))))));
  GlobalData_t partial_diff_Ca_SL_del_Ca_SL = ((((((VolSL*2.)*F)*((-(((((((exp((((2.*nr->V)*F)/(R*T))))-(1.))*(((((temp*fCa_SL)*Fx_ICaL_SL)*PCa)*4.)*(gamma_Cai*(exp((((2.*nr->V)*F)/(R*T)))))))/(((exp((((2.*nr->V)*F)/(R*T))))-(1.))*((exp((((2.*nr->V)*F)/(R*T))))-(1.))))-((2.*(((((((((K_mCai*(pow(Nao,HNa)))*(1.+(pow((sv->Na_SL/K_mNai),HNa))))+(((pow(K_mNao,HNa))*Rosenbrock_X[ROSEN_Ca_SL])*(1.+(Rosenbrock_X[ROSEN_Ca_SL]/K_mCai))))+(K_mCao*(pow(sv->Na_SL,HNa))))+((pow(sv->Na_SL,HNa))*Cao))+((pow(Nao,HNa))*Rosenbrock_X[ROSEN_Ca_SL]))*(((((Fx_NCX_SL*V_max)*((-((((((-Kd_act)/(Rosenbrock_X[ROSEN_Ca_SL]*Rosenbrock_X[ROSEN_Ca_SL]))*(Kd_act/Rosenbrock_X[ROSEN_Ca_SL]))+((Kd_act/Rosenbrock_X[ROSEN_Ca_SL])*((-Kd_act)/(Rosenbrock_X[ROSEN_Ca_SL]*Rosenbrock_X[ROSEN_Ca_SL]))))*(Kd_act/Rosenbrock_X[ROSEN_Ca_SL]))+(((Kd_act/Rosenbrock_X[ROSEN_Ca_SL])*(Kd_act/Rosenbrock_X[ROSEN_Ca_SL]))*((-Kd_act)/(Rosenbrock_X[ROSEN_Ca_SL]*Rosenbrock_X[ROSEN_Ca_SL])))))/((1.+(((Kd_act/Rosenbrock_X[ROSEN_Ca_SL])*(Kd_act/Rosenbrock_X[ROSEN_Ca_SL]))*(Kd_act/Rosenbrock_X[ROSEN_Ca_SL])))*(1.+(((Kd_act/Rosenbrock_X[ROSEN_Ca_SL])*(Kd_act/Rosenbrock_X[ROSEN_Ca_SL]))*(Kd_act/Rosenbrock_X[ROSEN_Ca_SL]))))))*Q_NCX)*temp_SL)+((((Fx_NCX_SL*V_max)*Ka_SL)*Q_NCX)*(((1.+(ksat*(exp(((((eta-(1.))*nr->V)*F)/(R*T))))))*(-((exp(((((eta-(1.))*nr->V)*F)/(R*T))))*(pow(Nao,HNa)))))/((1.+(ksat*(exp(((((eta-(1.))*nr->V)*F)/(R*T))))))*(1.+(ksat*(exp(((((eta-(1.))*nr->V)*F)/(R*T)))))))))))-((((((Fx_NCX_SL*V_max)*Ka_SL)*Q_NCX)*temp_SL)*((((pow(K_mNao,HNa))*(1.+(Rosenbrock_X[ROSEN_Ca_SL]/K_mCai)))+(((pow(K_mNao,HNa))*Rosenbrock_X[ROSEN_Ca_SL])*(K_mCai/(K_mCai*K_mCai))))+(pow(Nao,HNa))))))/(((((((K_mCai*(pow(Nao,HNa)))*(1.+(pow((sv->Na_SL/K_mNai),HNa))))+(((pow(K_mNao,HNa))*Rosenbrock_X[ROSEN_Ca_SL])*(1.+(Rosenbrock_X[ROSEN_Ca_SL]/K_mCai))))+(K_mCao*(pow(sv->Na_SL,HNa))))+((pow(sv->Na_SL,HNa))*Cao))+((pow(Nao,HNa))*Rosenbrock_X[ROSEN_Ca_SL]))*((((((K_mCai*(pow(Nao,HNa)))*(1.+(pow((sv->Na_SL/K_mNai),HNa))))+(((pow(K_mNao,HNa))*Rosenbrock_X[ROSEN_Ca_SL])*(1.+(Rosenbrock_X[ROSEN_Ca_SL]/K_mCai))))+(K_mCao*(pow(sv->Na_SL,HNa))))+((pow(sv->Na_SL,HNa))*Cao))+((pow(Nao,HNa))*Rosenbrock_X[ROSEN_Ca_SL])))))))+((GCaBk*Fx_CaBk_SL)*(-(((R*T)/(2.*F))*(((-Cao)/(Rosenbrock_X[ROSEN_Ca_SL]*Rosenbrock_X[ROSEN_Ca_SL]))/(Cao/Rosenbrock_X[ROSEN_Ca_SL]))))))+((-(((Q_SLCaP*V_maxAF)*Fx_SLCaP_SL)*(H*(((-Km)/(Rosenbrock_X[ROSEN_Ca_SL]*Rosenbrock_X[ROSEN_Ca_SL]))*(pow((Km/Rosenbrock_X[ROSEN_Ca_SL]),(H-(1.))))))))/((1.+(pow((Km/Rosenbrock_X[ROSEN_Ca_SL]),H)))*(1.+(pow((Km/Rosenbrock_X[ROSEN_Ca_SL]),H)))))))*Cm))/(((VolSL*2.)*F)*((VolSL*2.)*F)))+((VolSL*((-1.*8.2413e-13)-(3.7243e-12)))/(VolSL*VolSL)))-(((kon_SL*(((Bmax_SLB_SL*Volmyo)/VolSL)-(sv->Ca_SLB_SL)))+(kon_SL*(((Bmax_SLHigh_SL*Volmyo)/VolSL)-(sv->Ca_SLHigh_SL))))));
  GlobalData_t partial_diff_Ca_jct_del_Ca_jct = ((((((((Voljct*2.)*F)*((-(((((((exp((((2.*nr->V)*F)/(R*T))))-(1.))*(((((temp*fCa_jct)*Fx_ICaL_jct)*PCa)*4.)*(gamma_Cai*(exp((((2.*nr->V)*F)/(R*T)))))))/(((exp((((2.*nr->V)*F)/(R*T))))-(1.))*((exp((((2.*nr->V)*F)/(R*T))))-(1.))))-((2.*(((((((((K_mCai*(pow(Nao,HNa)))*(1.+(pow((sv->Na_jct/K_mNai),HNa))))+(((pow(K_mNao,HNa))*Rosenbrock_X[ROSEN_Ca_jct])*(1.+(Rosenbrock_X[ROSEN_Ca_jct]/K_mCai))))+(K_mCao*(pow(sv->Na_jct,HNa))))+((pow(sv->Na_jct,HNa))*Cao))+((pow(Nao,HNa))*Rosenbrock_X[ROSEN_Ca_jct]))*(((((Fx_NCX_jct*V_max)*((-((((((-Kd_act)/(Rosenbrock_X[ROSEN_Ca_jct]*Rosenbrock_X[ROSEN_Ca_jct]))*(Kd_act/Rosenbrock_X[ROSEN_Ca_jct]))+((Kd_act/Rosenbrock_X[ROSEN_Ca_jct])*((-Kd_act)/(Rosenbrock_X[ROSEN_Ca_jct]*Rosenbrock_X[ROSEN_Ca_jct]))))*(Kd_act/Rosenbrock_X[ROSEN_Ca_jct]))+(((Kd_act/Rosenbrock_X[ROSEN_Ca_jct])*(Kd_act/Rosenbrock_X[ROSEN_Ca_jct]))*((-Kd_act)/(Rosenbrock_X[ROSEN_Ca_jct]*Rosenbrock_X[ROSEN_Ca_jct])))))/((1.+(((Kd_act/Rosenbrock_X[ROSEN_Ca_jct])*(Kd_act/Rosenbrock_X[ROSEN_Ca_jct]))*(Kd_act/Rosenbrock_X[ROSEN_Ca_jct])))*(1.+(((Kd_act/Rosenbrock_X[ROSEN_Ca_jct])*(Kd_act/Rosenbrock_X[ROSEN_Ca_jct]))*(Kd_act/Rosenbrock_X[ROSEN_Ca_jct]))))))*Q_NCX)*temp_jct)+((((Fx_NCX_jct*V_max)*Ka_jct)*Q_NCX)*(((1.+(ksat*(exp(((((eta-(1.))*nr->V)*F)/(R*T))))))*(-((exp(((((eta-(1.))*nr->V)*F)/(R*T))))*(pow(Nao,HNa)))))/((1.+(ksat*(exp(((((eta-(1.))*nr->V)*F)/(R*T))))))*(1.+(ksat*(exp(((((eta-(1.))*nr->V)*F)/(R*T)))))))))))-((((((Fx_NCX_jct*V_max)*Ka_jct)*Q_NCX)*temp_jct)*((((pow(K_mNao,HNa))*(1.+(Rosenbrock_X[ROSEN_Ca_jct]/K_mCai)))+(((pow(K_mNao,HNa))*Rosenbrock_X[ROSEN_Ca_jct])*(K_mCai/(K_mCai*K_mCai))))+(pow(Nao,HNa))))))/(((((((K_mCai*(pow(Nao,HNa)))*(1.+(pow((sv->Na_jct/K_mNai),HNa))))+(((pow(K_mNao,HNa))*Rosenbrock_X[ROSEN_Ca_jct])*(1.+(Rosenbrock_X[ROSEN_Ca_jct]/K_mCai))))+(K_mCao*(pow(sv->Na_jct,HNa))))+((pow(sv->Na_jct,HNa))*Cao))+((pow(Nao,HNa))*Rosenbrock_X[ROSEN_Ca_jct]))*((((((K_mCai*(pow(Nao,HNa)))*(1.+(pow((sv->Na_jct/K_mNai),HNa))))+(((pow(K_mNao,HNa))*Rosenbrock_X[ROSEN_Ca_jct])*(1.+(Rosenbrock_X[ROSEN_Ca_jct]/K_mCai))))+(K_mCao*(pow(sv->Na_jct,HNa))))+((pow(sv->Na_jct,HNa))*Cao))+((pow(Nao,HNa))*Rosenbrock_X[ROSEN_Ca_jct])))))))+((GCaBk*Fx_CaBk_jct)*(-(((R*T)/(2.*F))*(((-Cao)/(Rosenbrock_X[ROSEN_Ca_jct]*Rosenbrock_X[ROSEN_Ca_jct]))/(Cao/Rosenbrock_X[ROSEN_Ca_jct]))))))+((-(((Q_SLCaP*V_maxAF)*Fx_SLCaP_jct)*(H*(((-Km)/(Rosenbrock_X[ROSEN_Ca_jct]*Rosenbrock_X[ROSEN_Ca_jct]))*(pow((Km/Rosenbrock_X[ROSEN_Ca_jct]),(H-(1.))))))))/((1.+(pow((Km/Rosenbrock_X[ROSEN_Ca_jct]),H)))*(1.+(pow((Km/Rosenbrock_X[ROSEN_Ca_jct]),H)))))))*Cm))/(((Voljct*2.)*F)*((Voljct*2.)*F)))-(((Voljct*8.2413e-13)/(Voljct*Voljct))))+((Voljct*(((ks*sv->O)*-1.)*VolSR))/(Voljct*Voljct)))+((Voljct*((KSRleak*-1.)*Volmyo))/(Voljct*Voljct)))-(((kon_SL*((((Bmax_SLB_jct*0.1)*Volmyo)/Voljct)-(sv->Ca_SLB_jct)))+(kon_SL*((((Bmax_SLHigh_jct*0.1)*Volmyo)/Voljct)-(sv->Ca_SLHigh_jct))))));
  Rosenbrock_jacobian[ROSEN_Ca_SL][ROSEN_Ca_SL] = partial_diff_Ca_SL_del_Ca_SL;
  Rosenbrock_jacobian[ROSEN_Ca_SL][ROSEN_Ca_SR] = partial_diff_Ca_SL_del_Ca_SR;
  Rosenbrock_jacobian[ROSEN_Ca_SL][ROSEN_Ca_SRB] = partial_diff_Ca_SL_del_Ca_SRB;
  Rosenbrock_jacobian[ROSEN_Ca_SL][ROSEN_Ca_jct] = partial_diff_Ca_SL_del_Ca_jct;
  Rosenbrock_jacobian[ROSEN_Ca_SL][ROSEN_Cai] = partial_diff_Ca_SL_del_Cai;
  Rosenbrock_jacobian[ROSEN_Ca_SR][ROSEN_Ca_SL] = partial_diff_Ca_SR_del_Ca_SL;
  Rosenbrock_jacobian[ROSEN_Ca_SR][ROSEN_Ca_SR] = partial_diff_Ca_SR_del_Ca_SR;
  Rosenbrock_jacobian[ROSEN_Ca_SR][ROSEN_Ca_SRB] = partial_diff_Ca_SR_del_Ca_SRB;
  Rosenbrock_jacobian[ROSEN_Ca_SR][ROSEN_Ca_jct] = partial_diff_Ca_SR_del_Ca_jct;
  Rosenbrock_jacobian[ROSEN_Ca_SR][ROSEN_Cai] = partial_diff_Ca_SR_del_Cai;
  Rosenbrock_jacobian[ROSEN_Ca_SRB][ROSEN_Ca_SL] = partial_diff_Ca_SRB_del_Ca_SL;
  Rosenbrock_jacobian[ROSEN_Ca_SRB][ROSEN_Ca_SR] = partial_diff_Ca_SRB_del_Ca_SR;
  Rosenbrock_jacobian[ROSEN_Ca_SRB][ROSEN_Ca_SRB] = partial_diff_Ca_SRB_del_Ca_SRB;
  Rosenbrock_jacobian[ROSEN_Ca_SRB][ROSEN_Ca_jct] = partial_diff_Ca_SRB_del_Ca_jct;
  Rosenbrock_jacobian[ROSEN_Ca_SRB][ROSEN_Cai] = partial_diff_Ca_SRB_del_Cai;
  Rosenbrock_jacobian[ROSEN_Ca_jct][ROSEN_Ca_SL] = partial_diff_Ca_jct_del_Ca_SL;
  Rosenbrock_jacobian[ROSEN_Ca_jct][ROSEN_Ca_SR] = partial_diff_Ca_jct_del_Ca_SR;
  Rosenbrock_jacobian[ROSEN_Ca_jct][ROSEN_Ca_SRB] = partial_diff_Ca_jct_del_Ca_SRB;
  Rosenbrock_jacobian[ROSEN_Ca_jct][ROSEN_Ca_jct] = partial_diff_Ca_jct_del_Ca_jct;
  Rosenbrock_jacobian[ROSEN_Ca_jct][ROSEN_Cai] = partial_diff_Ca_jct_del_Cai;
  Rosenbrock_jacobian[ROSEN_Cai][ROSEN_Ca_SL] = partial_diff_Cai_del_Ca_SL;
  Rosenbrock_jacobian[ROSEN_Cai][ROSEN_Ca_SR] = partial_diff_Cai_del_Ca_SR;
  Rosenbrock_jacobian[ROSEN_Cai][ROSEN_Ca_SRB] = partial_diff_Cai_del_Ca_SRB;
  Rosenbrock_jacobian[ROSEN_Cai][ROSEN_Ca_jct] = partial_diff_Cai_del_Ca_jct;
  Rosenbrock_jacobian[ROSEN_Cai][ROSEN_Cai] = partial_diff_Cai_del_Cai;

}


void trace_Shannon(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Shannon_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->Ca_SR\n"
        "sv->Ca_SRB\n"
        "sv->Ca_jct\n"
        "sv->Cai\n"
        "V\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Shannon_Params *p  = (Shannon_Params *)IF->params;

  Shannon_state *sv_base = (Shannon_state *)IF->sv_tab.y;
  Shannon_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e-3;
  
  
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->Ca_SR);
  fprintf(file, "%4.12f\t", sv->Ca_SRB);
  fprintf(file, "%4.12f\t", sv->Ca_jct);
  fprintf(file, "%4.12f\t", sv->Cai);
  fprintf(file, "%4.12f\t", V);
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e3;
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        