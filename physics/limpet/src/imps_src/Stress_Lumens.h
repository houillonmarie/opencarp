// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Joost Lumens, Tammo Delhaas, Borut Kirn, Theo Arts
*  Year: 2009
*  Title: Three-wall segment (TriSeg) model describing mechanics and hemodynamics of ventricular interaction
*  Journal: Ann Biomed Eng. 2009;37:2234-55
*  DOI: 10.1007/s10439-009-9774-2
*  Comment: Plugin
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __STRESS_LUMENS_H__
#define __STRESS_LUMENS_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Stress_Lumens_REQDAT Vm_DATA_FLAG|Lambda_DATA_FLAG
#define Stress_Lumens_MODDAT Tension_DATA_FLAG|delLambda_DATA_FLAG

struct Stress_Lumens_Params {
    GlobalData_t VmThresh;

};

struct Stress_Lumens_state {
    GlobalData_t C;
    GlobalData_t Lsc;
    GlobalData_t Vmp;
    GlobalData_t tact;
    GlobalData_t tcur;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Stress_Lumens(ION_IF *);
void construct_tables_Stress_Lumens(ION_IF *);
void destroy_Stress_Lumens(ION_IF *);
void initialize_sv_Stress_Lumens(ION_IF *, GlobalData_t**);
void compute_Stress_Lumens(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
