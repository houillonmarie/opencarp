// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Bondarenko VE, Szigeti GP, Bett GC, Kim SJ, Rasmusson RL
*  Year: 2004
*  Title: Computer model of action potential of mouse ventricular myocytes
*  Journal: Am J Physiol Heart Circ Physiol. 287:H1378-403
*  DOI: 10.1152/ajpheart.00185.2003
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __INA_BONDARENKO_H__
#define __INA_BONDARENKO_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define INa_Bondarenko_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG
#define INa_Bondarenko_MODDAT Iion_DATA_FLAG

struct INa_Bondarenko_Params {
    GlobalData_t GNa;
    GlobalData_t Ki;
    GlobalData_t Ko;
    GlobalData_t Nai;
    GlobalData_t Nao;

};

struct INa_Bondarenko_state {
    GlobalData_t C_Na1;
    GlobalData_t C_Na2;
    GlobalData_t I1_Na;
    GlobalData_t I2_Na;
    GlobalData_t IC_Na2;
    GlobalData_t IC_Na3;
    GlobalData_t IF_Na;
    GlobalData_t O_Na;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_INa_Bondarenko(ION_IF *);
void construct_tables_INa_Bondarenko(ION_IF *);
void destroy_INa_Bondarenko(ION_IF *);
void initialize_sv_INa_Bondarenko(ION_IF *, GlobalData_t**);
void compute_INa_Bondarenko(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
