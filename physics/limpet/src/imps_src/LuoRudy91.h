// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Luo CH, Rudy Y
*  Year: 1991
*  Title: A model of the ventricular cardiac action potential. Depolarization, repolarization, and their interaction
*  Journal: Circulation Research,68(6),1501-1526
*  DOI: 10.1161/01.RES.68.6.1501
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __LUORUDY91_H__
#define __LUORUDY91_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define LuoRudy91_REQDAT Vm_DATA_FLAG
#define LuoRudy91_MODDAT Iion_DATA_FLAG

struct LuoRudy91_Params {
    GlobalData_t Cai_init;
    GlobalData_t GK1bar;
    GlobalData_t GKbar;
    GlobalData_t GKp;
    GlobalData_t GNa;
    GlobalData_t Gb;
    GlobalData_t Gsi;
    GlobalData_t Ki;
    GlobalData_t Ko;
    GlobalData_t Nai;
    GlobalData_t Nao;
    GlobalData_t tau_d_factor;
    GlobalData_t tau_f_factor;

};

struct LuoRudy91_state {
    GlobalData_t Cai;
    Gatetype X;
    Gatetype d;
    Gatetype f;
    Gatetype h;
    Gatetype j;
    Gatetype m;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_LuoRudy91(ION_IF *);
void construct_tables_LuoRudy91(ION_IF *);
void destroy_LuoRudy91(ION_IF *);
void initialize_sv_LuoRudy91(ION_IF *, GlobalData_t**);
void compute_LuoRudy91(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
