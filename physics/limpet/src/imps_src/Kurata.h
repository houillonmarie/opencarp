// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Title: Dynamical description of sinoatrial node pacemaking: improved mathematical model for primary pacemaker cell
*  Authors: Yasutaka Kurata, Ichiro Hisatome, Sunao Imanishi, and Toshishige Shibamoto
*  Year: 2002
*  Journal: American Journal of Physiology-Heart and Circulatory Physiology 2002 283:5, H2074-H2101
*  DOI: 10.1152/ajpheart.00900.2001
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __KURATA_H__
#define __KURATA_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Kurata_REQDAT Vm_DATA_FLAG
#define Kurata_MODDAT Iion_DATA_FLAG

struct Kurata_Params {
    GlobalData_t ACh;
    GlobalData_t B_max;
    GlobalData_t Cm;
    GlobalData_t GCaL;
    GlobalData_t GKACh_max;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t Gh;
    GlobalData_t GhK;
    GlobalData_t GhNa;
    GlobalData_t Gst;
    GlobalData_t Gsus;
    GlobalData_t Gto;

};

struct Kurata_state {
    GlobalData_t CaimM;
    GlobalData_t Carel;
    GlobalData_t Casub;
    GlobalData_t Caup;
    GlobalData_t Ki;
    GlobalData_t Nai;
    Gatetype d_L;
    Gatetype d_T;
    GlobalData_t f_CMi;
    GlobalData_t f_CMs;
    GlobalData_t f_CQ;
    Gatetype f_Ca;
    Gatetype f_L;
    Gatetype f_T;
    GlobalData_t f_TC;
    GlobalData_t f_TMC;
    GlobalData_t f_TMM;
    Gatetype hf;
    Gatetype hs;
    Gatetype m;
    Gatetype n;
    Gatetype paF;
    Gatetype paS;
    Gatetype pi;
    Gatetype q;
    Gatetype q_a;
    Gatetype q_i;
    Gatetype r;
    Gatetype y;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Kurata(ION_IF *);
void construct_tables_Kurata(ION_IF *);
void destroy_Kurata(ION_IF *);
void initialize_sv_Kurata(ION_IF *, GlobalData_t**);
void compute_Kurata(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
