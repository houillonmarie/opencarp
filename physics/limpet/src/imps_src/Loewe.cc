// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Axel Loewe, Yannick Lutz, Alan Fabbri, Stefano Severi
*  Year: 2019
*  Title: Hypocalcemia-Induced Slowing of Human Sinus Node Pacemaking
*  Journal: Biophysical Journal, 117(12):2244-2254
*  DOI: 10.1016/j.bpj.2019.07.037
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Loewe.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Loewe(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Loewe( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define Ca_jsr_init (GlobalData_t)(0.5869378)
#define Ca_nsr_init (GlobalData_t)(0.669119)
#define Ca_sub_init (GlobalData_t)(8.59587700000000057e-05)
#define Cai_init (GlobalData_t)(1.26704600000000013e-04)
#define I_init (GlobalData_t)(1.88065699999999987e-09)
#define Ki_init (GlobalData_t)(139.1382)
#define Nai_init (GlobalData_t)(6.084085)
#define O_init (GlobalData_t)(1.75000900000000001e-08)
#define RI_init (GlobalData_t)(9.67726599999999965e-02)
#define R_1_init (GlobalData_t)(0.9004965)
#define V_init (GlobalData_t)(-4.61898999999999990e-02)
#define a_init (GlobalData_t)(2.81845900000000016e-03)
#define dL_init (GlobalData_t)(1.40229900000000008e-03)
#define dT_init (GlobalData_t)(0.192108)
#define fCMi_init (GlobalData_t)(0.2776014)
#define fCMs_init (GlobalData_t)(0.206602)
#define fCQ_init (GlobalData_t)(0.1878388)
#define fCa_init (GlobalData_t)(0.7820265)
#define fL_init (GlobalData_t)(0.9951828)
#define fTC_init (GlobalData_t)(0.0246504)
#define fTMC_init (GlobalData_t)(0.329845)
#define fTMM_init (GlobalData_t)(0.5920168)
#define fT_init (GlobalData_t)(3.74674099999999996e-02)
#define h_init (GlobalData_t)(5.20200199999999984e-03)
#define m_init (GlobalData_t)(0.3777047)
#define n_init (GlobalData_t)(0.1057902)
#define paF_init (GlobalData_t)(8.34449800000000054e-03)
#define paS_init (GlobalData_t)(0.3450797)
#define piy_init (GlobalData_t)(0.7369347)
#define q_init (GlobalData_t)(0.4735559)
#define r_Kur_init (GlobalData_t)(9.05902400000000059e-03)
#define r_init (GlobalData_t)(1.24570600000000007e-02)
#define s_Kur_init (GlobalData_t)(0.8655135)
#define x_init (GlobalData_t)(6.77693899999999988e-02)
#define y_init (GlobalData_t)(0.0103301)



void initialize_params_Loewe( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Loewe_Params *p = (Loewe_Params *)IF->params;

  // Compute the regional constants
  {
    p->ACh = 0.0;
    p->ACh_on = 0.0;
    p->C = 57.;
    p->CM_tot = 0.045;
    p->CQ_tot = 10.0;
    p->Ca_intracellular_fluxes_tau_dif_Ca = 5.469e-05;
    p->Ca_intracellular_fluxes_tau_tr = 0.04;
    p->Cao = 1.8;
    p->EC50_SK = 0.7;
    p->EC50_SR = 0.45;
    p->F = 9.64853414999999950e+04;
    p->GKACh = 0.00345;
    p->GKs_ = 8.63714000000000018e-04;
    p->GKur = 7.062e-05;
    p->GNa = 0.0223;
    p->GNa_L = 0.0;
    p->GSK = 0.000165;
    p->Gf_K = 0.0117;
    p->Gf_Na = 0.00696;
    p->Gto = 1.67347999999999998e-03;
    p->HSR = 2.5;
    p->ICaL_fCa_gate_alpha_fCa = 0.0075;
    p->INaK_max = 0.137171;
    p->Iso_1_uM = 0.0;
    p->K1ni = 395.3;
    p->K1no = 1628.0;
    p->K2ni = 2.289;
    p->K2no = 561.4;
    p->K3ni = 26.44;
    p->K3no = 4.663;
    p->K_NaCa = 3.343;
    p->K_up = 2.86113000000000003e-04;
    p->Kci = 0.0207;
    p->Kcni = 26.44;
    p->Kco = 3.663;
    p->Km_Kp = 1.4;
    p->Km_Nap = 14.0;
    p->Km_fCa = 0.000338;
    p->Ko = 5.4;
    p->L_cell = 67.0;
    p->L_sub = 0.02;
    p->MaxSR = 15.0;
    p->Mgi = 2.5;
    p->MinSR = 1.0;
    p->Nao = 140.0;
    p->P_CaL = 0.5046812;
    p->P_CaT = 0.04132;
    p->P_up_basal = 5.0;
    p->Qci = 0.1369;
    p->Qco = 0.0;
    p->Qn = 0.4315;
    p->R_2 = 8314.472;
    p->R_cell = 3.9;
    p->T = 310.0;
    p->TC_tot = 0.031;
    p->TMC_tot = 0.062;
    p->V_dL = -7.7713;
    p->V_i_part = 0.46;
    p->V_jsr_part = 0.0012;
    p->V_nsr_part = 0.0116;
    p->delta_m = 1e-05;
    p->k_dL = 5.854;
    p->k_fL = 5.1737;
    p->kb_CM = 542.0;
    p->kb_CQ = 445.0;
    p->kb_TC = 446.0;
    p->kb_TMC = 7.51;
    p->kb_TMM = 751.0;
    p->kf_CM = 1641986.0;
    p->kf_CQ = 175.4;
    p->kf_TC = 88800.0;
    p->kf_TMC = 227700.0;
    p->kf_TMM = 2277.0;
    p->kiCa = 500.0;
    p->kim = 5.0;
    p->koCa = 10000.0;
    p->kom = 660.0;
    p->ks = 1.48041085099999994e+08;
    p->n_SK = 2.2;
    p->offset_fT = 0.0;
    p->shift_fL = 0.0;
    p->slope_up = 5e-05;
    p->y_shift = 0.0;
    p->ACh_block = ((0.31*p->ACh)/(p->ACh+9e-05));
    p->ACh_shift = ((p->ACh>0.0) ? (-1.0-(((9.898*(pow(p->ACh,0.618)))/((pow(p->ACh,0.618))+1.22422999999999998e-03)))) : 0.0);
    p->Ca_intracellular_fluxes_b_up = ((p->Iso_1_uM>0.0) ? -0.25 : ((p->ACh>0.0) ? ((0.7*p->ACh)/(9e-05+p->ACh)) : 0.0));
    p->GKr = (4.98909900000000031e-03*(sqrt((p->Ko/5.4))));
    p->GKs = ((p->Iso_1_uM>0.0) ? (1.2*p->GKs_) : p->GKs_);
    p->IKACh_a_gate_alpha_a = (((3.5988-(0.025641))/(1.0+(1.21550000000000005e-06/(pow(p->ACh,1.6951)))))+0.025641);
    p->Iso_increase_1 = ((p->Iso_1_uM>0.0) ? 1.23 : 1.0);
    p->Iso_increase_2 = ((p->Iso_1_uM>0.0) ? 1.2 : 1.0);
    p->Iso_shift_1 = ((p->Iso_1_uM>0.0) ? -14.0 : 0.0);
    p->Iso_shift_2 = ((p->Iso_1_uM>0.0) ? 7.5 : 0.0);
    p->Iso_shift_dL = ((p->Iso_1_uM>0.0) ? -8.0 : 0.0);
    p->Iso_slope_dL = ((p->Iso_1_uM>0.0) ? -27.0 : 0.0);
    p->RTONF = ((p->R_2*p->T)/p->F);
    p->V_cell = (((1e-09*3.14159265358979312e+00)*(pow(p->R_cell,2.0)))*p->L_cell);
    p->V_sub = (((((1e-09*2.0)*3.14159265358979312e+00)*p->L_sub)*(p->R_cell-((p->L_sub/2.0))))*p->L_cell);
    p->k34 = (p->Nao/(p->K3no+p->Nao));
    p->P_up = (p->P_up_basal*(1.0-(p->Ca_intracellular_fluxes_b_up)));
    p->V_i = ((p->V_i_part*p->V_cell)-(p->V_sub));
    p->V_jsr = (p->V_jsr_part*p->V_cell);
    p->V_nsr = (p->V_nsr_part*p->V_cell);
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_Loewe;

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Loewe( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Loewe_Params *p = (Loewe_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.

}



void    initialize_sv_Loewe( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Loewe_Params *p = (Loewe_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Loewe_state) );
  Loewe_state *sv_base = (Loewe_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Loewe_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Ca_jsr = Ca_jsr_init;
    sv->Ca_nsr = Ca_nsr_init;
    sv->Ca_sub = Ca_sub_init;
    sv->Cai = Cai_init;
    sv->I = I_init;
    sv->Ki = Ki_init;
    sv->Nai = Nai_init;
    sv->O = O_init;
    sv->RI = RI_init;
    sv->R_1 = R_1_init;
    V = V_init;
    sv->a = a_init;
    sv->dL = dL_init;
    sv->dT = dT_init;
    sv->fCMi = fCMi_init;
    sv->fCMs = fCMs_init;
    sv->fCQ = fCQ_init;
    sv->fCa = fCa_init;
    sv->fL = fL_init;
    sv->fT = fT_init;
    sv->fTC = fTC_init;
    sv->fTMC = fTMC_init;
    sv->fTMM = fTMM_init;
    sv->h = h_init;
    sv->m = m_init;
    sv->n = n_init;
    sv->paF = paF_init;
    sv->paS = paS_init;
    sv->piy = piy_init;
    sv->q = q_init;
    sv->r = r_init;
    sv->r_Kur = r_Kur_init;
    sv->s_Kur = s_Kur_init;
    sv->x = x_init;
    sv->y = y_init;
    double E_K = (p->RTONF*(log((p->Ko/sv->Ki))));
    double E_Ks = (p->RTONF*(log(((p->Ko+(0.12*p->Nao))/(sv->Ki+(0.12*sv->Nai))))));
    double E_Na = (p->RTONF*(log((p->Nao/sv->Nai))));
    double E_mh = (p->RTONF*(log(((p->Nao+(0.12*p->Ko))/(sv->Nai+(0.12*sv->Ki))))));
    double ICaT = ((((((2.0*p->P_CaT)*V)/(p->RTONF*(-(expm1((((-1.0*V)*2.0)/p->RTONF))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.0*V)/p->RTONF)))))))*sv->dT)*sv->fT);
    double IsiCa = (((((((2.0*p->P_CaL)*V)/(p->RTONF*(-(expm1((((-1.0*V)*2.0)/p->RTONF))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    double IsiK = (((((((0.000365*p->P_CaL)*V)/(p->RTONF*(-(expm1(((-1.0*V)/p->RTONF))))))*(sv->Ki-((p->Ko*(exp(((-1.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    double IsiNa = (((((((1.85e-05*p->P_CaL)*V)/(p->RTONF*(-(expm1(((-1.0*V)/p->RTONF))))))*(sv->Nai-((p->Nao*(exp(((-1.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    double di = ((1.0+((sv->Ca_sub/p->Kci)*((1.0+(exp((((-p->Qci)*V)/p->RTONF))))+(sv->Nai/p->Kcni))))+((sv->Nai/p->K1ni)*(1.0+((sv->Nai/p->K2ni)*(1.0+(sv->Nai/p->K3ni))))));
    double do_ = ((1.0+((p->Cao/p->Kco)*(1.0+(exp(((p->Qco*V)/p->RTONF))))))+((p->Nao/p->K1no)*(1.0+((p->Nao/p->K2no)*(1.0+(p->Nao/p->K3no))))));
    double k32 = (exp(((p->Qn*V)/(2.0*p->RTONF))));
    double k41 = (exp((((-p->Qn)*V)/(2.0*p->RTONF))));
    double k43 = (sv->Nai/(p->K3ni+sv->Nai));
    double ICaL = ((((IsiCa+IsiK)+IsiNa)*(1.0-(p->ACh_block)))*p->Iso_increase_1);
    double IKACh = ((p->ACh>0.0) ? ((((p->ACh_on*p->GKACh)*(V-(E_K)))*(1.0+(exp(((V+20.0)/20.0)))))*sv->a) : 0.0);
    double IKr = (((p->GKr*(V-(E_K)))*((0.9*sv->paF)+(0.1*sv->paS)))*sv->piy);
    double IKs = ((p->GKs*(V-(E_Ks)))*(pow(sv->n,2.0)));
    double IKur = (((p->GKur*sv->r_Kur)*sv->s_Kur)*(V-(E_K)));
    double INaK = ((((p->Iso_increase_2*p->INaK_max)*(pow((1.0+(pow((p->Km_Kp/p->Ko),1.2))),-1.0)))*(pow((1.0+(pow((p->Km_Nap/sv->Nai),1.3))),-1.0)))*(pow((1.0+(exp(((-((V-(E_Na))+110.0))/20.0)))),-1.0)));
    double INa_ = (((p->GNa*(pow(sv->m,3.0)))*sv->h)*(V-(E_mh)));
    double INa_L = ((p->GNa_L*(pow(sv->m,3.0)))*(V-(E_mh)));
    double ISK = ((p->GSK*(V-(E_K)))*sv->x);
    double IfK = ((sv->y*p->Gf_K)*(V-(E_K)));
    double IfNa = ((sv->y*p->Gf_Na)*(V-(E_Na)));
    double Ito = (((p->Gto*(V-(E_K)))*sv->q)*sv->r);
    double k12 = (((sv->Ca_sub/p->Kci)*(exp((((-p->Qci)*V)/p->RTONF))))/di);
    double k14 = ((((((sv->Nai/p->K1ni)*sv->Nai)/p->K2ni)*(1.0+(sv->Nai/p->K3ni)))*(exp(((p->Qn*V)/(2.0*p->RTONF)))))/di);
    double k21 = (((p->Cao/p->Kco)*(exp(((p->Qco*V)/p->RTONF))))/do_);
    double k23 = ((((((p->Nao/p->K1no)*p->Nao)/p->K2no)*(1.0+(p->Nao/p->K3no)))*(exp((((-p->Qn)*V)/(2.0*p->RTONF)))))/do_);
    double INa = (INa_+INa_L);
    double If = (IfNa+IfK);
    double x1 = (((k41*p->k34)*(k23+k21))+((k21*k32)*(k43+k41)));
    double x2 = (((k32*k43)*(k14+k12))+((k41*k12)*(p->k34+k32)));
    double x3 = (((k14*k43)*(k23+k21))+((k12*k23)*(k43+k41)));
    double x4 = (((k23*p->k34)*(k14+k12))+((k14*k21)*(p->k34+k32)));
    double INaCa = ((p->K_NaCa*((x2*k21)-((x1*k12))))/(((x1+x2)+x3)+x4));
    double Itot = (((((((((((If+IKr)+IKs)+Ito)+INaK)+INaCa)+INa)+ICaL)+ICaT)+IKACh)+IKur)+ISK);
    Iion = ((Itot*1000.)/p->C);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Loewe(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Loewe_Params *p  = (Loewe_Params *)IF->params;
  Loewe_state *sv_base = (Loewe_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Loewe_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t E_K = (p->RTONF*(log((p->Ko/sv->Ki))));
    GlobalData_t E_Ks = (p->RTONF*(log(((p->Ko+(0.12*p->Nao))/(sv->Ki+(0.12*sv->Nai))))));
    GlobalData_t E_Na = (p->RTONF*(log((p->Nao/sv->Nai))));
    GlobalData_t E_mh = (p->RTONF*(log(((p->Nao+(0.12*p->Ko))/(sv->Nai+(0.12*sv->Ki))))));
    GlobalData_t ICaT = ((((((2.0*p->P_CaT)*V)/(p->RTONF*(-(expm1((((-1.0*V)*2.0)/p->RTONF))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.0*V)/p->RTONF)))))))*sv->dT)*sv->fT);
    GlobalData_t IsiCa = (((((((2.0*p->P_CaL)*V)/(p->RTONF*(-(expm1((((-1.0*V)*2.0)/p->RTONF))))))*(sv->Ca_sub-((p->Cao*(exp(((-2.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    GlobalData_t IsiK = (((((((0.000365*p->P_CaL)*V)/(p->RTONF*(-(expm1(((-1.0*V)/p->RTONF))))))*(sv->Ki-((p->Ko*(exp(((-1.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    GlobalData_t IsiNa = (((((((1.85e-05*p->P_CaL)*V)/(p->RTONF*(-(expm1(((-1.0*V)/p->RTONF))))))*(sv->Nai-((p->Nao*(exp(((-1.0*V)/p->RTONF)))))))*sv->dL)*sv->fL)*sv->fCa);
    GlobalData_t di = ((1.0+((sv->Ca_sub/p->Kci)*((1.0+(exp((((-p->Qci)*V)/p->RTONF))))+(sv->Nai/p->Kcni))))+((sv->Nai/p->K1ni)*(1.0+((sv->Nai/p->K2ni)*(1.0+(sv->Nai/p->K3ni))))));
    GlobalData_t do_ = ((1.0+((p->Cao/p->Kco)*(1.0+(exp(((p->Qco*V)/p->RTONF))))))+((p->Nao/p->K1no)*(1.0+((p->Nao/p->K2no)*(1.0+(p->Nao/p->K3no))))));
    GlobalData_t k32 = (exp(((p->Qn*V)/(2.0*p->RTONF))));
    GlobalData_t k41 = (exp((((-p->Qn)*V)/(2.0*p->RTONF))));
    GlobalData_t k43 = (sv->Nai/(p->K3ni+sv->Nai));
    GlobalData_t ICaL = ((((IsiCa+IsiK)+IsiNa)*(1.0-(p->ACh_block)))*p->Iso_increase_1);
    GlobalData_t IKACh = ((p->ACh>0.0) ? ((((p->ACh_on*p->GKACh)*(V-(E_K)))*(1.0+(exp(((V+20.0)/20.0)))))*sv->a) : 0.0);
    GlobalData_t IKr = (((p->GKr*(V-(E_K)))*((0.9*sv->paF)+(0.1*sv->paS)))*sv->piy);
    GlobalData_t IKs = ((p->GKs*(V-(E_Ks)))*(pow(sv->n,2.0)));
    GlobalData_t IKur = (((p->GKur*sv->r_Kur)*sv->s_Kur)*(V-(E_K)));
    GlobalData_t INaK = ((((p->Iso_increase_2*p->INaK_max)*(pow((1.0+(pow((p->Km_Kp/p->Ko),1.2))),-1.0)))*(pow((1.0+(pow((p->Km_Nap/sv->Nai),1.3))),-1.0)))*(pow((1.0+(exp(((-((V-(E_Na))+110.0))/20.0)))),-1.0)));
    GlobalData_t INa_ = (((p->GNa*(pow(sv->m,3.0)))*sv->h)*(V-(E_mh)));
    GlobalData_t INa_L = ((p->GNa_L*(pow(sv->m,3.0)))*(V-(E_mh)));
    GlobalData_t ISK = ((p->GSK*(V-(E_K)))*sv->x);
    GlobalData_t IfK = ((sv->y*p->Gf_K)*(V-(E_K)));
    GlobalData_t IfNa = ((sv->y*p->Gf_Na)*(V-(E_Na)));
    GlobalData_t Ito = (((p->Gto*(V-(E_K)))*sv->q)*sv->r);
    GlobalData_t k12 = (((sv->Ca_sub/p->Kci)*(exp((((-p->Qci)*V)/p->RTONF))))/di);
    GlobalData_t k14 = ((((((sv->Nai/p->K1ni)*sv->Nai)/p->K2ni)*(1.0+(sv->Nai/p->K3ni)))*(exp(((p->Qn*V)/(2.0*p->RTONF)))))/di);
    GlobalData_t k21 = (((p->Cao/p->Kco)*(exp(((p->Qco*V)/p->RTONF))))/do_);
    GlobalData_t k23 = ((((((p->Nao/p->K1no)*p->Nao)/p->K2no)*(1.0+(p->Nao/p->K3no)))*(exp((((-p->Qn)*V)/(2.0*p->RTONF)))))/do_);
    GlobalData_t INa = (INa_+INa_L);
    GlobalData_t If = (IfNa+IfK);
    GlobalData_t x1 = (((k41*p->k34)*(k23+k21))+((k21*k32)*(k43+k41)));
    GlobalData_t x2 = (((k32*k43)*(k14+k12))+((k41*k12)*(p->k34+k32)));
    GlobalData_t x3 = (((k14*k43)*(k23+k21))+((k12*k23)*(k43+k41)));
    GlobalData_t x4 = (((k23*p->k34)*(k14+k12))+((k14*k21)*(p->k34+k32)));
    GlobalData_t INaCa = ((p->K_NaCa*((x2*k21)-((x1*k12))))/(((x1+x2)+x3)+x4));
    GlobalData_t Itot = (((((((((((If+IKr)+IKs)+Ito)+INaK)+INaCa)+INa)+ICaL)+ICaT)+IKACh)+IKur)+ISK);
    Iion = ((Itot*1000.)/p->C);
    
    
    //Complete Forward Euler Update
    GlobalData_t ISK_x_gate_tau_x = (1.0/(((0.047*sv->Ca_sub)*1e3)+(1.0/76.0)));
    GlobalData_t delta_fCMi = (((p->kf_CM*sv->Cai)*(1.0-(sv->fCMi)))-((p->kb_CM*sv->fCMi)));
    GlobalData_t delta_fCMs = (((p->kf_CM*sv->Ca_sub)*(1.0-(sv->fCMs)))-((p->kb_CM*sv->fCMs)));
    GlobalData_t delta_fCQ = (((p->kf_CQ*sv->Ca_jsr)*(1.0-(sv->fCQ)))-((p->kb_CQ*sv->fCQ)));
    GlobalData_t delta_fTC = (((p->kf_TC*sv->Cai)*(1.0-(sv->fTC)))-((p->kb_TC*sv->fTC)));
    GlobalData_t delta_fTMC = (((p->kf_TMC*sv->Cai)*(1.0-((sv->fTMC+sv->fTMM))))-((p->kb_TMC*sv->fTMC)));
    GlobalData_t delta_fTMM = (((p->kf_TMM*p->Mgi)*(1.0-((sv->fTMC+sv->fTMM))))-((p->kb_TMM*sv->fTMM)));
    GlobalData_t diff_Ki = (((-1.0*(((((((IKur+Ito)+IKr)+IKs)+IfK)+IsiK)+ISK)-((2.0*INaK))))/((p->V_i+p->V_sub)*p->F))*0.001);
    GlobalData_t diff_Nai = (((-1.0*((((INa+IfNa)+IsiNa)+(3.0*INaK))+(3.0*INaCa)))/((p->V_i+p->V_sub)*p->F))*0.001);
    GlobalData_t fCa_infinity = (p->Km_fCa/(p->Km_fCa+sv->Ca_sub));
    GlobalData_t j_Ca_dif = ((sv->Ca_sub-(sv->Cai))/p->Ca_intracellular_fluxes_tau_dif_Ca);
    GlobalData_t j_SRCarel = ((p->ks*sv->O)*(sv->Ca_jsr-(sv->Ca_sub)));
    GlobalData_t j_tr = ((sv->Ca_nsr-(sv->Ca_jsr))/p->Ca_intracellular_fluxes_tau_tr);
    GlobalData_t j_up = (p->P_up/(1.0+(exp((((-sv->Cai)+p->K_up)/p->slope_up)))));
    GlobalData_t kCaSR = (p->MaxSR-(((p->MaxSR-(p->MinSR))/(1.0+(pow((p->EC50_SR/sv->Ca_jsr),p->HSR))))));
    GlobalData_t x_infinity = ((0.81*(pow((sv->Ca_sub*1e3),p->n_SK)))/((pow((sv->Ca_sub*1e3),p->n_SK))+(pow(p->EC50_SK,p->n_SK))));
    GlobalData_t ICaL_fCa_gate_tau_fCa = ((0.001*fCa_infinity)/p->ICaL_fCa_gate_alpha_fCa);
    GlobalData_t diff_Ca_jsr = ((j_tr-((j_SRCarel+(p->CQ_tot*delta_fCQ))))*0.001);
    GlobalData_t diff_Ca_nsr = ((j_up-(((j_tr*p->V_jsr)/p->V_nsr)))*0.001);
    GlobalData_t diff_Ca_sub = ((((j_SRCarel*p->V_jsr)/p->V_sub)-((((((IsiCa+ICaT)-((2.0*INaCa)))/((2.0*p->F)*p->V_sub))+j_Ca_dif)+(p->CM_tot*delta_fCMs))))*0.001);
    GlobalData_t diff_Cai = (((((j_Ca_dif*p->V_sub)-((j_up*p->V_nsr)))/p->V_i)-((((p->CM_tot*delta_fCMi)+(p->TC_tot*delta_fTC))+(p->TMC_tot*delta_fTMC))))*0.001);
    GlobalData_t diff_fCMi = (delta_fCMi*0.001);
    GlobalData_t diff_fCMs = (delta_fCMs*0.001);
    GlobalData_t diff_fCQ = (delta_fCQ*0.001);
    GlobalData_t diff_fTC = (delta_fTC*0.001);
    GlobalData_t diff_fTMC = (delta_fTMC*0.001);
    GlobalData_t diff_fTMM = (delta_fTMM*0.001);
    GlobalData_t diff_x = ((x_infinity-(sv->x))/ISK_x_gate_tau_x);
    GlobalData_t kiSRCa = (p->kiCa*kCaSR);
    GlobalData_t koSRCa = (p->koCa/kCaSR);
    GlobalData_t diff_I = (((((kiSRCa*sv->Ca_sub)*sv->O)-((p->kim*sv->I)))-(((p->kom*sv->I)-(((koSRCa*(pow(sv->Ca_sub,2.0)))*sv->RI)))))*0.001);
    GlobalData_t diff_O = (((((koSRCa*(pow(sv->Ca_sub,2.0)))*sv->R_1)-((p->kom*sv->O)))-((((kiSRCa*sv->Ca_sub)*sv->O)-((p->kim*sv->I)))))*0.001);
    GlobalData_t diff_RI = ((((p->kom*sv->I)-(((koSRCa*(pow(sv->Ca_sub,2.0)))*sv->RI)))-(((p->kim*sv->RI)-(((kiSRCa*sv->Ca_sub)*sv->R_1)))))*0.001);
    GlobalData_t diff_R_1 = ((((p->kim*sv->RI)-(((kiSRCa*sv->Ca_sub)*sv->R_1)))-((((koSRCa*(pow(sv->Ca_sub,2.0)))*sv->R_1)-((p->kom*sv->O)))))*0.001);
    GlobalData_t diff_fCa = (((fCa_infinity-(sv->fCa))/ICaL_fCa_gate_tau_fCa)*0.001);
    GlobalData_t Ca_jsr_new = sv->Ca_jsr+diff_Ca_jsr*dt;
    GlobalData_t Ca_nsr_new = sv->Ca_nsr+diff_Ca_nsr*dt;
    GlobalData_t Ca_sub_new = sv->Ca_sub+diff_Ca_sub*dt;
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    GlobalData_t I_new = sv->I+diff_I*dt;
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t O_new = sv->O+diff_O*dt;
    GlobalData_t RI_new = sv->RI+diff_RI*dt;
    GlobalData_t R_1_new = sv->R_1+diff_R_1*dt;
    GlobalData_t fCMi_new = sv->fCMi+diff_fCMi*dt;
    GlobalData_t fCMs_new = sv->fCMs+diff_fCMs*dt;
    GlobalData_t fCQ_new = sv->fCQ+diff_fCQ*dt;
    GlobalData_t fCa_new = sv->fCa+diff_fCa*dt;
    GlobalData_t fTC_new = sv->fTC+diff_fTC*dt;
    GlobalData_t fTMC_new = sv->fTMC+diff_fTMC*dt;
    GlobalData_t fTMM_new = sv->fTMM+diff_fTMM*dt;
    GlobalData_t x_new = sv->x+diff_x*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t E0_m = (V+41.0);
    GlobalData_t IKACh_a_gate_beta_a = (10.0*(exp((0.0133*(V+40.0)))));
    GlobalData_t IKs_n_gate_alpha_n = (28.0/(1.0+(exp(((-((V-(40.0))-(p->Iso_shift_1)))/3.0)))));
    GlobalData_t IKs_n_gate_beta_n = (exp(((-((V-(p->Iso_shift_1))-(5.0)))/25.0)));
    GlobalData_t INa_h_gate_alpha_h = (20.0*(exp((-0.125*(V+75.0)))));
    GlobalData_t INa_h_gate_beta_h = (2000.0/((320.0*(exp((-0.1*(V+75.0)))))+1.0));
    GlobalData_t INa_m_gate_beta_m = (8000.0*(exp((-0.056*(V+66.0)))));
    GlobalData_t adVm = ((V==-41.8) ? -41.80001 : ((V==0.0) ? 0.0 : ((V==-6.8) ? -6.80001 : V)));
    GlobalData_t bdVm = ((V==-1.8) ? -1.80001 : V);
    GlobalData_t dL_inf = (1.0/(1.0+(exp(((-((V-(p->V_dL))-(p->Iso_shift_dL)))/(p->k_dL*(1.0+(p->Iso_slope_dL/100.0))))))));
    GlobalData_t dT_inf = (1.0/(1.0+(exp(((-(V+38.3))/5.5)))));
    GlobalData_t fL_inf = (1.0/(1.0+(exp((((V+12.1847)+p->shift_fL)/p->k_fL)))));
    GlobalData_t fT_inf = (1.0/(1.0+(exp(((V+58.7)/3.8)))));
    GlobalData_t h_inf = (1.0/(1.0+(exp(((V+69.804)/4.4565)))));
    GlobalData_t m_inf = (1.0/(1.0+(exp(((-(V+42.0504))/8.3106)))));
    GlobalData_t n_inf = (sqrt((1.0/(1.0+(exp(((-((V+0.6383)-(p->Iso_shift_1)))/10.7071)))))));
    GlobalData_t pa_infinity = (1.0/(1.0+(exp(((-(V+10.0144))/7.6607)))));
    GlobalData_t piy_inf = (1.0/(1.0+(exp(((V+28.6)/17.1)))));
    GlobalData_t q_inf = (1.0/(1.0+(exp(((V+49.0)/13.0)))));
    GlobalData_t r_Kur_inf = (1.0/(1.0+(exp(((V+6.0)/-8.6)))));
    GlobalData_t r_inf = (1.0/(1.0+(exp(((-(V-(19.3)))/15.0)))));
    GlobalData_t s_Kur_inf = (1.0/(1.0+(exp(((V+7.5)/10.0)))));
    GlobalData_t tau_dT = ((0.001/((1.068*(exp(((V+38.3)/30.0))))+(1.068*(exp(((-(V+38.3))/30.0))))))*1000.0);
    GlobalData_t tau_fL = ((0.001*(44.3+(230.0*(exp((-(pow(((V+36.0)/10.0),2.0))))))))*1000.0);
    GlobalData_t tau_fT = (((1.0/((16.67*(exp(((-(V+75.0))/83.3))))+(16.67*(exp(((V+75.0)/15.38))))))+p->offset_fT)*1000.0);
    GlobalData_t tau_paF = ((1.0/((30.0*(exp((V/10.0))))+(exp(((-V)/12.0)))))*1000.0);
    GlobalData_t tau_paS = ((8.46553540000000049e-01/((4.2*(exp((V/17.0))))+(0.15*(exp(((-V)/21.6))))))*1000.0);
    GlobalData_t tau_piy = ((1.0/((100.0*(exp(((-V)/54.645))))+(656.0*(exp((V/106.157))))))*1000.0);
    GlobalData_t tau_q = (((0.001*0.6)*((65.17/((0.57*(exp((-0.08*(V+44.0)))))+(0.065*(exp((0.1*(V+45.93)))))))+10.1))*1000.0);
    GlobalData_t tau_r = ((((0.001*0.66)*1.4)*((15.59/((1.037*(exp((0.09*(V+30.61)))))+(0.369*(exp((-0.12*(V+23.84)))))))+2.98))*1000.0);
    GlobalData_t tau_r_Kur = (((0.009/(1.0+(exp(((V+5.0)/12.0)))))+0.0005)*1000.0);
    GlobalData_t tau_s_Kur = (((0.59/(1.0+(exp(((V+60.0)/10.0)))))+3.05)*1000.0);
    GlobalData_t tau_y = (((1.0/(((0.36*(((V+148.8)-(p->ACh_shift))-(p->Iso_shift_2)))/(expm1((0.066*(((V+148.8)-(p->ACh_shift))-(p->Iso_shift_2))))))+((0.1*(((V+87.3)-(p->ACh_shift))-(p->Iso_shift_2)))/(-(expm1((-0.2*(((V+87.3)-(p->ACh_shift))-(p->Iso_shift_2)))))))))-(0.054))*1000.0);
    GlobalData_t y_inf = ((V<(-(((80.0-(p->ACh_shift))-(p->Iso_shift_2))-(p->y_shift)))) ? (0.01329+(0.99921/(1.0+(exp((((((V+97.134)-(p->ACh_shift))-(p->Iso_shift_2))-(p->y_shift))/8.1752)))))) : (0.0002501*(exp(((-(((V-(p->ACh_shift))-(p->Iso_shift_2))-(p->y_shift)))/12.861)))));
    GlobalData_t ICaL_dL_gate_alpha_dL = (((-0.02839*(adVm+41.8))/(expm1(((-(adVm+41.8))/2.5))))-(((0.0849*(adVm+6.8))/(expm1(((-(adVm+6.8))/4.8))))));
    GlobalData_t ICaL_dL_gate_beta_dL = ((0.01143*(bdVm+1.8))/(expm1(((bdVm+1.8)/2.5))));
    GlobalData_t INa_m_gate_alpha_m = (((fabs(E0_m))<p->delta_m) ? 2000.0 : ((200.0*E0_m)/(-(expm1((-0.1*E0_m))))));
    GlobalData_t a_inf = (p->IKACh_a_gate_alpha_a/(p->IKACh_a_gate_alpha_a+IKACh_a_gate_beta_a));
    GlobalData_t dT_rush_larsen_B = (exp(((-dt)/tau_dT)));
    GlobalData_t dT_rush_larsen_C = (expm1(((-dt)/tau_dT)));
    GlobalData_t fL_rush_larsen_B = (exp(((-dt)/tau_fL)));
    GlobalData_t fL_rush_larsen_C = (expm1(((-dt)/tau_fL)));
    GlobalData_t fT_rush_larsen_B = (exp(((-dt)/tau_fT)));
    GlobalData_t fT_rush_larsen_C = (expm1(((-dt)/tau_fT)));
    GlobalData_t paF_inf = pa_infinity;
    GlobalData_t paF_rush_larsen_B = (exp(((-dt)/tau_paF)));
    GlobalData_t paF_rush_larsen_C = (expm1(((-dt)/tau_paF)));
    GlobalData_t paS_inf = pa_infinity;
    GlobalData_t paS_rush_larsen_B = (exp(((-dt)/tau_paS)));
    GlobalData_t paS_rush_larsen_C = (expm1(((-dt)/tau_paS)));
    GlobalData_t piy_rush_larsen_B = (exp(((-dt)/tau_piy)));
    GlobalData_t piy_rush_larsen_C = (expm1(((-dt)/tau_piy)));
    GlobalData_t q_rush_larsen_B = (exp(((-dt)/tau_q)));
    GlobalData_t q_rush_larsen_C = (expm1(((-dt)/tau_q)));
    GlobalData_t r_Kur_rush_larsen_B = (exp(((-dt)/tau_r_Kur)));
    GlobalData_t r_Kur_rush_larsen_C = (expm1(((-dt)/tau_r_Kur)));
    GlobalData_t r_rush_larsen_B = (exp(((-dt)/tau_r)));
    GlobalData_t r_rush_larsen_C = (expm1(((-dt)/tau_r)));
    GlobalData_t s_Kur_rush_larsen_B = (exp(((-dt)/tau_s_Kur)));
    GlobalData_t s_Kur_rush_larsen_C = (expm1(((-dt)/tau_s_Kur)));
    GlobalData_t tau_a = ((1.0/(p->IKACh_a_gate_alpha_a+IKACh_a_gate_beta_a))*1000.0);
    GlobalData_t tau_h = ((1.0/(INa_h_gate_alpha_h+INa_h_gate_beta_h))*1000.0);
    GlobalData_t tau_n = ((1.0/(IKs_n_gate_alpha_n+IKs_n_gate_beta_n))*1000.0);
    GlobalData_t y_rush_larsen_B = (exp(((-dt)/tau_y)));
    GlobalData_t y_rush_larsen_C = (expm1(((-dt)/tau_y)));
    GlobalData_t a_rush_larsen_B = (exp(((-dt)/tau_a)));
    GlobalData_t a_rush_larsen_C = (expm1(((-dt)/tau_a)));
    GlobalData_t dT_rush_larsen_A = ((-dT_inf)*dT_rush_larsen_C);
    GlobalData_t fL_rush_larsen_A = ((-fL_inf)*fL_rush_larsen_C);
    GlobalData_t fT_rush_larsen_A = ((-fT_inf)*fT_rush_larsen_C);
    GlobalData_t h_rush_larsen_B = (exp(((-dt)/tau_h)));
    GlobalData_t h_rush_larsen_C = (expm1(((-dt)/tau_h)));
    GlobalData_t n_rush_larsen_B = (exp(((-dt)/tau_n)));
    GlobalData_t n_rush_larsen_C = (expm1(((-dt)/tau_n)));
    GlobalData_t paF_rush_larsen_A = ((-paF_inf)*paF_rush_larsen_C);
    GlobalData_t paS_rush_larsen_A = ((-paS_inf)*paS_rush_larsen_C);
    GlobalData_t piy_rush_larsen_A = ((-piy_inf)*piy_rush_larsen_C);
    GlobalData_t q_rush_larsen_A = ((-q_inf)*q_rush_larsen_C);
    GlobalData_t r_Kur_rush_larsen_A = ((-r_Kur_inf)*r_Kur_rush_larsen_C);
    GlobalData_t r_rush_larsen_A = ((-r_inf)*r_rush_larsen_C);
    GlobalData_t s_Kur_rush_larsen_A = ((-s_Kur_inf)*s_Kur_rush_larsen_C);
    GlobalData_t tau_dL = ((0.001/(ICaL_dL_gate_alpha_dL+ICaL_dL_gate_beta_dL))*1000.0);
    GlobalData_t tau_m = ((1.0/(INa_m_gate_alpha_m+INa_m_gate_beta_m))*1000.0);
    GlobalData_t y_rush_larsen_A = ((-y_inf)*y_rush_larsen_C);
    GlobalData_t a_rush_larsen_A = ((-a_inf)*a_rush_larsen_C);
    GlobalData_t dL_rush_larsen_B = (exp(((-dt)/tau_dL)));
    GlobalData_t dL_rush_larsen_C = (expm1(((-dt)/tau_dL)));
    GlobalData_t h_rush_larsen_A = ((-h_inf)*h_rush_larsen_C);
    GlobalData_t m_rush_larsen_B = (exp(((-dt)/tau_m)));
    GlobalData_t m_rush_larsen_C = (expm1(((-dt)/tau_m)));
    GlobalData_t n_rush_larsen_A = ((-n_inf)*n_rush_larsen_C);
    GlobalData_t dL_rush_larsen_A = ((-dL_inf)*dL_rush_larsen_C);
    GlobalData_t m_rush_larsen_A = ((-m_inf)*m_rush_larsen_C);
    GlobalData_t a_new = a_rush_larsen_A+a_rush_larsen_B*sv->a;
    GlobalData_t dL_new = dL_rush_larsen_A+dL_rush_larsen_B*sv->dL;
    GlobalData_t dT_new = dT_rush_larsen_A+dT_rush_larsen_B*sv->dT;
    GlobalData_t fL_new = fL_rush_larsen_A+fL_rush_larsen_B*sv->fL;
    GlobalData_t fT_new = fT_rush_larsen_A+fT_rush_larsen_B*sv->fT;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t n_new = n_rush_larsen_A+n_rush_larsen_B*sv->n;
    GlobalData_t paF_new = paF_rush_larsen_A+paF_rush_larsen_B*sv->paF;
    GlobalData_t paS_new = paS_rush_larsen_A+paS_rush_larsen_B*sv->paS;
    GlobalData_t piy_new = piy_rush_larsen_A+piy_rush_larsen_B*sv->piy;
    GlobalData_t q_new = q_rush_larsen_A+q_rush_larsen_B*sv->q;
    GlobalData_t r_new = r_rush_larsen_A+r_rush_larsen_B*sv->r;
    GlobalData_t r_Kur_new = r_Kur_rush_larsen_A+r_Kur_rush_larsen_B*sv->r_Kur;
    GlobalData_t s_Kur_new = s_Kur_rush_larsen_A+s_Kur_rush_larsen_B*sv->s_Kur;
    GlobalData_t y_new = y_rush_larsen_A+y_rush_larsen_B*sv->y;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Ca_jsr = Ca_jsr_new;
    sv->Ca_nsr = Ca_nsr_new;
    sv->Ca_sub = Ca_sub_new;
    sv->Cai = Cai_new;
    sv->I = I_new;
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->O = O_new;
    sv->RI = RI_new;
    sv->R_1 = R_1_new;
    sv->a = a_new;
    sv->dL = dL_new;
    sv->dT = dT_new;
    sv->fCMi = fCMi_new;
    sv->fCMs = fCMs_new;
    sv->fCQ = fCQ_new;
    sv->fCa = fCa_new;
    sv->fL = fL_new;
    sv->fT = fT_new;
    sv->fTC = fTC_new;
    sv->fTMC = fTMC_new;
    sv->fTMM = fTMM_new;
    sv->h = h_new;
    sv->m = m_new;
    sv->n = n_new;
    sv->paF = paF_new;
    sv->paS = paS_new;
    sv->piy = piy_new;
    sv->q = q_new;
    sv->r = r_new;
    sv->r_Kur = r_Kur_new;
    sv->s_Kur = s_Kur_new;
    sv->x = x_new;
    sv->y = y_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_Loewe(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Loewe_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->Ca_jsr\n"
        "sv->Ca_nsr\n"
        "sv->Ca_sub\n"
        "sv->Cai\n"
        "sv->I\n"
        "sv->Ki\n"
        "sv->Nai\n"
        "sv->O\n"
        "sv->RI\n"
        "sv->R_1\n"
        "V\n"
        "sv->a\n"
        "sv->dL\n"
        "sv->dT\n"
        "sv->fCMi\n"
        "sv->fCMs\n"
        "sv->fCQ\n"
        "sv->fCa\n"
        "sv->fL\n"
        "sv->fT\n"
        "sv->fTC\n"
        "sv->fTMC\n"
        "sv->fTMM\n"
        "sv->h\n"
        "sv->m\n"
        "sv->n\n"
        "sv->paF\n"
        "sv->paS\n"
        "sv->piy\n"
        "sv->q\n"
        "sv->r\n"
        "sv->r_Kur\n"
        "sv->s_Kur\n"
        "sv->x\n"
        "sv->y\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Loewe_Params *p  = (Loewe_Params *)IF->params;

  Loewe_state *sv_base = (Loewe_state *)IF->sv_tab.y;
  Loewe_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->Ca_jsr);
  fprintf(file, "%4.12f\t", sv->Ca_nsr);
  fprintf(file, "%4.12f\t", sv->Ca_sub);
  fprintf(file, "%4.12f\t", sv->Cai);
  fprintf(file, "%4.12f\t", sv->I);
  fprintf(file, "%4.12f\t", sv->Ki);
  fprintf(file, "%4.12f\t", sv->Nai);
  fprintf(file, "%4.12f\t", sv->O);
  fprintf(file, "%4.12f\t", sv->RI);
  fprintf(file, "%4.12f\t", sv->R_1);
  fprintf(file, "%4.12f\t", V);
  fprintf(file, "%4.12f\t", sv->a);
  fprintf(file, "%4.12f\t", sv->dL);
  fprintf(file, "%4.12f\t", sv->dT);
  fprintf(file, "%4.12f\t", sv->fCMi);
  fprintf(file, "%4.12f\t", sv->fCMs);
  fprintf(file, "%4.12f\t", sv->fCQ);
  fprintf(file, "%4.12f\t", sv->fCa);
  fprintf(file, "%4.12f\t", sv->fL);
  fprintf(file, "%4.12f\t", sv->fT);
  fprintf(file, "%4.12f\t", sv->fTC);
  fprintf(file, "%4.12f\t", sv->fTMC);
  fprintf(file, "%4.12f\t", sv->fTMM);
  fprintf(file, "%4.12f\t", sv->h);
  fprintf(file, "%4.12f\t", sv->m);
  fprintf(file, "%4.12f\t", sv->n);
  fprintf(file, "%4.12f\t", sv->paF);
  fprintf(file, "%4.12f\t", sv->paS);
  fprintf(file, "%4.12f\t", sv->piy);
  fprintf(file, "%4.12f\t", sv->q);
  fprintf(file, "%4.12f\t", sv->r);
  fprintf(file, "%4.12f\t", sv->r_Kur);
  fprintf(file, "%4.12f\t", sv->s_Kur);
  fprintf(file, "%4.12f\t", sv->x);
  fprintf(file, "%4.12f\t", sv->y);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        