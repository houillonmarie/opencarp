// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Robert Plonsey
*  Year: 1974
*  Title: The Formulation of Bioelectric Source-field Relationships in Terms of Surface Discontinuities
*  Journal: Journal of the Franklin Institute, 297(5),317-324
*  DOI: 10.1016/0016-0032(74)90036-2.
*  Comment: Passive model
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __PLONSEY_H__
#define __PLONSEY_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Plonsey_REQDAT Vm_DATA_FLAG
#define Plonsey_MODDAT Iion_DATA_FLAG

struct Plonsey_Params {
    GlobalData_t Rm;
    GlobalData_t Vrest;

};

struct Plonsey_state {
    char dummy; //PGI doesn't allow a structure of 0 bytes.

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Plonsey(ION_IF *);
void construct_tables_Plonsey(ION_IF *);
void destroy_Plonsey(ION_IF *);
void initialize_sv_Plonsey(ION_IF *, GlobalData_t**);
void compute_Plonsey(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
