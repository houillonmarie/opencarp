// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Ferrero (Jr), J.M., Saiz, J., Ferrero-Corral, J.M., Thakor, N.V.
*  Year: 1996
*  Title: Simulation of Action Potentials from Metabolically Impaired Cardiac Myocytes. Role of ATP-Sensitive Potassium Current
*  Journal: Circulation Research, 79: 208-221
*  DOI: 10.1161/01.RES.79.2.208
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "IKATP_Ferrero.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_IKATP_Ferrero(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_IKATP_Ferrero( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define BETA (GlobalData_t)((2.0/0.0007))
#define F (GlobalData_t)(96485.)
#define Ke_init (GlobalData_t)(5.4)
#define Ki_init (GlobalData_t)(138.4)
#define Mgi (GlobalData_t)(3.1)
#define Nai_init (GlobalData_t)(10.0)
#define Q10 (GlobalData_t)(1.3)
#define T0 (GlobalData_t)(36.)
#define alpha (GlobalData_t)(1.0)
#define beta (GlobalData_t)(1.0)
#define delta_Mg (GlobalData_t)(0.32)
#define delta_Na (GlobalData_t)(0.35)
#define po (GlobalData_t)(0.91)
#define sigma (GlobalData_t)(3.8)
#define RTF (GlobalData_t)(((8.3143*(273.+T0))/96.4867))
#define T (GlobalData_t)(T0)
#define fT (GlobalData_t)((pow(Q10,((T-(T0))/10.))))



void initialize_params_IKATP_Ferrero( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  IKATP_Ferrero_Params *p = (IKATP_Ferrero_Params *)IF->params;

  // Compute the regional constants
  {
    p->ADPi_init = 15.0;
    p->ATPi_init = 6.8;
    p->GAMMA0 = 0.8;
    p->gamma = 35.375;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_IKATP_Ferrero;

}


// Define the parameters for the lookup tables
enum Tables {
  ADP_TAB,
  Ke_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum ADP_TableIndex {
  NROWS_ADP
};

enum Ke_TableIndex {
  gamma_0_idx,
  NROWS_Ke
};



void construct_tables_IKATP_Ferrero( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IKATP_Ferrero_Params *p = (IKATP_Ferrero_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  
  // Create the Ke lookup table
  LUT* Ke_tab = &IF->tables[Ke_TAB];
  LUT_alloc(Ke_tab, NROWS_Ke, 0., 16., 0.001, "IKATP_Ferrero Ke");
  for (int __i=Ke_tab->mn_ind; __i<=Ke_tab->mx_ind; __i++) {
    double Ke = Ke_tab->res*__i;
    LUT_data_t* Ke_row = Ke_tab->tab[__i];
    Ke_row[gamma_0_idx] = (p->gamma*(pow((Ke/5.4),0.24)));
  }
  check_LUT(Ke_tab);
  

}



void    initialize_sv_IKATP_Ferrero( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IKATP_Ferrero_Params *p = (IKATP_Ferrero_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(IKATP_Ferrero_state) );
  IKATP_Ferrero_state *sv_base = (IKATP_Ferrero_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Ke_ext = impdata[Ke];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  
  int __ADPi_sizeof;
  int __ADPi_offset;
  SVgetfcn __ADPi_SVgetfcn = get_sv_offset( IF->parent->type, "ADPi", &__ADPi_offset, &__ADPi_sizeof );
  SVputfcn __ADPi_SVputfcn = __ADPi_SVgetfcn ? getPutSV(__ADPi_SVgetfcn) : NULL;
  
  int __ATPi_sizeof;
  int __ATPi_offset;
  SVgetfcn __ATPi_SVgetfcn = get_sv_offset( IF->parent->type, "ATPi", &__ATPi_offset, &__ATPi_sizeof );
  SVputfcn __ATPi_SVputfcn = __ATPi_SVgetfcn ? getPutSV(__ATPi_SVgetfcn) : NULL;
  
  int __Ki_sizeof;
  int __Ki_offset;
  SVgetfcn __Ki_SVgetfcn = get_sv_offset( IF->parent->type, "Ki", &__Ki_offset, &__Ki_sizeof );
  SVputfcn __Ki_SVputfcn = __Ki_SVgetfcn ? getPutSV(__Ki_SVgetfcn) : NULL;
  
  int __Nai_sizeof;
  int __Nai_offset;
  SVgetfcn __Nai_SVgetfcn = get_sv_offset( IF->parent->type, "Nai", &__Nai_offset, &__Nai_sizeof );
  SVputfcn __Nai_SVputfcn = __Nai_SVgetfcn ? getPutSV(__Nai_SVgetfcn) : NULL;

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    IKATP_Ferrero_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Ke = Ke_ext[__i];
    GlobalData_t V = V_ext[__i];
    GlobalData_t ADPi = __ADPi_SVgetfcn ? __ADPi_SVgetfcn(IF->parent, __i, __ADPi_offset) :sv->__ADPi_local;
    GlobalData_t ATPi = __ATPi_SVgetfcn ? __ATPi_SVgetfcn(IF->parent, __i, __ATPi_offset) :sv->__ATPi_local;
    GlobalData_t Ki = __Ki_SVgetfcn ? __Ki_SVgetfcn(IF->parent, __i, __Ki_offset) :sv->__Ki_local;
    GlobalData_t Nai = __Nai_SVgetfcn ? __Nai_SVgetfcn(IF->parent, __i, __Nai_offset) :sv->__Nai_local;
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    ADPi = p->ADPi_init;
    ATPi = p->ATPi_init;
    Ke = Ke_init;
    Ki = Ki_init;
    Nai = Nai_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Ke_ext[__i] = Ke;
    V_ext[__i] = V;
    if( __ADPi_SVputfcn ) 
    	__ADPi_SVputfcn(IF->parent, __i, __ADPi_offset, ADPi);
    else
    	sv->__ADPi_local=ADPi;
    if( __ATPi_SVputfcn ) 
    	__ATPi_SVputfcn(IF->parent, __i, __ATPi_offset, ATPi);
    else
    	sv->__ATPi_local=ATPi;
    if( __Ki_SVputfcn ) 
    	__Ki_SVputfcn(IF->parent, __i, __Ki_offset, Ki);
    else
    	sv->__Ki_local=Ki;
    if( __Nai_SVputfcn ) 
    	__Nai_SVputfcn(IF->parent, __i, __Nai_offset, Nai);
    else
    	sv->__Nai_local=Nai;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_IKATP_Ferrero(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IKATP_Ferrero_Params *p  = (IKATP_Ferrero_Params *)IF->params;
  IKATP_Ferrero_state *sv_base = (IKATP_Ferrero_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Ke_ext = impdata[Ke];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  
  int __ADPi_sizeof;
  int __ADPi_offset;
  SVgetfcn __ADPi_SVgetfcn = get_sv_offset( IF->parent->type, "ADPi", &__ADPi_offset, &__ADPi_sizeof );
  SVputfcn __ADPi_SVputfcn = __ADPi_SVgetfcn ? getPutSV(__ADPi_SVgetfcn) : NULL;
  
  int __ATPi_sizeof;
  int __ATPi_offset;
  SVgetfcn __ATPi_SVgetfcn = get_sv_offset( IF->parent->type, "ATPi", &__ATPi_offset, &__ATPi_sizeof );
  SVputfcn __ATPi_SVputfcn = __ATPi_SVgetfcn ? getPutSV(__ATPi_SVgetfcn) : NULL;
  
  int __Ki_sizeof;
  int __Ki_offset;
  SVgetfcn __Ki_SVgetfcn = get_sv_offset( IF->parent->type, "Ki", &__Ki_offset, &__Ki_sizeof );
  SVputfcn __Ki_SVputfcn = __Ki_SVgetfcn ? getPutSV(__Ki_SVgetfcn) : NULL;
  
  int __Nai_sizeof;
  int __Nai_offset;
  SVgetfcn __Nai_SVgetfcn = get_sv_offset( IF->parent->type, "Nai", &__Nai_offset, &__Nai_sizeof );
  SVputfcn __Nai_SVputfcn = __Nai_SVgetfcn ? getPutSV(__Nai_SVgetfcn) : NULL;

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    IKATP_Ferrero_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Ke = Ke_ext[__i];
    GlobalData_t V = V_ext[__i];
    GlobalData_t ADPi = __ADPi_SVgetfcn ? __ADPi_SVgetfcn(IF->parent, __i, __ADPi_offset) :sv->__ADPi_local;
    GlobalData_t ATPi = __ATPi_SVgetfcn ? __ATPi_SVgetfcn(IF->parent, __i, __ATPi_offset) :sv->__ATPi_local;
    GlobalData_t Ki = __Ki_SVgetfcn ? __Ki_SVgetfcn(IF->parent, __i, __Ki_offset) :sv->__Ki_local;
    GlobalData_t Nai = __Nai_SVgetfcn ? __Nai_SVgetfcn(IF->parent, __i, __Nai_offset) :sv->__Nai_local;
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Ke_row[NROWS_Ke];
    LUT_interpRow(&IF->tables[Ke_TAB], Ke, __i, Ke_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t Ek = (RTF*(log((Ke/Ki))));
    GlobalData_t H = (1.3+((0.74*beta)*(exp((-0.09*ADPi)))));
    GlobalData_t KhMg = ((0.65/(sqrt((Ke+5.))))*(exp((((-2.*delta_Mg)/RTF)*V))));
    GlobalData_t KhNa = (25.9*(exp((((-delta_Na)/RTF)*V))));
    GlobalData_t Km = (alpha*(35.8+(17.9*(pow(ADPi,0.256)))));
    GlobalData_t fATP = (1./(1.+(pow((ATPi/(Km*0.001)),H))));
    GlobalData_t fM = (1./(1.+(Mgi/KhMg)));
    GlobalData_t fN = (1./(1.+((Nai/KhNa)*(Nai/KhNa))));
    GlobalData_t IKATP = (((((((sigma*po)*Ke_row[gamma_0_idx])*fM)*fN)*fT)*fATP)*(V-(Ek)));
    Iion = (Iion+IKATP);
    Ke = (Ke+(((((IKATP*BETA)/F)*1.e-9)*p->GAMMA0)/(1.-(p->GAMMA0))));
    Ki = (Ki-((((IKATP*BETA)/F)*1.e-9)));
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    Ke = Ke;
    Ki = Ki;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Ke_ext[__i] = Ke;
    V_ext[__i] = V;
    if( __ADPi_SVputfcn ) 
    	__ADPi_SVputfcn(IF->parent, __i, __ADPi_offset, ADPi);
    else
    	sv->__ADPi_local=ADPi;
    if( __ATPi_SVputfcn ) 
    	__ATPi_SVputfcn(IF->parent, __i, __ATPi_offset, ATPi);
    else
    	sv->__ATPi_local=ATPi;
    if( __Ki_SVputfcn ) 
    	__Ki_SVputfcn(IF->parent, __i, __Ki_offset, Ki);
    else
    	sv->__Ki_local=Ki;
    if( __Nai_SVputfcn ) 
    	__Nai_SVputfcn(IF->parent, __i, __Nai_offset, Nai);
    else
    	sv->__Nai_local=Nai;

  }


}


void trace_IKATP_Ferrero(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("IKATP_Ferrero_trace_header.txt","wt");
    fprintf(theader->fd,
        "fATP\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IKATP_Ferrero_Params *p  = (IKATP_Ferrero_Params *)IF->params;

  IKATP_Ferrero_state *sv_base = (IKATP_Ferrero_state *)IF->sv_tab.y;
  IKATP_Ferrero_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Ke_ext = impdata[Ke];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  
  int __ADPi_sizeof;
  int __ADPi_offset;
  SVgetfcn __ADPi_SVgetfcn = get_sv_offset( IF->parent->type, "ADPi", &__ADPi_offset, &__ADPi_sizeof );
  SVputfcn __ADPi_SVputfcn = __ADPi_SVgetfcn ? getPutSV(__ADPi_SVgetfcn) : NULL;
  
  int __ATPi_sizeof;
  int __ATPi_offset;
  SVgetfcn __ATPi_SVgetfcn = get_sv_offset( IF->parent->type, "ATPi", &__ATPi_offset, &__ATPi_sizeof );
  SVputfcn __ATPi_SVputfcn = __ATPi_SVgetfcn ? getPutSV(__ATPi_SVgetfcn) : NULL;
  
  int __Ki_sizeof;
  int __Ki_offset;
  SVgetfcn __Ki_SVgetfcn = get_sv_offset( IF->parent->type, "Ki", &__Ki_offset, &__Ki_sizeof );
  SVputfcn __Ki_SVputfcn = __Ki_SVgetfcn ? getPutSV(__Ki_SVgetfcn) : NULL;
  
  int __Nai_sizeof;
  int __Nai_offset;
  SVgetfcn __Nai_SVgetfcn = get_sv_offset( IF->parent->type, "Nai", &__Nai_offset, &__Nai_sizeof );
  SVputfcn __Nai_SVputfcn = __Nai_SVgetfcn ? getPutSV(__Nai_SVgetfcn) : NULL;
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t Ke = Ke_ext[__i];
  GlobalData_t V = V_ext[__i];
  GlobalData_t ADPi = __ADPi_SVgetfcn ? __ADPi_SVgetfcn(IF->parent, __i, __ADPi_offset) :sv->__ADPi_local;
  GlobalData_t ATPi = __ATPi_SVgetfcn ? __ATPi_SVgetfcn(IF->parent, __i, __ATPi_offset) :sv->__ATPi_local;
  GlobalData_t Ki = __Ki_SVgetfcn ? __Ki_SVgetfcn(IF->parent, __i, __Ki_offset) :sv->__Ki_local;
  GlobalData_t Nai = __Nai_SVgetfcn ? __Nai_SVgetfcn(IF->parent, __i, __Nai_offset) :sv->__Nai_local;
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t H = (1.3+((0.74*beta)*(exp((-0.09*ADPi)))));
  GlobalData_t Km = (alpha*(35.8+(17.9*(pow(ADPi,0.256)))));
  GlobalData_t fATP = (1./(1.+(pow((ATPi/(Km*0.001)),H))));
  //Output the desired variables
  fprintf(file, "%4.12f\t", fATP);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        