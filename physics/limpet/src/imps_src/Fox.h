// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Jeffrey J. Fox, Jennifer L. McHarg, and Robert F. Gilmour Jr
*  Year: 2002
*  Title: Ionic mechanism of electrical alternans
*  Journal: American Journal of Physiology: Heart and Circulatory Physiology, 282, H516-H530
*  DOI: 10.1152/ajpheart.00612.2001
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __FOX_H__
#define __FOX_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Fox_REQDAT Vm_DATA_FLAG
#define Fox_MODDAT Iion_DATA_FLAG

struct Fox_Params {
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t PCa;

};

struct Fox_state {
    GlobalData_t Ca_SR;
    GlobalData_t Cai;
    Gatetype X_kr;
    Gatetype X_ks;
    Gatetype X_to;
    Gatetype Y_to;
    Gatetype d;
    Gatetype f;
    Gatetype f_Ca;
    Gatetype h;
    Gatetype j;
    Gatetype m;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Fox(ION_IF *);
void construct_tables_Fox(ION_IF *);
void destroy_Fox(ION_IF *);
void initialize_sv_Fox(ION_IF *, GlobalData_t**);
void compute_Fox(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
