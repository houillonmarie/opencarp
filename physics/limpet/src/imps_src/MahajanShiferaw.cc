// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Aman Mahajan, Yohannes Shiferaw, Daisuke Sato, Ali Baher, Riccardo Olcese, Lai-Hua Xie, Ming-Jim Yang, Peng-Sheng Chen, Juan G. Restrepo, Alain Karma, Alan Garfinkel, Zhilin Qu, James N. Weiss
*  Year: 2008
*  Title: A Rabbit Ventricular Action Potential Model Replicating Cardiac Dynamics at Rapid Heart Rates
*  Journal: Biophysical Journal, 94(2), 392-410
*  DOI: 10.1529/biophysj.106.98160
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "MahajanShiferaw.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_MahajanShiferaw(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_MahajanShiferaw( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define BASE_Cao (GlobalData_t)(1.8)
#define BASE_Ko (GlobalData_t)(5.4)
#define BASE_Nai (GlobalData_t)(14.01807252)
#define BASE_Nao (GlobalData_t)(136.0)
#define Cai_init (GlobalData_t)(0.3863687451)
#define Cm (GlobalData_t)(3.1e-4)
#define F (GlobalData_t)(96485.0)
#define Gleak (GlobalData_t)(0.00002069)
#define Grel (GlobalData_t)(26841.8)
#define Ki_init (GlobalData_t)(0.0)
#define R (GlobalData_t)(8314.0)
#define V_init (GlobalData_t)(-86.79545769)
#define av (GlobalData_t)(11.3)
#define ax (GlobalData_t)(0.3576)
#define ay (GlobalData_t)(0.05)
#define bcal (GlobalData_t)(24.0)
#define bmem (GlobalData_t)(15.0)
#define bsar (GlobalData_t)(42.0)
#define btrop (GlobalData_t)(70.0)
#define c1_init (GlobalData_t)(1.925580885e-05)
#define c2_init (GlobalData_t)(0.9535940241)
#define cat (GlobalData_t)(3.0)
#define cj_init (GlobalData_t)(107.0388739)
#define cjp_init (GlobalData_t)(95.76256179)
#define cp_init (GlobalData_t)(1.682601371)
#define cpt (GlobalData_t)(6.09365)
#define cs_init (GlobalData_t)(0.3205609256)
#define cstar (GlobalData_t)(90.0)
#define g (GlobalData_t)(2.58079)
#define gdyad (GlobalData_t)(9000.0)
#define hh (GlobalData_t)(1.0)
#define ibarnak (GlobalData_t)(1.5)
#define kdna (GlobalData_t)(0.3)
#define kmem (GlobalData_t)(0.3)
#define kmko (GlobalData_t)(1.5)
#define kmnai (GlobalData_t)(12.0)
#define ksar (GlobalData_t)(13.0)
#define mCai (GlobalData_t)(0.0036)
#define mCao (GlobalData_t)(1.3)
#define mnai (GlobalData_t)(12.3)
#define mnao (GlobalData_t)(87.5)
#define pca (GlobalData_t)(0.000540)
#define prnak (GlobalData_t)(0.018330)
#define r1 (GlobalData_t)(0.30)
#define r2 (GlobalData_t)(3.0)
#define s1t (GlobalData_t)(0.00195)
#define s6 (GlobalData_t)(8.0)
#define srkd (GlobalData_t)(0.6)
#define srmax (GlobalData_t)(47.0)
#define sx (GlobalData_t)(3.0)
#define sy (GlobalData_t)(4.0)
#define syr (GlobalData_t)(11.32)
#define tau3 (GlobalData_t)(3.0)
#define taua (GlobalData_t)(100.0)
#define taupo (GlobalData_t)(1.0)
#define taups (GlobalData_t)(0.5)
#define taur (GlobalData_t)(30.0)
#define taus (GlobalData_t)(4.0)
#define tca (GlobalData_t)(78.0329)
#define tropi_init (GlobalData_t)(29.64807803)
#define trops_init (GlobalData_t)(26.37726416)
#define vss2vcell (GlobalData_t)(50.0)
#define vth (GlobalData_t)(0.0)
#define vup (GlobalData_t)(0.4)
#define vx (GlobalData_t)(-40.0)
#define vy (GlobalData_t)(-40.0)
#define vyr (GlobalData_t)(-40.0)
#define wca (GlobalData_t)(8.0)
#define xi1ba_init (GlobalData_t)(3.629261123e-05)
#define xi1ca_init (GlobalData_t)(0.007052299702)
#define xi2ba_init (GlobalData_t)(0.01613268649)
#define xi2ca_init (GlobalData_t)(0.02316349806)
#define xir_init (GlobalData_t)(0.006462569526)
#define xk1t (GlobalData_t)(0.00413)
#define xk2 (GlobalData_t)(1.03615e-4)
#define xk2t (GlobalData_t)(0.00224)
#define xkcal (GlobalData_t)(7.0)
#define xkoff (GlobalData_t)(0.0196)
#define xkon (GlobalData_t)(0.0327)
#define xup (GlobalData_t)(0.50)
#define Nai_init (GlobalData_t)(BASE_Nai)
#define bv (GlobalData_t)(((cstar-(50.))-((av*cstar))))
#define mNai3 (GlobalData_t)(((mnai*mnai)*mnai))
#define mNao3 (GlobalData_t)(((mnao*mnao)*mnao))
#define s2t (GlobalData_t)(((s1t*(r1/r2))*(xk2t/xk1t)))
#define s_s2 (GlobalData_t)(((r1/r2)*xk2))
#define wca_m2 (GlobalData_t)((2.0*wca))



void initialize_params_MahajanShiferaw( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  MahajanShiferaw_Params *p = (MahajanShiferaw_Params *)IF->params;

  // Compute the regional constants
  {
    p->BASE_Ki = 140.0;
    p->Cao = BASE_Cao;
    p->GCaL = 182.0;
    p->GK1 = 0.30;
    p->GKs = 0.32;
    p->GNa = 12.;
    p->GNaCa = 0.84;
    p->Gtof = 0.11;
    p->Gtos = 0.04;
    p->Ko = BASE_Ko;
    p->Nao = BASE_Nao;
    p->T = 308.0;
    region->a_cap = Cm;
    p->constant_Ki = 0.;
    region->fr_myo = 1.0;
    region->v_cell = 2.58e-5;
    p->gss = (sqrt((p->Ko/BASE_Ko)));
    region->sl_i2c = (region->a_cap/(region->v_cell*F));
  }
  // Compute the regional initialization
  {
    GlobalData_t Cai = Cai_init;
    p->GKr = 0.0125;
    p->Nai = Nai_init;
    p->factorGKs = ((p->GKs*0.433)*(1.+(0.8/(1.+(((0.5/Cai)*(0.5/Cai))*(0.5/Cai))))));
  }
  IF->type->trace = trace_MahajanShiferaw;

}


// Define the parameters for the lookup tables
enum Tables {
  Cai_TAB,
  Kii_TAB,
  Nai_TAB,
  V_TAB,
  VEK_TAB,
  cj_TAB,
  cp_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum Cai_TableIndex {
  beta_ci_idx,
  i_up_idx,
  NROWS_Cai
};

enum Kii_TableIndex {
  EK_idx,
  NROWS_Kii
};

enum Nai_TableIndex {
  ENa_idx,
  Nai3_idx,
  yz3_idx,
  NROWS_Nai
};

enum V_TableIndex {
  a_b_idx,
  alpha_idx,
  beta_idx,
  fnak_idx,
  h_rush_larsen_A_idx,
  h_rush_larsen_B_idx,
  j_rush_larsen_A_idx,
  j_rush_larsen_B_idx,
  m_rush_larsen_A_idx,
  m_rush_larsen_B_idx,
  poix_idx,
  prv_idx,
  recov_idx,
  rg_idx,
  rs_inf_idx,
  rxa1_idx,
  rxa2_idx,
  sparkV_idx,
  ssr_idx,
  xk3_idx,
  xk3t_idx,
  xk4t_idx,
  xk5t_idx,
  xk6t_idx,
  xr_rush_larsen_A_idx,
  xr_rush_larsen_B_idx,
  xs1_rush_larsen_A_idx,
  xs1_rush_larsen_B_idx,
  xs2_rush_larsen_A_idx,
  xs2_rush_larsen_B_idx,
  xtof_rush_larsen_A_idx,
  xtof_rush_larsen_B_idx,
  xtos_rush_larsen_A_idx,
  xtos_rush_larsen_B_idx,
  ytof_rush_larsen_A_idx,
  ytof_rush_larsen_B_idx,
  ytos_rush_larsen_A_idx,
  ytos_rush_larsen_B_idx,
  zw3a_idx,
  zw3b_idx,
  zw4_idx,
  NROWS_V
};

enum VEK_TableIndex {
  IK1_idx,
  NROWS_VEK
};

enum cj_TableIndex {
  xileak_temp_idx,
  NROWS_cj
};

enum cp_TableIndex {
  fca_idx,
  s1_idx,
  s2_idx,
  tau_ca_idx,
  xk1_idx,
  NROWS_cp
};



void construct_tables_MahajanShiferaw( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  MahajanShiferaw_Params *p = (MahajanShiferaw_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double Nao3 = ((p->Nao*p->Nao)*p->Nao);
  double RT_F = ((R*p->T)/F);
  double i2conc = ((Cm/F)/region->v_cell);
  double sigma = (((exp((p->Nao/67.3)))-(1.0))/7.0);
  double vsr = (0.06*region->v_cell);
  double vss = (0.02*region->v_cell);
  double F_RT = (1./RT_F);
  double conc2i = (1./i2conc);
  double vcell_vsr_rat = (region->v_cell/vsr);
  double f = ((((4.0*pca)*F)*1e-3)*F_RT);
  
  // Create the Cai lookup table
  LUT* Cai_tab = &IF->tables[Cai_TAB];
  LUT_alloc(Cai_tab, NROWS_Cai, 1e-3, 20, 1e-3, "MahajanShiferaw Cai");
  for (int __i=Cai_tab->mn_ind; __i<=Cai_tab->mx_ind; __i++) {
    double Cai = Cai_tab->res*__i;
    LUT_data_t* Cai_row = Cai_tab->tab[__i];
    double bix = ((bcal*xkcal)/((xkcal+Cai)*(xkcal+Cai)));
    Cai_row[i_up_idx] = (((vup*Cai)*Cai)/((Cai*Cai)+(xup*xup)));
    double memix = ((bmem*kmem)/((kmem+Cai)*(kmem+Cai)));
    double sarix = ((bsar*ksar)/((ksar+Cai)*(ksar+Cai)));
    double six = ((srmax*srkd)/((srkd+Cai)*(srkd+Cai)));
    Cai_row[beta_ci_idx] = (1.0/((((1.0+bix)+six)+memix)+sarix));
  }
  check_LUT(Cai_tab);
  
  
  // Create the Kii lookup table
  LUT* Kii_tab = &IF->tables[Kii_TAB];
  LUT_alloc(Kii_tab, NROWS_Kii, 1e-3, 1000, 1e-3, "MahajanShiferaw Kii");
  for (int __i=Kii_tab->mn_ind; __i<=Kii_tab->mx_ind; __i++) {
    double Kii = Kii_tab->res*__i;
    LUT_data_t* Kii_row = Kii_tab->tab[__i];
    Kii_row[EK_idx] = (RT_F*(log((p->Ko/Kii))));
  }
  check_LUT(Kii_tab);
  
  
  // Create the Nai lookup table
  LUT* Nai_tab = &IF->tables[Nai_TAB];
  LUT_alloc(Nai_tab, NROWS_Nai, 0.01, 60, 0.01, "MahajanShiferaw Nai");
  for (int __i=Nai_tab->mn_ind; __i<=Nai_tab->mx_ind; __i++) {
    double Nai = Nai_tab->res*__i;
    LUT_data_t* Nai_row = Nai_tab->tab[__i];
    Nai_row[ENa_idx] = (RT_F*(log((p->Nao/Nai))));
    Nai_row[Nai3_idx] = ((Nai*Nai)*Nai);
    Nai_row[yz3_idx] = ((mCai*Nao3)*(1.0+(Nai_row[Nai3_idx]/mNai3)));
  }
  check_LUT(Nai_tab);
  
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -800, 800, 0.05, "MahajanShiferaw V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    V_row[a_b_idx] = (exp((V/8.0)));
    double a_h = ((V<-40.0) ? (0.135*(exp(((80.0+V)/-6.8)))) : 0.0);
    double a_j = ((V<-40.0) ? (((-127140.0*(exp((0.2444*V))))-((0.00003474*(exp((-0.04391*V))))))*((V+37.78)/(1.0+(exp((0.311*(V+79.23))))))) : 0.0);
    double a_m = ((V==-47.13) ? 3.2 : ((0.32*(V+47.13))/(1.0-((exp((-0.1*(V+47.13))))))));
    double b_h = ((V<-40.0) ? ((3.56*(exp((0.079*V))))+(310000.0*(exp((0.35*V))))) : (1.0/(0.13*(1.0+(exp(((V+10.66)/-11.1)))))));
    double b_j = ((V<-40.0) ? ((0.1212*(exp((-0.01052*V))))/(1.0+(exp((-0.1378*(V+40.14)))))) : ((0.3*(exp((-0.0000002535*V))))/(1.0+(exp((-0.1*(V+32.0)))))));
    double b_m = (0.08*(exp(((-V)/11.0))));
    V_row[fnak_idx] = (1.0/((1.+(0.1245*(exp(((-0.1*V)*F_RT)))))+((0.0365*sigma)*(exp(((-V)*F_RT))))));
    double krv1 = ((V!=-7.0) ? ((0.00138*(V+7.0))/(1.-((exp((-0.123*(V+7.0))))))) : (0.00138/0.123));
    double krv2 = ((V!=-10.0) ? ((0.00061*(V+10.0))/((exp((0.145*(V+10.0))))-(1.0))) : (0.00061/0.145));
    double poi = (1.0/(1.0+(exp(((-(V-(vx)))/sx)))));
    double poinf = (1.0/(1.0+(exp(((-(V-(vth)))/s6)))));
    V_row[poix_idx] = (1.0/(1.0+(exp(((-(V-(vyr)))/syr)))));
    V_row[prv_idx] = (1.0-((1.0/(1.0+(exp(((-(V-(vy)))/sy)))))));
    V_row[recov_idx] = (10.0+(4954.0*(exp((V/15.6)))));
    V_row[rg_idx] = (1.0/(1.0+(exp(((V+33.0)/22.4)))));
    double rt1 = ((-(V+3.0))/15.0);
    double rt2 = ((V+33.5)/10.0);
    double rt3 = ((V+60.0)/10.0);
    double rt4 = ((-(V/30.0))*(V/30.0));
    double rt5 = ((V+33.5)/10.0);
    V_row[rxa1_idx] = ((V!=0.0) ? (((V*f)*(exp(((V*2.0)*F_RT))))/(expm1(((V*2.0)*F_RT)))) : (f/(2.0*F_RT)));
    V_row[rxa2_idx] = ((V!=0.0) ? (((((-V)*f)*0.341)*p->Cao)/(expm1(((V*2.0)*F_RT)))) : ((((-f)*0.341)*p->Cao)/(2.0*F_RT)));
    V_row[sparkV_idx] = ((exp(((-ay)*(V+30.0))))/(1.+(exp(((-ay)*(V+30.))))));
    V_row[ssr_idx] = ((exp(((-ax)*(V+30.0))))/(1.+(exp(((-ax)*(V+30.))))));
    double tau_xs1 = ((V!=-30.0) ? (1./(((7.19e-5*(V+30.))/(1.-((exp((-0.148*(V+30.)))))))+((1.31e-4*(V+30.))/(-1.+(exp((0.0687*(V+30.)))))))) : (1./((7.19e-5/0.148)+(1.31e-4/0.0687))));
    double xr_inf = (1.0/(1.0+(exp(((-(V+50.0))/7.5)))));
    double xs1_inf = (1./(1.+(exp(((-(V-(1.5)))/16.7)))));
    V_row[zw3a_idx] = (exp(((V*0.35)*F_RT)));
    V_row[zw3b_idx] = (exp(((V*(0.35-(1.0)))*F_RT)));
    V_row[zw4_idx] = (1.0+(0.2*(exp(((V*(0.35-(1.0)))*F_RT)))));
    V_row[alpha_idx] = (poinf/taupo);
    V_row[beta_idx] = ((1.0-(poinf))/taupo);
    V_row[h_rush_larsen_A_idx] = (((-a_h)/(a_h+b_h))*(expm1(((-dt)*(a_h+b_h)))));
    V_row[h_rush_larsen_B_idx] = (exp(((-dt)*(a_h+b_h))));
    V_row[j_rush_larsen_A_idx] = (((-a_j)/(a_j+b_j))*(expm1(((-dt)*(a_j+b_j)))));
    V_row[j_rush_larsen_B_idx] = (exp(((-dt)*(a_j+b_j))));
    V_row[m_rush_larsen_A_idx] = (((-a_m)/(a_m+b_m))*(expm1(((-dt)*(a_m+b_m)))));
    V_row[m_rush_larsen_B_idx] = (exp(((-dt)*(a_m+b_m))));
    V_row[rs_inf_idx] = (1.0/(1.0+(exp(rt2))));
    double tau_xr = (1.0/(krv1+krv2));
    double tau_xs2 = (4.*tau_xs1);
    double tau_xtof = ((3.5*(exp(rt4)))+1.5);
    double tau_xtos = ((9.0/(1.0+(exp((-rt1)))))+0.5);
    double tau_ytof = ((20.0/(1.0+(exp(rt5))))+20.0);
    double tau_ytos = ((3000.0/(1.0+(exp(rt3))))+30.0);
    double tauba = (((V_row[recov_idx]-(450.0))*V_row[prv_idx])+450.0);
    V_row[xk3_idx] = ((1.0-(poi))/tau3);
    V_row[xs1_rush_larsen_B_idx] = (exp(((-dt)/tau_xs1)));
    double xs1_rush_larsen_C = (expm1(((-dt)/tau_xs1)));
    double xs2_inf = xs1_inf;
    double xtos_inf = (1.0/(1.0+(exp(rt1))));
    double ytos_inf = (1.0/(1.0+(exp(rt2))));
    V_row[xk3t_idx] = V_row[xk3_idx];
    V_row[xk5t_idx] = ((1.0-(V_row[poix_idx]))/tauba);
    V_row[xk6t_idx] = (V_row[poix_idx]/tauba);
    V_row[xr_rush_larsen_B_idx] = (exp(((-dt)/tau_xr)));
    double xr_rush_larsen_C = (expm1(((-dt)/tau_xr)));
    V_row[xs1_rush_larsen_A_idx] = ((-xs1_inf)*xs1_rush_larsen_C);
    V_row[xs2_rush_larsen_B_idx] = (exp(((-dt)/tau_xs2)));
    double xs2_rush_larsen_C = (expm1(((-dt)/tau_xs2)));
    double xtof_inf = xtos_inf;
    V_row[xtof_rush_larsen_B_idx] = (exp(((-dt)/tau_xtof)));
    double xtof_rush_larsen_C = (expm1(((-dt)/tau_xtof)));
    V_row[xtos_rush_larsen_B_idx] = (exp(((-dt)/tau_xtos)));
    double xtos_rush_larsen_C = (expm1(((-dt)/tau_xtos)));
    double ytof_inf = ytos_inf;
    V_row[ytof_rush_larsen_B_idx] = (exp(((-dt)/tau_ytof)));
    double ytof_rush_larsen_C = (expm1(((-dt)/tau_ytof)));
    V_row[ytos_rush_larsen_B_idx] = (exp(((-dt)/tau_ytos)));
    double ytos_rush_larsen_C = (expm1(((-dt)/tau_ytos)));
    V_row[xk4t_idx] = (((V_row[xk3t_idx]*V_row[a_b_idx])*(xk1t/xk2t))*(V_row[xk5t_idx]/V_row[xk6t_idx]));
    V_row[xr_rush_larsen_A_idx] = ((-xr_inf)*xr_rush_larsen_C);
    V_row[xs2_rush_larsen_A_idx] = ((-xs2_inf)*xs2_rush_larsen_C);
    V_row[xtof_rush_larsen_A_idx] = ((-xtof_inf)*xtof_rush_larsen_C);
    V_row[xtos_rush_larsen_A_idx] = ((-xtos_inf)*xtos_rush_larsen_C);
    V_row[ytof_rush_larsen_A_idx] = ((-ytof_inf)*ytof_rush_larsen_C);
    V_row[ytos_rush_larsen_A_idx] = ((-ytos_inf)*ytos_rush_larsen_C);
  }
  check_LUT(V_tab);
  
  
  // Create the VEK lookup table
  LUT* VEK_tab = &IF->tables[VEK_TAB];
  LUT_alloc(VEK_tab, NROWS_VEK, -800, 800, 0.05, "MahajanShiferaw VEK");
  for (int __i=VEK_tab->mn_ind; __i<=VEK_tab->mx_ind; __i++) {
    double VEK = VEK_tab->res*__i;
    LUT_data_t* VEK_row = VEK_tab->tab[__i];
    double aa_K1 = (1.02/(1.0+(exp((0.2385*(VEK-(59.215)))))));
    double bb_K1 = (((0.49124*(exp((0.08032*(VEK+5.476)))))+(exp((0.061750*(VEK-(594.31))))))/(1.0+(exp((-0.5143*(VEK+4.753))))));
    double K1_inf = (aa_K1/(aa_K1+bb_K1));
    VEK_row[IK1_idx] = (((p->GK1*p->gss)*K1_inf)*VEK);
  }
  check_LUT(VEK_tab);
  
  
  // Create the cj lookup table
  LUT* cj_tab = &IF->tables[cj_TAB];
  LUT_alloc(cj_tab, NROWS_cj, 1e-3, 1000, 1e-3, "MahajanShiferaw cj");
  for (int __i=cj_tab->mn_ind; __i<=cj_tab->mx_ind; __i++) {
    double cj = cj_tab->res*__i;
    LUT_data_t* cj_row = cj_tab->tab[__i];
    cj_row[xileak_temp_idx] = (Gleak*((cj*cj)/((cj*cj)+(50.*50.))));
  }
  check_LUT(cj_tab);
  
  
  // Create the cp lookup table
  LUT* cp_tab = &IF->tables[cp_TAB];
  LUT_alloc(cp_tab, NROWS_cp, 1e-3, 10000, 1e-3, "MahajanShiferaw cp");
  for (int __i=cp_tab->mn_ind; __i<=cp_tab->mx_ind; __i++) {
    double cp = cp_tab->res*__i;
    LUT_data_t* cp_row = cp_tab->tab[__i];
    cp_row[fca_idx] = (1.0/(1.0+(((cat/cp)*(cat/cp))*(cat/cp))));
    cp_row[tau_ca_idx] = (tca/(1.0+((((cp/cpt)*(cp/cpt))*(cp/cpt))*(cp/cpt))));
    cp_row[s1_idx] = (0.0182688*cp_row[fca_idx]);
    cp_row[xk1_idx] = (0.024168*cp_row[fca_idx]);
    cp_row[s2_idx] = ((cp_row[s1_idx]*s_s2)/cp_row[xk1_idx]);
  }
  check_LUT(cp_tab);
  

}



void    initialize_sv_MahajanShiferaw( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  MahajanShiferaw_Params *p = (MahajanShiferaw_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(MahajanShiferaw_state) );
  MahajanShiferaw_state *sv_base = (MahajanShiferaw_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double Nao3 = ((p->Nao*p->Nao)*p->Nao);
  double RT_F = ((R*p->T)/F);
  double i2conc = ((Cm/F)/region->v_cell);
  double sigma = (((exp((p->Nao/67.3)))-(1.0))/7.0);
  double vsr = (0.06*region->v_cell);
  double vss = (0.02*region->v_cell);
  double F_RT = (1./RT_F);
  double conc2i = (1./i2conc);
  double vcell_vsr_rat = (region->v_cell/vsr);
  double f = ((((4.0*pca)*F)*1e-3)*F_RT);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    MahajanShiferaw_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    sv->GKr = p->GKr;
    sv->Nai = p->Nai;
    sv->factorGKs = p->factorGKs;
    // Initialize the rest of the nodal variables
    sv->Cai = Cai_init;
    double ENa = (RT_F*(log((p->Nao/sv->Nai))));
    sv->Ki = Ki_init;
    double Nai3 = ((sv->Nai*sv->Nai)*sv->Nai);
    V = V_init;
    sv->c1 = c1_init;
    sv->c2 = c2_init;
    sv->cj = cj_init;
    sv->cjp = cjp_init;
    sv->cp = cp_init;
    sv->cs = cs_init;
    sv->tropi = tropi_init;
    sv->trops = trops_init;
    sv->xi1ba = xi1ba_init;
    sv->xi1ca = xi1ca_init;
    sv->xi2ba = xi2ba_init;
    sv->xi2ca = xi2ca_init;
    sv->xir = xir_init;
    double GNaca_aloss = (p->GNaCa/(1.0+(((kdna/sv->cs)*(kdna/sv->cs))*(kdna/sv->cs))));
    double Kii = (sv->Ki+p->BASE_Ki);
    double a_h = ((V<-40.0) ? (0.135*(exp(((80.0+V)/-6.8)))) : 0.0);
    double a_j = ((V<-40.0) ? (((-127140.0*(exp((0.2444*V))))-((0.00003474*(exp((-0.04391*V))))))*((V+37.78)/(1.0+(exp((0.311*(V+79.23))))))) : 0.0);
    double a_m = ((V==-47.13) ? 3.2 : ((0.32*(V+47.13))/(1.0-((exp((-0.1*(V+47.13))))))));
    double b_h = ((V<-40.0) ? ((3.56*(exp((0.079*V))))+(310000.0*(exp((0.35*V))))) : (1.0/(0.13*(1.0+(exp(((V+10.66)/-11.1)))))));
    double b_j = ((V<-40.0) ? ((0.1212*(exp((-0.01052*V))))/(1.0+(exp((-0.1378*(V+40.14)))))) : ((0.3*(exp((-0.0000002535*V))))/(1.0+(exp((-0.1*(V+32.0)))))));
    double b_m = (0.08*(exp(((-V)/11.0))));
    double csm = (sv->cs/1000.);
    double fnak = (1.0/((1.+(0.1245*(exp(((-0.1*V)*F_RT)))))+((0.0365*sigma)*(exp(((-V)*F_RT))))));
    double po = ((((((1.0-(sv->xi1ca))-(sv->xi2ca))-(sv->xi1ba))-(sv->xi2ba))-(sv->c1))-(sv->c2));
    double rg = (1.0/(1.0+(exp(((V+33.0)/22.4)))));
    double rt1 = ((-(V+3.0))/15.0);
    double rt2 = ((V+33.5)/10.0);
    double rxa1 = ((V!=0.0) ? (((V*f)*(exp(((V*2.0)*F_RT))))/(expm1(((V*2.0)*F_RT)))) : (f/(2.0*F_RT)));
    double rxa2 = ((V!=0.0) ? (((((-V)*f)*0.341)*p->Cao)/(expm1(((V*2.0)*F_RT)))) : ((((-f)*0.341)*p->Cao)/(2.0*F_RT)));
    double xr_inf = (1.0/(1.0+(exp(((-(V+50.0))/7.5)))));
    double xs1_inf = (1./(1.+(exp(((-(V-(1.5)))/16.7)))));
    double yz3 = ((mCai*Nao3)*(1.0+(Nai3/mNai3)));
    double zw3a = (exp(((V*0.35)*F_RT)));
    double zw3b = (exp(((V*(0.35-(1.0)))*F_RT)));
    double zw4 = (1.0+(0.2*(exp(((V*(0.35-(1.0)))*F_RT)))));
    double EK = (RT_F*(log((p->Ko/Kii))));
    double EKs = (RT_F*(log(((p->Ko+(prnak*p->Nao))/(Kii+(prnak*sv->Nai))))));
    double INaK = (((((1.0/(1.0+(kmnai/sv->Nai)))*ibarnak)*fnak)*p->Ko)/(p->Ko+kmko));
    double h_init = (a_h/(a_h+b_h));
    double j_init = (a_j/(a_j+b_j));
    double m_init = (a_m/(a_m+b_m));
    double rs_inf = (1.0/(1.0+(exp(rt2))));
    double rxa = ((rxa1*csm)+rxa2);
    double xr_init = xr_inf;
    double xs1_init = xs1_inf;
    double xs2_inf = xs1_inf;
    double xtos_inf = (1.0/(1.0+(exp(rt1))));
    double ytos_inf = (1.0/(1.0+(exp(rt2))));
    double yz1 = ((mCao*Nai3)+(mNao3*csm));
    double yz2 = ((mNai3*p->Cao)*(1.0+(csm/mCai)));
    double yz4 = ((Nai3*p->Cao)+(Nao3*csm));
    double zw3 = (((Nai3*p->Cao)*zw3a)-(((Nao3*csm)*zw3b)));
    double VEK = (V-(EK));
    sv->h = h_init;
    sv->j = j_init;
    sv->m = m_init;
    double xicaq = ((p->GCaL*po)*rxa);
    sv->xr = xr_init;
    sv->xs1 = xs1_init;
    double xs2_init = xs2_inf;
    double xtof_inf = xtos_inf;
    double xtos_init = xtos_inf;
    double ytof_inf = ytos_inf;
    double ytos_init = ytos_inf;
    double zw8 = (((yz1+yz2)+yz3)+yz4);
    double ICa = (wca_m2*xicaq);
    double IKr = ((((sv->GKr*sv->xr)*rg)*p->gss)*(V-(EK)));
    double INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(ENa)));
    double aa_K1 = (1.02/(1.0+(exp((0.2385*(VEK-(59.215)))))));
    double bb_K1 = (((0.49124*(exp((0.08032*(VEK+5.476)))))+(exp((0.061750*(VEK-(594.31))))))/(1.0+(exp((-0.5143*(VEK+4.753))))));
    double xINaCaq = ((GNaca_aloss*zw3)/(zw4*zw8));
    sv->xs2 = xs2_init;
    double xtof_init = xtof_inf;
    sv->xtos = xtos_init;
    double ytof_init = ytof_inf;
    sv->ytos = ytos_init;
    double IKs = (((sv->factorGKs*sv->xs1)*sv->xs2)*(V-(EKs)));
    double INaCa = (wca*xINaCaq);
    double Itos = (((p->Gtos*sv->xtos)*(sv->ytos+(0.5*rs_inf)))*(V-(EK)));
    double K1_inf = (aa_K1/(aa_K1+bb_K1));
    sv->xtof = xtof_init;
    sv->ytof = ytof_init;
    double IK1 = (((p->GK1*p->gss)*K1_inf)*VEK);
    double Itof = (((p->Gtof*sv->xtof)*sv->ytof)*(V-(EK)));
    double Ito = (Itos+Itof);
    Iion = (((((((INa+IK1)+IKr)+IKs)+Ito)+INaCa)+ICa)+INaK);
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_MahajanShiferaw(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  MahajanShiferaw_Params *p  = (MahajanShiferaw_Params *)IF->params;
  MahajanShiferaw_state *sv_base = (MahajanShiferaw_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Nao3 = ((p->Nao*p->Nao)*p->Nao);
  GlobalData_t RT_F = ((R*p->T)/F);
  GlobalData_t i2conc = ((Cm/F)/region->v_cell);
  GlobalData_t sigma = (((exp((p->Nao/67.3)))-(1.0))/7.0);
  GlobalData_t vsr = (0.06*region->v_cell);
  GlobalData_t vss = (0.02*region->v_cell);
  GlobalData_t F_RT = (1./RT_F);
  GlobalData_t conc2i = (1./i2conc);
  GlobalData_t vcell_vsr_rat = (region->v_cell/vsr);
  GlobalData_t f = ((((4.0*pca)*F)*1e-3)*F_RT);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    MahajanShiferaw_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Cai_row[NROWS_Cai];
    LUT_interpRow(&IF->tables[Cai_TAB], sv->Cai, __i, Cai_row);
    LUT_data_t Nai_row[NROWS_Nai];
    LUT_interpRow(&IF->tables[Nai_TAB], sv->Nai, __i, Nai_row);
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    LUT_data_t cj_row[NROWS_cj];
    LUT_interpRow(&IF->tables[cj_TAB], sv->cj, __i, cj_row);
    LUT_data_t cp_row[NROWS_cp];
    LUT_interpRow(&IF->tables[cp_TAB], sv->cp, __i, cp_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t GNaca_aloss = (p->GNaCa/(1.0+(((kdna/sv->cs)*(kdna/sv->cs))*(kdna/sv->cs))));
    GlobalData_t Kii = (sv->Ki+p->BASE_Ki);
    LUT_data_t Kii_row[NROWS_Kii];
    LUT_interpRow(&IF->tables[Kii_TAB], Kii, __i, Kii_row);
    GlobalData_t csm = (sv->cs/1000.);
    GlobalData_t po = ((((((1.0-(sv->xi1ca))-(sv->xi2ca))-(sv->xi1ba))-(sv->xi2ba))-(sv->c1))-(sv->c2));
    GlobalData_t EKs = (RT_F*(log(((p->Ko+(prnak*p->Nao))/(Kii+(prnak*sv->Nai))))));
    GlobalData_t INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(Nai_row[ENa_idx])));
    GlobalData_t INaK = (((((1.0/(1.0+(kmnai/sv->Nai)))*ibarnak)*V_row[fnak_idx])*p->Ko)/(p->Ko+kmko));
    GlobalData_t rxa = ((V_row[rxa1_idx]*csm)+V_row[rxa2_idx]);
    GlobalData_t yz1 = ((mCao*Nai_row[Nai3_idx])+(mNao3*csm));
    GlobalData_t yz2 = ((mNai3*p->Cao)*(1.0+(csm/mCai)));
    GlobalData_t yz4 = ((Nai_row[Nai3_idx]*p->Cao)+(Nao3*csm));
    GlobalData_t zw3 = (((Nai_row[Nai3_idx]*p->Cao)*V_row[zw3a_idx])-(((Nao3*csm)*V_row[zw3b_idx])));
    GlobalData_t IKr = ((((sv->GKr*sv->xr)*V_row[rg_idx])*p->gss)*(V-(Kii_row[EK_idx])));
    GlobalData_t IKs = (((sv->factorGKs*sv->xs1)*sv->xs2)*(V-(EKs)));
    GlobalData_t Itof = (((p->Gtof*sv->xtof)*sv->ytof)*(V-(Kii_row[EK_idx])));
    GlobalData_t Itos = (((p->Gtos*sv->xtos)*(sv->ytos+(0.5*V_row[rs_inf_idx])))*(V-(Kii_row[EK_idx])));
    GlobalData_t VEK = (V-(Kii_row[EK_idx]));
    LUT_data_t VEK_row[NROWS_VEK];
    LUT_interpRow(&IF->tables[VEK_TAB], VEK, __i, VEK_row);
    GlobalData_t xicaq = ((p->GCaL*po)*rxa);
    GlobalData_t zw8 = (((yz1+yz2)+Nai_row[yz3_idx])+yz4);
    GlobalData_t ICa = (wca_m2*xicaq);
    GlobalData_t Ito = (Itos+Itof);
    GlobalData_t xINaCaq = ((GNaca_aloss*zw3)/(V_row[zw4_idx]*zw8));
    GlobalData_t INaCa = (wca*xINaCaq);
    Iion = (((((((INa+VEK_row[IK1_idx])+IKr)+IKs)+Ito)+INaCa)+ICa)+INaK);
    
    
    //Complete Forward Euler Update
    GlobalData_t Qr_temp = ((sv->cjp<50.) ? 0.0 : (((sv->cjp>=50.0)&&(sv->cjp<cstar)) ? (sv->cjp-(50.0)) : ((av*sv->cjp)+bv)));
    GlobalData_t bpx = ((bcal*xkcal)/((xkcal+sv->cs)*(xkcal+sv->cs)));
    GlobalData_t diff_Nai = ((-i2conc)*((INa+(3.*INaK))+(3.*INaCa)));
    GlobalData_t diff_cjp = ((sv->cj-(sv->cjp))/taua);
    GlobalData_t mempx = ((bmem*kmem)/((kmem+sv->cs)*(kmem+sv->cs)));
    GlobalData_t rxa_inward = ((rxa>0.) ? 0. : 1.);
    GlobalData_t sarpx = ((bsar*ksar)/((ksar+sv->cs)*(ksar+sv->cs)));
    GlobalData_t spx = ((srmax*srkd)/((srkd+sv->cs)*(srkd+sv->cs)));
    GlobalData_t xbi = (((xkon*sv->Cai)*(btrop-(sv->tropi)))-((xkoff*sv->tropi)));
    GlobalData_t xbs = (((xkon*sv->cs)*(btrop-(sv->trops)))-((xkoff*sv->trops)));
    GlobalData_t xdif = ((sv->cs-(sv->Cai))/taus);
    GlobalData_t xicap = (((-po)*gdyad)*rxa);
    GlobalData_t Qr = ((Qr_temp*sv->cj)/cstar);
    GlobalData_t beta_cs = (1.0/((((1.0+bpx)+spx)+mempx)+sarpx));
    GlobalData_t diff_tropi = xbi;
    GlobalData_t diff_trops = xbs;
    GlobalData_t i_leak = (cj_row[xileak_temp_idx]*((sv->cj*vcell_vsr_rat)-(sv->Cai)));
    GlobalData_t spark_rate = (((((-g)*po)*rxa)*V_row[sparkV_idx])*rxa_inward);
    GlobalData_t tauca = (((V_row[recov_idx]-(cp_row[tau_ca_idx]))*V_row[prv_idx])+cp_row[tau_ca_idx]);
    GlobalData_t diff_Cai = (Cai_row[beta_ci_idx]*(((xdif-(Cai_row[i_up_idx]))+i_leak)-(xbi)));
    GlobalData_t diff_c1 = (((((V_row[alpha_idx]*sv->c2)+(xk2*sv->xi1ca))+(xk2t*sv->xi1ba))+(r2*po))-(((((V_row[beta_idx]+r1)+xk1t)+cp_row[xk1_idx])*sv->c1)));
    GlobalData_t diff_cj = (((-sv->xir)+Cai_row[i_up_idx])-(i_leak));
    GlobalData_t diff_cs = (beta_cs*((vss2vcell*(((sv->xir-(xdif))-(xicaq))+xINaCaq))-(xbs)));
    GlobalData_t gain = (((((-po)*Qr)*rxa)*V_row[ssr_idx])*rxa_inward);
    GlobalData_t xk5 = ((1.0-(V_row[poix_idx]))/tauca);
    GlobalData_t xk6 = ((cp_row[fca_idx]*V_row[poix_idx])/tauca);
    GlobalData_t diff_Ki = ((p->constant_Ki==1.) ? 0. : ((-i2conc)*((((IKr+IKs)+VEK_row[IK1_idx])+Ito)-((2.*INaK)))));
    GlobalData_t diff_c2 = ((((V_row[beta_idx]*sv->c1)+(xk5*sv->xi2ca))+(V_row[xk5t_idx]*sv->xi2ba))-((((xk6+V_row[xk6t_idx])+V_row[alpha_idx])*sv->c2)));
    GlobalData_t irT = (taur/(1.-(((taur*diff_cj)/sv->cj))));
    GlobalData_t xirp = (Grel*gain);
    GlobalData_t xk4 = (((V_row[xk3_idx]*V_row[a_b_idx])*(cp_row[xk1_idx]/xk2))*(xk5/xk6));
    GlobalData_t diff_cp = ((xirp+xicap)-(((sv->cp-(sv->cs))/taups)));
    GlobalData_t diff_xi1ba = ((((xk1t*sv->c1)+(V_row[xk4t_idx]*sv->xi2ba))+(s1t*po))-((((V_row[xk3t_idx]+xk2t)+s2t)*sv->xi1ba)));
    GlobalData_t diff_xi1ca = ((((cp_row[xk1_idx]*sv->c1)+(xk4*sv->xi2ca))+(cp_row[s1_idx]*po))-((((V_row[xk3_idx]+xk2)+cp_row[s2_idx])*sv->xi1ca)));
    GlobalData_t diff_xi2ba = (((V_row[xk3t_idx]*sv->xi1ba)+(V_row[xk6t_idx]*sv->c2))-(((V_row[xk5t_idx]+V_row[xk4t_idx])*sv->xi2ba)));
    GlobalData_t diff_xi2ca = (((V_row[xk3_idx]*sv->xi1ca)+(xk6*sv->c2))-(((xk5+xk4)*sv->xi2ca)));
    GlobalData_t diff_xir = ((spark_rate*Qr)-((sv->xir/irT)));
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t c1_new = sv->c1+diff_c1*dt;
    GlobalData_t c2_new = sv->c2+diff_c2*dt;
    GlobalData_t cj_new = sv->cj+diff_cj*dt;
    GlobalData_t cjp_new = sv->cjp+diff_cjp*dt;
    GlobalData_t cp_new = sv->cp+diff_cp*dt;
    GlobalData_t cs_new = sv->cs+diff_cs*dt;
    GlobalData_t tropi_new = sv->tropi+diff_tropi*dt;
    GlobalData_t trops_new = sv->trops+diff_trops*dt;
    GlobalData_t xi1ba_new = sv->xi1ba+diff_xi1ba*dt;
    GlobalData_t xi1ca_new = sv->xi1ca+diff_xi1ca*dt;
    GlobalData_t xi2ba_new = sv->xi2ba+diff_xi2ba*dt;
    GlobalData_t xi2ca_new = sv->xi2ca+diff_xi2ca*dt;
    GlobalData_t xir_new = sv->xir+diff_xir*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t h_rush_larsen_A = V_row[h_rush_larsen_A_idx];
    GlobalData_t h_rush_larsen_B = V_row[h_rush_larsen_B_idx];
    GlobalData_t j_rush_larsen_A = V_row[j_rush_larsen_A_idx];
    GlobalData_t j_rush_larsen_B = V_row[j_rush_larsen_B_idx];
    GlobalData_t m_rush_larsen_A = V_row[m_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_B = V_row[m_rush_larsen_B_idx];
    GlobalData_t xs1_rush_larsen_B = V_row[xs1_rush_larsen_B_idx];
    GlobalData_t xr_rush_larsen_B = V_row[xr_rush_larsen_B_idx];
    GlobalData_t xs1_rush_larsen_A = V_row[xs1_rush_larsen_A_idx];
    GlobalData_t xs2_rush_larsen_B = V_row[xs2_rush_larsen_B_idx];
    GlobalData_t xtof_rush_larsen_B = V_row[xtof_rush_larsen_B_idx];
    GlobalData_t xtos_rush_larsen_B = V_row[xtos_rush_larsen_B_idx];
    GlobalData_t ytof_rush_larsen_B = V_row[ytof_rush_larsen_B_idx];
    GlobalData_t ytos_rush_larsen_B = V_row[ytos_rush_larsen_B_idx];
    GlobalData_t xr_rush_larsen_A = V_row[xr_rush_larsen_A_idx];
    GlobalData_t xs2_rush_larsen_A = V_row[xs2_rush_larsen_A_idx];
    GlobalData_t xtof_rush_larsen_A = V_row[xtof_rush_larsen_A_idx];
    GlobalData_t xtos_rush_larsen_A = V_row[xtos_rush_larsen_A_idx];
    GlobalData_t ytof_rush_larsen_A = V_row[ytof_rush_larsen_A_idx];
    GlobalData_t ytos_rush_larsen_A = V_row[ytos_rush_larsen_A_idx];
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t xr_new = xr_rush_larsen_A+xr_rush_larsen_B*sv->xr;
    GlobalData_t xs1_new = xs1_rush_larsen_A+xs1_rush_larsen_B*sv->xs1;
    GlobalData_t xs2_new = xs2_rush_larsen_A+xs2_rush_larsen_B*sv->xs2;
    GlobalData_t xtof_new = xtof_rush_larsen_A+xtof_rush_larsen_B*sv->xtof;
    GlobalData_t xtos_new = xtos_rush_larsen_A+xtos_rush_larsen_B*sv->xtos;
    GlobalData_t ytof_new = ytof_rush_larsen_A+ytof_rush_larsen_B*sv->ytof;
    GlobalData_t ytos_new = ytos_rush_larsen_A+ytos_rush_larsen_B*sv->ytos;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Cai = Cai_new;
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->c1 = c1_new;
    sv->c2 = c2_new;
    sv->cj = cj_new;
    sv->cjp = cjp_new;
    sv->cp = cp_new;
    sv->cs = cs_new;
    sv->h = h_new;
    sv->j = j_new;
    sv->m = m_new;
    sv->tropi = tropi_new;
    sv->trops = trops_new;
    sv->xi1ba = xi1ba_new;
    sv->xi1ca = xi1ca_new;
    sv->xi2ba = xi2ba_new;
    sv->xi2ca = xi2ca_new;
    sv->xir = xir_new;
    sv->xr = xr_new;
    sv->xs1 = xs1_new;
    sv->xs2 = xs2_new;
    sv->xtof = xtof_new;
    sv->xtos = xtos_new;
    sv->ytof = ytof_new;
    sv->ytos = ytos_new;
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_MahajanShiferaw(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("MahajanShiferaw_trace_header.txt","wt");
    fprintf(theader->fd,
        "ICa\n"
        "IK1\n"
        "IKr\n"
        "IKs\n"
        "INa\n"
        "INaCa\n"
        "INaK\n"
        "Itof\n"
        "Itos\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  MahajanShiferaw_Params *p  = (MahajanShiferaw_Params *)IF->params;

  MahajanShiferaw_state *sv_base = (MahajanShiferaw_state *)IF->sv_tab.y;
  MahajanShiferaw_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Nao3 = ((p->Nao*p->Nao)*p->Nao);
  GlobalData_t RT_F = ((R*p->T)/F);
  GlobalData_t i2conc = ((Cm/F)/region->v_cell);
  GlobalData_t sigma = (((exp((p->Nao/67.3)))-(1.0))/7.0);
  GlobalData_t vsr = (0.06*region->v_cell);
  GlobalData_t vss = (0.02*region->v_cell);
  GlobalData_t F_RT = (1./RT_F);
  GlobalData_t conc2i = (1./i2conc);
  GlobalData_t vcell_vsr_rat = (region->v_cell/vsr);
  GlobalData_t f = ((((4.0*pca)*F)*1e-3)*F_RT);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e-3;
  
  
  GlobalData_t ENa = (RT_F*(log((p->Nao/sv->Nai))));
  GlobalData_t GNaca_aloss = (p->GNaCa/(1.0+(((kdna/sv->cs)*(kdna/sv->cs))*(kdna/sv->cs))));
  GlobalData_t Kii = (sv->Ki+p->BASE_Ki);
  GlobalData_t Nai3 = ((sv->Nai*sv->Nai)*sv->Nai);
  GlobalData_t csm = (sv->cs/1000.);
  GlobalData_t fnak = (1.0/((1.+(0.1245*(exp(((-0.1*V)*F_RT)))))+((0.0365*sigma)*(exp(((-V)*F_RT))))));
  GlobalData_t po = ((((((1.0-(sv->xi1ca))-(sv->xi2ca))-(sv->xi1ba))-(sv->xi2ba))-(sv->c1))-(sv->c2));
  GlobalData_t rg = (1.0/(1.0+(exp(((V+33.0)/22.4)))));
  GlobalData_t rt2 = ((V+33.5)/10.0);
  GlobalData_t rxa1 = ((V!=0.0) ? (((V*f)*(exp(((V*2.0)*F_RT))))/(expm1(((V*2.0)*F_RT)))) : (f/(2.0*F_RT)));
  GlobalData_t rxa2 = ((V!=0.0) ? (((((-V)*f)*0.341)*p->Cao)/(expm1(((V*2.0)*F_RT)))) : ((((-f)*0.341)*p->Cao)/(2.0*F_RT)));
  GlobalData_t zw3a = (exp(((V*0.35)*F_RT)));
  GlobalData_t zw3b = (exp(((V*(0.35-(1.0)))*F_RT)));
  GlobalData_t zw4 = (1.0+(0.2*(exp(((V*(0.35-(1.0)))*F_RT)))));
  GlobalData_t EK = (RT_F*(log((p->Ko/Kii))));
  GlobalData_t EKs = (RT_F*(log(((p->Ko+(prnak*p->Nao))/(Kii+(prnak*sv->Nai))))));
  GlobalData_t INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(ENa)));
  GlobalData_t INaK = (((((1.0/(1.0+(kmnai/sv->Nai)))*ibarnak)*fnak)*p->Ko)/(p->Ko+kmko));
  GlobalData_t rs_inf = (1.0/(1.0+(exp(rt2))));
  GlobalData_t rxa = ((rxa1*csm)+rxa2);
  GlobalData_t yz1 = ((mCao*Nai3)+(mNao3*csm));
  GlobalData_t yz2 = ((mNai3*p->Cao)*(1.0+(csm/mCai)));
  GlobalData_t yz3 = ((mCai*Nao3)*(1.0+(Nai3/mNai3)));
  GlobalData_t yz4 = ((Nai3*p->Cao)+(Nao3*csm));
  GlobalData_t zw3 = (((Nai3*p->Cao)*zw3a)-(((Nao3*csm)*zw3b)));
  GlobalData_t IKr = ((((sv->GKr*sv->xr)*rg)*p->gss)*(V-(EK)));
  GlobalData_t IKs = (((sv->factorGKs*sv->xs1)*sv->xs2)*(V-(EKs)));
  GlobalData_t Itof = (((p->Gtof*sv->xtof)*sv->ytof)*(V-(EK)));
  GlobalData_t Itos = (((p->Gtos*sv->xtos)*(sv->ytos+(0.5*rs_inf)))*(V-(EK)));
  GlobalData_t VEK = (V-(EK));
  GlobalData_t xicaq = ((p->GCaL*po)*rxa);
  GlobalData_t zw8 = (((yz1+yz2)+yz3)+yz4);
  GlobalData_t ICa = (wca_m2*xicaq);
  GlobalData_t aa_K1 = (1.02/(1.0+(exp((0.2385*(VEK-(59.215)))))));
  GlobalData_t bb_K1 = (((0.49124*(exp((0.08032*(VEK+5.476)))))+(exp((0.061750*(VEK-(594.31))))))/(1.0+(exp((-0.5143*(VEK+4.753))))));
  GlobalData_t xINaCaq = ((GNaca_aloss*zw3)/(zw4*zw8));
  GlobalData_t INaCa = (wca*xINaCaq);
  GlobalData_t K1_inf = (aa_K1/(aa_K1+bb_K1));
  GlobalData_t IK1 = (((p->GK1*p->gss)*K1_inf)*VEK);
  //Output the desired variables
  fprintf(file, "%4.12f\t", ICa);
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", Itof);
  fprintf(file, "%4.12f\t", Itos);
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e3;
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        