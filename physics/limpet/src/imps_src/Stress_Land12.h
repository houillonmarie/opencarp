// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Title: An analysis of deformation-dependent electromechanical coupling in the mouse heart
*  Authors: Sander Land, Steven A Niederer, Jan Magnus Aronsen, Emil K S Espe, Lili Zhang, William E Louch, Ivar Sjaastad, Ole M Sejersted, Nicolas P Smith
*  Year: 2012
*  Journal: Journal of Physiology 2012;590:4553-69
*  DOI: 10.1113/jphysiol.2012.231928
*  Comment: Rat ventricular active stress model (plugin)
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __STRESS_LAND12_H__
#define __STRESS_LAND12_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Stress_Land12_REQDAT delLambda_DATA_FLAG|Lambda_DATA_FLAG
#define Stress_Land12_MODDAT Tension_DATA_FLAG

struct Stress_Land12_Params {
    GlobalData_t A_1;
    GlobalData_t A_2;
    GlobalData_t Ca_50ref;
    GlobalData_t TRPN_50;
    GlobalData_t T_ref;
    GlobalData_t a;
    GlobalData_t alpha_1;
    GlobalData_t alpha_2;
    GlobalData_t beta_0;
    GlobalData_t beta_1;
    GlobalData_t k_TRPN;
    GlobalData_t k_xb;
    GlobalData_t n_TRPN;
    GlobalData_t n_xb;

};

struct Stress_Land12_state {
    GlobalData_t Q_1;
    GlobalData_t Q_2;
    GlobalData_t TRPN;
    GlobalData_t xb;
    GlobalData_t __Ca_i_local;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Stress_Land12(ION_IF *);
void construct_tables_Stress_Land12(ION_IF *);
void destroy_Stress_Land12(ION_IF *);
void initialize_sv_Stress_Land12(ION_IF *, GlobalData_t**);
void compute_Stress_Land12(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
