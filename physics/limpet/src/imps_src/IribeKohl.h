// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Gentaro Iribe, Peter Kohl
*  Year: 2008
*  Title: Axial stretch enhances sarcoplasmic reticulum Ca2+ leak and cellular Ca2+ reuptake in guinea pig ventricular myocytes: Experiments and models
*  Journal: Progress in Biophysics and Molecular Biology,97(2-3),298-311
*  DOI: 10.1016/j.pbiomolbio.2008.02.012
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __IRIBEKOHL_H__
#define __IRIBEKOHL_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define IribeKohl_REQDAT Vm_DATA_FLAG
#define IribeKohl_MODDAT Iion_DATA_FLAG

struct IribeKohl_Params {
    GlobalData_t Ki_init;

};

struct IribeKohl_state {
    GlobalData_t CaSR;
    GlobalData_t Cai;
    GlobalData_t Cmdn_Ca;
    GlobalData_t F_1;
    GlobalData_t F_2;
    Gatetype F_CaMK;
    GlobalData_t F_SRCa_RyR;
    GlobalData_t Ki;
    GlobalData_t N_0;
    GlobalData_t Nai;
    GlobalData_t P_0;
    GlobalData_t P_1;
    GlobalData_t P_2;
    GlobalData_t P_3;
    GlobalData_t Trpn_Ca;
    Gatetype d;
    Gatetype f;
    Gatetype h;
    Gatetype m;
    GlobalData_t r;
    Gatetype s;
    Gatetype x;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_IribeKohl(ION_IF *);
void construct_tables_IribeKohl(ION_IF *);
void destroy_IribeKohl(ION_IF *);
void initialize_sv_IribeKohl(ION_IF *, GlobalData_t**);
void compute_IribeKohl(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
