// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Thomas R. Shannon, Fei Wang, Jose Puglisi, Christopher Weber, Donald M. Bers
*  Year: 2004
*  Title: A Mathematical Treatment of Integrated Ca Dynamics within the Ventricular Myocyte
*  Journal: Biophysical Journal, 87(5), 3351-3371
*  DOI: 10.1529/biophysj.104.047449
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __SHANNON_H__
#define __SHANNON_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Shannon_REQDAT Vm_DATA_FLAG
#define Shannon_MODDAT Iion_DATA_FLAG

struct Shannon_Params {

};

struct Shannon_state {
    GlobalData_t Ca_Calmodulin;
    GlobalData_t Ca_Calsequestrin;
    GlobalData_t Ca_Myosin;
    GlobalData_t Ca_SL;
    GlobalData_t Ca_SLB_SL;
    GlobalData_t Ca_SLB_jct;
    GlobalData_t Ca_SLHigh_SL;
    GlobalData_t Ca_SLHigh_jct;
    GlobalData_t Ca_SR;
    GlobalData_t Ca_SRB;
    GlobalData_t Ca_TroponinC;
    GlobalData_t Ca_TroponinC_Ca_Mg;
    GlobalData_t Ca_jct;
    GlobalData_t Cai;
    GlobalData_t I;
    GlobalData_t MGMyosin;
    GlobalData_t MGTroponinC_Ca_Mg;
    GlobalData_t Na_SL;
    GlobalData_t Na_SL_buf;
    GlobalData_t Na_jct;
    GlobalData_t Na_jct_buf;
    GlobalData_t Nai;
    GlobalData_t O;
    GlobalData_t R_cp0;
    Gatetype R_tos;
    Gatetype X_tof;
    Gatetype X_tos;
    Gatetype Xr;
    Gatetype Xs;
    Gatetype Y_tof;
    Gatetype Y_tos;
    Gatetype d;
    Gatetype f;
    GlobalData_t fCaB_SL;
    GlobalData_t fCaB_jct;
    Gatetype h;
    Gatetype j;
    Gatetype m;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Shannon(ION_IF *);
void construct_tables_Shannon(ION_IF *);
void destroy_Shannon(ION_IF *);
void initialize_sv_Shannon(ION_IF *, GlobalData_t**);
void compute_Shannon(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
