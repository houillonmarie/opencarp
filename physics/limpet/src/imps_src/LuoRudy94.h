// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Luo CH, Rudy Y
*  Year: 1994
*  Title: A dynamic model of the cardiac ventricular action potential. I. Simulations of ionic currents and concentration changes
*  Journal: Circ Res., 74(6),1071-96
*  DOI: 10.1161/01.res.74.6.1071
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __LUORUDY94_H__
#define __LUORUDY94_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define LuoRudy94_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG
#define LuoRudy94_MODDAT Iion_DATA_FLAG

struct LuoRudy94_Params {
    GlobalData_t GNaK;

};

struct LuoRudy94_state {
    GlobalData_t Cai;
    GlobalData_t ICaIold;
    GlobalData_t Ki;
    GlobalData_t Nai;
    Gatetype b;
    Gatetype d;
    Gatetype f;
    GlobalData_t fdiff_ICaIold;
    Gatetype g;
    Gatetype h;
    Gatetype j;
    GlobalData_t jsr;
    Gatetype m;
    GlobalData_t nsr;
    GlobalData_t tcicr;
    GlobalData_t tjsrol;
    Gatetype xr;
    Gatetype xs1;
    Gatetype xs2;
    Gatetype ydv;
    Gatetype zdv;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_LuoRudy94(ION_IF *);
void construct_tables_LuoRudy94(ION_IF *);
void destroy_LuoRudy94(ION_IF *);
void initialize_sv_LuoRudy94(ION_IF *, GlobalData_t**);
void compute_LuoRudy94(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
