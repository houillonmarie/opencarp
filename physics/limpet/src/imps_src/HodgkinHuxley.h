// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Hodgkin, A. L., Huxley, A. F.
*  Year: 1952
*  Title: A quantitative description of membrane current and its application to conduction and excitation in nerve
*  Journal: The Journal of Physiology, 117
*  DOI: 10.1113/jphysiol.1952.sp004764
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __HODGKINHUXLEY_H__
#define __HODGKINHUXLEY_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define HodgkinHuxley_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG
#define HodgkinHuxley_MODDAT Iion_DATA_FLAG

struct HodgkinHuxley_Params {
    GlobalData_t GK;
    GlobalData_t GNa;
    GlobalData_t g_L;

};

struct HodgkinHuxley_state {
    Gatetype h;
    Gatetype m;
    Gatetype n;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_HodgkinHuxley(ION_IF *);
void construct_tables_HodgkinHuxley(ION_IF *);
void destroy_HodgkinHuxley(ION_IF *);
void initialize_sv_HodgkinHuxley(ION_IF *, GlobalData_t**);
void compute_HodgkinHuxley(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
