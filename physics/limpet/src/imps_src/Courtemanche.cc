// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Courtemanche, M., Ramirez, R. J., and Nattel, S.
*  Year: 1998
*  Title: Ionic mechanisms underlying human atrial action potential properties: insights from a mathematical model
*  Journal: Am. J. Physiol. 275(1 Pt 2), H301-H321
*  DOI: 10.1152/ajpheart.1998.275.1.H301
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Courtemanche.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Courtemanche(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Courtemanche( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define C_B1a (GlobalData_t)(3.79138232501097e-05)
#define C_B1b (GlobalData_t)(0.0811764705882353)
#define C_B1c (GlobalData_t)(0.00705882352941176)
#define C_B1d (GlobalData_t)(0.00537112496043221)
#define C_B1e (GlobalData_t)(11.5)
#define C_Fn1 (GlobalData_t)(9.648e-13)
#define C_Fn2 (GlobalData_t)(2.5910306809e-13)
#define C_dCa_rel (GlobalData_t)(8.)
#define C_dCaup (GlobalData_t)(0.0869565217391304)
#define Ca_rel_init (GlobalData_t)(1.49)
#define Ca_up_init (GlobalData_t)(1.49)
#define Cai_init (GlobalData_t)(1.02e-1)
#define F (GlobalData_t)(96.4867)
#define K_Q10 (GlobalData_t)(3.)
#define K_up (GlobalData_t)(0.00092)
#define Ki_init (GlobalData_t)(139.0)
#define KmCa (GlobalData_t)(1.38)
#define KmCmdn (GlobalData_t)(0.00238)
#define KmCsqn (GlobalData_t)(0.8)
#define KmKo (GlobalData_t)(1.5)
#define KmNa (GlobalData_t)(87.5)
#define KmNa3 (GlobalData_t)(669921.875)
#define KmNai (GlobalData_t)(10.)
#define KmTrpn (GlobalData_t)(0.0005)
#define Nai (GlobalData_t)(11.2)
#define R (GlobalData_t)(8.3143)
#define T (GlobalData_t)(310.)
#define V_init (GlobalData_t)(-81.2)
#define Volcell (GlobalData_t)(20100.)
#define Voli (GlobalData_t)(13668.)
#define Volrel (GlobalData_t)(96.48)
#define Volup (GlobalData_t)(1109.52)
#define d_init (GlobalData_t)(1.37e-4)
#define f_Ca_init (GlobalData_t)(0.775)
#define f_init (GlobalData_t)(0.999)
#define gamma (GlobalData_t)(0.35)
#define h_init (GlobalData_t)(0.965)
#define j_init (GlobalData_t)(0.978)
#define k_rel (GlobalData_t)(30.)
#define k_sat (GlobalData_t)(0.1)
#define m_init (GlobalData_t)(2.91e-3)
#define maxCmdn (GlobalData_t)(0.05)
#define maxCsqn (GlobalData_t)(10.)
#define maxTrpn (GlobalData_t)(0.07)
#define oa_init (GlobalData_t)(3.04e-2)
#define oi_init (GlobalData_t)(0.999)
#define tau_f_Ca (GlobalData_t)(2.)
#define tau_tr (GlobalData_t)(180.)
#define tau_u (GlobalData_t)(8.)
#define u_init (GlobalData_t)(0.)
#define ua_init (GlobalData_t)(4.96e-3)
#define ui_init (GlobalData_t)(0.999)
#define v_init (GlobalData_t)(1.)
#define w_init (GlobalData_t)(0.999)
#define xr_init (GlobalData_t)(3.29e-5)
#define xs_init (GlobalData_t)(1.87e-2)



void initialize_params_Courtemanche( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Courtemanche_Params *p = (Courtemanche_Params *)IF->params;

  // Compute the regional constants
  {
    p->ACh = 0.000001;
    p->Cao = 1.8;
    p->Cm = 100.;
    p->GACh = 0.;
    p->GCaL = 0.1238;
    p->GK1 = 0.09;
    p->GKr = 0.0294;
    p->GKs = 0.129;
    p->GNa = 7.8;
    p->GbCa = 0.00113;
    p->GbNa = 0.000674;
    p->Gto = 0.1652;
    p->Ko = 5.4;
    p->Nao = 140.;
    p->factorGKur = 1.;
    p->factorGrel = 1.;
    p->factorGtr = 1.;
    p->factorGup = 1.;
    p->factorhGate = 0.;
    p->factormGate = 0.;
    p->factoroaGate = 0.;
    p->factorxrGate = 1.;
    p->maxCaup = 15.;
    p->maxINaCa = 1600.;
    p->maxINaK = 0.60;
    p->maxIpCa = 0.275;
    p->maxIup = 0.005;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_Courtemanche;

}


// Define the parameters for the lookup tables
enum Tables {
  Cai_TAB,
  V_TAB,
  fn_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum Cai_TableIndex {
  carow_1_idx,
  carow_2_idx,
  carow_3_idx,
  conCa_idx,
  f_Ca_rush_larsen_A_idx,
  NROWS_Cai
};

enum V_TableIndex {
  GKur_idx,
  INaK_idx,
  IbNa_idx,
  d_rush_larsen_A_idx,
  d_rush_larsen_B_idx,
  f_rush_larsen_A_idx,
  f_rush_larsen_B_idx,
  h_rush_larsen_A_idx,
  h_rush_larsen_B_idx,
  j_rush_larsen_A_idx,
  j_rush_larsen_B_idx,
  m_rush_larsen_A_idx,
  m_rush_larsen_B_idx,
  oa_rush_larsen_A_idx,
  oa_rush_larsen_B_idx,
  oi_rush_larsen_A_idx,
  oi_rush_larsen_B_idx,
  ua_rush_larsen_A_idx,
  ua_rush_larsen_B_idx,
  ui_rush_larsen_A_idx,
  ui_rush_larsen_B_idx,
  vrow_29_idx,
  vrow_31_idx,
  vrow_32_idx,
  vrow_36_idx,
  vrow_7_idx,
  w_rush_larsen_A_idx,
  w_rush_larsen_B_idx,
  xr_rush_larsen_A_idx,
  xr_rush_larsen_B_idx,
  xs_rush_larsen_A_idx,
  xs_rush_larsen_B_idx,
  NROWS_V
};

enum fn_TableIndex {
  u_rush_larsen_A_idx,
  v_rush_larsen_A_idx,
  v_rush_larsen_B_idx,
  NROWS_fn
};



void construct_tables_Courtemanche( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Courtemanche_Params *p = (Courtemanche_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double E_Na = (((R*T)/F)*(log((p->Nao/Nai))));
  double f_Ca_rush_larsen_B = (exp(((-dt)/tau_f_Ca)));
  double f_Ca_rush_larsen_C = (expm1(((-dt)/tau_f_Ca)));
  double sigma = (((exp((p->Nao/67.3)))-(1.))/7.);
  double u_rush_larsen_B = (exp(((-dt)/tau_u)));
  double u_rush_larsen_C = (expm1(((-dt)/tau_u)));
  
  // Create the Cai lookup table
  LUT* Cai_tab = &IF->tables[Cai_TAB];
  LUT_alloc(Cai_tab, NROWS_Cai, 3e-4, 30.0, 3e-4, "Courtemanche Cai");
  for (int __i=Cai_tab->mn_ind; __i<=Cai_tab->mx_ind; __i++) {
    double Cai = Cai_tab->res*__i;
    LUT_data_t* Cai_row = Cai_tab->tab[__i];
    Cai_row[conCa_idx] = (Cai/1000.);
    Cai_row[carow_1_idx] = ((p->factorGup*p->maxIup)/(1.+(K_up/Cai_row[conCa_idx])));
    Cai_row[carow_2_idx] = (((((((maxTrpn*KmTrpn)/(Cai_row[conCa_idx]+KmTrpn))/(Cai_row[conCa_idx]+KmTrpn))+(((maxCmdn*KmCmdn)/(Cai_row[conCa_idx]+KmCmdn))/(Cai_row[conCa_idx]+KmCmdn)))+1.)/C_B1c)/1000.);
    Cai_row[carow_3_idx] = (((p->maxIpCa*Cai_row[conCa_idx])/(0.0005+Cai_row[conCa_idx]))-((((((p->GbCa*R)*T)/2.)/F)*(log((p->Cao/Cai_row[conCa_idx]))))));
    double f_Ca_inf = (1./(1.+(Cai_row[conCa_idx]/0.00035)));
    Cai_row[f_Ca_rush_larsen_A_idx] = ((-f_Ca_inf)*f_Ca_rush_larsen_C);
  }
  check_LUT(Cai_tab);
  
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -200, 200, 0.1, "Courtemanche V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    V_row[GKur_idx] = (0.005+(0.05/(1.+(exp(((15.-(V))/13.))))));
    V_row[IbNa_idx] = (p->GbNa*(V-(E_Na)));
    double a_h = ((V>=-40.) ? 0.0 : (0.135*(exp((((V+80.)-(p->factorhGate))/-6.8)))));
    double a_j = ((V<-40.) ? ((((-127140.*(exp((0.2444*V))))-((3.474e-5*(exp((-0.04391*V))))))*(V+37.78))/(1.+(exp((0.311*(V+79.23)))))) : 0.);
    double a_m = ((V==-47.13) ? 3.2 : ((0.32*(V+47.13))/(1.-((exp((-0.1*(V+47.13))))))));
    double aa_oa = (0.65/((exp(((V+10.)/-8.5)))+(exp(((30.-(V))/59.0)))));
    double aa_oi = (1./(18.53+(exp(((V+113.7)/10.95)))));
    double aa_ua = (0.65/((exp(((V+10.)/-8.5)))+(exp(((V-(30.))/-59.0)))));
    double aa_ui = (1./(21.+(exp(((V-(185.))/-28.)))));
    double aa_xr = (p->factorxrGate*((0.0003*(V+14.1))/(1.-((exp(((V+14.1)/-5.)))))));
    double aa_xs = ((4.e-5*(V-(19.9)))/(1.-((exp(((19.9-(V))/17.))))));
    double b_h = ((V>=-40.) ? ((1./0.13)/(1.+(exp(((-(V+10.66))/11.1))))) : ((3.56*(exp((0.079*V))))+(3.1e5*(exp((0.35*V))))));
    double b_j = ((V>=-40.) ? ((0.3*(exp((-2.535e-7*V))))/(1.+(exp((-0.1*(V+32.)))))) : ((0.1212*(exp((-0.01052*V))))/(1.+(exp((-0.1378*(V+40.14)))))));
    double b_m = (0.08*(exp(((-(V-(p->factormGate)))/11.))));
    double bb_oa = (0.65/(2.5+(exp(((V+82.)/17.)))));
    double bb_oi = (1./(35.56+(exp(((-(V+1.26))/7.44)))));
    double bb_ua = (0.65/(2.5+(exp(((V+82.)/17.)))));
    double bb_ui = (exp(((V-(158.))/16.)));
    double bb_xr = ((1.0/p->factorxrGate)*((7.3898e-5*(V-(3.3328)))/((exp(((V-(3.3328))/5.1237)))-(1.))));
    double bb_xs = ((3.5e-5*(V-(19.9)))/((exp(((V-(19.9))/9.)))-(1.)));
    double d_inf = (1./(1.+(exp(((-(V+10.))/8.)))));
    double f_NaK = (1./((1.+(0.1245*(exp(((((-0.1*F)*V)/R)/T)))))+((0.0365*sigma)*(exp(((((-F)*V)/R)/T))))));
    double f_inf = (1./(1.+(exp(((V+28.)/6.9)))));
    double oa_inf = (1./(1.+(exp(((-((V+20.47)-(p->factoroaGate)))/17.54)))));
    double oi_inf = (1./(1.+(exp(((V+43.1)/5.3)))));
    double tau_d = ((V==-10.) ? (((1./6.24)/0.035)/2.) : ((((1.-((exp(((-(V+10.))/6.24)))))/0.035)/(V+10.))/(1.+(exp(((-(V+10.))/6.24))))));
    double tau_f = (9./((0.0197*(exp((((-0.0337*0.0337)*(V+10.))*(V+10.)))))+0.02));
    double tau_w = (((6.*(1.-((exp(((7.9-(V))/5.))))))/(1.+(0.3*(exp(((7.9-(V))/5.))))))/(V-(7.9)));
    double ua_inf = (1./(1.+(exp(((V+30.3)/-9.6)))));
    double ui_inf = (1./(1.+(exp(((V-(99.45))/27.48)))));
    V_row[vrow_29_idx] = (p->GCaL*(V-(65.)));
    V_row[vrow_31_idx] = ((((((((p->maxINaCa*(exp(((((gamma*F)*V)/R)/T))))*Nai)*Nai)*Nai)*p->Cao)/(KmNa3+((p->Nao*p->Nao)*p->Nao)))/(KmCa+p->Cao))/(1.+(k_sat*(exp((((((gamma-(1.))*F)*V)/R)/T))))));
    V_row[vrow_32_idx] = ((((((((p->maxINaCa*(exp((((((gamma-(1.))*F)*V)/R)/T))))*p->Nao)*p->Nao)*p->Nao)/(KmNa3+((p->Nao*p->Nao)*p->Nao)))/(KmCa+p->Cao))/(1.+(k_sat*(exp((((((gamma-(1.))*F)*V)/R)/T))))))/1000.);
    V_row[vrow_36_idx] = (V*p->GbCa);
    V_row[vrow_7_idx] = (p->GNa*(V-(E_Na)));
    double w_inf = (1.-((1./(1.+(exp(((40.-(V))/17.)))))));
    double xr_inf = (1./(1.+(exp(((V+14.1)/-6.5)))));
    double xs_inf = (1./(sqrt((1.+(exp(((V-(19.9))/-12.7)))))));
    V_row[INaK_idx] = ((((p->maxINaK*f_NaK)/(1.+(pow((KmNai/Nai),1.5))))*p->Ko)/(p->Ko+KmKo));
    V_row[d_rush_larsen_B_idx] = (exp(((-dt)/tau_d)));
    double d_rush_larsen_C = (expm1(((-dt)/tau_d)));
    V_row[f_rush_larsen_B_idx] = (exp(((-dt)/tau_f)));
    double f_rush_larsen_C = (expm1(((-dt)/tau_f)));
    V_row[h_rush_larsen_A_idx] = (((-a_h)/(a_h+b_h))*(expm1(((-dt)*(a_h+b_h)))));
    V_row[h_rush_larsen_B_idx] = (exp(((-dt)*(a_h+b_h))));
    V_row[j_rush_larsen_A_idx] = (((-a_j)/(a_j+b_j))*(expm1(((-dt)*(a_j+b_j)))));
    V_row[j_rush_larsen_B_idx] = (exp(((-dt)*(a_j+b_j))));
    V_row[m_rush_larsen_A_idx] = (((-a_m)/(a_m+b_m))*(expm1(((-dt)*(a_m+b_m)))));
    V_row[m_rush_larsen_B_idx] = (exp(((-dt)*(a_m+b_m))));
    double tau_oa = ((1./(aa_oa+bb_oa))/K_Q10);
    double tau_oi = ((1./(aa_oi+bb_oi))/K_Q10);
    double tau_ua = ((1./(aa_ua+bb_ua))/K_Q10);
    double tau_ui = ((1./(aa_ui+bb_ui))/K_Q10);
    double tau_xr = (1./(aa_xr+bb_xr));
    double tau_xs = (0.5/(aa_xs+bb_xs));
    V_row[w_rush_larsen_B_idx] = (exp(((-dt)/tau_w)));
    double w_rush_larsen_C = (expm1(((-dt)/tau_w)));
    V_row[d_rush_larsen_A_idx] = ((-d_inf)*d_rush_larsen_C);
    V_row[f_rush_larsen_A_idx] = ((-f_inf)*f_rush_larsen_C);
    V_row[oa_rush_larsen_B_idx] = (exp(((-dt)/tau_oa)));
    double oa_rush_larsen_C = (expm1(((-dt)/tau_oa)));
    V_row[oi_rush_larsen_B_idx] = (exp(((-dt)/tau_oi)));
    double oi_rush_larsen_C = (expm1(((-dt)/tau_oi)));
    V_row[ua_rush_larsen_B_idx] = (exp(((-dt)/tau_ua)));
    double ua_rush_larsen_C = (expm1(((-dt)/tau_ua)));
    V_row[ui_rush_larsen_B_idx] = (exp(((-dt)/tau_ui)));
    double ui_rush_larsen_C = (expm1(((-dt)/tau_ui)));
    V_row[w_rush_larsen_A_idx] = ((-w_inf)*w_rush_larsen_C);
    V_row[xr_rush_larsen_B_idx] = (exp(((-dt)/tau_xr)));
    double xr_rush_larsen_C = (expm1(((-dt)/tau_xr)));
    V_row[xs_rush_larsen_B_idx] = (exp(((-dt)/tau_xs)));
    double xs_rush_larsen_C = (expm1(((-dt)/tau_xs)));
    V_row[oa_rush_larsen_A_idx] = ((-oa_inf)*oa_rush_larsen_C);
    V_row[oi_rush_larsen_A_idx] = ((-oi_inf)*oi_rush_larsen_C);
    V_row[ua_rush_larsen_A_idx] = ((-ua_inf)*ua_rush_larsen_C);
    V_row[ui_rush_larsen_A_idx] = ((-ui_inf)*ui_rush_larsen_C);
    V_row[xr_rush_larsen_A_idx] = ((-xr_inf)*xr_rush_larsen_C);
    V_row[xs_rush_larsen_A_idx] = ((-xs_inf)*xs_rush_larsen_C);
  }
  check_LUT(V_tab);
  
  
  // Create the fn lookup table
  LUT* fn_tab = &IF->tables[fn_TAB];
  LUT_alloc(fn_tab, NROWS_fn, -2e-11, 10.0e-11, 2e-15, "Courtemanche fn");
  for (int __i=fn_tab->mn_ind; __i<=fn_tab->mx_ind; __i++) {
    double fn = fn_tab->res*__i;
    LUT_data_t* fn_row = fn_tab->tab[__i];
    double tau_v = (1.91+(2.09/(1.+(exp(((3.4175e-13-(fn))/13.67e-16))))));
    double u_inf = (1./(1.+(exp(((3.4175e-13-(fn))/13.67e-16)))));
    double v_inf = (1.-((1./(1.+(exp(((6.835e-14-(fn))/13.67e-16)))))));
    fn_row[u_rush_larsen_A_idx] = ((-u_inf)*u_rush_larsen_C);
    fn_row[v_rush_larsen_B_idx] = (exp(((-dt)/tau_v)));
    double v_rush_larsen_C = (expm1(((-dt)/tau_v)));
    fn_row[v_rush_larsen_A_idx] = ((-v_inf)*v_rush_larsen_C);
  }
  check_LUT(fn_tab);
  

}



void    initialize_sv_Courtemanche( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Courtemanche_Params *p = (Courtemanche_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Courtemanche_state) );
  Courtemanche_state *sv_base = (Courtemanche_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double E_Na = (((R*T)/F)*(log((p->Nao/Nai))));
  double f_Ca_rush_larsen_B = (exp(((-dt)/tau_f_Ca)));
  double f_Ca_rush_larsen_C = (expm1(((-dt)/tau_f_Ca)));
  double sigma = (((exp((p->Nao/67.3)))-(1.))/7.);
  double u_rush_larsen_B = (exp(((-dt)/tau_u)));
  double u_rush_larsen_C = (expm1(((-dt)/tau_u)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Courtemanche_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Ca_rel = Ca_rel_init;
    sv->Ca_up = Ca_up_init;
    sv->Cai = Cai_init;
    sv->Ki = Ki_init;
    V = V_init;
    sv->d = d_init;
    sv->f = f_init;
    sv->f_Ca = f_Ca_init;
    sv->h = h_init;
    sv->j = j_init;
    sv->m = m_init;
    sv->oa = oa_init;
    sv->oi = oi_init;
    sv->u = u_init;
    sv->ua = ua_init;
    sv->ui = ui_init;
    sv->v = v_init;
    sv->w = w_init;
    sv->xr = xr_init;
    sv->xs = xs_init;
    double E_K = (((R*T)/F)*(log((p->Ko/sv->Ki))));
    double GKur = (0.005+(0.05/(1.+(exp(((15.-(V))/13.))))));
    double IbNa = (p->GbNa*(V-(E_Na)));
    double conCa = (sv->Cai/1000.);
    double f_NaK = (1./((1.+(0.1245*(exp(((((-0.1*F)*V)/R)/T)))))+((0.0365*sigma)*(exp(((((-F)*V)/R)/T))))));
    double vrow_29 = (p->GCaL*(V-(65.)));
    double vrow_31 = ((((((((p->maxINaCa*(exp(((((gamma*F)*V)/R)/T))))*Nai)*Nai)*Nai)*p->Cao)/(KmNa3+((p->Nao*p->Nao)*p->Nao)))/(KmCa+p->Cao))/(1.+(k_sat*(exp((((((gamma-(1.))*F)*V)/R)/T))))));
    double vrow_32 = ((((((((p->maxINaCa*(exp((((((gamma-(1.))*F)*V)/R)/T))))*p->Nao)*p->Nao)*p->Nao)/(KmNa3+((p->Nao*p->Nao)*p->Nao)))/(KmCa+p->Cao))/(1.+(k_sat*(exp((((((gamma-(1.))*F)*V)/R)/T))))))/1000.);
    double vrow_36 = (V*p->GbCa);
    double vrow_7 = (p->GNa*(V-(E_Na)));
    double ICaL = (((vrow_29*sv->d)*sv->f)*sv->f_Ca);
    double IKACh = (((p->GACh*(10.0/(1.0+(9.13652/(pow(p->ACh,0.477811))))))*(0.0517+(0.4516/(1.0+(exp(((V+59.53)/17.18)))))))*(V-(E_K)));
    double INa = (((((vrow_7*sv->m)*sv->m)*sv->m)*sv->h)*sv->j);
    double INaCa = (vrow_31-((sv->Cai*vrow_32)));
    double INaK = ((((p->maxINaK*f_NaK)/(1.+(pow((KmNai/Nai),1.5))))*p->Ko)/(p->Ko+KmKo));
    double carow_3 = (((p->maxIpCa*conCa)/(0.0005+conCa))-((((((p->GbCa*R)*T)/2.)/F)*(log((p->Cao/conCa))))));
    double vrow_13 = (p->Gto*(V-(E_K)));
    double vrow_18 = ((p->factorGKur*GKur)*(V-(E_K)));
    double vrow_21 = ((p->GKr*(V-(E_K)))/(1.+(exp(((V+15.)/22.4)))));
    double vrow_24 = (p->GKs*(V-(E_K)));
    double vrow_8 = ((p->GK1*(V-(E_K)))/(1.+(exp((0.07*(V+80.))))));
    double IK1 = vrow_8;
    double IKr = (vrow_21*sv->xr);
    double IKs = ((vrow_24*sv->xs)*sv->xs);
    double IKur = ((((vrow_18*sv->ua)*sv->ua)*sv->ua)*sv->ui);
    double IpCa = (carow_3+vrow_36);
    double Ito = ((((vrow_13*sv->oa)*sv->oa)*sv->oa)*sv->oi);
    Iion = (((((((((((INa+IK1)+Ito)+IKur)+IKr)+IKs)+ICaL)+IpCa)+INaCa)+IbNa)+INaK)+IKACh);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Courtemanche(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Courtemanche_Params *p  = (Courtemanche_Params *)IF->params;
  Courtemanche_state *sv_base = (Courtemanche_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t E_Na = (((R*T)/F)*(log((p->Nao/Nai))));
  GlobalData_t f_Ca_rush_larsen_B = (exp(((-dt)/tau_f_Ca)));
  GlobalData_t f_Ca_rush_larsen_C = (expm1(((-dt)/tau_f_Ca)));
  GlobalData_t sigma = (((exp((p->Nao/67.3)))-(1.))/7.);
  GlobalData_t u_rush_larsen_B = (exp(((-dt)/tau_u)));
  GlobalData_t u_rush_larsen_C = (expm1(((-dt)/tau_u)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Courtemanche_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Cai_row[NROWS_Cai];
    LUT_interpRow(&IF->tables[Cai_TAB], sv->Cai, __i, Cai_row);
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t E_K = (((R*T)/F)*(log((p->Ko/sv->Ki))));
    GlobalData_t ICaL = (((V_row[vrow_29_idx]*sv->d)*sv->f)*sv->f_Ca);
    GlobalData_t IKACh = (((p->GACh*(10.0/(1.0+(9.13652/(pow(p->ACh,0.477811))))))*(0.0517+(0.4516/(1.0+(exp(((V+59.53)/17.18)))))))*(V-(E_K)));
    GlobalData_t INa = (((((V_row[vrow_7_idx]*sv->m)*sv->m)*sv->m)*sv->h)*sv->j);
    GlobalData_t INaCa = (V_row[vrow_31_idx]-((sv->Cai*V_row[vrow_32_idx])));
    GlobalData_t vrow_13 = (p->Gto*(V-(E_K)));
    GlobalData_t vrow_18 = ((p->factorGKur*V_row[GKur_idx])*(V-(E_K)));
    GlobalData_t vrow_21 = ((p->GKr*(V-(E_K)))/(1.+(exp(((V+15.)/22.4)))));
    GlobalData_t vrow_24 = (p->GKs*(V-(E_K)));
    GlobalData_t vrow_8 = ((p->GK1*(V-(E_K)))/(1.+(exp((0.07*(V+80.))))));
    GlobalData_t IK1 = vrow_8;
    GlobalData_t IKr = (vrow_21*sv->xr);
    GlobalData_t IKs = ((vrow_24*sv->xs)*sv->xs);
    GlobalData_t IKur = ((((vrow_18*sv->ua)*sv->ua)*sv->ua)*sv->ui);
    GlobalData_t IpCa = (Cai_row[carow_3_idx]+V_row[vrow_36_idx]);
    GlobalData_t Ito = ((((vrow_13*sv->oa)*sv->oa)*sv->oa)*sv->oi);
    Iion = (((((((((((INa+IK1)+Ito)+IKur)+IKr)+IKs)+ICaL)+IpCa)+INaCa)+V_row[IbNa_idx])+V_row[INaK_idx])+IKACh);
    
    
    //Complete Forward Euler Update
    GlobalData_t Itr = ((p->factorGtr*(sv->Ca_up-(sv->Ca_rel)))/tau_tr);
    GlobalData_t Irel = ((((((p->factorGrel*sv->u)*sv->u)*sv->v)*k_rel)*sv->w)*(sv->Ca_rel-(Cai_row[conCa_idx])));
    GlobalData_t dIups = (Cai_row[carow_1_idx]-(((p->maxIup/p->maxCaup)*sv->Ca_up)));
    GlobalData_t diff_Ca_rel = ((Itr-(Irel))/(1.+((C_dCa_rel/(sv->Ca_rel+KmCsqn))/(sv->Ca_rel+KmCsqn))));
    GlobalData_t diff_Ki = ((-((((((Ito+IKr)+IKur)+IKs)+IK1)+IKACh)-((2.0*V_row[INaK_idx]))))/(F*Voli));
    GlobalData_t diff_Ca_up = (dIups-((Itr*C_dCaup)));
    GlobalData_t diff_Cai = ((((C_B1d*(((INaCa+INaCa)-(IpCa))-(ICaL)))-((C_B1e*dIups)))+Irel)/Cai_row[carow_2_idx]);
    GlobalData_t Ca_rel_new = sv->Ca_rel+diff_Ca_rel*dt;
    GlobalData_t Ca_up_new = sv->Ca_up+diff_Ca_up*dt;
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t fn = ((C_Fn1*Irel)-((C_Fn2*(ICaL-((0.4*INaCa))))));
    LUT_data_t fn_row[NROWS_fn];
    LUT_interpRow(&IF->tables[fn_TAB], fn, __i, fn_row);
    GlobalData_t d_rush_larsen_B = V_row[d_rush_larsen_B_idx];
    GlobalData_t f_rush_larsen_B = V_row[f_rush_larsen_B_idx];
    GlobalData_t h_rush_larsen_A = V_row[h_rush_larsen_A_idx];
    GlobalData_t h_rush_larsen_B = V_row[h_rush_larsen_B_idx];
    GlobalData_t j_rush_larsen_A = V_row[j_rush_larsen_A_idx];
    GlobalData_t j_rush_larsen_B = V_row[j_rush_larsen_B_idx];
    GlobalData_t m_rush_larsen_A = V_row[m_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_B = V_row[m_rush_larsen_B_idx];
    GlobalData_t w_rush_larsen_B = V_row[w_rush_larsen_B_idx];
    GlobalData_t d_rush_larsen_A = V_row[d_rush_larsen_A_idx];
    GlobalData_t f_Ca_rush_larsen_A = Cai_row[f_Ca_rush_larsen_A_idx];
    GlobalData_t f_rush_larsen_A = V_row[f_rush_larsen_A_idx];
    GlobalData_t oa_rush_larsen_B = V_row[oa_rush_larsen_B_idx];
    GlobalData_t oi_rush_larsen_B = V_row[oi_rush_larsen_B_idx];
    GlobalData_t u_rush_larsen_A = fn_row[u_rush_larsen_A_idx];
    GlobalData_t ua_rush_larsen_B = V_row[ua_rush_larsen_B_idx];
    GlobalData_t ui_rush_larsen_B = V_row[ui_rush_larsen_B_idx];
    GlobalData_t v_rush_larsen_B = fn_row[v_rush_larsen_B_idx];
    GlobalData_t w_rush_larsen_A = V_row[w_rush_larsen_A_idx];
    GlobalData_t xr_rush_larsen_B = V_row[xr_rush_larsen_B_idx];
    GlobalData_t xs_rush_larsen_B = V_row[xs_rush_larsen_B_idx];
    GlobalData_t oa_rush_larsen_A = V_row[oa_rush_larsen_A_idx];
    GlobalData_t oi_rush_larsen_A = V_row[oi_rush_larsen_A_idx];
    GlobalData_t ua_rush_larsen_A = V_row[ua_rush_larsen_A_idx];
    GlobalData_t ui_rush_larsen_A = V_row[ui_rush_larsen_A_idx];
    GlobalData_t v_rush_larsen_A = fn_row[v_rush_larsen_A_idx];
    GlobalData_t xr_rush_larsen_A = V_row[xr_rush_larsen_A_idx];
    GlobalData_t xs_rush_larsen_A = V_row[xs_rush_larsen_A_idx];
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f_new = f_rush_larsen_A+f_rush_larsen_B*sv->f;
    GlobalData_t f_Ca_new = f_Ca_rush_larsen_A+f_Ca_rush_larsen_B*sv->f_Ca;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t oa_new = oa_rush_larsen_A+oa_rush_larsen_B*sv->oa;
    GlobalData_t oi_new = oi_rush_larsen_A+oi_rush_larsen_B*sv->oi;
    GlobalData_t u_new = u_rush_larsen_A+u_rush_larsen_B*sv->u;
    GlobalData_t ua_new = ua_rush_larsen_A+ua_rush_larsen_B*sv->ua;
    GlobalData_t ui_new = ui_rush_larsen_A+ui_rush_larsen_B*sv->ui;
    GlobalData_t v_new = v_rush_larsen_A+v_rush_larsen_B*sv->v;
    GlobalData_t w_new = w_rush_larsen_A+w_rush_larsen_B*sv->w;
    GlobalData_t xr_new = xr_rush_larsen_A+xr_rush_larsen_B*sv->xr;
    GlobalData_t xs_new = xs_rush_larsen_A+xs_rush_larsen_B*sv->xs;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Ca_rel = Ca_rel_new;
    sv->Ca_up = Ca_up_new;
    sv->Cai = Cai_new;
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->d = d_new;
    sv->f = f_new;
    sv->f_Ca = f_Ca_new;
    sv->h = h_new;
    sv->j = j_new;
    sv->m = m_new;
    sv->oa = oa_new;
    sv->oi = oi_new;
    sv->u = u_new;
    sv->ua = ua_new;
    sv->ui = ui_new;
    sv->v = v_new;
    sv->w = w_new;
    sv->xr = xr_new;
    sv->xs = xs_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_Courtemanche(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Courtemanche_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->Ca_rel\n"
        "sv->Ca_up\n"
        "sv->Cai\n"
        "ICaL\n"
        "IK1\n"
        "IKr\n"
        "IKs\n"
        "IKur\n"
        "INa\n"
        "INaCa\n"
        "Iion\n"
        "IpCa\n"
        "Irel\n"
        "Ito\n"
        "Itr\n"
        "V\n"
        "sv->h\n"
        "sv->j\n"
        "sv->m\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Courtemanche_Params *p  = (Courtemanche_Params *)IF->params;

  Courtemanche_state *sv_base = (Courtemanche_state *)IF->sv_tab.y;
  Courtemanche_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t E_Na = (((R*T)/F)*(log((p->Nao/Nai))));
  GlobalData_t f_Ca_rush_larsen_B = (exp(((-dt)/tau_f_Ca)));
  GlobalData_t f_Ca_rush_larsen_C = (expm1(((-dt)/tau_f_Ca)));
  GlobalData_t sigma = (((exp((p->Nao/67.3)))-(1.))/7.);
  GlobalData_t u_rush_larsen_B = (exp(((-dt)/tau_u)));
  GlobalData_t u_rush_larsen_C = (expm1(((-dt)/tau_u)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t E_K = (((R*T)/F)*(log((p->Ko/sv->Ki))));
  GlobalData_t GKur = (0.005+(0.05/(1.+(exp(((15.-(V))/13.))))));
  GlobalData_t IbNa = (p->GbNa*(V-(E_Na)));
  GlobalData_t Itr = ((p->factorGtr*(sv->Ca_up-(sv->Ca_rel)))/tau_tr);
  GlobalData_t conCa = (sv->Cai/1000.);
  GlobalData_t f_NaK = (1./((1.+(0.1245*(exp(((((-0.1*F)*V)/R)/T)))))+((0.0365*sigma)*(exp(((((-F)*V)/R)/T))))));
  GlobalData_t vrow_29 = (p->GCaL*(V-(65.)));
  GlobalData_t vrow_31 = ((((((((p->maxINaCa*(exp(((((gamma*F)*V)/R)/T))))*Nai)*Nai)*Nai)*p->Cao)/(KmNa3+((p->Nao*p->Nao)*p->Nao)))/(KmCa+p->Cao))/(1.+(k_sat*(exp((((((gamma-(1.))*F)*V)/R)/T))))));
  GlobalData_t vrow_32 = ((((((((p->maxINaCa*(exp((((((gamma-(1.))*F)*V)/R)/T))))*p->Nao)*p->Nao)*p->Nao)/(KmNa3+((p->Nao*p->Nao)*p->Nao)))/(KmCa+p->Cao))/(1.+(k_sat*(exp((((((gamma-(1.))*F)*V)/R)/T))))))/1000.);
  GlobalData_t vrow_36 = (V*p->GbCa);
  GlobalData_t vrow_7 = (p->GNa*(V-(E_Na)));
  GlobalData_t ICaL = (((vrow_29*sv->d)*sv->f)*sv->f_Ca);
  GlobalData_t IKACh = (((p->GACh*(10.0/(1.0+(9.13652/(pow(p->ACh,0.477811))))))*(0.0517+(0.4516/(1.0+(exp(((V+59.53)/17.18)))))))*(V-(E_K)));
  GlobalData_t INa = (((((vrow_7*sv->m)*sv->m)*sv->m)*sv->h)*sv->j);
  GlobalData_t INaCa = (vrow_31-((sv->Cai*vrow_32)));
  GlobalData_t INaK = ((((p->maxINaK*f_NaK)/(1.+(pow((KmNai/Nai),1.5))))*p->Ko)/(p->Ko+KmKo));
  GlobalData_t Irel = ((((((p->factorGrel*sv->u)*sv->u)*sv->v)*k_rel)*sv->w)*(sv->Ca_rel-(conCa)));
  GlobalData_t carow_3 = (((p->maxIpCa*conCa)/(0.0005+conCa))-((((((p->GbCa*R)*T)/2.)/F)*(log((p->Cao/conCa))))));
  GlobalData_t vrow_13 = (p->Gto*(V-(E_K)));
  GlobalData_t vrow_18 = ((p->factorGKur*GKur)*(V-(E_K)));
  GlobalData_t vrow_21 = ((p->GKr*(V-(E_K)))/(1.+(exp(((V+15.)/22.4)))));
  GlobalData_t vrow_24 = (p->GKs*(V-(E_K)));
  GlobalData_t vrow_8 = ((p->GK1*(V-(E_K)))/(1.+(exp((0.07*(V+80.))))));
  GlobalData_t IK1 = vrow_8;
  GlobalData_t IKr = (vrow_21*sv->xr);
  GlobalData_t IKs = ((vrow_24*sv->xs)*sv->xs);
  GlobalData_t IKur = ((((vrow_18*sv->ua)*sv->ua)*sv->ua)*sv->ui);
  GlobalData_t IpCa = (carow_3+vrow_36);
  GlobalData_t Ito = ((((vrow_13*sv->oa)*sv->oa)*sv->oa)*sv->oi);
  Iion = (((((((((((INa+IK1)+Ito)+IKur)+IKr)+IKs)+ICaL)+IpCa)+INaCa)+IbNa)+INaK)+IKACh);
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->Ca_rel);
  fprintf(file, "%4.12f\t", sv->Ca_up);
  fprintf(file, "%4.12f\t", sv->Cai);
  fprintf(file, "%4.12f\t", ICaL);
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", IKur);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", Iion);
  fprintf(file, "%4.12f\t", IpCa);
  fprintf(file, "%4.12f\t", Irel);
  fprintf(file, "%4.12f\t", Ito);
  fprintf(file, "%4.12f\t", Itr);
  fprintf(file, "%4.12f\t", V);
  fprintf(file, "%4.12f\t", sv->h);
  fprintf(file, "%4.12f\t", sv->j);
  fprintf(file, "%4.12f\t", sv->m);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        