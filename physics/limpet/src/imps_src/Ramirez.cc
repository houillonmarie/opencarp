// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Rafael J. Ramirez, Stanley Nattel, and Marc Courtemanche
*  Year: 2000
*  Title: Mathematical analysis of canine atrial action potentials: rate, regional factors, and electrical remodeling
*  Journal: American Journal of Physiology-Heart and Circulatory Physiology, 279(4), 1767-1785
*  DOI: 10.1152/ajpheart.2000.279.4.H1767
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Ramirez.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Ramirez(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Ramirez( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define C_B1a (GlobalData_t)(3.79138232501097e-05)
#define C_B1b (GlobalData_t)(0.0811764705882353)
#define C_B1c (GlobalData_t)(0.00705882352941176)
#define C_B1d (GlobalData_t)(0.00537112496043221)
#define C_B1e (GlobalData_t)(11.5)
#define C_Fn1 (GlobalData_t)(9.648e-13)
#define C_Fn2 (GlobalData_t)(2.5910306809e-13)
#define C_RelCSQN (GlobalData_t)(3.1)
#define C_dCaup (GlobalData_t)(0.0869565217391304)
#define C_dConCa_rel (GlobalData_t)(8.)
#define CaCmdn_init (GlobalData_t)(1.856e-3)
#define CaCsqn_init (GlobalData_t)(6.432)
#define CaRel_init (GlobalData_t)(1.87)
#define CaTrpn_init (GlobalData_t)(7.022e-3)
#define CaUp_init (GlobalData_t)(1.87)
#define Cai_init (GlobalData_t)(1.31e-4)
#define Cli_init (GlobalData_t)(29.26)
#define Cm (GlobalData_t)(100.)
#define Cm_F_Vi (GlobalData_t)(7.582764650021945e-05)
#define F (GlobalData_t)(96.4867)
#define K_Q10 (GlobalData_t)(1.)
#define K_up (GlobalData_t)(0.00092)
#define Ki_init (GlobalData_t)(137.)
#define KmCa (GlobalData_t)(1.38)
#define KmKo (GlobalData_t)(1.5)
#define KmNa (GlobalData_t)(87.5)
#define KmNai (GlobalData_t)(10.)
#define NGBAR (GlobalData_t)(1.)
#define Nai_init (GlobalData_t)(13.86)
#define Oa_init (GlobalData_t)(7.35e-2)
#define Oi_init (GlobalData_t)(0.999)
#define QCa_init (GlobalData_t)(0.0)
#define R (GlobalData_t)(8.3143)
#define T (GlobalData_t)(310.)
#define Ua_init (GlobalData_t)(6.04e-2)
#define Ui_init (GlobalData_t)(0.999)
#define V_init (GlobalData_t)(-82.72)
#define Vcell (GlobalData_t)(20100.)
#define Vi (GlobalData_t)(13668.)
#define Vrel (GlobalData_t)(96.48)
#define Vup (GlobalData_t)(1109.52)
#define d_init (GlobalData_t)(5.45e-6)
#define fCa_init (GlobalData_t)(0.65)
#define f_init (GlobalData_t)(0.999)
#define gamma (GlobalData_t)(0.35)
#define h_init (GlobalData_t)(0.975)
#define j_init (GlobalData_t)(0.983)
#define k_rel (GlobalData_t)(30.)
#define k_sat (GlobalData_t)(0.1)
#define m_init (GlobalData_t)(2.25e-3)
#define maxCmdn (GlobalData_t)(0.045)
#define maxCsqn (GlobalData_t)(10.)
#define maxTrpn (GlobalData_t)(0.35)
#define tau_QCa (GlobalData_t)(2.)
#define tau_fCa (GlobalData_t)(2.)
#define u_init (GlobalData_t)(0.)
#define v_init (GlobalData_t)(1.)
#define w_init (GlobalData_t)(0.999)
#define xr_init (GlobalData_t)(8.647e-7)
#define xs_init (GlobalData_t)(1.853e-2)
#define KmNa3 (GlobalData_t)(((KmNa*KmNa)*KmNa))



void initialize_params_Ramirez( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Ramirez_Params *p = (Ramirez_Params *)IF->params;

  // Compute the regional constants
  {
    p->Cao = 1.8;
    p->Clo = 132.0;
    p->GCaL = 0.24;
    p->GClCa = 0.3;
    p->GK1 = 0.15;
    p->GKr = 0.06984;
    p->GKs = 0.0561;
    p->GNa = 7.8;
    p->GbCa = 0.00113;
    p->GbNa = 0.000674;
    p->Gto = 0.19824;
    p->Ko = 5.4;
    p->Nao = 140.;
    p->maxCa_up = 15.;
    p->maxINaCa = 1600.;
    p->maxINaK = 0.60;
    p->maxIpCa = 0.275;
    p->maxIup = 0.005;
    p->tau_tr = 180.;
    p->tau_u = 8.;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_Ramirez;

}


// Define the parameters for the lookup tables
enum Tables {
  Cai_TAB,
  V_TAB,
  fn_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum Cai_TableIndex {
  IpCa_idx,
  fCa_rush_larsen_A_idx,
  log_Cao_Cai_idx,
  NROWS_Cai
};

enum V_TableIndex {
  GKur_idx,
  IK1_term_idx,
  IKr_term_idx,
  INaCa_term1_idx,
  INaCa_term2_idx,
  Oa_rush_larsen_A_idx,
  Oa_rush_larsen_B_idx,
  Oi_rush_larsen_A_idx,
  Oi_rush_larsen_B_idx,
  Ua_rush_larsen_A_idx,
  Ua_rush_larsen_B_idx,
  Ui_rush_larsen_A_idx,
  Ui_rush_larsen_B_idx,
  d_rush_larsen_A_idx,
  d_rush_larsen_B_idx,
  f_NaK_idx,
  f_rush_larsen_A_idx,
  f_rush_larsen_B_idx,
  h_rush_larsen_A_idx,
  h_rush_larsen_B_idx,
  j_rush_larsen_A_idx,
  j_rush_larsen_B_idx,
  m_rush_larsen_A_idx,
  m_rush_larsen_B_idx,
  w_rush_larsen_A_idx,
  w_rush_larsen_B_idx,
  xr_rush_larsen_A_idx,
  xr_rush_larsen_B_idx,
  xs_rush_larsen_A_idx,
  xs_rush_larsen_B_idx,
  NROWS_V
};

enum fn_TableIndex {
  QCa_rush_larsen_A_idx,
  u_rush_larsen_A_idx,
  v_rush_larsen_A_idx,
  v_rush_larsen_B_idx,
  NROWS_fn
};



void construct_tables_Ramirez( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Ramirez_Params *p = (Ramirez_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double QCa_rush_larsen_B = (exp(((-dt)/tau_QCa)));
  double QCa_rush_larsen_C = (expm1(((-dt)/tau_QCa)));
  double fCa_rush_larsen_B = (exp(((-dt)/tau_fCa)));
  double fCa_rush_larsen_C = (expm1(((-dt)/tau_fCa)));
  double sigma = (((exp((p->Nao/67.3)))-(1.))/7.);
  double u_rush_larsen_B = (exp(((-dt)/p->tau_u)));
  double u_rush_larsen_C = (expm1(((-dt)/p->tau_u)));
  
  // Create the Cai lookup table
  LUT* Cai_tab = &IF->tables[Cai_TAB];
  LUT_alloc(Cai_tab, NROWS_Cai, 1e-7, 1e-2, 1e-7, "Ramirez Cai");
  for (int __i=Cai_tab->mn_ind; __i<=Cai_tab->mx_ind; __i++) {
    double Cai = Cai_tab->res*__i;
    LUT_data_t* Cai_row = Cai_tab->tab[__i];
    Cai_row[IpCa_idx] = ((p->maxIpCa*Cai)/(0.0005+Cai));
    double fCa_inf = (0.29+(0.8/(1.+(exp(((Cai-(0.00012))/0.00006))))));
    Cai_row[log_Cao_Cai_idx] = (log((p->Cao/Cai)));
    Cai_row[fCa_rush_larsen_A_idx] = ((-fCa_inf)*fCa_rush_larsen_C);
  }
  check_LUT(Cai_tab);
  
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -800, 800, 0.01, "Ramirez V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    V_row[GKur_idx] = (0.00855+(0.0779/(1.+(exp(((V+11.)/-16.))))));
    V_row[IK1_term_idx] = (1./(1.+(exp((0.07*(V+80.))))));
    V_row[IKr_term_idx] = (0.07+(0.58/(1.+(exp(((V+15.)/22.4))))));
    V_row[INaCa_term1_idx] = ((((p->maxINaCa*(exp((((((gamma-(1.))*F)*V)/R)/T))))/(1.+(k_sat*(exp((((((gamma-(1.))*F)*V)/R)/T))))))/(KmNa3+((p->Nao*p->Nao)*p->Nao)))/(KmCa+p->Cao));
    double Oa_inf = (pow((1.0+(exp(((V+0.5)/-10.5)))),(-1./3.)));
    double Oi_inf = (1./(1.0+(exp(((V+43.377)/6.45)))));
    double Ua_inf = (pow((1.+(exp(((V+2.81)/-9.49)))),(-1./3.)));
    double Ui_inf = (1./(1.+(exp(((V-(99.45))/27.48)))));
    double a_h = ((V>=-40.) ? 0. : (0.135*(exp(((V+80.)/-6.8)))));
    double a_j = ((V<-40.) ? ((((-127140.*(exp((0.2444*V))))-((3.474e-5*(exp((-0.04391*V))))))*(V+37.78))/(1.+(exp((0.311*(V+79.23)))))) : 0.);
    double a_m = ((V==-47.13) ? 3.2 : ((0.32*(V+47.13))/(1.-((exp((-0.1*(V+47.13))))))));
    double a_oa = (0.65/((exp(((V+18.)/-8.5)))+(exp(((V-(16.))/-59.0)))));
    double a_oi = (1./(6.2+(exp(((V+105.2)/9.85)))));
    double a_ua = (1.47/((exp(((V+33.2)/-30.63)))+(exp(((V-(27.6))/-30.65)))));
    double a_ui = (1./(21.+(exp(((V-(185.))/-28.)))));
    double aa_xr = ((V!=248.) ? ((0.04*(V-(248.0)))/(1.-((exp(((V-(248.))/-28.)))))) : (0.04*28.));
    double aa_xs = ((V!=-28.5) ? ((0.00001*(V+28.5))/(1.-((exp(((V+28.5)/-115.)))))) : (0.00001*115.));
    double b_h = ((V>=-40.) ? ((1./0.13)/(1.+(exp(((-(V+10.66))/11.1))))) : ((3.56*(exp((0.079*V))))+(3.1e5*(exp((0.35*V))))));
    double b_j = ((V>=-40.) ? ((0.3*(exp((-2.535e-7*V))))/(1.+(exp((-0.1*(V+32.)))))) : ((0.1212*(exp((-0.01052*V))))/(1.+(exp((-0.1378*(V+40.14)))))));
    double b_m = (0.08*(exp(((-V)/11.))));
    double b_oa = (1.20/(2.2+(exp(((V+75.)/18.)))));
    double b_oi = (1./(7.54+(exp(((V-(8.89))/-12.87)))));
    double b_ua = (0.42/((exp(((V+26.64)/2.94)))+(exp(((V+44.41)/20.36)))));
    double b_ui = (exp(((V-(158.))/16.)));
    double bb_xr = ((V!=-163.) ? ((0.028*(V+163.))/((exp(((V+163.)/21.)))-(1.))) : (0.028*21.));
    double bb_xs = ((V!=-28.5) ? ((0.00023*(V+28.5))/((exp(((V+28.5)/3.3)))-(1.))) : (0.00023*3.3));
    double d_inf = (1./(1.+(exp(((V+10.)/-6.24)))));
    V_row[f_NaK_idx] = (1./((1.+(0.1245*(exp(((((-0.1*F)*V)/R)/T)))))+((0.0365*sigma)*(exp(((((-F)*V)/R)/T))))));
    double f_inf = (1./(1.+(exp(((V+24.6)/6.2)))));
    double tau_d = ((V==-10.) ? (((1./6.24)/0.035)/2.) : ((((1.-((exp(((-(V+10.))/6.24)))))/0.035)/(V+10.))/(1.+(exp(((-(V+10.))/6.24))))));
    double tau_f = (400./(1.+(4.5*(exp(((-0.0007*(V-(9.)))*(V-(9.0))))))));
    double tau_w = (((6.*(1.-((exp(((7.9-(V))/5.))))))/(1.+(0.3*(exp(((7.9-(V))/5.))))))/(V-(7.9)));
    double w_inf = (1.-((1./(1.+(exp(((40.-(V))/17.)))))));
    double xr_inf = (1./(1.+(exp(((V+7.654)/-5.377)))));
    double xs_inf = (1./(sqrt((1.+(exp(((V-(13.))/-12.)))))));
    V_row[INaCa_term2_idx] = ((exp((((V*F)/R)/T)))*V_row[INaCa_term1_idx]);
    V_row[d_rush_larsen_B_idx] = (exp(((-dt)/tau_d)));
    double d_rush_larsen_C = (expm1(((-dt)/tau_d)));
    V_row[f_rush_larsen_B_idx] = (exp(((-dt)/tau_f)));
    double f_rush_larsen_C = (expm1(((-dt)/tau_f)));
    V_row[h_rush_larsen_A_idx] = (((-a_h)/(a_h+b_h))*(expm1(((-dt)*(a_h+b_h)))));
    V_row[h_rush_larsen_B_idx] = (exp(((-dt)*(a_h+b_h))));
    V_row[j_rush_larsen_A_idx] = (((-a_j)/(a_j+b_j))*(expm1(((-dt)*(a_j+b_j)))));
    V_row[j_rush_larsen_B_idx] = (exp(((-dt)*(a_j+b_j))));
    V_row[m_rush_larsen_A_idx] = (((-a_m)/(a_m+b_m))*(expm1(((-dt)*(a_m+b_m)))));
    V_row[m_rush_larsen_B_idx] = (exp(((-dt)*(a_m+b_m))));
    double tau_Oa = ((1./(a_oa+b_oa))/K_Q10);
    double tau_Oi = ((1./(a_oi+b_oi))/K_Q10);
    double tau_Ua = (1./(a_ua+b_ua));
    double tau_Ui = (1./(a_ui+b_ui));
    double tau_xr = (1./(aa_xr+bb_xr));
    double tau_xs = (1.0/(aa_xs+bb_xs));
    V_row[w_rush_larsen_B_idx] = (exp(((-dt)/tau_w)));
    double w_rush_larsen_C = (expm1(((-dt)/tau_w)));
    V_row[Oa_rush_larsen_B_idx] = (exp(((-dt)/tau_Oa)));
    double Oa_rush_larsen_C = (expm1(((-dt)/tau_Oa)));
    V_row[Oi_rush_larsen_B_idx] = (exp(((-dt)/tau_Oi)));
    double Oi_rush_larsen_C = (expm1(((-dt)/tau_Oi)));
    V_row[Ua_rush_larsen_B_idx] = (exp(((-dt)/tau_Ua)));
    double Ua_rush_larsen_C = (expm1(((-dt)/tau_Ua)));
    V_row[Ui_rush_larsen_B_idx] = (exp(((-dt)/tau_Ui)));
    double Ui_rush_larsen_C = (expm1(((-dt)/tau_Ui)));
    V_row[d_rush_larsen_A_idx] = ((-d_inf)*d_rush_larsen_C);
    V_row[f_rush_larsen_A_idx] = ((-f_inf)*f_rush_larsen_C);
    V_row[w_rush_larsen_A_idx] = ((-w_inf)*w_rush_larsen_C);
    V_row[xr_rush_larsen_B_idx] = (exp(((-dt)/tau_xr)));
    double xr_rush_larsen_C = (expm1(((-dt)/tau_xr)));
    V_row[xs_rush_larsen_B_idx] = (exp(((-dt)/tau_xs)));
    double xs_rush_larsen_C = (expm1(((-dt)/tau_xs)));
    V_row[Oa_rush_larsen_A_idx] = ((-Oa_inf)*Oa_rush_larsen_C);
    V_row[Oi_rush_larsen_A_idx] = ((-Oi_inf)*Oi_rush_larsen_C);
    V_row[Ua_rush_larsen_A_idx] = ((-Ua_inf)*Ua_rush_larsen_C);
    V_row[Ui_rush_larsen_A_idx] = ((-Ui_inf)*Ui_rush_larsen_C);
    V_row[xr_rush_larsen_A_idx] = ((-xr_inf)*xr_rush_larsen_C);
    V_row[xs_rush_larsen_A_idx] = ((-xs_inf)*xs_rush_larsen_C);
  }
  check_LUT(V_tab);
  
  
  // Create the fn lookup table
  LUT* fn_tab = &IF->tables[fn_TAB];
  LUT_alloc(fn_tab, NROWS_fn, -20e-12, 300e-12, 1e-16, "Ramirez fn");
  for (int __i=fn_tab->mn_ind; __i<=fn_tab->mx_ind; __i++) {
    double fn = fn_tab->res*__i;
    LUT_data_t* fn_row = fn_tab->tab[__i];
    double QCa_inf = (1.-((1./(1.+(pow((fn/1.1e-10),3.))))));
    double tau_v = (1.91+(2.09/(1.+(exp(((3.4175e-13-(fn))/13.67e-16))))));
    double u_inf = (1./(1.+(exp(((3.4175e-13-(fn))/13.67e-16)))));
    double v_inf = (1.-((1./(1.+(exp(((6.835e-14-(fn))/13.67e-16)))))));
    fn_row[QCa_rush_larsen_A_idx] = ((-QCa_inf)*QCa_rush_larsen_C);
    fn_row[u_rush_larsen_A_idx] = ((-u_inf)*u_rush_larsen_C);
    fn_row[v_rush_larsen_B_idx] = (exp(((-dt)/tau_v)));
    double v_rush_larsen_C = (expm1(((-dt)/tau_v)));
    fn_row[v_rush_larsen_A_idx] = ((-v_inf)*v_rush_larsen_C);
  }
  check_LUT(fn_tab);
  

}



void    initialize_sv_Ramirez( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Ramirez_Params *p = (Ramirez_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Ramirez_state) );
  Ramirez_state *sv_base = (Ramirez_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double QCa_rush_larsen_B = (exp(((-dt)/tau_QCa)));
  double QCa_rush_larsen_C = (expm1(((-dt)/tau_QCa)));
  double fCa_rush_larsen_B = (exp(((-dt)/tau_fCa)));
  double fCa_rush_larsen_C = (expm1(((-dt)/tau_fCa)));
  double sigma = (((exp((p->Nao/67.3)))-(1.))/7.);
  double u_rush_larsen_B = (exp(((-dt)/p->tau_u)));
  double u_rush_larsen_C = (expm1(((-dt)/p->tau_u)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Ramirez_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    // Initialize the rest of the nodal variables
    sv->CaCmdn = CaCmdn_init;
    sv->CaCsqn = CaCsqn_init;
    sv->CaRel = CaRel_init;
    sv->CaTrpn = CaTrpn_init;
    sv->CaUp = CaUp_init;
    sv->Cai = Cai_init;
    sv->Cli = Cli_init;
    sv->Ki = Ki_init;
    sv->Nai = Nai_init;
    sv->Oa = Oa_init;
    sv->Oi = Oi_init;
    sv->QCa = QCa_init;
    sv->Ua = Ua_init;
    sv->Ui = Ui_init;
    V = V_init;
    sv->d = d_init;
    sv->f = f_init;
    sv->fCa = fCa_init;
    sv->h = h_init;
    sv->j = j_init;
    sv->m = m_init;
    sv->u = u_init;
    sv->v = v_init;
    sv->w = w_init;
    sv->xr = xr_init;
    sv->xs = xs_init;
    double E_Cl = ((((-R)*T)/F)*(log((p->Clo/sv->Cli))));
    double E_K = (((R*T)/F)*(log((p->Ko/sv->Ki))));
    double E_Na = (((R*T)/F)*(log((p->Nao/sv->Nai))));
    double GKur = (0.00855+(0.0779/(1.+(exp(((V+11.)/-16.))))));
    double ICaL = (((((NGBAR*p->GCaL)*(V-(65.)))*sv->d)*sv->f)*sv->fCa);
    double IK1_term = (1./(1.+(exp((0.07*(V+80.))))));
    double IKr_term = (0.07+(0.58/(1.+(exp(((V+15.)/22.4))))));
    double INaCa_term1 = ((((p->maxINaCa*(exp((((((gamma-(1.))*F)*V)/R)/T))))/(1.+(k_sat*(exp((((((gamma-(1.))*F)*V)/R)/T))))))/(KmNa3+((p->Nao*p->Nao)*p->Nao)))/(KmCa+p->Cao));
    double IpCa = ((p->maxIpCa*sv->Cai)/(0.0005+sv->Cai));
    double f_NaK = (1./((1.+(0.1245*(exp(((((-0.1*F)*V)/R)/T)))))+((0.0365*sigma)*(exp(((((-F)*V)/R)/T))))));
    double log_Cao_Cai = (log((p->Cao/sv->Cai)));
    double pow_KmNaINai_15 = (pow((KmNai/sv->Nai),1.5));
    double IClCa = ((sv->QCa*p->GClCa)*(V-(E_Cl)));
    double IK1 = ((p->GK1*(V-(E_K)))*IK1_term);
    double IKr = (((p->GKr*(V-(E_K)))*IKr_term)*sv->xr);
    double IKs = (((p->GKs*(V-(E_K)))*sv->xs)*sv->xs);
    double IKur = (((((GKur*(V-(E_K)))*sv->Ua)*sv->Ua)*sv->Ua)*sv->Ui);
    double INa = ((((((p->GNa*(V-(E_Na)))*sv->m)*sv->m)*sv->m)*sv->h)*sv->j);
    double INaCa_term2 = ((exp((((V*F)/R)/T)))*INaCa_term1);
    double INaK = ((((p->maxINaK*f_NaK)/(1.+pow_KmNaINai_15))*p->Ko)/(p->Ko+KmKo));
    double IbCa = ((V*p->GbCa)-((((((p->GbCa*R)*T)/2.)/F)*log_Cao_Cai)));
    double IbNa = (p->GbNa*(V-(E_Na)));
    double Ito = (((((p->Gto*(V-(E_K)))*sv->Oa)*sv->Oa)*sv->Oa)*sv->Oi);
    double INaCa = (((((INaCa_term2*sv->Nai)*sv->Nai)*sv->Nai)*p->Cao)-(((((INaCa_term1*p->Nao)*p->Nao)*p->Nao)*sv->Cai)));
    Iion = ((((((((((((INa+IK1)+Ito)+IKur)+IKr)+IKs)+ICaL)+IpCa)+INaCa)+INaK)+IClCa)+IbCa)+IbNa);
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Ramirez(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Ramirez_Params *p  = (Ramirez_Params *)IF->params;
  Ramirez_state *sv_base = (Ramirez_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t QCa_rush_larsen_B = (exp(((-dt)/tau_QCa)));
  GlobalData_t QCa_rush_larsen_C = (expm1(((-dt)/tau_QCa)));
  GlobalData_t fCa_rush_larsen_B = (exp(((-dt)/tau_fCa)));
  GlobalData_t fCa_rush_larsen_C = (expm1(((-dt)/tau_fCa)));
  GlobalData_t sigma = (((exp((p->Nao/67.3)))-(1.))/7.);
  GlobalData_t u_rush_larsen_B = (exp(((-dt)/p->tau_u)));
  GlobalData_t u_rush_larsen_C = (expm1(((-dt)/p->tau_u)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Ramirez_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Cai_row[NROWS_Cai];
    LUT_interpRow(&IF->tables[Cai_TAB], sv->Cai, __i, Cai_row);
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t E_Cl = ((((-R)*T)/F)*(log((p->Clo/sv->Cli))));
    GlobalData_t E_K = (((R*T)/F)*(log((p->Ko/sv->Ki))));
    GlobalData_t E_Na = (((R*T)/F)*(log((p->Nao/sv->Nai))));
    GlobalData_t ICaL = (((((NGBAR*p->GCaL)*(V-(65.)))*sv->d)*sv->f)*sv->fCa);
    GlobalData_t pow_KmNaINai_15 = (pow((KmNai/sv->Nai),1.5));
    GlobalData_t IClCa = ((sv->QCa*p->GClCa)*(V-(E_Cl)));
    GlobalData_t IK1 = ((p->GK1*(V-(E_K)))*V_row[IK1_term_idx]);
    GlobalData_t IKr = (((p->GKr*(V-(E_K)))*V_row[IKr_term_idx])*sv->xr);
    GlobalData_t IKs = (((p->GKs*(V-(E_K)))*sv->xs)*sv->xs);
    GlobalData_t IKur = (((((V_row[GKur_idx]*(V-(E_K)))*sv->Ua)*sv->Ua)*sv->Ua)*sv->Ui);
    GlobalData_t INa = ((((((p->GNa*(V-(E_Na)))*sv->m)*sv->m)*sv->m)*sv->h)*sv->j);
    GlobalData_t INaK = ((((p->maxINaK*V_row[f_NaK_idx])/(1.+pow_KmNaINai_15))*p->Ko)/(p->Ko+KmKo));
    GlobalData_t IbCa = ((V*p->GbCa)-((((((p->GbCa*R)*T)/2.)/F)*Cai_row[log_Cao_Cai_idx])));
    GlobalData_t IbNa = (p->GbNa*(V-(E_Na)));
    GlobalData_t Ito = (((((p->Gto*(V-(E_K)))*sv->Oa)*sv->Oa)*sv->Oa)*sv->Oi);
    GlobalData_t INaCa = (((((V_row[INaCa_term2_idx]*sv->Nai)*sv->Nai)*sv->Nai)*p->Cao)-(((((V_row[INaCa_term1_idx]*p->Nao)*p->Nao)*p->Nao)*sv->Cai)));
    Iion = ((((((((((((INa+IK1)+Ito)+IKur)+IKr)+IKs)+ICaL)+Cai_row[IpCa_idx])+INaCa)+INaK)+IClCa)+IbCa)+IbNa);
    
    
    //Complete Forward Euler Update
    GlobalData_t Irel = (((((sv->u*sv->u)*sv->v)*sv->w)*k_rel)*(sv->CaRel-(sv->Cai)));
    GlobalData_t Itr = ((sv->CaUp-(sv->CaRel))/p->tau_tr);
    GlobalData_t Iup = ((p->maxIup/(1.+(K_up/sv->Cai)))-(((p->maxIup/p->maxCa_up)*sv->CaUp)));
    GlobalData_t diff_CaCmdn = (((sv->Cai*200.)*(maxCmdn-(sv->CaCmdn)))-((0.476*sv->CaCmdn)));
    GlobalData_t diff_CaCsqn = (((sv->CaRel*0.48)*(maxCsqn-(sv->CaCsqn)))-((0.4*sv->CaCsqn)));
    GlobalData_t diff_CaTrpn = (((sv->Cai*78.4)*(maxTrpn-(sv->CaTrpn)))-((0.392*sv->CaTrpn)));
    GlobalData_t diff_Cli = (Cm_F_Vi*IClCa);
    GlobalData_t diff_Ki = ((-Cm_F_Vi)*((((((-2.*INaK)+IK1)+Ito)+IKur)+IKr)+IKs));
    GlobalData_t diff_Nai = ((-Cm_F_Vi)*((INa+(3.*(INaK+INaCa)))+IbNa));
    GlobalData_t diff_CaRel = ((Itr-(Irel))-((C_RelCSQN*diff_CaCsqn)));
    GlobalData_t diff_CaUp = (Iup-((Itr*C_dCaup)));
    GlobalData_t diff_Cai = (((((C_B1a*(((INaCa+INaCa)-(Cai_row[IpCa_idx]))-(ICaL)))-((C_B1b*Iup)))+(C_B1c*Irel))-(diff_CaTrpn))-(diff_CaCmdn));
    GlobalData_t CaCmdn_new = sv->CaCmdn+diff_CaCmdn*dt;
    GlobalData_t CaCsqn_new = sv->CaCsqn+diff_CaCsqn*dt;
    GlobalData_t CaRel_new = sv->CaRel+diff_CaRel*dt;
    GlobalData_t CaTrpn_new = sv->CaTrpn+diff_CaTrpn*dt;
    GlobalData_t CaUp_new = sv->CaUp+diff_CaUp*dt;
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    GlobalData_t Cli_new = sv->Cli+diff_Cli*dt;
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t fn = ((C_Fn1*Irel)-((C_Fn2*(ICaL-((0.4*INaCa))))));
    LUT_data_t fn_row[NROWS_fn];
    LUT_interpRow(&IF->tables[fn_TAB], fn, __i, fn_row);
    GlobalData_t d_rush_larsen_B = V_row[d_rush_larsen_B_idx];
    GlobalData_t fCa_rush_larsen_A = Cai_row[fCa_rush_larsen_A_idx];
    GlobalData_t f_rush_larsen_B = V_row[f_rush_larsen_B_idx];
    GlobalData_t h_rush_larsen_A = V_row[h_rush_larsen_A_idx];
    GlobalData_t h_rush_larsen_B = V_row[h_rush_larsen_B_idx];
    GlobalData_t j_rush_larsen_A = V_row[j_rush_larsen_A_idx];
    GlobalData_t j_rush_larsen_B = V_row[j_rush_larsen_B_idx];
    GlobalData_t m_rush_larsen_A = V_row[m_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_B = V_row[m_rush_larsen_B_idx];
    GlobalData_t w_rush_larsen_B = V_row[w_rush_larsen_B_idx];
    GlobalData_t Oa_rush_larsen_B = V_row[Oa_rush_larsen_B_idx];
    GlobalData_t Oi_rush_larsen_B = V_row[Oi_rush_larsen_B_idx];
    GlobalData_t QCa_rush_larsen_A = fn_row[QCa_rush_larsen_A_idx];
    GlobalData_t Ua_rush_larsen_B = V_row[Ua_rush_larsen_B_idx];
    GlobalData_t Ui_rush_larsen_B = V_row[Ui_rush_larsen_B_idx];
    GlobalData_t d_rush_larsen_A = V_row[d_rush_larsen_A_idx];
    GlobalData_t f_rush_larsen_A = V_row[f_rush_larsen_A_idx];
    GlobalData_t u_rush_larsen_A = fn_row[u_rush_larsen_A_idx];
    GlobalData_t v_rush_larsen_B = fn_row[v_rush_larsen_B_idx];
    GlobalData_t w_rush_larsen_A = V_row[w_rush_larsen_A_idx];
    GlobalData_t xr_rush_larsen_B = V_row[xr_rush_larsen_B_idx];
    GlobalData_t xs_rush_larsen_B = V_row[xs_rush_larsen_B_idx];
    GlobalData_t Oa_rush_larsen_A = V_row[Oa_rush_larsen_A_idx];
    GlobalData_t Oi_rush_larsen_A = V_row[Oi_rush_larsen_A_idx];
    GlobalData_t Ua_rush_larsen_A = V_row[Ua_rush_larsen_A_idx];
    GlobalData_t Ui_rush_larsen_A = V_row[Ui_rush_larsen_A_idx];
    GlobalData_t v_rush_larsen_A = fn_row[v_rush_larsen_A_idx];
    GlobalData_t xr_rush_larsen_A = V_row[xr_rush_larsen_A_idx];
    GlobalData_t xs_rush_larsen_A = V_row[xs_rush_larsen_A_idx];
    GlobalData_t Oa_new = Oa_rush_larsen_A+Oa_rush_larsen_B*sv->Oa;
    GlobalData_t Oi_new = Oi_rush_larsen_A+Oi_rush_larsen_B*sv->Oi;
    GlobalData_t QCa_new = QCa_rush_larsen_A+QCa_rush_larsen_B*sv->QCa;
    GlobalData_t Ua_new = Ua_rush_larsen_A+Ua_rush_larsen_B*sv->Ua;
    GlobalData_t Ui_new = Ui_rush_larsen_A+Ui_rush_larsen_B*sv->Ui;
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f_new = f_rush_larsen_A+f_rush_larsen_B*sv->f;
    GlobalData_t fCa_new = fCa_rush_larsen_A+fCa_rush_larsen_B*sv->fCa;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t u_new = u_rush_larsen_A+u_rush_larsen_B*sv->u;
    GlobalData_t v_new = v_rush_larsen_A+v_rush_larsen_B*sv->v;
    GlobalData_t w_new = w_rush_larsen_A+w_rush_larsen_B*sv->w;
    GlobalData_t xr_new = xr_rush_larsen_A+xr_rush_larsen_B*sv->xr;
    GlobalData_t xs_new = xs_rush_larsen_A+xs_rush_larsen_B*sv->xs;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->CaCmdn = CaCmdn_new;
    sv->CaCsqn = CaCsqn_new;
    sv->CaRel = CaRel_new;
    sv->CaTrpn = CaTrpn_new;
    sv->CaUp = CaUp_new;
    sv->Cai = Cai_new;
    sv->Cli = Cli_new;
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->Oa = Oa_new;
    sv->Oi = Oi_new;
    sv->QCa = QCa_new;
    sv->Ua = Ua_new;
    sv->Ui = Ui_new;
    sv->d = d_new;
    sv->f = f_new;
    sv->fCa = fCa_new;
    sv->h = h_new;
    sv->j = j_new;
    sv->m = m_new;
    sv->u = u_new;
    sv->v = v_new;
    sv->w = w_new;
    sv->xr = xr_new;
    sv->xs = xs_new;
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_Ramirez(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Ramirez_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->CaCmdn\n"
        "sv->CaCsqn\n"
        "sv->CaRel\n"
        "sv->CaTrpn\n"
        "sv->CaUp\n"
        "sv->Cai\n"
        "sv->Cli\n"
        "ICaL\n"
        "IClCa\n"
        "IK1\n"
        "IKr\n"
        "IKs\n"
        "IKur\n"
        "INa\n"
        "INaCa\n"
        "INaK\n"
        "IbCa\n"
        "IbNa\n"
        "IpCa\n"
        "Irel\n"
        "Ito\n"
        "Itr\n"
        "Iup\n"
        "sv->Ki\n"
        "sv->Nai\n"
        "sv->Oa\n"
        "sv->Oi\n"
        "sv->QCa\n"
        "sv->Ua\n"
        "sv->Ui\n"
        "V\n"
        "sv->d\n"
        "sv->f\n"
        "sv->fCa\n"
        "sv->h\n"
        "sv->j\n"
        "sv->m\n"
        "sv->u\n"
        "sv->v\n"
        "sv->w\n"
        "sv->xr\n"
        "sv->xs\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Ramirez_Params *p  = (Ramirez_Params *)IF->params;

  Ramirez_state *sv_base = (Ramirez_state *)IF->sv_tab.y;
  Ramirez_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t QCa_rush_larsen_B = (exp(((-dt)/tau_QCa)));
  GlobalData_t QCa_rush_larsen_C = (expm1(((-dt)/tau_QCa)));
  GlobalData_t fCa_rush_larsen_B = (exp(((-dt)/tau_fCa)));
  GlobalData_t fCa_rush_larsen_C = (expm1(((-dt)/tau_fCa)));
  GlobalData_t sigma = (((exp((p->Nao/67.3)))-(1.))/7.);
  GlobalData_t u_rush_larsen_B = (exp(((-dt)/p->tau_u)));
  GlobalData_t u_rush_larsen_C = (expm1(((-dt)/p->tau_u)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e-3;
  
  
  GlobalData_t E_Cl = ((((-R)*T)/F)*(log((p->Clo/sv->Cli))));
  GlobalData_t E_K = (((R*T)/F)*(log((p->Ko/sv->Ki))));
  GlobalData_t E_Na = (((R*T)/F)*(log((p->Nao/sv->Nai))));
  GlobalData_t GKur = (0.00855+(0.0779/(1.+(exp(((V+11.)/-16.))))));
  GlobalData_t ICaL = (((((NGBAR*p->GCaL)*(V-(65.)))*sv->d)*sv->f)*sv->fCa);
  GlobalData_t IK1_term = (1./(1.+(exp((0.07*(V+80.))))));
  GlobalData_t IKr_term = (0.07+(0.58/(1.+(exp(((V+15.)/22.4))))));
  GlobalData_t INaCa_term1 = ((((p->maxINaCa*(exp((((((gamma-(1.))*F)*V)/R)/T))))/(1.+(k_sat*(exp((((((gamma-(1.))*F)*V)/R)/T))))))/(KmNa3+((p->Nao*p->Nao)*p->Nao)))/(KmCa+p->Cao));
  GlobalData_t IpCa = ((p->maxIpCa*sv->Cai)/(0.0005+sv->Cai));
  GlobalData_t Irel = (((((sv->u*sv->u)*sv->v)*sv->w)*k_rel)*(sv->CaRel-(sv->Cai)));
  GlobalData_t Itr = ((sv->CaUp-(sv->CaRel))/p->tau_tr);
  GlobalData_t Iup = ((p->maxIup/(1.+(K_up/sv->Cai)))-(((p->maxIup/p->maxCa_up)*sv->CaUp)));
  GlobalData_t f_NaK = (1./((1.+(0.1245*(exp(((((-0.1*F)*V)/R)/T)))))+((0.0365*sigma)*(exp(((((-F)*V)/R)/T))))));
  GlobalData_t log_Cao_Cai = (log((p->Cao/sv->Cai)));
  GlobalData_t pow_KmNaINai_15 = (pow((KmNai/sv->Nai),1.5));
  GlobalData_t IClCa = ((sv->QCa*p->GClCa)*(V-(E_Cl)));
  GlobalData_t IK1 = ((p->GK1*(V-(E_K)))*IK1_term);
  GlobalData_t IKr = (((p->GKr*(V-(E_K)))*IKr_term)*sv->xr);
  GlobalData_t IKs = (((p->GKs*(V-(E_K)))*sv->xs)*sv->xs);
  GlobalData_t IKur = (((((GKur*(V-(E_K)))*sv->Ua)*sv->Ua)*sv->Ua)*sv->Ui);
  GlobalData_t INa = ((((((p->GNa*(V-(E_Na)))*sv->m)*sv->m)*sv->m)*sv->h)*sv->j);
  GlobalData_t INaCa_term2 = ((exp((((V*F)/R)/T)))*INaCa_term1);
  GlobalData_t INaK = ((((p->maxINaK*f_NaK)/(1.+pow_KmNaINai_15))*p->Ko)/(p->Ko+KmKo));
  GlobalData_t IbCa = ((V*p->GbCa)-((((((p->GbCa*R)*T)/2.)/F)*log_Cao_Cai)));
  GlobalData_t IbNa = (p->GbNa*(V-(E_Na)));
  GlobalData_t Ito = (((((p->Gto*(V-(E_K)))*sv->Oa)*sv->Oa)*sv->Oa)*sv->Oi);
  GlobalData_t INaCa = (((((INaCa_term2*sv->Nai)*sv->Nai)*sv->Nai)*p->Cao)-(((((INaCa_term1*p->Nao)*p->Nao)*p->Nao)*sv->Cai)));
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->CaCmdn);
  fprintf(file, "%4.12f\t", sv->CaCsqn);
  fprintf(file, "%4.12f\t", sv->CaRel);
  fprintf(file, "%4.12f\t", sv->CaTrpn);
  fprintf(file, "%4.12f\t", sv->CaUp);
  fprintf(file, "%4.12f\t", sv->Cai);
  fprintf(file, "%4.12f\t", sv->Cli);
  fprintf(file, "%4.12f\t", ICaL);
  fprintf(file, "%4.12f\t", IClCa);
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", IKur);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", IbCa);
  fprintf(file, "%4.12f\t", IbNa);
  fprintf(file, "%4.12f\t", IpCa);
  fprintf(file, "%4.12f\t", Irel);
  fprintf(file, "%4.12f\t", Ito);
  fprintf(file, "%4.12f\t", Itr);
  fprintf(file, "%4.12f\t", Iup);
  fprintf(file, "%4.12f\t", sv->Ki);
  fprintf(file, "%4.12f\t", sv->Nai);
  fprintf(file, "%4.12f\t", sv->Oa);
  fprintf(file, "%4.12f\t", sv->Oi);
  fprintf(file, "%4.12f\t", sv->QCa);
  fprintf(file, "%4.12f\t", sv->Ua);
  fprintf(file, "%4.12f\t", sv->Ui);
  fprintf(file, "%4.12f\t", V);
  fprintf(file, "%4.12f\t", sv->d);
  fprintf(file, "%4.12f\t", sv->f);
  fprintf(file, "%4.12f\t", sv->fCa);
  fprintf(file, "%4.12f\t", sv->h);
  fprintf(file, "%4.12f\t", sv->j);
  fprintf(file, "%4.12f\t", sv->m);
  fprintf(file, "%4.12f\t", sv->u);
  fprintf(file, "%4.12f\t", sv->v);
  fprintf(file, "%4.12f\t", sv->w);
  fprintf(file, "%4.12f\t", sv->xr);
  fprintf(file, "%4.12f\t", sv->xs);
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e3;
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        