// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Jeffrey J. Fox, Jennifer L. McHarg, and Robert F. Gilmour Jr
*  Year: 2002
*  Title: Ionic mechanism of electrical alternans
*  Journal: American Journal of Physiology: Heart and Circulatory Physiology, 282, H516-H530
*  DOI: 10.1152/ajpheart.00612.2001
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Fox.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Fox(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Fox( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define A_Cap (GlobalData_t)(0.0001534)
#define CMDN_tot (GlobalData_t)(10.)
#define CSQN_tot (GlobalData_t)(10000.)
#define C_sc (GlobalData_t)(1.)
#define Ca_SR_init (GlobalData_t)(320.)
#define Cai_init (GlobalData_t)(0.0472)
#define Cao (GlobalData_t)(2000.)
#define F (GlobalData_t)(96.5)
#define GK1 (GlobalData_t)(2.8)
#define GKp (GlobalData_t)(0.002216)
#define GbCa (GlobalData_t)(0.0003842)
#define GbNa (GlobalData_t)(0.0031)
#define Gto (GlobalData_t)(0.23815)
#define ICa_half (GlobalData_t)(-0.265)
#define ICap_max (GlobalData_t)(0.05)
#define INaK_max (GlobalData_t)(0.693)
#define K_NaCa (GlobalData_t)(1500.)
#define K_mCMDN (GlobalData_t)(2.)
#define K_mCSQN (GlobalData_t)(600.)
#define K_mCa (GlobalData_t)(1380.)
#define K_mK1 (GlobalData_t)(13.)
#define K_mKo (GlobalData_t)(1.5)
#define K_mNa (GlobalData_t)(87.5)
#define K_mNai (GlobalData_t)(10.)
#define K_mfCa (GlobalData_t)(0.18)
#define K_mpCa (GlobalData_t)(0.05)
#define K_mup (GlobalData_t)(0.32)
#define K_sat (GlobalData_t)(0.2)
#define Ki (GlobalData_t)(149.4)
#define Ko (GlobalData_t)(4.)
#define Nai (GlobalData_t)(10.)
#define Nao (GlobalData_t)(138.)
#define PCaK (GlobalData_t)(0.000000579)
#define P_leak (GlobalData_t)(0.000001)
#define P_rel (GlobalData_t)(6.)
#define R (GlobalData_t)(8.314)
#define T (GlobalData_t)(310.)
#define V_SR (GlobalData_t)(0.000002)
#define V_init (GlobalData_t)(-94.7)
#define V_myo (GlobalData_t)(0.00002584)
#define V_up (GlobalData_t)(0.1)
#define X_kr_init (GlobalData_t)(0.229)
#define X_ks_init (GlobalData_t)(0.0001)
#define X_to_init (GlobalData_t)((3.742*(pow(10.,-5.))))
#define Y_to_init (GlobalData_t)(1.)
#define d_init (GlobalData_t)(0.0001)
#define eta (GlobalData_t)(0.35)
#define f_Ca_init (GlobalData_t)(0.942)
#define f_init (GlobalData_t)(0.983)
#define h_init (GlobalData_t)(0.99869)
#define j_init (GlobalData_t)(0.99887)
#define m_init (GlobalData_t)((2.4676*(pow(10.,-4.))))
#define tau_f_Ca (GlobalData_t)(30.)
#define E_K (GlobalData_t)((((R*T)/F)*(log((Ko/Ki)))))
#define E_Ks (GlobalData_t)((((R*T)/F)*(log(((Ko+(0.01833*Nao))/(Ki+(0.01833*Nai)))))))
#define E_Na (GlobalData_t)((((R*T)/F)*(log((Nao/Nai)))))
#define K_mNa3 (GlobalData_t)(((K_mNa*K_mNa)*K_mNa))
#define Nai3 (GlobalData_t)(((Nai*Nai)*Nai))
#define Nao3 (GlobalData_t)(((Nao*Nao)*Nao))
#define sigma (GlobalData_t)(((1./7.)*((exp((Nao/67.3)))-(1.))))
#define sq_rt_Ko_4 (GlobalData_t)((sqrt((Ko/4.))))



void initialize_params_Fox( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Fox_Params *p = (Fox_Params *)IF->params;

  // Compute the regional constants
  {
    p->GKr = 0.0136;
    p->GKs = 0.0245;
    p->GNa = 12.8;
    p->PCa = 0.0000226;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_Fox;

}


// Define the parameters for the lookup tables
enum Tables {
  Cai_TAB,
  V_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum Cai_TableIndex {
  E_Ca_idx,
  ICap_idx,
  J_up_idx,
  beta_i_idx,
  f_Ca_rush_larsen_A_idx,
  NROWS_Cai
};

enum V_TableIndex {
  IK1_idx,
  IKp_idx,
  INaK_idx,
  IbNa_idx,
  J_rel_temp_idx,
  R_V_idx,
  X_kr_rush_larsen_A_idx,
  X_kr_rush_larsen_B_idx,
  X_ks_rush_larsen_A_idx,
  X_ks_rush_larsen_B_idx,
  X_to_rush_larsen_A_idx,
  X_to_rush_larsen_B_idx,
  Y_to_rush_larsen_A_idx,
  Y_to_rush_larsen_B_idx,
  d_rush_larsen_A_idx,
  d_rush_larsen_B_idx,
  e_2VFRT_idx,
  e_VFRT_idx,
  e_eta1VFRT_idx,
  e_etaVFRT_idx,
  f_rush_larsen_A_idx,
  f_rush_larsen_B_idx,
  h_rush_larsen_A_idx,
  h_rush_larsen_B_idx,
  j_rush_larsen_A_idx,
  j_rush_larsen_B_idx,
  m_rush_larsen_A_idx,
  m_rush_larsen_B_idx,
  NROWS_V
};



void construct_tables_Fox( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Fox_Params *p = (Fox_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double f_Ca_rush_larsen_B = (exp(((-dt)/tau_f_Ca)));
  double f_Ca_rush_larsen_C = (expm1(((-dt)/tau_f_Ca)));
  
  // Create the Cai lookup table
  LUT* Cai_tab = &IF->tables[Cai_TAB];
  LUT_alloc(Cai_tab, NROWS_Cai, 1e-4, 100, 1e-4, "Fox Cai");
  for (int __i=Cai_tab->mn_ind; __i<=Cai_tab->mx_ind; __i++) {
    double Cai = Cai_tab->res*__i;
    LUT_data_t* Cai_row = Cai_tab->tab[__i];
    double CaIK_mfCa = (Cai/K_mfCa);
    Cai_row[E_Ca_idx] = (((R*T)/(2.*F))*(log((Cao/Cai))));
    Cai_row[ICap_idx] = ((ICap_max*Cai)/(K_mpCa+Cai));
    double K_mCMDN_Cai2 = ((K_mCMDN+Cai)*(K_mCMDN+Cai));
    double K_muPCai2 = (((K_mup/Cai)*K_mup)/Cai);
    Cai_row[J_up_idx] = (V_up/(1.+K_muPCai2));
    Cai_row[beta_i_idx] = (1./(1.+((CMDN_tot*K_mCMDN)/K_mCMDN_Cai2)));
    double f_Ca_infinity = (1./(1.+((CaIK_mfCa*CaIK_mfCa)*CaIK_mfCa)));
    Cai_row[f_Ca_rush_larsen_A_idx] = ((-f_Ca_infinity)*f_Ca_rush_larsen_C);
  }
  check_LUT(Cai_tab);
  
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -800, 800, 0.05, "Fox V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    double E0_d = (V+40.);
    double E0_m = (V+47.13);
    V_row[IbNa_idx] = (GbNa*(V-(E_Na)));
    V_row[J_rel_temp_idx] = (1.+(1.65*(exp((V/20.)))));
    double K1_infinity = (1./(2.+(exp((((1.62*F)/(R*T))*(V-(E_K)))))));
    double Kp_V = (1./(1.+(exp(((7.488-(V))/5.98)))));
    V_row[R_V_idx] = (1./(1.+(2.5*(exp((0.1*(V+28.)))))));
    double X_kr_inf = (1./(1.+(exp((-2.182-((0.1819*V)))))));
    double X_ks_infinity = (1./(1.+(exp(((V-(16.))/-13.6)))));
    double alpha_X_to = (0.04516*(exp((0.03577*V))));
    double alpha_Y_to = ((0.005415*(exp(((V+33.5)/-5.))))/(1.+(0.051335*(exp(((V+33.5)/-5.))))));
    double alpha_h = (0.135*(exp(((V+80.)/-6.8))));
    double alpha_j = ((0.175*(exp(((V+100.)/-23.))))/(1.+(exp((0.15*(V+79.))))));
    double beta_X_to = (0.0989*(exp((-0.06237*V))));
    double beta_Y_to = ((0.005415*(exp(((V+33.5)/5.))))/(1.+(0.051335*(exp(((V+33.5)/5.))))));
    double beta_h = (7.5/(1.+(exp((-0.1*(V+11.))))));
    double beta_j = (0.3/(1.+(exp((-0.1*(V+32.))))));
    double beta_m = (0.08*(exp(((-V)/11.))));
    double d_infinity = (1./(1.+(exp(((V+10.)/-6.24)))));
    V_row[e_2VFRT_idx] = (exp((((2.*V)*F)/(R*T))));
    V_row[e_VFRT_idx] = (exp(((V*F)/(R*T))));
    V_row[e_eta1VFRT_idx] = (exp(((((eta-(1.))*V)*F)/(R*T))));
    V_row[e_etaVFRT_idx] = (exp((((eta*V)*F)/(R*T))));
    double f_NaK = (1./((1.+(0.1245*(exp((((-0.1*V)*F)/(R*T))))))+((0.0365*sigma)*(exp((((-V)*F)/(R*T)))))));
    double f_infinity = (1./(1.+(exp(((V+12.5)/5.)))));
    double tau_X_kr = (43.+(1./((exp((-5.495+(0.1691*V))))+(exp((-7.677-((0.0128*V))))))));
    double tau_X_ks = ((V==10.) ? (1./((0.0000719/-0.148)+(0.000131/0.0687))) : (1./(((0.0000719*(V-(10.)))/(1.-((exp((-0.148*(V-(10.))))))))+((0.000131*(V-(10.)))/((exp((0.0687*(V-(10.)))))-(1.))))));
    double tau_f = (30.+(200./(1.+(exp(((V+20.)/9.5))))));
    V_row[IK1_idx] = ((((GK1*K1_infinity)*Ko)/(Ko+K_mK1))*(V-(E_K)));
    V_row[IKp_idx] = ((GKp*Kp_V)*(V-(E_K)));
    V_row[INaK_idx] = ((((INaK_max*f_NaK)/(1.+(pow((K_mNai/Nai),1.5))))*Ko)/(Ko+K_mKo));
    V_row[X_kr_rush_larsen_B_idx] = (exp(((-dt)/tau_X_kr)));
    double X_kr_rush_larsen_C = (expm1(((-dt)/tau_X_kr)));
    V_row[X_ks_rush_larsen_B_idx] = (exp(((-dt)/tau_X_ks)));
    double X_ks_rush_larsen_C = (expm1(((-dt)/tau_X_ks)));
    V_row[X_to_rush_larsen_A_idx] = (((-alpha_X_to)/(alpha_X_to+beta_X_to))*(expm1(((-dt)*(alpha_X_to+beta_X_to)))));
    V_row[X_to_rush_larsen_B_idx] = (exp(((-dt)*(alpha_X_to+beta_X_to))));
    V_row[Y_to_rush_larsen_A_idx] = (((-alpha_Y_to)/(alpha_Y_to+beta_Y_to))*(expm1(((-dt)*(alpha_Y_to+beta_Y_to)))));
    V_row[Y_to_rush_larsen_B_idx] = (exp(((-dt)*(alpha_Y_to+beta_Y_to))));
    double alpha_m = ((0.32*E0_m)/(1.-((exp((-0.1*E0_m))))));
    V_row[f_rush_larsen_B_idx] = (exp(((-dt)/tau_f)));
    double f_rush_larsen_C = (expm1(((-dt)/tau_f)));
    V_row[h_rush_larsen_A_idx] = (((-alpha_h)/(alpha_h+beta_h))*(expm1(((-dt)*(alpha_h+beta_h)))));
    V_row[h_rush_larsen_B_idx] = (exp(((-dt)*(alpha_h+beta_h))));
    V_row[j_rush_larsen_A_idx] = (((-alpha_j)/(alpha_j+beta_j))*(expm1(((-dt)*(alpha_j+beta_j)))));
    V_row[j_rush_larsen_B_idx] = (exp(((-dt)*(alpha_j+beta_j))));
    double tau_d_temp = ((0.07*(exp((-0.05*E0_d))))/(1.+(exp((0.05*E0_d)))));
    V_row[X_kr_rush_larsen_A_idx] = ((-X_kr_inf)*X_kr_rush_larsen_C);
    V_row[X_ks_rush_larsen_A_idx] = ((-X_ks_infinity)*X_ks_rush_larsen_C);
    V_row[f_rush_larsen_A_idx] = ((-f_infinity)*f_rush_larsen_C);
    V_row[m_rush_larsen_A_idx] = (((-alpha_m)/(alpha_m+beta_m))*(expm1(((-dt)*(alpha_m+beta_m)))));
    V_row[m_rush_larsen_B_idx] = (exp(((-dt)*(alpha_m+beta_m))));
    double tau_d = (1./(((0.25*(exp((-0.01*V))))/(1.+(exp((-0.07*V)))))+tau_d_temp));
    V_row[d_rush_larsen_B_idx] = (exp(((-dt)/tau_d)));
    double d_rush_larsen_C = (expm1(((-dt)/tau_d)));
    V_row[d_rush_larsen_A_idx] = ((-d_infinity)*d_rush_larsen_C);
  }
  check_LUT(V_tab);
  

}



void    initialize_sv_Fox( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Fox_Params *p = (Fox_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Fox_state) );
  Fox_state *sv_base = (Fox_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double f_Ca_rush_larsen_B = (exp(((-dt)/tau_f_Ca)));
  double f_Ca_rush_larsen_C = (expm1(((-dt)/tau_f_Ca)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Fox_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Ca_SR = Ca_SR_init;
    sv->Cai = Cai_init;
    V = V_init;
    sv->X_kr = X_kr_init;
    sv->X_ks = X_ks_init;
    sv->X_to = X_to_init;
    sv->Y_to = Y_to_init;
    sv->d = d_init;
    sv->f = f_init;
    sv->f_Ca = f_Ca_init;
    sv->h = h_init;
    sv->j = j_init;
    sv->m = m_init;
    double E_Ca = (((R*T)/(2.*F))*(log((Cao/sv->Cai))));
    double ICap = ((ICap_max*sv->Cai)/(K_mpCa+sv->Cai));
    double IKs = (((p->GKs*sv->X_ks)*sv->X_ks)*(V-(E_Ks)));
    double INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(E_Na)));
    double IbNa = (GbNa*(V-(E_Na)));
    double Ito = (((Gto*sv->X_to)*sv->Y_to)*(V-(E_K)));
    double K1_infinity = (1./(2.+(exp((((1.62*F)/(R*T))*(V-(E_K)))))));
    double Kp_V = (1./(1.+(exp(((7.488-(V))/5.98)))));
    double R_V = (1./(1.+(2.5*(exp((0.1*(V+28.)))))));
    double e_2VFRT = (exp((((2.*V)*F)/(R*T))));
    double e_VFRT = (exp(((V*F)/(R*T))));
    double e_eta1VFRT = (exp(((((eta-(1.))*V)*F)/(R*T))));
    double e_etaVFRT = (exp((((eta*V)*F)/(R*T))));
    double f_NaK = (1./((1.+(0.1245*(exp((((-0.1*V)*F)/(R*T))))))+((0.0365*sigma)*(exp((((-V)*F)/(R*T)))))));
    double ICa_max = ((((((((p->PCa/C_sc)*4.)*V)*F)*F)/(R*T))*((sv->Cai*e_2VFRT)-((0.341*Cao))))/(e_2VFRT-(1.)));
    double IK1 = ((((GK1*K1_infinity)*Ko)/(Ko+K_mK1))*(V-(E_K)));
    double IKp = ((GKp*Kp_V)*(V-(E_K)));
    double IKr = ((((p->GKr*R_V)*sv->X_kr)*sq_rt_Ko_4)*(V-(E_K)));
    double INaCa = ((K_NaCa/(((K_mNa3+Nao3)*(K_mCa+Cao))*(1.+(K_sat*e_eta1VFRT))))*(((e_etaVFRT*Nai3)*Cao)-(((e_eta1VFRT*Nao3)*sv->Cai))));
    double INaK = ((((INaK_max*f_NaK)/(1.+(pow((K_mNai/Nai),1.5))))*Ko)/(Ko+K_mKo));
    double IbCa = (GbCa*(V-(E_Ca)));
    double ICa = (((ICa_max*sv->f)*sv->d)*sv->f_Ca);
    double ICaK = ((((((((((((PCaK/C_sc)*sv->f)*sv->d)*sv->f_Ca)/(1.+(ICa_max/ICa_half)))*1000.)*V)*F)*F)/(R*T))*((Ki*e_VFRT)-(Ko)))/(e_VFRT-(1.)));
    Iion = ((((((((((((INa+ICa)+ICaK)+IKr)+IKs)+Ito)+IK1)+IKp)+INaCa)+INaK)+ICap)+IbNa)+IbCa);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Fox(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Fox_Params *p  = (Fox_Params *)IF->params;
  Fox_state *sv_base = (Fox_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t f_Ca_rush_larsen_B = (exp(((-dt)/tau_f_Ca)));
  GlobalData_t f_Ca_rush_larsen_C = (expm1(((-dt)/tau_f_Ca)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Fox_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Cai_row[NROWS_Cai];
    LUT_interpRow(&IF->tables[Cai_TAB], sv->Cai, __i, Cai_row);
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t IKs = (((p->GKs*sv->X_ks)*sv->X_ks)*(V-(E_Ks)));
    GlobalData_t INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(E_Na)));
    GlobalData_t Ito = (((Gto*sv->X_to)*sv->Y_to)*(V-(E_K)));
    GlobalData_t ICa_max = ((((((((p->PCa/C_sc)*4.)*V)*F)*F)/(R*T))*((sv->Cai*V_row[e_2VFRT_idx])-((0.341*Cao))))/(V_row[e_2VFRT_idx]-(1.)));
    GlobalData_t IKr = ((((p->GKr*V_row[R_V_idx])*sv->X_kr)*sq_rt_Ko_4)*(V-(E_K)));
    GlobalData_t INaCa = ((K_NaCa/(((K_mNa3+Nao3)*(K_mCa+Cao))*(1.+(K_sat*V_row[e_eta1VFRT_idx]))))*(((V_row[e_etaVFRT_idx]*Nai3)*Cao)-(((V_row[e_eta1VFRT_idx]*Nao3)*sv->Cai))));
    GlobalData_t IbCa = (GbCa*(V-(Cai_row[E_Ca_idx])));
    GlobalData_t ICa = (((ICa_max*sv->f)*sv->d)*sv->f_Ca);
    GlobalData_t ICaK = ((((((((((((PCaK/C_sc)*sv->f)*sv->d)*sv->f_Ca)/(1.+(ICa_max/ICa_half)))*1000.)*V)*F)*F)/(R*T))*((Ki*V_row[e_VFRT_idx])-(Ko)))/(V_row[e_VFRT_idx]-(1.)));
    Iion = ((((((((((((INa+ICa)+ICaK)+IKr)+IKs)+Ito)+V_row[IK1_idx])+V_row[IKp_idx])+INaCa)+V_row[INaK_idx])+Cai_row[ICap_idx])+V_row[IbNa_idx])+IbCa);
    
    
    //Complete Forward Euler Update
    GlobalData_t Ca_SR_2000_3 = (((((2000./sv->Ca_SR)*2000.)/sv->Ca_SR)*2000.)/sv->Ca_SR);
    GlobalData_t J_leak = (P_leak*(sv->Ca_SR-(sv->Cai)));
    GlobalData_t K_mCSQN_Ca_SR2 = ((K_mCSQN+sv->Ca_SR)*(K_mCSQN+sv->Ca_SR));
    GlobalData_t beta_SR = (1./(1.+((CSQN_tot*K_mCSQN)/K_mCSQN_Ca_SR2)));
    GlobalData_t gamma = (1./(1.+Ca_SR_2000_3));
    GlobalData_t J_rel = (((((P_rel*sv->f)*sv->d)*sv->f_Ca)*((gamma*sv->Ca_SR)-(sv->Cai)))/V_row[J_rel_temp_idx]);
    GlobalData_t diff_Ca_SR = (((beta_SR*((Cai_row[J_up_idx]-(J_leak))-(J_rel)))*V_myo)/V_SR);
    GlobalData_t diff_Cai = (Cai_row[beta_i_idx]*(((J_rel+J_leak)-(Cai_row[J_up_idx]))-((((A_Cap*C_sc)/((2.*F)*V_myo))*(((ICa+IbCa)+Cai_row[ICap_idx])-((2.*INaCa)))))));
    GlobalData_t Ca_SR_new = sv->Ca_SR+diff_Ca_SR*dt;
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t X_kr_rush_larsen_B = V_row[X_kr_rush_larsen_B_idx];
    GlobalData_t X_ks_rush_larsen_B = V_row[X_ks_rush_larsen_B_idx];
    GlobalData_t X_to_rush_larsen_A = V_row[X_to_rush_larsen_A_idx];
    GlobalData_t X_to_rush_larsen_B = V_row[X_to_rush_larsen_B_idx];
    GlobalData_t Y_to_rush_larsen_A = V_row[Y_to_rush_larsen_A_idx];
    GlobalData_t Y_to_rush_larsen_B = V_row[Y_to_rush_larsen_B_idx];
    GlobalData_t f_rush_larsen_B = V_row[f_rush_larsen_B_idx];
    GlobalData_t h_rush_larsen_A = V_row[h_rush_larsen_A_idx];
    GlobalData_t h_rush_larsen_B = V_row[h_rush_larsen_B_idx];
    GlobalData_t j_rush_larsen_A = V_row[j_rush_larsen_A_idx];
    GlobalData_t j_rush_larsen_B = V_row[j_rush_larsen_B_idx];
    GlobalData_t X_kr_rush_larsen_A = V_row[X_kr_rush_larsen_A_idx];
    GlobalData_t X_ks_rush_larsen_A = V_row[X_ks_rush_larsen_A_idx];
    GlobalData_t f_Ca_rush_larsen_A = Cai_row[f_Ca_rush_larsen_A_idx];
    GlobalData_t f_rush_larsen_A = V_row[f_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_A = V_row[m_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_B = V_row[m_rush_larsen_B_idx];
    GlobalData_t d_rush_larsen_B = V_row[d_rush_larsen_B_idx];
    GlobalData_t d_rush_larsen_A = V_row[d_rush_larsen_A_idx];
    GlobalData_t X_kr_new = X_kr_rush_larsen_A+X_kr_rush_larsen_B*sv->X_kr;
    GlobalData_t X_ks_new = X_ks_rush_larsen_A+X_ks_rush_larsen_B*sv->X_ks;
    GlobalData_t X_to_new = X_to_rush_larsen_A+X_to_rush_larsen_B*sv->X_to;
    GlobalData_t Y_to_new = Y_to_rush_larsen_A+Y_to_rush_larsen_B*sv->Y_to;
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f_new = f_rush_larsen_A+f_rush_larsen_B*sv->f;
    GlobalData_t f_Ca_new = f_Ca_rush_larsen_A+f_Ca_rush_larsen_B*sv->f_Ca;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Ca_SR = Ca_SR_new;
    sv->Cai = Cai_new;
    Iion = Iion;
    sv->X_kr = X_kr_new;
    sv->X_ks = X_ks_new;
    sv->X_to = X_to_new;
    sv->Y_to = Y_to_new;
    sv->d = d_new;
    sv->f = f_new;
    sv->f_Ca = f_Ca_new;
    sv->h = h_new;
    sv->j = j_new;
    sv->m = m_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_Fox(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Fox_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->Ca_SR\n"
        "sv->Cai\n"
        "E_Na\n"
        "ICa\n"
        "ICaK\n"
        "ICap\n"
        "IK1\n"
        "IKp\n"
        "IKr\n"
        "IKs\n"
        "INa\n"
        "INaCa\n"
        "INaK\n"
        "IbCa\n"
        "IbNa\n"
        "Ito\n"
        "J_rel\n"
        "J_up\n"
        "V\n"
        "sv->h\n"
        "sv->j\n"
        "sv->m\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Fox_Params *p  = (Fox_Params *)IF->params;

  Fox_state *sv_base = (Fox_state *)IF->sv_tab.y;
  Fox_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t f_Ca_rush_larsen_B = (exp(((-dt)/tau_f_Ca)));
  GlobalData_t f_Ca_rush_larsen_C = (expm1(((-dt)/tau_f_Ca)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t Ca_SR_2000_3 = (((((2000./sv->Ca_SR)*2000.)/sv->Ca_SR)*2000.)/sv->Ca_SR);
  GlobalData_t E_Ca = (((R*T)/(2.*F))*(log((Cao/sv->Cai))));
  GlobalData_t ICap = ((ICap_max*sv->Cai)/(K_mpCa+sv->Cai));
  GlobalData_t IKs = (((p->GKs*sv->X_ks)*sv->X_ks)*(V-(E_Ks)));
  GlobalData_t INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(E_Na)));
  GlobalData_t IbNa = (GbNa*(V-(E_Na)));
  GlobalData_t Ito = (((Gto*sv->X_to)*sv->Y_to)*(V-(E_K)));
  GlobalData_t J_rel_temp = (1.+(1.65*(exp((V/20.)))));
  GlobalData_t K1_infinity = (1./(2.+(exp((((1.62*F)/(R*T))*(V-(E_K)))))));
  GlobalData_t K_muPCai2 = (((K_mup/sv->Cai)*K_mup)/sv->Cai);
  GlobalData_t Kp_V = (1./(1.+(exp(((7.488-(V))/5.98)))));
  GlobalData_t R_V = (1./(1.+(2.5*(exp((0.1*(V+28.)))))));
  GlobalData_t e_2VFRT = (exp((((2.*V)*F)/(R*T))));
  GlobalData_t e_VFRT = (exp(((V*F)/(R*T))));
  GlobalData_t e_eta1VFRT = (exp(((((eta-(1.))*V)*F)/(R*T))));
  GlobalData_t e_etaVFRT = (exp((((eta*V)*F)/(R*T))));
  GlobalData_t f_NaK = (1./((1.+(0.1245*(exp((((-0.1*V)*F)/(R*T))))))+((0.0365*sigma)*(exp((((-V)*F)/(R*T)))))));
  GlobalData_t ICa_max = ((((((((p->PCa/C_sc)*4.)*V)*F)*F)/(R*T))*((sv->Cai*e_2VFRT)-((0.341*Cao))))/(e_2VFRT-(1.)));
  GlobalData_t IK1 = ((((GK1*K1_infinity)*Ko)/(Ko+K_mK1))*(V-(E_K)));
  GlobalData_t IKp = ((GKp*Kp_V)*(V-(E_K)));
  GlobalData_t IKr = ((((p->GKr*R_V)*sv->X_kr)*sq_rt_Ko_4)*(V-(E_K)));
  GlobalData_t INaCa = ((K_NaCa/(((K_mNa3+Nao3)*(K_mCa+Cao))*(1.+(K_sat*e_eta1VFRT))))*(((e_etaVFRT*Nai3)*Cao)-(((e_eta1VFRT*Nao3)*sv->Cai))));
  GlobalData_t INaK = ((((INaK_max*f_NaK)/(1.+(pow((K_mNai/Nai),1.5))))*Ko)/(Ko+K_mKo));
  GlobalData_t IbCa = (GbCa*(V-(E_Ca)));
  GlobalData_t J_up = (V_up/(1.+K_muPCai2));
  GlobalData_t gamma = (1./(1.+Ca_SR_2000_3));
  GlobalData_t ICa = (((ICa_max*sv->f)*sv->d)*sv->f_Ca);
  GlobalData_t ICaK = ((((((((((((PCaK/C_sc)*sv->f)*sv->d)*sv->f_Ca)/(1.+(ICa_max/ICa_half)))*1000.)*V)*F)*F)/(R*T))*((Ki*e_VFRT)-(Ko)))/(e_VFRT-(1.)));
  GlobalData_t J_rel = (((((P_rel*sv->f)*sv->d)*sv->f_Ca)*((gamma*sv->Ca_SR)-(sv->Cai)))/J_rel_temp);
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->Ca_SR);
  fprintf(file, "%4.12f\t", sv->Cai);
  fprintf(file, "%4.12f\t", E_Na);
  fprintf(file, "%4.12f\t", ICa);
  fprintf(file, "%4.12f\t", ICaK);
  fprintf(file, "%4.12f\t", ICap);
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", IKp);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", IbCa);
  fprintf(file, "%4.12f\t", IbNa);
  fprintf(file, "%4.12f\t", Ito);
  fprintf(file, "%4.12f\t", J_rel);
  fprintf(file, "%4.12f\t", J_up);
  fprintf(file, "%4.12f\t", V);
  fprintf(file, "%4.12f\t", sv->h);
  fprintf(file, "%4.12f\t", sv->j);
  fprintf(file, "%4.12f\t", sv->m);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        