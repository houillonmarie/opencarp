// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Maleckar MM, Greenstein JL, Giles WR, Trayanova NA
*  Year: 2009
*  Title: Electrotonic coupling between human atrial myocytes and fibroblasts alters myocyte excitability and repolarization
*  Journal: Biophys J., 97(8), 2179-90
*  DOI: 10.1016/j.bpj.2009.07.054
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Maleckar.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Maleckar(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Maleckar( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define B (GlobalData_t)(-200.0)
#define Cm (GlobalData_t)(6.3)
#define F (GlobalData_t)(96487.0)
#define GK1 (GlobalData_t)(0.4822)
#define GbNa (GlobalData_t)(0.0095)
#define K_mK (GlobalData_t)(1.0)
#define K_mNa (GlobalData_t)(11.0)
#define Ki_init (GlobalData_t)(129.4349)
#define Ko (GlobalData_t)(5.3581)
#define Nai_init (GlobalData_t)(8.5547)
#define Nao (GlobalData_t)(130.0110)
#define R (GlobalData_t)(8314.0)
#define T (GlobalData_t)(306.15)
#define V_rev (GlobalData_t)(-150.0)
#define Vol_i (GlobalData_t)(0.00137)
#define Vol_o (GlobalData_t)(0.0008)



void initialize_params_Maleckar( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Maleckar_Params *p = (Maleckar_Params *)IF->params;

  // Compute the regional constants
  {
    p->type = 1.;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Maleckar( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Maleckar_Params *p = (Maleckar_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double V_init = ((p->type==1.) ? -47.75 : -31.38);
  double g_Kv = ((p->type==1.) ? 0.25 : 0.22);
  double g_NaK = ((p->type==1.) ? 1.644 : 1.355);

}



void    initialize_sv_Maleckar( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Maleckar_Params *p = (Maleckar_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Maleckar_state) );
  Maleckar_state *sv_base = (Maleckar_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double V_init = ((p->type==1.) ? -47.75 : -31.38);
  double g_Kv = ((p->type==1.) ? 0.25 : 0.22);
  double g_NaK = ((p->type==1.) ? 1.644 : 1.355);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Maleckar_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Ki = Ki_init;
    sv->Nai = Nai_init;
    V = V_init;
    double E_K = (((R*T)/F)*(log((Ko/sv->Ki))));
    double E_Na = (((R*T)/F)*(log((Nao/sv->Nai))));
    double i_NaK = ((((g_NaK*(Ko/(Ko+K_mK)))*((pow(sv->Nai,1.5))/((pow(sv->Nai,1.5))+(pow(K_mNa,1.5)))))*(V-(V_rev)))/(V-(B)));
    double r_Kv_infinity = ((p->type==1.) ? (1./(1.+(exp(((-(V+20.))/11.))))) : (1./(1.+(exp(((-V)/11.))))));
    double s_Kv_infinity = ((p->type==1.) ? (1./(1.+(exp(((V+23.)/7.))))) : (1./(1.+(exp(((V+3.)/7.))))));
    double IbNa = (GbNa*(V-(E_Na)));
    double alpha_K1 = (0.1/(1.+(exp((0.06*((V-(E_K))-(200.0)))))));
    double beta_K1 = (((3.*(exp((0.0002*((V-(E_K))+100.0)))))+(exp((0.1*((V-(E_K))-(10.0))))))/(1.0+(exp((-0.5*(V-(E_K)))))));
    double r_Kv_init = r_Kv_infinity;
    double s_Kv_init = s_Kv_infinity;
    double IK1 = (((GK1*alpha_K1)*(V-(E_K)))/(alpha_K1+beta_K1));
    sv->r_Kv = r_Kv_init;
    sv->s_Kv = s_Kv_init;
    double IKv = (((g_Kv*sv->r_Kv)*sv->s_Kv)*(V-(E_K)));
    Iion = (((IKv+IK1)+i_NaK)+IbNa);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Maleckar(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Maleckar_Params *p  = (Maleckar_Params *)IF->params;
  Maleckar_state *sv_base = (Maleckar_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t V_init = ((p->type==1.) ? -47.75 : -31.38);
  GlobalData_t g_Kv = ((p->type==1.) ? 0.25 : 0.22);
  GlobalData_t g_NaK = ((p->type==1.) ? 1.644 : 1.355);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Maleckar_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t E_K = (((R*T)/F)*(log((Ko/sv->Ki))));
    GlobalData_t E_Na = (((R*T)/F)*(log((Nao/sv->Nai))));
    GlobalData_t i_NaK = ((((g_NaK*(Ko/(Ko+K_mK)))*((pow(sv->Nai,1.5))/((pow(sv->Nai,1.5))+(pow(K_mNa,1.5)))))*(V-(V_rev)))/(V-(B)));
    GlobalData_t IKv = (((g_Kv*sv->r_Kv)*sv->s_Kv)*(V-(E_K)));
    GlobalData_t IbNa = (GbNa*(V-(E_Na)));
    GlobalData_t alpha_K1 = (0.1/(1.+(exp((0.06*((V-(E_K))-(200.0)))))));
    GlobalData_t beta_K1 = (((3.*(exp((0.0002*((V-(E_K))+100.0)))))+(exp((0.1*((V-(E_K))-(10.0))))))/(1.0+(exp((-0.5*(V-(E_K)))))));
    GlobalData_t IK1 = (((GK1*alpha_K1)*(V-(E_K)))/(alpha_K1+beta_K1));
    Iion = (((IKv+IK1)+i_NaK)+IbNa);
    
    
    //Complete Forward Euler Update
    GlobalData_t diff_Ki = (((-(1./Cm))*((IK1+IKv)-((2.*i_NaK))))/(Vol_i*F));
    GlobalData_t diff_Nai = (((-(1./Cm))*(IbNa+(3.*i_NaK)))/(Vol_i*F));
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t r_Kv_infinity = ((p->type==1.) ? (1./(1.+(exp(((-(V+20.))/11.))))) : (1./(1.+(exp(((-V)/11.))))));
    GlobalData_t s_Kv_infinity = ((p->type==1.) ? (1./(1.+(exp(((V+23.)/7.))))) : (1./(1.+(exp(((V+3.)/7.))))));
    GlobalData_t tau_r_Kv = (0.0203+(0.1380*(exp(((-((V+20.)/25.9))*((V+20.)/25.9))))));
    GlobalData_t tau_s_Kv = (1.574+(5.268*(exp(((-((V+23.)/22.7))*((V+23.)/22.7))))));
    GlobalData_t r_Kv_rush_larsen_B = (exp(((-dt)/tau_r_Kv)));
    GlobalData_t r_Kv_rush_larsen_C = (expm1(((-dt)/tau_r_Kv)));
    GlobalData_t s_Kv_rush_larsen_B = (exp(((-dt)/tau_s_Kv)));
    GlobalData_t s_Kv_rush_larsen_C = (expm1(((-dt)/tau_s_Kv)));
    GlobalData_t r_Kv_rush_larsen_A = ((-r_Kv_infinity)*r_Kv_rush_larsen_C);
    GlobalData_t s_Kv_rush_larsen_A = ((-s_Kv_infinity)*s_Kv_rush_larsen_C);
    GlobalData_t r_Kv_new = r_Kv_rush_larsen_A+r_Kv_rush_larsen_B*sv->r_Kv;
    GlobalData_t s_Kv_new = s_Kv_rush_larsen_A+s_Kv_rush_larsen_B*sv->s_Kv;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->r_Kv = r_Kv_new;
    sv->s_Kv = s_Kv_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        