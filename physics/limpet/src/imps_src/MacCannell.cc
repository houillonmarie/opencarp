// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: MacCannell KA, Bazzazi H, Chilton L, Shibukawa Y, Clark RB, Giles WR
*  Year: 2007
*  Title: A mathematical model of electrotonic interactions between ventricular myocytes and fibroblasts
*  Journal: Biophys J., 92(11), 4121-32
*  DOI: 10.1529/biophysj.106.101410
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "MacCannell.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_MacCannell(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_MacCannell( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define B (GlobalData_t)(-200.0)
#define Cm (GlobalData_t)(6.3)
#define F (GlobalData_t)(96487.0)
#define K_mK (GlobalData_t)(1.0)
#define K_mNa (GlobalData_t)(11.0)
#define R (GlobalData_t)(8314.0)
#define T (GlobalData_t)(306.15)
#define V_init (GlobalData_t)(-48.378634)
#define V_rev (GlobalData_t)(-150.0)
#define Vol_i (GlobalData_t)(0.00137)
#define Vol_o (GlobalData_t)(0.0008)
#define r_Kv_init (GlobalData_t)(0.070448)
#define s_Kv_init (GlobalData_t)(0.970886)
#define K_mNa_15 (GlobalData_t)((pow(K_mNa,1.5)))



void initialize_params_MacCannell( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  MacCannell_Params *p = (MacCannell_Params *)IF->params;

  // Compute the regional constants
  {
    p->GK1 = 0.4822;
    p->GKv = 0.25;
    p->GbNa = 0.0095;
    p->Ki_init = 140.045865;
    p->Ko = 5.4;
    p->Nai_init = 9.954213;
    p->Nao = 130.0;
    p->maxINaK = 1.4;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {
  Ki_TAB,
  Nai_TAB,
  V_TAB,
  V_EK_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum Ki_TableIndex {
  E_K_idx,
  NROWS_Ki
};

enum Nai_TableIndex {
  E_Na_idx,
  Nai_15_idx,
  NROWS_Nai
};

enum V_TableIndex {
  r_Kv_rush_larsen_A_idx,
  r_Kv_rush_larsen_B_idx,
  s_Kv_rush_larsen_A_idx,
  s_Kv_rush_larsen_B_idx,
  NROWS_V
};

enum V_EK_TableIndex {
  IK1_idx,
  NROWS_V_EK
};



void construct_tables_MacCannell( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e-3;
  cell_geom *region = &IF->cgeom;
  MacCannell_Params *p = (MacCannell_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  
  // Create the Ki lookup table
  LUT* Ki_tab = &IF->tables[Ki_TAB];
  LUT_alloc(Ki_tab, NROWS_Ki, 0.01, 500, 0.01, "MacCannell Ki");
  for (int __i=Ki_tab->mn_ind; __i<=Ki_tab->mx_ind; __i++) {
    double Ki = Ki_tab->res*__i;
    LUT_data_t* Ki_row = Ki_tab->tab[__i];
    Ki_row[E_K_idx] = (((R*T)/F)*(log((p->Ko/Ki))));
  }
  check_LUT(Ki_tab);
  
  
  // Create the Nai lookup table
  LUT* Nai_tab = &IF->tables[Nai_TAB];
  LUT_alloc(Nai_tab, NROWS_Nai, 0.01, 1000, 0.01, "MacCannell Nai");
  for (int __i=Nai_tab->mn_ind; __i<=Nai_tab->mx_ind; __i++) {
    double Nai = Nai_tab->res*__i;
    LUT_data_t* Nai_row = Nai_tab->tab[__i];
    Nai_row[E_Na_idx] = (((R*T)/F)*(log((p->Nao/Nai))));
    Nai_row[Nai_15_idx] = (pow(Nai,1.5));
  }
  check_LUT(Nai_tab);
  
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -1000, 1000, 0.005, "MacCannell V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    double r_Kv_infinity = (1./(1.+(exp(((-(V+20.))/11.)))));
    double s_Kv_infinity = (1./(1.+(exp(((V+23.)/7.)))));
    double tau_r_Kv = ((20.3+(138.0*(exp(((-((V+20.)/25.9))*((V+20.)/25.9))))))/1000.);
    double tau_s_Kv = ((1574.0+(5268.0*(exp(((-((V+23.)/22.7))*((V+23.)/22.7))))))/1000.);
    V_row[r_Kv_rush_larsen_B_idx] = (exp(((-dt)/tau_r_Kv)));
    double r_Kv_rush_larsen_C = (expm1(((-dt)/tau_r_Kv)));
    V_row[s_Kv_rush_larsen_B_idx] = (exp(((-dt)/tau_s_Kv)));
    double s_Kv_rush_larsen_C = (expm1(((-dt)/tau_s_Kv)));
    V_row[r_Kv_rush_larsen_A_idx] = ((-r_Kv_infinity)*r_Kv_rush_larsen_C);
    V_row[s_Kv_rush_larsen_A_idx] = ((-s_Kv_infinity)*s_Kv_rush_larsen_C);
  }
  check_LUT(V_tab);
  
  
  // Create the V_EK lookup table
  LUT* V_EK_tab = &IF->tables[V_EK_TAB];
  LUT_alloc(V_EK_tab, NROWS_V_EK, -1000, 1000, 0.005, "MacCannell V_EK");
  for (int __i=V_EK_tab->mn_ind; __i<=V_EK_tab->mx_ind; __i++) {
    double V_EK = V_EK_tab->res*__i;
    LUT_data_t* V_EK_row = V_EK_tab->tab[__i];
    double alpha_K1 = (0.1/(1.+(exp((0.06*(V_EK-(200.0)))))));
    double beta_K1 = (((3.*(exp((0.0002*(V_EK+100.0)))))+(exp((0.1*(V_EK-(10.0))))))/(1.0+(exp((-0.5*V_EK)))));
    V_EK_row[IK1_idx] = (((p->GK1*alpha_K1)*V_EK)/(alpha_K1+beta_K1));
  }
  check_LUT(V_EK_tab);
  

}



void    initialize_sv_MacCannell( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e-3;
  cell_geom *region = &IF->cgeom;
  MacCannell_Params *p = (MacCannell_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(MacCannell_state) );
  MacCannell_state *sv_base = (MacCannell_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    MacCannell_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Ki = p->Ki_init;
    sv->Nai = p->Nai_init;
    V = V_init;
    sv->r_Kv = r_Kv_init;
    sv->s_Kv = s_Kv_init;
    double E_K = (((R*T)/F)*(log((p->Ko/sv->Ki))));
    double E_Na = (((R*T)/F)*(log((p->Nao/sv->Nai))));
    double Nai_15 = (pow(sv->Nai,1.5));
    double IKv = (((p->GKv*sv->r_Kv)*sv->s_Kv)*(V-(E_K)));
    double INaK = ((((p->maxINaK*(p->Ko/(p->Ko+K_mK)))*(Nai_15/(Nai_15+K_mNa_15)))*(V-(V_rev)))/(V-(B)));
    double IbNa = (p->GbNa*(V-(E_Na)));
    double V_EK = (V-(E_K));
    double alpha_K1 = (0.1/(1.+(exp((0.06*(V_EK-(200.0)))))));
    double beta_K1 = (((3.*(exp((0.0002*(V_EK+100.0)))))+(exp((0.1*(V_EK-(10.0))))))/(1.0+(exp((-0.5*V_EK)))));
    double IK1 = (((p->GK1*alpha_K1)*V_EK)/(alpha_K1+beta_K1));
    Iion = (((IKv+IK1)+IbNa)+INaK);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_MacCannell(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e-3;
  cell_geom *region = &IF->cgeom;
  MacCannell_Params *p  = (MacCannell_Params *)IF->params;
  MacCannell_state *sv_base = (MacCannell_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    MacCannell_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Ki_row[NROWS_Ki];
    LUT_interpRow(&IF->tables[Ki_TAB], sv->Ki, __i, Ki_row);
    LUT_data_t Nai_row[NROWS_Nai];
    LUT_interpRow(&IF->tables[Nai_TAB], sv->Nai, __i, Nai_row);
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t IKv = (((p->GKv*sv->r_Kv)*sv->s_Kv)*(V-(Ki_row[E_K_idx])));
    GlobalData_t INaK = ((((p->maxINaK*(p->Ko/(p->Ko+K_mK)))*(Nai_row[Nai_15_idx]/(Nai_row[Nai_15_idx]+K_mNa_15)))*(V-(V_rev)))/(V-(B)));
    GlobalData_t IbNa = (p->GbNa*(V-(Nai_row[E_Na_idx])));
    GlobalData_t V_EK = (V-(Ki_row[E_K_idx]));
    LUT_data_t V_EK_row[NROWS_V_EK];
    LUT_interpRow(&IF->tables[V_EK_TAB], V_EK, __i, V_EK_row);
    Iion = (((IKv+V_EK_row[IK1_idx])+IbNa)+INaK);
    
    
    //Complete Forward Euler Update
    GlobalData_t diff_Nai = (Cm*(((-IbNa)-((3.*INaK)))/(Vol_i*F)));
    GlobalData_t diff_Ki = (Cm*((-((V_EK_row[IK1_idx]+IKv)-((2.*INaK))))/(Vol_i*F)));
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t r_Kv_rush_larsen_B = V_row[r_Kv_rush_larsen_B_idx];
    GlobalData_t s_Kv_rush_larsen_B = V_row[s_Kv_rush_larsen_B_idx];
    GlobalData_t r_Kv_rush_larsen_A = V_row[r_Kv_rush_larsen_A_idx];
    GlobalData_t s_Kv_rush_larsen_A = V_row[s_Kv_rush_larsen_A_idx];
    GlobalData_t r_Kv_new = r_Kv_rush_larsen_A+r_Kv_rush_larsen_B*sv->r_Kv;
    GlobalData_t s_Kv_new = s_Kv_rush_larsen_A+s_Kv_rush_larsen_B*sv->s_Kv;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->r_Kv = r_Kv_new;
    sv->s_Kv = s_Kv_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        