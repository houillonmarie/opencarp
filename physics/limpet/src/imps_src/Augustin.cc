// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  DOI: GrandiPanditVoigt model (doi:10.1161/CIRCRESAHA.111.253955) coupled to the Land tension model (doi:10.1113/jphysiol.2012.231928)
*  Comment: GrandiPanditVoigt model (doi:10.1161/CIRCRESAHA.111.253955) coupled to the Land tension model (doi:10.1113/jphysiol.2012.231928)
*  Authors: Christoph M Augustin, Aurel Neic, Manfred Liebmann, Anton J Prassl, Steven A Niederer, Gundolf Haase, Gernot Plank
*  Year: 2016
*  Title: Anatomically accurate high resolution modeling of human whole heart electromechanics: A strongly scalable algebraic multigrid solver method for nonlinear deformation
*  Journal: J Comput Phys. 305:622-646
*  DOI: 10.1016/j.jcp.2015.10.045
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Augustin.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Augustin(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Augustin( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define Bmax_CaM (GlobalData_t)(24e-3)
#define Bmax_Naj (GlobalData_t)(7.561)
#define Bmax_Nasl (GlobalData_t)(1.65)
#define Bmax_SR (GlobalData_t)((19.*.9e-3))
#define Bmax_TnChigh (GlobalData_t)(140e-3)
#define Bmax_TnClow (GlobalData_t)(70e-3)
#define Bmax_myosin (GlobalData_t)(140e-3)
#define C10_init (GlobalData_t)(0.)
#define C11_init (GlobalData_t)(0.)
#define C12_init (GlobalData_t)(0.)
#define C13_init (GlobalData_t)(0.)
#define C14_init (GlobalData_t)(0.)
#define C15_init (GlobalData_t)(0.)
#define C1_init (GlobalData_t)(0.0015)
#define C2_init (GlobalData_t)(0.0244)
#define C3_init (GlobalData_t)(0.1494)
#define C4_init (GlobalData_t)(0.4071)
#define C5_init (GlobalData_t)(0.4161)
#define C7_init (GlobalData_t)(0.0001)
#define C8_init (GlobalData_t)(0.0006)
#define C9_init (GlobalData_t)(0.0008)
#define CaM_init (GlobalData_t)(2.911916e-4)
#define Ca_sr_init (GlobalData_t)(0.1e-1)
#define Ca_sr_rush_larsen_B (GlobalData_t)(1.)
#define Cai_rush_larsen_B (GlobalData_t)(1.)
#define Caj_init (GlobalData_t)(1.737475e-4)
#define Caj_rush_larsen_B (GlobalData_t)(1.)
#define Casl_init (GlobalData_t)(1.031812e-4)
#define Casl_rush_larsen_B (GlobalData_t)(1.)
#define Cm (GlobalData_t)(1.3810e-10)
#define Csqnb_init (GlobalData_t)(1.242988)
#define DcaJuncSL (GlobalData_t)(1.64e-6)
#define DcaSLcyto (GlobalData_t)(1.22e-6)
#define DnaJuncSL (GlobalData_t)(1.09e-5)
#define DnaSLcyto (GlobalData_t)(1.79e-5)
#define ENDO (GlobalData_t)(2.)
#define EPI (GlobalData_t)(0.)
#define Fjunc (GlobalData_t)(0.11)
#define Fjunc_CaL (GlobalData_t)(0.9)
#define Frdy (GlobalData_t)(96485.)
#define GCaB (GlobalData_t)(5.513e-4)
#define GClB (GlobalData_t)(9e-3)
#define GClCa (GlobalData_t)((0.5*0.109625))
#define GNa (GlobalData_t)(23.)
#define GNaB (GlobalData_t)(0.597e-3)
#define ICajuncint_init (GlobalData_t)(1.)
#define ICaslint_init (GlobalData_t)(0.)
#define IbarNCX (GlobalData_t)(4.5)
#define IbarNaK (GlobalData_t)(1.8)
#define IbarSLCaP (GlobalData_t)(0.0673)
#define J_ca_juncsl (GlobalData_t)((1./1.2134e12))
#define J_ca_slmyo (GlobalData_t)((1./2.68510e11))
#define J_na_juncsl (GlobalData_t)((1./((1.6382e12/3.)*100.)))
#define J_na_slmyo (GlobalData_t)((1./((1.8308e10/3.)*100.)))
#define KdClCa (GlobalData_t)(100e-3)
#define Kdact (GlobalData_t)(0.150e-3)
#define KmCai (GlobalData_t)(3.59e-3)
#define KmCao (GlobalData_t)(1.3)
#define KmKo (GlobalData_t)(1.5)
#define KmNai (GlobalData_t)(12.29)
#define KmNaip (GlobalData_t)(11.)
#define KmNao (GlobalData_t)(87.5)
#define KmPCa (GlobalData_t)(0.5e-3)
#define Kmf (GlobalData_t)(0.246e-3)
#define Kmr (GlobalData_t)(1.7)
#define MCELL (GlobalData_t)(1.)
#define MaxSR (GlobalData_t)(15.)
#define MinSR (GlobalData_t)(1.)
#define Myoc_init (GlobalData_t)(1.298754e-3)
#define Myom_init (GlobalData_t)(1.381982e-1)
#define NaBj_init (GlobalData_t)(3.539892)
#define NaBsl_init (GlobalData_t)(7.720854e-1)
#define Naj_init (GlobalData_t)(9.06)
#define Naj_rush_larsen_B (GlobalData_t)(1.)
#define Nasl_init (GlobalData_t)(9.06)
#define Nasl_rush_larsen_B (GlobalData_t)(1.)
#define O1_init (GlobalData_t)(0.)
#define O2_init (GlobalData_t)(0.)
#define Q10CaL (GlobalData_t)(1.8)
#define Q10KmNai (GlobalData_t)(1.39)
#define Q10NCX (GlobalData_t)(1.57)
#define Q10NaK (GlobalData_t)(1.63)
#define Q10SLCaP (GlobalData_t)(2.35)
#define Q10SRCaP (GlobalData_t)(2.6)
#define Q_1_init (GlobalData_t)(0.)
#define Q_2_init (GlobalData_t)(0.)
#define R (GlobalData_t)(8314.)
#define RyRi_init (GlobalData_t)(1.024274e-7)
#define RyRo_init (GlobalData_t)(8.156628e-7)
#define RyRr_init (GlobalData_t)(8.884332e-1)
#define SLHj_init (GlobalData_t)(7.347888e-3)
#define SLHsl_init (GlobalData_t)(7.297378e-2)
#define SLLj_init (GlobalData_t)(9.566355e-3)
#define SLLsl_init (GlobalData_t)(1.110363e-1)
#define SRB_init (GlobalData_t)(2.143165e-3)
#define Temp (GlobalData_t)(310.)
#define TnCHc_init (GlobalData_t)(1.078283e-1)
#define TnCHm_init (GlobalData_t)(1.524002e-2)
#define TnCL_init (GlobalData_t)(8.773191e-3)
#define V_init (GlobalData_t)(-8.09763e+1)
#define Vmax_SRCaP (GlobalData_t)(5.3114e-3)
#define cellLength (GlobalData_t)(100.)
#define cellRadius (GlobalData_t)(10.25)
#define d_init (GlobalData_t)(7.175662e-6)
#define d_remove_tol (GlobalData_t)(1e-4)
#define delta_sl_init (GlobalData_t)(0.)
#define distJuncSL (GlobalData_t)(0.5)
#define distSLcyto (GlobalData_t)(0.45)
#define ec50SR (GlobalData_t)(0.45)
#define f_init (GlobalData_t)(0.998607)
#define fcaBj_init (GlobalData_t)(2.421991e-2)
#define fcaBsl_init (GlobalData_t)(1.452605e-2)
#define fcaCaMSL (GlobalData_t)(0.)
#define fcaCaj (GlobalData_t)(0.)
#define gkp (GlobalData_t)((2.*0.001))
#define h_init (GlobalData_t)(9.867005e-1)
#define hillSRCaP (GlobalData_t)(1.787)
#define j_init (GlobalData_t)(9.915620e-1)
#define junctionLength (GlobalData_t)(160e-3)
#define junctionRadius (GlobalData_t)(15e-3)
#define kiCa (GlobalData_t)(0.5)
#define kim (GlobalData_t)(0.005)
#define koCa (GlobalData_t)(10.)
#define koff_cam (GlobalData_t)(238e-3)
#define koff_csqn (GlobalData_t)(65.)
#define koff_myoca (GlobalData_t)(0.46e-3)
#define koff_myomg (GlobalData_t)(0.057e-3)
#define koff_na (GlobalData_t)(1e-3)
#define koff_slh (GlobalData_t)(30e-3)
#define koff_sll (GlobalData_t)(1300e-3)
#define koff_sr (GlobalData_t)(60e-3)
#define koff_tnchca (GlobalData_t)(0.032e-3)
#define koff_tnchmg (GlobalData_t)(3.33e-3)
#define koff_tncl (GlobalData_t)(19.6e-3)
#define kom (GlobalData_t)(0.06)
#define kon_cam (GlobalData_t)(34.)
#define kon_csqn (GlobalData_t)(100.)
#define kon_myoca (GlobalData_t)(13.8)
#define kon_myomg (GlobalData_t)(0.0157)
#define kon_na (GlobalData_t)(0.1e-3)
#define kon_slh (GlobalData_t)(100.)
#define kon_sll (GlobalData_t)(100.)
#define kon_sr (GlobalData_t)(100.)
#define kon_tnchca (GlobalData_t)(2.37)
#define kon_tnchmg (GlobalData_t)(3e-3)
#define kon_tncl (GlobalData_t)(32.7)
#define ks (GlobalData_t)(25.)
#define ksat (GlobalData_t)(0.32)
#define length_init (GlobalData_t)(1.)
#define m_init (GlobalData_t)(1.405627e-3)
#define nu (GlobalData_t)(0.27)
#define pCa (GlobalData_t)((0.50*5.4e-4))
#define pK (GlobalData_t)((0.50*2.7e-7))
#define pNa (GlobalData_t)((0.50*1.5e-8))
#define pNaK (GlobalData_t)(0.01833)
#define partial_A_1_del_Q_1 (GlobalData_t)(0.)
#define partial_A_2_del_Q_2 (GlobalData_t)(0.)
#define partial_Bmax_CaM_del_CaM (GlobalData_t)(0.)
#define partial_Bmax_CaM_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_Csqn_del_Ca_sr (GlobalData_t)(0.)
#define partial_Bmax_Csqn_del_Csqnb (GlobalData_t)(0.)
#define partial_Bmax_Naj_del_NaBj (GlobalData_t)(0.)
#define partial_Bmax_Naj_del_Naj (GlobalData_t)(0.)
#define partial_Bmax_Nasl_del_NaBsl (GlobalData_t)(0.)
#define partial_Bmax_Nasl_del_Nasl (GlobalData_t)(0.)
#define partial_Bmax_SLhighj_del_Caj (GlobalData_t)(0.)
#define partial_Bmax_SLhighj_del_SLHj (GlobalData_t)(0.)
#define partial_Bmax_SLhighsl_del_Casl (GlobalData_t)(0.)
#define partial_Bmax_SLhighsl_del_SLHsl (GlobalData_t)(0.)
#define partial_Bmax_SLlowj_del_Caj (GlobalData_t)(0.)
#define partial_Bmax_SLlowj_del_SLLj (GlobalData_t)(0.)
#define partial_Bmax_SLlowsl_del_Casl (GlobalData_t)(0.)
#define partial_Bmax_SLlowsl_del_SLLsl (GlobalData_t)(0.)
#define partial_Bmax_SR_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_SR_del_SRB (GlobalData_t)(0.)
#define partial_Bmax_TnChigh_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_TnChigh_del_TnCHc (GlobalData_t)(0.)
#define partial_Bmax_TnChigh_del_TnCHm (GlobalData_t)(0.)
#define partial_Bmax_TnClow_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_TnClow_del_xb (GlobalData_t)(0.)
#define partial_Bmax_myosin_del_Cai (GlobalData_t)(0.)
#define partial_Bmax_myosin_del_Myoc (GlobalData_t)(0.)
#define partial_Bmax_myosin_del_Myom (GlobalData_t)(0.)
#define partial_Ca_50_mM_del_Cai (GlobalData_t)(0.)
#define partial_Ca_50ref_del_Cai (GlobalData_t)(0.)
#define partial_Ca_50ref_mM_del_Cai (GlobalData_t)(0.)
#define partial_Cai_mM_del_CaM (GlobalData_t)(0.)
#define partial_Cai_mM_del_Ca_sr (GlobalData_t)(0.)
#define partial_Cai_mM_del_Cai (GlobalData_t)((1000./(1000.*1000.)))
#define partial_Cai_mM_del_Casl (GlobalData_t)(0.)
#define partial_Cai_mM_del_Myoc (GlobalData_t)(0.)
#define partial_Cai_mM_del_SRB (GlobalData_t)(0.)
#define partial_Cai_mM_del_TnCHc (GlobalData_t)(0.)
#define partial_Cao_del_Caj (GlobalData_t)(0.)
#define partial_Cao_del_Casl (GlobalData_t)(0.)
#define partial_Cao_del_Naj (GlobalData_t)(0.)
#define partial_Cao_del_Nasl (GlobalData_t)(0.)
#define partial_Cm_del_Caj (GlobalData_t)(0.)
#define partial_Cm_del_Casl (GlobalData_t)(0.)
#define partial_Cm_del_Naj (GlobalData_t)(0.)
#define partial_Cm_del_Nasl (GlobalData_t)(0.)
#define partial_Fjunc_CaL_del_Caj (GlobalData_t)(0.)
#define partial_Fjunc_CaL_del_Casl (GlobalData_t)(0.)
#define partial_Fjunc_CaL_del_Naj (GlobalData_t)(0.)
#define partial_Fjunc_CaL_del_Nasl (GlobalData_t)(0.)
#define partial_Fjunc_del_Caj (GlobalData_t)(0.)
#define partial_Fjunc_del_Casl (GlobalData_t)(0.)
#define partial_Fjunc_del_Naj (GlobalData_t)(0.)
#define partial_Fjunc_del_Nasl (GlobalData_t)(0.)
#define partial_FoRT_del_C1 (GlobalData_t)(0.)
#define partial_FoRT_del_C10 (GlobalData_t)(0.)
#define partial_FoRT_del_C11 (GlobalData_t)(0.)
#define partial_FoRT_del_C12 (GlobalData_t)(0.)
#define partial_FoRT_del_C13 (GlobalData_t)(0.)
#define partial_FoRT_del_C14 (GlobalData_t)(0.)
#define partial_FoRT_del_C15 (GlobalData_t)(0.)
#define partial_FoRT_del_C2 (GlobalData_t)(0.)
#define partial_FoRT_del_C3 (GlobalData_t)(0.)
#define partial_FoRT_del_C4 (GlobalData_t)(0.)
#define partial_FoRT_del_C5 (GlobalData_t)(0.)
#define partial_FoRT_del_C6 (GlobalData_t)(0.)
#define partial_FoRT_del_C7 (GlobalData_t)(0.)
#define partial_FoRT_del_C8 (GlobalData_t)(0.)
#define partial_FoRT_del_C9 (GlobalData_t)(0.)
#define partial_FoRT_del_Caj (GlobalData_t)(0.)
#define partial_FoRT_del_Casl (GlobalData_t)(0.)
#define partial_FoRT_del_Naj (GlobalData_t)(0.)
#define partial_FoRT_del_Nasl (GlobalData_t)(0.)
#define partial_FoRT_del_O1 (GlobalData_t)(0.)
#define partial_Frdy_del_C1 (GlobalData_t)(0.)
#define partial_Frdy_del_C10 (GlobalData_t)(0.)
#define partial_Frdy_del_C11 (GlobalData_t)(0.)
#define partial_Frdy_del_C12 (GlobalData_t)(0.)
#define partial_Frdy_del_C13 (GlobalData_t)(0.)
#define partial_Frdy_del_C14 (GlobalData_t)(0.)
#define partial_Frdy_del_C15 (GlobalData_t)(0.)
#define partial_Frdy_del_C2 (GlobalData_t)(0.)
#define partial_Frdy_del_C3 (GlobalData_t)(0.)
#define partial_Frdy_del_C4 (GlobalData_t)(0.)
#define partial_Frdy_del_C5 (GlobalData_t)(0.)
#define partial_Frdy_del_C6 (GlobalData_t)(0.)
#define partial_Frdy_del_C7 (GlobalData_t)(0.)
#define partial_Frdy_del_C8 (GlobalData_t)(0.)
#define partial_Frdy_del_C9 (GlobalData_t)(0.)
#define partial_Frdy_del_Caj (GlobalData_t)(0.)
#define partial_Frdy_del_Casl (GlobalData_t)(0.)
#define partial_Frdy_del_Naj (GlobalData_t)(0.)
#define partial_Frdy_del_Nasl (GlobalData_t)(0.)
#define partial_Frdy_del_O1 (GlobalData_t)(0.)
#define partial_Fsl_CaL_del_Casl (GlobalData_t)(0.)
#define partial_Fsl_CaL_del_Nasl (GlobalData_t)(0.)
#define partial_Fsl_del_Casl (GlobalData_t)(0.)
#define partial_Fsl_del_Nasl (GlobalData_t)(0.)
#define partial_GCaB_del_Caj (GlobalData_t)(0.)
#define partial_GCaB_del_Casl (GlobalData_t)(0.)
#define partial_GNaB_del_Naj (GlobalData_t)(0.)
#define partial_GNaB_del_Nasl (GlobalData_t)(0.)
#define partial_GNa_del_Naj (GlobalData_t)(0.)
#define partial_GNa_del_Nasl (GlobalData_t)(0.)
#define partial_IbarNCX_del_Caj (GlobalData_t)(0.)
#define partial_IbarNCX_del_Casl (GlobalData_t)(0.)
#define partial_IbarNCX_del_Naj (GlobalData_t)(0.)
#define partial_IbarNCX_del_Nasl (GlobalData_t)(0.)
#define partial_IbarNaK_del_Naj (GlobalData_t)(0.)
#define partial_IbarNaK_del_Nasl (GlobalData_t)(0.)
#define partial_IbarSLCaP_del_Caj (GlobalData_t)(0.)
#define partial_IbarSLCaP_del_Casl (GlobalData_t)(0.)
#define partial_J_SRleak_del_Ca_sr (GlobalData_t)(5.348e-6)
#define partial_J_SRleak_del_Caj (GlobalData_t)((5.348e-6*-1.))
#define partial_J_ca_juncsl_del_Caj (GlobalData_t)(0.)
#define partial_J_ca_juncsl_del_Casl (GlobalData_t)(0.)
#define partial_J_ca_slmyo_del_Cai (GlobalData_t)(0.)
#define partial_J_ca_slmyo_del_Casl (GlobalData_t)(0.)
#define partial_J_na_juncsl_del_Naj (GlobalData_t)(0.)
#define partial_J_na_juncsl_del_Nasl (GlobalData_t)(0.)
#define partial_J_na_slmyo_del_Nasl (GlobalData_t)(0.)
#define partial_Ka_junc_del_Naj (GlobalData_t)(0.)
#define partial_Ka_sl_del_Nasl (GlobalData_t)(0.)
#define partial_Kdact_del_Caj (GlobalData_t)(0.)
#define partial_Kdact_del_Casl (GlobalData_t)(0.)
#define partial_Kdact_del_Naj (GlobalData_t)(0.)
#define partial_Kdact_del_Nasl (GlobalData_t)(0.)
#define partial_KmCai_del_Caj (GlobalData_t)(0.)
#define partial_KmCai_del_Casl (GlobalData_t)(0.)
#define partial_KmCai_del_Naj (GlobalData_t)(0.)
#define partial_KmCai_del_Nasl (GlobalData_t)(0.)
#define partial_KmCao_del_Caj (GlobalData_t)(0.)
#define partial_KmCao_del_Casl (GlobalData_t)(0.)
#define partial_KmCao_del_Naj (GlobalData_t)(0.)
#define partial_KmCao_del_Nasl (GlobalData_t)(0.)
#define partial_KmKo_del_Naj (GlobalData_t)(0.)
#define partial_KmKo_del_Nasl (GlobalData_t)(0.)
#define partial_KmNai_del_Caj (GlobalData_t)(0.)
#define partial_KmNai_del_Casl (GlobalData_t)(0.)
#define partial_KmNai_del_Naj (GlobalData_t)(0.)
#define partial_KmNai_del_Nasl (GlobalData_t)(0.)
#define partial_KmNaip_del_Naj (GlobalData_t)(0.)
#define partial_KmNaip_del_Nasl (GlobalData_t)(0.)
#define partial_KmNao_del_Caj (GlobalData_t)(0.)
#define partial_KmNao_del_Casl (GlobalData_t)(0.)
#define partial_KmNao_del_Naj (GlobalData_t)(0.)
#define partial_KmNao_del_Nasl (GlobalData_t)(0.)
#define partial_KmPCa_del_Caj (GlobalData_t)(0.)
#define partial_KmPCa_del_Casl (GlobalData_t)(0.)
#define partial_Kmf_del_Ca_sr (GlobalData_t)(0.)
#define partial_Kmf_del_Cai (GlobalData_t)(0.)
#define partial_Kmr_del_Ca_sr (GlobalData_t)(0.)
#define partial_Kmr_del_Cai (GlobalData_t)(0.)
#define partial_Ko_del_Naj (GlobalData_t)(0.)
#define partial_Ko_del_Nasl (GlobalData_t)(0.)
#define partial_MaxSR_del_RyRi (GlobalData_t)(0.)
#define partial_MaxSR_del_RyRo (GlobalData_t)(0.)
#define partial_MaxSR_del_RyRr (GlobalData_t)(0.)
#define partial_Mgi_del_Cai (GlobalData_t)(0.)
#define partial_Mgi_del_Myom (GlobalData_t)(0.)
#define partial_Mgi_del_TnCHm (GlobalData_t)(0.)
#define partial_MinSR_del_RyRi (GlobalData_t)(0.)
#define partial_MinSR_del_RyRo (GlobalData_t)(0.)
#define partial_MinSR_del_RyRr (GlobalData_t)(0.)
#define partial_Nao_del_Caj (GlobalData_t)(0.)
#define partial_Nao_del_Casl (GlobalData_t)(0.)
#define partial_Nao_del_Naj (GlobalData_t)(0.)
#define partial_Nao_del_Nasl (GlobalData_t)(0.)
#define partial_O2_del_O1 (GlobalData_t)(-1.)
#define partial_Q10CaL_del_Caj (GlobalData_t)(0.)
#define partial_Q10CaL_del_Casl (GlobalData_t)(0.)
#define partial_Q10CaL_del_Naj (GlobalData_t)(0.)
#define partial_Q10CaL_del_Nasl (GlobalData_t)(0.)
#define partial_Q10NCX_del_Caj (GlobalData_t)(0.)
#define partial_Q10NCX_del_Casl (GlobalData_t)(0.)
#define partial_Q10NCX_del_Naj (GlobalData_t)(0.)
#define partial_Q10NCX_del_Nasl (GlobalData_t)(0.)
#define partial_Q10SLCaP_del_Caj (GlobalData_t)(0.)
#define partial_Q10SLCaP_del_Casl (GlobalData_t)(0.)
#define partial_Q10SRCaP_del_Ca_sr (GlobalData_t)(0.)
#define partial_Q10SRCaP_del_Cai (GlobalData_t)(0.)
#define partial_Qpow_del_Ca_sr (GlobalData_t)(0.)
#define partial_Qpow_del_Cai (GlobalData_t)(0.)
#define partial_Qpow_del_Caj (GlobalData_t)(0.)
#define partial_Qpow_del_Casl (GlobalData_t)(0.)
#define partial_Qpow_del_Naj (GlobalData_t)(0.)
#define partial_Qpow_del_Nasl (GlobalData_t)(0.)
#define partial_RI_del_RyRi (GlobalData_t)(-1.)
#define partial_RI_del_RyRr (GlobalData_t)(-1.)
#define partial_R_del_C1 (GlobalData_t)(0.)
#define partial_R_del_C10 (GlobalData_t)(0.)
#define partial_R_del_C11 (GlobalData_t)(0.)
#define partial_R_del_C12 (GlobalData_t)(0.)
#define partial_R_del_C13 (GlobalData_t)(0.)
#define partial_R_del_C14 (GlobalData_t)(0.)
#define partial_R_del_C15 (GlobalData_t)(0.)
#define partial_R_del_C2 (GlobalData_t)(0.)
#define partial_R_del_C3 (GlobalData_t)(0.)
#define partial_R_del_C4 (GlobalData_t)(0.)
#define partial_R_del_C5 (GlobalData_t)(0.)
#define partial_R_del_C6 (GlobalData_t)(0.)
#define partial_R_del_C7 (GlobalData_t)(0.)
#define partial_R_del_C8 (GlobalData_t)(0.)
#define partial_R_del_C9 (GlobalData_t)(0.)
#define partial_R_del_Caj (GlobalData_t)(0.)
#define partial_R_del_Casl (GlobalData_t)(0.)
#define partial_R_del_Naj (GlobalData_t)(0.)
#define partial_R_del_Nasl (GlobalData_t)(0.)
#define partial_R_del_O1 (GlobalData_t)(0.)
#define partial_TRPN_50_del_xb (GlobalData_t)(0.)
#define partial_Temp_del_C1 (GlobalData_t)(0.)
#define partial_Temp_del_C10 (GlobalData_t)(0.)
#define partial_Temp_del_C11 (GlobalData_t)(0.)
#define partial_Temp_del_C12 (GlobalData_t)(0.)
#define partial_Temp_del_C13 (GlobalData_t)(0.)
#define partial_Temp_del_C14 (GlobalData_t)(0.)
#define partial_Temp_del_C15 (GlobalData_t)(0.)
#define partial_Temp_del_C2 (GlobalData_t)(0.)
#define partial_Temp_del_C3 (GlobalData_t)(0.)
#define partial_Temp_del_C4 (GlobalData_t)(0.)
#define partial_Temp_del_C5 (GlobalData_t)(0.)
#define partial_Temp_del_C6 (GlobalData_t)(0.)
#define partial_Temp_del_C7 (GlobalData_t)(0.)
#define partial_Temp_del_C8 (GlobalData_t)(0.)
#define partial_Temp_del_C9 (GlobalData_t)(0.)
#define partial_Temp_del_Ca_sr (GlobalData_t)(0.)
#define partial_Temp_del_Cai (GlobalData_t)(0.)
#define partial_Temp_del_Caj (GlobalData_t)(0.)
#define partial_Temp_del_Casl (GlobalData_t)(0.)
#define partial_Temp_del_Naj (GlobalData_t)(0.)
#define partial_Temp_del_Nasl (GlobalData_t)(0.)
#define partial_Temp_del_O1 (GlobalData_t)(0.)
#define partial_Vcell_del_Ca_sr (GlobalData_t)(0.)
#define partial_Vcell_del_Cai (GlobalData_t)(0.)
#define partial_Vcell_del_Caj (GlobalData_t)(0.)
#define partial_Vcell_del_Casl (GlobalData_t)(0.)
#define partial_Vcell_del_Csqnb (GlobalData_t)(0.)
#define partial_Vcell_del_Naj (GlobalData_t)(0.)
#define partial_Vcell_del_Nasl (GlobalData_t)(0.)
#define partial_Vcell_del_SLHj (GlobalData_t)(0.)
#define partial_Vcell_del_SLHsl (GlobalData_t)(0.)
#define partial_Vcell_del_SLLj (GlobalData_t)(0.)
#define partial_Vcell_del_SLLsl (GlobalData_t)(0.)
#define partial_Vjunc_del_Caj (GlobalData_t)(0.)
#define partial_Vjunc_del_Naj (GlobalData_t)(0.)
#define partial_Vjunc_del_SLHj (GlobalData_t)(0.)
#define partial_Vjunc_del_SLLj (GlobalData_t)(0.)
#define partial_Vmax_SRCaP_del_Ca_sr (GlobalData_t)(0.)
#define partial_Vmax_SRCaP_del_Cai (GlobalData_t)(0.)
#define partial_Vmyo_del_Ca_sr (GlobalData_t)(0.)
#define partial_Vmyo_del_Cai (GlobalData_t)(0.)
#define partial_Vmyo_del_Caj (GlobalData_t)(0.)
#define partial_Vmyo_del_Casl (GlobalData_t)(0.)
#define partial_Vmyo_del_Csqnb (GlobalData_t)(0.)
#define partial_Vmyo_del_SLHj (GlobalData_t)(0.)
#define partial_Vmyo_del_SLHsl (GlobalData_t)(0.)
#define partial_Vmyo_del_SLLj (GlobalData_t)(0.)
#define partial_Vmyo_del_SLLsl (GlobalData_t)(0.)
#define partial_Vsl_del_Casl (GlobalData_t)(0.)
#define partial_Vsl_del_Nasl (GlobalData_t)(0.)
#define partial_Vsl_del_SLHsl (GlobalData_t)(0.)
#define partial_Vsl_del_SLLsl (GlobalData_t)(0.)
#define partial_Vsr_del_Ca_sr (GlobalData_t)(0.)
#define partial_Vsr_del_Cai (GlobalData_t)(0.)
#define partial_Vsr_del_Caj (GlobalData_t)(0.)
#define partial_Vsr_del_Csqnb (GlobalData_t)(0.)
#define partial_alpha_1_del_Q_1 (GlobalData_t)(0.)
#define partial_alpha_2_del_Q_2 (GlobalData_t)(0.)
#define partial_alpha_del_C1 (GlobalData_t)(0.)
#define partial_alpha_del_C10 (GlobalData_t)(0.)
#define partial_alpha_del_C11 (GlobalData_t)(0.)
#define partial_alpha_del_C12 (GlobalData_t)(0.)
#define partial_alpha_del_C13 (GlobalData_t)(0.)
#define partial_alpha_del_C14 (GlobalData_t)(0.)
#define partial_alpha_del_C2 (GlobalData_t)(0.)
#define partial_alpha_del_C3 (GlobalData_t)(0.)
#define partial_alpha_del_C4 (GlobalData_t)(0.)
#define partial_alpha_del_C5 (GlobalData_t)(0.)
#define partial_alpha_del_C6 (GlobalData_t)(0.)
#define partial_alpha_del_C7 (GlobalData_t)(0.)
#define partial_alpha_del_C8 (GlobalData_t)(0.)
#define partial_alpha_del_C9 (GlobalData_t)(0.)
#define partial_beta_1_del_Cai (GlobalData_t)(0.)
#define partial_beta_del_C1 (GlobalData_t)(0.)
#define partial_beta_del_C10 (GlobalData_t)(0.)
#define partial_beta_del_C11 (GlobalData_t)(0.)
#define partial_beta_del_C12 (GlobalData_t)(0.)
#define partial_beta_del_C13 (GlobalData_t)(0.)
#define partial_beta_del_C14 (GlobalData_t)(0.)
#define partial_beta_del_C2 (GlobalData_t)(0.)
#define partial_beta_del_C3 (GlobalData_t)(0.)
#define partial_beta_del_C4 (GlobalData_t)(0.)
#define partial_beta_del_C5 (GlobalData_t)(0.)
#define partial_beta_del_C6 (GlobalData_t)(0.)
#define partial_beta_del_C7 (GlobalData_t)(0.)
#define partial_beta_del_C8 (GlobalData_t)(0.)
#define partial_beta_del_C9 (GlobalData_t)(0.)
#define partial_cellLength_del_Ca_sr (GlobalData_t)(0.)
#define partial_cellLength_del_Cai (GlobalData_t)(0.)
#define partial_cellLength_del_Caj (GlobalData_t)(0.)
#define partial_cellLength_del_Casl (GlobalData_t)(0.)
#define partial_cellLength_del_Csqnb (GlobalData_t)(0.)
#define partial_cellLength_del_Naj (GlobalData_t)(0.)
#define partial_cellLength_del_Nasl (GlobalData_t)(0.)
#define partial_cellLength_del_SLHj (GlobalData_t)(0.)
#define partial_cellLength_del_SLHsl (GlobalData_t)(0.)
#define partial_cellLength_del_SLLj (GlobalData_t)(0.)
#define partial_cellLength_del_SLLsl (GlobalData_t)(0.)
#define partial_cellRadius_del_Ca_sr (GlobalData_t)(0.)
#define partial_cellRadius_del_Cai (GlobalData_t)(0.)
#define partial_cellRadius_del_Caj (GlobalData_t)(0.)
#define partial_cellRadius_del_Casl (GlobalData_t)(0.)
#define partial_cellRadius_del_Csqnb (GlobalData_t)(0.)
#define partial_cellRadius_del_Naj (GlobalData_t)(0.)
#define partial_cellRadius_del_Nasl (GlobalData_t)(0.)
#define partial_cellRadius_del_SLHj (GlobalData_t)(0.)
#define partial_cellRadius_del_SLHsl (GlobalData_t)(0.)
#define partial_cellRadius_del_SLLj (GlobalData_t)(0.)
#define partial_cellRadius_del_SLLsl (GlobalData_t)(0.)
#define partial_delta_del_C10 (GlobalData_t)(0.)
#define partial_delta_del_C11 (GlobalData_t)(0.)
#define partial_delta_del_C12 (GlobalData_t)(0.)
#define partial_delta_del_C13 (GlobalData_t)(0.)
#define partial_delta_del_C14 (GlobalData_t)(0.)
#define partial_delta_del_C15 (GlobalData_t)(0.)
#define partial_delta_del_C5 (GlobalData_t)(0.)
#define partial_delta_del_C6 (GlobalData_t)(0.)
#define partial_delta_del_C7 (GlobalData_t)(0.)
#define partial_delta_del_C8 (GlobalData_t)(0.)
#define partial_delta_del_C9 (GlobalData_t)(0.)
#define partial_diff_Myom_del_Cai (GlobalData_t)(0.)
#define partial_diff_TnCHm_del_Cai (GlobalData_t)(0.)
#define partial_dlambdadt_del_Q_1 (GlobalData_t)(0.)
#define partial_dlambdadt_del_Q_2 (GlobalData_t)(0.)
#define partial_ec50SR_del_RyRi (GlobalData_t)(0.)
#define partial_ec50SR_del_RyRo (GlobalData_t)(0.)
#define partial_ec50SR_del_RyRr (GlobalData_t)(0.)
#define partial_eta_del_C15 (GlobalData_t)(0.)
#define partial_eta_del_O1 (GlobalData_t)(0.)
#define partial_exp_2VFoRT_del_Caj (GlobalData_t)(0.)
#define partial_exp_2VFoRT_del_Casl (GlobalData_t)(0.)
#define partial_exp_VFoRT_del_Naj (GlobalData_t)(0.)
#define partial_exp_VFoRT_del_Nasl (GlobalData_t)(0.)
#define partial_exp_nu_V_FoRT_del_Caj (GlobalData_t)(0.)
#define partial_exp_nu_V_FoRT_del_Casl (GlobalData_t)(0.)
#define partial_exp_nu_V_FoRT_del_Naj (GlobalData_t)(0.)
#define partial_exp_nu_V_FoRT_del_Nasl (GlobalData_t)(0.)
#define partial_expm1_2VFoRT_del_Caj (GlobalData_t)(0.)
#define partial_expm1_2VFoRT_del_Casl (GlobalData_t)(0.)
#define partial_expm1_VFoRT_del_Naj (GlobalData_t)(0.)
#define partial_expm1_VFoRT_del_Nasl (GlobalData_t)(0.)
#define partial_expm1_nu_V_FoRT_del_Caj (GlobalData_t)(0.)
#define partial_expm1_nu_V_FoRT_del_Casl (GlobalData_t)(0.)
#define partial_expm1_nu_V_FoRT_del_Naj (GlobalData_t)(0.)
#define partial_expm1_nu_V_FoRT_del_Nasl (GlobalData_t)(0.)
#define partial_fcaCaMSL_del_Casl (GlobalData_t)(0.)
#define partial_fcaCaMSL_del_Nasl (GlobalData_t)(0.)
#define partial_fcaCaj_del_Caj (GlobalData_t)(0.)
#define partial_fcaCaj_del_Naj (GlobalData_t)(0.)
#define partial_fnak_del_Naj (GlobalData_t)(0.)
#define partial_fnak_del_Nasl (GlobalData_t)(0.)
#define partial_gamma_del_C10 (GlobalData_t)(0.)
#define partial_gamma_del_C11 (GlobalData_t)(0.)
#define partial_gamma_del_C12 (GlobalData_t)(0.)
#define partial_gamma_del_C13 (GlobalData_t)(0.)
#define partial_gamma_del_C14 (GlobalData_t)(0.)
#define partial_gamma_del_C15 (GlobalData_t)(0.)
#define partial_gamma_del_C2 (GlobalData_t)(0.)
#define partial_gamma_del_C3 (GlobalData_t)(0.)
#define partial_gamma_del_C4 (GlobalData_t)(0.)
#define partial_gamma_del_C5 (GlobalData_t)(0.)
#define partial_gamma_del_C6 (GlobalData_t)(0.)
#define partial_gamma_del_C7 (GlobalData_t)(0.)
#define partial_gamma_del_C8 (GlobalData_t)(0.)
#define partial_gamma_del_C9 (GlobalData_t)(0.)
#define partial_hillSRCaP_del_Ca_sr (GlobalData_t)(0.)
#define partial_hillSRCaP_del_Cai (GlobalData_t)(0.)
#define partial_kCaSR_del_RyRi (GlobalData_t)(0.)
#define partial_kCaSR_del_RyRo (GlobalData_t)(0.)
#define partial_kCaSR_del_RyRr (GlobalData_t)(0.)
#define partial_k_TRPN_del_Cai (GlobalData_t)(0.)
#define partial_k_xb_del_xb (GlobalData_t)(0.)
#define partial_kiCa_del_RyRi (GlobalData_t)(0.)
#define partial_kiCa_del_RyRo (GlobalData_t)(0.)
#define partial_kiCa_del_RyRr (GlobalData_t)(0.)
#define partial_kiSRCa_del_RyRi (GlobalData_t)(0.)
#define partial_kiSRCa_del_RyRo (GlobalData_t)(0.)
#define partial_kiSRCa_del_RyRr (GlobalData_t)(0.)
#define partial_kim_del_RyRi (GlobalData_t)(0.)
#define partial_kim_del_RyRo (GlobalData_t)(0.)
#define partial_kim_del_RyRr (GlobalData_t)(0.)
#define partial_koCa_del_RyRi (GlobalData_t)(0.)
#define partial_koCa_del_RyRo (GlobalData_t)(0.)
#define partial_koCa_del_RyRr (GlobalData_t)(0.)
#define partial_koSRCa_del_RyRi (GlobalData_t)(0.)
#define partial_koSRCa_del_RyRo (GlobalData_t)(0.)
#define partial_koSRCa_del_RyRr (GlobalData_t)(0.)
#define partial_koff_cam_del_CaM (GlobalData_t)(0.)
#define partial_koff_cam_del_Cai (GlobalData_t)(0.)
#define partial_koff_csqn_del_Ca_sr (GlobalData_t)(0.)
#define partial_koff_csqn_del_Csqnb (GlobalData_t)(0.)
#define partial_koff_myoca_del_Cai (GlobalData_t)(0.)
#define partial_koff_myoca_del_Myoc (GlobalData_t)(0.)
#define partial_koff_myomg_del_Cai (GlobalData_t)(0.)
#define partial_koff_myomg_del_Myom (GlobalData_t)(0.)
#define partial_koff_na_del_NaBj (GlobalData_t)(0.)
#define partial_koff_na_del_NaBsl (GlobalData_t)(0.)
#define partial_koff_na_del_Naj (GlobalData_t)(0.)
#define partial_koff_na_del_Nasl (GlobalData_t)(0.)
#define partial_koff_slh_del_Caj (GlobalData_t)(0.)
#define partial_koff_slh_del_Casl (GlobalData_t)(0.)
#define partial_koff_slh_del_SLHj (GlobalData_t)(0.)
#define partial_koff_slh_del_SLHsl (GlobalData_t)(0.)
#define partial_koff_sll_del_Caj (GlobalData_t)(0.)
#define partial_koff_sll_del_Casl (GlobalData_t)(0.)
#define partial_koff_sll_del_SLLj (GlobalData_t)(0.)
#define partial_koff_sll_del_SLLsl (GlobalData_t)(0.)
#define partial_koff_sr_del_Cai (GlobalData_t)(0.)
#define partial_koff_sr_del_SRB (GlobalData_t)(0.)
#define partial_koff_tnchca_del_Cai (GlobalData_t)(0.)
#define partial_koff_tnchca_del_TnCHc (GlobalData_t)(0.)
#define partial_koff_tnchmg_del_Cai (GlobalData_t)(0.)
#define partial_koff_tnchmg_del_TnCHm (GlobalData_t)(0.)
#define partial_kom_del_RyRi (GlobalData_t)(0.)
#define partial_kom_del_RyRo (GlobalData_t)(0.)
#define partial_kom_del_RyRr (GlobalData_t)(0.)
#define partial_kon_cam_del_CaM (GlobalData_t)(0.)
#define partial_kon_cam_del_Cai (GlobalData_t)(0.)
#define partial_kon_csqn_del_Ca_sr (GlobalData_t)(0.)
#define partial_kon_csqn_del_Csqnb (GlobalData_t)(0.)
#define partial_kon_myoca_del_Cai (GlobalData_t)(0.)
#define partial_kon_myoca_del_Myoc (GlobalData_t)(0.)
#define partial_kon_myomg_del_Cai (GlobalData_t)(0.)
#define partial_kon_myomg_del_Myom (GlobalData_t)(0.)
#define partial_kon_na_del_NaBj (GlobalData_t)(0.)
#define partial_kon_na_del_NaBsl (GlobalData_t)(0.)
#define partial_kon_na_del_Naj (GlobalData_t)(0.)
#define partial_kon_na_del_Nasl (GlobalData_t)(0.)
#define partial_kon_slh_del_Caj (GlobalData_t)(0.)
#define partial_kon_slh_del_Casl (GlobalData_t)(0.)
#define partial_kon_slh_del_SLHj (GlobalData_t)(0.)
#define partial_kon_slh_del_SLHsl (GlobalData_t)(0.)
#define partial_kon_sll_del_Caj (GlobalData_t)(0.)
#define partial_kon_sll_del_Casl (GlobalData_t)(0.)
#define partial_kon_sll_del_SLLj (GlobalData_t)(0.)
#define partial_kon_sll_del_SLLsl (GlobalData_t)(0.)
#define partial_kon_sr_del_Cai (GlobalData_t)(0.)
#define partial_kon_sr_del_SRB (GlobalData_t)(0.)
#define partial_kon_tnchca_del_Cai (GlobalData_t)(0.)
#define partial_kon_tnchca_del_TnCHc (GlobalData_t)(0.)
#define partial_kon_tnchmg_del_Cai (GlobalData_t)(0.)
#define partial_kon_tnchmg_del_TnCHm (GlobalData_t)(0.)
#define partial_ks_del_Ca_sr (GlobalData_t)(0.)
#define partial_ks_del_Caj (GlobalData_t)(0.)
#define partial_ksat_del_Caj (GlobalData_t)(0.)
#define partial_ksat_del_Casl (GlobalData_t)(0.)
#define partial_ksat_del_Naj (GlobalData_t)(0.)
#define partial_ksat_del_Nasl (GlobalData_t)(0.)
#define partial_lambda_del_Cai (GlobalData_t)(0.)
#define partial_lambda_m_del_Cai (GlobalData_t)(0.)
#define partial_n_TRPN_del_Cai (GlobalData_t)(0.)
#define partial_n_xb_del_xb (GlobalData_t)(0.)
#define partial_nu_del_Caj (GlobalData_t)(0.)
#define partial_nu_del_Casl (GlobalData_t)(0.)
#define partial_nu_del_Naj (GlobalData_t)(0.)
#define partial_nu_del_Nasl (GlobalData_t)(0.)
#define partial_omega_del_O1 (GlobalData_t)(0.)
#define partial_pCa_del_Caj (GlobalData_t)(0.)
#define partial_pCa_del_Casl (GlobalData_t)(0.)
#define partial_pNa_del_Naj (GlobalData_t)(0.)
#define partial_pNa_del_Nasl (GlobalData_t)(0.)
#define partial_permtot_del_xb (GlobalData_t)(0.)
#define partial_pi_del_Ca_sr (GlobalData_t)(0.)
#define partial_pi_del_Cai (GlobalData_t)(0.)
#define partial_pi_del_Caj (GlobalData_t)(0.)
#define partial_pi_del_Casl (GlobalData_t)(0.)
#define partial_pi_del_Csqnb (GlobalData_t)(0.)
#define partial_pi_del_Naj (GlobalData_t)(0.)
#define partial_pi_del_Nasl (GlobalData_t)(0.)
#define partial_pi_del_SLHj (GlobalData_t)(0.)
#define partial_pi_del_SLHsl (GlobalData_t)(0.)
#define partial_pi_del_SLLj (GlobalData_t)(0.)
#define partial_pi_del_SLLsl (GlobalData_t)(0.)
#define partial_pow_KmNao_3_del_Caj (GlobalData_t)(0.)
#define partial_pow_KmNao_3_del_Casl (GlobalData_t)(0.)
#define partial_pow_KmNao_3_del_Naj (GlobalData_t)(0.)
#define partial_pow_KmNao_3_del_Nasl (GlobalData_t)(0.)
#define partial_pow_KmPCa_1_6_del_Caj (GlobalData_t)(0.)
#define partial_pow_KmPCa_1_6_del_Casl (GlobalData_t)(0.)
#define partial_pow_Q10Cal_Qpow_del_Caj (GlobalData_t)(0.)
#define partial_pow_Q10Cal_Qpow_del_Casl (GlobalData_t)(0.)
#define partial_pow_Q10Cal_Qpow_del_Naj (GlobalData_t)(0.)
#define partial_pow_Q10Cal_Qpow_del_Nasl (GlobalData_t)(0.)
#define partial_pow_Q10NCX_Qpow_del_Caj (GlobalData_t)(0.)
#define partial_pow_Q10NCX_Qpow_del_Casl (GlobalData_t)(0.)
#define partial_pow_Q10NCX_Qpow_del_Naj (GlobalData_t)(0.)
#define partial_pow_Q10NCX_Qpow_del_Nasl (GlobalData_t)(0.)
#define partial_pow_Q10SLCaP_Qpow_del_Caj (GlobalData_t)(0.)
#define partial_pow_Q10SLCaP_Qpow_del_Casl (GlobalData_t)(0.)
#define partial_psi_del_O1 (GlobalData_t)(0.)
#define partial_s1_junc_del_Caj (GlobalData_t)(0.)
#define partial_s1_sl_del_Casl (GlobalData_t)(0.)
#define partial_s2_junc_del_Naj (GlobalData_t)(0.)
#define partial_s2_sl_del_Nasl (GlobalData_t)(0.)
#define partial_sigma_del_Naj (GlobalData_t)(0.)
#define partial_sigma_del_Nasl (GlobalData_t)(0.)
#define partial_teta_del_C15 (GlobalData_t)(0.)
#define partial_teta_del_O1 (GlobalData_t)(0.)
#define pi (GlobalData_t)(3.14159)
#define rtos_init (GlobalData_t)(0.9946)
#define set_C10_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C10_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C10_tozero_in_R (GlobalData_t)(8314.)
#define set_C10_tozero_in_Temp (GlobalData_t)(310.)
#define set_C11_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C11_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C11_tozero_in_R (GlobalData_t)(8314.)
#define set_C11_tozero_in_Temp (GlobalData_t)(310.)
#define set_C12_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C12_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C12_tozero_in_R (GlobalData_t)(8314.)
#define set_C12_tozero_in_Temp (GlobalData_t)(310.)
#define set_C13_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C13_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C13_tozero_in_R (GlobalData_t)(8314.)
#define set_C13_tozero_in_Temp (GlobalData_t)(310.)
#define set_C14_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C14_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C14_tozero_in_R (GlobalData_t)(8314.)
#define set_C14_tozero_in_Temp (GlobalData_t)(310.)
#define set_C15_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C15_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C15_tozero_in_R (GlobalData_t)(8314.)
#define set_C15_tozero_in_Temp (GlobalData_t)(310.)
#define set_C15_tozero_in_teta (GlobalData_t)(6.47e-3)
#define set_C1_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C1_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C1_tozero_in_R (GlobalData_t)(8314.)
#define set_C1_tozero_in_Temp (GlobalData_t)(310.)
#define set_C2_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C2_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C2_tozero_in_R (GlobalData_t)(8314.)
#define set_C2_tozero_in_Temp (GlobalData_t)(310.)
#define set_C3_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C3_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C3_tozero_in_R (GlobalData_t)(8314.)
#define set_C3_tozero_in_Temp (GlobalData_t)(310.)
#define set_C4_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C4_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C4_tozero_in_R (GlobalData_t)(8314.)
#define set_C4_tozero_in_Temp (GlobalData_t)(310.)
#define set_C5_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C5_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C5_tozero_in_R (GlobalData_t)(8314.)
#define set_C5_tozero_in_Temp (GlobalData_t)(310.)
#define set_C6_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C6_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C6_tozero_in_R (GlobalData_t)(8314.)
#define set_C6_tozero_in_Temp (GlobalData_t)(310.)
#define set_C7_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C7_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C7_tozero_in_R (GlobalData_t)(8314.)
#define set_C7_tozero_in_Temp (GlobalData_t)(310.)
#define set_C8_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C8_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C8_tozero_in_R (GlobalData_t)(8314.)
#define set_C8_tozero_in_Temp (GlobalData_t)(310.)
#define set_C9_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_C9_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_C9_tozero_in_R (GlobalData_t)(8314.)
#define set_C9_tozero_in_Temp (GlobalData_t)(310.)
#define set_CaM_tozero_in_Bmax_CaM (GlobalData_t)(24e-3)
#define set_CaM_tozero_in_koff_cam (GlobalData_t)(238e-3)
#define set_CaM_tozero_in_kon_cam (GlobalData_t)(34.)
#define set_Csqnb_tozero_in_Bmax_Csqn (GlobalData_t)(((140e-3*(0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))/(0.035*((((3.14159*10.25)*10.25)*100.)*1e-15))))
#define set_Csqnb_tozero_in_Vcell (GlobalData_t)(((((3.14159*10.25)*10.25)*100.)*1e-15))
#define set_Csqnb_tozero_in_Vmyo (GlobalData_t)((0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))
#define set_Csqnb_tozero_in_Vsr (GlobalData_t)((0.035*((((3.14159*10.25)*10.25)*100.)*1e-15)))
#define set_Csqnb_tozero_in_cellLength (GlobalData_t)(100.)
#define set_Csqnb_tozero_in_cellRadius (GlobalData_t)(10.25)
#define set_Csqnb_tozero_in_koff_csqn (GlobalData_t)(65.)
#define set_Csqnb_tozero_in_kon_csqn (GlobalData_t)(100.)
#define set_Csqnb_tozero_in_pi (GlobalData_t)(3.14159)
#define set_Myoc_tozero_in_Bmax_myosin (GlobalData_t)(140e-3)
#define set_Myoc_tozero_in_koff_myoca (GlobalData_t)(0.46e-3)
#define set_Myoc_tozero_in_kon_myoca (GlobalData_t)(13.8)
#define set_Myom_tozero_in_Bmax_myosin (GlobalData_t)(140e-3)
#define set_Myom_tozero_in_Mgi (GlobalData_t)(1.)
#define set_Myom_tozero_in_koff_myomg (GlobalData_t)(0.057e-3)
#define set_Myom_tozero_in_kon_myomg (GlobalData_t)(0.0157)
#define set_NaBj_tozero_in_Bmax_Naj (GlobalData_t)(7.561)
#define set_NaBj_tozero_in_koff_na (GlobalData_t)(1e-3)
#define set_NaBj_tozero_in_kon_na (GlobalData_t)(0.1e-3)
#define set_NaBsl_tozero_in_Bmax_Nasl (GlobalData_t)(1.65)
#define set_NaBsl_tozero_in_koff_na (GlobalData_t)(1e-3)
#define set_NaBsl_tozero_in_kon_na (GlobalData_t)(0.1e-3)
#define set_O1_tozero_in_FoRT (GlobalData_t)(((96485./8314.)/310.))
#define set_O1_tozero_in_Frdy (GlobalData_t)(96485.)
#define set_O1_tozero_in_R (GlobalData_t)(8314.)
#define set_O1_tozero_in_Temp (GlobalData_t)(310.)
#define set_O1_tozero_in_teta (GlobalData_t)(6.47e-3)
#define set_Q_1_tozero_in_A_1 (GlobalData_t)(-29.)
#define set_Q_1_tozero_in_alpha_1 (GlobalData_t)(0.1)
#define set_Q_2_tozero_in_A_2 (GlobalData_t)(116.)
#define set_Q_2_tozero_in_alpha_2 (GlobalData_t)(0.5)
#define set_RyRi_tozero_in_MaxSR (GlobalData_t)(15.)
#define set_RyRi_tozero_in_MinSR (GlobalData_t)(1.)
#define set_RyRi_tozero_in_ec50SR (GlobalData_t)(0.45)
#define set_RyRi_tozero_in_kiCa (GlobalData_t)(0.5)
#define set_RyRi_tozero_in_kim (GlobalData_t)(0.005)
#define set_RyRi_tozero_in_koCa (GlobalData_t)(10.)
#define set_RyRi_tozero_in_kom (GlobalData_t)(0.06)
#define set_RyRo_tozero_in_MaxSR (GlobalData_t)(15.)
#define set_RyRo_tozero_in_MinSR (GlobalData_t)(1.)
#define set_RyRo_tozero_in_ec50SR (GlobalData_t)(0.45)
#define set_RyRo_tozero_in_kiCa (GlobalData_t)(0.5)
#define set_RyRo_tozero_in_kim (GlobalData_t)(0.005)
#define set_RyRo_tozero_in_koCa (GlobalData_t)(10.)
#define set_RyRo_tozero_in_kom (GlobalData_t)(0.06)
#define set_RyRr_tozero_in_MaxSR (GlobalData_t)(15.)
#define set_RyRr_tozero_in_MinSR (GlobalData_t)(1.)
#define set_RyRr_tozero_in_ec50SR (GlobalData_t)(0.45)
#define set_RyRr_tozero_in_kiCa (GlobalData_t)(0.5)
#define set_RyRr_tozero_in_kim (GlobalData_t)(0.005)
#define set_RyRr_tozero_in_koCa (GlobalData_t)(10.)
#define set_RyRr_tozero_in_kom (GlobalData_t)(0.06)
#define set_SLHj_tozero_in_Bmax_SLhighj (GlobalData_t)((((1.65e-3*(0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))/((0.0539*.01)*((((3.14159*10.25)*10.25)*100.)*1e-15)))*0.1))
#define set_SLHj_tozero_in_Vcell (GlobalData_t)(((((3.14159*10.25)*10.25)*100.)*1e-15))
#define set_SLHj_tozero_in_Vjunc (GlobalData_t)(((0.0539*.01)*((((3.14159*10.25)*10.25)*100.)*1e-15)))
#define set_SLHj_tozero_in_Vmyo (GlobalData_t)((0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))
#define set_SLHj_tozero_in_cellLength (GlobalData_t)(100.)
#define set_SLHj_tozero_in_cellRadius (GlobalData_t)(10.25)
#define set_SLHj_tozero_in_koff_slh (GlobalData_t)(30e-3)
#define set_SLHj_tozero_in_kon_slh (GlobalData_t)(100.)
#define set_SLHj_tozero_in_pi (GlobalData_t)(3.14159)
#define set_SLHsl_tozero_in_Bmax_SLhighsl (GlobalData_t)(((13.4e-3*(0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))/(0.02*((((3.14159*10.25)*10.25)*100.)*1e-15))))
#define set_SLHsl_tozero_in_Vcell (GlobalData_t)(((((3.14159*10.25)*10.25)*100.)*1e-15))
#define set_SLHsl_tozero_in_Vmyo (GlobalData_t)((0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))
#define set_SLHsl_tozero_in_Vsl (GlobalData_t)((0.02*((((3.14159*10.25)*10.25)*100.)*1e-15)))
#define set_SLHsl_tozero_in_cellLength (GlobalData_t)(100.)
#define set_SLHsl_tozero_in_cellRadius (GlobalData_t)(10.25)
#define set_SLHsl_tozero_in_koff_slh (GlobalData_t)(30e-3)
#define set_SLHsl_tozero_in_kon_slh (GlobalData_t)(100.)
#define set_SLHsl_tozero_in_pi (GlobalData_t)(3.14159)
#define set_SLLj_tozero_in_Bmax_SLlowj (GlobalData_t)((((4.6e-3*(0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))/((0.0539*.01)*((((3.14159*10.25)*10.25)*100.)*1e-15)))*0.1))
#define set_SLLj_tozero_in_Vcell (GlobalData_t)(((((3.14159*10.25)*10.25)*100.)*1e-15))
#define set_SLLj_tozero_in_Vjunc (GlobalData_t)(((0.0539*.01)*((((3.14159*10.25)*10.25)*100.)*1e-15)))
#define set_SLLj_tozero_in_Vmyo (GlobalData_t)((0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))
#define set_SLLj_tozero_in_cellLength (GlobalData_t)(100.)
#define set_SLLj_tozero_in_cellRadius (GlobalData_t)(10.25)
#define set_SLLj_tozero_in_koff_sll (GlobalData_t)(1300e-3)
#define set_SLLj_tozero_in_kon_sll (GlobalData_t)(100.)
#define set_SLLj_tozero_in_pi (GlobalData_t)(3.14159)
#define set_SLLsl_tozero_in_Bmax_SLlowsl (GlobalData_t)(((37.4e-3*(0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))/(0.02*((((3.14159*10.25)*10.25)*100.)*1e-15))))
#define set_SLLsl_tozero_in_Vcell (GlobalData_t)(((((3.14159*10.25)*10.25)*100.)*1e-15))
#define set_SLLsl_tozero_in_Vmyo (GlobalData_t)((0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))
#define set_SLLsl_tozero_in_Vsl (GlobalData_t)((0.02*((((3.14159*10.25)*10.25)*100.)*1e-15)))
#define set_SLLsl_tozero_in_cellLength (GlobalData_t)(100.)
#define set_SLLsl_tozero_in_cellRadius (GlobalData_t)(10.25)
#define set_SLLsl_tozero_in_koff_sll (GlobalData_t)(1300e-3)
#define set_SLLsl_tozero_in_kon_sll (GlobalData_t)(100.)
#define set_SLLsl_tozero_in_pi (GlobalData_t)(3.14159)
#define set_SRB_tozero_in_Bmax_SR (GlobalData_t)((19.*.9e-3))
#define set_SRB_tozero_in_koff_sr (GlobalData_t)(60e-3)
#define set_SRB_tozero_in_kon_sr (GlobalData_t)(100.)
#define set_TnCHc_tozero_in_Bmax_TnChigh (GlobalData_t)(140e-3)
#define set_TnCHc_tozero_in_koff_tnchca (GlobalData_t)(0.032e-3)
#define set_TnCHc_tozero_in_kon_tnchca (GlobalData_t)(2.37)
#define set_TnCHm_tozero_in_Bmax_TnChigh (GlobalData_t)(140e-3)
#define set_TnCHm_tozero_in_Mgi (GlobalData_t)(1.)
#define set_TnCHm_tozero_in_koff_tnchmg (GlobalData_t)(3.33e-3)
#define set_TnCHm_tozero_in_kon_tnchmg (GlobalData_t)(3e-3)
#define set_xb_tozero_in_Bmax_TnClow (GlobalData_t)(70e-3)
#define set_xb_tozero_in_TRPN_50 (GlobalData_t)(0.37)
#define set_xb_tozero_in_k_xb (GlobalData_t)(4.9e-3)
#define set_xb_tozero_in_n_xb (GlobalData_t)(3.38)
#define teta (GlobalData_t)(6.47e-3)
#define xb_init (GlobalData_t)(0.00046)
#define xkr_init (GlobalData_t)(8.641386e-3)
#define xks_init (GlobalData_t)(5.412034e-3)
#define xtof_init (GlobalData_t)(4.051574e-3)
#define xtos_init (GlobalData_t)(4.051574e-3)
#define ytof_init (GlobalData_t)(9.945511e-1)
#define ytos_init (GlobalData_t)(9.945511e-1)
#define C6_init (GlobalData_t)((1.-((((((((((((((((C1_init+C2_init)+C3_init)+C4_init)+C5_init)+C7_init)+C8_init)+C9_init)+C10_init)+C11_init)+C12_init)+C13_init)+C14_init)+C15_init)+O1_init)+O2_init))))
#define FoRT (GlobalData_t)(((Frdy/R)/Temp))
#define Fsl (GlobalData_t)((1.-(Fjunc)))
#define Fsl_CaL (GlobalData_t)((1.-(Fjunc_CaL)))
#define Qpow (GlobalData_t)(((Temp-(310.))/10.))
#define SAjunc (GlobalData_t)(((((20150.*pi)*2.)*junctionLength)*junctionRadius))
#define SAsl (GlobalData_t)((((pi*2.)*cellRadius)*cellLength))
#define TRPN_init (GlobalData_t)((TnCL_init/Bmax_TnClow))
#define Vcell (GlobalData_t)(((((pi*cellRadius)*cellRadius)*cellLength)*1e-15))
#define pow_KmNao_3 (GlobalData_t)((pow(KmNao,3.)))
#define pow_KmPCa_1_6 (GlobalData_t)((pow(KmPCa,1.6)))
#define Vjunc (GlobalData_t)(((0.0539*.01)*Vcell))
#define Vmyo (GlobalData_t)((0.65*Vcell))
#define Vsl (GlobalData_t)((0.02*Vcell))
#define Vsr (GlobalData_t)((0.035*Vcell))
#define pow_Q10Cal_Qpow (GlobalData_t)((pow(Q10CaL,Qpow)))
#define pow_Q10NCX_Qpow (GlobalData_t)((pow(Q10NCX,Qpow)))
#define pow_Q10SLCaP_Qpow (GlobalData_t)((pow(Q10SLCaP,Qpow)))
#define Bmax_Csqn (GlobalData_t)(((140e-3*Vmyo)/Vsr))
#define Bmax_SLhighj (GlobalData_t)((((1.65e-3*Vmyo)/Vjunc)*0.1))
#define Bmax_SLhighsl (GlobalData_t)(((13.4e-3*Vmyo)/Vsl))
#define Bmax_SLlowj (GlobalData_t)((((4.6e-3*Vmyo)/Vjunc)*0.1))
#define Bmax_SLlowsl (GlobalData_t)(((37.4e-3*Vmyo)/Vsl))



void initialize_params_Augustin( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Augustin_Params *p = (Augustin_Params *)IF->params;

  // Compute the regional constants
  {
    p->A_1 = -29.;
    p->A_2 = 116.;
    p->Ca_50ref = 0.52;
    p->Cai_init = 8.597401e-2;
    p->Cao = 1.8;
    p->Cli = 15.;
    p->Clo = 150.;
    p->Ki_init = 120.;
    p->Ko = 5.4;
    p->Mgi = 1.;
    p->Nai_init = 9.06;
    p->Nao = 140.;
    p->TRPN_50 = 0.37;
    p->T_ref = 117.1;
    p->a = 0.35;
    p->alpha_1 = 0.1;
    p->alpha_2 = 0.5;
    p->beta_0 = 1.65;
    p->beta_1 = -1.5;
    p->cell_type = EPI;
    if (0) ;
    else if (flag_set(p->flags, "EPI")) p->cell_type = EPI;
    else if (flag_set(p->flags, "MCELL")) p->cell_type = MCELL;
    else if (flag_set(p->flags, "ENDO")) p->cell_type = ENDO;
    p->k_TRPN = 0.14;
    p->k_xb = 4.9e-3;
    p->lengthDep = 0.;
    p->markov_iks = 0.;
    p->n_TRPN = 1.54;
    p->n_xb = 3.38;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Augustin( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Augustin_Params *p = (Augustin_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double Ca_50ref_mM = (p->Ca_50ref*1e-3);
  double GtoFast = ((p->cell_type==EPI) ? (0.13*0.88) : ((0.13*0.3)*0.036));
  double GtoSlow = ((p->cell_type==EPI) ? (0.13*0.12) : ((0.13*0.3)*0.964));
  double ecl = ((1./FoRT)*(log((p->Cli/p->Clo))));
  double gkr = (0.035*(sqrt((p->Ko/5.4))));
  double gks_junc = ((p->markov_iks==0.) ? 0.0035 : 0.0065);
  double gks_sl = ((p->markov_iks==0.) ? 0.0035 : 0.0065);
  double partial_diff_Myom_del_Myom = (((kon_myomg*p->Mgi)*-1.)-(koff_myomg));
  double partial_diff_Q_1_del_Q_1 = (-p->alpha_1);
  double partial_diff_Q_2_del_Q_2 = (-p->alpha_2);
  double partial_diff_TnCHm_del_TnCHm = (((kon_tnchmg*p->Mgi)*-1.)-(koff_tnchmg));
  double sigma = (((exp((p->Nao/67.3)))-(1.))/7.);
  double Myom_rush_larsen_B = (exp((dt*partial_diff_Myom_del_Myom)));
  double Q_1_rush_larsen_B = (exp((dt*partial_diff_Q_1_del_Q_1)));
  double Q_2_rush_larsen_B = (exp((dt*partial_diff_Q_2_del_Q_2)));
  double TnCHm_rush_larsen_B = (exp((dt*partial_diff_TnCHm_del_TnCHm)));

}



void    initialize_sv_Augustin( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Augustin_Params *p = (Augustin_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Augustin_state) );
  Augustin_state *sv_base = (Augustin_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double Ca_50ref_mM = (p->Ca_50ref*1e-3);
  double GtoFast = ((p->cell_type==EPI) ? (0.13*0.88) : ((0.13*0.3)*0.036));
  double GtoSlow = ((p->cell_type==EPI) ? (0.13*0.12) : ((0.13*0.3)*0.964));
  double ecl = ((1./FoRT)*(log((p->Cli/p->Clo))));
  double gkr = (0.035*(sqrt((p->Ko/5.4))));
  double gks_junc = ((p->markov_iks==0.) ? 0.0035 : 0.0065);
  double gks_sl = ((p->markov_iks==0.) ? 0.0035 : 0.0065);
  double partial_diff_Myom_del_Myom = (((kon_myomg*p->Mgi)*-1.)-(koff_myomg));
  double partial_diff_Q_1_del_Q_1 = (-p->alpha_1);
  double partial_diff_Q_2_del_Q_2 = (-p->alpha_2);
  double partial_diff_TnCHm_del_TnCHm = (((kon_tnchmg*p->Mgi)*-1.)-(koff_tnchmg));
  double sigma = (((exp((p->Nao/67.3)))-(1.))/7.);
  double Myom_rush_larsen_B = (exp((dt*partial_diff_Myom_del_Myom)));
  double Q_1_rush_larsen_B = (exp((dt*partial_diff_Q_1_del_Q_1)));
  double Q_2_rush_larsen_B = (exp((dt*partial_diff_Q_2_del_Q_2)));
  double TnCHm_rush_larsen_B = (exp((dt*partial_diff_TnCHm_del_TnCHm)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *V_ext = impdata[Vm];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Augustin_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t V = V_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->C1 = C1_init;
    sv->C10 = C10_init;
    sv->C11 = C11_init;
    sv->C12 = C12_init;
    sv->C13 = C13_init;
    sv->C14 = C14_init;
    sv->C15 = C15_init;
    sv->C2 = C2_init;
    sv->C3 = C3_init;
    sv->C4 = C4_init;
    sv->C5 = C5_init;
    sv->C6 = C6_init;
    sv->C7 = C7_init;
    sv->C8 = C8_init;
    sv->C9 = C9_init;
    sv->CaM = CaM_init;
    sv->Ca_sr = Ca_sr_init;
    sv->Cai = p->Cai_init;
    sv->Caj = Caj_init;
    sv->Casl = Casl_init;
    sv->Csqnb = Csqnb_init;
    sv->Ki = p->Ki_init;
    sv->Myoc = Myoc_init;
    sv->Myom = Myom_init;
    sv->NaBj = NaBj_init;
    sv->NaBsl = NaBsl_init;
    sv->Nai = p->Nai_init;
    sv->Naj = Naj_init;
    sv->Nasl = Nasl_init;
    sv->O1 = O1_init;
    double O2 = O2_init;
    sv->Q_1 = Q_1_init;
    sv->Q_2 = Q_2_init;
    sv->RyRi = RyRi_init;
    sv->RyRo = RyRo_init;
    sv->RyRr = RyRr_init;
    sv->SLHj = SLHj_init;
    sv->SLHsl = SLHsl_init;
    sv->SLLj = SLLj_init;
    sv->SLLsl = SLLsl_init;
    sv->SRB = SRB_init;
    sv->TRPN = TRPN_init;
    sv->TnCHc = TnCHc_init;
    sv->TnCHm = TnCHm_init;
    sv->TnCL = TnCL_init;
    V = V_init;
    sv->d = d_init;
    delta_sl = delta_sl_init;
    sv->f = f_init;
    sv->fcaBj = fcaBj_init;
    sv->fcaBsl = fcaBsl_init;
    sv->h = h_init;
    sv->j = j_init;
    length = length_init;
    sv->m = m_init;
    sv->xb = xb_init;
    sv->xkr = xkr_init;
    sv->xks = xks_init;
    sv->xtof = xtof_init;
    sv->xtos = xtos_init;
    sv->ytof = ytof_init;
    sv->ytos = ytos_init;
    double I_ClCa_junc = (((Fjunc*GClCa)/(1.+(KdClCa/sv->Caj)))*(V-(ecl)));
    double I_ClCa_sl = (((Fsl*GClCa)/(1.+(KdClCa/sv->Casl)))*(V-(ecl)));
    double I_Clbk = (GClB*(V-(ecl)));
    double Ka_junc = (1./(1.+(pow((Kdact/sv->Caj),2.))));
    double Ka_sl = (1./(1.+(pow((Kdact/sv->Casl),2.))));
    double Q = (sv->Q_1+sv->Q_2);
    double eca_junc = (((1./FoRT)/2.)*(log((p->Cao/sv->Caj))));
    double eca_sl = (((1./FoRT)/2.)*(log((p->Cao/sv->Casl))));
    double ek = ((1./FoRT)*(log((p->Ko/sv->Ki))));
    double eks = ((1./FoRT)*(log(((p->Ko+(pNaK*p->Nao))/(sv->Ki+(pNaK*sv->Nai))))));
    double ena_junc = ((1./FoRT)*(log((p->Nao/sv->Naj))));
    double ena_sl = ((1./FoRT)*(log((p->Nao/sv->Nasl))));
    double exp_2VFoRT = (exp(((2.*V)*FoRT)));
    double exp_VFoRT = (exp((V*FoRT)));
    double exp_nu_V_FoRT = (exp(((nu*V)*FoRT)));
    double expm1_2VFoRT = (expm1(((2.*V)*FoRT)));
    double expm1_VFoRT = (expm1((V*FoRT)));
    double expm1_nu_V_FoRT = (exp((((nu-(1.))*V)*FoRT)));
    double fnak = (1./((1.+(0.1245*(exp(((-0.1*V)*FoRT)))))+((0.0365*sigma)*(exp(((-V)*FoRT))))));
    double kp_kp = (1./(1.+(exp((7.488-((V/5.98)))))));
    double lambda = length;
    double pow_Caj_1_6 = (pow(sv->Caj,1.6));
    double pow_Casl_1_6 = (pow(sv->Casl,1.6));
    double pow_KmNaip_Naj_4 = (pow((KmNaip/sv->Naj),4.));
    double pow_KmNaip_Nasl_4 = (pow((KmNaip/sv->Nasl),4.));
    double rkr = (1./(1.+(exp(((V+74.)/24.)))));
    double s3_junc = ((((((((KmCai*p->Nao)*p->Nao)*p->Nao)*(1.+(pow((sv->Naj/KmNai),3.))))+((pow_KmNao_3*sv->Caj)*(1.+(sv->Caj/KmCai))))+(((KmCao*sv->Naj)*sv->Naj)*sv->Naj))+(((sv->Naj*sv->Naj)*sv->Naj)*p->Cao))+(((p->Nao*p->Nao)*p->Nao)*sv->Caj));
    double s3_sl = ((((((((KmCai*p->Nao)*p->Nao)*p->Nao)*(1.+(pow((sv->Nasl/KmNai),3.))))+((pow_KmNao_3*sv->Casl)*(1.+(sv->Casl/KmCai))))+(((KmCao*sv->Nasl)*sv->Nasl)*sv->Nasl))+(((sv->Nasl*sv->Nasl)*sv->Nasl)*p->Cao))+(((p->Nao*p->Nao)*p->Nao)*sv->Casl));
    double ICabk_junc = ((Fjunc*GCaB)*(V-(eca_junc)));
    double ICabk_sl = ((Fsl*GCaB)*(V-(eca_sl)));
    double INaK_junc = (((((Fjunc*IbarNaK)*fnak)*p->Ko)/(1.+pow_KmNaip_Naj_4))/(p->Ko+KmKo));
    double INaK_sl = (((((Fsl*IbarNaK)*fnak)*p->Ko)/(1.+pow_KmNaip_Nasl_4))/(p->Ko+KmKo));
    double INa_junc = (((((((Fjunc*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(ena_junc)));
    double INa_sl = (((((((Fsl*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(ena_sl)));
    double INabk_junc = ((Fjunc*GNaB)*(V-(ena_junc)));
    double INabk_sl = ((Fsl*GNaB)*(V-(ena_sl)));
    double I_ClCa = (I_ClCa_junc+I_ClCa_sl);
    double I_pca_junc = ((((Fjunc*pow_Q10SLCaP_Qpow)*IbarSLCaP)*pow_Caj_1_6)/(pow_KmPCa_1_6+pow_Caj_1_6));
    double I_pca_sl = ((((Fsl*pow_Q10SLCaP_Qpow)*IbarSLCaP)*pow_Casl_1_6)/(pow_KmPCa_1_6+pow_Casl_1_6));
    double VEk = (V-(ek));
    double VEks = (V-(eks));
    double ibarca_j = ((V==0.) ? ((((pCa*4.)*(Frdy*FoRT))*(0.341*sv->Caj))/(2.*FoRT)) : ((((pCa*4.)*((V*Frdy)*FoRT))*(((0.341*sv->Caj)*exp_2VFoRT)-((0.341*p->Cao))))/expm1_2VFoRT));
    double ibarca_sl = ((V==0.) ? ((((pCa*4.)*(Frdy*FoRT))*(0.341*sv->Casl))/(2.*FoRT)) : ((((pCa*4.)*((V*Frdy)*FoRT))*(((0.341*sv->Casl)*exp_2VFoRT)-((0.341*p->Cao))))/expm1_2VFoRT));
    double ibark = ((V==0.) ? (((pK*(Frdy*FoRT))*(0.75*sv->Ki))/FoRT) : (((pK*((V*Frdy)*FoRT))*(((0.75*sv->Ki)*exp_VFoRT)-((0.75*p->Ko))))/expm1_VFoRT));
    double ibarna_j = ((V==0.) ? (((pNa*(Frdy*FoRT))*(0.75*sv->Naj))/FoRT) : (((pNa*((V*Frdy)*FoRT))*(((0.75*sv->Naj)*exp_VFoRT)-((0.75*p->Nao))))/expm1_VFoRT));
    double ibarna_sl = ((V==0.) ? (((pNa*(Frdy*FoRT))*(0.75*sv->Nasl))/FoRT) : (((pNa*((V*Frdy)*FoRT))*(((0.75*sv->Nasl)*exp_VFoRT)-((0.75*p->Nao))))/expm1_VFoRT));
    double lambda_m = ((lambda>1.2) ? 1.2 : lambda);
    double s1_junc = ((((exp_nu_V_FoRT*sv->Naj)*sv->Naj)*sv->Naj)*p->Cao);
    double s1_sl = ((((exp_nu_V_FoRT*sv->Nasl)*sv->Nasl)*sv->Nasl)*p->Cao);
    double s2_junc = ((((expm1_nu_V_FoRT*p->Nao)*p->Nao)*p->Nao)*sv->Caj);
    double s2_sl = ((((expm1_nu_V_FoRT*p->Nao)*p->Nao)*p->Nao)*sv->Casl);
    double ICaK = (((((ibark*sv->d)*sv->f)*((Fjunc_CaL*(fcaCaj+(1.-(sv->fcaBj))))+(Fsl_CaL*(fcaCaMSL+(1.-(sv->fcaBsl))))))*pow_Q10Cal_Qpow)*0.45);
    double ICaNa_junc = ((((((Fjunc_CaL*ibarna_j)*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*pow_Q10Cal_Qpow)*0.45);
    double ICaNa_sl = ((((((Fsl_CaL*ibarna_sl)*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*pow_Q10Cal_Qpow)*.45);
    double ICa_junc = ((((((Fjunc_CaL*ibarca_j)*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*pow_Q10Cal_Qpow)*0.45);
    double ICa_sl = ((((((Fsl_CaL*ibarca_sl)*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*pow_Q10Cal_Qpow)*0.45);
    double IKp_junc = (((Fjunc*gkp)*kp_kp)*VEk);
    double IKp_sl = (((Fsl*gkp)*kp_kp)*VEk);
    double IKr = (((gkr*sv->xkr)*rkr)*VEk);
    double IKs_junc = ((p->markov_iks==0.) ? ((((Fjunc*gks_junc)*sv->xks)*sv->xks)*VEks) : (((Fjunc*gks_junc)*(sv->O1+O2))*VEks));
    double IKs_sl = ((p->markov_iks==0.) ? ((((Fsl*gks_sl)*sv->xks)*sv->xks)*VEks) : (((Fsl*gks_sl)*(sv->O1+O2))*VEks));
    double INCX_junc = ((((((Fjunc*IbarNCX)*pow_Q10NCX_Qpow)*Ka_junc)*(s1_junc-(s2_junc)))/s3_junc)/(1.+(ksat*expm1_nu_V_FoRT)));
    double INCX_sl = ((((((Fsl*IbarNCX)*pow_Q10NCX_Qpow)*Ka_sl)*(s1_sl-(s2_sl)))/s3_sl)/(1.+(ksat*expm1_nu_V_FoRT)));
    double INaK = (INaK_junc+INaK_sl);
    double I_Cl_tot = (I_ClCa+I_Clbk);
    double Itof = (((GtoFast*sv->xtof)*sv->ytof)*VEk);
    double Itos = (((GtoSlow*sv->xtos)*sv->ytos)*VEk);
    double aki = (1.02/(1.+(exp((0.2385*(VEk-(59.215)))))));
    double bki = (((0.49124*(exp((0.08032*(VEk+5.476)))))+(exp((0.06175*(VEk-(594.31))))))/(1.+(exp((-0.5143*(VEk+4.753))))));
    double lambda_s = ((lambda_m<0.87) ? 0.87 : lambda_m);
    double ICa_tot_junc = (((ICa_junc+ICabk_junc)+I_pca_junc)-((2.*INCX_junc)));
    double ICa_tot_sl = (((ICa_sl+ICabk_sl)+I_pca_sl)-((2.*INCX_sl)));
    double IKp = (IKp_junc+IKp_sl);
    double IKs = (IKs_junc+IKs_sl);
    double INa_tot_junc = ((((INa_junc+INabk_junc)+(3.*INCX_junc))+(3.*INaK_junc))+ICaNa_junc);
    double INa_tot_sl = ((((INa_sl+INabk_sl)+(3.*INCX_sl))+(3.*INaK_sl))+ICaNa_sl);
    double Ito = (Itos+Itof);
    double kiss = (aki/(aki+bki));
    double overlap = (1.+(p->beta_0*((lambda_m+lambda_s)-(1.87))));
    double ICa_tot = (ICa_tot_junc+ICa_tot_sl);
    double IKi = (((0.35*(sqrt((p->Ko/5.4))))*kiss)*VEk);
    double INa_tot = (INa_tot_junc+INa_tot_sl);
    double T_0 = ((p->T_ref*sv->xb)*overlap);
    double IK_tot = ((((((Ito+IKr)+IKs)+IKi)-((2.*INaK)))+ICaK)+IKp);
    Tension = ((Q<0.) ? ((T_0*((p->a*Q)+1.))/(1.-(Q))) : ((T_0*(1.+((p->a+2.)*Q)))/(1.+Q)));
    Iion = (((INa_tot+I_Cl_tot)+ICa_tot)+IK_tot);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Tension_ext[__i] = Tension;
    V_ext[__i] = V;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Augustin(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Augustin_Params *p  = (Augustin_Params *)IF->params;
  Augustin_state *sv_base = (Augustin_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Ca_50ref_mM = (p->Ca_50ref*1e-3);
  GlobalData_t GtoFast = ((p->cell_type==EPI) ? (0.13*0.88) : ((0.13*0.3)*0.036));
  GlobalData_t GtoSlow = ((p->cell_type==EPI) ? (0.13*0.12) : ((0.13*0.3)*0.964));
  GlobalData_t ecl = ((1./FoRT)*(log((p->Cli/p->Clo))));
  GlobalData_t gkr = (0.035*(sqrt((p->Ko/5.4))));
  GlobalData_t gks_junc = ((p->markov_iks==0.) ? 0.0035 : 0.0065);
  GlobalData_t gks_sl = ((p->markov_iks==0.) ? 0.0035 : 0.0065);
  GlobalData_t partial_diff_Myom_del_Myom = (((kon_myomg*p->Mgi)*-1.)-(koff_myomg));
  GlobalData_t partial_diff_Q_1_del_Q_1 = (-p->alpha_1);
  GlobalData_t partial_diff_Q_2_del_Q_2 = (-p->alpha_2);
  GlobalData_t partial_diff_TnCHm_del_TnCHm = (((kon_tnchmg*p->Mgi)*-1.)-(koff_tnchmg));
  GlobalData_t sigma = (((exp((p->Nao/67.3)))-(1.))/7.);
  GlobalData_t Myom_rush_larsen_B = (exp((dt*partial_diff_Myom_del_Myom)));
  GlobalData_t Q_1_rush_larsen_B = (exp((dt*partial_diff_Q_1_del_Q_1)));
  GlobalData_t Q_2_rush_larsen_B = (exp((dt*partial_diff_Q_2_del_Q_2)));
  GlobalData_t TnCHm_rush_larsen_B = (exp((dt*partial_diff_TnCHm_del_TnCHm)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *V_ext = impdata[Vm];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Augustin_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t V = V_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t I_ClCa_junc = (((Fjunc*GClCa)/(1.+(KdClCa/sv->Caj)))*(V-(ecl)));
    GlobalData_t I_ClCa_sl = (((Fsl*GClCa)/(1.+(KdClCa/sv->Casl)))*(V-(ecl)));
    GlobalData_t I_Clbk = (GClB*(V-(ecl)));
    GlobalData_t Ka_junc = (1./(1.+(pow((Kdact/sv->Caj),2.))));
    GlobalData_t Ka_sl = (1./(1.+(pow((Kdact/sv->Casl),2.))));
    GlobalData_t O2 = (1.-((((((((((((((((sv->C1+sv->C2)+sv->C3)+sv->C4)+sv->C5)+sv->C6)+sv->C7)+sv->C8)+sv->C9)+sv->C10)+sv->C11)+sv->C12)+sv->C13)+sv->C14)+sv->C15)+sv->O1)));
    GlobalData_t Q = (sv->Q_1+sv->Q_2);
    GlobalData_t eca_junc = (((1./FoRT)/2.)*(log((p->Cao/sv->Caj))));
    GlobalData_t eca_sl = (((1./FoRT)/2.)*(log((p->Cao/sv->Casl))));
    GlobalData_t ek = ((1./FoRT)*(log((p->Ko/sv->Ki))));
    GlobalData_t eks = ((1./FoRT)*(log(((p->Ko+(pNaK*p->Nao))/(sv->Ki+(pNaK*sv->Nai))))));
    GlobalData_t ena_junc = ((1./FoRT)*(log((p->Nao/sv->Naj))));
    GlobalData_t ena_sl = ((1./FoRT)*(log((p->Nao/sv->Nasl))));
    GlobalData_t exp_2VFoRT = (exp(((2.*V)*FoRT)));
    GlobalData_t exp_VFoRT = (exp((V*FoRT)));
    GlobalData_t exp_nu_V_FoRT = (exp(((nu*V)*FoRT)));
    GlobalData_t expm1_2VFoRT = (expm1(((2.*V)*FoRT)));
    GlobalData_t expm1_VFoRT = (expm1((V*FoRT)));
    GlobalData_t expm1_nu_V_FoRT = (exp((((nu-(1.))*V)*FoRT)));
    GlobalData_t fnak = (1./((1.+(0.1245*(exp(((-0.1*V)*FoRT)))))+((0.0365*sigma)*(exp(((-V)*FoRT))))));
    GlobalData_t kp_kp = (1./(1.+(exp((7.488-((V/5.98)))))));
    GlobalData_t lambda = length;
    GlobalData_t pow_Caj_1_6 = (pow(sv->Caj,1.6));
    GlobalData_t pow_Casl_1_6 = (pow(sv->Casl,1.6));
    GlobalData_t pow_KmNaip_Naj_4 = (pow((KmNaip/sv->Naj),4.));
    GlobalData_t pow_KmNaip_Nasl_4 = (pow((KmNaip/sv->Nasl),4.));
    GlobalData_t rkr = (1./(1.+(exp(((V+74.)/24.)))));
    GlobalData_t s3_junc = ((((((((KmCai*p->Nao)*p->Nao)*p->Nao)*(1.+(pow((sv->Naj/KmNai),3.))))+((pow_KmNao_3*sv->Caj)*(1.+(sv->Caj/KmCai))))+(((KmCao*sv->Naj)*sv->Naj)*sv->Naj))+(((sv->Naj*sv->Naj)*sv->Naj)*p->Cao))+(((p->Nao*p->Nao)*p->Nao)*sv->Caj));
    GlobalData_t s3_sl = ((((((((KmCai*p->Nao)*p->Nao)*p->Nao)*(1.+(pow((sv->Nasl/KmNai),3.))))+((pow_KmNao_3*sv->Casl)*(1.+(sv->Casl/KmCai))))+(((KmCao*sv->Nasl)*sv->Nasl)*sv->Nasl))+(((sv->Nasl*sv->Nasl)*sv->Nasl)*p->Cao))+(((p->Nao*p->Nao)*p->Nao)*sv->Casl));
    GlobalData_t ICabk_junc = ((Fjunc*GCaB)*(V-(eca_junc)));
    GlobalData_t ICabk_sl = ((Fsl*GCaB)*(V-(eca_sl)));
    GlobalData_t INaK_junc = (((((Fjunc*IbarNaK)*fnak)*p->Ko)/(1.+pow_KmNaip_Naj_4))/(p->Ko+KmKo));
    GlobalData_t INaK_sl = (((((Fsl*IbarNaK)*fnak)*p->Ko)/(1.+pow_KmNaip_Nasl_4))/(p->Ko+KmKo));
    GlobalData_t INa_junc = (((((((Fjunc*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(ena_junc)));
    GlobalData_t INa_sl = (((((((Fsl*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(ena_sl)));
    GlobalData_t INabk_junc = ((Fjunc*GNaB)*(V-(ena_junc)));
    GlobalData_t INabk_sl = ((Fsl*GNaB)*(V-(ena_sl)));
    GlobalData_t I_ClCa = (I_ClCa_junc+I_ClCa_sl);
    GlobalData_t I_pca_junc = ((((Fjunc*pow_Q10SLCaP_Qpow)*IbarSLCaP)*pow_Caj_1_6)/(pow_KmPCa_1_6+pow_Caj_1_6));
    GlobalData_t I_pca_sl = ((((Fsl*pow_Q10SLCaP_Qpow)*IbarSLCaP)*pow_Casl_1_6)/(pow_KmPCa_1_6+pow_Casl_1_6));
    GlobalData_t VEk = (V-(ek));
    GlobalData_t VEks = (V-(eks));
    GlobalData_t ibarca_j = ((V==0.) ? ((((pCa*4.)*(Frdy*FoRT))*(0.341*sv->Caj))/(2.*FoRT)) : ((((pCa*4.)*((V*Frdy)*FoRT))*(((0.341*sv->Caj)*exp_2VFoRT)-((0.341*p->Cao))))/expm1_2VFoRT));
    GlobalData_t ibarca_sl = ((V==0.) ? ((((pCa*4.)*(Frdy*FoRT))*(0.341*sv->Casl))/(2.*FoRT)) : ((((pCa*4.)*((V*Frdy)*FoRT))*(((0.341*sv->Casl)*exp_2VFoRT)-((0.341*p->Cao))))/expm1_2VFoRT));
    GlobalData_t ibark = ((V==0.) ? (((pK*(Frdy*FoRT))*(0.75*sv->Ki))/FoRT) : (((pK*((V*Frdy)*FoRT))*(((0.75*sv->Ki)*exp_VFoRT)-((0.75*p->Ko))))/expm1_VFoRT));
    GlobalData_t ibarna_j = ((V==0.) ? (((pNa*(Frdy*FoRT))*(0.75*sv->Naj))/FoRT) : (((pNa*((V*Frdy)*FoRT))*(((0.75*sv->Naj)*exp_VFoRT)-((0.75*p->Nao))))/expm1_VFoRT));
    GlobalData_t ibarna_sl = ((V==0.) ? (((pNa*(Frdy*FoRT))*(0.75*sv->Nasl))/FoRT) : (((pNa*((V*Frdy)*FoRT))*(((0.75*sv->Nasl)*exp_VFoRT)-((0.75*p->Nao))))/expm1_VFoRT));
    GlobalData_t lambda_m = ((lambda>1.2) ? 1.2 : lambda);
    GlobalData_t s1_junc = ((((exp_nu_V_FoRT*sv->Naj)*sv->Naj)*sv->Naj)*p->Cao);
    GlobalData_t s1_sl = ((((exp_nu_V_FoRT*sv->Nasl)*sv->Nasl)*sv->Nasl)*p->Cao);
    GlobalData_t s2_junc = ((((expm1_nu_V_FoRT*p->Nao)*p->Nao)*p->Nao)*sv->Caj);
    GlobalData_t s2_sl = ((((expm1_nu_V_FoRT*p->Nao)*p->Nao)*p->Nao)*sv->Casl);
    GlobalData_t ICaK = (((((ibark*sv->d)*sv->f)*((Fjunc_CaL*(fcaCaj+(1.-(sv->fcaBj))))+(Fsl_CaL*(fcaCaMSL+(1.-(sv->fcaBsl))))))*pow_Q10Cal_Qpow)*0.45);
    GlobalData_t ICaNa_junc = ((((((Fjunc_CaL*ibarna_j)*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*pow_Q10Cal_Qpow)*0.45);
    GlobalData_t ICaNa_sl = ((((((Fsl_CaL*ibarna_sl)*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*pow_Q10Cal_Qpow)*.45);
    GlobalData_t ICa_junc = ((((((Fjunc_CaL*ibarca_j)*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*pow_Q10Cal_Qpow)*0.45);
    GlobalData_t ICa_sl = ((((((Fsl_CaL*ibarca_sl)*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*pow_Q10Cal_Qpow)*0.45);
    GlobalData_t IKp_junc = (((Fjunc*gkp)*kp_kp)*VEk);
    GlobalData_t IKp_sl = (((Fsl*gkp)*kp_kp)*VEk);
    GlobalData_t IKr = (((gkr*sv->xkr)*rkr)*VEk);
    GlobalData_t IKs_junc = ((p->markov_iks==0.) ? ((((Fjunc*gks_junc)*sv->xks)*sv->xks)*VEks) : (((Fjunc*gks_junc)*(sv->O1+O2))*VEks));
    GlobalData_t IKs_sl = ((p->markov_iks==0.) ? ((((Fsl*gks_sl)*sv->xks)*sv->xks)*VEks) : (((Fsl*gks_sl)*(sv->O1+O2))*VEks));
    GlobalData_t INCX_junc = ((((((Fjunc*IbarNCX)*pow_Q10NCX_Qpow)*Ka_junc)*(s1_junc-(s2_junc)))/s3_junc)/(1.+(ksat*expm1_nu_V_FoRT)));
    GlobalData_t INCX_sl = ((((((Fsl*IbarNCX)*pow_Q10NCX_Qpow)*Ka_sl)*(s1_sl-(s2_sl)))/s3_sl)/(1.+(ksat*expm1_nu_V_FoRT)));
    GlobalData_t INaK = (INaK_junc+INaK_sl);
    GlobalData_t I_Cl_tot = (I_ClCa+I_Clbk);
    GlobalData_t Itof = (((GtoFast*sv->xtof)*sv->ytof)*VEk);
    GlobalData_t Itos = (((GtoSlow*sv->xtos)*sv->ytos)*VEk);
    GlobalData_t aki = (1.02/(1.+(exp((0.2385*(VEk-(59.215)))))));
    GlobalData_t bki = (((0.49124*(exp((0.08032*(VEk+5.476)))))+(exp((0.06175*(VEk-(594.31))))))/(1.+(exp((-0.5143*(VEk+4.753))))));
    GlobalData_t lambda_s = ((lambda_m<0.87) ? 0.87 : lambda_m);
    GlobalData_t ICa_tot_junc = (((ICa_junc+ICabk_junc)+I_pca_junc)-((2.*INCX_junc)));
    GlobalData_t ICa_tot_sl = (((ICa_sl+ICabk_sl)+I_pca_sl)-((2.*INCX_sl)));
    GlobalData_t IKp = (IKp_junc+IKp_sl);
    GlobalData_t IKs = (IKs_junc+IKs_sl);
    GlobalData_t INa_tot_junc = ((((INa_junc+INabk_junc)+(3.*INCX_junc))+(3.*INaK_junc))+ICaNa_junc);
    GlobalData_t INa_tot_sl = ((((INa_sl+INabk_sl)+(3.*INCX_sl))+(3.*INaK_sl))+ICaNa_sl);
    GlobalData_t Ito = (Itos+Itof);
    GlobalData_t kiss = (aki/(aki+bki));
    GlobalData_t overlap = (1.+(p->beta_0*((lambda_m+lambda_s)-(1.87))));
    GlobalData_t ICa_tot = (ICa_tot_junc+ICa_tot_sl);
    GlobalData_t IKi = (((0.35*(sqrt((p->Ko/5.4))))*kiss)*VEk);
    GlobalData_t INa_tot = (INa_tot_junc+INa_tot_sl);
    GlobalData_t T_0 = ((p->T_ref*sv->xb)*overlap);
    GlobalData_t IK_tot = ((((((Ito+IKr)+IKs)+IKi)-((2.*INaK)))+ICaK)+IKp);
    Tension = ((Q<0.) ? ((T_0*((p->a*Q)+1.))/(1.-(Q))) : ((T_0*(1.+((p->a+2.)*Q)))/(1.+Q)));
    Iion = (((INa_tot+I_Cl_tot)+ICa_tot)+IK_tot);
    
    
    //Complete Forward Euler Update
    GlobalData_t Ca_50 = (p->Ca_50ref*(1.+(p->beta_1*(lambda_m-(1.)))));
    GlobalData_t Ca_50_mM = ((p->lengthDep>0.) ? (Ca_50ref_mM*(1.+(p->beta_1*(lambda_m-(1.))))) : Ca_50ref_mM);
    GlobalData_t Cai_mM = (sv->Cai/1000.);
    GlobalData_t diff_Ki = (((-IK_tot)*Cm)/(Vmyo*Frdy));
    GlobalData_t diff_Nai = ((J_na_slmyo/Vmyo)*(sv->Nasl-(sv->Nai)));
    GlobalData_t diff_TRPN = (((p->k_TRPN*(pow((sv->Cai/Ca_50),p->n_TRPN)))*(1.-((sv->TnCL/Bmax_TnClow))))-(((p->k_TRPN*sv->TnCL)/Bmax_TnClow)));
    GlobalData_t diff_TnCL = (((p->k_TRPN*(pow((Cai_mM/Ca_50_mM),p->n_TRPN)))*(Bmax_TnClow-(sv->TnCL)))-((p->k_TRPN*sv->TnCL)));
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t TRPN_new = sv->TRPN+diff_TRPN*dt;
    GlobalData_t TnCL_new = sv->TnCL+diff_TnCL*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t J_SRCarel = ((ks*sv->RyRo)*(sv->Ca_sr-(sv->Caj)));
    GlobalData_t J_SRleak = (5.348e-6*(sv->Ca_sr-(sv->Caj)));
    GlobalData_t J_serca = ((((pow(Q10SRCaP,Qpow))*Vmax_SRCaP)*((pow((Cai_mM/Kmf),hillSRCaP))-((pow((sv->Ca_sr/Kmr),hillSRCaP)))))/((1.+(pow((Cai_mM/Kmf),hillSRCaP)))+(pow((sv->Ca_sr/Kmr),hillSRCaP))));
    GlobalData_t ah = ((V>=-40.) ? 0. : (0.057*(exp(((-(V+80.))/6.8)))));
    GlobalData_t aj = ((V>=-40.) ? 0. : (((((-2.5428*10e4)*(exp((0.2444*V))))-((6.948e-6*(exp((-0.04391*V))))))*(V+37.78))/(1.+(exp((0.311*(V+79.23)))))));
    GlobalData_t alpha = (3.98e-4*(exp(((3.61e-1*V)*FoRT))));
    GlobalData_t beta = (5.74e-5*(exp(((-9.23e-2*V)*FoRT))));
    GlobalData_t bh = ((V>=-40.) ? (0.77/(0.13*(1.+(exp(((-(V+10.66))/11.1)))))) : ((2.7*(exp((0.079*V))))+(3.1e5*(exp((0.3485*V))))));
    GlobalData_t bj = ((V>=-40.) ? ((0.6*(exp((0.057*V))))/(1.+(exp((-0.1*(V+32.)))))) : ((0.02424*(exp((-0.01052*V))))/(1.+(exp((-0.1378*(V+40.14)))))));
    GlobalData_t d_infinity = (1./(1.+(exp(((-(V+5.))/6.0)))));
    GlobalData_t delta = (1.2e-3*(exp(((-3.3e-1*V)*FoRT))));
    GlobalData_t diff_CaM = (((kon_cam*Cai_mM)*(Bmax_CaM-(sv->CaM)))-((koff_cam*sv->CaM)));
    GlobalData_t diff_Csqnb = (((kon_csqn*sv->Ca_sr)*(Bmax_Csqn-(sv->Csqnb)))-((koff_csqn*sv->Csqnb)));
    GlobalData_t diff_Myoc = (((kon_myoca*Cai_mM)*((Bmax_myosin-(sv->Myoc))-(sv->Myom)))-((koff_myoca*sv->Myoc)));
    GlobalData_t diff_Myom = (((kon_myomg*p->Mgi)*((Bmax_myosin-(sv->Myoc))-(sv->Myom)))-((koff_myomg*sv->Myom)));
    GlobalData_t diff_NaBj = (((kon_na*sv->Naj)*(Bmax_Naj-(sv->NaBj)))-((koff_na*sv->NaBj)));
    GlobalData_t diff_NaBsl = (((kon_na*sv->Nasl)*(Bmax_Nasl-(sv->NaBsl)))-((koff_na*sv->NaBsl)));
    GlobalData_t diff_SLHj = (((kon_slh*sv->Caj)*(Bmax_SLhighj-(sv->SLHj)))-((koff_slh*sv->SLHj)));
    GlobalData_t diff_SLHsl = (((kon_slh*sv->Casl)*(Bmax_SLhighsl-(sv->SLHsl)))-((koff_slh*sv->SLHsl)));
    GlobalData_t diff_SLLj = (((kon_sll*sv->Caj)*(Bmax_SLlowj-(sv->SLLj)))-((koff_sll*sv->SLLj)));
    GlobalData_t diff_SLLsl = (((kon_sll*sv->Casl)*(Bmax_SLlowsl-(sv->SLLsl)))-((koff_sll*sv->SLLsl)));
    GlobalData_t diff_SRB = (((kon_sr*Cai_mM)*(Bmax_SR-(sv->SRB)))-((koff_sr*sv->SRB)));
    GlobalData_t diff_TnCHc = (((kon_tnchca*Cai_mM)*((Bmax_TnChigh-(sv->TnCHc))-(sv->TnCHm)))-((koff_tnchca*sv->TnCHc)));
    GlobalData_t diff_TnCHm = (((kon_tnchmg*p->Mgi)*((Bmax_TnChigh-(sv->TnCHc))-(sv->TnCHm)))-((koff_tnchmg*sv->TnCHm)));
    GlobalData_t eta = (1.25e-2*(exp(((-4.81e-1*V)*FoRT))));
    GlobalData_t f_infinity = ((1./(1.+(exp(((V+35.)/9.)))))+(0.6/(1.+(exp(((50.-(V))/20.))))));
    GlobalData_t gamma = (3.41e-3*(exp(((8.68e-1*V)*FoRT))));
    GlobalData_t hss_factor = (1.+(exp(((V+71.55)/7.43))));
    GlobalData_t jss_factor = (1.+(exp(((V+71.55)/7.43))));
    GlobalData_t kCaSR = (MaxSR-(((MaxSR-(MinSR))/(1.+(pow((ec50SR/sv->Ca_sr),2.5))))));
    GlobalData_t mss_factor = (1.+(exp(((-(56.86+V))/9.03))));
    GlobalData_t omega = (4.91e-3*(exp(((-6.79e-1*V)*FoRT))));
    GlobalData_t partial_diff_CaM_del_CaM = (((kon_cam*Cai_mM)*-1.)-(koff_cam));
    GlobalData_t partial_diff_Ca_sr_del_Ca_sr = (((((((1.+(pow((Cai_mM/Kmf),hillSRCaP)))+(pow((sv->Ca_sr/Kmr),hillSRCaP)))*(((pow(Q10SRCaP,Qpow))*Vmax_SRCaP)*(-(hillSRCaP*((Kmr/(Kmr*Kmr))*(pow((sv->Ca_sr/Kmr),(hillSRCaP-(1.)))))))))-(((((pow(Q10SRCaP,Qpow))*Vmax_SRCaP)*((pow((Cai_mM/Kmf),hillSRCaP))-((pow((sv->Ca_sr/Kmr),hillSRCaP)))))*(hillSRCaP*((Kmr/(Kmr*Kmr))*(pow((sv->Ca_sr/Kmr),(hillSRCaP-(1.)))))))))/(((1.+(pow((Cai_mM/Kmf),hillSRCaP)))+(pow((sv->Ca_sr/Kmr),hillSRCaP)))*((1.+(pow((Cai_mM/Kmf),hillSRCaP)))+(pow((sv->Ca_sr/Kmr),hillSRCaP)))))-((((Vsr*(5.348e-6*Vmyo))/(Vsr*Vsr))+(ks*sv->RyRo))))-((kon_csqn*(Bmax_Csqn-(sv->Csqnb)))));
    GlobalData_t partial_diff_Cai_del_Cai = (((((Vmyo*((-(((((1.+(pow((Cai_mM/Kmf),hillSRCaP)))+(pow((sv->Ca_sr/Kmr),hillSRCaP)))*(((pow(Q10SRCaP,Qpow))*Vmax_SRCaP)*(hillSRCaP*(((Kmf*(1000./(1000.*1000.)))/(Kmf*Kmf))*(pow((Cai_mM/Kmf),(hillSRCaP-(1.))))))))-(((((pow(Q10SRCaP,Qpow))*Vmax_SRCaP)*((pow((Cai_mM/Kmf),hillSRCaP))-((pow((sv->Ca_sr/Kmr),hillSRCaP)))))*(hillSRCaP*(((Kmf*(1000./(1000.*1000.)))/(Kmf*Kmf))*(pow((Cai_mM/Kmf),(hillSRCaP-(1.)))))))))/(((1.+(pow((Cai_mM/Kmf),hillSRCaP)))+(pow((sv->Ca_sr/Kmr),hillSRCaP)))*((1.+(pow((Cai_mM/Kmf),hillSRCaP)))+(pow((sv->Ca_sr/Kmr),hillSRCaP))))))*Vsr))/(Vmyo*Vmyo))-(((((((p->k_TRPN*(p->n_TRPN*(((Ca_50_mM*(1000./(1000.*1000.)))/(Ca_50_mM*Ca_50_mM))*(pow((Cai_mM/Ca_50_mM),(p->n_TRPN-(1.)))))))*(Bmax_TnClow-(sv->TnCL)))+((kon_tnchca*(1000./(1000.*1000.)))*((Bmax_TnChigh-(sv->TnCHc))-(sv->TnCHm))))+((kon_cam*(1000./(1000.*1000.)))*(Bmax_CaM-(sv->CaM))))+((kon_myoca*(1000./(1000.*1000.)))*((Bmax_myosin-(sv->Myoc))-(sv->Myom))))+((kon_sr*(1000./(1000.*1000.)))*(Bmax_SR-(sv->SRB))))))+((J_ca_slmyo/Vmyo)*(-(1000./(1000.*1000.)))))*1.e3);
    GlobalData_t partial_diff_Caj_del_Caj = ((((((((Vjunc*2.)*Frdy)*((-(((((((((Fjunc_CaL*((V==0.) ? (((2.*FoRT)*(((pCa*4.)*(Frdy*FoRT))*0.341))/((2.*FoRT)*(2.*FoRT))) : ((expm1_2VFoRT*(((pCa*4.)*((V*Frdy)*FoRT))*(0.341*exp_2VFoRT)))/(expm1_2VFoRT*expm1_2VFoRT))))*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*pow_Q10Cal_Qpow)*0.45)+((Fjunc*GCaB)*(-(((1./FoRT)/2.)*(((-p->Cao)/(sv->Caj*sv->Caj))/(p->Cao/sv->Caj))))))+((((pow_KmPCa_1_6+pow_Caj_1_6)*(((Fjunc*pow_Q10SLCaP_Qpow)*IbarSLCaP)*(1.6*(pow(sv->Caj,0.6000000000000001)))))-(((((Fjunc*pow_Q10SLCaP_Qpow)*IbarSLCaP)*pow_Caj_1_6)*(1.6*(pow(sv->Caj,0.6000000000000001))))))/((pow_KmPCa_1_6+pow_Caj_1_6)*(pow_KmPCa_1_6+pow_Caj_1_6))))-((2.*(((1.+(ksat*expm1_nu_V_FoRT))*(((s3_junc*(((((Fjunc*IbarNCX)*pow_Q10NCX_Qpow)*((-(2.*(((-Kdact)/(sv->Caj*sv->Caj))*(pow((Kdact/sv->Caj),1.0)))))/((1.+(pow((Kdact/sv->Caj),2.)))*(1.+(pow((Kdact/sv->Caj),2.))))))*(s1_junc-(s2_junc)))+((((Fjunc*IbarNCX)*pow_Q10NCX_Qpow)*Ka_junc)*(-(((expm1_nu_V_FoRT*p->Nao)*p->Nao)*p->Nao)))))-((((((Fjunc*IbarNCX)*pow_Q10NCX_Qpow)*Ka_junc)*(s1_junc-(s2_junc)))*(((pow_KmNao_3*(1.+(sv->Caj/KmCai)))+((pow_KmNao_3*sv->Caj)*(KmCai/(KmCai*KmCai))))+((p->Nao*p->Nao)*p->Nao)))))/(s3_junc*s3_junc)))/((1.+(ksat*expm1_nu_V_FoRT))*(1.+(ksat*expm1_nu_V_FoRT))))))))*Cm))/(((Vjunc*2.)*Frdy)*((Vjunc*2.)*Frdy)))+((J_ca_juncsl/Vjunc)*-1.))-(((kon_sll*(Bmax_SLlowj-(sv->SLLj)))+(kon_slh*(Bmax_SLhighj-(sv->SLHj))))))+((Vjunc*(((ks*sv->RyRo)*-1.)*Vsr))/(Vjunc*Vjunc)))+((Vjunc*((5.348e-6*-1.)*Vmyo))/(Vjunc*Vjunc)));
    GlobalData_t partial_diff_Casl_del_Casl = (((((((Vsl*2.)*Frdy)*((-(((((((((Fsl_CaL*((V==0.) ? (((2.*FoRT)*(((pCa*4.)*(Frdy*FoRT))*0.341))/((2.*FoRT)*(2.*FoRT))) : ((expm1_2VFoRT*(((pCa*4.)*((V*Frdy)*FoRT))*(0.341*exp_2VFoRT)))/(expm1_2VFoRT*expm1_2VFoRT))))*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*pow_Q10Cal_Qpow)*0.45)+((Fsl*GCaB)*(-(((1./FoRT)/2.)*(((-p->Cao)/(sv->Casl*sv->Casl))/(p->Cao/sv->Casl))))))+((((pow_KmPCa_1_6+pow_Casl_1_6)*(((Fsl*pow_Q10SLCaP_Qpow)*IbarSLCaP)*(1.6*(pow(sv->Casl,0.6000000000000001)))))-(((((Fsl*pow_Q10SLCaP_Qpow)*IbarSLCaP)*pow_Casl_1_6)*(1.6*(pow(sv->Casl,0.6000000000000001))))))/((pow_KmPCa_1_6+pow_Casl_1_6)*(pow_KmPCa_1_6+pow_Casl_1_6))))-((2.*(((1.+(ksat*expm1_nu_V_FoRT))*(((s3_sl*(((((Fsl*IbarNCX)*pow_Q10NCX_Qpow)*((-(2.*(((-Kdact)/(sv->Casl*sv->Casl))*(pow((Kdact/sv->Casl),1.0)))))/((1.+(pow((Kdact/sv->Casl),2.)))*(1.+(pow((Kdact/sv->Casl),2.))))))*(s1_sl-(s2_sl)))+((((Fsl*IbarNCX)*pow_Q10NCX_Qpow)*Ka_sl)*(-(((expm1_nu_V_FoRT*p->Nao)*p->Nao)*p->Nao)))))-((((((Fsl*IbarNCX)*pow_Q10NCX_Qpow)*Ka_sl)*(s1_sl-(s2_sl)))*(((pow_KmNao_3*(1.+(sv->Casl/KmCai)))+((pow_KmNao_3*sv->Casl)*(KmCai/(KmCai*KmCai))))+((p->Nao*p->Nao)*p->Nao)))))/(s3_sl*s3_sl)))/((1.+(ksat*expm1_nu_V_FoRT))*(1.+(ksat*expm1_nu_V_FoRT))))))))*Cm))/(((Vsl*2.)*Frdy)*((Vsl*2.)*Frdy)))+((J_ca_juncsl/Vsl)*-1.))+((J_ca_slmyo/Vsl)*-1.))-(((kon_sll*(Bmax_SLlowsl-(sv->SLLsl)))+(kon_slh*(Bmax_SLhighsl-(sv->SLHsl))))));
    GlobalData_t partial_diff_Csqnb_del_Csqnb = (((kon_csqn*sv->Ca_sr)*-1.)-(koff_csqn));
    GlobalData_t partial_diff_Myoc_del_Myoc = (((kon_myoca*Cai_mM)*-1.)-(koff_myoca));
    GlobalData_t partial_diff_NaBj_del_NaBj = (((kon_na*sv->Naj)*-1.)-(koff_na));
    GlobalData_t partial_diff_NaBsl_del_NaBsl = (((kon_na*sv->Nasl)*-1.)-(koff_na));
    GlobalData_t partial_diff_Naj_del_Naj = (((((Vjunc*Frdy)*((-(((((((((((Fjunc*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(-((1./FoRT)*(((-p->Nao)/(sv->Naj*sv->Naj))/(p->Nao/sv->Naj)))))+((Fjunc*GNaB)*(-((1./FoRT)*(((-p->Nao)/(sv->Naj*sv->Naj))/(p->Nao/sv->Naj))))))+(3.*(((1.+(ksat*expm1_nu_V_FoRT))*(((s3_junc*((((Fjunc*IbarNCX)*pow_Q10NCX_Qpow)*Ka_junc)*(((((exp_nu_V_FoRT*sv->Naj)+(exp_nu_V_FoRT*sv->Naj))*sv->Naj)+((exp_nu_V_FoRT*sv->Naj)*sv->Naj))*p->Cao)))-((((((Fjunc*IbarNCX)*pow_Q10NCX_Qpow)*Ka_junc)*(s1_junc-(s2_junc)))*((((((KmCai*p->Nao)*p->Nao)*p->Nao)*(3.*((KmNai/(KmNai*KmNai))*(pow((sv->Naj/KmNai),2.0)))))+((((KmCao*sv->Naj)+(KmCao*sv->Naj))*sv->Naj)+((KmCao*sv->Naj)*sv->Naj)))+((((sv->Naj+sv->Naj)*sv->Naj)+(sv->Naj*sv->Naj))*p->Cao)))))/(s3_junc*s3_junc)))/((1.+(ksat*expm1_nu_V_FoRT))*(1.+(ksat*expm1_nu_V_FoRT))))))+(3.*(((p->Ko+KmKo)*((-((((Fjunc*IbarNaK)*fnak)*p->Ko)*(4.*(((-KmNaip)/(sv->Naj*sv->Naj))*(pow((KmNaip/sv->Naj),3.0))))))/((1.+pow_KmNaip_Naj_4)*(1.+pow_KmNaip_Naj_4))))/((p->Ko+KmKo)*(p->Ko+KmKo)))))+((((((Fjunc_CaL*((V==0.) ? ((FoRT*((pNa*(Frdy*FoRT))*0.75))/(FoRT*FoRT)) : ((expm1_VFoRT*((pNa*((V*Frdy)*FoRT))*(0.75*exp_VFoRT)))/(expm1_VFoRT*expm1_VFoRT))))*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*pow_Q10Cal_Qpow)*0.45)))*Cm))/((Vjunc*Frdy)*(Vjunc*Frdy)))+((J_na_juncsl/Vjunc)*-1.))-((kon_na*(Bmax_Naj-(sv->NaBj)))));
    GlobalData_t partial_diff_Nasl_del_Nasl = ((((((Vsl*Frdy)*((-(((((((((((Fsl*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(-((1./FoRT)*(((-p->Nao)/(sv->Nasl*sv->Nasl))/(p->Nao/sv->Nasl)))))+((Fsl*GNaB)*(-((1./FoRT)*(((-p->Nao)/(sv->Nasl*sv->Nasl))/(p->Nao/sv->Nasl))))))+(3.*(((1.+(ksat*expm1_nu_V_FoRT))*(((s3_sl*((((Fsl*IbarNCX)*pow_Q10NCX_Qpow)*Ka_sl)*(((((exp_nu_V_FoRT*sv->Nasl)+(exp_nu_V_FoRT*sv->Nasl))*sv->Nasl)+((exp_nu_V_FoRT*sv->Nasl)*sv->Nasl))*p->Cao)))-((((((Fsl*IbarNCX)*pow_Q10NCX_Qpow)*Ka_sl)*(s1_sl-(s2_sl)))*((((((KmCai*p->Nao)*p->Nao)*p->Nao)*(3.*((KmNai/(KmNai*KmNai))*(pow((sv->Nasl/KmNai),2.0)))))+((((KmCao*sv->Nasl)+(KmCao*sv->Nasl))*sv->Nasl)+((KmCao*sv->Nasl)*sv->Nasl)))+((((sv->Nasl+sv->Nasl)*sv->Nasl)+(sv->Nasl*sv->Nasl))*p->Cao)))))/(s3_sl*s3_sl)))/((1.+(ksat*expm1_nu_V_FoRT))*(1.+(ksat*expm1_nu_V_FoRT))))))+(3.*(((p->Ko+KmKo)*((-((((Fsl*IbarNaK)*fnak)*p->Ko)*(4.*(((-KmNaip)/(sv->Nasl*sv->Nasl))*(pow((KmNaip/sv->Nasl),3.0))))))/((1.+pow_KmNaip_Nasl_4)*(1.+pow_KmNaip_Nasl_4))))/((p->Ko+KmKo)*(p->Ko+KmKo)))))+((((((Fsl_CaL*((V==0.) ? ((FoRT*((pNa*(Frdy*FoRT))*0.75))/(FoRT*FoRT)) : ((expm1_VFoRT*((pNa*((V*Frdy)*FoRT))*(0.75*exp_VFoRT)))/(expm1_VFoRT*expm1_VFoRT))))*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*pow_Q10Cal_Qpow)*.45)))*Cm))/((Vsl*Frdy)*(Vsl*Frdy)))+((J_na_juncsl/Vsl)*-1.))+((J_na_slmyo/Vsl)*-1.))-((kon_na*(Bmax_Nasl-(sv->NaBsl)))));
    GlobalData_t partial_diff_SLHj_del_SLHj = (((kon_slh*sv->Caj)*-1.)-(koff_slh));
    GlobalData_t partial_diff_SLHsl_del_SLHsl = (((kon_slh*sv->Casl)*-1.)-(koff_slh));
    GlobalData_t partial_diff_SLLj_del_SLLj = (((kon_sll*sv->Caj)*-1.)-(koff_sll));
    GlobalData_t partial_diff_SLLsl_del_SLLsl = (((kon_sll*sv->Casl)*-1.)-(koff_sll));
    GlobalData_t partial_diff_SRB_del_SRB = (((kon_sr*Cai_mM)*-1.)-(koff_sr));
    GlobalData_t partial_diff_TnCHc_del_TnCHc = (((kon_tnchca*Cai_mM)*-1.)-(koff_tnchca));
    GlobalData_t partial_diff_fcaBj_del_fcaBj = (((1.7*sv->Caj)*-1.)-(11.9e-3));
    GlobalData_t partial_diff_fcaBsl_del_fcaBsl = (((1.7*sv->Casl)*-1.)-(11.9e-3));
    GlobalData_t permtot = (sqrt((pow(((sv->TnCL/Bmax_TnClow)/p->TRPN_50),p->n_xb))));
    GlobalData_t psi = (6.33e-3*(exp(((1.27*V)*FoRT))));
    GlobalData_t set_C10_tozero_in_diff_C10 = (((3.41e-3*(exp(((8.68e-1*V)*((96485./8314.)/310.)))))*sv->C7)+((5.74e-5*(exp(((-9.23e-2*V)*((96485./8314.)/310.)))))*sv->C11));
    GlobalData_t set_C11_tozero_in_diff_C11 = (((((2.*(3.41e-3*(exp(((8.68e-1*V)*((96485./8314.)/310.))))))*sv->C8)+((2.*(3.98e-4*(exp(((3.61e-1*V)*((96485./8314.)/310.))))))*sv->C10))+((2.*(5.74e-5*(exp(((-9.23e-2*V)*((96485./8314.)/310.))))))*sv->C12))+((3.*(1.2e-3*(exp(((-3.3e-1*V)*((96485./8314.)/310.))))))*sv->C13));
    GlobalData_t set_C12_tozero_in_diff_C12 = ((((3.*(3.41e-3*(exp(((8.68e-1*V)*((96485./8314.)/310.))))))*sv->C9)+((3.98e-4*(exp(((3.61e-1*V)*((96485./8314.)/310.)))))*sv->C11))+((3.*(1.2e-3*(exp(((-3.3e-1*V)*((96485./8314.)/310.))))))*sv->C14));
    GlobalData_t set_C13_tozero_in_diff_C13 = (((3.41e-3*(exp(((8.68e-1*V)*((96485./8314.)/310.)))))*sv->C11)+((5.74e-5*(exp(((-9.23e-2*V)*((96485./8314.)/310.)))))*sv->C14));
    GlobalData_t set_C14_tozero_in_diff_C14 = ((((2.*(3.41e-3*(exp(((8.68e-1*V)*((96485./8314.)/310.))))))*sv->C12)+((3.98e-4*(exp(((3.61e-1*V)*((96485./8314.)/310.)))))*sv->C13))+((4.*(1.2e-3*(exp(((-3.3e-1*V)*((96485./8314.)/310.))))))*sv->C15));
    GlobalData_t set_C15_tozero_in_diff_C15 = (((3.41e-3*(exp(((8.68e-1*V)*((96485./8314.)/310.)))))*sv->C14)+((1.25e-2*(exp(((-4.81e-1*V)*((96485./8314.)/310.)))))*sv->O1));
    GlobalData_t set_C1_tozero_in_diff_C1 = ((5.74e-5*(exp(((-9.23e-2*V)*((96485./8314.)/310.)))))*sv->C2);
    GlobalData_t set_C2_tozero_in_diff_C2 = (((4.*(3.98e-4*(exp(((3.61e-1*V)*((96485./8314.)/310.))))))*sv->C1)+((2.*(5.74e-5*(exp(((-9.23e-2*V)*((96485./8314.)/310.))))))*sv->C3));
    GlobalData_t set_C3_tozero_in_diff_C3 = (((3.*(3.98e-4*(exp(((3.61e-1*V)*((96485./8314.)/310.))))))*sv->C2)+((3.*(5.74e-5*(exp(((-9.23e-2*V)*((96485./8314.)/310.))))))*sv->C4));
    GlobalData_t set_C4_tozero_in_diff_C4 = (((2.*(3.98e-4*(exp(((3.61e-1*V)*((96485./8314.)/310.))))))*sv->C3)+((4.*(5.74e-5*(exp(((-9.23e-2*V)*((96485./8314.)/310.))))))*sv->C5));
    GlobalData_t set_C5_tozero_in_diff_C5 = (((3.98e-4*(exp(((3.61e-1*V)*((96485./8314.)/310.)))))*sv->C3)+((1.2e-3*(exp(((-3.3e-1*V)*((96485./8314.)/310.)))))*sv->C9));
    GlobalData_t set_C6_tozero_in_diff_C6 = (((3.41e-3*(exp(((8.68e-1*V)*((96485./8314.)/310.)))))*sv->C2)+((5.74e-5*(exp(((-9.23e-2*V)*((96485./8314.)/310.)))))*sv->C7));
    GlobalData_t set_C7_tozero_in_diff_C7 = (((((2.*(3.41e-3*(exp(((8.68e-1*V)*((96485./8314.)/310.))))))*sv->C3)+((3.*(3.98e-4*(exp(((3.61e-1*V)*((96485./8314.)/310.))))))*sv->C6))+((2.*(5.74e-5*(exp(((-9.23e-2*V)*((96485./8314.)/310.))))))*sv->C8))+((2.*(1.2e-3*(exp(((-3.3e-1*V)*((96485./8314.)/310.))))))*sv->C10));
    GlobalData_t set_C8_tozero_in_diff_C8 = (((((3.*(3.41e-3*(exp(((8.68e-1*V)*((96485./8314.)/310.))))))*sv->C4)+((2.*(3.98e-4*(exp(((3.61e-1*V)*((96485./8314.)/310.))))))*sv->C7))+((3.*(5.74e-5*(exp(((-9.23e-2*V)*((96485./8314.)/310.))))))*sv->C9))+((2.*(1.2e-3*(exp(((-3.3e-1*V)*((96485./8314.)/310.))))))*sv->C11));
    GlobalData_t set_C9_tozero_in_diff_C9 = ((((4.*(3.41e-3*(exp(((8.68e-1*V)*((96485./8314.)/310.))))))*sv->C5)+((3.98e-4*(exp(((3.61e-1*V)*((96485./8314.)/310.)))))*sv->C8))+((2.*(1.2e-3*(exp(((-3.3e-1*V)*((96485./8314.)/310.))))))*sv->C12));
    GlobalData_t set_CaM_tozero_in_diff_CaM = ((34.*(sv->Cai/1000.))*24e-3);
    GlobalData_t set_Csqnb_tozero_in_diff_Csqnb = ((100.*sv->Ca_sr)*((140e-3*(0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))/(0.035*((((3.14159*10.25)*10.25)*100.)*1e-15))));
    GlobalData_t set_Myoc_tozero_in_diff_Myoc = ((13.8*(sv->Cai/1000.))*(140e-3-(sv->Myom)));
    GlobalData_t set_Myom_tozero_in_diff_Myom = (0.0157*(140e-3-(sv->Myoc)));
    GlobalData_t set_NaBj_tozero_in_diff_NaBj = ((0.1e-3*sv->Naj)*7.561);
    GlobalData_t set_NaBsl_tozero_in_diff_NaBsl = ((0.1e-3*sv->Nasl)*1.65);
    GlobalData_t set_O1_tozero_in_diff_O1 = ((6.47e-3*sv->C15)+((4.91e-3*(exp(((-6.79e-1*V)*((96485./8314.)/310.)))))*(1.-(((((((((((((((sv->C1+sv->C2)+sv->C3)+sv->C4)+sv->C5)+sv->C6)+sv->C7)+sv->C8)+sv->C9)+sv->C10)+sv->C11)+sv->C12)+sv->C13)+sv->C14)+sv->C15)))));
    GlobalData_t set_Q_1_tozero_in_diff_Q_1 = (-29.*delta_sl);
    GlobalData_t set_Q_2_tozero_in_diff_Q_2 = (116.*delta_sl);
    GlobalData_t set_RyRi_tozero_in_diff_RyRi = ((((0.5*(15.-(((15.-(1.))/(1.+(pow((0.45/sv->Ca_sr),2.5)))))))*sv->Caj)*sv->RyRo)-((-((((10./(15.-(((15.-(1.))/(1.+(pow((0.45/sv->Ca_sr),2.5)))))))*sv->Caj)*sv->Caj)*((1.-(sv->RyRr))-(sv->RyRo))))));
    GlobalData_t set_RyRo_tozero_in_diff_RyRo = (((((10./(15.-(((15.-(1.))/(1.+(pow((0.45/sv->Ca_sr),2.5)))))))*sv->Caj)*sv->Caj)*sv->RyRr)-((-(0.005*sv->RyRi))));
    GlobalData_t set_RyRr_tozero_in_diff_RyRr = ((0.005*((1.-(sv->RyRo))-(sv->RyRi)))-((-(0.06*sv->RyRo))));
    GlobalData_t set_SLHj_tozero_in_diff_SLHj = ((100.*sv->Caj)*(((1.65e-3*(0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))/((0.0539*.01)*((((3.14159*10.25)*10.25)*100.)*1e-15)))*0.1));
    GlobalData_t set_SLHsl_tozero_in_diff_SLHsl = ((100.*sv->Casl)*((13.4e-3*(0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))/(0.02*((((3.14159*10.25)*10.25)*100.)*1e-15))));
    GlobalData_t set_SLLj_tozero_in_diff_SLLj = ((100.*sv->Caj)*(((4.6e-3*(0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))/((0.0539*.01)*((((3.14159*10.25)*10.25)*100.)*1e-15)))*0.1));
    GlobalData_t set_SLLsl_tozero_in_diff_SLLsl = ((100.*sv->Casl)*((37.4e-3*(0.65*((((3.14159*10.25)*10.25)*100.)*1e-15)))/(0.02*((((3.14159*10.25)*10.25)*100.)*1e-15))));
    GlobalData_t set_SRB_tozero_in_diff_SRB = ((100.*(sv->Cai/1000.))*(19.*.9e-3));
    GlobalData_t set_TnCHc_tozero_in_diff_TnCHc = ((2.37*(sv->Cai/1000.))*(140e-3-(sv->TnCHm)));
    GlobalData_t set_TnCHm_tozero_in_diff_TnCHm = (3e-3*(140e-3-(sv->TnCHc)));
    GlobalData_t set_fcaBj_tozero_in_diff_fcaBj = (1.7*sv->Caj);
    GlobalData_t set_fcaBsl_tozero_in_diff_fcaBsl = (1.7*sv->Casl);
    GlobalData_t set_xb_tozero_in_diff_xb = (4.9e-3*(sqrt((pow(((sv->TnCL/70e-3)/0.37),3.38)))));
    GlobalData_t tau_f = (1./((0.0197*(exp((-(pow((0.0337*(V+14.5)),2.))))))+0.02));
    GlobalData_t tau_xkr = ((((550./(1.+(exp(((-22.-(V))/9.)))))*6.)/(1.+(exp(((V-(-11.))/9.)))))+(230./(1.+(exp(((V-(-40.))/20.))))));
    GlobalData_t tau_xks = (990.1/(1.+(exp(((-(V+2.436))/14.12)))));
    GlobalData_t tau_xtof = ((8.5*(exp((-(pow(((V+45.)/50.),2.))))))+0.5);
    GlobalData_t tau_xtos = ((9./(1.+(exp(((V+3.0)/15.)))))+0.5);
    GlobalData_t tau_ytof = ((85.*(exp(((-(pow((V+40.),2.)))/220.))))+7.);
    GlobalData_t tau_ytos = ((800./(1.+(exp(((V+60.0)/10.)))))+30.);
    GlobalData_t taum_factor1 = ((V+45.79)/15.54);
    GlobalData_t taum_factor2 = ((V-(4.823))/51.12);
    GlobalData_t xkr_infinity = (1./(1.+(exp(((-(V+10.))/5.)))));
    GlobalData_t xks_infinity = (1./(1.+(exp(((-(V+3.8))/14.25)))));
    GlobalData_t xtos_infinity = (1./(1.+(exp(((-(V-(19.0)))/13.)))));
    GlobalData_t ytos_infinity = (1./(1.+(exp(((V+19.5)/5.)))));
    GlobalData_t CaM_rush_larsen_A = ((set_CaM_tozero_in_diff_CaM/partial_diff_CaM_del_CaM)*(expm1((dt*partial_diff_CaM_del_CaM))));
    GlobalData_t CaM_rush_larsen_B = (exp((dt*partial_diff_CaM_del_CaM)));
    GlobalData_t Csqnb_rush_larsen_A = ((set_Csqnb_tozero_in_diff_Csqnb/partial_diff_Csqnb_del_Csqnb)*(expm1((dt*partial_diff_Csqnb_del_Csqnb))));
    GlobalData_t Csqnb_rush_larsen_B = (exp((dt*partial_diff_Csqnb_del_Csqnb)));
    GlobalData_t J_CaB_cytosol = ((((((diff_TnCL+diff_TnCHc)+diff_TnCHm)+diff_CaM)+diff_Myoc)+diff_Myom)+diff_SRB);
    GlobalData_t J_CaB_junction = (diff_SLLj+diff_SLHj);
    GlobalData_t J_CaB_sl = (diff_SLLsl+diff_SLHsl);
    GlobalData_t Myoc_rush_larsen_A = ((set_Myoc_tozero_in_diff_Myoc/partial_diff_Myoc_del_Myoc)*(expm1((dt*partial_diff_Myoc_del_Myoc))));
    GlobalData_t Myoc_rush_larsen_B = (exp((dt*partial_diff_Myoc_del_Myoc)));
    GlobalData_t Myom_rush_larsen_A = ((set_Myom_tozero_in_diff_Myom/partial_diff_Myom_del_Myom)*(expm1((dt*partial_diff_Myom_del_Myom))));
    GlobalData_t NaBj_rush_larsen_A = ((set_NaBj_tozero_in_diff_NaBj/partial_diff_NaBj_del_NaBj)*(expm1((dt*partial_diff_NaBj_del_NaBj))));
    GlobalData_t NaBj_rush_larsen_B = (exp((dt*partial_diff_NaBj_del_NaBj)));
    GlobalData_t NaBsl_rush_larsen_A = ((set_NaBsl_tozero_in_diff_NaBsl/partial_diff_NaBsl_del_NaBsl)*(expm1((dt*partial_diff_NaBsl_del_NaBsl))));
    GlobalData_t NaBsl_rush_larsen_B = (exp((dt*partial_diff_NaBsl_del_NaBsl)));
    GlobalData_t Q_1_rush_larsen_A = ((set_Q_1_tozero_in_diff_Q_1/partial_diff_Q_1_del_Q_1)*(expm1((dt*partial_diff_Q_1_del_Q_1))));
    GlobalData_t Q_2_rush_larsen_A = ((set_Q_2_tozero_in_diff_Q_2/partial_diff_Q_2_del_Q_2)*(expm1((dt*partial_diff_Q_2_del_Q_2))));
    GlobalData_t SLHj_rush_larsen_A = ((set_SLHj_tozero_in_diff_SLHj/partial_diff_SLHj_del_SLHj)*(expm1((dt*partial_diff_SLHj_del_SLHj))));
    GlobalData_t SLHj_rush_larsen_B = (exp((dt*partial_diff_SLHj_del_SLHj)));
    GlobalData_t SLHsl_rush_larsen_A = ((set_SLHsl_tozero_in_diff_SLHsl/partial_diff_SLHsl_del_SLHsl)*(expm1((dt*partial_diff_SLHsl_del_SLHsl))));
    GlobalData_t SLHsl_rush_larsen_B = (exp((dt*partial_diff_SLHsl_del_SLHsl)));
    GlobalData_t SLLj_rush_larsen_A = ((set_SLLj_tozero_in_diff_SLLj/partial_diff_SLLj_del_SLLj)*(expm1((dt*partial_diff_SLLj_del_SLLj))));
    GlobalData_t SLLj_rush_larsen_B = (exp((dt*partial_diff_SLLj_del_SLLj)));
    GlobalData_t SLLsl_rush_larsen_A = ((set_SLLsl_tozero_in_diff_SLLsl/partial_diff_SLLsl_del_SLLsl)*(expm1((dt*partial_diff_SLLsl_del_SLLsl))));
    GlobalData_t SLLsl_rush_larsen_B = (exp((dt*partial_diff_SLLsl_del_SLLsl)));
    GlobalData_t SRB_rush_larsen_A = ((set_SRB_tozero_in_diff_SRB/partial_diff_SRB_del_SRB)*(expm1((dt*partial_diff_SRB_del_SRB))));
    GlobalData_t SRB_rush_larsen_B = (exp((dt*partial_diff_SRB_del_SRB)));
    GlobalData_t TnCHc_rush_larsen_A = ((set_TnCHc_tozero_in_diff_TnCHc/partial_diff_TnCHc_del_TnCHc)*(expm1((dt*partial_diff_TnCHc_del_TnCHc))));
    GlobalData_t TnCHc_rush_larsen_B = (exp((dt*partial_diff_TnCHc_del_TnCHc)));
    GlobalData_t TnCHm_rush_larsen_A = ((set_TnCHm_tozero_in_diff_TnCHm/partial_diff_TnCHm_del_TnCHm)*(expm1((dt*partial_diff_TnCHm_del_TnCHm))));
    GlobalData_t diff_Ca_sr = ((J_serca-((((J_SRleak*Vmyo)/Vsr)+J_SRCarel)))-(diff_Csqnb));
    GlobalData_t diff_Naj = (((((-INa_tot_junc)*Cm)/(Vjunc*Frdy))+((J_na_juncsl/Vjunc)*(sv->Nasl-(sv->Naj))))-(diff_NaBj));
    GlobalData_t diff_Nasl = ((((((-INa_tot_sl)*Cm)/(Vsl*Frdy))+((J_na_juncsl/Vsl)*(sv->Naj-(sv->Nasl))))+((J_na_slmyo/Vsl)*(sv->Nai-(sv->Nasl))))-(diff_NaBsl));
    GlobalData_t f_rush_larsen_B = (exp(((-dt)/tau_f)));
    GlobalData_t f_rush_larsen_C = (expm1(((-dt)/tau_f)));
    GlobalData_t fcaBj_rush_larsen_A = ((set_fcaBj_tozero_in_diff_fcaBj/partial_diff_fcaBj_del_fcaBj)*(expm1((dt*partial_diff_fcaBj_del_fcaBj))));
    GlobalData_t fcaBj_rush_larsen_B = (exp((dt*partial_diff_fcaBj_del_fcaBj)));
    GlobalData_t fcaBsl_rush_larsen_A = ((set_fcaBsl_tozero_in_diff_fcaBsl/partial_diff_fcaBsl_del_fcaBsl)*(expm1((dt*partial_diff_fcaBsl_del_fcaBsl))));
    GlobalData_t fcaBsl_rush_larsen_B = (exp((dt*partial_diff_fcaBsl_del_fcaBsl)));
    GlobalData_t h_infinity = (1./(hss_factor*hss_factor));
    GlobalData_t j_infinity = (1./(jss_factor*jss_factor));
    GlobalData_t kiSRCa = (kiCa*kCaSR);
    GlobalData_t koSRCa = (koCa/kCaSR);
    GlobalData_t m_infinity = (1./(mss_factor*mss_factor));
    GlobalData_t partial_diff_C10_del_C10 = (-((2.*delta)+(2.*alpha)));
    GlobalData_t partial_diff_C11_del_C11 = (-((((2.*delta)+beta)+alpha)+gamma));
    GlobalData_t partial_diff_C12_del_C12 = (-(((2.*delta)+(2.*beta))+(2.*gamma)));
    GlobalData_t partial_diff_C13_del_C13 = (-((3.*delta)+alpha));
    GlobalData_t partial_diff_C14_del_C14 = (-(((3.*delta)+beta)+gamma));
    GlobalData_t partial_diff_C15_del_C15 = (-((4.*delta)+teta));
    GlobalData_t partial_diff_C1_del_C1 = (-4.*alpha);
    GlobalData_t partial_diff_C2_del_C2 = (-((beta+gamma)+(3.*alpha)));
    GlobalData_t partial_diff_C3_del_C3 = (-(((2.*beta)+(2.*gamma))+(2.*alpha)));
    GlobalData_t partial_diff_C4_del_C4 = (-(((3.*beta)+(3.*gamma))+alpha));
    GlobalData_t partial_diff_C5_del_C5 = (-((4.*beta)+(4.*gamma)));
    GlobalData_t partial_diff_C6_del_C6 = (-(delta+(3.*alpha)));
    GlobalData_t partial_diff_C7_del_C7 = (-(((delta+beta)+(2.*alpha))+gamma));
    GlobalData_t partial_diff_C8_del_C8 = (-(((delta+(2.*beta))+alpha)+(2.*gamma)));
    GlobalData_t partial_diff_C9_del_C9 = (-((delta+(3.*beta))+(3.*gamma)));
    GlobalData_t partial_diff_O1_del_O1 = ((-(eta+psi))+(omega*-1.));
    GlobalData_t partial_diff_xb_del_xb = (p->k_xb*((permtot*-1.)-((1./permtot))));
    GlobalData_t tau_d = ((((-5.-(d_remove_tol))<=V)&&(V<=(-5.+d_remove_tol))) ? ((d_infinity/6.0)/0.035) : ((d_infinity*(1.-((exp(((-(V+5.))/6.0))))))/(0.035*(V+5.))));
    GlobalData_t tau_h = (1./(ah+bh));
    GlobalData_t tau_j = (1./(aj+bj));
    GlobalData_t tau_m = ((0.1292*(exp(((-taum_factor1)*taum_factor1))))+(0.06487*(exp(((-taum_factor2)*taum_factor2)))));
    GlobalData_t xkr_rush_larsen_B = (exp(((-dt)/tau_xkr)));
    GlobalData_t xkr_rush_larsen_C = (expm1(((-dt)/tau_xkr)));
    GlobalData_t xks_rush_larsen_B = (exp(((-dt)/tau_xks)));
    GlobalData_t xks_rush_larsen_C = (expm1(((-dt)/tau_xks)));
    GlobalData_t xtof_infinity = xtos_infinity;
    GlobalData_t xtof_rush_larsen_B = (exp(((-dt)/tau_xtof)));
    GlobalData_t xtof_rush_larsen_C = (expm1(((-dt)/tau_xtof)));
    GlobalData_t xtos_rush_larsen_B = (exp(((-dt)/tau_xtos)));
    GlobalData_t xtos_rush_larsen_C = (expm1(((-dt)/tau_xtos)));
    GlobalData_t ytof_infinity = ytos_infinity;
    GlobalData_t ytof_rush_larsen_B = (exp(((-dt)/tau_ytof)));
    GlobalData_t ytof_rush_larsen_C = (expm1(((-dt)/tau_ytof)));
    GlobalData_t ytos_rush_larsen_B = (exp(((-dt)/tau_ytos)));
    GlobalData_t ytos_rush_larsen_C = (expm1(((-dt)/tau_ytos)));
    GlobalData_t C10_rush_larsen_A = ((set_C10_tozero_in_diff_C10/partial_diff_C10_del_C10)*(expm1((dt*partial_diff_C10_del_C10))));
    GlobalData_t C10_rush_larsen_B = (exp((dt*partial_diff_C10_del_C10)));
    GlobalData_t C11_rush_larsen_A = ((set_C11_tozero_in_diff_C11/partial_diff_C11_del_C11)*(expm1((dt*partial_diff_C11_del_C11))));
    GlobalData_t C11_rush_larsen_B = (exp((dt*partial_diff_C11_del_C11)));
    GlobalData_t C12_rush_larsen_A = ((set_C12_tozero_in_diff_C12/partial_diff_C12_del_C12)*(expm1((dt*partial_diff_C12_del_C12))));
    GlobalData_t C12_rush_larsen_B = (exp((dt*partial_diff_C12_del_C12)));
    GlobalData_t C13_rush_larsen_A = ((set_C13_tozero_in_diff_C13/partial_diff_C13_del_C13)*(expm1((dt*partial_diff_C13_del_C13))));
    GlobalData_t C13_rush_larsen_B = (exp((dt*partial_diff_C13_del_C13)));
    GlobalData_t C14_rush_larsen_A = ((set_C14_tozero_in_diff_C14/partial_diff_C14_del_C14)*(expm1((dt*partial_diff_C14_del_C14))));
    GlobalData_t C14_rush_larsen_B = (exp((dt*partial_diff_C14_del_C14)));
    GlobalData_t C15_rush_larsen_A = ((set_C15_tozero_in_diff_C15/partial_diff_C15_del_C15)*(expm1((dt*partial_diff_C15_del_C15))));
    GlobalData_t C15_rush_larsen_B = (exp((dt*partial_diff_C15_del_C15)));
    GlobalData_t C1_rush_larsen_A = ((set_C1_tozero_in_diff_C1/partial_diff_C1_del_C1)*(expm1((dt*partial_diff_C1_del_C1))));
    GlobalData_t C1_rush_larsen_B = (exp((dt*partial_diff_C1_del_C1)));
    GlobalData_t C2_rush_larsen_A = ((set_C2_tozero_in_diff_C2/partial_diff_C2_del_C2)*(expm1((dt*partial_diff_C2_del_C2))));
    GlobalData_t C2_rush_larsen_B = (exp((dt*partial_diff_C2_del_C2)));
    GlobalData_t C3_rush_larsen_A = ((set_C3_tozero_in_diff_C3/partial_diff_C3_del_C3)*(expm1((dt*partial_diff_C3_del_C3))));
    GlobalData_t C3_rush_larsen_B = (exp((dt*partial_diff_C3_del_C3)));
    GlobalData_t C4_rush_larsen_A = ((set_C4_tozero_in_diff_C4/partial_diff_C4_del_C4)*(expm1((dt*partial_diff_C4_del_C4))));
    GlobalData_t C4_rush_larsen_B = (exp((dt*partial_diff_C4_del_C4)));
    GlobalData_t C5_rush_larsen_A = ((set_C5_tozero_in_diff_C5/partial_diff_C5_del_C5)*(expm1((dt*partial_diff_C5_del_C5))));
    GlobalData_t C5_rush_larsen_B = (exp((dt*partial_diff_C5_del_C5)));
    GlobalData_t C6_rush_larsen_A = ((set_C6_tozero_in_diff_C6/partial_diff_C6_del_C6)*(expm1((dt*partial_diff_C6_del_C6))));
    GlobalData_t C6_rush_larsen_B = (exp((dt*partial_diff_C6_del_C6)));
    GlobalData_t C7_rush_larsen_A = ((set_C7_tozero_in_diff_C7/partial_diff_C7_del_C7)*(expm1((dt*partial_diff_C7_del_C7))));
    GlobalData_t C7_rush_larsen_B = (exp((dt*partial_diff_C7_del_C7)));
    GlobalData_t C8_rush_larsen_A = ((set_C8_tozero_in_diff_C8/partial_diff_C8_del_C8)*(expm1((dt*partial_diff_C8_del_C8))));
    GlobalData_t C8_rush_larsen_B = (exp((dt*partial_diff_C8_del_C8)));
    GlobalData_t C9_rush_larsen_A = ((set_C9_tozero_in_diff_C9/partial_diff_C9_del_C9)*(expm1((dt*partial_diff_C9_del_C9))));
    GlobalData_t C9_rush_larsen_B = (exp((dt*partial_diff_C9_del_C9)));
    GlobalData_t Ca_sr_rush_larsen_A = ((expm1((partial_diff_Ca_sr_del_Ca_sr*dt)))*(diff_Ca_sr/partial_diff_Ca_sr_del_Ca_sr));
    GlobalData_t Naj_rush_larsen_A = ((expm1((partial_diff_Naj_del_Naj*dt)))*(diff_Naj/partial_diff_Naj_del_Naj));
    GlobalData_t Nasl_rush_larsen_A = ((expm1((partial_diff_Nasl_del_Nasl*dt)))*(diff_Nasl/partial_diff_Nasl_del_Nasl));
    GlobalData_t O1_rush_larsen_A = ((set_O1_tozero_in_diff_O1/partial_diff_O1_del_O1)*(expm1((dt*partial_diff_O1_del_O1))));
    GlobalData_t O1_rush_larsen_B = (exp((dt*partial_diff_O1_del_O1)));
    GlobalData_t d_rush_larsen_B = (exp(((-dt)/tau_d)));
    GlobalData_t d_rush_larsen_C = (expm1(((-dt)/tau_d)));
    GlobalData_t diff_Cai = ((((((-J_serca)*Vsr)/Vmyo)-(J_CaB_cytosol))+((J_ca_slmyo/Vmyo)*(sv->Casl-(Cai_mM))))*1.e3);
    GlobalData_t diff_Caj = (((((((-ICa_tot_junc)*Cm)/((Vjunc*2.)*Frdy))+((J_ca_juncsl/Vjunc)*(sv->Casl-(sv->Caj))))-(J_CaB_junction))+((J_SRCarel*Vsr)/Vjunc))+((J_SRleak*Vmyo)/Vjunc));
    GlobalData_t diff_Casl = ((((((-ICa_tot_sl)*Cm)/((Vsl*2.)*Frdy))+((J_ca_juncsl/Vsl)*(sv->Caj-(sv->Casl))))+((J_ca_slmyo/Vsl)*(Cai_mM-(sv->Casl))))-(J_CaB_sl));
    GlobalData_t f_rush_larsen_A = ((-f_infinity)*f_rush_larsen_C);
    GlobalData_t h_rush_larsen_B = (exp(((-dt)/tau_h)));
    GlobalData_t h_rush_larsen_C = (expm1(((-dt)/tau_h)));
    GlobalData_t j_rush_larsen_B = (exp(((-dt)/tau_j)));
    GlobalData_t j_rush_larsen_C = (expm1(((-dt)/tau_j)));
    GlobalData_t m_rush_larsen_B = (exp(((-dt)/tau_m)));
    GlobalData_t m_rush_larsen_C = (expm1(((-dt)/tau_m)));
    GlobalData_t partial_diff_RyRi_del_RyRi = ((-kim)-((kom-((((koSRCa*sv->Caj)*sv->Caj)*-1.)))));
    GlobalData_t partial_diff_RyRo_del_RyRo = ((-kom)-((kiSRCa*sv->Caj)));
    GlobalData_t partial_diff_RyRr_del_RyRr = (((kim*-1.)-((kiSRCa*sv->Caj)))-(((koSRCa*sv->Caj)*sv->Caj)));
    GlobalData_t xb_rush_larsen_A = ((set_xb_tozero_in_diff_xb/partial_diff_xb_del_xb)*(expm1((dt*partial_diff_xb_del_xb))));
    GlobalData_t xb_rush_larsen_B = (exp((dt*partial_diff_xb_del_xb)));
    GlobalData_t xkr_rush_larsen_A = ((-xkr_infinity)*xkr_rush_larsen_C);
    GlobalData_t xks_rush_larsen_A = ((-xks_infinity)*xks_rush_larsen_C);
    GlobalData_t xtof_rush_larsen_A = ((-xtof_infinity)*xtof_rush_larsen_C);
    GlobalData_t xtos_rush_larsen_A = ((-xtos_infinity)*xtos_rush_larsen_C);
    GlobalData_t ytof_rush_larsen_A = ((-ytof_infinity)*ytof_rush_larsen_C);
    GlobalData_t ytos_rush_larsen_A = ((-ytos_infinity)*ytos_rush_larsen_C);
    GlobalData_t Cai_rush_larsen_A = ((expm1((partial_diff_Cai_del_Cai*dt)))*(diff_Cai/partial_diff_Cai_del_Cai));
    GlobalData_t Caj_rush_larsen_A = ((expm1((partial_diff_Caj_del_Caj*dt)))*(diff_Caj/partial_diff_Caj_del_Caj));
    GlobalData_t Casl_rush_larsen_A = ((expm1((partial_diff_Casl_del_Casl*dt)))*(diff_Casl/partial_diff_Casl_del_Casl));
    GlobalData_t RyRi_rush_larsen_A = ((set_RyRi_tozero_in_diff_RyRi/partial_diff_RyRi_del_RyRi)*(expm1((dt*partial_diff_RyRi_del_RyRi))));
    GlobalData_t RyRi_rush_larsen_B = (exp((dt*partial_diff_RyRi_del_RyRi)));
    GlobalData_t RyRo_rush_larsen_A = ((set_RyRo_tozero_in_diff_RyRo/partial_diff_RyRo_del_RyRo)*(expm1((dt*partial_diff_RyRo_del_RyRo))));
    GlobalData_t RyRo_rush_larsen_B = (exp((dt*partial_diff_RyRo_del_RyRo)));
    GlobalData_t RyRr_rush_larsen_A = ((set_RyRr_tozero_in_diff_RyRr/partial_diff_RyRr_del_RyRr)*(expm1((dt*partial_diff_RyRr_del_RyRr))));
    GlobalData_t RyRr_rush_larsen_B = (exp((dt*partial_diff_RyRr_del_RyRr)));
    GlobalData_t d_rush_larsen_A = ((-d_infinity)*d_rush_larsen_C);
    GlobalData_t h_rush_larsen_A = ((-h_infinity)*h_rush_larsen_C);
    GlobalData_t j_rush_larsen_A = ((-j_infinity)*j_rush_larsen_C);
    GlobalData_t m_rush_larsen_A = ((-m_infinity)*m_rush_larsen_C);
    GlobalData_t C1_new = C1_rush_larsen_A+C1_rush_larsen_B*sv->C1;
    GlobalData_t C10_new = C10_rush_larsen_A+C10_rush_larsen_B*sv->C10;
    GlobalData_t C11_new = C11_rush_larsen_A+C11_rush_larsen_B*sv->C11;
    GlobalData_t C12_new = C12_rush_larsen_A+C12_rush_larsen_B*sv->C12;
    GlobalData_t C13_new = C13_rush_larsen_A+C13_rush_larsen_B*sv->C13;
    GlobalData_t C14_new = C14_rush_larsen_A+C14_rush_larsen_B*sv->C14;
    GlobalData_t C15_new = C15_rush_larsen_A+C15_rush_larsen_B*sv->C15;
    GlobalData_t C2_new = C2_rush_larsen_A+C2_rush_larsen_B*sv->C2;
    GlobalData_t C3_new = C3_rush_larsen_A+C3_rush_larsen_B*sv->C3;
    GlobalData_t C4_new = C4_rush_larsen_A+C4_rush_larsen_B*sv->C4;
    GlobalData_t C5_new = C5_rush_larsen_A+C5_rush_larsen_B*sv->C5;
    GlobalData_t C6_new = C6_rush_larsen_A+C6_rush_larsen_B*sv->C6;
    GlobalData_t C7_new = C7_rush_larsen_A+C7_rush_larsen_B*sv->C7;
    GlobalData_t C8_new = C8_rush_larsen_A+C8_rush_larsen_B*sv->C8;
    GlobalData_t C9_new = C9_rush_larsen_A+C9_rush_larsen_B*sv->C9;
    GlobalData_t CaM_new = CaM_rush_larsen_A+CaM_rush_larsen_B*sv->CaM;
    GlobalData_t Ca_sr_new = Ca_sr_rush_larsen_A+Ca_sr_rush_larsen_B*sv->Ca_sr;
    GlobalData_t Cai_new = Cai_rush_larsen_A+Cai_rush_larsen_B*sv->Cai;
    GlobalData_t Caj_new = Caj_rush_larsen_A+Caj_rush_larsen_B*sv->Caj;
    GlobalData_t Casl_new = Casl_rush_larsen_A+Casl_rush_larsen_B*sv->Casl;
    GlobalData_t Csqnb_new = Csqnb_rush_larsen_A+Csqnb_rush_larsen_B*sv->Csqnb;
    GlobalData_t Myoc_new = Myoc_rush_larsen_A+Myoc_rush_larsen_B*sv->Myoc;
    GlobalData_t Myom_new = Myom_rush_larsen_A+Myom_rush_larsen_B*sv->Myom;
    GlobalData_t NaBj_new = NaBj_rush_larsen_A+NaBj_rush_larsen_B*sv->NaBj;
    GlobalData_t NaBsl_new = NaBsl_rush_larsen_A+NaBsl_rush_larsen_B*sv->NaBsl;
    GlobalData_t Naj_new = Naj_rush_larsen_A+Naj_rush_larsen_B*sv->Naj;
    GlobalData_t Nasl_new = Nasl_rush_larsen_A+Nasl_rush_larsen_B*sv->Nasl;
    GlobalData_t O1_new = O1_rush_larsen_A+O1_rush_larsen_B*sv->O1;
    GlobalData_t Q_1_new = Q_1_rush_larsen_A+Q_1_rush_larsen_B*sv->Q_1;
    GlobalData_t Q_2_new = Q_2_rush_larsen_A+Q_2_rush_larsen_B*sv->Q_2;
    GlobalData_t RyRi_new = RyRi_rush_larsen_A+RyRi_rush_larsen_B*sv->RyRi;
    GlobalData_t RyRo_new = RyRo_rush_larsen_A+RyRo_rush_larsen_B*sv->RyRo;
    GlobalData_t RyRr_new = RyRr_rush_larsen_A+RyRr_rush_larsen_B*sv->RyRr;
    GlobalData_t SLHj_new = SLHj_rush_larsen_A+SLHj_rush_larsen_B*sv->SLHj;
    GlobalData_t SLHsl_new = SLHsl_rush_larsen_A+SLHsl_rush_larsen_B*sv->SLHsl;
    GlobalData_t SLLj_new = SLLj_rush_larsen_A+SLLj_rush_larsen_B*sv->SLLj;
    GlobalData_t SLLsl_new = SLLsl_rush_larsen_A+SLLsl_rush_larsen_B*sv->SLLsl;
    GlobalData_t SRB_new = SRB_rush_larsen_A+SRB_rush_larsen_B*sv->SRB;
    GlobalData_t TnCHc_new = TnCHc_rush_larsen_A+TnCHc_rush_larsen_B*sv->TnCHc;
    GlobalData_t TnCHm_new = TnCHm_rush_larsen_A+TnCHm_rush_larsen_B*sv->TnCHm;
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f_new = f_rush_larsen_A+f_rush_larsen_B*sv->f;
    GlobalData_t fcaBj_new = fcaBj_rush_larsen_A+fcaBj_rush_larsen_B*sv->fcaBj;
    GlobalData_t fcaBsl_new = fcaBsl_rush_larsen_A+fcaBsl_rush_larsen_B*sv->fcaBsl;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t xb_new = xb_rush_larsen_A+xb_rush_larsen_B*sv->xb;
    GlobalData_t xkr_new = xkr_rush_larsen_A+xkr_rush_larsen_B*sv->xkr;
    GlobalData_t xks_new = xks_rush_larsen_A+xks_rush_larsen_B*sv->xks;
    GlobalData_t xtof_new = xtof_rush_larsen_A+xtof_rush_larsen_B*sv->xtof;
    GlobalData_t xtos_new = xtos_rush_larsen_A+xtos_rush_larsen_B*sv->xtos;
    GlobalData_t ytof_new = ytof_rush_larsen_A+ytof_rush_larsen_B*sv->ytof;
    GlobalData_t ytos_new = ytos_rush_larsen_A+ytos_rush_larsen_B*sv->ytos;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    if (C1_new < 0) { IIF_warn(__i, "C1 < 0, clamping to 0"); sv->C1 = 0; }
    else if (C1_new > 1) { IIF_warn(__i, "C1 > 1, clamping to 1"); sv->C1 = 1; }
    else {sv->C1 = C1_new;}
    if (C10_new < 0) { IIF_warn(__i, "C10 < 0, clamping to 0"); sv->C10 = 0; }
    else if (C10_new > 1) { IIF_warn(__i, "C10 > 1, clamping to 1"); sv->C10 = 1; }
    else {sv->C10 = C10_new;}
    if (C11_new < 0) { IIF_warn(__i, "C11 < 0, clamping to 0"); sv->C11 = 0; }
    else if (C11_new > 1) { IIF_warn(__i, "C11 > 1, clamping to 1"); sv->C11 = 1; }
    else {sv->C11 = C11_new;}
    if (C12_new < 0) { IIF_warn(__i, "C12 < 0, clamping to 0"); sv->C12 = 0; }
    else if (C12_new > 1) { IIF_warn(__i, "C12 > 1, clamping to 1"); sv->C12 = 1; }
    else {sv->C12 = C12_new;}
    if (C13_new < 0) { IIF_warn(__i, "C13 < 0, clamping to 0"); sv->C13 = 0; }
    else if (C13_new > 1) { IIF_warn(__i, "C13 > 1, clamping to 1"); sv->C13 = 1; }
    else {sv->C13 = C13_new;}
    if (C14_new < 0) { IIF_warn(__i, "C14 < 0, clamping to 0"); sv->C14 = 0; }
    else if (C14_new > 1) { IIF_warn(__i, "C14 > 1, clamping to 1"); sv->C14 = 1; }
    else {sv->C14 = C14_new;}
    if (C15_new < 0) { IIF_warn(__i, "C15 < 0, clamping to 0"); sv->C15 = 0; }
    else if (C15_new > 1) { IIF_warn(__i, "C15 > 1, clamping to 1"); sv->C15 = 1; }
    else {sv->C15 = C15_new;}
    if (C2_new < 0) { IIF_warn(__i, "C2 < 0, clamping to 0"); sv->C2 = 0; }
    else if (C2_new > 1) { IIF_warn(__i, "C2 > 1, clamping to 1"); sv->C2 = 1; }
    else {sv->C2 = C2_new;}
    if (C3_new < 0) { IIF_warn(__i, "C3 < 0, clamping to 0"); sv->C3 = 0; }
    else if (C3_new > 1) { IIF_warn(__i, "C3 > 1, clamping to 1"); sv->C3 = 1; }
    else {sv->C3 = C3_new;}
    if (C4_new < 0) { IIF_warn(__i, "C4 < 0, clamping to 0"); sv->C4 = 0; }
    else if (C4_new > 1) { IIF_warn(__i, "C4 > 1, clamping to 1"); sv->C4 = 1; }
    else {sv->C4 = C4_new;}
    if (C5_new < 0) { IIF_warn(__i, "C5 < 0, clamping to 0"); sv->C5 = 0; }
    else if (C5_new > 1) { IIF_warn(__i, "C5 > 1, clamping to 1"); sv->C5 = 1; }
    else {sv->C5 = C5_new;}
    if (C6_new < 0) { IIF_warn(__i, "C6 < 0, clamping to 0"); sv->C6 = 0; }
    else if (C6_new > 1) { IIF_warn(__i, "C6 > 1, clamping to 1"); sv->C6 = 1; }
    else {sv->C6 = C6_new;}
    if (C7_new < 0) { IIF_warn(__i, "C7 < 0, clamping to 0"); sv->C7 = 0; }
    else if (C7_new > 1) { IIF_warn(__i, "C7 > 1, clamping to 1"); sv->C7 = 1; }
    else {sv->C7 = C7_new;}
    if (C8_new < 0) { IIF_warn(__i, "C8 < 0, clamping to 0"); sv->C8 = 0; }
    else if (C8_new > 1) { IIF_warn(__i, "C8 > 1, clamping to 1"); sv->C8 = 1; }
    else {sv->C8 = C8_new;}
    if (C9_new < 0) { IIF_warn(__i, "C9 < 0, clamping to 0"); sv->C9 = 0; }
    else if (C9_new > 1) { IIF_warn(__i, "C9 > 1, clamping to 1"); sv->C9 = 1; }
    else {sv->C9 = C9_new;}
    if (CaM_new < 1e-10) { IIF_warn(__i, "CaM < 1e-10, clamping to 1e-10"); sv->CaM = 1e-10; }
    else {sv->CaM = CaM_new;}
    if (Ca_sr_new < 1e-10) { IIF_warn(__i, "Ca_sr < 1e-10, clamping to 1e-10"); sv->Ca_sr = 1e-10; }
    else {sv->Ca_sr = Ca_sr_new;}
    if (Cai_new < 1e-10) { IIF_warn(__i, "Cai < 1e-10, clamping to 1e-10"); sv->Cai = 1e-10; }
    else {sv->Cai = Cai_new;}
    if (Caj_new < 1e-10) { IIF_warn(__i, "Caj < 1e-10, clamping to 1e-10"); sv->Caj = 1e-10; }
    else {sv->Caj = Caj_new;}
    if (Casl_new < 1e-10) { IIF_warn(__i, "Casl < 1e-10, clamping to 1e-10"); sv->Casl = 1e-10; }
    else {sv->Casl = Casl_new;}
    if (Csqnb_new < 1e-10) { IIF_warn(__i, "Csqnb < 1e-10, clamping to 1e-10"); sv->Csqnb = 1e-10; }
    else {sv->Csqnb = Csqnb_new;}
    Iion = Iion;
    sv->Ki = Ki_new;
    if (Myoc_new < 1e-10) { IIF_warn(__i, "Myoc < 1e-10, clamping to 1e-10"); sv->Myoc = 1e-10; }
    else {sv->Myoc = Myoc_new;}
    if (Myom_new < 1e-10) { IIF_warn(__i, "Myom < 1e-10, clamping to 1e-10"); sv->Myom = 1e-10; }
    else {sv->Myom = Myom_new;}
    if (NaBj_new < 1e-10) { IIF_warn(__i, "NaBj < 1e-10, clamping to 1e-10"); sv->NaBj = 1e-10; }
    else {sv->NaBj = NaBj_new;}
    if (NaBsl_new < 1e-10) { IIF_warn(__i, "NaBsl < 1e-10, clamping to 1e-10"); sv->NaBsl = 1e-10; }
    else {sv->NaBsl = NaBsl_new;}
    sv->Nai = Nai_new;
    if (Naj_new < 1e-10) { IIF_warn(__i, "Naj < 1e-10, clamping to 1e-10"); sv->Naj = 1e-10; }
    else {sv->Naj = Naj_new;}
    if (Nasl_new < 1e-10) { IIF_warn(__i, "Nasl < 1e-10, clamping to 1e-10"); sv->Nasl = 1e-10; }
    else {sv->Nasl = Nasl_new;}
    if (O1_new < 0) { IIF_warn(__i, "O1 < 0, clamping to 0"); sv->O1 = 0; }
    else if (O1_new > 1) { IIF_warn(__i, "O1 > 1, clamping to 1"); sv->O1 = 1; }
    else {sv->O1 = O1_new;}
    sv->Q_1 = Q_1_new;
    sv->Q_2 = Q_2_new;
    if (RyRi_new < 0) { IIF_warn(__i, "RyRi < 0, clamping to 0"); sv->RyRi = 0; }
    else if (RyRi_new > 1) { IIF_warn(__i, "RyRi > 1, clamping to 1"); sv->RyRi = 1; }
    else {sv->RyRi = RyRi_new;}
    if (RyRo_new < 0) { IIF_warn(__i, "RyRo < 0, clamping to 0"); sv->RyRo = 0; }
    else if (RyRo_new > 1) { IIF_warn(__i, "RyRo > 1, clamping to 1"); sv->RyRo = 1; }
    else {sv->RyRo = RyRo_new;}
    if (RyRr_new < 0) { IIF_warn(__i, "RyRr < 0, clamping to 0"); sv->RyRr = 0; }
    else if (RyRr_new > 1) { IIF_warn(__i, "RyRr > 1, clamping to 1"); sv->RyRr = 1; }
    else {sv->RyRr = RyRr_new;}
    if (SLHj_new < 1e-10) { IIF_warn(__i, "SLHj < 1e-10, clamping to 1e-10"); sv->SLHj = 1e-10; }
    else {sv->SLHj = SLHj_new;}
    if (SLHsl_new < 1e-10) { IIF_warn(__i, "SLHsl < 1e-10, clamping to 1e-10"); sv->SLHsl = 1e-10; }
    else {sv->SLHsl = SLHsl_new;}
    if (SLLj_new < 1e-10) { IIF_warn(__i, "SLLj < 1e-10, clamping to 1e-10"); sv->SLLj = 1e-10; }
    else {sv->SLLj = SLLj_new;}
    if (SLLsl_new < 1e-10) { IIF_warn(__i, "SLLsl < 1e-10, clamping to 1e-10"); sv->SLLsl = 1e-10; }
    else {sv->SLLsl = SLLsl_new;}
    if (SRB_new < 1e-10) { IIF_warn(__i, "SRB < 1e-10, clamping to 1e-10"); sv->SRB = 1e-10; }
    else {sv->SRB = SRB_new;}
    if (TRPN_new < 0) { IIF_warn(__i, "TRPN < 0, clamping to 0"); sv->TRPN = 0; }
    else if (TRPN_new > 1) { IIF_warn(__i, "TRPN > 1, clamping to 1"); sv->TRPN = 1; }
    else {sv->TRPN = TRPN_new;}
    Tension = Tension;
    if (TnCHc_new < 1e-10) { IIF_warn(__i, "TnCHc < 1e-10, clamping to 1e-10"); sv->TnCHc = 1e-10; }
    else {sv->TnCHc = TnCHc_new;}
    if (TnCHm_new < 1e-10) { IIF_warn(__i, "TnCHm < 1e-10, clamping to 1e-10"); sv->TnCHm = 1e-10; }
    else {sv->TnCHm = TnCHm_new;}
    sv->TnCL = TnCL_new;
    if (d_new < 0) { IIF_warn(__i, "d < 0, clamping to 0"); sv->d = 0; }
    else if (d_new > 1) { IIF_warn(__i, "d > 1, clamping to 1"); sv->d = 1; }
    else {sv->d = d_new;}
    if (f_new < 0) { IIF_warn(__i, "f < 0, clamping to 0"); sv->f = 0; }
    else if (f_new > 1) { IIF_warn(__i, "f > 1, clamping to 1"); sv->f = 1; }
    else {sv->f = f_new;}
    if (fcaBj_new < 0) { IIF_warn(__i, "fcaBj < 0, clamping to 0"); sv->fcaBj = 0; }
    else if (fcaBj_new > 1) { IIF_warn(__i, "fcaBj > 1, clamping to 1"); sv->fcaBj = 1; }
    else {sv->fcaBj = fcaBj_new;}
    if (fcaBsl_new < 0) { IIF_warn(__i, "fcaBsl < 0, clamping to 0"); sv->fcaBsl = 0; }
    else if (fcaBsl_new > 1) { IIF_warn(__i, "fcaBsl > 1, clamping to 1"); sv->fcaBsl = 1; }
    else {sv->fcaBsl = fcaBsl_new;}
    if (h_new < 0) { IIF_warn(__i, "h < 0, clamping to 0"); sv->h = 0; }
    else if (h_new > 1) { IIF_warn(__i, "h > 1, clamping to 1"); sv->h = 1; }
    else {sv->h = h_new;}
    if (j_new < 0) { IIF_warn(__i, "j < 0, clamping to 0"); sv->j = 0; }
    else if (j_new > 1) { IIF_warn(__i, "j > 1, clamping to 1"); sv->j = 1; }
    else {sv->j = j_new;}
    if (m_new < 0) { IIF_warn(__i, "m < 0, clamping to 0"); sv->m = 0; }
    else if (m_new > 1) { IIF_warn(__i, "m > 1, clamping to 1"); sv->m = 1; }
    else {sv->m = m_new;}
    sv->xb = xb_new;
    if (xkr_new < 0) { IIF_warn(__i, "xkr < 0, clamping to 0"); sv->xkr = 0; }
    else if (xkr_new > 1) { IIF_warn(__i, "xkr > 1, clamping to 1"); sv->xkr = 1; }
    else {sv->xkr = xkr_new;}
    sv->xks = xks_new;
    if (xtof_new < 0) { IIF_warn(__i, "xtof < 0, clamping to 0"); sv->xtof = 0; }
    else if (xtof_new > 1) { IIF_warn(__i, "xtof > 1, clamping to 1"); sv->xtof = 1; }
    else {sv->xtof = xtof_new;}
    if (xtos_new < 0) { IIF_warn(__i, "xtos < 0, clamping to 0"); sv->xtos = 0; }
    else if (xtos_new > 1) { IIF_warn(__i, "xtos > 1, clamping to 1"); sv->xtos = 1; }
    else {sv->xtos = xtos_new;}
    if (ytof_new < 0) { IIF_warn(__i, "ytof < 0, clamping to 0"); sv->ytof = 0; }
    else if (ytof_new > 1) { IIF_warn(__i, "ytof > 1, clamping to 1"); sv->ytof = 1; }
    else {sv->ytof = ytof_new;}
    if (ytos_new < 0) { IIF_warn(__i, "ytos < 0, clamping to 0"); sv->ytos = 0; }
    else if (ytos_new > 1) { IIF_warn(__i, "ytos > 1, clamping to 1"); sv->ytos = 1; }
    else {sv->ytos = ytos_new;}
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Tension_ext[__i] = Tension;
    V_ext[__i] = V;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        