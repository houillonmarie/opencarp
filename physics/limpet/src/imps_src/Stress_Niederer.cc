// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Steven Niederer, Peter Hunter, Nicholas Smith
*  Year: 2006
*  Title: A Quantitative Analysis of Cardiac Myocyte Relaxation: A Simulation Study
*  Journal: Biophysical Journal 2006;90:1697-1722
*  DOI: 10.1529/biophysj.105.069534
*  Comment: Rat ventricular active stress model (plugin)
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Stress_Niederer.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Stress_Niederer(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Stress_Niederer( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define Q_1_init (GlobalData_t)(0.)
#define Q_2_init (GlobalData_t)(0.)
#define Q_3_init (GlobalData_t)(0.)
#define TRPN_init (GlobalData_t)(0.067593139865)
#define dExtensionRatiodt (GlobalData_t)(0.)
#define delta_sl_init (GlobalData_t)(0.)
#define length_init (GlobalData_t)(1.)
#define partial_A_1_del_Q_1 (GlobalData_t)(0.)
#define partial_A_2_del_Q_2 (GlobalData_t)(0.)
#define partial_A_3_del_Q_3 (GlobalData_t)(0.)
#define partial_Ca_50_del_TRPN (GlobalData_t)(0.)
#define partial_Ca_50_del_z (GlobalData_t)(0.)
#define partial_Ca_50ref_del_TRPN (GlobalData_t)(0.)
#define partial_Ca_50ref_del_z (GlobalData_t)(0.)
#define partial_Ca_TRPN_50_del_TRPN (GlobalData_t)(0.)
#define partial_Ca_TRPN_50_del_z (GlobalData_t)(0.)
#define partial_Ca_TRPN_Max_del_TRPN (GlobalData_t)(0.)
#define partial_Ca_TRPN_Max_del_z (GlobalData_t)(0.)
#define partial_Ca_b_del_z (GlobalData_t)(0.)
#define partial_Ca_i_mM_del_TRPN (GlobalData_t)(0.)
#define partial_K_1_del_TRPN (GlobalData_t)(0.)
#define partial_K_2_del_TRPN (GlobalData_t)(0.)
#define partial_K_z_del_TRPN (GlobalData_t)(0.)
#define partial_K_z_del_z (GlobalData_t)(0.)
#define partial_Q_del_TRPN (GlobalData_t)(0.)
#define partial_T_0_del_TRPN (GlobalData_t)(0.)
#define partial_T_Base_del_TRPN (GlobalData_t)(0.)
#define partial_T_ref_del_TRPN (GlobalData_t)(0.)
#define partial_Tension_del_TRPN (GlobalData_t)(0.)
#define partial_a_del_TRPN (GlobalData_t)(0.)
#define partial_alpha_0_del_TRPN (GlobalData_t)(0.)
#define partial_alpha_0_del_z (GlobalData_t)(0.)
#define partial_alpha_1_del_Q_1 (GlobalData_t)(0.)
#define partial_alpha_2_del_Q_2 (GlobalData_t)(0.)
#define partial_alpha_3_del_Q_3 (GlobalData_t)(0.)
#define partial_alpha_Tm_del_z (GlobalData_t)(0.)
#define partial_alpha_r1_del_TRPN (GlobalData_t)(0.)
#define partial_alpha_r1_del_z (GlobalData_t)(0.)
#define partial_alpha_r2_del_TRPN (GlobalData_t)(0.)
#define partial_alpha_r2_del_z (GlobalData_t)(0.)
#define partial_beta_0_del_TRPN (GlobalData_t)(0.)
#define partial_beta_0_del_z (GlobalData_t)(0.)
#define partial_beta_1_del_TRPN (GlobalData_t)(0.)
#define partial_beta_1_del_z (GlobalData_t)(0.)
#define partial_dExtensionRatiodt_del_Q_1 (GlobalData_t)(0.)
#define partial_dExtensionRatiodt_del_Q_2 (GlobalData_t)(0.)
#define partial_dExtensionRatiodt_del_Q_3 (GlobalData_t)(0.)
#define partial_gamma_trpn_del_TRPN (GlobalData_t)(0.)
#define partial_gamma_trpn_del_z (GlobalData_t)(0.)
#define partial_k_Ref_off_del_TRPN (GlobalData_t)(0.)
#define partial_k_Ref_off_del_z (GlobalData_t)(0.)
#define partial_k_off_del_TRPN (GlobalData_t)(0.)
#define partial_k_on_del_TRPN (GlobalData_t)(0.)
#define partial_k_on_del_z (GlobalData_t)(0.)
#define partial_lambda_del_TRPN (GlobalData_t)(0.)
#define partial_lambda_del_z (GlobalData_t)(0.)
#define partial_n_Hill_del_TRPN (GlobalData_t)(0.)
#define partial_n_Hill_del_z (GlobalData_t)(0.)
#define partial_n_Rel_del_TRPN (GlobalData_t)(0.)
#define partial_n_Rel_del_z (GlobalData_t)(0.)
#define partial_overlap_del_TRPN (GlobalData_t)(0.)
#define partial_z_max_del_TRPN (GlobalData_t)(0.)
#define partial_z_p_del_TRPN (GlobalData_t)(0.)
#define set_Q_1_tozero_in_A_1 (GlobalData_t)(-29.)
#define set_Q_1_tozero_in_alpha_1 (GlobalData_t)(0.03)
#define set_Q_1_tozero_in_dExtensionRatiodt (GlobalData_t)(0.)
#define set_Q_1_tozero_in_diff_Q_1 (GlobalData_t)(0.)
#define set_Q_2_tozero_in_A_2 (GlobalData_t)(138.)
#define set_Q_2_tozero_in_alpha_2 (GlobalData_t)(0.13)
#define set_Q_2_tozero_in_dExtensionRatiodt (GlobalData_t)(0.)
#define set_Q_2_tozero_in_diff_Q_2 (GlobalData_t)(0.)
#define set_Q_3_tozero_in_A_3 (GlobalData_t)(129.)
#define set_Q_3_tozero_in_alpha_3 (GlobalData_t)(0.625)
#define set_Q_3_tozero_in_dExtensionRatiodt (GlobalData_t)(0.)
#define set_Q_3_tozero_in_diff_Q_3 (GlobalData_t)(0.)
#define set_TRPN_tozero_in_Ca_50ref (GlobalData_t)(1.05e-3)
#define set_TRPN_tozero_in_Ca_TRPN_Max (GlobalData_t)(70e-3)
#define set_TRPN_tozero_in_K_1 (GlobalData_t)(((((1.75e-3*(pow(0.85,(3.-(1.)))))*3.)*(pow(0.15,3.)))/(((pow(0.85,3.))+(pow(0.15,3.)))*((pow(0.85,3.))+(pow(0.15,3.))))))
#define set_TRPN_tozero_in_K_2 (GlobalData_t)((((1.75e-3*(pow(0.85,3.)))/((pow(0.85,3.))+(pow(0.15,3.))))*(1.-(((3.*(pow(0.15,3.)))/((pow(0.85,3.))+(pow(0.15,3.))))))))
#define set_TRPN_tozero_in_K_z (GlobalData_t)(0.15)
#define set_TRPN_tozero_in_T_ref (GlobalData_t)(56.2)
#define set_TRPN_tozero_in_a (GlobalData_t)(0.35)
#define set_TRPN_tozero_in_alpha_0 (GlobalData_t)(8e-3)
#define set_TRPN_tozero_in_alpha_r1 (GlobalData_t)(2e-3)
#define set_TRPN_tozero_in_alpha_r2 (GlobalData_t)(1.75e-3)
#define set_TRPN_tozero_in_beta_0 (GlobalData_t)(4.9)
#define set_TRPN_tozero_in_beta_1 (GlobalData_t)(-4.)
#define set_TRPN_tozero_in_gamma_trpn (GlobalData_t)(2.)
#define set_TRPN_tozero_in_k_Ref_off (GlobalData_t)(0.2)
#define set_TRPN_tozero_in_k_on (GlobalData_t)(100.)
#define set_TRPN_tozero_in_n_Hill (GlobalData_t)(3.)
#define set_TRPN_tozero_in_n_Rel (GlobalData_t)(3.)
#define set_TRPN_tozero_in_z_p (GlobalData_t)(0.85)
#define z_init (GlobalData_t)(0.014417937837)
#define z_rush_larsen_B (GlobalData_t)(1.)



void initialize_params_Stress_Niederer( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Stress_Niederer_Params *p = (Stress_Niederer_Params *)IF->params;

  // Compute the regional constants
  {
    p->A_1 = -29.;
    p->A_2 = 138.;
    p->A_3 = 129.;
    p->Ca_50ref = 1.05e-3;
    p->Ca_TRPN_Max = 70e-3;
    p->K_z = 0.15;
    p->T_ref = 56.2;
    p->a = 0.35;
    p->alpha_0 = 8e-3;
    p->alpha_1 = 0.03;
    p->alpha_2 = 0.13;
    p->alpha_3 = 0.625;
    p->alpha_r1 = 2e-3;
    p->alpha_r2 = 1.75e-3;
    p->beta_0 = 4.9;
    p->beta_1 = -4.;
    p->gamma_trpn = 2.;
    p->k_Ref_off = 0.2;
    p->k_on = 100.;
    p->n_Hill = 3.;
    p->n_Rel = 3.;
    p->z_p = 0.85;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Stress_Niederer( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Niederer_Params *p = (Stress_Niederer_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double K_1 = ((((p->alpha_r2*(pow(p->z_p,(p->n_Rel-(1.)))))*p->n_Rel)*(pow(p->K_z,p->n_Rel)))/(((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))*((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))));
  double K_2 = (((p->alpha_r2*(pow(p->z_p,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel))))*(1.-(((p->n_Rel*(pow(p->K_z,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))))));
  double partial_diff_Q_1_del_Q_1 = (-p->alpha_1);
  double partial_diff_Q_2_del_Q_2 = (-p->alpha_2);
  double partial_diff_Q_3_del_Q_3 = (-p->alpha_3);
  double Q_1_rush_larsen_A = ((set_Q_1_tozero_in_diff_Q_1/partial_diff_Q_1_del_Q_1)*(expm1((dt*partial_diff_Q_1_del_Q_1))));
  double Q_1_rush_larsen_B = (exp((dt*partial_diff_Q_1_del_Q_1)));
  double Q_2_rush_larsen_A = ((set_Q_2_tozero_in_diff_Q_2/partial_diff_Q_2_del_Q_2)*(expm1((dt*partial_diff_Q_2_del_Q_2))));
  double Q_2_rush_larsen_B = (exp((dt*partial_diff_Q_2_del_Q_2)));
  double Q_3_rush_larsen_A = ((set_Q_3_tozero_in_diff_Q_3/partial_diff_Q_3_del_Q_3)*(expm1((dt*partial_diff_Q_3_del_Q_3))));
  double Q_3_rush_larsen_B = (exp((dt*partial_diff_Q_3_del_Q_3)));

}



void    initialize_sv_Stress_Niederer( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Niederer_Params *p = (Stress_Niederer_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Stress_Niederer_state) );
  Stress_Niederer_state *sv_base = (Stress_Niederer_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double K_1 = ((((p->alpha_r2*(pow(p->z_p,(p->n_Rel-(1.)))))*p->n_Rel)*(pow(p->K_z,p->n_Rel)))/(((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))*((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))));
  double K_2 = (((p->alpha_r2*(pow(p->z_p,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel))))*(1.-(((p->n_Rel*(pow(p->K_z,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))))));
  double partial_diff_Q_1_del_Q_1 = (-p->alpha_1);
  double partial_diff_Q_2_del_Q_2 = (-p->alpha_2);
  double partial_diff_Q_3_del_Q_3 = (-p->alpha_3);
  double Q_1_rush_larsen_A = ((set_Q_1_tozero_in_diff_Q_1/partial_diff_Q_1_del_Q_1)*(expm1((dt*partial_diff_Q_1_del_Q_1))));
  double Q_1_rush_larsen_B = (exp((dt*partial_diff_Q_1_del_Q_1)));
  double Q_2_rush_larsen_A = ((set_Q_2_tozero_in_diff_Q_2/partial_diff_Q_2_del_Q_2)*(expm1((dt*partial_diff_Q_2_del_Q_2))));
  double Q_2_rush_larsen_B = (exp((dt*partial_diff_Q_2_del_Q_2)));
  double Q_3_rush_larsen_A = ((set_Q_3_tozero_in_diff_Q_3/partial_diff_Q_3_del_Q_3)*(expm1((dt*partial_diff_Q_3_del_Q_3))));
  double Q_3_rush_larsen_B = (exp((dt*partial_diff_Q_3_del_Q_3)));
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Ca_i_sizeof;
  int __Ca_i_offset;
  SVgetfcn __Ca_i_SVgetfcn = get_sv_offset( IF->parent->type, "Ca_i", &__Ca_i_offset, &__Ca_i_sizeof );
  SVputfcn __Ca_i_SVputfcn = __Ca_i_SVgetfcn ? getPutSV(__Ca_i_SVgetfcn) : NULL;

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Stress_Niederer_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Ca_i = __Ca_i_SVgetfcn ? __Ca_i_SVgetfcn(IF->parent, __i, __Ca_i_offset) :sv->__Ca_i_local;
    //Change the units of external variables as appropriate.
    Tension *= 1e-3;
    
    
    // Initialize the rest of the nodal variables
    sv->Q_1 = Q_1_init;
    sv->Q_2 = Q_2_init;
    sv->Q_3 = Q_3_init;
    sv->TRPN = TRPN_init;
    delta_sl = delta_sl_init;
    length = length_init;
    sv->z = z_init;
    //Change the units of external variables as appropriate.
    Tension *= 1e3;
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Ca_i_SVputfcn ) 
    	__Ca_i_SVputfcn(IF->parent, __i, __Ca_i_offset, Ca_i);
    else
    	sv->__Ca_i_local=Ca_i;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Stress_Niederer(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Niederer_Params *p  = (Stress_Niederer_Params *)IF->params;
  Stress_Niederer_state *sv_base = (Stress_Niederer_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t K_1 = ((((p->alpha_r2*(pow(p->z_p,(p->n_Rel-(1.)))))*p->n_Rel)*(pow(p->K_z,p->n_Rel)))/(((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))*((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))));
  GlobalData_t K_2 = (((p->alpha_r2*(pow(p->z_p,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel))))*(1.-(((p->n_Rel*(pow(p->K_z,p->n_Rel)))/((pow(p->z_p,p->n_Rel))+(pow(p->K_z,p->n_Rel)))))));
  GlobalData_t partial_diff_Q_1_del_Q_1 = (-p->alpha_1);
  GlobalData_t partial_diff_Q_2_del_Q_2 = (-p->alpha_2);
  GlobalData_t partial_diff_Q_3_del_Q_3 = (-p->alpha_3);
  GlobalData_t Q_1_rush_larsen_A = ((set_Q_1_tozero_in_diff_Q_1/partial_diff_Q_1_del_Q_1)*(expm1((dt*partial_diff_Q_1_del_Q_1))));
  GlobalData_t Q_1_rush_larsen_B = (exp((dt*partial_diff_Q_1_del_Q_1)));
  GlobalData_t Q_2_rush_larsen_A = ((set_Q_2_tozero_in_diff_Q_2/partial_diff_Q_2_del_Q_2)*(expm1((dt*partial_diff_Q_2_del_Q_2))));
  GlobalData_t Q_2_rush_larsen_B = (exp((dt*partial_diff_Q_2_del_Q_2)));
  GlobalData_t Q_3_rush_larsen_A = ((set_Q_3_tozero_in_diff_Q_3/partial_diff_Q_3_del_Q_3)*(expm1((dt*partial_diff_Q_3_del_Q_3))));
  GlobalData_t Q_3_rush_larsen_B = (exp((dt*partial_diff_Q_3_del_Q_3)));
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Ca_i_sizeof;
  int __Ca_i_offset;
  SVgetfcn __Ca_i_SVgetfcn = get_sv_offset( IF->parent->type, "Ca_i", &__Ca_i_offset, &__Ca_i_sizeof );
  SVputfcn __Ca_i_SVputfcn = __Ca_i_SVgetfcn ? getPutSV(__Ca_i_SVgetfcn) : NULL;

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Stress_Niederer_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Ca_i = __Ca_i_SVgetfcn ? __Ca_i_SVgetfcn(IF->parent, __i, __Ca_i_offset) :sv->__Ca_i_local;
    //Change the units of external variables as appropriate.
    Tension *= 1e-3;
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t Q = ((sv->Q_1+sv->Q_2)+sv->Q_3);
    GlobalData_t lambda = length;
    GlobalData_t Ca_50 = (p->Ca_50ref*(1.+(p->beta_1*(lambda-(1.)))));
    GlobalData_t overlap = (1.+(p->beta_0*(lambda-(1.))));
    GlobalData_t Ca_TRPN_50 = ((Ca_50*p->Ca_TRPN_Max)/(Ca_50+((p->k_Ref_off/p->k_on)*(1.-((((1.+(p->beta_0*(lambda-(1.))))*0.5)/p->gamma_trpn))))));
    GlobalData_t z_max = (((p->alpha_0/(pow((Ca_TRPN_50/p->Ca_TRPN_Max),p->n_Hill)))-(K_2))/((p->alpha_r1+K_1)+(p->alpha_0/(pow((Ca_TRPN_50/p->Ca_TRPN_Max),p->n_Hill)))));
    GlobalData_t T_Base = ((p->T_ref*sv->z)/z_max);
    GlobalData_t T_0 = (T_Base*overlap);
    Tension = ((Q<0.) ? ((T_0*((p->a*Q)+1.))/(1.-(Q))) : ((T_0*(1.+((p->a+2.)*Q)))/(1.+Q)));
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    GlobalData_t Ca_b = (p->Ca_TRPN_Max-(sv->TRPN));
    GlobalData_t Ca_i_mM = (Ca_i/1000.);
    GlobalData_t beta_Tm = (p->alpha_r1+((p->alpha_r2*(pow(sv->z,(p->n_Rel-(1.)))))/((pow(sv->z,p->n_Rel))+(pow(p->K_z,p->n_Rel)))));
    GlobalData_t k_off = (((1.-((Tension/(p->gamma_trpn*p->T_ref))))>0.1) ? (p->k_Ref_off*(1.-((Tension/(p->gamma_trpn*p->T_ref))))) : (p->k_Ref_off*0.1));
    GlobalData_t set_TRPN_tozero_in_diff_TRPN = (70e-3*(((1.-((((((sv->Q_1+sv->Q_2)+sv->Q_3)<0.) ? (((((56.2*sv->z)/(((8e-3/(pow(((((1.05e-3*(1.+(-4.*(length-(1.)))))*70e-3)/((1.05e-3*(1.+(-4.*(length-(1.)))))+((0.2/100.)*(1.-((((1.+(4.9*(length-(1.))))*0.5)/2.))))))/70e-3),3.)))-((((1.75e-3*(pow(0.85,3.)))/((pow(0.85,3.))+(pow(0.15,3.))))*(1.-(((3.*(pow(0.15,3.)))/((pow(0.85,3.))+(pow(0.15,3.)))))))))/((2e-3+((((1.75e-3*(pow(0.85,(3.-(1.)))))*3.)*(pow(0.15,3.)))/(((pow(0.85,3.))+(pow(0.15,3.)))*((pow(0.85,3.))+(pow(0.15,3.))))))+(8e-3/(pow(((((1.05e-3*(1.+(-4.*(length-(1.)))))*70e-3)/((1.05e-3*(1.+(-4.*(length-(1.)))))+((0.2/100.)*(1.-((((1.+(4.9*(length-(1.))))*0.5)/2.))))))/70e-3),3.))))))*(1.+(4.9*(length-(1.)))))*((0.35*((sv->Q_1+sv->Q_2)+sv->Q_3))+1.))/(1.-(((sv->Q_1+sv->Q_2)+sv->Q_3)))) : (((((56.2*sv->z)/(((8e-3/(pow(((((1.05e-3*(1.+(-4.*(length-(1.)))))*70e-3)/((1.05e-3*(1.+(-4.*(length-(1.)))))+((0.2/100.)*(1.-((((1.+(4.9*(length-(1.))))*0.5)/2.))))))/70e-3),3.)))-((((1.75e-3*(pow(0.85,3.)))/((pow(0.85,3.))+(pow(0.15,3.))))*(1.-(((3.*(pow(0.15,3.)))/((pow(0.85,3.))+(pow(0.15,3.)))))))))/((2e-3+((((1.75e-3*(pow(0.85,(3.-(1.)))))*3.)*(pow(0.15,3.)))/(((pow(0.85,3.))+(pow(0.15,3.)))*((pow(0.85,3.))+(pow(0.15,3.))))))+(8e-3/(pow(((((1.05e-3*(1.+(-4.*(length-(1.)))))*70e-3)/((1.05e-3*(1.+(-4.*(length-(1.)))))+((0.2/100.)*(1.-((((1.+(4.9*(length-(1.))))*0.5)/2.))))))/70e-3),3.))))))*(1.+(4.9*(length-(1.)))))*(1.+((0.35+2.)*((sv->Q_1+sv->Q_2)+sv->Q_3))))/(1.+((sv->Q_1+sv->Q_2)+sv->Q_3))))/(2.*56.2))))>0.1) ? (0.2*(1.-((((((sv->Q_1+sv->Q_2)+sv->Q_3)<0.) ? (((((56.2*sv->z)/(((8e-3/(pow(((((1.05e-3*(1.+(-4.*(length-(1.)))))*70e-3)/((1.05e-3*(1.+(-4.*(length-(1.)))))+((0.2/100.)*(1.-((((1.+(4.9*(length-(1.))))*0.5)/2.))))))/70e-3),3.)))-((((1.75e-3*(pow(0.85,3.)))/((pow(0.85,3.))+(pow(0.15,3.))))*(1.-(((3.*(pow(0.15,3.)))/((pow(0.85,3.))+(pow(0.15,3.)))))))))/((2e-3+((((1.75e-3*(pow(0.85,(3.-(1.)))))*3.)*(pow(0.15,3.)))/(((pow(0.85,3.))+(pow(0.15,3.)))*((pow(0.85,3.))+(pow(0.15,3.))))))+(8e-3/(pow(((((1.05e-3*(1.+(-4.*(length-(1.)))))*70e-3)/((1.05e-3*(1.+(-4.*(length-(1.)))))+((0.2/100.)*(1.-((((1.+(4.9*(length-(1.))))*0.5)/2.))))))/70e-3),3.))))))*(1.+(4.9*(length-(1.)))))*((0.35*((sv->Q_1+sv->Q_2)+sv->Q_3))+1.))/(1.-(((sv->Q_1+sv->Q_2)+sv->Q_3)))) : (((((56.2*sv->z)/(((8e-3/(pow(((((1.05e-3*(1.+(-4.*(length-(1.)))))*70e-3)/((1.05e-3*(1.+(-4.*(length-(1.)))))+((0.2/100.)*(1.-((((1.+(4.9*(length-(1.))))*0.5)/2.))))))/70e-3),3.)))-((((1.75e-3*(pow(0.85,3.)))/((pow(0.85,3.))+(pow(0.15,3.))))*(1.-(((3.*(pow(0.15,3.)))/((pow(0.85,3.))+(pow(0.15,3.)))))))))/((2e-3+((((1.75e-3*(pow(0.85,(3.-(1.)))))*3.)*(pow(0.15,3.)))/(((pow(0.85,3.))+(pow(0.15,3.)))*((pow(0.85,3.))+(pow(0.15,3.))))))+(8e-3/(pow(((((1.05e-3*(1.+(-4.*(length-(1.)))))*70e-3)/((1.05e-3*(1.+(-4.*(length-(1.)))))+((0.2/100.)*(1.-((((1.+(4.9*(length-(1.))))*0.5)/2.))))))/70e-3),3.))))))*(1.+(4.9*(length-(1.)))))*(1.+((0.35+2.)*((sv->Q_1+sv->Q_2)+sv->Q_3))))/(1.+((sv->Q_1+sv->Q_2)+sv->Q_3))))/(2.*56.2))))) : (0.2*0.1)));
    GlobalData_t alpha_Tm = (p->alpha_0*(pow((Ca_b/Ca_TRPN_50),p->n_Hill)));
    GlobalData_t partial_diff_TRPN_del_TRPN = ((-1.*k_off)-((Ca_i_mM*p->k_on)));
    GlobalData_t TRPN_rush_larsen_A = ((set_TRPN_tozero_in_diff_TRPN/partial_diff_TRPN_del_TRPN)*(expm1((dt*partial_diff_TRPN_del_TRPN))));
    GlobalData_t TRPN_rush_larsen_B = (exp((dt*partial_diff_TRPN_del_TRPN)));
    GlobalData_t diff_z = ((alpha_Tm*(1.-(sv->z)))-((beta_Tm*sv->z)));
    GlobalData_t partial_diff_z_del_z = ((alpha_Tm*-1.)-((((((((pow(sv->z,p->n_Rel))+(pow(p->K_z,p->n_Rel)))*(p->alpha_r2*((p->n_Rel-(1.))*(pow(sv->z,((p->n_Rel-(1.))-(1.)))))))-(((p->alpha_r2*(pow(sv->z,(p->n_Rel-(1.)))))*(p->n_Rel*(pow(sv->z,(p->n_Rel-(1.))))))))/(((pow(sv->z,p->n_Rel))+(pow(p->K_z,p->n_Rel)))*((pow(sv->z,p->n_Rel))+(pow(p->K_z,p->n_Rel)))))*sv->z)+beta_Tm)));
    GlobalData_t z_rush_larsen_A = ((expm1((partial_diff_z_del_z*dt)))*(diff_z/partial_diff_z_del_z));
    GlobalData_t Q_1_new = Q_1_rush_larsen_A+Q_1_rush_larsen_B*sv->Q_1;
    GlobalData_t Q_2_new = Q_2_rush_larsen_A+Q_2_rush_larsen_B*sv->Q_2;
    GlobalData_t Q_3_new = Q_3_rush_larsen_A+Q_3_rush_larsen_B*sv->Q_3;
    GlobalData_t TRPN_new = TRPN_rush_larsen_A+TRPN_rush_larsen_B*sv->TRPN;
    GlobalData_t z_new = z_rush_larsen_A+z_rush_larsen_B*sv->z;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Q_1 = Q_1_new;
    sv->Q_2 = Q_2_new;
    sv->Q_3 = Q_3_new;
    sv->TRPN = TRPN_new;
    Tension = Tension;
    sv->z = z_new;
    //Change the units of external variables as appropriate.
    Tension *= 1e3;
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Ca_i_SVputfcn ) 
    	__Ca_i_SVputfcn(IF->parent, __i, __Ca_i_offset, Ca_i);
    else
    	sv->__Ca_i_local=Ca_i;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        