// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Rice JJ, Winslow RL, Hunter WC
*  Year: 1999
*  Title: Comparison of putative cooperative mechanisms in cardiac muscle: length dependence and dynamic responses
*  Journal: Am J Physiol., 276(5):H1734-54
*  DOI: 10.1152/ajpheart.1999.276.5.H1734
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Stress_Rice.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Stress_Rice(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Stress_Rice( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define Actkoff (GlobalData_t)(0.065536e-2)
#define Actkon (GlobalData_t)(1.0e-2)
#define Catau_SL_cytosol (GlobalData_t)((1.2205e11*2.2))
#define Catau_junction_SL (GlobalData_t)((1.0921e13/9.0))
#define ChrisKm (GlobalData_t)(0.00025)
#define CytosolReductionFactor (GlobalData_t)(1.0)
#define Feedback_factor (GlobalData_t)(1.5)
#define I (GlobalData_t)(1.024274e-7)
#define ICl_KdCa (GlobalData_t)(100.0)
#define INaCaXCa_Kmf (GlobalData_t)(3.59e-3)
#define INaCaXCa_Kmr (GlobalData_t)(1.3)
#define INaCaXNa_Kmf (GlobalData_t)(12.29)
#define INaCaXNa_Kmr (GlobalData_t)(1.0)
#define INaCaX_Hill (GlobalData_t)(3.0)
#define INaK_Hill (GlobalData_t)(4.0)
#define INaK_Kmr (GlobalData_t)(1.5)
#define ISLCaP_Hill (GlobalData_t)(1.6)
#define ISRCaP_Hill (GlobalData_t)(1.787)
#define ISRCaP_k_leak (GlobalData_t)(0.00047)
#define JunctionReductionFactor (GlobalData_t)(0.1)
#define KSE (GlobalData_t)(1.)
#define KSat (GlobalData_t)(0.27)
#define KdAct (GlobalData_t)(0.256)
#define KmNSCa (GlobalData_t)(1.2)
#define LslackSE (GlobalData_t)(2.2)
#define N_NoXB (GlobalData_t)(0.99)
#define N_init (GlobalData_t)(0.97)
#define Natau_SL_cytosol (GlobalData_t)((6.1025e9*100.))
#define Natau_junction_SL (GlobalData_t)((5.4605e11*100.))
#define Nue (GlobalData_t)(0.35)
#define O (GlobalData_t)(8.156628e-7)
#define PCa (GlobalData_t)(5.4e-4)
#define PCon_col (GlobalData_t)(0.02)
#define PCon_t (GlobalData_t)(0.002)
#define PExp_col (GlobalData_t)(70.)
#define PExp_t (GlobalData_t)(10.)
#define PK (GlobalData_t)(2.7e-7)
#define PNSCa (GlobalData_t)(0.0)
#define PNa (GlobalData_t)(1.5e-8)
#define PNaK (GlobalData_t)(0.01833)
#define P_NoXB (GlobalData_t)(0.01)
#define P_init (GlobalData_t)(0.01)
#define Qfapp (GlobalData_t)(6.25)
#define Qgapp (GlobalData_t)(2.5)
#define Qgxb (GlobalData_t)(6.25)
#define Qhb (GlobalData_t)(6.25)
#define Qhf (GlobalData_t)(6.25)
#define Qkn_p (GlobalData_t)(1.6)
#define Qkoff (GlobalData_t)(1.3)
#define Qkon (GlobalData_t)(1.5)
#define Qkp_n (GlobalData_t)(1.6)
#define R (GlobalData_t)(8.884332e-1)
#define RI (GlobalData_t)(1.115656e-1)
#define RTF (GlobalData_t)(((8314.0*(273.+37.))/96500.0))
#define RTOL (GlobalData_t)(1e-4)
#define Ratio1 (GlobalData_t)(1205.9)
#define Ratio2 (GlobalData_t)(32.5)
#define Ratio3 (GlobalData_t)(18.57)
#define SLReductionFactor (GlobalData_t)(1.0)
#define SLcol (GlobalData_t)(2.25)
#define SLrest (GlobalData_t)(1.9)
#define SLset (GlobalData_t)(2.3)
#define TempCelsius (GlobalData_t)(37.)
#define TnCaH_init (GlobalData_t)(0.)
#define TnCaL_init (GlobalData_t)(0.)
#define TropCaH (GlobalData_t)(0.2320947)
#define TropCaL (GlobalData_t)(0.01447254)
#define Trop_conc (GlobalData_t)(70.0)
#define V_init (GlobalData_t)(-85.56885)
#define XBpostr_init (GlobalData_t)(0.01)
#define XBprer_init (GlobalData_t)(0.01)
#define delta_sl_init (GlobalData_t)(0.)
#define dl_init (GlobalData_t)(0.)
#define fCaKd (GlobalData_t)(7.0)
#define fCaKoff (GlobalData_t)((10.0*1.19e-3))
#define fCaKon (GlobalData_t)((10.0*0.17e-3))
#define gammaCaiJunction (GlobalData_t)(0.341)
#define gammaCaiSL (GlobalData_t)(0.341)
#define gammaCao (GlobalData_t)(0.341)
#define gammaKi (GlobalData_t)(0.75)
#define gammaKo (GlobalData_t)(0.75)
#define gammaNai (GlobalData_t)(0.75)
#define gammaNao (GlobalData_t)(0.75)
#define gxbT_3D (GlobalData_t)(0.005)
#define gxmdc (GlobalData_t)(60.0)
#define hbmdc (GlobalData_t)(0.)
#define hfmd (GlobalData_t)(1.)
#define hfmdc (GlobalData_t)(5.)
#define koffH (GlobalData_t)((25.e-3/12.0))
#define koffL (GlobalData_t)((250.e-3/12.0))
#define kon (GlobalData_t)((50.e-3/5.0))
#define len_hbare (GlobalData_t)(0.1)
#define len_thick (GlobalData_t)(1.65)
#define len_thin (GlobalData_t)(1.2)
#define length_init (GlobalData_t)(1.)
#define main_k (GlobalData_t)((6.5*0.8))
#define nperm (GlobalData_t)(15.0)
#define outputVoltage (GlobalData_t)(0.)
#define pi (GlobalData_t)(3.14159)
#define scl (GlobalData_t)((160.*7.))
#define sigman (GlobalData_t)(1.0)
#define sigmap (GlobalData_t)(1600.)
#define visc (GlobalData_t)(0.003e3)
#define visco (GlobalData_t)(100.)
#define xPsi (GlobalData_t)(2.)
#define xPsi_mode (GlobalData_t)(1.)
#define xXBprer_init (GlobalData_t)(0.0)
#define x_0 (GlobalData_t)(0.01)
#define xbm (GlobalData_t)(0.1)
#define xbmod (GlobalData_t)(4.)
#define zCa (GlobalData_t)(2.0)
#define zK (GlobalData_t)(1.0)
#define zNa (GlobalData_t)(1.0)
#define TempKelvin (GlobalData_t)((273.0+TempCelsius))
#define fapp (GlobalData_t)(((500.e-3*xbmod)*xbm))
#define gapp (GlobalData_t)(((5000.0e-3*xbmod)*xbm))
#define gappT_term (GlobalData_t)((pow(Qgapp,((TempCelsius-(37.))/10.))))
#define gxb (GlobalData_t)(((70.e-3*xbmod)*xbm))
#define gxbT_term (GlobalData_t)((pow(Qgxb,((TempCelsius-(37.))/10.))))
#define hb (GlobalData_t)(((5.0e-3*xbmod)*xbm))
#define hf (GlobalData_t)(((20.0e-3*xbmod)*xbm))
#define kn_p (GlobalData_t)(((500e-3*xbmod)/10.0))
#define kn_pT_term (GlobalData_t)((pow(Qkn_p,((TempCelsius-(37.))/10.))))
#define koffHT (GlobalData_t)((koffH*(pow(Qkoff,((TempCelsius-(37.))/10.)))))
#define koffLT (GlobalData_t)((koffL*(pow(Qkoff,((TempCelsius-(37.))/10.)))))
#define konT (GlobalData_t)((kon*(pow(Qkon,((TempCelsius-(37.))/10.)))))
#define kp_n (GlobalData_t)((((50e-3*xbmod)/10.0)/6.0))
#define kp_nT_term (GlobalData_t)((pow(Qkp_n,((TempCelsius-(37.))/10.))))
#define kxb (GlobalData_t)((70.*main_k))
#define xXBpostr_init (GlobalData_t)(x_0)
#define SSXBpostr (GlobalData_t)(((fapp*hf)/((((((gxb*hf)+(fapp*hf))+(gxb*gapp))+(hb*fapp))+(hb*gapp))+(gxb*fapp))))
#define SSXBprer (GlobalData_t)((((hb*fapp)+(gxb*fapp))/((((((gxb*hf)+(fapp*hf))+(gxb*gapp))+(hb*fapp))+(hb*gapp))+(gxb*fapp))))
#define fappT (GlobalData_t)((fapp*(pow(Qfapp,((TempCelsius-(37.))/10.)))))
#define hbT (GlobalData_t)((hb*(pow(Qhb,((TempCelsius-(37.))/10.)))))
#define hfT (GlobalData_t)(((hf*hfmd)*(pow(Qhf,((TempCelsius-(37.))/10.)))))



void initialize_params_Stress_Rice( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Stress_Rice_Params *p = (Stress_Rice_Params *)IF->params;

  // Compute the regional constants
  {
    p->SLmax = 2.4;
    p->SLmin = 1.4;
    p->force_coeff = ((1.0/x_0)/SSXBpostr);
  }
  // Compute the regional initialization
  {
    p->failing = 0.;
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Stress_Rice( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Rice_Params *p = (Stress_Rice_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.

}



void    initialize_sv_Stress_Rice( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Rice_Params *p = (Stress_Rice_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Stress_Rice_state) );
  Stress_Rice_state *sv_base = (Stress_Rice_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Ca_i_sizeof;
  int __Ca_i_offset;
  SVgetfcn __Ca_i_SVgetfcn = get_sv_offset( IF->parent->type, "Ca_i", &__Ca_i_offset, &__Ca_i_sizeof );
  SVputfcn __Ca_i_SVputfcn = __Ca_i_SVgetfcn ? getPutSV(__Ca_i_SVgetfcn) : NULL;

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Stress_Rice_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Ca_i = __Ca_i_SVgetfcn ? __Ca_i_SVgetfcn(IF->parent, __i, __Ca_i_offset) :sv->__Ca_i_local;
    //Change the units of external variables as appropriate.
    
    
    sv->failing = p->failing;
    // Initialize the rest of the nodal variables
    sv->N = N_init;
    sv->P = P_init;
    sv->TnCaH = TnCaH_init;
    sv->TnCaL = TnCaL_init;
    sv->XBpostr = XBpostr_init;
    sv->XBprer = XBprer_init;
    delta_sl = delta_sl_init;
    length = length_init;
    sv->xXBpostr = xXBpostr_init;
    sv->xXBprer = xXBprer_init;
    double realLength = (length*SLrest);
    double sovr_cle_a = ((realLength/2.)-((realLength-(len_thin))));
    double sovr_ze = (((len_thick/2.)<(realLength/2.)) ? (len_thick/2.) : (realLength/2.));
    double sovr_cle = ((sovr_cle_a<(len_hbare/2.)) ? (len_hbare/2.) : sovr_cle_a);
    double len_sovr = (sovr_ze-(sovr_cle));
    double SOVFThick = ((len_sovr*2.)/(len_thick-(len_hbare)));
    Tension = (((kxb*SOVFThick)*((sv->xXBpostr*sv->XBpostr)+(sv->xXBprer*sv->XBprer)))*p->force_coeff);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Ca_i_SVputfcn ) 
    	__Ca_i_SVputfcn(IF->parent, __i, __Ca_i_offset, Ca_i);
    else
    	sv->__Ca_i_local=Ca_i;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Stress_Rice(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Rice_Params *p  = (Stress_Rice_Params *)IF->params;
  Stress_Rice_state *sv_base = (Stress_Rice_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Ca_i_sizeof;
  int __Ca_i_offset;
  SVgetfcn __Ca_i_SVgetfcn = get_sv_offset( IF->parent->type, "Ca_i", &__Ca_i_offset, &__Ca_i_sizeof );
  SVputfcn __Ca_i_SVputfcn = __Ca_i_SVgetfcn ? getPutSV(__Ca_i_SVgetfcn) : NULL;

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Stress_Rice_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Ca_i = __Ca_i_SVgetfcn ? __Ca_i_SVgetfcn(IF->parent, __i, __Ca_i_offset) :sv->__Ca_i_local;
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t realLength = (length*SLrest);
    GlobalData_t sovr_cle_a = ((realLength/2.)-((realLength-(len_thin))));
    GlobalData_t sovr_ze = (((len_thick/2.)<(realLength/2.)) ? (len_thick/2.) : (realLength/2.));
    GlobalData_t sovr_cle = ((sovr_cle_a<(len_hbare/2.)) ? (len_hbare/2.) : sovr_cle_a);
    GlobalData_t len_sovr = (sovr_ze-(sovr_cle));
    GlobalData_t SOVFThick = ((len_sovr*2.)/(len_thick-(len_hbare)));
    Tension = (((kxb*SOVFThick)*((sv->xXBpostr*sv->XBpostr)+(sv->xXBprer*sv->XBprer)))*p->force_coeff);
    
    
    //Complete Forward Euler Update
    GlobalData_t SOVFThin = (len_sovr/len_thin);
    GlobalData_t dSL = ((delta_sl*SLrest)/2.0);
    GlobalData_t gslmod = ((sv->failing>0.5) ? 6. : 6.);
    GlobalData_t gxbmd = (((x_0-(sv->xXBpostr))>0.) ? (exp(((sigmap*((x_0-(sv->xXBpostr))/x_0))*((x_0-(sv->xXBpostr))/x_0)))) : (exp((sigman*((((sv->xXBpostr-(x_0))/x_0)*(sv->xXBpostr-(x_0)))/x_0)))));
    GlobalData_t perm50 = ((sv->failing>0.5) ? 0.5 : 0.5);
    GlobalData_t simCa = Ca_i;
    GlobalData_t diff_TnCaH = (((konT*simCa)*(1.0-(sv->TnCaH)))-((koffHT*sv->TnCaH)));
    GlobalData_t diff_TnCaL = (((konT*simCa)*(1.0-(sv->TnCaL)))-((koffLT*sv->TnCaL)));
    GlobalData_t gapslmd = (1.0+((1.0-(SOVFThick))*gslmod));
    GlobalData_t gxbT = ((gxb*gxbmd)*gxbT_term);
    GlobalData_t perm = (((1.0-(SOVFThin))*sv->TnCaL)+(SOVFThin*sv->TnCaH));
    GlobalData_t diff_XBpostr = (((hfT*sv->XBprer)-((hbT*sv->XBpostr)))-((gxbT*sv->XBpostr)));
    GlobalData_t gappT = (((gapp*gapslmd)*gappT_term)*(exp((pow(((sv->xXBprer/x_0)*20.),4.)))));
    GlobalData_t permtot_temp = (sqrt((1.0/(1.0+(pow((perm50/perm),nperm))))));
    GlobalData_t diff_XBprer = ((((fappT*sv->P)-((gappT*sv->XBprer)))-((hfT*sv->XBprer)))+(hbT*sv->XBpostr));
    GlobalData_t dtyf_postr = (fappT/((((((fappT*hfT)+(gxbT*hfT))+(gxbT*gappT))+(hbT*fappT))+(hbT*gappT))+(gxbT*fappT)));
    GlobalData_t dtyf_prer = (((hbT*fappT)+(gxbT*fappT))/((((((fappT*hfT)+(gxbT*hfT))+(gxbT*gappT))+(hbT*fappT))+(hbT*gappT))+(gxbT*fappT)));
    GlobalData_t permtot = ((permtot_temp<0.00000001) ? 0.00000001 : permtot_temp);
    GlobalData_t diff_xXBpostr = ((dSL/2.)+((xPsi/dtyf_postr)*((x_0+sv->xXBprer)-(sv->xXBpostr))));
    GlobalData_t diff_xXBprer = ((dSL/2.)+((xPsi/dtyf_prer)*(((-sv->xXBprer)*fappT)+(((sv->xXBpostr-(x_0))-(sv->xXBprer))*hbT))));
    GlobalData_t inprmt = ((100.0<(1.0/permtot)) ? 100.0 : (1.0/permtot));
    GlobalData_t kn_pT = ((kn_p*permtot)*kn_pT_term);
    GlobalData_t kp_nT = ((kp_n*inprmt)*kp_nT_term);
    GlobalData_t diff_N = (((-kn_pT)*sv->N)+(kp_nT*sv->P));
    GlobalData_t diff_P = ((((((-kp_nT)*sv->P)+(kn_pT*sv->N))-((fappT*sv->P)))+(gappT*sv->XBprer))+(gxbT*sv->XBpostr));
    GlobalData_t N_new = sv->N+diff_N*dt;
    GlobalData_t P_new = sv->P+diff_P*dt;
    GlobalData_t TnCaH_new = sv->TnCaH+diff_TnCaH*dt;
    GlobalData_t TnCaL_new = sv->TnCaL+diff_TnCaL*dt;
    GlobalData_t XBpostr_new = sv->XBpostr+diff_XBpostr*dt;
    GlobalData_t XBprer_new = sv->XBprer+diff_XBprer*dt;
    GlobalData_t xXBpostr_new = sv->xXBpostr+diff_xXBpostr*dt;
    GlobalData_t xXBprer_new = sv->xXBprer+diff_xXBprer*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    if (N_new < 0) { IIF_warn(__i, "N < 0, clamping to 0"); sv->N = 0; }
    else {sv->N = N_new;}
    if (P_new < 0) { IIF_warn(__i, "P < 0, clamping to 0"); sv->P = 0; }
    else {sv->P = P_new;}
    Tension = Tension;
    if (TnCaH_new < 0) { IIF_warn(__i, "TnCaH < 0, clamping to 0"); sv->TnCaH = 0; }
    else {sv->TnCaH = TnCaH_new;}
    if (TnCaL_new < 0) { IIF_warn(__i, "TnCaL < 0, clamping to 0"); sv->TnCaL = 0; }
    else {sv->TnCaL = TnCaL_new;}
    if (XBpostr_new < 0) { IIF_warn(__i, "XBpostr < 0, clamping to 0"); sv->XBpostr = 0; }
    else {sv->XBpostr = XBpostr_new;}
    if (XBprer_new < 0) { IIF_warn(__i, "XBprer < 0, clamping to 0"); sv->XBprer = 0; }
    else {sv->XBprer = XBprer_new;}
    sv->xXBpostr = xXBpostr_new;
    sv->xXBprer = xXBprer_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Ca_i_SVputfcn ) 
    	__Ca_i_SVputfcn(IF->parent, __i, __Ca_i_offset, Ca_i);
    else
    	sv->__Ca_i_local=Ca_i;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        