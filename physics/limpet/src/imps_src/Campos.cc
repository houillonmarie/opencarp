// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Fernando O. Campos, Yohannes Shiferaw, Anton J. Prassl, Patrick M. Boyle, Edward J. Vigmond, Gernot Plank
*  Year: 2015
*  Title: Stochastic spontaneous calcium release events trigger premature ventricular complexes by overcoming electrotonic load
*  Journal: Cardiovascular Research, 107(1), 175-183
*  DOI: 10.1093/cvr/cvv149
*  Comment: Modified version of the Mahajan-Shiferaw rabbit ventricular myocyte model with spontaneous Ca release
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Campos.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Campos(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Campos( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define BCd (GlobalData_t)(24.0)
#define Bmem (GlobalData_t)(15.0)
#define Bsar (GlobalData_t)(42.0)
#define Bsr (GlobalData_t)(47.0)
#define Btrop (GlobalData_t)((70.0*0.6))
#define Cai_init (GlobalData_t)(0.3)
#define Cm (GlobalData_t)(3.10e-04)
#define F (GlobalData_t)(96485.0)
#define KCd (GlobalData_t)(7.0)
#define KSR (GlobalData_t)(0.6)
#define KToff (GlobalData_t)(0.0196)
#define KTon (GlobalData_t)(0.0327)
#define Ki_init (GlobalData_t)(140.0)
#define KmCai (GlobalData_t)(0.0036)
#define KmCao (GlobalData_t)(1.3)
#define KmNai (GlobalData_t)(12.3)
#define KmNao (GlobalData_t)(87.5)
#define Kmem (GlobalData_t)(0.3)
#define Ksar (GlobalData_t)(13.0)
#define Nai_init (GlobalData_t)(10.0)
#define P_Ca (GlobalData_t)(0.000540)
#define R (GlobalData_t)(8314.0)
#define T (GlobalData_t)(308.)
#define TBa (GlobalData_t)(450.)
#define V_init (GlobalData_t)(-90.0)
#define ax (GlobalData_t)(0.356)
#define ay (GlobalData_t)(0.05)
#define c1_init (GlobalData_t)(0.0)
#define c2_init (GlobalData_t)(1.0)
#define c_up (GlobalData_t)(0.5)
#define cnaca (GlobalData_t)(0.3)
#define cp_init (GlobalData_t)(0.3)
#define cpb (GlobalData_t)(3.0)
#define cpt (GlobalData_t)(10.0)
#define cs_init (GlobalData_t)(0.3)
#define diff_Ki (GlobalData_t)(0.0)
#define diff_Nai (GlobalData_t)(0.0)
#define g_dyad (GlobalData_t)(9000.0)
#define g_leak (GlobalData_t)(0.0)
#define g_rel (GlobalData_t)(23600.0)
#define h_init (GlobalData_t)(1.0)
#define i1ba_init (GlobalData_t)(0.0)
#define i1ca_init (GlobalData_t)(0.0)
#define i2ba_init (GlobalData_t)(0.0)
#define i2ca_init (GlobalData_t)(0.0)
#define j_init (GlobalData_t)(1.0)
#define j_rel_init (GlobalData_t)(0.0)
#define k1t (GlobalData_t)(0.00413)
#define k2 (GlobalData_t)(0.0005)
#define k2t (GlobalData_t)(0.00224)
#define kj (GlobalData_t)(500.0)
#define kmKo (GlobalData_t)(1.5)
#define kmNai (GlobalData_t)(12.0)
#define ksat (GlobalData_t)(0.2)
#define m2u (GlobalData_t)(1000.0)
#define m_init (GlobalData_t)(0.001)
#define ndyad (GlobalData_t)(150.)
#define poc (GlobalData_t)(0.02)
#define prnak (GlobalData_t)(0.018330)
#define r1 (GlobalData_t)(0.30)
#define r2 (GlobalData_t)(3.0)
#define rq (GlobalData_t)((0.10/900.0))
#define s1t (GlobalData_t)(0.00195)
#define s6 (GlobalData_t)(8.0)
#define spont_rate (GlobalData_t)(0.0)
#define sx (GlobalData_t)(3.0)
#define sy (GlobalData_t)(4.0)
#define syr (GlobalData_t)(11.32)
#define tau3 (GlobalData_t)(3.0)
#define taua (GlobalData_t)(20.0)
#define taud (GlobalData_t)(4.0)
#define taupo (GlobalData_t)(1.0)
#define taur (GlobalData_t)(20.0)
#define taus (GlobalData_t)(0.5)
#define tca (GlobalData_t)(114.0)
#define tropi_init (GlobalData_t)(10.0)
#define trops_init (GlobalData_t)(10.0)
#define vi (GlobalData_t)(2.58e-05)
#define viovs (GlobalData_t)(50.0)
#define viovsr (GlobalData_t)(20.0)
#define vth (GlobalData_t)(0.0)
#define vx (GlobalData_t)(-40.0)
#define vy (GlobalData_t)(-40.0)
#define vyr (GlobalData_t)(-40.0)
#define xKr_init (GlobalData_t)(0.0)
#define xi (GlobalData_t)(0.35)
#define xs1_init (GlobalData_t)(0.004)
#define xs2_init (GlobalData_t)(0.004)
#define xtof_init (GlobalData_t)(0.02)
#define xtos_init (GlobalData_t)(0.01)
#define ytof_init (GlobalData_t)(0.8)
#define ytos_init (GlobalData_t)(1.0)
#define RT_F (GlobalData_t)(((R*T)/F))
#define mNai3 (GlobalData_t)(((KmNai*KmNai)*KmNai))
#define mNao3 (GlobalData_t)(((KmNao*KmNao)*KmNao))
#define s2t (GlobalData_t)(((s1t*(k2t/k1t))*(r1/r2)))
#define vs (GlobalData_t)((0.02*vi))
#define vsr (GlobalData_t)((0.06*vi))
#define wca (GlobalData_t)((((F/m2u)*vi)/Cm))
#define F_RT (GlobalData_t)((1.0/RT_F))
#define i2conc (GlobalData_t)(((1.0/wca)/m2u))
#define wca_m2 (GlobalData_t)((2.0*wca))
#define f1 (GlobalData_t)(((((4.0*P_Ca)*F)*F_RT)/m2u))



void initialize_params_Campos( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Campos_Params *p = (Campos_Params *)IF->params;

  // Compute the regional constants
  {
    p->CSR = 1200.0;
    p->Cao = 1.8;
    p->INaCamax = 0.3;
    p->INaKmax = 1.5;
    p->Ko = 5.4;
    p->Nao = 136.;
    p->g_Ca = 0.6;
    p->g_K1 = 0.30;
    p->g_Kr = 0.003;
    p->g_Ks = 0.043;
    p->g_Na = 12.0;
    p->g_RyR = 2.25;
    p->g_sp = (1.0e-08*60.0);
    p->g_tof = 0.1;
    p->g_tos = 0.04;
    p->v_Na = 0.0;
    p->v_up = 0.3;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_Campos;

}


// Define the parameters for the lookup tables
enum Tables {
  Cai_TAB,
  Ki_TAB,
  Nai_TAB,
  V_TAB,
  cj_TAB,
  cp_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum Cai_TableIndex {
  beta_ci_idx,
  j_up_idx,
  q_Ks_idx,
  NROWS_Cai
};

enum Ki_TableIndex {
  E_K_idx,
  NROWS_Ki
};

enum Nai_TableIndex {
  E_Na_idx,
  Nai3_idx,
  yz3_idx,
  NROWS_Nai
};

enum V_TableIndex {
  Pr_idx,
  Ps_idx,
  R_V_idx,
  Rv_idx,
  a_b_idx,
  alpha_idx,
  beta_idx,
  f2_idx,
  fNaK_idx,
  gSR_V_idx,
  g_RyR_V_idx,
  h_rush_larsen_A_idx,
  h_rush_larsen_B_idx,
  j_rush_larsen_A_idx,
  j_rush_larsen_B_idx,
  k3_idx,
  k3t_idx,
  k4t_idx,
  k5t_idx,
  k6t_idx,
  m_rush_larsen_A_idx,
  m_rush_larsen_B_idx,
  rs_inf_idx,
  xKr_rush_larsen_A_idx,
  xKr_rush_larsen_B_idx,
  xs1_rush_larsen_A_idx,
  xs1_rush_larsen_B_idx,
  xs2_rush_larsen_A_idx,
  xs2_rush_larsen_B_idx,
  xtof_rush_larsen_A_idx,
  xtof_rush_larsen_B_idx,
  xtos_rush_larsen_A_idx,
  xtos_rush_larsen_B_idx,
  ytof_rush_larsen_A_idx,
  ytof_rush_larsen_B_idx,
  ytos_rush_larsen_A_idx,
  ytos_rush_larsen_B_idx,
  za_idx,
  zw3a_idx,
  zw3b_idx,
  zw4_idx,
  NROWS_V
};

enum cj_TableIndex {
  Lcj_idx,
  NROWS_cj
};

enum cp_TableIndex {
  TCa_idx,
  fca_idx,
  k1_idx,
  s1_idx,
  s2_idx,
  NROWS_cp
};



void construct_tables_Campos( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Campos_Params *p = (Campos_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double Nao3 = ((p->Nao*p->Nao)*p->Nao);
  double R_scr = (p->g_sp*spont_rate);
  double cj_init = p->CSR;
  double cjp_init = p->CSR;
  double g_ss = (sqrt((p->Ko/5.4)));
  double sigma = (((exp((p->Nao/67.3)))-(1.0))/7.0);
  
  // Create the Cai lookup table
  LUT* Cai_tab = &IF->tables[Cai_TAB];
  LUT_alloc(Cai_tab, NROWS_Cai, 1.0e-03, 2.0e+01, 1.0e-03, "Campos Cai");
  for (int __i=Cai_tab->mn_ind; __i<=Cai_tab->mx_ind; __i++) {
    double Cai = Cai_tab->res*__i;
    LUT_data_t* Cai_row = Cai_tab->tab[__i];
    double bix = ((BCd*KCd)/((KCd+Cai)*(KCd+Cai)));
    Cai_row[j_up_idx] = (((p->v_up*Cai)*Cai)/((Cai*Cai)+(c_up*c_up)));
    double memix = ((Bmem*Kmem)/((Kmem+Cai)*(Kmem+Cai)));
    Cai_row[q_Ks_idx] = (1.0+(0.8*(1.0+(((0.5/Cai)*(0.5/Cai))*(0.5/Cai)))));
    double sarix = ((Bsar*Ksar)/((Ksar+Cai)*(Ksar+Cai)));
    double six = ((Bsr*KSR)/((KSR+Cai)*(KSR+Cai)));
    Cai_row[beta_ci_idx] = (1.0/((((1.0+bix)+six)+memix)+sarix));
  }
  check_LUT(Cai_tab);
  
  
  // Create the Ki lookup table
  LUT* Ki_tab = &IF->tables[Ki_TAB];
  LUT_alloc(Ki_tab, NROWS_Ki, 1.0e-03, 1.0e+03, 1.0e-03, "Campos Ki");
  for (int __i=Ki_tab->mn_ind; __i<=Ki_tab->mx_ind; __i++) {
    double Ki = Ki_tab->res*__i;
    LUT_data_t* Ki_row = Ki_tab->tab[__i];
    Ki_row[E_K_idx] = (RT_F*(log((p->Ko/Ki))));
  }
  check_LUT(Ki_tab);
  
  
  // Create the Nai lookup table
  LUT* Nai_tab = &IF->tables[Nai_TAB];
  LUT_alloc(Nai_tab, NROWS_Nai, 1.0e-02, 6.0e+01, 1.0e-02, "Campos Nai");
  for (int __i=Nai_tab->mn_ind; __i<=Nai_tab->mx_ind; __i++) {
    double Nai = Nai_tab->res*__i;
    LUT_data_t* Nai_row = Nai_tab->tab[__i];
    Nai_row[E_Na_idx] = (RT_F*(log((p->Nao/Nai))));
    Nai_row[Nai3_idx] = ((Nai*Nai)*Nai);
    Nai_row[yz3_idx] = ((KmCai*Nao3)*(1.0+(Nai_row[Nai3_idx]/mNai3)));
  }
  check_LUT(Nai_tab);
  
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -8.0e+02, 8.0e+02, 5.0e-02, "Campos V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    V_row[Pr_idx] = (1.0-((1.0/(1.0+(exp(((-(V-(vy)))/sy)))))));
    V_row[Ps_idx] = (1.0/(1.0+(exp(((-(V-(vyr)))/syr)))));
    V_row[R_V_idx] = (10.0+(4954.0*(exp((V/15.6)))));
    V_row[Rv_idx] = (1.0/(1.0+(exp(((V+33.0)/22.4)))));
    V_row[a_b_idx] = (exp((V/8.0)));
    double a_h = (((V-(p->v_Na))<-40.0) ? (0.135*(exp((((V-(p->v_Na))+80.0)/-6.8)))) : 0.0);
    double a_j = (((V-(p->v_Na))<-40.0) ? (((-127140.0*(exp((0.2444*(V-(p->v_Na))))))-((0.00003474*(exp((-0.04391*(V-(p->v_Na))))))))*(((V-(p->v_Na))+37.78)/(1.0+(exp((0.311*((V-(p->v_Na))+79.23))))))) : 0.0);
    double a_m = ((V==-47.13) ? 3.2 : ((0.32*(V+47.13))/(1.0-((exp((-0.1*(V+47.13))))))));
    double b_h = (((V-(p->v_Na))<-40.0) ? ((3.56*(exp((0.079*(V-(p->v_Na))))))+(310000.0*(exp((0.35*(V-(p->v_Na))))))) : (1.0/(0.13*(1.0+(exp((((V-(p->v_Na))+10.66)/-11.1)))))));
    double b_j = (((V-(p->v_Na))<-40.0) ? ((0.1212*(exp((-0.01052*(V-(p->v_Na))))))/(1.0+(exp((-0.1378*((V-(p->v_Na))+40.14)))))) : ((0.3*(exp((-0.0000002535*(V-(p->v_Na))))))/(1.0+(exp((-0.1*((V-(p->v_Na))+32.0)))))));
    double b_m = (0.08*(exp(((-V)/11.0))));
    V_row[f2_idx] = (V*f1);
    V_row[fNaK_idx] = (1.0/((1.0+(0.1245*(exp(((-0.1*V)*F_RT)))))+((0.0365*sigma)*(exp(((-V)*F_RT))))));
    V_row[gSR_V_idx] = ((g_rel*(exp(((-ax)*(V+30.0)))))/(1.0+(exp(((-ax)*(V+30.0))))));
    V_row[g_RyR_V_idx] = ((p->g_RyR*(exp(((-ay)*(V+30.0)))))/(1.0+(exp(((-ay)*(V+30.0))))));
    double poi = (1.0/(1.0+(exp(((-(V-(vx)))/sx)))));
    double poinf = (1.0/(1.0+(exp(((-(V-(vth)))/s6)))));
    double rt1 = ((-(V+3.00))/15.0);
    double rt2 = ((V+33.5)/10.0);
    double tau_xs1 = ((V!=-30.0) ? (1.0/(((7.19e-05*(V+30.0))/(1.0-((exp((-0.148*(V+30.0)))))))+((1.31e-04*(V+30.0))/(-1.0+(exp((0.0687*(V+30.0)))))))) : (1.0/((7.19e-05/0.148)+(1.31e-04/0.0687))));
    double tau_xtof = ((3.5*(exp(((-(V/30.0))*(V/30.0)))))+1.5);
    double tau_ytof = ((20.0/(1.0+(exp(((V+33.5)/10.0)))))+20.0);
    double tau_ytos = ((3000.0/(1.0+(exp(((V+60.0)/10.0)))))+30.0);
    double txKr1 = ((V!=-7.0) ? ((0.00138*(V+7.0))/(1.0-((exp((-0.123*(V+7.0))))))) : (0.00138/0.123));
    double txKr2 = ((V!=-10.0) ? ((0.00061*(V+10.0))/((exp((0.145*(V+10.0))))-(1.0))) : (0.00061/0.145));
    double xKr_inf = (1.0/(1.0+(exp(((-(V+50.0))/7.5)))));
    double xs1_inf = (1.0/(1.0+(exp(((-(V-(1.5)))/16.7)))));
    V_row[za_idx] = ((2.0*V)*F_RT);
    V_row[zw3a_idx] = (exp(((V*xi)*F_RT)));
    V_row[zw3b_idx] = (exp(((V*(xi-(1.0)))*F_RT)));
    V_row[zw4_idx] = (1.0+(ksat*(exp(((V*(xi-(1.0)))*F_RT)))));
    V_row[alpha_idx] = (poinf/taupo);
    V_row[beta_idx] = ((1.0-(poinf))/taupo);
    V_row[h_rush_larsen_A_idx] = (((-a_h)/(a_h+b_h))*(expm1(((-dt)*(a_h+b_h)))));
    V_row[h_rush_larsen_B_idx] = (exp(((-dt)*(a_h+b_h))));
    V_row[j_rush_larsen_A_idx] = (((-a_j)/(a_j+b_j))*(expm1(((-dt)*(a_j+b_j)))));
    V_row[j_rush_larsen_B_idx] = (exp(((-dt)*(a_j+b_j))));
    V_row[k3_idx] = ((1.0-(poi))/tau3);
    V_row[m_rush_larsen_A_idx] = (((-a_m)/(a_m+b_m))*(expm1(((-dt)*(a_m+b_m)))));
    V_row[m_rush_larsen_B_idx] = (exp(((-dt)*(a_m+b_m))));
    double tauBa = (((V_row[R_V_idx]-(TBa))*V_row[Pr_idx])+TBa);
    double tau_xKr = (1.0/(txKr1+txKr2));
    double tau_xs2 = (4.0*tau_xs1);
    double tau_xtos = ((9.0/(1.0+(exp((-rt1)))))+0.5);
    V_row[xs1_rush_larsen_B_idx] = (exp(((-dt)/tau_xs1)));
    double xs1_rush_larsen_C = (expm1(((-dt)/tau_xs1)));
    double xs2_inf = xs1_inf;
    double xtof_inf = (1.0/(1.0+(exp(rt1))));
    V_row[xtof_rush_larsen_B_idx] = (exp(((-dt)/tau_xtof)));
    double xtof_rush_larsen_C = (expm1(((-dt)/tau_xtof)));
    double ytof_inf = (1.0/(1.0+(exp(rt2))));
    V_row[ytof_rush_larsen_B_idx] = (exp(((-dt)/tau_ytof)));
    double ytof_rush_larsen_C = (expm1(((-dt)/tau_ytof)));
    V_row[ytos_rush_larsen_B_idx] = (exp(((-dt)/tau_ytos)));
    double ytos_rush_larsen_C = (expm1(((-dt)/tau_ytos)));
    V_row[k3t_idx] = V_row[k3_idx];
    V_row[k5t_idx] = ((1.0-(V_row[Ps_idx]))/tauBa);
    V_row[k6t_idx] = (V_row[Ps_idx]/tauBa);
    V_row[rs_inf_idx] = ytof_inf;
    V_row[xKr_rush_larsen_B_idx] = (exp(((-dt)/tau_xKr)));
    double xKr_rush_larsen_C = (expm1(((-dt)/tau_xKr)));
    V_row[xs1_rush_larsen_A_idx] = ((-xs1_inf)*xs1_rush_larsen_C);
    V_row[xs2_rush_larsen_B_idx] = (exp(((-dt)/tau_xs2)));
    double xs2_rush_larsen_C = (expm1(((-dt)/tau_xs2)));
    V_row[xtof_rush_larsen_A_idx] = ((-xtof_inf)*xtof_rush_larsen_C);
    double xtos_inf = xtof_inf;
    V_row[xtos_rush_larsen_B_idx] = (exp(((-dt)/tau_xtos)));
    double xtos_rush_larsen_C = (expm1(((-dt)/tau_xtos)));
    V_row[ytof_rush_larsen_A_idx] = ((-ytof_inf)*ytof_rush_larsen_C);
    double ytos_inf = ytof_inf;
    V_row[k4t_idx] = (((V_row[k3t_idx]*V_row[a_b_idx])*(k1t/k2t))*(V_row[k5t_idx]/V_row[k6t_idx]));
    V_row[xKr_rush_larsen_A_idx] = ((-xKr_inf)*xKr_rush_larsen_C);
    V_row[xs2_rush_larsen_A_idx] = ((-xs2_inf)*xs2_rush_larsen_C);
    V_row[xtos_rush_larsen_A_idx] = ((-xtos_inf)*xtos_rush_larsen_C);
    V_row[ytos_rush_larsen_A_idx] = ((-ytos_inf)*ytos_rush_larsen_C);
  }
  check_LUT(V_tab);
  
  
  // Create the cj lookup table
  LUT* cj_tab = &IF->tables[cj_TAB];
  LUT_alloc(cj_tab, NROWS_cj, 1.0e-03, 1.0e+04, 1.0e-03, "Campos cj");
  for (int __i=cj_tab->mn_ind; __i<=cj_tab->mx_ind; __i++) {
    double cj = cj_tab->res*__i;
    LUT_data_t* cj_row = cj_tab->tab[__i];
    cj_row[Lcj_idx] = ((cj*cj)/((cj*cj)+(kj*kj)));
  }
  check_LUT(cj_tab);
  
  
  // Create the cp lookup table
  LUT* cp_tab = &IF->tables[cp_TAB];
  LUT_alloc(cp_tab, NROWS_cp, 1.0e-03, 1.0e+04, 1.0e-03, "Campos cp");
  for (int __i=cp_tab->mn_ind; __i<=cp_tab->mx_ind; __i++) {
    double cp = cp_tab->res*__i;
    LUT_data_t* cp_row = cp_tab->tab[__i];
    cp_row[TCa_idx] = (tca/(1.0+((((cp/cpt)*(cp/cpt))*(cp/cpt))*(cp/cpt))));
    cp_row[fca_idx] = (1.0/(1.0+(((cpb/cp)*(cpb/cp))*(cpb/cp))));
    cp_row[k1_idx] = (0.03*cp_row[fca_idx]);
    cp_row[s1_idx] = (0.02*cp_row[fca_idx]);
    cp_row[s2_idx] = ((cp_row[s1_idx]*(k2/cp_row[k1_idx]))*(r1/r2));
  }
  check_LUT(cp_tab);
  

}



void    initialize_sv_Campos( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Campos_Params *p = (Campos_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Campos_state) );
  Campos_state *sv_base = (Campos_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double Nao3 = ((p->Nao*p->Nao)*p->Nao);
  double R_scr = (p->g_sp*spont_rate);
  double cj_init = p->CSR;
  double cjp_init = p->CSR;
  double g_ss = (sqrt((p->Ko/5.4)));
  double sigma = (((exp((p->Nao/67.3)))-(1.0))/7.0);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Campos_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Cai = Cai_init;
    sv->Ki = Ki_init;
    sv->Nai = Nai_init;
    V = V_init;
    sv->c1 = c1_init;
    sv->c2 = c2_init;
    sv->cj = cj_init;
    sv->cjp = cjp_init;
    sv->cp = cp_init;
    sv->cs = cs_init;
    sv->h = h_init;
    sv->i1ba = i1ba_init;
    sv->i1ca = i1ca_init;
    sv->i2ba = i2ba_init;
    sv->i2ca = i2ca_init;
    sv->j = j_init;
    sv->j_rel = j_rel_init;
    sv->m = m_init;
    sv->tropi = tropi_init;
    sv->trops = trops_init;
    sv->xKr = xKr_init;
    sv->xs1 = xs1_init;
    sv->xs2 = xs2_init;
    sv->xtof = xtof_init;
    sv->xtos = xtos_init;
    sv->ytof = ytof_init;
    sv->ytos = ytos_init;
    double E_K = (RT_F*(log((p->Ko/sv->Ki))));
    double E_Ks = (RT_F*(log(((p->Ko+(prnak*p->Nao))/(sv->Ki+(prnak*sv->Nai))))));
    double E_Na = (RT_F*(log((p->Nao/sv->Nai))));
    double Ka = (1.0/(1.0+(((cnaca/sv->cs)*(cnaca/sv->cs))*(cnaca/sv->cs))));
    double Nai3 = ((sv->Nai*sv->Nai)*sv->Nai);
    double Rv = (1.0/(1.0+(exp(((V+33.0)/22.4)))));
    double csm = (sv->cs/m2u);
    double f2 = (V*f1);
    double fNaK = (1.0/((1.0+(0.1245*(exp(((-0.1*V)*F_RT)))))+((0.0365*sigma)*(exp(((-V)*F_RT))))));
    double po = (1.0-((((((sv->c1+sv->c2)+sv->i1ca)+sv->i2ca)+sv->i1ba)+sv->i2ba)));
    double q_Ks = (1.0+(0.8*(1.0+(((0.5/sv->Cai)*(0.5/sv->Cai))*(0.5/sv->Cai)))));
    double rt2 = ((V+33.5)/10.0);
    double za = ((2.0*V)*F_RT);
    double zw3a = (exp(((V*xi)*F_RT)));
    double zw3b = (exp(((V*(xi-(1.0)))*F_RT)));
    double zw4 = (1.0+(ksat*(exp(((V*(xi-(1.0)))*F_RT)))));
    double AK1 = (1.02/(1.0+(exp((0.2385*((V-(E_K))-(59.215)))))));
    double BK1 = (((0.49124*(exp((0.08032*((V-(E_K))+5.476)))))+(exp((0.061750*((V-(E_K))-(594.31))))))/(1.0+(exp((-0.5143*((V-(E_K))+4.753))))));
    double IKr = ((((p->g_Kr*g_ss)*sv->xKr)*Rv)*(V-(E_K)));
    double IKs = ((((p->g_Ks*sv->xs1)*sv->xs2)*q_Ks)*(V-(E_Ks)));
    double INa = (((p->g_Na*((sv->m*sv->m)*sv->m))*(sv->h*sv->j))*(V-(E_Na)));
    double INaK = ((((p->INaKmax*fNaK)*(1.0/(1.0+(kmNai/sv->Nai))))*p->Ko)/(p->Ko+kmKo));
    double Itof = (((p->g_tof*sv->xtof)*sv->ytof)*(V-(E_K)));
    double iCa = (((fabs(za))<0.001) ? ((f1*((csm*(exp(za)))-((0.341*p->Cao))))/(2.0*F_RT)) : ((f2*((csm*(exp(za)))-((0.341*p->Cao))))/((exp(za))-(1.0))));
    double ytof_inf = (1.0/(1.0+(exp(rt2))));
    double yz1 = ((KmCao*Nai3)+(mNao3*csm));
    double yz2 = ((mNai3*p->Cao)*(1.0+(csm/KmCai)));
    double yz3 = ((KmCai*Nao3)*(1.0+(Nai3/mNai3)));
    double yz4 = ((Nai3*p->Cao)+(Nao3*csm));
    double zw3 = (((zw3a*Nai3)*p->Cao)-(((zw3b*Nao3)*csm)));
    double K1_inf = (AK1/(AK1+BK1));
    double j_Ca = (((140.0*p->g_Ca)*po)*iCa);
    double rs_inf = ytof_inf;
    double zw8 = (((yz1+yz2)+yz3)+yz4);
    double ICa = (wca_m2*j_Ca);
    double IK1 = (((p->g_K1*g_ss)*K1_inf)*(V-(E_K)));
    double Itos = (((p->g_tos*sv->xtos)*(sv->ytos+(0.5*rs_inf)))*(V-(E_K)));
    double j_NaCa = (((p->INaCamax*Ka)*zw3)/(zw4*zw8));
    double INaCa = (wca*j_NaCa);
    Iion = ((((((((INa+ICa)+Itof)+Itos)+IKr)+IKs)+IK1)+INaCa)+INaK);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Campos(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Campos_Params *p  = (Campos_Params *)IF->params;
  Campos_state *sv_base = (Campos_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Nao3 = ((p->Nao*p->Nao)*p->Nao);
  GlobalData_t R_scr = (p->g_sp*spont_rate);
  GlobalData_t cj_init = p->CSR;
  GlobalData_t cjp_init = p->CSR;
  GlobalData_t g_ss = (sqrt((p->Ko/5.4)));
  GlobalData_t sigma = (((exp((p->Nao/67.3)))-(1.0))/7.0);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Campos_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Cai_row[NROWS_Cai];
    LUT_interpRow(&IF->tables[Cai_TAB], sv->Cai, __i, Cai_row);
    LUT_data_t Ki_row[NROWS_Ki];
    LUT_interpRow(&IF->tables[Ki_TAB], sv->Ki, __i, Ki_row);
    LUT_data_t Nai_row[NROWS_Nai];
    LUT_interpRow(&IF->tables[Nai_TAB], sv->Nai, __i, Nai_row);
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    LUT_data_t cj_row[NROWS_cj];
    LUT_interpRow(&IF->tables[cj_TAB], sv->cj, __i, cj_row);
    LUT_data_t cp_row[NROWS_cp];
    LUT_interpRow(&IF->tables[cp_TAB], sv->cp, __i, cp_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t E_Ks = (RT_F*(log(((p->Ko+(prnak*p->Nao))/(sv->Ki+(prnak*sv->Nai))))));
    GlobalData_t Ka = (1.0/(1.0+(((cnaca/sv->cs)*(cnaca/sv->cs))*(cnaca/sv->cs))));
    GlobalData_t csm = (sv->cs/m2u);
    GlobalData_t po = (1.0-((((((sv->c1+sv->c2)+sv->i1ca)+sv->i2ca)+sv->i1ba)+sv->i2ba)));
    GlobalData_t AK1 = (1.02/(1.0+(exp((0.2385*((V-(Ki_row[E_K_idx]))-(59.215)))))));
    GlobalData_t BK1 = (((0.49124*(exp((0.08032*((V-(Ki_row[E_K_idx]))+5.476)))))+(exp((0.061750*((V-(Ki_row[E_K_idx]))-(594.31))))))/(1.0+(exp((-0.5143*((V-(Ki_row[E_K_idx]))+4.753))))));
    GlobalData_t IKr = ((((p->g_Kr*g_ss)*sv->xKr)*V_row[Rv_idx])*(V-(Ki_row[E_K_idx])));
    GlobalData_t IKs = ((((p->g_Ks*sv->xs1)*sv->xs2)*Cai_row[q_Ks_idx])*(V-(E_Ks)));
    GlobalData_t INa = (((p->g_Na*((sv->m*sv->m)*sv->m))*(sv->h*sv->j))*(V-(Nai_row[E_Na_idx])));
    GlobalData_t INaK = ((((p->INaKmax*V_row[fNaK_idx])*(1.0/(1.0+(kmNai/sv->Nai))))*p->Ko)/(p->Ko+kmKo));
    GlobalData_t Itof = (((p->g_tof*sv->xtof)*sv->ytof)*(V-(Ki_row[E_K_idx])));
    GlobalData_t iCa = (((fabs(V_row[za_idx]))<0.001) ? ((f1*((csm*(exp(V_row[za_idx])))-((0.341*p->Cao))))/(2.0*F_RT)) : ((V_row[f2_idx]*((csm*(exp(V_row[za_idx])))-((0.341*p->Cao))))/((exp(V_row[za_idx]))-(1.0))));
    GlobalData_t yz1 = ((KmCao*Nai_row[Nai3_idx])+(mNao3*csm));
    GlobalData_t yz2 = ((mNai3*p->Cao)*(1.0+(csm/KmCai)));
    GlobalData_t yz4 = ((Nai_row[Nai3_idx]*p->Cao)+(Nao3*csm));
    GlobalData_t zw3 = (((V_row[zw3a_idx]*Nai_row[Nai3_idx])*p->Cao)-(((V_row[zw3b_idx]*Nao3)*csm)));
    GlobalData_t K1_inf = (AK1/(AK1+BK1));
    GlobalData_t j_Ca = (((140.0*p->g_Ca)*po)*iCa);
    GlobalData_t zw8 = (((yz1+yz2)+Nai_row[yz3_idx])+yz4);
    GlobalData_t ICa = (wca_m2*j_Ca);
    GlobalData_t IK1 = (((p->g_K1*g_ss)*K1_inf)*(V-(Ki_row[E_K_idx])));
    GlobalData_t Itos = (((p->g_tos*sv->xtos)*(sv->ytos+(0.5*V_row[rs_inf_idx])))*(V-(Ki_row[E_K_idx])));
    GlobalData_t j_NaCa = (((p->INaCamax*Ka)*zw3)/(V_row[zw4_idx]*zw8));
    GlobalData_t INaCa = (wca*j_NaCa);
    Iion = ((((((((INa+ICa)+Itof)+Itos)+IKr)+IKs)+IK1)+INaCa)+INaK);
    
    
    //Complete Forward Euler Update
    GlobalData_t Qr = ((sv->cjp<500.) ? 0.0 : (rq*(sv->cjp-(500.0))));
    GlobalData_t bpx = ((BCd*KCd)/((KCd+sv->cs)*(KCd+sv->cs)));
    GlobalData_t diff_cjp = ((sv->cj-(sv->cjp))/taua);
    GlobalData_t iCainward = ((iCa>0.) ? 0.0 : 1.0);
    GlobalData_t j_d = ((sv->cs-(sv->Cai))/taud);
    GlobalData_t ji_trpn = (((KTon*sv->Cai)*(Btrop-(sv->tropi)))-((KToff*sv->tropi)));
    GlobalData_t js_trpn = (((KTon*sv->cs)*(Btrop-(sv->trops)))-((KToff*sv->trops)));
    GlobalData_t jt_Ca = (((-g_dyad)*po)*iCa);
    GlobalData_t mempx = ((Bmem*Kmem)/((Kmem+sv->cs)*(Kmem+sv->cs)));
    GlobalData_t sarpx = ((Bsar*Ksar)/((Ksar+sv->cs)*(Ksar+sv->cs)));
    GlobalData_t spx = ((Bsr*KSR)/((KSR+sv->cs)*(KSR+sv->cs)));
    GlobalData_t Nsp = ((V_row[g_RyR_V_idx]*po)*(fabs(iCa)));
    GlobalData_t beta_cs = (1.0/((((1.0+bpx)+spx)+mempx)+sarpx));
    GlobalData_t diff_tropi = ji_trpn;
    GlobalData_t diff_trops = js_trpn;
    GlobalData_t j_leak = ((g_leak*cj_row[Lcj_idx])*(sv->cj-(sv->Cai)));
    GlobalData_t jt_SR = ((((V_row[gSR_V_idx]*Qr)*po)*(fabs(iCa)))*iCainward);
    GlobalData_t rT = (taur/(1.0-((taur*(diff_cjp/sv->cjp)))));
    GlobalData_t tauCa = (((V_row[R_V_idx]-(cp_row[TCa_idx]))*V_row[Pr_idx])+cp_row[TCa_idx]);
    GlobalData_t R_ica = (Nsp*Qr);
    GlobalData_t diff_Cai = (Cai_row[beta_ci_idx]*(((j_d-(Cai_row[j_up_idx]))+j_leak)-(ji_trpn)));
    GlobalData_t diff_c1 = (((((V_row[alpha_idx]*sv->c2)+(k2*sv->i1ca))+(k2t*sv->i1ba))+(r2*po))-(((((r1+V_row[beta_idx])+cp_row[k1_idx])+k1t)*sv->c1)));
    GlobalData_t diff_cj = (viovsr*(((-sv->j_rel)+Cai_row[j_up_idx])-(j_leak)));
    GlobalData_t diff_cp = ((jt_SR+jt_Ca)-(((sv->cp-(sv->cs))/taus)));
    GlobalData_t diff_cs = (beta_cs*((viovs*(((sv->j_rel-(j_d))-(j_Ca))+j_NaCa))-(js_trpn)));
    GlobalData_t k5 = ((1.0-(V_row[Ps_idx]))/tauCa);
    GlobalData_t k6 = ((cp_row[fca_idx]*V_row[Ps_idx])/tauCa);
    GlobalData_t R_sparks = ((R_ica+R_scr)*sv->cjp);
    GlobalData_t diff_c2 = ((((V_row[beta_idx]*sv->c1)+(k5*sv->i2ca))+(V_row[k5t_idx]*sv->i2ba))-((((k6+V_row[k6t_idx])+V_row[alpha_idx])*sv->c2)));
    GlobalData_t k4 = (((V_row[k3_idx]*V_row[a_b_idx])*(cp_row[k1_idx]/k2))*(k5/k6));
    GlobalData_t diff_i1ba = ((((k1t*sv->c1)+(V_row[k4t_idx]*sv->i2ba))+(s1t*po))-((((k2t+V_row[k3t_idx])+s2t)*sv->i1ba)));
    GlobalData_t diff_i1ca = ((((cp_row[k1_idx]*sv->c1)+(k4*sv->i2ca))+(cp_row[s1_idx]*po))-((((k2+V_row[k3_idx])+cp_row[s2_idx])*sv->i1ca)));
    GlobalData_t diff_i2ba = (((V_row[k3t_idx]*sv->i1ba)+(V_row[k6t_idx]*sv->c2))-(((V_row[k5t_idx]+V_row[k4t_idx])*sv->i2ba)));
    GlobalData_t diff_i2ca = (((V_row[k3_idx]*sv->i1ca)+(k6*sv->c2))-(((k4+k5)*sv->i2ca)));
    GlobalData_t diff_j_rel = (R_sparks-((sv->j_rel/rT)));
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t c1_new = sv->c1+diff_c1*dt;
    GlobalData_t c2_new = sv->c2+diff_c2*dt;
    GlobalData_t cj_new = sv->cj+diff_cj*dt;
    GlobalData_t cjp_new = sv->cjp+diff_cjp*dt;
    GlobalData_t cp_new = sv->cp+diff_cp*dt;
    GlobalData_t cs_new = sv->cs+diff_cs*dt;
    GlobalData_t i1ba_new = sv->i1ba+diff_i1ba*dt;
    GlobalData_t i1ca_new = sv->i1ca+diff_i1ca*dt;
    GlobalData_t i2ba_new = sv->i2ba+diff_i2ba*dt;
    GlobalData_t i2ca_new = sv->i2ca+diff_i2ca*dt;
    GlobalData_t j_rel_new = sv->j_rel+diff_j_rel*dt;
    GlobalData_t tropi_new = sv->tropi+diff_tropi*dt;
    GlobalData_t trops_new = sv->trops+diff_trops*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t h_rush_larsen_A = V_row[h_rush_larsen_A_idx];
    GlobalData_t h_rush_larsen_B = V_row[h_rush_larsen_B_idx];
    GlobalData_t j_rush_larsen_A = V_row[j_rush_larsen_A_idx];
    GlobalData_t j_rush_larsen_B = V_row[j_rush_larsen_B_idx];
    GlobalData_t m_rush_larsen_A = V_row[m_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_B = V_row[m_rush_larsen_B_idx];
    GlobalData_t xs1_rush_larsen_B = V_row[xs1_rush_larsen_B_idx];
    GlobalData_t xtof_rush_larsen_B = V_row[xtof_rush_larsen_B_idx];
    GlobalData_t ytof_rush_larsen_B = V_row[ytof_rush_larsen_B_idx];
    GlobalData_t ytos_rush_larsen_B = V_row[ytos_rush_larsen_B_idx];
    GlobalData_t xKr_rush_larsen_B = V_row[xKr_rush_larsen_B_idx];
    GlobalData_t xs1_rush_larsen_A = V_row[xs1_rush_larsen_A_idx];
    GlobalData_t xs2_rush_larsen_B = V_row[xs2_rush_larsen_B_idx];
    GlobalData_t xtof_rush_larsen_A = V_row[xtof_rush_larsen_A_idx];
    GlobalData_t xtos_rush_larsen_B = V_row[xtos_rush_larsen_B_idx];
    GlobalData_t ytof_rush_larsen_A = V_row[ytof_rush_larsen_A_idx];
    GlobalData_t xKr_rush_larsen_A = V_row[xKr_rush_larsen_A_idx];
    GlobalData_t xs2_rush_larsen_A = V_row[xs2_rush_larsen_A_idx];
    GlobalData_t xtos_rush_larsen_A = V_row[xtos_rush_larsen_A_idx];
    GlobalData_t ytos_rush_larsen_A = V_row[ytos_rush_larsen_A_idx];
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t xKr_new = xKr_rush_larsen_A+xKr_rush_larsen_B*sv->xKr;
    GlobalData_t xs1_new = xs1_rush_larsen_A+xs1_rush_larsen_B*sv->xs1;
    GlobalData_t xs2_new = xs2_rush_larsen_A+xs2_rush_larsen_B*sv->xs2;
    GlobalData_t xtof_new = xtof_rush_larsen_A+xtof_rush_larsen_B*sv->xtof;
    GlobalData_t xtos_new = xtos_rush_larsen_A+xtos_rush_larsen_B*sv->xtos;
    GlobalData_t ytof_new = ytof_rush_larsen_A+ytof_rush_larsen_B*sv->ytof;
    GlobalData_t ytos_new = ytos_rush_larsen_A+ytos_rush_larsen_B*sv->ytos;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Cai = Cai_new;
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->c1 = c1_new;
    sv->c2 = c2_new;
    sv->cj = cj_new;
    sv->cjp = cjp_new;
    sv->cp = cp_new;
    sv->cs = cs_new;
    sv->h = h_new;
    sv->i1ba = i1ba_new;
    sv->i1ca = i1ca_new;
    sv->i2ba = i2ba_new;
    sv->i2ca = i2ca_new;
    sv->j = j_new;
    sv->j_rel = j_rel_new;
    sv->m = m_new;
    sv->tropi = tropi_new;
    sv->trops = trops_new;
    sv->xKr = xKr_new;
    sv->xs1 = xs1_new;
    sv->xs2 = xs2_new;
    sv->xtof = xtof_new;
    sv->xtos = xtos_new;
    sv->ytof = ytof_new;
    sv->ytos = ytos_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_Campos(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Campos_trace_header.txt","wt");
    fprintf(theader->fd,
        "ICa\n"
        "IK1\n"
        "IKr\n"
        "IKs\n"
        "INa\n"
        "INaCa\n"
        "INaK\n"
        "Itof\n"
        "Itos\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Campos_Params *p  = (Campos_Params *)IF->params;

  Campos_state *sv_base = (Campos_state *)IF->sv_tab.y;
  Campos_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Nao3 = ((p->Nao*p->Nao)*p->Nao);
  GlobalData_t R_scr = (p->g_sp*spont_rate);
  GlobalData_t cj_init = p->CSR;
  GlobalData_t cjp_init = p->CSR;
  GlobalData_t g_ss = (sqrt((p->Ko/5.4)));
  GlobalData_t sigma = (((exp((p->Nao/67.3)))-(1.0))/7.0);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t E_K = (RT_F*(log((p->Ko/sv->Ki))));
  GlobalData_t E_Ks = (RT_F*(log(((p->Ko+(prnak*p->Nao))/(sv->Ki+(prnak*sv->Nai))))));
  GlobalData_t E_Na = (RT_F*(log((p->Nao/sv->Nai))));
  GlobalData_t Ka = (1.0/(1.0+(((cnaca/sv->cs)*(cnaca/sv->cs))*(cnaca/sv->cs))));
  GlobalData_t Nai3 = ((sv->Nai*sv->Nai)*sv->Nai);
  GlobalData_t Rv = (1.0/(1.0+(exp(((V+33.0)/22.4)))));
  GlobalData_t csm = (sv->cs/m2u);
  GlobalData_t f2 = (V*f1);
  GlobalData_t fNaK = (1.0/((1.0+(0.1245*(exp(((-0.1*V)*F_RT)))))+((0.0365*sigma)*(exp(((-V)*F_RT))))));
  GlobalData_t po = (1.0-((((((sv->c1+sv->c2)+sv->i1ca)+sv->i2ca)+sv->i1ba)+sv->i2ba)));
  GlobalData_t q_Ks = (1.0+(0.8*(1.0+(((0.5/sv->Cai)*(0.5/sv->Cai))*(0.5/sv->Cai)))));
  GlobalData_t rt2 = ((V+33.5)/10.0);
  GlobalData_t za = ((2.0*V)*F_RT);
  GlobalData_t zw3a = (exp(((V*xi)*F_RT)));
  GlobalData_t zw3b = (exp(((V*(xi-(1.0)))*F_RT)));
  GlobalData_t zw4 = (1.0+(ksat*(exp(((V*(xi-(1.0)))*F_RT)))));
  GlobalData_t AK1 = (1.02/(1.0+(exp((0.2385*((V-(E_K))-(59.215)))))));
  GlobalData_t BK1 = (((0.49124*(exp((0.08032*((V-(E_K))+5.476)))))+(exp((0.061750*((V-(E_K))-(594.31))))))/(1.0+(exp((-0.5143*((V-(E_K))+4.753))))));
  GlobalData_t IKr = ((((p->g_Kr*g_ss)*sv->xKr)*Rv)*(V-(E_K)));
  GlobalData_t IKs = ((((p->g_Ks*sv->xs1)*sv->xs2)*q_Ks)*(V-(E_Ks)));
  GlobalData_t INa = (((p->g_Na*((sv->m*sv->m)*sv->m))*(sv->h*sv->j))*(V-(E_Na)));
  GlobalData_t INaK = ((((p->INaKmax*fNaK)*(1.0/(1.0+(kmNai/sv->Nai))))*p->Ko)/(p->Ko+kmKo));
  GlobalData_t Itof = (((p->g_tof*sv->xtof)*sv->ytof)*(V-(E_K)));
  GlobalData_t iCa = (((fabs(za))<0.001) ? ((f1*((csm*(exp(za)))-((0.341*p->Cao))))/(2.0*F_RT)) : ((f2*((csm*(exp(za)))-((0.341*p->Cao))))/((exp(za))-(1.0))));
  GlobalData_t ytof_inf = (1.0/(1.0+(exp(rt2))));
  GlobalData_t yz1 = ((KmCao*Nai3)+(mNao3*csm));
  GlobalData_t yz2 = ((mNai3*p->Cao)*(1.0+(csm/KmCai)));
  GlobalData_t yz3 = ((KmCai*Nao3)*(1.0+(Nai3/mNai3)));
  GlobalData_t yz4 = ((Nai3*p->Cao)+(Nao3*csm));
  GlobalData_t zw3 = (((zw3a*Nai3)*p->Cao)-(((zw3b*Nao3)*csm)));
  GlobalData_t K1_inf = (AK1/(AK1+BK1));
  GlobalData_t j_Ca = (((140.0*p->g_Ca)*po)*iCa);
  GlobalData_t rs_inf = ytof_inf;
  GlobalData_t zw8 = (((yz1+yz2)+yz3)+yz4);
  GlobalData_t ICa = (wca_m2*j_Ca);
  GlobalData_t IK1 = (((p->g_K1*g_ss)*K1_inf)*(V-(E_K)));
  GlobalData_t Itos = (((p->g_tos*sv->xtos)*(sv->ytos+(0.5*rs_inf)))*(V-(E_K)));
  GlobalData_t j_NaCa = (((p->INaCamax*Ka)*zw3)/(zw4*zw8));
  GlobalData_t INaCa = (wca*j_NaCa);
  //Output the desired variables
  fprintf(file, "%4.12f\t", ICa);
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", Itof);
  fprintf(file, "%4.12f\t", Itos);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        