// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Title: Dynamical description of sinoatrial node pacemaking: improved mathematical model for primary pacemaker cell
*  Authors: Yasutaka Kurata, Ichiro Hisatome, Sunao Imanishi, and Toshishige Shibamoto
*  Year: 2002
*  Journal: American Journal of Physiology-Heart and Circulatory Physiology 2002 283:5, H2074-H2101
*  DOI: 10.1152/ajpheart.00900.2001
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Kurata.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Kurata(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Kurata( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define CM_tot (GlobalData_t)(0.045)
#define CQ_tot (GlobalData_t)(10.0)
#define CaimM_init (GlobalData_t)(3.795155379112219e-07)
#define Cao (GlobalData_t)(2.0)
#define Carel_init (GlobalData_t)(0.05109621361760)
#define Casub_init (GlobalData_t)(2.489829771897582e-04)
#define Caup_init (GlobalData_t)(1.24614474094109)
#define E_CaL (GlobalData_t)(45.0)
#define E_CaT (GlobalData_t)(45.0)
#define E_st (GlobalData_t)(37.4)
#define F (GlobalData_t)(96485.0)
#define GCaT (GlobalData_t)(0.458)
#define GNab (GlobalData_t)(0.0054)
#define INaK_max (GlobalData_t)(3.6)
#define K_1ni (GlobalData_t)(395.3)
#define K_1no (GlobalData_t)(1628.)
#define K_2ni (GlobalData_t)(2.289)
#define K_2no (GlobalData_t)(561.4)
#define K_3ni (GlobalData_t)(26.44)
#define K_3no (GlobalData_t)(4.663)
#define K_ci (GlobalData_t)(0.0207)
#define K_cni (GlobalData_t)(26.44)
#define K_co (GlobalData_t)(3.663)
#define K_mKp (GlobalData_t)(1.4)
#define K_mNap (GlobalData_t)(14.0)
#define K_mfCa (GlobalData_t)(0.00035)
#define Ki_init (GlobalData_t)(1.398701082677243e+02)
#define Ko (GlobalData_t)(5.4)
#define Krel (GlobalData_t)(0.0012)
#define Kup (GlobalData_t)(0.0006)
#define Mgi (GlobalData_t)(2.5)
#define Na_init (GlobalData_t)(0.151095)
#define Nai_init (GlobalData_t)(9.53940943560366)
#define Nao (GlobalData_t)(140.0)
#define Qci (GlobalData_t)(0.1369)
#define Qco (GlobalData_t)(0.0)
#define Qn (GlobalData_t)(0.4315)
#define R (GlobalData_t)(8314.4)
#define T (GlobalData_t)(310.15)
#define TC_tot (GlobalData_t)(0.031)
#define TMC_tot (GlobalData_t)(0.062)
#define V_init (GlobalData_t)(-70.)
#define Vol_cell (GlobalData_t)(3.5)
#define alpha_f_Ca (GlobalData_t)(0.035)
#define d_L_init (GlobalData_t)(0.00138412708286)
#define d_T_init (GlobalData_t)(0.00986335506081)
#define f_CMi_init (GlobalData_t)(0.13888438595936)
#define f_CMs_init (GlobalData_t)(0.09593230019178)
#define f_CQ_init (GlobalData_t)(0.05594783657901)
#define f_Ca_init (GlobalData_t)(0.53567907507444)
#define f_L_init (GlobalData_t)(0.32567825523113)
#define f_TC_init (GlobalData_t)(0.07125492282319)
#define f_TMC_init (GlobalData_t)(0.63423741728926)
#define f_TMM_init (GlobalData_t)(0.32298630261587)
#define f_T_init (GlobalData_t)(0.08331723416001)
#define hf_init (GlobalData_t)(0.0403522)
#define hs_init (GlobalData_t)(0.00310340)
#define k_NaCa (GlobalData_t)(125.0)
#define k_bCM (GlobalData_t)(0.542)
#define k_bCQ (GlobalData_t)(0.445)
#define k_bTC (GlobalData_t)(0.446)
#define k_bTMC (GlobalData_t)(0.00751)
#define k_bTMM (GlobalData_t)(0.751)
#define k_b_BAPTA (GlobalData_t)(0.11938)
#define k_b_EGTA (GlobalData_t)(0.0009983)
#define k_fCM (GlobalData_t)(227.7)
#define k_fCQ (GlobalData_t)(0.534)
#define k_fTC (GlobalData_t)(88.8)
#define k_fTMC (GlobalData_t)(227.7)
#define k_fTMM (GlobalData_t)(2.277)
#define k_f_BAPTA (GlobalData_t)(940.0)
#define k_f_EGTA (GlobalData_t)(6.7)
#define m_init (GlobalData_t)(0.16295546496562)
#define n_init (GlobalData_t)(0.90961866441464)
#define p_rel (GlobalData_t)(5.0)
#define p_up (GlobalData_t)(0.005)
#define paF_init (GlobalData_t)(0.55683380942037)
#define paS_init (GlobalData_t)(0.65512738205954)
#define pi_init (GlobalData_t)(0.81182637609361)
#define q_a_init (GlobalData_t)(0.69870393261115)
#define q_i_init (GlobalData_t)(0.31555406373068)
#define q_init (GlobalData_t)(0.18434378176445)
#define r_init (GlobalData_t)(0.00965531278600)
#define tau_dif_Ca (GlobalData_t)(0.04)
#define tau_tr (GlobalData_t)(60.0)
#define y_init (GlobalData_t)(0.04096413617865)
#define Cai_init (GlobalData_t)((CaimM_init*1000.))
#define E_Ks (GlobalData_t)((((R*T)/F)*(log(((Ko+(0.12*Nao))/(Ki_init+(0.12*Nai_init)))))))
#define E_Na (GlobalData_t)((((R*T)/F)*(log((Nao/Nai_init)))))
#define E_mh (GlobalData_t)((((R*T)/F)*(log(((Nao+(0.12*Ko))/(Nai_init+(0.12*Ki_init)))))))
#define Vol_rel (GlobalData_t)((0.0012*Vol_cell))
#define Vol_sub (GlobalData_t)((0.01*Vol_cell))
#define Vol_up (GlobalData_t)((0.0116*Vol_cell))
#define k_34 (GlobalData_t)((Nao/(K_3no+Nao)))
#define Vol_i (GlobalData_t)(((0.46*Vol_cell)-(Vol_sub)))



void initialize_params_Kurata( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Kurata_Params *p = (Kurata_Params *)IF->params;

  // Compute the regional constants
  {
    p->ACh = 0.;
    p->B_max = 0.56;
    p->Cm = 32.0;
    p->GCaL = 0.58;
    p->GKACh_max = (0.0011*(pow(Ko,0.41)));
    p->GKr = (0.025*(pow(Ko,0.59)));
    p->GKs = 0.0259;
    p->GNa = 0.;
    p->Gh = 0.357;
    p->GhK = 0.6167;
    p->GhNa = 0.3833;
    p->Gst = 0.015;
    p->Gsus = 0.02;
    p->Gto = 0.18;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_Kurata;

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Kurata( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Kurata_Params *p = (Kurata_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.

}



void    initialize_sv_Kurata( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Kurata_Params *p = (Kurata_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Kurata_state) );
  Kurata_state *sv_base = (Kurata_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Kurata_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->CaimM = CaimM_init;
    sv->Carel = Carel_init;
    sv->Casub = Casub_init;
    sv->Caup = Caup_init;
    sv->Ki = Ki_init;
    sv->Nai = Nai_init;
    V = V_init;
    sv->d_L = d_L_init;
    sv->d_T = d_T_init;
    sv->f_CMi = f_CMi_init;
    sv->f_CMs = f_CMs_init;
    sv->f_CQ = f_CQ_init;
    sv->f_Ca = f_Ca_init;
    sv->f_L = f_L_init;
    sv->f_T = f_T_init;
    sv->f_TC = f_TC_init;
    sv->f_TMC = f_TMC_init;
    sv->f_TMM = f_TMM_init;
    sv->hf = hf_init;
    sv->hs = hs_init;
    sv->m = m_init;
    sv->n = n_init;
    sv->paF = paF_init;
    sv->paS = paS_init;
    sv->pi = pi_init;
    sv->q = q_init;
    sv->q_a = q_a_init;
    sv->q_i = q_i_init;
    sv->r = r_init;
    sv->y = y_init;
    double E_K = (((R*T)/F)*(log((Ko/sv->Ki))));
    double FNa = (((0.0952*(exp((-0.063*(V+14.4)))))/(1.0+(1.66*(exp((-0.225*(V+43.7)))))))+0.0869);
    double ICaL = ((((p->GCaL*sv->d_L)*sv->f_L)*sv->f_Ca)*(V-(E_CaL)));
    double ICaT = (((GCaT*sv->d_T)*sv->f_T)*(V-(E_CaT)));
    double IKACh = (p->GKACh_max*(sv->Ki-((Ko*(exp((((-V)*F)/(R*T))))))));
    double IKs = (((p->GKs*sv->n)*sv->n)*(V-(E_Ks)));
    double INaK = (((INaK_max/(1.+(pow((K_mKp/Ko),1.2))))/(1.+(pow((K_mNap/sv->Nai),1.3))))/(1.+(exp(((-((V-(E_Na))+120.))/30.)))));
    double INab = (GNab*(V-(E_Na)));
    double IhNa = (((sv->y*sv->y)*(p->GhNa*(V-(E_Na))))*p->Gh);
    double Ist = (((p->Gst*sv->q_a)*sv->q_i)*(V-(E_st)));
    double d_i = ((1.0+((sv->Casub/K_ci)*((1.0+(exp(((((-Qci)*V)*F)/(R*T)))))+(sv->Nai/K_cni))))+((sv->Nai/K_1ni)*(1.0+((sv->Nai/K_2ni)*(1.0+(sv->Nai/K_3ni))))));
    double d_o = ((1.0+((Cao/K_co)*(1.0+(exp((((Qco*V)*F)/(R*T)))))))+((Nao/K_1no)*(1.0+((Nao/K_2no)*(1.0+(Nao/K_3no))))));
    double k_32 = (exp(((((Qn*V)*F)/(R*T))/2.)));
    double k_41 = (1.0/(exp(((((Qn*V)*F)/(R*T))/2.))));
    double k_43 = (sv->Nai/(K_3ni+sv->Nai));
    double IKr = (((p->GKr*((0.6*sv->paF)+(0.4*sv->paS)))*sv->pi)*(V-(E_K)));
    double INa = ((((p->GNa*sv->m)*sv->m)*sv->m)*(((1.-(FNa))*sv->hf)+(FNa*sv->hs)));
    double IhK = (((sv->y*sv->y)*(p->GhK*(V-(E_K))))*p->Gh);
    double Isus = ((p->Gsus*sv->r)*(V-(E_K)));
    double Ito = (((p->Gto*sv->q)*sv->r)*(V-(E_K)));
    double k_12 = (((sv->Casub/K_ci)*(exp(((((-Qci)*V)*F)/(R*T)))))/d_i);
    double k_14 = (((((sv->Nai/K_1ni)*(sv->Nai/K_2ni))*(1.0+(sv->Nai/K_3ni)))*(exp(((((Qn*V)*F)/(R*T))/2.))))/d_i);
    double k_21 = (((Cao/K_co)*(exp((((Qco*V)*F)/(R*T)))))/d_o);
    double k_23 = (((((Nao/K_1no)*(Nao/K_2no))*(1.0+(Nao/K_3no)))*(1.0/(exp(((((Qn*V)*F)/(R*T))/2.)))))/d_o);
    double Ih = (IhNa+IhK);
    double x_1 = (((k_34*k_41)*(k_23+k_21))+((k_21*k_32)*(k_43+k_41)));
    double x_2 = (((k_43*k_32)*(k_14+k_12))+((k_41*k_12)*(k_34+k_32)));
    double x_3 = (((k_43*k_14)*(k_23+k_21))+((k_12*k_23)*(k_43+k_41)));
    double x_4 = (((k_34*k_23)*(k_14+k_12))+((k_21*k_14)*(k_34+k_32)));
    double INaCa = ((k_NaCa*((k_21*x_2)-((k_12*x_1))))/(((x_1+x_2)+x_3)+x_4));
    Iion = ((((((((((((ICaL+ICaT)+IKr)+IKs)+Ito)+Isus)+Ih)+Ist)+INa)+INab)+IKACh)+INaK)+INaCa);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Kurata(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Kurata_Params *p  = (Kurata_Params *)IF->params;
  Kurata_state *sv_base = (Kurata_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Kurata_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t E_K = (((R*T)/F)*(log((Ko/sv->Ki))));
    GlobalData_t FNa = (((0.0952*(exp((-0.063*(V+14.4)))))/(1.0+(1.66*(exp((-0.225*(V+43.7)))))))+0.0869);
    GlobalData_t ICaL = ((((p->GCaL*sv->d_L)*sv->f_L)*sv->f_Ca)*(V-(E_CaL)));
    GlobalData_t ICaT = (((GCaT*sv->d_T)*sv->f_T)*(V-(E_CaT)));
    GlobalData_t IKACh = (p->GKACh_max*(sv->Ki-((Ko*(exp((((-V)*F)/(R*T))))))));
    GlobalData_t IKs = (((p->GKs*sv->n)*sv->n)*(V-(E_Ks)));
    GlobalData_t INaK = (((INaK_max/(1.+(pow((K_mKp/Ko),1.2))))/(1.+(pow((K_mNap/sv->Nai),1.3))))/(1.+(exp(((-((V-(E_Na))+120.))/30.)))));
    GlobalData_t INab = (GNab*(V-(E_Na)));
    GlobalData_t IhNa = (((sv->y*sv->y)*(p->GhNa*(V-(E_Na))))*p->Gh);
    GlobalData_t Ist = (((p->Gst*sv->q_a)*sv->q_i)*(V-(E_st)));
    GlobalData_t d_i = ((1.0+((sv->Casub/K_ci)*((1.0+(exp(((((-Qci)*V)*F)/(R*T)))))+(sv->Nai/K_cni))))+((sv->Nai/K_1ni)*(1.0+((sv->Nai/K_2ni)*(1.0+(sv->Nai/K_3ni))))));
    GlobalData_t d_o = ((1.0+((Cao/K_co)*(1.0+(exp((((Qco*V)*F)/(R*T)))))))+((Nao/K_1no)*(1.0+((Nao/K_2no)*(1.0+(Nao/K_3no))))));
    GlobalData_t k_32 = (exp(((((Qn*V)*F)/(R*T))/2.)));
    GlobalData_t k_41 = (1.0/(exp(((((Qn*V)*F)/(R*T))/2.))));
    GlobalData_t k_43 = (sv->Nai/(K_3ni+sv->Nai));
    GlobalData_t IKr = (((p->GKr*((0.6*sv->paF)+(0.4*sv->paS)))*sv->pi)*(V-(E_K)));
    GlobalData_t INa = ((((p->GNa*sv->m)*sv->m)*sv->m)*(((1.-(FNa))*sv->hf)+(FNa*sv->hs)));
    GlobalData_t IhK = (((sv->y*sv->y)*(p->GhK*(V-(E_K))))*p->Gh);
    GlobalData_t Isus = ((p->Gsus*sv->r)*(V-(E_K)));
    GlobalData_t Ito = (((p->Gto*sv->q)*sv->r)*(V-(E_K)));
    GlobalData_t k_12 = (((sv->Casub/K_ci)*(exp(((((-Qci)*V)*F)/(R*T)))))/d_i);
    GlobalData_t k_14 = (((((sv->Nai/K_1ni)*(sv->Nai/K_2ni))*(1.0+(sv->Nai/K_3ni)))*(exp(((((Qn*V)*F)/(R*T))/2.))))/d_i);
    GlobalData_t k_21 = (((Cao/K_co)*(exp((((Qco*V)*F)/(R*T)))))/d_o);
    GlobalData_t k_23 = (((((Nao/K_1no)*(Nao/K_2no))*(1.0+(Nao/K_3no)))*(1.0/(exp(((((Qn*V)*F)/(R*T))/2.)))))/d_o);
    GlobalData_t Ih = (IhNa+IhK);
    GlobalData_t x_1 = (((k_34*k_41)*(k_23+k_21))+((k_21*k_32)*(k_43+k_41)));
    GlobalData_t x_2 = (((k_43*k_32)*(k_14+k_12))+((k_41*k_12)*(k_34+k_32)));
    GlobalData_t x_3 = (((k_43*k_14)*(k_23+k_21))+((k_12*k_23)*(k_43+k_41)));
    GlobalData_t x_4 = (((k_34*k_23)*(k_14+k_12))+((k_21*k_14)*(k_34+k_32)));
    GlobalData_t INaCa = ((k_NaCa*((k_21*x_2)-((k_12*x_1))))/(((x_1+x_2)+x_3)+x_4));
    Iion = ((((((((((((ICaL+ICaT)+IKr)+IKs)+Ito)+Isus)+Ih)+Ist)+INa)+INab)+IKACh)+INaK)+INaCa);
    
    
    //Complete Forward Euler Update
    GlobalData_t diff_Ki = (((-p->Cm)*((((((IKr+IKs)+Ito)+Isus)+IhK)+IKACh)-((2.0*INaK))))/(F*Vol_i));
    GlobalData_t diff_Nai = (((-p->Cm)*(((((IhNa+Ist)+INab)+(3.0*INaK))+(3.0*INaCa))+INa))/(F*Vol_i));
    GlobalData_t diff_f_CMi = (((k_fCM*sv->CaimM)*(1.0-(sv->f_CMi)))-((k_bCM*sv->f_CMi)));
    GlobalData_t diff_f_CMs = (((k_fCM*sv->Casub)*(1.0-(sv->f_CMs)))-((k_bCM*sv->f_CMs)));
    GlobalData_t diff_f_CQ = (((k_fCQ*sv->Carel)*(1.0-(sv->f_CQ)))-((k_bCQ*sv->f_CQ)));
    GlobalData_t diff_f_TC = (((k_fTC*sv->CaimM)*(1.0-(sv->f_TC)))-((k_bTC*sv->f_TC)));
    GlobalData_t diff_f_TMC = (((k_fTMC*sv->CaimM)*((1.0-(sv->f_TMC))-(sv->f_TMM)))-((k_bTMC*sv->f_TMC)));
    GlobalData_t diff_f_TMM = (((k_fTMM*Mgi)*((1.0-(sv->f_TMC))-(sv->f_TMM)))-((k_bTMM*sv->f_TMM)));
    GlobalData_t j_Ca_dif = ((sv->Casub-(sv->CaimM))/tau_dif_Ca);
    GlobalData_t j_rel = ((p_rel*(sv->Carel-(sv->Casub)))/(1.+((Krel/sv->Casub)*(Krel/sv->Casub))));
    GlobalData_t j_tr = ((sv->Caup-(sv->Carel))/tau_tr);
    GlobalData_t j_up = (p_up/(1.0+(Kup/sv->CaimM)));
    GlobalData_t diff_CaimM = ((((j_Ca_dif*Vol_sub)-((j_up*Vol_up)))/Vol_i)-((((CM_tot*diff_f_CMi)+(TC_tot*diff_f_TC))+(TMC_tot*diff_f_TMC))));
    GlobalData_t diff_Carel = ((j_tr-(j_rel))-((CQ_tot*diff_f_CQ)));
    GlobalData_t diff_Casub = (((((((-p->Cm)*((ICaL+ICaT)-((2.0*INaCa))))/(2.0*F))+(j_rel*Vol_rel))/Vol_sub)-(j_Ca_dif))-((CM_tot*diff_f_CMs)));
    GlobalData_t diff_Caup = (j_up-(((j_tr*Vol_rel)/Vol_up)));
    GlobalData_t CaimM_new = sv->CaimM+diff_CaimM*dt;
    GlobalData_t Carel_new = sv->Carel+diff_Carel*dt;
    GlobalData_t Casub_new = sv->Casub+diff_Casub*dt;
    GlobalData_t Caup_new = sv->Caup+diff_Caup*dt;
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t f_CMi_new = sv->f_CMi+diff_f_CMi*dt;
    GlobalData_t f_CMs_new = sv->f_CMs+diff_f_CMs*dt;
    GlobalData_t f_CQ_new = sv->f_CQ+diff_f_CQ*dt;
    GlobalData_t f_TC_new = sv->f_TC+diff_f_TC*dt;
    GlobalData_t f_TMC_new = sv->f_TMC+diff_f_TMC*dt;
    GlobalData_t f_TMM_new = sv->f_TMM+diff_f_TMM*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t alpha0_d_L = ((-0.02839*((V==-35.) ? -2.5 : ((V+35.)/((exp(((-(V+35.))/2.5)))-(1.)))))-((0.0849*((V==0.) ? -4.8 : (V/((exp(((-V)/4.8)))-(1.)))))));
    GlobalData_t alpha0_q_a = (1./((0.15*(exp(((-V)/11.))))+(0.2*(exp(((-V)/700.))))));
    GlobalData_t alpha_n = (0.014/(1.+(exp(((-(V-(40.)))/9.)))));
    GlobalData_t alpha_q_i = (0.1504/((3100.*(exp((V/13.))))+(700.*(exp((V/70.))))));
    GlobalData_t beta0_d_L = (0.01143*((V==5.) ? 2.5 : ((V-(5.))/((exp(((V-(5.))/2.5)))-(1.)))));
    GlobalData_t beta0_q_a = (1./((16.*(exp((V/8.))))+(15.*(exp((V/50.))))));
    GlobalData_t beta_n = (0.001*(exp(((-V)/45.))));
    GlobalData_t beta_q_i = ((0.1504/((95.*(exp(((-V)/10.))))+(50.*(exp(((-V)/700.))))))+(0.000229/(1.+(exp(((-V)/5.))))));
    GlobalData_t d_L_inf = (1./(1.+(exp(((-(V+14.1))/6.)))));
    GlobalData_t d_T_inf = (1./(1.+(exp(((-(V+26.3))/6.)))));
    GlobalData_t f_Ca_inf = (K_mfCa/(K_mfCa+sv->Casub));
    GlobalData_t f_L_inf = (1./(1.+(exp(((V+30.)/5.)))));
    GlobalData_t f_T_inf = (1./(1.+(exp(((V+61.7)/5.6)))));
    GlobalData_t hs_inf = (1./(1.+(exp(((V+70.6)/6.4)))));
    GlobalData_t m_inf = (pow((1.+(exp(((-(V+21.4))/6.)))),-0.333));
    GlobalData_t paF_inf = (1./(1.+(exp(((-(V+23.3))/10.6)))));
    GlobalData_t pi_inf = (1./(1.+(exp(((V+28.6)/17.1)))));
    GlobalData_t q_a_inf = (1./(1.+(exp(((-(V+57.))/5.)))));
    GlobalData_t q_inf = (1./(1.+(exp(((V+49.)/13.)))));
    GlobalData_t r_inf = (1./(1.+(exp(((-(V-(19.3)))/15.)))));
    GlobalData_t tau_d_T = (1./((1.068*(exp(((V+26.3)/30.))))+(1.068*(exp(((-(V+26.3))/30.))))));
    GlobalData_t tau_f_L = ((257.1*(exp((-(pow(((V+32.5)/13.9),2.))))))+44.3);
    GlobalData_t tau_f_T = (1./((0.0153*(exp(((-(V+61.7))/83.3))))+(0.015*(exp(((V+61.7)/15.38))))));
    GlobalData_t tau_hf = (0.4757/((1./(15600.*(exp(((V-(20.))/16.949)))))+(1./(0.92+(0.037*(exp(((-(V-(20.)))/11.494))))))));
    GlobalData_t tau_hs = (0.4757/((1./(168000.*(exp(((V-(20.))/18.182)))))+(1./(2.58+(0.94*(exp(((-(V-(20.)))/15.873))))))));
    GlobalData_t tau_m = ((0.6247/((0.832*(exp(((-(V+36.))/2.985))))+(0.6227*(exp(((V+45.01)/12.195))))))+0.04);
    GlobalData_t tau_paF = (0.84655/((0.0372*(exp((V/15.9))))+(0.00096*(exp(((-V)/22.5))))));
    GlobalData_t tau_paS = (0.84655/((0.0042*(exp((V/17.))))+(0.00015*(exp(((-V)/21.6))))));
    GlobalData_t tau_pi = (1./((0.1*(exp(((-V)/54.645))))+(0.656*(exp((V/106.157))))));
    GlobalData_t tau_q = ((65.17/(0.57*(exp(((-0.08*(V+44.))+(0.065*(exp((0.1*(V+45.93))))))))))+10.1);
    GlobalData_t tau_r = ((21.826/((1.037*(exp((0.09*(V+30.61)))))+(0.369*(exp((-0.12*(V+23.84)))))))+4.172);
    GlobalData_t tau_y = (0.71665/((exp(((-(V+386.9))/45.3)))+(exp(((V-(73.08))/19.23)))));
    GlobalData_t y_inf = (1./(1.+(exp(((V+64.)/13.5)))));
    GlobalData_t d_T_rush_larsen_B = (exp(((-dt)/tau_d_T)));
    GlobalData_t d_T_rush_larsen_C = (expm1(((-dt)/tau_d_T)));
    GlobalData_t f_L_rush_larsen_B = (exp(((-dt)/tau_f_L)));
    GlobalData_t f_L_rush_larsen_C = (expm1(((-dt)/tau_f_L)));
    GlobalData_t f_T_rush_larsen_B = (exp(((-dt)/tau_f_T)));
    GlobalData_t f_T_rush_larsen_C = (expm1(((-dt)/tau_f_T)));
    GlobalData_t hf_inf = hs_inf;
    GlobalData_t hf_rush_larsen_B = (exp(((-dt)/tau_hf)));
    GlobalData_t hf_rush_larsen_C = (expm1(((-dt)/tau_hf)));
    GlobalData_t hs_rush_larsen_B = (exp(((-dt)/tau_hs)));
    GlobalData_t hs_rush_larsen_C = (expm1(((-dt)/tau_hs)));
    GlobalData_t m_rush_larsen_B = (exp(((-dt)/tau_m)));
    GlobalData_t m_rush_larsen_C = (expm1(((-dt)/tau_m)));
    GlobalData_t n_rush_larsen_A = (((-alpha_n)/(alpha_n+beta_n))*(expm1(((-dt)*(alpha_n+beta_n)))));
    GlobalData_t n_rush_larsen_B = (exp(((-dt)*(alpha_n+beta_n))));
    GlobalData_t paF_rush_larsen_B = (exp(((-dt)/tau_paF)));
    GlobalData_t paF_rush_larsen_C = (expm1(((-dt)/tau_paF)));
    GlobalData_t paS_inf = paF_inf;
    GlobalData_t paS_rush_larsen_B = (exp(((-dt)/tau_paS)));
    GlobalData_t paS_rush_larsen_C = (expm1(((-dt)/tau_paS)));
    GlobalData_t pi_rush_larsen_B = (exp(((-dt)/tau_pi)));
    GlobalData_t pi_rush_larsen_C = (expm1(((-dt)/tau_pi)));
    GlobalData_t q_i_rush_larsen_A = (((-alpha_q_i)/(alpha_q_i+beta_q_i))*(expm1(((-dt)*(alpha_q_i+beta_q_i)))));
    GlobalData_t q_i_rush_larsen_B = (exp(((-dt)*(alpha_q_i+beta_q_i))));
    GlobalData_t q_rush_larsen_B = (exp(((-dt)/tau_q)));
    GlobalData_t q_rush_larsen_C = (expm1(((-dt)/tau_q)));
    GlobalData_t r_rush_larsen_B = (exp(((-dt)/tau_r)));
    GlobalData_t r_rush_larsen_C = (expm1(((-dt)/tau_r)));
    GlobalData_t tau_d_L = (1./(alpha0_d_L+beta0_d_L));
    GlobalData_t tau_f_Ca = (f_Ca_inf/alpha_f_Ca);
    GlobalData_t tau_q_a = (1./(alpha0_q_a+beta0_q_a));
    GlobalData_t y_rush_larsen_B = (exp(((-dt)/tau_y)));
    GlobalData_t y_rush_larsen_C = (expm1(((-dt)/tau_y)));
    GlobalData_t d_L_rush_larsen_B = (exp(((-dt)/tau_d_L)));
    GlobalData_t d_L_rush_larsen_C = (expm1(((-dt)/tau_d_L)));
    GlobalData_t d_T_rush_larsen_A = ((-d_T_inf)*d_T_rush_larsen_C);
    GlobalData_t f_Ca_rush_larsen_B = (exp(((-dt)/tau_f_Ca)));
    GlobalData_t f_Ca_rush_larsen_C = (expm1(((-dt)/tau_f_Ca)));
    GlobalData_t f_L_rush_larsen_A = ((-f_L_inf)*f_L_rush_larsen_C);
    GlobalData_t f_T_rush_larsen_A = ((-f_T_inf)*f_T_rush_larsen_C);
    GlobalData_t hf_rush_larsen_A = ((-hf_inf)*hf_rush_larsen_C);
    GlobalData_t hs_rush_larsen_A = ((-hs_inf)*hs_rush_larsen_C);
    GlobalData_t m_rush_larsen_A = ((-m_inf)*m_rush_larsen_C);
    GlobalData_t paF_rush_larsen_A = ((-paF_inf)*paF_rush_larsen_C);
    GlobalData_t paS_rush_larsen_A = ((-paS_inf)*paS_rush_larsen_C);
    GlobalData_t pi_rush_larsen_A = ((-pi_inf)*pi_rush_larsen_C);
    GlobalData_t q_a_rush_larsen_B = (exp(((-dt)/tau_q_a)));
    GlobalData_t q_a_rush_larsen_C = (expm1(((-dt)/tau_q_a)));
    GlobalData_t q_rush_larsen_A = ((-q_inf)*q_rush_larsen_C);
    GlobalData_t r_rush_larsen_A = ((-r_inf)*r_rush_larsen_C);
    GlobalData_t y_rush_larsen_A = ((-y_inf)*y_rush_larsen_C);
    GlobalData_t d_L_rush_larsen_A = ((-d_L_inf)*d_L_rush_larsen_C);
    GlobalData_t f_Ca_rush_larsen_A = ((-f_Ca_inf)*f_Ca_rush_larsen_C);
    GlobalData_t q_a_rush_larsen_A = ((-q_a_inf)*q_a_rush_larsen_C);
    GlobalData_t d_L_new = d_L_rush_larsen_A+d_L_rush_larsen_B*sv->d_L;
    GlobalData_t d_T_new = d_T_rush_larsen_A+d_T_rush_larsen_B*sv->d_T;
    GlobalData_t f_Ca_new = f_Ca_rush_larsen_A+f_Ca_rush_larsen_B*sv->f_Ca;
    GlobalData_t f_L_new = f_L_rush_larsen_A+f_L_rush_larsen_B*sv->f_L;
    GlobalData_t f_T_new = f_T_rush_larsen_A+f_T_rush_larsen_B*sv->f_T;
    GlobalData_t hf_new = hf_rush_larsen_A+hf_rush_larsen_B*sv->hf;
    GlobalData_t hs_new = hs_rush_larsen_A+hs_rush_larsen_B*sv->hs;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t n_new = n_rush_larsen_A+n_rush_larsen_B*sv->n;
    GlobalData_t paF_new = paF_rush_larsen_A+paF_rush_larsen_B*sv->paF;
    GlobalData_t paS_new = paS_rush_larsen_A+paS_rush_larsen_B*sv->paS;
    GlobalData_t pi_new = pi_rush_larsen_A+pi_rush_larsen_B*sv->pi;
    GlobalData_t q_new = q_rush_larsen_A+q_rush_larsen_B*sv->q;
    GlobalData_t q_a_new = q_a_rush_larsen_A+q_a_rush_larsen_B*sv->q_a;
    GlobalData_t q_i_new = q_i_rush_larsen_A+q_i_rush_larsen_B*sv->q_i;
    GlobalData_t r_new = r_rush_larsen_A+r_rush_larsen_B*sv->r;
    GlobalData_t y_new = y_rush_larsen_A+y_rush_larsen_B*sv->y;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->CaimM = CaimM_new;
    sv->Carel = Carel_new;
    sv->Casub = Casub_new;
    sv->Caup = Caup_new;
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->d_L = d_L_new;
    sv->d_T = d_T_new;
    sv->f_CMi = f_CMi_new;
    sv->f_CMs = f_CMs_new;
    sv->f_CQ = f_CQ_new;
    sv->f_Ca = f_Ca_new;
    sv->f_L = f_L_new;
    sv->f_T = f_T_new;
    sv->f_TC = f_TC_new;
    sv->f_TMC = f_TMC_new;
    sv->f_TMM = f_TMM_new;
    sv->hf = hf_new;
    sv->hs = hs_new;
    sv->m = m_new;
    sv->n = n_new;
    sv->paF = paF_new;
    sv->paS = paS_new;
    sv->pi = pi_new;
    sv->q = q_new;
    sv->q_a = q_a_new;
    sv->q_i = q_i_new;
    sv->r = r_new;
    sv->y = y_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_Kurata(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Kurata_trace_header.txt","wt");
    fprintf(theader->fd,
        "Cai\n"
        "ICaL\n"
        "ICaT\n"
        "IKACh\n"
        "IKr\n"
        "IKs\n"
        "INa\n"
        "INaCa\n"
        "INaK\n"
        "INab\n"
        "IhK\n"
        "IhNa\n"
        "Ist\n"
        "Isus\n"
        "Ito\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Kurata_Params *p  = (Kurata_Params *)IF->params;

  Kurata_state *sv_base = (Kurata_state *)IF->sv_tab.y;
  Kurata_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t Cai = (sv->CaimM*1000.);
  GlobalData_t E_K = (((R*T)/F)*(log((Ko/sv->Ki))));
  GlobalData_t FNa = (((0.0952*(exp((-0.063*(V+14.4)))))/(1.0+(1.66*(exp((-0.225*(V+43.7)))))))+0.0869);
  GlobalData_t ICaL = ((((p->GCaL*sv->d_L)*sv->f_L)*sv->f_Ca)*(V-(E_CaL)));
  GlobalData_t ICaT = (((GCaT*sv->d_T)*sv->f_T)*(V-(E_CaT)));
  GlobalData_t IKACh = (p->GKACh_max*(sv->Ki-((Ko*(exp((((-V)*F)/(R*T))))))));
  GlobalData_t IKs = (((p->GKs*sv->n)*sv->n)*(V-(E_Ks)));
  GlobalData_t INaK = (((INaK_max/(1.+(pow((K_mKp/Ko),1.2))))/(1.+(pow((K_mNap/sv->Nai),1.3))))/(1.+(exp(((-((V-(E_Na))+120.))/30.)))));
  GlobalData_t INab = (GNab*(V-(E_Na)));
  GlobalData_t IhNa = (((sv->y*sv->y)*(p->GhNa*(V-(E_Na))))*p->Gh);
  GlobalData_t Ist = (((p->Gst*sv->q_a)*sv->q_i)*(V-(E_st)));
  GlobalData_t d_i = ((1.0+((sv->Casub/K_ci)*((1.0+(exp(((((-Qci)*V)*F)/(R*T)))))+(sv->Nai/K_cni))))+((sv->Nai/K_1ni)*(1.0+((sv->Nai/K_2ni)*(1.0+(sv->Nai/K_3ni))))));
  GlobalData_t d_o = ((1.0+((Cao/K_co)*(1.0+(exp((((Qco*V)*F)/(R*T)))))))+((Nao/K_1no)*(1.0+((Nao/K_2no)*(1.0+(Nao/K_3no))))));
  GlobalData_t k_32 = (exp(((((Qn*V)*F)/(R*T))/2.)));
  GlobalData_t k_41 = (1.0/(exp(((((Qn*V)*F)/(R*T))/2.))));
  GlobalData_t k_43 = (sv->Nai/(K_3ni+sv->Nai));
  GlobalData_t IKr = (((p->GKr*((0.6*sv->paF)+(0.4*sv->paS)))*sv->pi)*(V-(E_K)));
  GlobalData_t INa = ((((p->GNa*sv->m)*sv->m)*sv->m)*(((1.-(FNa))*sv->hf)+(FNa*sv->hs)));
  GlobalData_t IhK = (((sv->y*sv->y)*(p->GhK*(V-(E_K))))*p->Gh);
  GlobalData_t Isus = ((p->Gsus*sv->r)*(V-(E_K)));
  GlobalData_t Ito = (((p->Gto*sv->q)*sv->r)*(V-(E_K)));
  GlobalData_t k_12 = (((sv->Casub/K_ci)*(exp(((((-Qci)*V)*F)/(R*T)))))/d_i);
  GlobalData_t k_14 = (((((sv->Nai/K_1ni)*(sv->Nai/K_2ni))*(1.0+(sv->Nai/K_3ni)))*(exp(((((Qn*V)*F)/(R*T))/2.))))/d_i);
  GlobalData_t k_21 = (((Cao/K_co)*(exp((((Qco*V)*F)/(R*T)))))/d_o);
  GlobalData_t k_23 = (((((Nao/K_1no)*(Nao/K_2no))*(1.0+(Nao/K_3no)))*(1.0/(exp(((((Qn*V)*F)/(R*T))/2.)))))/d_o);
  GlobalData_t x_1 = (((k_34*k_41)*(k_23+k_21))+((k_21*k_32)*(k_43+k_41)));
  GlobalData_t x_2 = (((k_43*k_32)*(k_14+k_12))+((k_41*k_12)*(k_34+k_32)));
  GlobalData_t x_3 = (((k_43*k_14)*(k_23+k_21))+((k_12*k_23)*(k_43+k_41)));
  GlobalData_t x_4 = (((k_34*k_23)*(k_14+k_12))+((k_21*k_14)*(k_34+k_32)));
  GlobalData_t INaCa = ((k_NaCa*((k_21*x_2)-((k_12*x_1))))/(((x_1+x_2)+x_3)+x_4));
  //Output the desired variables
  fprintf(file, "%4.12f\t", Cai);
  fprintf(file, "%4.12f\t", ICaL);
  fprintf(file, "%4.12f\t", ICaT);
  fprintf(file, "%4.12f\t", IKACh);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", INab);
  fprintf(file, "%4.12f\t", IhK);
  fprintf(file, "%4.12f\t", IhNa);
  fprintf(file, "%4.12f\t", Ist);
  fprintf(file, "%4.12f\t", Isus);
  fprintf(file, "%4.12f\t", Ito);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        