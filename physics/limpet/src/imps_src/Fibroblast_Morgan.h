// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Morgan R, Colman MA, Chubb H, Seemann G and Aslanidi OV
*  Year: 2016
*  Title: Slow Conduction in the Border Zones of Patchy Fibrosis Stabilizes the Drivers for Atrial Fibrillation: Insights from Multi-Scale Human Atrial Modeling
*  Journal: Front. Physiol. 7,474
*  DOI: 10.3389/fphys.2016.00474
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __FIBROBLAST_MORGAN_H__
#define __FIBROBLAST_MORGAN_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Fibroblast_Morgan_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG
#define Fibroblast_Morgan_MODDAT Iion_DATA_FLAG

struct Fibroblast_Morgan_Params {
    GlobalData_t GbNa;
    GlobalData_t Ggj;
    GlobalData_t Gto;
    GlobalData_t Ki_init;
    GlobalData_t Ko;
    GlobalData_t Nai_init;
    GlobalData_t Nao;
    GlobalData_t Vfb_init;
    GlobalData_t fact_gkur;
    GlobalData_t fact_gt0;
    GlobalData_t fb_myo_surf_ratio;
    GlobalData_t fb_surf;
    GlobalData_t maxINaK;
    GlobalData_t num_fb;

};

struct Fibroblast_Morgan_state {
    GlobalData_t Ki;
    GlobalData_t Nai;
    GlobalData_t Vfb;
    Gatetype oa;
    Gatetype oi;
    Gatetype ua;
    Gatetype ui;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Fibroblast_Morgan(ION_IF *);
void construct_tables_Fibroblast_Morgan(ION_IF *);
void destroy_Fibroblast_Morgan(ION_IF *);
void initialize_sv_Fibroblast_Morgan(ION_IF *, GlobalData_t**);
void compute_Fibroblast_Morgan(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
