// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Mitchell, C.C., Schaeffer, D.G
*  Year: 2003
*  Title: A two-current model for the dynamics of cardiac membrane
*  Journal: Bull. Math. Biol. 65, 767-793
*  DOI: 10.1016/S0092-8240(03)00041-7
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __MITCHELLSCHAEFFER_H__
#define __MITCHELLSCHAEFFER_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define MitchellSchaeffer_REQDAT Vm_DATA_FLAG
#define MitchellSchaeffer_MODDAT Iion_DATA_FLAG

struct MitchellSchaeffer_Params {
    GlobalData_t V_gate;
    GlobalData_t V_max;
    GlobalData_t V_min;
    GlobalData_t a_crit;
    GlobalData_t tau_close;
    GlobalData_t tau_in;
    GlobalData_t tau_open;
    GlobalData_t tau_out;

};

struct MitchellSchaeffer_state {
    GlobalData_t V_gate;
    GlobalData_t V_max;
    GlobalData_t V_min;
    GlobalData_t a_crit;
    GlobalData_t h;
    GlobalData_t tau_close;
    GlobalData_t tau_in;
    GlobalData_t tau_open;
    GlobalData_t tau_out;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_MitchellSchaeffer(ION_IF *);
void construct_tables_MitchellSchaeffer(ION_IF *);
void destroy_MitchellSchaeffer(ION_IF *);
void initialize_sv_MitchellSchaeffer(ION_IF *, GlobalData_t**);
void compute_MitchellSchaeffer(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
