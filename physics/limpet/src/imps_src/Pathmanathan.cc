// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Pras Pathmanathan, Richard A. Gray
*  Year: 2013
*  Title: Verification of computational models of cardiac electro-physiology
*  Journal: International Journal for Numerical Methods in Biomedical Engineering, 30(5):525-544
*  DOI: 10.1002/cnm.2615
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Pathmanathan.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Pathmanathan(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Pathmanathan( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define V_init (GlobalData_t)(0.)
#define diff_u3 (GlobalData_t)(0.)
#define u1_init (GlobalData_t)(0.)
#define u2_init (GlobalData_t)(0.)
#define u3_init (GlobalData_t)(0.)



void initialize_params_Pathmanathan( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Pathmanathan_Params *p = (Pathmanathan_Params *)IF->params;

  // Compute the regional constants
  {
    p->Cm = 200.;
    p->beta = 1.;
    p->xi = 3.;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Pathmanathan( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Pathmanathan_Params *p = (Pathmanathan_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.

}



void    initialize_sv_Pathmanathan( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Pathmanathan_Params *p = (Pathmanathan_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Pathmanathan_state) );
  Pathmanathan_state *sv_base = (Pathmanathan_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Pathmanathan_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    V = V_init;
    sv->u1 = u1_init;
    sv->u2 = u2_init;
    sv->u3 = u3_init;
    Iion = ((((((-(p->Cm/2.))*((sv->u1+sv->u3)-(V)))*(square(sv->u2)))*(V-(sv->u3)))+((p->beta*(V-(sv->u3)))/p->xi))/200.);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Pathmanathan(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Pathmanathan_Params *p  = (Pathmanathan_Params *)IF->params;
  Pathmanathan_state *sv_base = (Pathmanathan_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Pathmanathan_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    Iion = ((((((-(p->Cm/2.))*((sv->u1+sv->u3)-(V)))*(square(sv->u2)))*(V-(sv->u3)))+((p->beta*(V-(sv->u3)))/p->xi))/200.);
    
    
    //Complete Forward Euler Update
    GlobalData_t diff_u1 = (((square(((sv->u1+sv->u3)-(V))))*(square(sv->u2)))+(((0.5*((sv->u1+sv->u3)-(V)))*(square(sv->u2)))*(V-(sv->u3))));
    GlobalData_t diff_u2 = ((-((sv->u1+sv->u3)-(V)))*(cube(sv->u2)));
    GlobalData_t u1_new = sv->u1+diff_u1*dt;
    GlobalData_t u2_new = sv->u2+diff_u2*dt;
    GlobalData_t u3_new = sv->u3+diff_u3*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    sv->u1 = u1_new;
    sv->u2 = u2_new;
    sv->u3 = u3_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        