// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: A. Nygren , C. Fiset , L. Firek, J. W. Clark , D. S. Lindblad , R. B. Clark , and W. R. Giles
*  Year: 1998
*  Title: Mathematical model of an adult human atrial cell: the role of K+ currents in repolarization
*  Journal: Circulation Research,9-23,82(1),63-81
*  DOI: 10.1161/01.res.82.1.63
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __NYGREN_H__
#define __NYGREN_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Nygren_REQDAT Vm_DATA_FLAG
#define Nygren_MODDAT Iion_DATA_FLAG

struct Nygren_Params {
    GlobalData_t GCaB;
    GlobalData_t GCaL;
    GlobalData_t GK1;
    GlobalData_t GKS;
    GlobalData_t GKr;
    GlobalData_t GNaB;
    GlobalData_t Gsus;
    GlobalData_t Gt;
    GlobalData_t KNaCa;
    GlobalData_t maxINaK;

};

struct Nygren_state {
    GlobalData_t Ca_d;
    GlobalData_t Ca_rel;
    GlobalData_t Ca_up;
    GlobalData_t Cai;
    GlobalData_t F1;
    GlobalData_t F2;
    Gatetype O_C;
    Gatetype O_Calse;
    Gatetype O_TC;
    GlobalData_t O_TMgC;
    GlobalData_t O_TMgMg;
    Gatetype d_L;
    Gatetype f_L1;
    Gatetype f_L2;
    Gatetype h1;
    Gatetype h2;
    Gatetype m;
    Gatetype n;
    Gatetype p_a;
    Gatetype r;
    Gatetype r_sus;
    Gatetype s;
    Gatetype s_sus;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Nygren(ION_IF *);
void construct_tables_Nygren(ION_IF *);
void destroy_Nygren(ION_IF *);
void initialize_sv_Nygren(ION_IF *, GlobalData_t**);
void compute_Nygren(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
