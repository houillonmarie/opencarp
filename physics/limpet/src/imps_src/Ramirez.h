// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Rafael J. Ramirez, Stanley Nattel, and Marc Courtemanche
*  Year: 2000
*  Title: Mathematical analysis of canine atrial action potentials: rate, regional factors, and electrical remodeling
*  Journal: American Journal of Physiology-Heart and Circulatory Physiology, 279(4), 1767-1785
*  DOI: 10.1152/ajpheart.2000.279.4.H1767
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __RAMIREZ_H__
#define __RAMIREZ_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Ramirez_REQDAT Vm_DATA_FLAG
#define Ramirez_MODDAT Iion_DATA_FLAG

struct Ramirez_Params {
    GlobalData_t Cao;
    GlobalData_t Clo;
    GlobalData_t GCaL;
    GlobalData_t GClCa;
    GlobalData_t GK1;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t GbCa;
    GlobalData_t GbNa;
    GlobalData_t Gto;
    GlobalData_t Ko;
    GlobalData_t Nao;
    GlobalData_t maxCa_up;
    GlobalData_t maxINaCa;
    GlobalData_t maxINaK;
    GlobalData_t maxIpCa;
    GlobalData_t maxIup;
    GlobalData_t tau_tr;
    GlobalData_t tau_u;

};

struct Ramirez_state {
    GlobalData_t CaCmdn;
    GlobalData_t CaCsqn;
    GlobalData_t CaRel;
    GlobalData_t CaTrpn;
    GlobalData_t CaUp;
    GlobalData_t Cai;
    GlobalData_t Cli;
    GlobalData_t Ki;
    GlobalData_t Nai;
    Gatetype Oa;
    Gatetype Oi;
    Gatetype QCa;
    Gatetype Ua;
    Gatetype Ui;
    Gatetype d;
    Gatetype f;
    Gatetype fCa;
    Gatetype h;
    Gatetype j;
    Gatetype m;
    Gatetype u;
    Gatetype v;
    Gatetype w;
    Gatetype xr;
    Gatetype xs;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Ramirez(ION_IF *);
void construct_tables_Ramirez(ION_IF *);
void destroy_Ramirez(ION_IF *);
void initialize_sv_Ramirez(ION_IF *, GlobalData_t**);
void compute_Ramirez(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
