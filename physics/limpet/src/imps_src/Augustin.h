// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  DOI: GrandiPanditVoigt model (doi:10.1161/CIRCRESAHA.111.253955) coupled to the Land tension model (doi:10.1113/jphysiol.2012.231928)
*  Comment: GrandiPanditVoigt model (doi:10.1161/CIRCRESAHA.111.253955) coupled to the Land tension model (doi:10.1113/jphysiol.2012.231928)
*  Authors: Christoph M Augustin, Aurel Neic, Manfred Liebmann, Anton J Prassl, Steven A Niederer, Gundolf Haase, Gernot Plank
*  Year: 2016
*  Title: Anatomically accurate high resolution modeling of human whole heart electromechanics: A strongly scalable algebraic multigrid solver method for nonlinear deformation
*  Journal: J Comput Phys. 305:622-646
*  DOI: 10.1016/j.jcp.2015.10.045
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __AUGUSTIN_H__
#define __AUGUSTIN_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Augustin_REQDAT Vm_DATA_FLAG|delLambda_DATA_FLAG|Lambda_DATA_FLAG
#define Augustin_MODDAT Iion_DATA_FLAG|Tension_DATA_FLAG

struct Augustin_Params {
    GlobalData_t A_1;
    GlobalData_t A_2;
    GlobalData_t Ca_50ref;
    GlobalData_t Cai_init;
    GlobalData_t Cao;
    GlobalData_t Cli;
    GlobalData_t Clo;
    GlobalData_t Ki_init;
    GlobalData_t Ko;
    GlobalData_t Mgi;
    GlobalData_t Nai_init;
    GlobalData_t Nao;
    GlobalData_t TRPN_50;
    GlobalData_t T_ref;
    GlobalData_t a;
    GlobalData_t alpha_1;
    GlobalData_t alpha_2;
    GlobalData_t beta_0;
    GlobalData_t beta_1;
    GlobalData_t cell_type;
    GlobalData_t k_TRPN;
    GlobalData_t k_xb;
    GlobalData_t lengthDep;
    GlobalData_t markov_iks;
    GlobalData_t n_TRPN;
    GlobalData_t n_xb;
    char* flags;

};
static const char* Augustin_flags = "EPI|MCELL|ENDO";


struct Augustin_state {
    GlobalData_t C1;
    GlobalData_t C10;
    GlobalData_t C11;
    GlobalData_t C12;
    GlobalData_t C13;
    GlobalData_t C14;
    GlobalData_t C15;
    GlobalData_t C2;
    GlobalData_t C3;
    GlobalData_t C4;
    GlobalData_t C5;
    GlobalData_t C6;
    GlobalData_t C7;
    GlobalData_t C8;
    GlobalData_t C9;
    GlobalData_t CaM;
    GlobalData_t Ca_sr;
    GlobalData_t Cai;
    GlobalData_t Caj;
    GlobalData_t Casl;
    GlobalData_t Csqnb;
    GlobalData_t Ki;
    GlobalData_t Myoc;
    GlobalData_t Myom;
    GlobalData_t NaBj;
    GlobalData_t NaBsl;
    GlobalData_t Nai;
    GlobalData_t Naj;
    GlobalData_t Nasl;
    GlobalData_t O1;
    GlobalData_t Q_1;
    GlobalData_t Q_2;
    GlobalData_t RyRi;
    GlobalData_t RyRo;
    GlobalData_t RyRr;
    GlobalData_t SLHj;
    GlobalData_t SLHsl;
    GlobalData_t SLLj;
    GlobalData_t SLLsl;
    GlobalData_t SRB;
    GlobalData_t TRPN;
    GlobalData_t TnCHc;
    GlobalData_t TnCHm;
    GlobalData_t TnCL;
    Gatetype d;
    Gatetype f;
    GlobalData_t fcaBj;
    GlobalData_t fcaBsl;
    Gatetype h;
    Gatetype j;
    Gatetype m;
    GlobalData_t xb;
    Gatetype xkr;
    Gatetype xks;
    Gatetype xtof;
    Gatetype xtos;
    Gatetype ytof;
    Gatetype ytos;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Augustin(ION_IF *);
void construct_tables_Augustin(ION_IF *);
void destroy_Augustin(ION_IF *);
void initialize_sv_Augustin(ION_IF *, GlobalData_t**);
void compute_Augustin(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
