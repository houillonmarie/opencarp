// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Jean-Pierre Drouhard, Fernand A. Roberge
*  Year: 1987
*  Title: Revised formulation of the Hodgkin-Huxley representation of the sodium current in cardiac cells
*  Journal: Computers and Biomedical Research, 20(4), 333-350
*  DOI: 10.1016/0010-4809(87)90048-6
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "DrouhardRoberge.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_DrouhardRoberge(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_DrouhardRoberge( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define Cai_init (GlobalData_t)(3.e-1)
#define ENa (GlobalData_t)(40.0)
#define V_init (GlobalData_t)(-86.926861)
#define X_init (GlobalData_t)(0.09)
#define d_init (GlobalData_t)(0.00481312)
#define f_init (GlobalData_t)(0.898895)
#define h_init (GlobalData_t)(0.772909)
#define m_init (GlobalData_t)(0.00255455)



void initialize_params_DrouhardRoberge( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  DrouhardRoberge_Params *p = (DrouhardRoberge_Params *)IF->params;

  // Compute the regional constants
  {
    p->APDshorten = 1.;
    p->GNa = 15.;
    p->Gsi = 0.09;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_DrouhardRoberge;

}


// Define the parameters for the lookup tables
enum Tables {
  Cai_TAB,
  V_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum Cai_TableIndex {
  Esi_idx,
  NROWS_Cai
};

enum V_TableIndex {
  IK_idx,
  X_rush_larsen_A_idx,
  X_rush_larsen_B_idx,
  d_rush_larsen_A_idx,
  d_rush_larsen_B_idx,
  f_rush_larsen_A_idx,
  f_rush_larsen_B_idx,
  h_rush_larsen_A_idx,
  h_rush_larsen_B_idx,
  m_rush_larsen_A_idx,
  m_rush_larsen_B_idx,
  xti_idx,
  NROWS_V
};



void construct_tables_DrouhardRoberge( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  DrouhardRoberge_Params *p = (DrouhardRoberge_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  
  // Create the Cai lookup table
  LUT* Cai_tab = &IF->tables[Cai_TAB];
  LUT_alloc(Cai_tab, NROWS_Cai, 0.001, 30, 0.001, "DrouhardRoberge Cai");
  for (int __i=Cai_tab->mn_ind; __i<=Cai_tab->mx_ind; __i++) {
    double Cai = Cai_tab->res*__i;
    LUT_data_t* Cai_row = Cai_tab->tab[__i];
    Cai_row[Esi_idx] = (-82.3-((13.0287*(log((Cai/1.e6))))));
  }
  check_LUT(Cai_tab);
  
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -800, 800, 0.05, "DrouhardRoberge V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    V_row[IK_idx] = ((V!=-23.) ? (0.35*(((4.*((exp((0.04*(V+85.))))-(1.)))/((exp((0.08*(V+53.))))+(exp((0.04*(V+53.))))))-(((0.2*(V+23.))/(expm1((-0.04*(V+23.)))))))) : (0.35*(((4.*((exp((0.04*(V+85.))))-(1.)))/((exp((0.08*(V+53.))))+(exp((0.04*(V+53.))))))+(0.2/0.04))));
    double a_X = ((V<400.) ? ((0.0005*(exp((0.083*(V+50.)))))/((exp((0.057*(V+50.))))+1.)) : ((151.7994692*(exp((.06546786198*(V-(400.))))))/(1.+(1.517994692*(exp((.06546786198*(V-(400.)))))))));
    double a_d = ((p->APDshorten*(0.095*(exp((-0.01*(V-(5.)))))))/((exp((-0.072*(V-(5.)))))+1.));
    double a_f = ((p->APDshorten*(0.012*(exp((-0.008*(V+28.))))))/((exp((0.15*(V+28.))))+1.));
    double a_h = ((V>-90.) ? (0.1*(exp((-.193*(V+79.65))))) : (.737097507-((.1422598189*(V+90.)))));
    double a_m = ((V<100.) ? ((0.9*(V+42.65))/(1.-((exp((-0.22*(V+42.65))))))) : ((890.94379*(exp((.0486479163*(V-(100.))))))/(1.+(5.93962526*(exp((.0486479163*(V-(100.)))))))));
    double b_X = ((0.0013*(exp((-0.06*(V+20.)))))/((exp((-0.04*(V+20.))))+1.));
    double b_d = ((p->APDshorten*(0.07*(exp((-0.017*(V+44.))))))/((exp((0.05*(V+44.))))+1.));
    double b_f = ((p->APDshorten*(0.0065*(exp((-0.02*(V+30.))))))/((exp((-0.2*(V+30.))))+1.));
    double b_h = (1.7/(1.+(exp((-.095*(V+20.5))))));
    double b_m = ((V>-85.) ? (1.437*(exp((-.085*(V+39.75))))) : (100./(1.+(.48640816*(exp((.2597503577*(V+85.))))))));
    V_row[xti_idx] = ((0.8*((exp((0.04*(V+77.))))-(1.)))/(exp((0.04*(V+35.)))));
    V_row[X_rush_larsen_A_idx] = (((-a_X)/(a_X+b_X))*(expm1(((-dt)*(a_X+b_X)))));
    V_row[X_rush_larsen_B_idx] = (exp(((-dt)*(a_X+b_X))));
    V_row[d_rush_larsen_A_idx] = (((-a_d)/(a_d+b_d))*(expm1(((-dt)*(a_d+b_d)))));
    V_row[d_rush_larsen_B_idx] = (exp(((-dt)*(a_d+b_d))));
    V_row[f_rush_larsen_A_idx] = (((-a_f)/(a_f+b_f))*(expm1(((-dt)*(a_f+b_f)))));
    V_row[f_rush_larsen_B_idx] = (exp(((-dt)*(a_f+b_f))));
    V_row[h_rush_larsen_A_idx] = (((-a_h)/(a_h+b_h))*(expm1(((-dt)*(a_h+b_h)))));
    V_row[h_rush_larsen_B_idx] = (exp(((-dt)*(a_h+b_h))));
    V_row[m_rush_larsen_A_idx] = (((-a_m)/(a_m+b_m))*(expm1(((-dt)*(a_m+b_m)))));
    V_row[m_rush_larsen_B_idx] = (exp(((-dt)*(a_m+b_m))));
  }
  check_LUT(V_tab);
  

}



void    initialize_sv_DrouhardRoberge( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  DrouhardRoberge_Params *p = (DrouhardRoberge_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(DrouhardRoberge_state) );
  DrouhardRoberge_state *sv_base = (DrouhardRoberge_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    DrouhardRoberge_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Cai = Cai_init;
    V = V_init;
    sv->X = X_init;
    sv->d = d_init;
    sv->f = f_init;
    sv->h = h_init;
    sv->m = m_init;
    double Esi = (-82.3-((13.0287*(log((sv->Cai/1.e6))))));
    double IK = ((V!=-23.) ? (0.35*(((4.*((exp((0.04*(V+85.))))-(1.)))/((exp((0.08*(V+53.))))+(exp((0.04*(V+53.))))))-(((0.2*(V+23.))/(expm1((-0.04*(V+23.)))))))) : (0.35*(((4.*((exp((0.04*(V+85.))))-(1.)))/((exp((0.08*(V+53.))))+(exp((0.04*(V+53.))))))+(0.2/0.04))));
    double INa = (((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*(V-(ENa)));
    double xti = ((0.8*((exp((0.04*(V+77.))))-(1.)))/(exp((0.04*(V+35.)))));
    double IX = (sv->X*xti);
    double Isi = (((p->Gsi*sv->d)*sv->f)*(V-(Esi)));
    Iion = (((INa+Isi)+IX)+IK);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_DrouhardRoberge(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  DrouhardRoberge_Params *p  = (DrouhardRoberge_Params *)IF->params;
  DrouhardRoberge_state *sv_base = (DrouhardRoberge_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    DrouhardRoberge_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Cai_row[NROWS_Cai];
    LUT_interpRow(&IF->tables[Cai_TAB], sv->Cai, __i, Cai_row);
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t INa = (((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*(V-(ENa)));
    GlobalData_t IX = (sv->X*V_row[xti_idx]);
    GlobalData_t Isi = (((p->Gsi*sv->d)*sv->f)*(V-(Cai_row[Esi_idx])));
    Iion = (((INa+Isi)+IX)+V_row[IK_idx]);
    
    
    //Complete Forward Euler Update
    GlobalData_t diff_Cai = ((V<200.) ? ((-1.e-1*Isi)+((0.07*1.e6)*(1.e-7-((sv->Cai/1.e6))))) : 0.);
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t X_rush_larsen_A = V_row[X_rush_larsen_A_idx];
    GlobalData_t X_rush_larsen_B = V_row[X_rush_larsen_B_idx];
    GlobalData_t d_rush_larsen_A = V_row[d_rush_larsen_A_idx];
    GlobalData_t d_rush_larsen_B = V_row[d_rush_larsen_B_idx];
    GlobalData_t f_rush_larsen_A = V_row[f_rush_larsen_A_idx];
    GlobalData_t f_rush_larsen_B = V_row[f_rush_larsen_B_idx];
    GlobalData_t h_rush_larsen_A = V_row[h_rush_larsen_A_idx];
    GlobalData_t h_rush_larsen_B = V_row[h_rush_larsen_B_idx];
    GlobalData_t m_rush_larsen_A = V_row[m_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_B = V_row[m_rush_larsen_B_idx];
    GlobalData_t X_new = X_rush_larsen_A+X_rush_larsen_B*sv->X;
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f_new = f_rush_larsen_A+f_rush_larsen_B*sv->f;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Cai = Cai_new;
    Iion = Iion;
    sv->X = X_new;
    sv->d = d_new;
    sv->f = f_new;
    sv->h = h_new;
    sv->m = m_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_DrouhardRoberge(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("DrouhardRoberge_trace_header.txt","wt");
    fprintf(theader->fd,
        "IK\n"
        "INa\n"
        "IX\n"
        "Isi\n"
        "V\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  DrouhardRoberge_Params *p  = (DrouhardRoberge_Params *)IF->params;

  DrouhardRoberge_state *sv_base = (DrouhardRoberge_state *)IF->sv_tab.y;
  DrouhardRoberge_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t Esi = (-82.3-((13.0287*(log((sv->Cai/1.e6))))));
  GlobalData_t IK = ((V!=-23.) ? (0.35*(((4.*((exp((0.04*(V+85.))))-(1.)))/((exp((0.08*(V+53.))))+(exp((0.04*(V+53.))))))-(((0.2*(V+23.))/(expm1((-0.04*(V+23.)))))))) : (0.35*(((4.*((exp((0.04*(V+85.))))-(1.)))/((exp((0.08*(V+53.))))+(exp((0.04*(V+53.))))))+(0.2/0.04))));
  GlobalData_t INa = (((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*(V-(ENa)));
  GlobalData_t xti = ((0.8*((exp((0.04*(V+77.))))-(1.)))/(exp((0.04*(V+35.)))));
  GlobalData_t IX = (sv->X*xti);
  GlobalData_t Isi = (((p->Gsi*sv->d)*sv->f)*(V-(Esi)));
  //Output the desired variables
  fprintf(file, "%4.12f\t", IK);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", IX);
  fprintf(file, "%4.12f\t", Isi);
  fprintf(file, "%4.12f\t", V);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        