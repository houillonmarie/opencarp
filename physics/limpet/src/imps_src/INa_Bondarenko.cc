// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Bondarenko VE, Szigeti GP, Bett GC, Kim SJ, Rasmusson RL
*  Year: 2004
*  Title: Computer model of action potential of mouse ventricular myocytes
*  Journal: Am J Physiol Heart Circ Physiol. 287:H1378-403
*  DOI: 10.1152/ajpheart.00185.2003
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "INa_Bondarenko.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_INa_Bondarenko(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_INa_Bondarenko( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define C_Na1_init (GlobalData_t)(0.279132e-03)
#define C_Na2_init (GlobalData_t)(0.020752)
#define F (GlobalData_t)(96.5)
#define I1_Na_init (GlobalData_t)(0.673345e-06)
#define I2_Na_init (GlobalData_t)(0.155787e-08)
#define IC_Na2_init (GlobalData_t)(0.0113879)
#define IC_Na3_init (GlobalData_t)(0.342780)
#define IF_Na_init (GlobalData_t)(0.153176e-03)
#define O_Na_init (GlobalData_t)(0.713483e-06)
#define R (GlobalData_t)(8.314)
#define T (GlobalData_t)(298.)
#define partial_C_Na3_del_C_Na2 (GlobalData_t)(-1.)
#define partial_C_Na3_del_IC_Na3 (GlobalData_t)(-1.)
#define partial_alpha_Na11_del_C_Na2 (GlobalData_t)(0.)
#define partial_alpha_Na11_del_IC_Na2 (GlobalData_t)(0.)
#define partial_alpha_Na11_del_IC_Na3 (GlobalData_t)(0.)
#define partial_alpha_Na12_del_C_Na1 (GlobalData_t)(0.)
#define partial_alpha_Na12_del_C_Na2 (GlobalData_t)(0.)
#define partial_alpha_Na12_del_IC_Na2 (GlobalData_t)(0.)
#define partial_alpha_Na12_del_IF_Na (GlobalData_t)(0.)
#define partial_alpha_Na13_del_C_Na1 (GlobalData_t)(0.)
#define partial_alpha_Na13_del_IF_Na (GlobalData_t)(0.)
#define partial_alpha_Na13_del_O_Na (GlobalData_t)(0.)
#define partial_alpha_Na2_del_I1_Na (GlobalData_t)(0.)
#define partial_alpha_Na2_del_I2_Na (GlobalData_t)(0.)
#define partial_alpha_Na2_del_IF_Na (GlobalData_t)(0.)
#define partial_alpha_Na2_del_O_Na (GlobalData_t)(0.)
#define partial_alpha_Na3_del_C_Na1 (GlobalData_t)(0.)
#define partial_alpha_Na3_del_C_Na2 (GlobalData_t)(0.)
#define partial_alpha_Na3_del_I1_Na (GlobalData_t)(0.)
#define partial_alpha_Na3_del_I2_Na (GlobalData_t)(0.)
#define partial_alpha_Na3_del_IC_Na2 (GlobalData_t)(0.)
#define partial_alpha_Na3_del_IC_Na3 (GlobalData_t)(0.)
#define partial_alpha_Na3_del_IF_Na (GlobalData_t)(0.)
#define partial_alpha_Na3_del_O_Na (GlobalData_t)(0.)
#define partial_alpha_Na4_del_I1_Na (GlobalData_t)(0.)
#define partial_alpha_Na4_del_IF_Na (GlobalData_t)(0.)
#define partial_alpha_Na5_del_I1_Na (GlobalData_t)(0.)
#define partial_alpha_Na5_del_I2_Na (GlobalData_t)(0.)
#define partial_beta_Na11_del_C_Na2 (GlobalData_t)(0.)
#define partial_beta_Na11_del_IC_Na2 (GlobalData_t)(0.)
#define partial_beta_Na11_del_IC_Na3 (GlobalData_t)(0.)
#define partial_beta_Na12_del_C_Na1 (GlobalData_t)(0.)
#define partial_beta_Na12_del_C_Na2 (GlobalData_t)(0.)
#define partial_beta_Na12_del_IC_Na2 (GlobalData_t)(0.)
#define partial_beta_Na12_del_IF_Na (GlobalData_t)(0.)
#define partial_beta_Na13_del_C_Na1 (GlobalData_t)(0.)
#define partial_beta_Na13_del_IF_Na (GlobalData_t)(0.)
#define partial_beta_Na13_del_O_Na (GlobalData_t)(0.)
#define partial_beta_Na2_del_IF_Na (GlobalData_t)(0.)
#define partial_beta_Na2_del_O_Na (GlobalData_t)(0.)
#define partial_beta_Na3_del_C_Na1 (GlobalData_t)(0.)
#define partial_beta_Na3_del_C_Na2 (GlobalData_t)(0.)
#define partial_beta_Na3_del_IC_Na2 (GlobalData_t)(0.)
#define partial_beta_Na3_del_IC_Na3 (GlobalData_t)(0.)
#define partial_beta_Na3_del_IF_Na (GlobalData_t)(0.)
#define partial_beta_Na3_del_O_Na (GlobalData_t)(0.)
#define partial_beta_Na4_del_I1_Na (GlobalData_t)(0.)
#define partial_beta_Na4_del_IF_Na (GlobalData_t)(0.)
#define partial_beta_Na5_del_I1_Na (GlobalData_t)(0.)
#define partial_beta_Na5_del_I2_Na (GlobalData_t)(0.)



void initialize_params_INa_Bondarenko( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  INa_Bondarenko_Params *p = (INa_Bondarenko_Params *)IF->params;

  // Compute the regional constants
  {
    p->GNa = 13.0;
    p->Ki = 143.720;
    p->Ko = 5.40;
    p->Nai = 14.2371;
    p->Nao = 140.0;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {
  V_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum V_TableIndex {
  alpha_Na11_idx,
  alpha_Na12_idx,
  alpha_Na13_idx,
  alpha_Na2_idx,
  alpha_Na3_idx,
  alpha_Na4_idx,
  alpha_Na5_idx,
  beta_Na11_idx,
  beta_Na12_idx,
  beta_Na13_idx,
  beta_Na2_idx,
  beta_Na3_idx,
  beta_Na4_idx,
  beta_Na5_idx,
  partial_diff_C_Na1_del_C_Na1_idx,
  partial_diff_C_Na2_del_C_Na2_idx,
  partial_diff_I1_Na_del_I1_Na_idx,
  partial_diff_I2_Na_del_I2_Na_idx,
  partial_diff_IC_Na2_del_IC_Na2_idx,
  partial_diff_IC_Na3_del_IC_Na3_idx,
  partial_diff_IF_Na_del_IF_Na_idx,
  partial_diff_O_Na_del_O_Na_idx,
  NROWS_V
};



void construct_tables_INa_Bondarenko( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  INa_Bondarenko_Params *p = (INa_Bondarenko_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double E_Na = (((R*T)/F)*(log((((0.9*p->Nao)+(0.1*p->Ko))/((0.9*p->Nai)+(0.1*p->Ki))))));
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -800, 800, 0.05, "INa_Bondarenko V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    V_row[alpha_Na11_idx] = (3.802/((0.1027*(exp(((-(V+2.5))/17.))))+(0.20*(exp(((-(V+2.5))/150.))))));
    V_row[alpha_Na12_idx] = (3.802/((0.1027*(exp(((-(V+2.5))/15.))))+(0.23*(exp(((-(V+2.5))/150.))))));
    V_row[alpha_Na13_idx] = (3.802/((0.1027*(exp(((-(V+2.5))/12.))))+(0.25*(exp(((-(V+2.5))/150.))))));
    V_row[alpha_Na2_idx] = (1.0/((0.188495*(exp(((-(V+7.0))/16.6))))+0.393956));
    V_row[alpha_Na3_idx] = (7.0e-07*(exp(((-(V+7.0))/7.7))));
    V_row[beta_Na11_idx] = (0.1917*(exp(((-(V+2.5))/20.3))));
    V_row[beta_Na12_idx] = (0.2000*(exp(((-(V-(2.5)))/20.3))));
    V_row[beta_Na13_idx] = (0.2200*(exp(((-(V-(7.5)))/20.3))));
    V_row[beta_Na3_idx] = (0.0084+(0.00002*(V+7.0)));
    V_row[alpha_Na4_idx] = (V_row[alpha_Na2_idx]/1000.);
    V_row[alpha_Na5_idx] = (V_row[alpha_Na2_idx]/95000.);
    V_row[beta_Na2_idx] = (((V_row[alpha_Na13_idx]*V_row[alpha_Na2_idx])*V_row[alpha_Na3_idx])/(V_row[beta_Na13_idx]*V_row[beta_Na3_idx]));
    V_row[beta_Na4_idx] = V_row[alpha_Na3_idx];
    V_row[beta_Na5_idx] = (V_row[alpha_Na3_idx]/50.);
    V_row[partial_diff_C_Na1_del_C_Na1_idx] = (((-V_row[beta_Na12_idx])-(V_row[alpha_Na13_idx]))-(V_row[beta_Na3_idx]));
    V_row[partial_diff_C_Na2_del_C_Na2_idx] = ((((V_row[alpha_Na11_idx]*-1.)-(V_row[beta_Na11_idx]))-(V_row[alpha_Na12_idx]))-(V_row[beta_Na3_idx]));
    V_row[partial_diff_IC_Na2_del_IC_Na2_idx] = (((-V_row[beta_Na11_idx])-(V_row[alpha_Na12_idx]))-(V_row[alpha_Na3_idx]));
    V_row[partial_diff_IC_Na3_del_IC_Na3_idx] = (((-V_row[alpha_Na11_idx])+(V_row[beta_Na3_idx]*-1.))-(V_row[alpha_Na3_idx]));
    V_row[partial_diff_O_Na_del_O_Na_idx] = ((-V_row[beta_Na13_idx])-(V_row[alpha_Na2_idx]));
    V_row[partial_diff_I1_Na_del_I1_Na_idx] = ((-V_row[beta_Na4_idx])-(V_row[alpha_Na5_idx]));
    V_row[partial_diff_I2_Na_del_I2_Na_idx] = (-V_row[beta_Na5_idx]);
    V_row[partial_diff_IF_Na_del_IF_Na_idx] = ((((-V_row[beta_Na2_idx])-(V_row[alpha_Na3_idx]))-(V_row[alpha_Na4_idx]))-(V_row[beta_Na12_idx]));
  }
  check_LUT(V_tab);
  

}



void    initialize_sv_INa_Bondarenko( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  INa_Bondarenko_Params *p = (INa_Bondarenko_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(INa_Bondarenko_state) );
  INa_Bondarenko_state *sv_base = (INa_Bondarenko_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double E_Na = (((R*T)/F)*(log((((0.9*p->Nao)+(0.1*p->Ko))/((0.9*p->Nai)+(0.1*p->Ki))))));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    INa_Bondarenko_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->C_Na1 = C_Na1_init;
    sv->C_Na2 = C_Na2_init;
    sv->I1_Na = I1_Na_init;
    sv->I2_Na = I2_Na_init;
    sv->IC_Na2 = IC_Na2_init;
    sv->IC_Na3 = IC_Na3_init;
    sv->IF_Na = IF_Na_init;
    sv->O_Na = O_Na_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_INa_Bondarenko(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  INa_Bondarenko_Params *p  = (INa_Bondarenko_Params *)IF->params;
  INa_Bondarenko_state *sv_base = (INa_Bondarenko_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t E_Na = (((R*T)/F)*(log((((0.9*p->Nao)+(0.1*p->Ko))/((0.9*p->Nai)+(0.1*p->Ki))))));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    INa_Bondarenko_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t INa = ((p->GNa*sv->O_Na)*(V-(E_Na)));
    Iion = (Iion+INa);
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    GlobalData_t C_Na3 = (1.-((((((((sv->O_Na+sv->C_Na1)+sv->C_Na2)+sv->IF_Na)+sv->I1_Na)+sv->I2_Na)+sv->IC_Na2)+sv->IC_Na3)));
    GlobalData_t diff_C_Na1 = ((((((V_row[alpha_Na12_idx]*sv->C_Na2)-((V_row[beta_Na12_idx]*sv->C_Na1)))+(V_row[beta_Na13_idx]*sv->O_Na))-((V_row[alpha_Na13_idx]*sv->C_Na1)))+(V_row[alpha_Na3_idx]*sv->IF_Na))-((V_row[beta_Na3_idx]*sv->C_Na1)));
    GlobalData_t diff_C_Na2 = ((((((V_row[alpha_Na11_idx]*C_Na3)-((V_row[beta_Na11_idx]*sv->C_Na2)))+(V_row[beta_Na12_idx]*sv->C_Na1))-((V_row[alpha_Na12_idx]*sv->C_Na2)))+(V_row[alpha_Na3_idx]*sv->IC_Na2))-((V_row[beta_Na3_idx]*sv->C_Na2)));
    GlobalData_t diff_IC_Na2 = ((((((V_row[alpha_Na11_idx]*sv->IC_Na3)-((V_row[beta_Na11_idx]*sv->IC_Na2)))+(V_row[beta_Na12_idx]*sv->IF_Na))-((V_row[alpha_Na12_idx]*sv->IC_Na2)))+(V_row[beta_Na3_idx]*sv->C_Na2))-((V_row[alpha_Na3_idx]*sv->IC_Na2)));
    GlobalData_t diff_IC_Na3 = ((((V_row[beta_Na11_idx]*sv->IC_Na2)-((V_row[alpha_Na11_idx]*sv->IC_Na3)))+(V_row[beta_Na3_idx]*C_Na3))-((V_row[alpha_Na3_idx]*sv->IC_Na3)));
    GlobalData_t C_Na1_sundnes_half = (sv->C_Na1+(diff_C_Na1*(((fabs(V_row[partial_diff_C_Na1_del_C_Na1_idx]))<1e-8) ? (dt/2.) : ((expm1(((V_row[partial_diff_C_Na1_del_C_Na1_idx]*dt)/2.)))/V_row[partial_diff_C_Na1_del_C_Na1_idx]))));
    GlobalData_t C_Na2_sundnes_half = (sv->C_Na2+(diff_C_Na2*(((fabs(V_row[partial_diff_C_Na2_del_C_Na2_idx]))<1e-8) ? (dt/2.) : ((expm1(((V_row[partial_diff_C_Na2_del_C_Na2_idx]*dt)/2.)))/V_row[partial_diff_C_Na2_del_C_Na2_idx]))));
    GlobalData_t IC_Na2_sundnes_half = (sv->IC_Na2+(diff_IC_Na2*(((fabs(V_row[partial_diff_IC_Na2_del_IC_Na2_idx]))<1e-8) ? (dt/2.) : ((expm1(((V_row[partial_diff_IC_Na2_del_IC_Na2_idx]*dt)/2.)))/V_row[partial_diff_IC_Na2_del_IC_Na2_idx]))));
    GlobalData_t IC_Na3_sundnes_half = (sv->IC_Na3+(diff_IC_Na3*(((fabs(V_row[partial_diff_IC_Na3_del_IC_Na3_idx]))<1e-8) ? (dt/2.) : ((expm1(((V_row[partial_diff_IC_Na3_del_IC_Na3_idx]*dt)/2.)))/V_row[partial_diff_IC_Na3_del_IC_Na3_idx]))));
    GlobalData_t diff_I1_Na = ((((V_row[alpha_Na4_idx]*sv->IF_Na)-((V_row[beta_Na4_idx]*sv->I1_Na)))+(V_row[beta_Na5_idx]*sv->I2_Na))-((V_row[alpha_Na5_idx]*sv->I1_Na)));
    GlobalData_t diff_I2_Na = ((V_row[alpha_Na5_idx]*sv->I1_Na)-((V_row[beta_Na5_idx]*sv->I2_Na)));
    GlobalData_t diff_IF_Na = ((((((((V_row[alpha_Na2_idx]*sv->O_Na)-((V_row[beta_Na2_idx]*sv->IF_Na)))+(V_row[beta_Na3_idx]*sv->C_Na1))-((V_row[alpha_Na3_idx]*sv->IF_Na)))+(V_row[beta_Na4_idx]*sv->I1_Na))-((V_row[alpha_Na4_idx]*sv->IF_Na)))+(V_row[alpha_Na12_idx]*sv->IC_Na2))-((V_row[beta_Na12_idx]*sv->IF_Na)));
    GlobalData_t diff_O_Na = ((((V_row[alpha_Na13_idx]*sv->C_Na1)-((V_row[beta_Na13_idx]*sv->O_Na)))+(V_row[beta_Na2_idx]*sv->IF_Na))-((V_row[alpha_Na2_idx]*sv->O_Na)));
    GlobalData_t I1_Na_sundnes_half = (sv->I1_Na+(diff_I1_Na*(((fabs(V_row[partial_diff_I1_Na_del_I1_Na_idx]))<1e-8) ? (dt/2.) : ((expm1(((V_row[partial_diff_I1_Na_del_I1_Na_idx]*dt)/2.)))/V_row[partial_diff_I1_Na_del_I1_Na_idx]))));
    GlobalData_t I2_Na_sundnes_half = (sv->I2_Na+(diff_I2_Na*(((fabs(V_row[partial_diff_I2_Na_del_I2_Na_idx]))<1e-8) ? (dt/2.) : ((expm1(((V_row[partial_diff_I2_Na_del_I2_Na_idx]*dt)/2.)))/V_row[partial_diff_I2_Na_del_I2_Na_idx]))));
    GlobalData_t IF_Na_sundnes_half = (sv->IF_Na+(diff_IF_Na*(((fabs(V_row[partial_diff_IF_Na_del_IF_Na_idx]))<1e-8) ? (dt/2.) : ((expm1(((V_row[partial_diff_IF_Na_del_IF_Na_idx]*dt)/2.)))/V_row[partial_diff_IF_Na_del_IF_Na_idx]))));
    GlobalData_t O_Na_sundnes_half = (sv->O_Na+(diff_O_Na*(((fabs(V_row[partial_diff_O_Na_del_O_Na_idx]))<1e-8) ? (dt/2.) : ((expm1(((V_row[partial_diff_O_Na_del_O_Na_idx]*dt)/2.)))/V_row[partial_diff_O_Na_del_O_Na_idx]))));
    GlobalData_t C_Na1_new;
    GlobalData_t C_Na2_new;
    GlobalData_t I1_Na_new;
    GlobalData_t I2_Na_new;
    GlobalData_t IC_Na2_new;
    GlobalData_t IC_Na3_new;
    GlobalData_t IF_Na_new;
    GlobalData_t O_Na_new;
    {
      GlobalData_t sv_intermediate_C_Na1 = sv->C_Na1;
      GlobalData_t sv_intermediate_C_Na2 = C_Na2_sundnes_half;
      GlobalData_t sv_intermediate_I1_Na = I1_Na_sundnes_half;
      GlobalData_t sv_intermediate_I2_Na = I2_Na_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na2 = IC_Na2_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na3 = IC_Na3_sundnes_half;
      GlobalData_t sv_intermediate_IF_Na = IF_Na_sundnes_half;
      GlobalData_t sv_intermediate_O_Na = O_Na_sundnes_half;
      GlobalData_t diff_C_Na1 = ((((((V_row[alpha_Na12_idx]*sv_intermediate_C_Na2)-((V_row[beta_Na12_idx]*sv_intermediate_C_Na1)))+(V_row[beta_Na13_idx]*sv_intermediate_O_Na))-((V_row[alpha_Na13_idx]*sv_intermediate_C_Na1)))+(V_row[alpha_Na3_idx]*sv_intermediate_IF_Na))-((V_row[beta_Na3_idx]*sv_intermediate_C_Na1)));
      GlobalData_t C_Na1_sundnes_full = (sv_intermediate_C_Na1+(diff_C_Na1*(((fabs(V_row[partial_diff_C_Na1_del_C_Na1_idx]))<1e-8) ? dt : ((expm1((V_row[partial_diff_C_Na1_del_C_Na1_idx]*dt)))/V_row[partial_diff_C_Na1_del_C_Na1_idx]))));
      C_Na1_new = C_Na1_sundnes_full;
    }
    {
      GlobalData_t sv_intermediate_C_Na1 = C_Na1_sundnes_half;
      GlobalData_t sv_intermediate_C_Na2 = sv->C_Na2;
      GlobalData_t sv_intermediate_I1_Na = I1_Na_sundnes_half;
      GlobalData_t sv_intermediate_I2_Na = I2_Na_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na2 = IC_Na2_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na3 = IC_Na3_sundnes_half;
      GlobalData_t sv_intermediate_IF_Na = IF_Na_sundnes_half;
      GlobalData_t sv_intermediate_O_Na = O_Na_sundnes_half;
      GlobalData_t C_Na3 = (1.-((((((((sv_intermediate_O_Na+sv_intermediate_C_Na1)+sv_intermediate_C_Na2)+sv_intermediate_IF_Na)+sv_intermediate_I1_Na)+sv_intermediate_I2_Na)+sv_intermediate_IC_Na2)+sv_intermediate_IC_Na3)));
      GlobalData_t diff_C_Na2 = ((((((V_row[alpha_Na11_idx]*C_Na3)-((V_row[beta_Na11_idx]*sv_intermediate_C_Na2)))+(V_row[beta_Na12_idx]*sv_intermediate_C_Na1))-((V_row[alpha_Na12_idx]*sv_intermediate_C_Na2)))+(V_row[alpha_Na3_idx]*sv_intermediate_IC_Na2))-((V_row[beta_Na3_idx]*sv_intermediate_C_Na2)));
      GlobalData_t C_Na2_sundnes_full = (sv_intermediate_C_Na2+(diff_C_Na2*(((fabs(V_row[partial_diff_C_Na2_del_C_Na2_idx]))<1e-8) ? dt : ((expm1((V_row[partial_diff_C_Na2_del_C_Na2_idx]*dt)))/V_row[partial_diff_C_Na2_del_C_Na2_idx]))));
      C_Na2_new = C_Na2_sundnes_full;
    }
    {
      GlobalData_t sv_intermediate_C_Na1 = C_Na1_sundnes_half;
      GlobalData_t sv_intermediate_C_Na2 = C_Na2_sundnes_half;
      GlobalData_t sv_intermediate_I1_Na = sv->I1_Na;
      GlobalData_t sv_intermediate_I2_Na = I2_Na_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na2 = IC_Na2_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na3 = IC_Na3_sundnes_half;
      GlobalData_t sv_intermediate_IF_Na = IF_Na_sundnes_half;
      GlobalData_t sv_intermediate_O_Na = O_Na_sundnes_half;
      GlobalData_t diff_I1_Na = ((((V_row[alpha_Na4_idx]*sv_intermediate_IF_Na)-((V_row[beta_Na4_idx]*sv_intermediate_I1_Na)))+(V_row[beta_Na5_idx]*sv_intermediate_I2_Na))-((V_row[alpha_Na5_idx]*sv_intermediate_I1_Na)));
      GlobalData_t I1_Na_sundnes_full = (sv_intermediate_I1_Na+(diff_I1_Na*(((fabs(V_row[partial_diff_I1_Na_del_I1_Na_idx]))<1e-8) ? dt : ((expm1((V_row[partial_diff_I1_Na_del_I1_Na_idx]*dt)))/V_row[partial_diff_I1_Na_del_I1_Na_idx]))));
      I1_Na_new = I1_Na_sundnes_full;
    }
    {
      GlobalData_t sv_intermediate_C_Na1 = C_Na1_sundnes_half;
      GlobalData_t sv_intermediate_C_Na2 = C_Na2_sundnes_half;
      GlobalData_t sv_intermediate_I1_Na = I1_Na_sundnes_half;
      GlobalData_t sv_intermediate_I2_Na = sv->I2_Na;
      GlobalData_t sv_intermediate_IC_Na2 = IC_Na2_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na3 = IC_Na3_sundnes_half;
      GlobalData_t sv_intermediate_IF_Na = IF_Na_sundnes_half;
      GlobalData_t sv_intermediate_O_Na = O_Na_sundnes_half;
      GlobalData_t diff_I2_Na = ((V_row[alpha_Na5_idx]*sv_intermediate_I1_Na)-((V_row[beta_Na5_idx]*sv_intermediate_I2_Na)));
      GlobalData_t I2_Na_sundnes_full = (sv_intermediate_I2_Na+(diff_I2_Na*(((fabs(V_row[partial_diff_I2_Na_del_I2_Na_idx]))<1e-8) ? dt : ((expm1((V_row[partial_diff_I2_Na_del_I2_Na_idx]*dt)))/V_row[partial_diff_I2_Na_del_I2_Na_idx]))));
      I2_Na_new = I2_Na_sundnes_full;
    }
    {
      GlobalData_t sv_intermediate_C_Na1 = C_Na1_sundnes_half;
      GlobalData_t sv_intermediate_C_Na2 = C_Na2_sundnes_half;
      GlobalData_t sv_intermediate_I1_Na = I1_Na_sundnes_half;
      GlobalData_t sv_intermediate_I2_Na = I2_Na_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na2 = sv->IC_Na2;
      GlobalData_t sv_intermediate_IC_Na3 = IC_Na3_sundnes_half;
      GlobalData_t sv_intermediate_IF_Na = IF_Na_sundnes_half;
      GlobalData_t sv_intermediate_O_Na = O_Na_sundnes_half;
      GlobalData_t diff_IC_Na2 = ((((((V_row[alpha_Na11_idx]*sv_intermediate_IC_Na3)-((V_row[beta_Na11_idx]*sv_intermediate_IC_Na2)))+(V_row[beta_Na12_idx]*sv_intermediate_IF_Na))-((V_row[alpha_Na12_idx]*sv_intermediate_IC_Na2)))+(V_row[beta_Na3_idx]*sv_intermediate_C_Na2))-((V_row[alpha_Na3_idx]*sv_intermediate_IC_Na2)));
      GlobalData_t IC_Na2_sundnes_full = (sv_intermediate_IC_Na2+(diff_IC_Na2*(((fabs(V_row[partial_diff_IC_Na2_del_IC_Na2_idx]))<1e-8) ? dt : ((expm1((V_row[partial_diff_IC_Na2_del_IC_Na2_idx]*dt)))/V_row[partial_diff_IC_Na2_del_IC_Na2_idx]))));
      IC_Na2_new = IC_Na2_sundnes_full;
    }
    {
      GlobalData_t sv_intermediate_C_Na1 = C_Na1_sundnes_half;
      GlobalData_t sv_intermediate_C_Na2 = C_Na2_sundnes_half;
      GlobalData_t sv_intermediate_I1_Na = I1_Na_sundnes_half;
      GlobalData_t sv_intermediate_I2_Na = I2_Na_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na2 = IC_Na2_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na3 = sv->IC_Na3;
      GlobalData_t sv_intermediate_IF_Na = IF_Na_sundnes_half;
      GlobalData_t sv_intermediate_O_Na = O_Na_sundnes_half;
      GlobalData_t C_Na3 = (1.-((((((((sv_intermediate_O_Na+sv_intermediate_C_Na1)+sv_intermediate_C_Na2)+sv_intermediate_IF_Na)+sv_intermediate_I1_Na)+sv_intermediate_I2_Na)+sv_intermediate_IC_Na2)+sv_intermediate_IC_Na3)));
      GlobalData_t diff_IC_Na3 = ((((V_row[beta_Na11_idx]*sv_intermediate_IC_Na2)-((V_row[alpha_Na11_idx]*sv_intermediate_IC_Na3)))+(V_row[beta_Na3_idx]*C_Na3))-((V_row[alpha_Na3_idx]*sv_intermediate_IC_Na3)));
      GlobalData_t IC_Na3_sundnes_full = (sv_intermediate_IC_Na3+(diff_IC_Na3*(((fabs(V_row[partial_diff_IC_Na3_del_IC_Na3_idx]))<1e-8) ? dt : ((expm1((V_row[partial_diff_IC_Na3_del_IC_Na3_idx]*dt)))/V_row[partial_diff_IC_Na3_del_IC_Na3_idx]))));
      IC_Na3_new = IC_Na3_sundnes_full;
    }
    {
      GlobalData_t sv_intermediate_C_Na1 = C_Na1_sundnes_half;
      GlobalData_t sv_intermediate_C_Na2 = C_Na2_sundnes_half;
      GlobalData_t sv_intermediate_I1_Na = I1_Na_sundnes_half;
      GlobalData_t sv_intermediate_I2_Na = I2_Na_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na2 = IC_Na2_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na3 = IC_Na3_sundnes_half;
      GlobalData_t sv_intermediate_IF_Na = sv->IF_Na;
      GlobalData_t sv_intermediate_O_Na = O_Na_sundnes_half;
      GlobalData_t diff_IF_Na = ((((((((V_row[alpha_Na2_idx]*sv_intermediate_O_Na)-((V_row[beta_Na2_idx]*sv_intermediate_IF_Na)))+(V_row[beta_Na3_idx]*sv_intermediate_C_Na1))-((V_row[alpha_Na3_idx]*sv_intermediate_IF_Na)))+(V_row[beta_Na4_idx]*sv_intermediate_I1_Na))-((V_row[alpha_Na4_idx]*sv_intermediate_IF_Na)))+(V_row[alpha_Na12_idx]*sv_intermediate_IC_Na2))-((V_row[beta_Na12_idx]*sv_intermediate_IF_Na)));
      GlobalData_t IF_Na_sundnes_full = (sv_intermediate_IF_Na+(diff_IF_Na*(((fabs(V_row[partial_diff_IF_Na_del_IF_Na_idx]))<1e-8) ? dt : ((expm1((V_row[partial_diff_IF_Na_del_IF_Na_idx]*dt)))/V_row[partial_diff_IF_Na_del_IF_Na_idx]))));
      IF_Na_new = IF_Na_sundnes_full;
    }
    {
      GlobalData_t sv_intermediate_C_Na1 = C_Na1_sundnes_half;
      GlobalData_t sv_intermediate_C_Na2 = C_Na2_sundnes_half;
      GlobalData_t sv_intermediate_I1_Na = I1_Na_sundnes_half;
      GlobalData_t sv_intermediate_I2_Na = I2_Na_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na2 = IC_Na2_sundnes_half;
      GlobalData_t sv_intermediate_IC_Na3 = IC_Na3_sundnes_half;
      GlobalData_t sv_intermediate_IF_Na = IF_Na_sundnes_half;
      GlobalData_t sv_intermediate_O_Na = sv->O_Na;
      GlobalData_t diff_O_Na = ((((V_row[alpha_Na13_idx]*sv_intermediate_C_Na1)-((V_row[beta_Na13_idx]*sv_intermediate_O_Na)))+(V_row[beta_Na2_idx]*sv_intermediate_IF_Na))-((V_row[alpha_Na2_idx]*sv_intermediate_O_Na)));
      GlobalData_t O_Na_sundnes_full = (sv_intermediate_O_Na+(diff_O_Na*(((fabs(V_row[partial_diff_O_Na_del_O_Na_idx]))<1e-8) ? dt : ((expm1((V_row[partial_diff_O_Na_del_O_Na_idx]*dt)))/V_row[partial_diff_O_Na_del_O_Na_idx]))));
      O_Na_new = O_Na_sundnes_full;
    }
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->C_Na1 = C_Na1_new;
    sv->C_Na2 = C_Na2_new;
    sv->I1_Na = I1_Na_new;
    sv->I2_Na = I2_Na_new;
    sv->IC_Na2 = IC_Na2_new;
    sv->IC_Na3 = IC_Na3_new;
    sv->IF_Na = IF_Na_new;
    Iion = Iion;
    sv->O_Na = O_Na_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        