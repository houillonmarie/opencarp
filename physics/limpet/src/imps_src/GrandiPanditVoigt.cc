// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Eleonora Grandi, Sandeep V. Pandit, Niels Voigt, Antony J. Workman, Dobromir Dobrev, Jose Jalife, and Donald M. Bers
*  Year: 2011
*  Title: Human Atrial Action Potential and Ca2+ Model: Sinus Rhythm and Chronic Atrial Fibrillation
*  Journal: Circ. Res. 109:1055-1066
*  DOI: 10.1161/CIRCRESAHA.111.253955
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "GrandiPanditVoigt.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_GrandiPanditVoigt(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_GrandiPanditVoigt( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define Bmax_CaM (GlobalData_t)(24e-3)
#define Bmax_Naj (GlobalData_t)(7.561)
#define Bmax_Nasl (GlobalData_t)(1.65)
#define Bmax_SR (GlobalData_t)((19.*.9e-3))
#define Bmax_TnChigh (GlobalData_t)(140e-3)
#define Bmax_TnClow (GlobalData_t)(70e-3)
#define Bmax_myosin (GlobalData_t)(140e-3)
#define C10_init (GlobalData_t)(0.000422735)
#define C11_init (GlobalData_t)(0.0101662)
#define C12_init (GlobalData_t)(0.0958745)
#define C13_init (GlobalData_t)(0.000735654)
#define C14_init (GlobalData_t)(0.0301493)
#define C15_init (GlobalData_t)(0.0164185)
#define C1_init (GlobalData_t)(4.62486e-05)
#define C2_init (GlobalData_t)(0.000541684)
#define C3_init (GlobalData_t)(0.00532603)
#define C4_init (GlobalData_t)(0.0509979)
#define C5_init (GlobalData_t)(0.425787)
#define C6_init (GlobalData_t)(0.000164829)
#define C7_init (GlobalData_t)(0.0042408)
#define C8_init (GlobalData_t)(0.0512494)
#define C9_init (GlobalData_t)(0.296445)
#define CaM_init (GlobalData_t)(0.000744258)
#define Ca_sr_init (GlobalData_t)(0.508805)
#define Cai_init (GlobalData_t)(0.223757)
#define Caj_init (GlobalData_t)(0.000345341)
#define Cao (GlobalData_t)(1.8)
#define Casl_init (GlobalData_t)(0.000248244)
#define Cli (GlobalData_t)(15.)
#define Clo (GlobalData_t)(150.)
#define Cmem (GlobalData_t)(1.1e-10)
#define Csqnb_init (GlobalData_t)(1.1416)
#define DcaJuncSL (GlobalData_t)(1.64e-6)
#define DcaSLcyto (GlobalData_t)(1.22e-6)
#define DnaJuncSL (GlobalData_t)(1.09e-5)
#define DnaSLcyto (GlobalData_t)(1.79e-5)
#define Fjunc (GlobalData_t)(0.11)
#define Fjunc_CaL (GlobalData_t)(0.9)
#define Frdy (GlobalData_t)(96485.)
#define GCaB (GlobalData_t)((1.10*5.513e-4))
#define GClB (GlobalData_t)(9.0e-3)
#define GClCFTR (GlobalData_t)(0.)
#define GClCa (GlobalData_t)((0.5*0.109625))
#define GNaB (GlobalData_t)(0.597e-3)
#define Iapp (GlobalData_t)(0.)
#define IbarNaK (GlobalData_t)((0.7*1.8))
#define IbarSLCaP (GlobalData_t)((0.7*0.0673))
#define J_ca_juncsl (GlobalData_t)((1./1.2134e12))
#define J_ca_slmyo (GlobalData_t)((1./2.68510e11))
#define J_na_juncsl (GlobalData_t)((1./((1.6382e12/3.)*100.)))
#define J_na_slmyo (GlobalData_t)((1./((1.8308e10/3.)*100.)))
#define KdClCa (GlobalData_t)(100e-3)
#define Kdact (GlobalData_t)((1.5*0.256e-3))
#define Ki_init (GlobalData_t)(120.)
#define KmCai (GlobalData_t)(3.59e-3)
#define KmCao (GlobalData_t)(1.3)
#define KmKo (GlobalData_t)(1.5)
#define KmNai (GlobalData_t)(12.29)
#define KmNao (GlobalData_t)(87.5)
#define KmPCa (GlobalData_t)(0.5e-3)
#define Kmr (GlobalData_t)(1.7)
#define Ko (GlobalData_t)(5.4)
#define MaxSR (GlobalData_t)(15.)
#define Mgi (GlobalData_t)(1.)
#define MinSR (GlobalData_t)(1.)
#define Myoc_init (GlobalData_t)(0.0044043)
#define Myom_init (GlobalData_t)(0.135079)
#define NaBj_init (GlobalData_t)(3.72572)
#define NaBsl_init (GlobalData_t)(0.812997)
#define Nai_init (GlobalData_t)(9.70961)
#define Naj_init (GlobalData_t)(9.70869)
#define Nao (GlobalData_t)(140.)
#define Nasl_init (GlobalData_t)(9.7094)
#define O1_init (GlobalData_t)(0.00598219)
#define Q10CaL (GlobalData_t)(1.8)
#define Q10KmNai (GlobalData_t)(1.39)
#define Q10NCX (GlobalData_t)(1.57)
#define Q10NaK (GlobalData_t)(1.63)
#define Q10SLCaP (GlobalData_t)(2.35)
#define Q10SRCaP (GlobalData_t)(2.6)
#define R (GlobalData_t)(8314.)
#define RyRi_init (GlobalData_t)(6.22791e-07)
#define RyRo_init (GlobalData_t)(2.25304e-06)
#define RyRr_init (GlobalData_t)(0.783437)
#define SLHj_init (GlobalData_t)(0.106605)
#define SLHsl_init (GlobalData_t)(0.197796)
#define SLLj_init (GlobalData_t)(0.0143565)
#define SLLsl_init (GlobalData_t)(0.0227807)
#define SRB_init (GlobalData_t)(0.00465719)
#define Temp (GlobalData_t)(310.)
#define TnCHc_init (GlobalData_t)(0.129035)
#define TnCHm_init (GlobalData_t)(0.00513574)
#define TnCL_init (GlobalData_t)(0.0191988)
#define Vm_init (GlobalData_t)(-74.5579)
#define cellLength (GlobalData_t)(100.)
#define cellRadius (GlobalData_t)(10.25)
#define d_init (GlobalData_t)(1.79809e-05)
#define diff_Ki (GlobalData_t)(0.)
#define distJuncSL (GlobalData_t)(0.5)
#define distSLcyto (GlobalData_t)(0.45)
#define ec50SR (GlobalData_t)(0.45)
#define f_init (GlobalData_t)(0.998607)
#define fcaBj_init (GlobalData_t)(0.047971)
#define fcaBsl_init (GlobalData_t)(0.0354354)
#define gkp (GlobalData_t)((2.*0.001))
#define h_init (GlobalData_t)(0.859516)
#define hillSRCaP (GlobalData_t)(1.787)
#define hl_init (GlobalData_t)(0.0407106)
#define j_init (GlobalData_t)(0.878424)
#define junctionLength (GlobalData_t)(160e-3)
#define junctionRadius (GlobalData_t)(15e-3)
#define kiCa (GlobalData_t)(0.5)
#define kim (GlobalData_t)(0.005)
#define koff_cam (GlobalData_t)(238e-3)
#define koff_csqn (GlobalData_t)(65.)
#define koff_myoca (GlobalData_t)(0.46e-3)
#define koff_myomg (GlobalData_t)(0.057e-3)
#define koff_na (GlobalData_t)(1e-3)
#define koff_slh (GlobalData_t)(30e-3)
#define koff_sll (GlobalData_t)(1300e-3)
#define koff_sr (GlobalData_t)(60e-3)
#define koff_tnchca (GlobalData_t)(0.032e-3)
#define koff_tnchmg (GlobalData_t)(3.33e-3)
#define kom (GlobalData_t)(0.06)
#define kon_cam (GlobalData_t)(34.)
#define kon_csqn (GlobalData_t)(100.)
#define kon_myoca (GlobalData_t)(13.8)
#define kon_myomg (GlobalData_t)(0.0157)
#define kon_na (GlobalData_t)(0.1e-3)
#define kon_slh (GlobalData_t)(100.)
#define kon_sll (GlobalData_t)(100.)
#define kon_sr (GlobalData_t)(100.)
#define kon_tnchca (GlobalData_t)(2.37)
#define kon_tnchmg (GlobalData_t)(3e-3)
#define kon_tncl (GlobalData_t)(32.7)
#define ksat (GlobalData_t)(0.27)
#define m_init (GlobalData_t)(0.00852355)
#define ml_init (GlobalData_t)(0.00852355)
#define nu (GlobalData_t)(0.35)
#define pNaK (GlobalData_t)(0.01833)
#define partial_AF_del_C1 (GlobalData_t)(0.)
#define partial_AF_del_C10 (GlobalData_t)(0.)
#define partial_AF_del_C11 (GlobalData_t)(0.)
#define partial_AF_del_C12 (GlobalData_t)(0.)
#define partial_AF_del_C13 (GlobalData_t)(0.)
#define partial_AF_del_C14 (GlobalData_t)(0.)
#define partial_AF_del_C15 (GlobalData_t)(0.)
#define partial_AF_del_C2 (GlobalData_t)(0.)
#define partial_AF_del_C3 (GlobalData_t)(0.)
#define partial_AF_del_C4 (GlobalData_t)(0.)
#define partial_AF_del_C5 (GlobalData_t)(0.)
#define partial_AF_del_C6 (GlobalData_t)(0.)
#define partial_AF_del_C7 (GlobalData_t)(0.)
#define partial_AF_del_C8 (GlobalData_t)(0.)
#define partial_AF_del_C9 (GlobalData_t)(0.)
#define partial_AF_del_O1 (GlobalData_t)(0.)
#define partial_AF_del_RyRi (GlobalData_t)(0.)
#define partial_AF_del_RyRo (GlobalData_t)(0.)
#define partial_AF_del_RyRr (GlobalData_t)(0.)
#define partial_FoRT_del_C1 (GlobalData_t)(0.)
#define partial_FoRT_del_C10 (GlobalData_t)(0.)
#define partial_FoRT_del_C11 (GlobalData_t)(0.)
#define partial_FoRT_del_C12 (GlobalData_t)(0.)
#define partial_FoRT_del_C13 (GlobalData_t)(0.)
#define partial_FoRT_del_C14 (GlobalData_t)(0.)
#define partial_FoRT_del_C15 (GlobalData_t)(0.)
#define partial_FoRT_del_C2 (GlobalData_t)(0.)
#define partial_FoRT_del_C3 (GlobalData_t)(0.)
#define partial_FoRT_del_C4 (GlobalData_t)(0.)
#define partial_FoRT_del_C5 (GlobalData_t)(0.)
#define partial_FoRT_del_C6 (GlobalData_t)(0.)
#define partial_FoRT_del_C7 (GlobalData_t)(0.)
#define partial_FoRT_del_C8 (GlobalData_t)(0.)
#define partial_FoRT_del_C9 (GlobalData_t)(0.)
#define partial_FoRT_del_O1 (GlobalData_t)(0.)
#define partial_FoRT_del_RyRi (GlobalData_t)(0.)
#define partial_FoRT_del_RyRo (GlobalData_t)(0.)
#define partial_FoRT_del_RyRr (GlobalData_t)(0.)
#define partial_Frdy_del_C1 (GlobalData_t)(0.)
#define partial_Frdy_del_C10 (GlobalData_t)(0.)
#define partial_Frdy_del_C11 (GlobalData_t)(0.)
#define partial_Frdy_del_C12 (GlobalData_t)(0.)
#define partial_Frdy_del_C13 (GlobalData_t)(0.)
#define partial_Frdy_del_C14 (GlobalData_t)(0.)
#define partial_Frdy_del_C15 (GlobalData_t)(0.)
#define partial_Frdy_del_C2 (GlobalData_t)(0.)
#define partial_Frdy_del_C3 (GlobalData_t)(0.)
#define partial_Frdy_del_C4 (GlobalData_t)(0.)
#define partial_Frdy_del_C5 (GlobalData_t)(0.)
#define partial_Frdy_del_C6 (GlobalData_t)(0.)
#define partial_Frdy_del_C7 (GlobalData_t)(0.)
#define partial_Frdy_del_C8 (GlobalData_t)(0.)
#define partial_Frdy_del_C9 (GlobalData_t)(0.)
#define partial_Frdy_del_O1 (GlobalData_t)(0.)
#define partial_Frdy_del_RyRi (GlobalData_t)(0.)
#define partial_Frdy_del_RyRo (GlobalData_t)(0.)
#define partial_Frdy_del_RyRr (GlobalData_t)(0.)
#define partial_ISO_del_C1 (GlobalData_t)(0.)
#define partial_ISO_del_C10 (GlobalData_t)(0.)
#define partial_ISO_del_C11 (GlobalData_t)(0.)
#define partial_ISO_del_C12 (GlobalData_t)(0.)
#define partial_ISO_del_C13 (GlobalData_t)(0.)
#define partial_ISO_del_C14 (GlobalData_t)(0.)
#define partial_ISO_del_C15 (GlobalData_t)(0.)
#define partial_ISO_del_C2 (GlobalData_t)(0.)
#define partial_ISO_del_C3 (GlobalData_t)(0.)
#define partial_ISO_del_C4 (GlobalData_t)(0.)
#define partial_ISO_del_C5 (GlobalData_t)(0.)
#define partial_ISO_del_C6 (GlobalData_t)(0.)
#define partial_ISO_del_C7 (GlobalData_t)(0.)
#define partial_ISO_del_C8 (GlobalData_t)(0.)
#define partial_ISO_del_C9 (GlobalData_t)(0.)
#define partial_ISO_del_O1 (GlobalData_t)(0.)
#define partial_ISO_del_RyRi (GlobalData_t)(0.)
#define partial_ISO_del_RyRo (GlobalData_t)(0.)
#define partial_ISO_del_RyRr (GlobalData_t)(0.)
#define partial_MaxSR_del_C1 (GlobalData_t)(0.)
#define partial_MaxSR_del_C10 (GlobalData_t)(0.)
#define partial_MaxSR_del_C11 (GlobalData_t)(0.)
#define partial_MaxSR_del_C12 (GlobalData_t)(0.)
#define partial_MaxSR_del_C13 (GlobalData_t)(0.)
#define partial_MaxSR_del_C14 (GlobalData_t)(0.)
#define partial_MaxSR_del_C15 (GlobalData_t)(0.)
#define partial_MaxSR_del_C2 (GlobalData_t)(0.)
#define partial_MaxSR_del_C3 (GlobalData_t)(0.)
#define partial_MaxSR_del_C4 (GlobalData_t)(0.)
#define partial_MaxSR_del_C5 (GlobalData_t)(0.)
#define partial_MaxSR_del_C6 (GlobalData_t)(0.)
#define partial_MaxSR_del_C7 (GlobalData_t)(0.)
#define partial_MaxSR_del_C8 (GlobalData_t)(0.)
#define partial_MaxSR_del_C9 (GlobalData_t)(0.)
#define partial_MaxSR_del_O1 (GlobalData_t)(0.)
#define partial_MaxSR_del_RyRi (GlobalData_t)(0.)
#define partial_MaxSR_del_RyRo (GlobalData_t)(0.)
#define partial_MaxSR_del_RyRr (GlobalData_t)(0.)
#define partial_MinSR_del_C1 (GlobalData_t)(0.)
#define partial_MinSR_del_C10 (GlobalData_t)(0.)
#define partial_MinSR_del_C11 (GlobalData_t)(0.)
#define partial_MinSR_del_C12 (GlobalData_t)(0.)
#define partial_MinSR_del_C13 (GlobalData_t)(0.)
#define partial_MinSR_del_C14 (GlobalData_t)(0.)
#define partial_MinSR_del_C15 (GlobalData_t)(0.)
#define partial_MinSR_del_C2 (GlobalData_t)(0.)
#define partial_MinSR_del_C3 (GlobalData_t)(0.)
#define partial_MinSR_del_C4 (GlobalData_t)(0.)
#define partial_MinSR_del_C5 (GlobalData_t)(0.)
#define partial_MinSR_del_C6 (GlobalData_t)(0.)
#define partial_MinSR_del_C7 (GlobalData_t)(0.)
#define partial_MinSR_del_C8 (GlobalData_t)(0.)
#define partial_MinSR_del_C9 (GlobalData_t)(0.)
#define partial_MinSR_del_O1 (GlobalData_t)(0.)
#define partial_MinSR_del_RyRi (GlobalData_t)(0.)
#define partial_MinSR_del_RyRo (GlobalData_t)(0.)
#define partial_MinSR_del_RyRr (GlobalData_t)(0.)
#define partial_O2_del_C1 (GlobalData_t)(-1.)
#define partial_O2_del_C10 (GlobalData_t)(-1.)
#define partial_O2_del_C11 (GlobalData_t)(-1.)
#define partial_O2_del_C12 (GlobalData_t)(-1.)
#define partial_O2_del_C13 (GlobalData_t)(-1.)
#define partial_O2_del_C14 (GlobalData_t)(-1.)
#define partial_O2_del_C15 (GlobalData_t)(-1.)
#define partial_O2_del_C2 (GlobalData_t)(-1.)
#define partial_O2_del_C3 (GlobalData_t)(-1.)
#define partial_O2_del_C4 (GlobalData_t)(-1.)
#define partial_O2_del_C5 (GlobalData_t)(-1.)
#define partial_O2_del_C6 (GlobalData_t)(-1.)
#define partial_O2_del_C7 (GlobalData_t)(-1.)
#define partial_O2_del_C8 (GlobalData_t)(-1.)
#define partial_O2_del_C9 (GlobalData_t)(-1.)
#define partial_O2_del_O1 (GlobalData_t)(-1.)
#define partial_O2_del_RyRi (GlobalData_t)(0.)
#define partial_O2_del_RyRo (GlobalData_t)(0.)
#define partial_O2_del_RyRr (GlobalData_t)(0.)
#define partial_RI_del_C1 (GlobalData_t)(0.)
#define partial_RI_del_C10 (GlobalData_t)(0.)
#define partial_RI_del_C11 (GlobalData_t)(0.)
#define partial_RI_del_C12 (GlobalData_t)(0.)
#define partial_RI_del_C13 (GlobalData_t)(0.)
#define partial_RI_del_C14 (GlobalData_t)(0.)
#define partial_RI_del_C15 (GlobalData_t)(0.)
#define partial_RI_del_C2 (GlobalData_t)(0.)
#define partial_RI_del_C3 (GlobalData_t)(0.)
#define partial_RI_del_C4 (GlobalData_t)(0.)
#define partial_RI_del_C5 (GlobalData_t)(0.)
#define partial_RI_del_C6 (GlobalData_t)(0.)
#define partial_RI_del_C7 (GlobalData_t)(0.)
#define partial_RI_del_C8 (GlobalData_t)(0.)
#define partial_RI_del_C9 (GlobalData_t)(0.)
#define partial_RI_del_O1 (GlobalData_t)(0.)
#define partial_RI_del_RyRi (GlobalData_t)(-1.)
#define partial_RI_del_RyRo (GlobalData_t)(-1.)
#define partial_RI_del_RyRr (GlobalData_t)(-1.)
#define partial_R_del_C1 (GlobalData_t)(0.)
#define partial_R_del_C10 (GlobalData_t)(0.)
#define partial_R_del_C11 (GlobalData_t)(0.)
#define partial_R_del_C12 (GlobalData_t)(0.)
#define partial_R_del_C13 (GlobalData_t)(0.)
#define partial_R_del_C14 (GlobalData_t)(0.)
#define partial_R_del_C15 (GlobalData_t)(0.)
#define partial_R_del_C2 (GlobalData_t)(0.)
#define partial_R_del_C3 (GlobalData_t)(0.)
#define partial_R_del_C4 (GlobalData_t)(0.)
#define partial_R_del_C5 (GlobalData_t)(0.)
#define partial_R_del_C6 (GlobalData_t)(0.)
#define partial_R_del_C7 (GlobalData_t)(0.)
#define partial_R_del_C8 (GlobalData_t)(0.)
#define partial_R_del_C9 (GlobalData_t)(0.)
#define partial_R_del_O1 (GlobalData_t)(0.)
#define partial_R_del_RyRi (GlobalData_t)(0.)
#define partial_R_del_RyRo (GlobalData_t)(0.)
#define partial_R_del_RyRr (GlobalData_t)(0.)
#define partial_Temp_del_C1 (GlobalData_t)(0.)
#define partial_Temp_del_C10 (GlobalData_t)(0.)
#define partial_Temp_del_C11 (GlobalData_t)(0.)
#define partial_Temp_del_C12 (GlobalData_t)(0.)
#define partial_Temp_del_C13 (GlobalData_t)(0.)
#define partial_Temp_del_C14 (GlobalData_t)(0.)
#define partial_Temp_del_C15 (GlobalData_t)(0.)
#define partial_Temp_del_C2 (GlobalData_t)(0.)
#define partial_Temp_del_C3 (GlobalData_t)(0.)
#define partial_Temp_del_C4 (GlobalData_t)(0.)
#define partial_Temp_del_C5 (GlobalData_t)(0.)
#define partial_Temp_del_C6 (GlobalData_t)(0.)
#define partial_Temp_del_C7 (GlobalData_t)(0.)
#define partial_Temp_del_C8 (GlobalData_t)(0.)
#define partial_Temp_del_C9 (GlobalData_t)(0.)
#define partial_Temp_del_O1 (GlobalData_t)(0.)
#define partial_Temp_del_RyRi (GlobalData_t)(0.)
#define partial_Temp_del_RyRo (GlobalData_t)(0.)
#define partial_Temp_del_RyRr (GlobalData_t)(0.)
#define partial_alpha_del_C1 (GlobalData_t)(0.)
#define partial_alpha_del_C10 (GlobalData_t)(0.)
#define partial_alpha_del_C11 (GlobalData_t)(0.)
#define partial_alpha_del_C12 (GlobalData_t)(0.)
#define partial_alpha_del_C13 (GlobalData_t)(0.)
#define partial_alpha_del_C14 (GlobalData_t)(0.)
#define partial_alpha_del_C15 (GlobalData_t)(0.)
#define partial_alpha_del_C2 (GlobalData_t)(0.)
#define partial_alpha_del_C3 (GlobalData_t)(0.)
#define partial_alpha_del_C4 (GlobalData_t)(0.)
#define partial_alpha_del_C5 (GlobalData_t)(0.)
#define partial_alpha_del_C6 (GlobalData_t)(0.)
#define partial_alpha_del_C7 (GlobalData_t)(0.)
#define partial_alpha_del_C8 (GlobalData_t)(0.)
#define partial_alpha_del_C9 (GlobalData_t)(0.)
#define partial_alpha_del_O1 (GlobalData_t)(0.)
#define partial_alpha_del_RyRi (GlobalData_t)(0.)
#define partial_alpha_del_RyRo (GlobalData_t)(0.)
#define partial_alpha_del_RyRr (GlobalData_t)(0.)
#define partial_beta_del_C1 (GlobalData_t)(0.)
#define partial_beta_del_C10 (GlobalData_t)(0.)
#define partial_beta_del_C11 (GlobalData_t)(0.)
#define partial_beta_del_C12 (GlobalData_t)(0.)
#define partial_beta_del_C13 (GlobalData_t)(0.)
#define partial_beta_del_C14 (GlobalData_t)(0.)
#define partial_beta_del_C15 (GlobalData_t)(0.)
#define partial_beta_del_C2 (GlobalData_t)(0.)
#define partial_beta_del_C3 (GlobalData_t)(0.)
#define partial_beta_del_C4 (GlobalData_t)(0.)
#define partial_beta_del_C5 (GlobalData_t)(0.)
#define partial_beta_del_C6 (GlobalData_t)(0.)
#define partial_beta_del_C7 (GlobalData_t)(0.)
#define partial_beta_del_C8 (GlobalData_t)(0.)
#define partial_beta_del_C9 (GlobalData_t)(0.)
#define partial_beta_del_O1 (GlobalData_t)(0.)
#define partial_beta_del_RyRi (GlobalData_t)(0.)
#define partial_beta_del_RyRo (GlobalData_t)(0.)
#define partial_beta_del_RyRr (GlobalData_t)(0.)
#define partial_delta_del_C1 (GlobalData_t)(0.)
#define partial_delta_del_C10 (GlobalData_t)(0.)
#define partial_delta_del_C11 (GlobalData_t)(0.)
#define partial_delta_del_C12 (GlobalData_t)(0.)
#define partial_delta_del_C13 (GlobalData_t)(0.)
#define partial_delta_del_C14 (GlobalData_t)(0.)
#define partial_delta_del_C15 (GlobalData_t)(0.)
#define partial_delta_del_C2 (GlobalData_t)(0.)
#define partial_delta_del_C3 (GlobalData_t)(0.)
#define partial_delta_del_C4 (GlobalData_t)(0.)
#define partial_delta_del_C5 (GlobalData_t)(0.)
#define partial_delta_del_C6 (GlobalData_t)(0.)
#define partial_delta_del_C7 (GlobalData_t)(0.)
#define partial_delta_del_C8 (GlobalData_t)(0.)
#define partial_delta_del_C9 (GlobalData_t)(0.)
#define partial_delta_del_O1 (GlobalData_t)(0.)
#define partial_delta_del_RyRi (GlobalData_t)(0.)
#define partial_delta_del_RyRo (GlobalData_t)(0.)
#define partial_delta_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C10_del_C1 (GlobalData_t)(0.)
#define partial_diff_C10_del_C12 (GlobalData_t)(0.)
#define partial_diff_C10_del_C13 (GlobalData_t)(0.)
#define partial_diff_C10_del_C14 (GlobalData_t)(0.)
#define partial_diff_C10_del_C15 (GlobalData_t)(0.)
#define partial_diff_C10_del_C2 (GlobalData_t)(0.)
#define partial_diff_C10_del_C3 (GlobalData_t)(0.)
#define partial_diff_C10_del_C4 (GlobalData_t)(0.)
#define partial_diff_C10_del_C5 (GlobalData_t)(0.)
#define partial_diff_C10_del_C6 (GlobalData_t)(0.)
#define partial_diff_C10_del_C8 (GlobalData_t)(0.)
#define partial_diff_C10_del_C9 (GlobalData_t)(0.)
#define partial_diff_C10_del_O1 (GlobalData_t)(0.)
#define partial_diff_C10_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C10_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C10_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C11_del_C1 (GlobalData_t)(0.)
#define partial_diff_C11_del_C14 (GlobalData_t)(0.)
#define partial_diff_C11_del_C15 (GlobalData_t)(0.)
#define partial_diff_C11_del_C2 (GlobalData_t)(0.)
#define partial_diff_C11_del_C3 (GlobalData_t)(0.)
#define partial_diff_C11_del_C4 (GlobalData_t)(0.)
#define partial_diff_C11_del_C5 (GlobalData_t)(0.)
#define partial_diff_C11_del_C6 (GlobalData_t)(0.)
#define partial_diff_C11_del_C7 (GlobalData_t)(0.)
#define partial_diff_C11_del_C9 (GlobalData_t)(0.)
#define partial_diff_C11_del_O1 (GlobalData_t)(0.)
#define partial_diff_C11_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C11_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C11_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C12_del_C1 (GlobalData_t)(0.)
#define partial_diff_C12_del_C10 (GlobalData_t)(0.)
#define partial_diff_C12_del_C13 (GlobalData_t)(0.)
#define partial_diff_C12_del_C15 (GlobalData_t)(0.)
#define partial_diff_C12_del_C2 (GlobalData_t)(0.)
#define partial_diff_C12_del_C3 (GlobalData_t)(0.)
#define partial_diff_C12_del_C4 (GlobalData_t)(0.)
#define partial_diff_C12_del_C5 (GlobalData_t)(0.)
#define partial_diff_C12_del_C6 (GlobalData_t)(0.)
#define partial_diff_C12_del_C7 (GlobalData_t)(0.)
#define partial_diff_C12_del_C8 (GlobalData_t)(0.)
#define partial_diff_C12_del_O1 (GlobalData_t)(0.)
#define partial_diff_C12_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C12_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C12_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C13_del_C1 (GlobalData_t)(0.)
#define partial_diff_C13_del_C10 (GlobalData_t)(0.)
#define partial_diff_C13_del_C12 (GlobalData_t)(0.)
#define partial_diff_C13_del_C15 (GlobalData_t)(0.)
#define partial_diff_C13_del_C2 (GlobalData_t)(0.)
#define partial_diff_C13_del_C3 (GlobalData_t)(0.)
#define partial_diff_C13_del_C4 (GlobalData_t)(0.)
#define partial_diff_C13_del_C5 (GlobalData_t)(0.)
#define partial_diff_C13_del_C6 (GlobalData_t)(0.)
#define partial_diff_C13_del_C7 (GlobalData_t)(0.)
#define partial_diff_C13_del_C8 (GlobalData_t)(0.)
#define partial_diff_C13_del_C9 (GlobalData_t)(0.)
#define partial_diff_C13_del_O1 (GlobalData_t)(0.)
#define partial_diff_C13_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C13_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C13_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C14_del_C1 (GlobalData_t)(0.)
#define partial_diff_C14_del_C10 (GlobalData_t)(0.)
#define partial_diff_C14_del_C11 (GlobalData_t)(0.)
#define partial_diff_C14_del_C2 (GlobalData_t)(0.)
#define partial_diff_C14_del_C3 (GlobalData_t)(0.)
#define partial_diff_C14_del_C4 (GlobalData_t)(0.)
#define partial_diff_C14_del_C5 (GlobalData_t)(0.)
#define partial_diff_C14_del_C6 (GlobalData_t)(0.)
#define partial_diff_C14_del_C7 (GlobalData_t)(0.)
#define partial_diff_C14_del_C8 (GlobalData_t)(0.)
#define partial_diff_C14_del_C9 (GlobalData_t)(0.)
#define partial_diff_C14_del_O1 (GlobalData_t)(0.)
#define partial_diff_C14_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C14_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C14_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C15_del_C1 (GlobalData_t)(0.)
#define partial_diff_C15_del_C10 (GlobalData_t)(0.)
#define partial_diff_C15_del_C11 (GlobalData_t)(0.)
#define partial_diff_C15_del_C12 (GlobalData_t)(0.)
#define partial_diff_C15_del_C13 (GlobalData_t)(0.)
#define partial_diff_C15_del_C2 (GlobalData_t)(0.)
#define partial_diff_C15_del_C3 (GlobalData_t)(0.)
#define partial_diff_C15_del_C4 (GlobalData_t)(0.)
#define partial_diff_C15_del_C5 (GlobalData_t)(0.)
#define partial_diff_C15_del_C6 (GlobalData_t)(0.)
#define partial_diff_C15_del_C7 (GlobalData_t)(0.)
#define partial_diff_C15_del_C8 (GlobalData_t)(0.)
#define partial_diff_C15_del_C9 (GlobalData_t)(0.)
#define partial_diff_C15_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C15_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C15_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C1_del_C10 (GlobalData_t)(0.)
#define partial_diff_C1_del_C11 (GlobalData_t)(0.)
#define partial_diff_C1_del_C12 (GlobalData_t)(0.)
#define partial_diff_C1_del_C13 (GlobalData_t)(0.)
#define partial_diff_C1_del_C14 (GlobalData_t)(0.)
#define partial_diff_C1_del_C15 (GlobalData_t)(0.)
#define partial_diff_C1_del_C3 (GlobalData_t)(0.)
#define partial_diff_C1_del_C4 (GlobalData_t)(0.)
#define partial_diff_C1_del_C5 (GlobalData_t)(0.)
#define partial_diff_C1_del_C6 (GlobalData_t)(0.)
#define partial_diff_C1_del_C7 (GlobalData_t)(0.)
#define partial_diff_C1_del_C8 (GlobalData_t)(0.)
#define partial_diff_C1_del_C9 (GlobalData_t)(0.)
#define partial_diff_C1_del_O1 (GlobalData_t)(0.)
#define partial_diff_C1_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C1_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C1_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C2_del_C10 (GlobalData_t)(0.)
#define partial_diff_C2_del_C11 (GlobalData_t)(0.)
#define partial_diff_C2_del_C12 (GlobalData_t)(0.)
#define partial_diff_C2_del_C13 (GlobalData_t)(0.)
#define partial_diff_C2_del_C14 (GlobalData_t)(0.)
#define partial_diff_C2_del_C15 (GlobalData_t)(0.)
#define partial_diff_C2_del_C4 (GlobalData_t)(0.)
#define partial_diff_C2_del_C5 (GlobalData_t)(0.)
#define partial_diff_C2_del_C6 (GlobalData_t)(0.)
#define partial_diff_C2_del_C7 (GlobalData_t)(0.)
#define partial_diff_C2_del_C8 (GlobalData_t)(0.)
#define partial_diff_C2_del_C9 (GlobalData_t)(0.)
#define partial_diff_C2_del_O1 (GlobalData_t)(0.)
#define partial_diff_C2_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C2_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C2_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C3_del_C1 (GlobalData_t)(0.)
#define partial_diff_C3_del_C10 (GlobalData_t)(0.)
#define partial_diff_C3_del_C11 (GlobalData_t)(0.)
#define partial_diff_C3_del_C12 (GlobalData_t)(0.)
#define partial_diff_C3_del_C13 (GlobalData_t)(0.)
#define partial_diff_C3_del_C14 (GlobalData_t)(0.)
#define partial_diff_C3_del_C15 (GlobalData_t)(0.)
#define partial_diff_C3_del_C5 (GlobalData_t)(0.)
#define partial_diff_C3_del_C6 (GlobalData_t)(0.)
#define partial_diff_C3_del_C7 (GlobalData_t)(0.)
#define partial_diff_C3_del_C8 (GlobalData_t)(0.)
#define partial_diff_C3_del_C9 (GlobalData_t)(0.)
#define partial_diff_C3_del_O1 (GlobalData_t)(0.)
#define partial_diff_C3_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C3_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C3_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C4_del_C1 (GlobalData_t)(0.)
#define partial_diff_C4_del_C10 (GlobalData_t)(0.)
#define partial_diff_C4_del_C11 (GlobalData_t)(0.)
#define partial_diff_C4_del_C12 (GlobalData_t)(0.)
#define partial_diff_C4_del_C13 (GlobalData_t)(0.)
#define partial_diff_C4_del_C14 (GlobalData_t)(0.)
#define partial_diff_C4_del_C15 (GlobalData_t)(0.)
#define partial_diff_C4_del_C2 (GlobalData_t)(0.)
#define partial_diff_C4_del_C6 (GlobalData_t)(0.)
#define partial_diff_C4_del_C7 (GlobalData_t)(0.)
#define partial_diff_C4_del_C8 (GlobalData_t)(0.)
#define partial_diff_C4_del_C9 (GlobalData_t)(0.)
#define partial_diff_C4_del_O1 (GlobalData_t)(0.)
#define partial_diff_C4_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C4_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C4_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C5_del_C1 (GlobalData_t)(0.)
#define partial_diff_C5_del_C10 (GlobalData_t)(0.)
#define partial_diff_C5_del_C11 (GlobalData_t)(0.)
#define partial_diff_C5_del_C12 (GlobalData_t)(0.)
#define partial_diff_C5_del_C13 (GlobalData_t)(0.)
#define partial_diff_C5_del_C14 (GlobalData_t)(0.)
#define partial_diff_C5_del_C15 (GlobalData_t)(0.)
#define partial_diff_C5_del_C2 (GlobalData_t)(0.)
#define partial_diff_C5_del_C4 (GlobalData_t)(0.)
#define partial_diff_C5_del_C6 (GlobalData_t)(0.)
#define partial_diff_C5_del_C7 (GlobalData_t)(0.)
#define partial_diff_C5_del_C8 (GlobalData_t)(0.)
#define partial_diff_C5_del_O1 (GlobalData_t)(0.)
#define partial_diff_C5_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C5_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C5_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C6_del_C1 (GlobalData_t)(0.)
#define partial_diff_C6_del_C10 (GlobalData_t)(0.)
#define partial_diff_C6_del_C11 (GlobalData_t)(0.)
#define partial_diff_C6_del_C12 (GlobalData_t)(0.)
#define partial_diff_C6_del_C13 (GlobalData_t)(0.)
#define partial_diff_C6_del_C14 (GlobalData_t)(0.)
#define partial_diff_C6_del_C15 (GlobalData_t)(0.)
#define partial_diff_C6_del_C3 (GlobalData_t)(0.)
#define partial_diff_C6_del_C4 (GlobalData_t)(0.)
#define partial_diff_C6_del_C5 (GlobalData_t)(0.)
#define partial_diff_C6_del_C8 (GlobalData_t)(0.)
#define partial_diff_C6_del_C9 (GlobalData_t)(0.)
#define partial_diff_C6_del_O1 (GlobalData_t)(0.)
#define partial_diff_C6_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C6_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C6_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C7_del_C1 (GlobalData_t)(0.)
#define partial_diff_C7_del_C11 (GlobalData_t)(0.)
#define partial_diff_C7_del_C12 (GlobalData_t)(0.)
#define partial_diff_C7_del_C13 (GlobalData_t)(0.)
#define partial_diff_C7_del_C14 (GlobalData_t)(0.)
#define partial_diff_C7_del_C15 (GlobalData_t)(0.)
#define partial_diff_C7_del_C2 (GlobalData_t)(0.)
#define partial_diff_C7_del_C4 (GlobalData_t)(0.)
#define partial_diff_C7_del_C5 (GlobalData_t)(0.)
#define partial_diff_C7_del_C9 (GlobalData_t)(0.)
#define partial_diff_C7_del_O1 (GlobalData_t)(0.)
#define partial_diff_C7_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C7_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C7_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C8_del_C1 (GlobalData_t)(0.)
#define partial_diff_C8_del_C10 (GlobalData_t)(0.)
#define partial_diff_C8_del_C12 (GlobalData_t)(0.)
#define partial_diff_C8_del_C13 (GlobalData_t)(0.)
#define partial_diff_C8_del_C14 (GlobalData_t)(0.)
#define partial_diff_C8_del_C15 (GlobalData_t)(0.)
#define partial_diff_C8_del_C2 (GlobalData_t)(0.)
#define partial_diff_C8_del_C3 (GlobalData_t)(0.)
#define partial_diff_C8_del_C5 (GlobalData_t)(0.)
#define partial_diff_C8_del_C6 (GlobalData_t)(0.)
#define partial_diff_C8_del_O1 (GlobalData_t)(0.)
#define partial_diff_C8_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C8_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C8_del_RyRr (GlobalData_t)(0.)
#define partial_diff_C9_del_C1 (GlobalData_t)(0.)
#define partial_diff_C9_del_C10 (GlobalData_t)(0.)
#define partial_diff_C9_del_C11 (GlobalData_t)(0.)
#define partial_diff_C9_del_C13 (GlobalData_t)(0.)
#define partial_diff_C9_del_C14 (GlobalData_t)(0.)
#define partial_diff_C9_del_C15 (GlobalData_t)(0.)
#define partial_diff_C9_del_C2 (GlobalData_t)(0.)
#define partial_diff_C9_del_C3 (GlobalData_t)(0.)
#define partial_diff_C9_del_C4 (GlobalData_t)(0.)
#define partial_diff_C9_del_C6 (GlobalData_t)(0.)
#define partial_diff_C9_del_C7 (GlobalData_t)(0.)
#define partial_diff_C9_del_O1 (GlobalData_t)(0.)
#define partial_diff_C9_del_RyRi (GlobalData_t)(0.)
#define partial_diff_C9_del_RyRo (GlobalData_t)(0.)
#define partial_diff_C9_del_RyRr (GlobalData_t)(0.)
#define partial_diff_O1_del_RyRi (GlobalData_t)(0.)
#define partial_diff_O1_del_RyRo (GlobalData_t)(0.)
#define partial_diff_O1_del_RyRr (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C1 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C10 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C11 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C12 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C13 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C14 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C15 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C2 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C3 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C4 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C5 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C6 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C7 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C8 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_C9 (GlobalData_t)(0.)
#define partial_diff_RyRi_del_O1 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C1 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C10 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C11 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C12 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C13 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C14 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C15 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C2 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C3 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C4 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C5 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C6 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C7 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C8 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_C9 (GlobalData_t)(0.)
#define partial_diff_RyRo_del_O1 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C1 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C10 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C11 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C12 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C13 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C14 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C15 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C2 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C3 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C4 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C5 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C6 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C7 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C8 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_C9 (GlobalData_t)(0.)
#define partial_diff_RyRr_del_O1 (GlobalData_t)(0.)
#define partial_ec50SR_del_C1 (GlobalData_t)(0.)
#define partial_ec50SR_del_C10 (GlobalData_t)(0.)
#define partial_ec50SR_del_C11 (GlobalData_t)(0.)
#define partial_ec50SR_del_C12 (GlobalData_t)(0.)
#define partial_ec50SR_del_C13 (GlobalData_t)(0.)
#define partial_ec50SR_del_C14 (GlobalData_t)(0.)
#define partial_ec50SR_del_C15 (GlobalData_t)(0.)
#define partial_ec50SR_del_C2 (GlobalData_t)(0.)
#define partial_ec50SR_del_C3 (GlobalData_t)(0.)
#define partial_ec50SR_del_C4 (GlobalData_t)(0.)
#define partial_ec50SR_del_C5 (GlobalData_t)(0.)
#define partial_ec50SR_del_C6 (GlobalData_t)(0.)
#define partial_ec50SR_del_C7 (GlobalData_t)(0.)
#define partial_ec50SR_del_C8 (GlobalData_t)(0.)
#define partial_ec50SR_del_C9 (GlobalData_t)(0.)
#define partial_ec50SR_del_O1 (GlobalData_t)(0.)
#define partial_ec50SR_del_RyRi (GlobalData_t)(0.)
#define partial_ec50SR_del_RyRo (GlobalData_t)(0.)
#define partial_ec50SR_del_RyRr (GlobalData_t)(0.)
#define partial_eta_del_C1 (GlobalData_t)(0.)
#define partial_eta_del_C10 (GlobalData_t)(0.)
#define partial_eta_del_C11 (GlobalData_t)(0.)
#define partial_eta_del_C12 (GlobalData_t)(0.)
#define partial_eta_del_C13 (GlobalData_t)(0.)
#define partial_eta_del_C14 (GlobalData_t)(0.)
#define partial_eta_del_C15 (GlobalData_t)(0.)
#define partial_eta_del_C2 (GlobalData_t)(0.)
#define partial_eta_del_C3 (GlobalData_t)(0.)
#define partial_eta_del_C4 (GlobalData_t)(0.)
#define partial_eta_del_C5 (GlobalData_t)(0.)
#define partial_eta_del_C6 (GlobalData_t)(0.)
#define partial_eta_del_C7 (GlobalData_t)(0.)
#define partial_eta_del_C8 (GlobalData_t)(0.)
#define partial_eta_del_C9 (GlobalData_t)(0.)
#define partial_eta_del_O1 (GlobalData_t)(0.)
#define partial_eta_del_RyRi (GlobalData_t)(0.)
#define partial_eta_del_RyRo (GlobalData_t)(0.)
#define partial_eta_del_RyRr (GlobalData_t)(0.)
#define partial_gamma_del_C1 (GlobalData_t)(0.)
#define partial_gamma_del_C10 (GlobalData_t)(0.)
#define partial_gamma_del_C11 (GlobalData_t)(0.)
#define partial_gamma_del_C12 (GlobalData_t)(0.)
#define partial_gamma_del_C13 (GlobalData_t)(0.)
#define partial_gamma_del_C14 (GlobalData_t)(0.)
#define partial_gamma_del_C15 (GlobalData_t)(0.)
#define partial_gamma_del_C2 (GlobalData_t)(0.)
#define partial_gamma_del_C3 (GlobalData_t)(0.)
#define partial_gamma_del_C4 (GlobalData_t)(0.)
#define partial_gamma_del_C5 (GlobalData_t)(0.)
#define partial_gamma_del_C6 (GlobalData_t)(0.)
#define partial_gamma_del_C7 (GlobalData_t)(0.)
#define partial_gamma_del_C8 (GlobalData_t)(0.)
#define partial_gamma_del_C9 (GlobalData_t)(0.)
#define partial_gamma_del_O1 (GlobalData_t)(0.)
#define partial_gamma_del_RyRi (GlobalData_t)(0.)
#define partial_gamma_del_RyRo (GlobalData_t)(0.)
#define partial_gamma_del_RyRr (GlobalData_t)(0.)
#define partial_kCaSR_del_C1 (GlobalData_t)(0.)
#define partial_kCaSR_del_C10 (GlobalData_t)(0.)
#define partial_kCaSR_del_C11 (GlobalData_t)(0.)
#define partial_kCaSR_del_C12 (GlobalData_t)(0.)
#define partial_kCaSR_del_C13 (GlobalData_t)(0.)
#define partial_kCaSR_del_C14 (GlobalData_t)(0.)
#define partial_kCaSR_del_C15 (GlobalData_t)(0.)
#define partial_kCaSR_del_C2 (GlobalData_t)(0.)
#define partial_kCaSR_del_C3 (GlobalData_t)(0.)
#define partial_kCaSR_del_C4 (GlobalData_t)(0.)
#define partial_kCaSR_del_C5 (GlobalData_t)(0.)
#define partial_kCaSR_del_C6 (GlobalData_t)(0.)
#define partial_kCaSR_del_C7 (GlobalData_t)(0.)
#define partial_kCaSR_del_C8 (GlobalData_t)(0.)
#define partial_kCaSR_del_C9 (GlobalData_t)(0.)
#define partial_kCaSR_del_O1 (GlobalData_t)(0.)
#define partial_kCaSR_del_RyRi (GlobalData_t)(0.)
#define partial_kCaSR_del_RyRo (GlobalData_t)(0.)
#define partial_kCaSR_del_RyRr (GlobalData_t)(0.)
#define partial_kiCa_del_C1 (GlobalData_t)(0.)
#define partial_kiCa_del_C10 (GlobalData_t)(0.)
#define partial_kiCa_del_C11 (GlobalData_t)(0.)
#define partial_kiCa_del_C12 (GlobalData_t)(0.)
#define partial_kiCa_del_C13 (GlobalData_t)(0.)
#define partial_kiCa_del_C14 (GlobalData_t)(0.)
#define partial_kiCa_del_C15 (GlobalData_t)(0.)
#define partial_kiCa_del_C2 (GlobalData_t)(0.)
#define partial_kiCa_del_C3 (GlobalData_t)(0.)
#define partial_kiCa_del_C4 (GlobalData_t)(0.)
#define partial_kiCa_del_C5 (GlobalData_t)(0.)
#define partial_kiCa_del_C6 (GlobalData_t)(0.)
#define partial_kiCa_del_C7 (GlobalData_t)(0.)
#define partial_kiCa_del_C8 (GlobalData_t)(0.)
#define partial_kiCa_del_C9 (GlobalData_t)(0.)
#define partial_kiCa_del_O1 (GlobalData_t)(0.)
#define partial_kiCa_del_RyRi (GlobalData_t)(0.)
#define partial_kiCa_del_RyRo (GlobalData_t)(0.)
#define partial_kiCa_del_RyRr (GlobalData_t)(0.)
#define partial_kiSRCa_del_C1 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C10 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C11 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C12 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C13 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C14 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C15 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C2 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C3 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C4 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C5 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C6 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C7 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C8 (GlobalData_t)(0.)
#define partial_kiSRCa_del_C9 (GlobalData_t)(0.)
#define partial_kiSRCa_del_O1 (GlobalData_t)(0.)
#define partial_kiSRCa_del_RyRi (GlobalData_t)(0.)
#define partial_kiSRCa_del_RyRo (GlobalData_t)(0.)
#define partial_kiSRCa_del_RyRr (GlobalData_t)(0.)
#define partial_kim_del_C1 (GlobalData_t)(0.)
#define partial_kim_del_C10 (GlobalData_t)(0.)
#define partial_kim_del_C11 (GlobalData_t)(0.)
#define partial_kim_del_C12 (GlobalData_t)(0.)
#define partial_kim_del_C13 (GlobalData_t)(0.)
#define partial_kim_del_C14 (GlobalData_t)(0.)
#define partial_kim_del_C15 (GlobalData_t)(0.)
#define partial_kim_del_C2 (GlobalData_t)(0.)
#define partial_kim_del_C3 (GlobalData_t)(0.)
#define partial_kim_del_C4 (GlobalData_t)(0.)
#define partial_kim_del_C5 (GlobalData_t)(0.)
#define partial_kim_del_C6 (GlobalData_t)(0.)
#define partial_kim_del_C7 (GlobalData_t)(0.)
#define partial_kim_del_C8 (GlobalData_t)(0.)
#define partial_kim_del_C9 (GlobalData_t)(0.)
#define partial_kim_del_O1 (GlobalData_t)(0.)
#define partial_kim_del_RyRi (GlobalData_t)(0.)
#define partial_kim_del_RyRo (GlobalData_t)(0.)
#define partial_kim_del_RyRr (GlobalData_t)(0.)
#define partial_koCa_del_C1 (GlobalData_t)(0.)
#define partial_koCa_del_C10 (GlobalData_t)(0.)
#define partial_koCa_del_C11 (GlobalData_t)(0.)
#define partial_koCa_del_C12 (GlobalData_t)(0.)
#define partial_koCa_del_C13 (GlobalData_t)(0.)
#define partial_koCa_del_C14 (GlobalData_t)(0.)
#define partial_koCa_del_C15 (GlobalData_t)(0.)
#define partial_koCa_del_C2 (GlobalData_t)(0.)
#define partial_koCa_del_C3 (GlobalData_t)(0.)
#define partial_koCa_del_C4 (GlobalData_t)(0.)
#define partial_koCa_del_C5 (GlobalData_t)(0.)
#define partial_koCa_del_C6 (GlobalData_t)(0.)
#define partial_koCa_del_C7 (GlobalData_t)(0.)
#define partial_koCa_del_C8 (GlobalData_t)(0.)
#define partial_koCa_del_C9 (GlobalData_t)(0.)
#define partial_koCa_del_O1 (GlobalData_t)(0.)
#define partial_koCa_del_RyRi (GlobalData_t)(0.)
#define partial_koCa_del_RyRo (GlobalData_t)(0.)
#define partial_koCa_del_RyRr (GlobalData_t)(0.)
#define partial_koSRCa_del_C1 (GlobalData_t)(0.)
#define partial_koSRCa_del_C10 (GlobalData_t)(0.)
#define partial_koSRCa_del_C11 (GlobalData_t)(0.)
#define partial_koSRCa_del_C12 (GlobalData_t)(0.)
#define partial_koSRCa_del_C13 (GlobalData_t)(0.)
#define partial_koSRCa_del_C14 (GlobalData_t)(0.)
#define partial_koSRCa_del_C15 (GlobalData_t)(0.)
#define partial_koSRCa_del_C2 (GlobalData_t)(0.)
#define partial_koSRCa_del_C3 (GlobalData_t)(0.)
#define partial_koSRCa_del_C4 (GlobalData_t)(0.)
#define partial_koSRCa_del_C5 (GlobalData_t)(0.)
#define partial_koSRCa_del_C6 (GlobalData_t)(0.)
#define partial_koSRCa_del_C7 (GlobalData_t)(0.)
#define partial_koSRCa_del_C8 (GlobalData_t)(0.)
#define partial_koSRCa_del_C9 (GlobalData_t)(0.)
#define partial_koSRCa_del_O1 (GlobalData_t)(0.)
#define partial_koSRCa_del_RyRi (GlobalData_t)(0.)
#define partial_koSRCa_del_RyRo (GlobalData_t)(0.)
#define partial_koSRCa_del_RyRr (GlobalData_t)(0.)
#define partial_kom_del_C1 (GlobalData_t)(0.)
#define partial_kom_del_C10 (GlobalData_t)(0.)
#define partial_kom_del_C11 (GlobalData_t)(0.)
#define partial_kom_del_C12 (GlobalData_t)(0.)
#define partial_kom_del_C13 (GlobalData_t)(0.)
#define partial_kom_del_C14 (GlobalData_t)(0.)
#define partial_kom_del_C15 (GlobalData_t)(0.)
#define partial_kom_del_C2 (GlobalData_t)(0.)
#define partial_kom_del_C3 (GlobalData_t)(0.)
#define partial_kom_del_C4 (GlobalData_t)(0.)
#define partial_kom_del_C5 (GlobalData_t)(0.)
#define partial_kom_del_C6 (GlobalData_t)(0.)
#define partial_kom_del_C7 (GlobalData_t)(0.)
#define partial_kom_del_C8 (GlobalData_t)(0.)
#define partial_kom_del_C9 (GlobalData_t)(0.)
#define partial_kom_del_O1 (GlobalData_t)(0.)
#define partial_kom_del_RyRi (GlobalData_t)(0.)
#define partial_kom_del_RyRo (GlobalData_t)(0.)
#define partial_kom_del_RyRr (GlobalData_t)(0.)
#define partial_omega_del_C1 (GlobalData_t)(0.)
#define partial_omega_del_C10 (GlobalData_t)(0.)
#define partial_omega_del_C11 (GlobalData_t)(0.)
#define partial_omega_del_C12 (GlobalData_t)(0.)
#define partial_omega_del_C13 (GlobalData_t)(0.)
#define partial_omega_del_C14 (GlobalData_t)(0.)
#define partial_omega_del_C15 (GlobalData_t)(0.)
#define partial_omega_del_C2 (GlobalData_t)(0.)
#define partial_omega_del_C3 (GlobalData_t)(0.)
#define partial_omega_del_C4 (GlobalData_t)(0.)
#define partial_omega_del_C5 (GlobalData_t)(0.)
#define partial_omega_del_C6 (GlobalData_t)(0.)
#define partial_omega_del_C7 (GlobalData_t)(0.)
#define partial_omega_del_C8 (GlobalData_t)(0.)
#define partial_omega_del_C9 (GlobalData_t)(0.)
#define partial_omega_del_O1 (GlobalData_t)(0.)
#define partial_omega_del_RyRi (GlobalData_t)(0.)
#define partial_omega_del_RyRo (GlobalData_t)(0.)
#define partial_omega_del_RyRr (GlobalData_t)(0.)
#define partial_psi_del_C1 (GlobalData_t)(0.)
#define partial_psi_del_C10 (GlobalData_t)(0.)
#define partial_psi_del_C11 (GlobalData_t)(0.)
#define partial_psi_del_C12 (GlobalData_t)(0.)
#define partial_psi_del_C13 (GlobalData_t)(0.)
#define partial_psi_del_C14 (GlobalData_t)(0.)
#define partial_psi_del_C15 (GlobalData_t)(0.)
#define partial_psi_del_C2 (GlobalData_t)(0.)
#define partial_psi_del_C3 (GlobalData_t)(0.)
#define partial_psi_del_C4 (GlobalData_t)(0.)
#define partial_psi_del_C5 (GlobalData_t)(0.)
#define partial_psi_del_C6 (GlobalData_t)(0.)
#define partial_psi_del_C7 (GlobalData_t)(0.)
#define partial_psi_del_C8 (GlobalData_t)(0.)
#define partial_psi_del_C9 (GlobalData_t)(0.)
#define partial_psi_del_O1 (GlobalData_t)(0.)
#define partial_psi_del_RyRi (GlobalData_t)(0.)
#define partial_psi_del_RyRo (GlobalData_t)(0.)
#define partial_psi_del_RyRr (GlobalData_t)(0.)
#define partial_teta_del_C1 (GlobalData_t)(0.)
#define partial_teta_del_C10 (GlobalData_t)(0.)
#define partial_teta_del_C11 (GlobalData_t)(0.)
#define partial_teta_del_C12 (GlobalData_t)(0.)
#define partial_teta_del_C13 (GlobalData_t)(0.)
#define partial_teta_del_C14 (GlobalData_t)(0.)
#define partial_teta_del_C15 (GlobalData_t)(0.)
#define partial_teta_del_C2 (GlobalData_t)(0.)
#define partial_teta_del_C3 (GlobalData_t)(0.)
#define partial_teta_del_C4 (GlobalData_t)(0.)
#define partial_teta_del_C5 (GlobalData_t)(0.)
#define partial_teta_del_C6 (GlobalData_t)(0.)
#define partial_teta_del_C7 (GlobalData_t)(0.)
#define partial_teta_del_C8 (GlobalData_t)(0.)
#define partial_teta_del_C9 (GlobalData_t)(0.)
#define partial_teta_del_O1 (GlobalData_t)(0.)
#define partial_teta_del_RyRi (GlobalData_t)(0.)
#define partial_teta_del_RyRo (GlobalData_t)(0.)
#define partial_teta_del_RyRr (GlobalData_t)(0.)
#define pi (GlobalData_t)(3.14159)
#define rkur_init (GlobalData_t)(0.000345624)
#define skur_init (GlobalData_t)(0.947691)
#define tau_hl (GlobalData_t)(600.)
#define teta (GlobalData_t)(6.47e-3)
#define xkr_init (GlobalData_t)(0.00335222)
#define xks_init (GlobalData_t)(0.00693162)
#define xtof_init (GlobalData_t)(0.00124567)
#define ytof_init (GlobalData_t)(0.950559)
#define FoRT (GlobalData_t)(((Frdy/R)/Temp))
#define Fsl (GlobalData_t)((1.-(Fjunc)))
#define Fsl_CaL (GlobalData_t)((1.-(Fjunc_CaL)))
#define Qpow (GlobalData_t)(((Temp-(310.))/10.))
#define SAjunc (GlobalData_t)(((((20150.*pi)*2.)*junctionLength)*junctionRadius))
#define SAsl (GlobalData_t)((((pi*2.)*cellRadius)*cellLength))
#define Volcell (GlobalData_t)(((((pi*cellRadius)*cellRadius)*cellLength)*1e-15))
#define gkr (GlobalData_t)((0.035*(sqrt((Ko/5.4)))))
#define partial_diff_RyRo_del_RyRi (GlobalData_t)((-(-kim)))
#define partial_diff_RyRr_del_RyRi (GlobalData_t)((kim*-1.))
#define partial_diff_RyRr_del_RyRo (GlobalData_t)(((kim*-1.)-((-kom))))
#define sigma (GlobalData_t)((((exp((Nao/67.3)))-(1.))/7.))
#define VolSL (GlobalData_t)((0.02*Volcell))
#define VolSR (GlobalData_t)((0.035*Volcell))
#define Voljunc (GlobalData_t)(((0.0539*.01)*Volcell))
#define Volmyo (GlobalData_t)((0.65*Volcell))
#define ecl (GlobalData_t)(((1./FoRT)*(log((Cli/Clo)))))
#define Bmax_Csqn (GlobalData_t)(((140e-3*Volmyo)/VolSR))
#define Bmax_SLhighj (GlobalData_t)((((1.65e-3*Volmyo)/Voljunc)*0.1))
#define Bmax_SLhighsl (GlobalData_t)(((13.4e-3*Volmyo)/VolSL))
#define Bmax_SLlowj (GlobalData_t)((((4.6e-3*Volmyo)/Voljunc)*0.1))
#define Bmax_SLlowsl (GlobalData_t)(((37.4e-3*Volmyo)/VolSL))



void initialize_params_GrandiPanditVoigt( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  GrandiPanditVoigt_Params *p = (GrandiPanditVoigt_Params *)IF->params;

  // Compute the regional constants
  {
    p->AF = 0.;
    p->ISO = 0.;
    p->RA = 0.;
    p->factor_GCaL = 1.0;
    p->factor_GK1 = 1.0;
    p->factor_J_SRCarel = 1.0;
    p->factor_J_SRleak = 1.0;
    p->factor_Vmax_SRCaP = 1.0;
    p->markov_IKs = 0.;
    p->usefca = 0.;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_GrandiPanditVoigt;

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_GrandiPanditVoigt( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  GrandiPanditVoigt_Params *p = (GrandiPanditVoigt_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double GNa = (7.8*(1.-((0.1*p->AF))));
  double GNaL = (0.0025*p->AF);
  double Gkur = ((((1.0-((0.5*p->AF)))*(1.+(2.*p->ISO)))*0.045)*(1.+(0.2*p->RA)));
  double GtoFast = ((1.0-((0.7*p->AF)))*0.165);
  double IbarNCX = (((1.+(0.4*p->AF))*0.7)*4.5);
  double K_SRleak = ((p->factor_J_SRleak*(1.+(0.25*p->AF)))*5.348e-6);
  double KmNaip = (11.*(1.-((0.25*p->ISO))));
  double Kmf = ((2.5-((1.25*p->ISO)))*0.246e-3);
  double Vmax_SRCaP = (p->factor_Vmax_SRCaP*5.3114e-3);
  double gks_junc = ((p->markov_IKs==0.) ? (((1.+p->AF)+(2.*p->ISO))*0.0035) : 0.0065);
  double gks_sl = ((p->markov_IKs==0.) ? (((1.+p->AF)+(2.*p->ISO))*0.0035) : 0.0065);
  double hl_rush_larsen_B = (exp(((-dt)/tau_hl)));
  double hl_rush_larsen_C = (expm1(((-dt)/tau_hl)));
  double koCa = ((10.+(20.*p->AF))+((10.*p->ISO)*(1.-(p->AF))));
  double koff_tncl = ((1.+(0.5*p->ISO))*19.6e-3);
  double ks = (p->factor_J_SRCarel*25.);
  double pCa = ((((p->factor_GCaL*(1.+(0.5*p->ISO)))*(1.-((0.5*p->AF))))*0.50)*5.4e-4);
  double pK = ((((p->factor_GCaL*(1.+(0.5*p->ISO)))*(1.-((0.5*p->AF))))*0.50)*2.7e-7);
  double pNa = ((((p->factor_GCaL*(1.+(0.5*p->ISO)))*(1.-((0.5*p->AF))))*0.50)*1.5e-8);

}



void    initialize_sv_GrandiPanditVoigt( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  GrandiPanditVoigt_Params *p = (GrandiPanditVoigt_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(GrandiPanditVoigt_state) );
  GrandiPanditVoigt_state *sv_base = (GrandiPanditVoigt_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double GNa = (7.8*(1.-((0.1*p->AF))));
  double GNaL = (0.0025*p->AF);
  double Gkur = ((((1.0-((0.5*p->AF)))*(1.+(2.*p->ISO)))*0.045)*(1.+(0.2*p->RA)));
  double GtoFast = ((1.0-((0.7*p->AF)))*0.165);
  double IbarNCX = (((1.+(0.4*p->AF))*0.7)*4.5);
  double K_SRleak = ((p->factor_J_SRleak*(1.+(0.25*p->AF)))*5.348e-6);
  double KmNaip = (11.*(1.-((0.25*p->ISO))));
  double Kmf = ((2.5-((1.25*p->ISO)))*0.246e-3);
  double Vmax_SRCaP = (p->factor_Vmax_SRCaP*5.3114e-3);
  double gks_junc = ((p->markov_IKs==0.) ? (((1.+p->AF)+(2.*p->ISO))*0.0035) : 0.0065);
  double gks_sl = ((p->markov_IKs==0.) ? (((1.+p->AF)+(2.*p->ISO))*0.0035) : 0.0065);
  double hl_rush_larsen_B = (exp(((-dt)/tau_hl)));
  double hl_rush_larsen_C = (expm1(((-dt)/tau_hl)));
  double koCa = ((10.+(20.*p->AF))+((10.*p->ISO)*(1.-(p->AF))));
  double koff_tncl = ((1.+(0.5*p->ISO))*19.6e-3);
  double ks = (p->factor_J_SRCarel*25.);
  double pCa = ((((p->factor_GCaL*(1.+(0.5*p->ISO)))*(1.-((0.5*p->AF))))*0.50)*5.4e-4);
  double pK = ((((p->factor_GCaL*(1.+(0.5*p->ISO)))*(1.-((0.5*p->AF))))*0.50)*2.7e-7);
  double pNa = ((((p->factor_GCaL*(1.+(0.5*p->ISO)))*(1.-((0.5*p->AF))))*0.50)*1.5e-8);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vm_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    GrandiPanditVoigt_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Vm = Vm_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->C1 = C1_init;
    sv->C10 = C10_init;
    sv->C11 = C11_init;
    sv->C12 = C12_init;
    sv->C13 = C13_init;
    sv->C14 = C14_init;
    sv->C15 = C15_init;
    sv->C2 = C2_init;
    sv->C3 = C3_init;
    sv->C4 = C4_init;
    sv->C5 = C5_init;
    sv->C6 = C6_init;
    sv->C7 = C7_init;
    sv->C8 = C8_init;
    sv->C9 = C9_init;
    sv->CaM = CaM_init;
    sv->Ca_sr = Ca_sr_init;
    sv->Cai = Cai_init;
    sv->Caj = Caj_init;
    sv->Casl = Casl_init;
    sv->Csqnb = Csqnb_init;
    sv->Ki = Ki_init;
    sv->Myoc = Myoc_init;
    sv->Myom = Myom_init;
    sv->NaBj = NaBj_init;
    sv->NaBsl = NaBsl_init;
    sv->Nai = Nai_init;
    sv->Naj = Naj_init;
    sv->Nasl = Nasl_init;
    sv->O1 = O1_init;
    sv->RyRi = RyRi_init;
    sv->RyRo = RyRo_init;
    sv->RyRr = RyRr_init;
    sv->SLHj = SLHj_init;
    sv->SLHsl = SLHsl_init;
    sv->SLLj = SLLj_init;
    sv->SLLsl = SLLsl_init;
    sv->SRB = SRB_init;
    sv->TnCHc = TnCHc_init;
    sv->TnCHm = TnCHm_init;
    sv->TnCL = TnCL_init;
    Vm = Vm_init;
    sv->d = d_init;
    sv->f = f_init;
    sv->fcaBj = fcaBj_init;
    sv->fcaBsl = fcaBsl_init;
    sv->h = h_init;
    sv->hl = hl_init;
    sv->j = j_init;
    sv->m = m_init;
    sv->ml = ml_init;
    sv->rkur = rkur_init;
    sv->skur = skur_init;
    sv->xkr = xkr_init;
    sv->xks = xks_init;
    sv->xtof = xtof_init;
    sv->ytof = ytof_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Vm_ext[__i] = Vm;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_GrandiPanditVoigt(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  GrandiPanditVoigt_Params *p  = (GrandiPanditVoigt_Params *)IF->params;
  GrandiPanditVoigt_state *sv_base = (GrandiPanditVoigt_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t GNa = (7.8*(1.-((0.1*p->AF))));
  GlobalData_t GNaL = (0.0025*p->AF);
  GlobalData_t Gkur = ((((1.0-((0.5*p->AF)))*(1.+(2.*p->ISO)))*0.045)*(1.+(0.2*p->RA)));
  GlobalData_t GtoFast = ((1.0-((0.7*p->AF)))*0.165);
  GlobalData_t IbarNCX = (((1.+(0.4*p->AF))*0.7)*4.5);
  GlobalData_t K_SRleak = ((p->factor_J_SRleak*(1.+(0.25*p->AF)))*5.348e-6);
  GlobalData_t KmNaip = (11.*(1.-((0.25*p->ISO))));
  GlobalData_t Kmf = ((2.5-((1.25*p->ISO)))*0.246e-3);
  GlobalData_t Vmax_SRCaP = (p->factor_Vmax_SRCaP*5.3114e-3);
  GlobalData_t gks_junc = ((p->markov_IKs==0.) ? (((1.+p->AF)+(2.*p->ISO))*0.0035) : 0.0065);
  GlobalData_t gks_sl = ((p->markov_IKs==0.) ? (((1.+p->AF)+(2.*p->ISO))*0.0035) : 0.0065);
  GlobalData_t hl_rush_larsen_B = (exp(((-dt)/tau_hl)));
  GlobalData_t hl_rush_larsen_C = (expm1(((-dt)/tau_hl)));
  GlobalData_t koCa = ((10.+(20.*p->AF))+((10.*p->ISO)*(1.-(p->AF))));
  GlobalData_t koff_tncl = ((1.+(0.5*p->ISO))*19.6e-3);
  GlobalData_t ks = (p->factor_J_SRCarel*25.);
  GlobalData_t pCa = ((((p->factor_GCaL*(1.+(0.5*p->ISO)))*(1.-((0.5*p->AF))))*0.50)*5.4e-4);
  GlobalData_t pK = ((((p->factor_GCaL*(1.+(0.5*p->ISO)))*(1.-((0.5*p->AF))))*0.50)*2.7e-7);
  GlobalData_t pNa = ((((p->factor_GCaL*(1.+(0.5*p->ISO)))*(1.-((0.5*p->AF))))*0.50)*1.5e-8);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vm_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    GrandiPanditVoigt_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Vm = Vm_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t ICap_junc = ((((Fjunc*(pow(Q10SLCaP,Qpow)))*IbarSLCaP)*(pow(sv->Caj,1.6)))/((pow(KmPCa,1.6))+(pow(sv->Caj,1.6))));
    GlobalData_t ICap_sl = ((((Fsl*(pow(Q10SLCaP,Qpow)))*IbarSLCaP)*(pow(sv->Casl,1.6)))/((pow(KmPCa,1.6))+(pow(sv->Casl,1.6))));
    GlobalData_t IClCFTR = (GClCFTR*(Vm-(ecl)));
    GlobalData_t IClCa_junc = (((Fjunc*GClCa)/(1.+(KdClCa/sv->Caj)))*(Vm-(ecl)));
    GlobalData_t IClCa_sl = (((Fsl*GClCa)/(1.+(KdClCa/sv->Casl)))*(Vm-(ecl)));
    GlobalData_t IbCl = (GClB*(Vm-(ecl)));
    GlobalData_t Ka_junc = (1./(1.+((Kdact/sv->Caj)*(Kdact/sv->Caj))));
    GlobalData_t Ka_sl = (1./(1.+((Kdact/sv->Casl)*(Kdact/sv->Casl))));
    GlobalData_t O2 = (1.-((((((((((((((((sv->C1+sv->C2)+sv->C3)+sv->C4)+sv->C5)+sv->C6)+sv->C7)+sv->C8)+sv->C9)+sv->C10)+sv->C11)+sv->C12)+sv->C13)+sv->C14)+sv->C15)+sv->O1)));
    GlobalData_t eca_junc = (((1./FoRT)/2.)*(log((Cao/sv->Caj))));
    GlobalData_t eca_sl = (((1./FoRT)/2.)*(log((Cao/sv->Casl))));
    GlobalData_t ek = ((1./FoRT)*(log((Ko/sv->Ki))));
    GlobalData_t eks = ((1./FoRT)*(log(((Ko+(pNaK*Nao))/(sv->Ki+(pNaK*sv->Nai))))));
    GlobalData_t ena_junc = ((1./FoRT)*(log((Nao/sv->Naj))));
    GlobalData_t ena_sl = ((1./FoRT)*(log((Nao/sv->Nasl))));
    GlobalData_t fcaCaMSL = ((p->usefca==1.) ? (0.1/(1.+(0.01/sv->Casl))) : 0.);
    GlobalData_t fcaCaj = ((p->usefca==1.) ? (0.1/(1.+(0.01/sv->Caj))) : 0.);
    GlobalData_t fnak = (1./((1.+(0.1245*(exp(((-0.1*Vm)*FoRT)))))+((0.0365*sigma)*(exp(((-Vm)*FoRT))))));
    GlobalData_t ibarca_j = ((((pCa*4.)*((Vm*Frdy)*FoRT))*(((0.341*sv->Caj)*(exp(((2.*Vm)*FoRT))))-((0.341*Cao))))/((exp(((2.*Vm)*FoRT)))-(1.)));
    GlobalData_t ibarca_sl = ((((pCa*4.)*((Vm*Frdy)*FoRT))*(((0.341*sv->Casl)*(exp(((2.*Vm)*FoRT))))-((0.341*Cao))))/((exp(((2.*Vm)*FoRT)))-(1.)));
    GlobalData_t ibark = (((pK*((Vm*Frdy)*FoRT))*(((0.75*sv->Ki)*(exp((Vm*FoRT))))-((0.75*Ko))))/((exp((Vm*FoRT)))-(1.)));
    GlobalData_t ibarna_j = (((pNa*((Vm*Frdy)*FoRT))*(((0.75*sv->Naj)*(exp((Vm*FoRT))))-((0.75*Nao))))/((exp((Vm*FoRT)))-(1.)));
    GlobalData_t ibarna_sl = (((pNa*((Vm*Frdy)*FoRT))*(((0.75*sv->Nasl)*(exp((Vm*FoRT))))-((0.75*Nao))))/((exp((Vm*FoRT)))-(1.)));
    GlobalData_t kp_kp = (1./(1.+(exp((7.488-((Vm/5.98)))))));
    GlobalData_t rkr = (1./(1.+(exp(((Vm+74.)/24.)))));
    GlobalData_t s1_junc = (((((exp(((nu*Vm)*FoRT)))*sv->Naj)*sv->Naj)*sv->Naj)*Cao);
    GlobalData_t s1_sl = (((((exp(((nu*Vm)*FoRT)))*sv->Nasl)*sv->Nasl)*sv->Nasl)*Cao);
    GlobalData_t s2_junc = (((((exp((((nu-(1.))*Vm)*FoRT)))*Nao)*Nao)*Nao)*sv->Caj);
    GlobalData_t s2_sl = (((((exp((((nu-(1.))*Vm)*FoRT)))*Nao)*Nao)*Nao)*sv->Casl);
    GlobalData_t s3_junc = ((((((((KmCai*Nao)*Nao)*Nao)*(1.+(((sv->Naj/KmNai)*(sv->Naj/KmNai))*(sv->Naj/KmNai))))+((((KmNao*KmNao)*KmNao)*sv->Caj)*(1.+(sv->Caj/KmCai))))+(((KmCao*sv->Naj)*sv->Naj)*sv->Naj))+(((sv->Naj*sv->Naj)*sv->Naj)*Cao))+(((Nao*Nao)*Nao)*sv->Caj));
    GlobalData_t s3_sl = ((((((((KmCai*Nao)*Nao)*Nao)*(1.+(((sv->Nasl/KmNai)*(sv->Nasl/KmNai))*(sv->Nasl/KmNai))))+((((KmNao*KmNao)*KmNao)*sv->Casl)*(1.+(sv->Casl/KmCai))))+(((KmCao*sv->Nasl)*sv->Nasl)*sv->Nasl))+(((sv->Nasl*sv->Nasl)*sv->Nasl)*Cao))+(((Nao*Nao)*Nao)*sv->Casl));
    GlobalData_t ICaK = (((((ibark*sv->d)*sv->f)*((Fjunc_CaL*(fcaCaj+(1.-(sv->fcaBj))))+(Fsl_CaL*(fcaCaMSL+(1.-(sv->fcaBsl))))))*(pow(Q10CaL,Qpow)))*0.45);
    GlobalData_t ICaNa_junc = ((((((Fjunc_CaL*ibarna_j)*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*(pow(Q10CaL,Qpow)))*0.45);
    GlobalData_t ICaNa_sl = ((((((Fsl_CaL*ibarna_sl)*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*(pow(Q10CaL,Qpow)))*.45);
    GlobalData_t ICa_junc = ((((((Fjunc_CaL*ibarca_j)*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*(pow(Q10CaL,Qpow)))*0.45);
    GlobalData_t ICa_sl = ((((((Fsl_CaL*ibarca_sl)*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*(pow(Q10CaL,Qpow)))*0.45);
    GlobalData_t IClCa = (IClCa_junc+IClCa_sl);
    GlobalData_t IKp_junc = (((Fjunc*gkp)*kp_kp)*(Vm-(ek)));
    GlobalData_t IKp_sl = (((Fsl*gkp)*kp_kp)*(Vm-(ek)));
    GlobalData_t IKr = (((gkr*sv->xkr)*rkr)*(Vm-(ek)));
    GlobalData_t IKs_junc = ((p->markov_IKs==0.) ? ((((Fjunc*gks_junc)*sv->xks)*sv->xks)*(Vm-(eks))) : (((Fjunc*gks_junc)*(sv->O1+O2))*(Vm-(eks))));
    GlobalData_t IKs_sl = ((p->markov_IKs==0.) ? ((((Fsl*gks_sl)*sv->xks)*sv->xks)*(Vm-(eks))) : (((Fsl*gks_sl)*(sv->O1+O2))*(Vm-(eks))));
    GlobalData_t IKur = (((Gkur*sv->rkur)*sv->skur)*(Vm-(ek)));
    GlobalData_t INaCa_junc = ((((((Fjunc*IbarNCX)*(pow(Q10NCX,Qpow)))*Ka_junc)*(s1_junc-(s2_junc)))/s3_junc)/(1.+(ksat*(exp((((nu-(1.))*Vm)*FoRT))))));
    GlobalData_t INaCa_sl = ((((((Fsl*IbarNCX)*(pow(Q10NCX,Qpow)))*Ka_sl)*(s1_sl-(s2_sl)))/s3_sl)/(1.+(ksat*(exp((((nu-(1.))*Vm)*FoRT))))));
    GlobalData_t INaK_junc = (((((Fjunc*IbarNaK)*fnak)*Ko)/(1.+((((KmNaip/sv->Naj)*(KmNaip/sv->Naj))*(KmNaip/sv->Naj))*(KmNaip/sv->Naj))))/(Ko+KmKo));
    GlobalData_t INaK_sl = (((((Fsl*IbarNaK)*fnak)*Ko)/(1.+((((KmNaip/sv->Nasl)*(KmNaip/sv->Nasl))*(KmNaip/sv->Nasl))*(KmNaip/sv->Nasl))))/(Ko+KmKo));
    GlobalData_t INaL_junc = ((((((Fjunc*GNaL)*sv->ml)*sv->ml)*sv->ml)*sv->hl)*(Vm-(ena_junc)));
    GlobalData_t INaL_sl = ((((((Fsl*GNaL)*sv->ml)*sv->ml)*sv->ml)*sv->hl)*(Vm-(ena_sl)));
    GlobalData_t INa_junc = (((((((Fjunc*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(Vm-(ena_junc)));
    GlobalData_t INa_sl = (((((((Fsl*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(Vm-(ena_sl)));
    GlobalData_t IbCa_junc = ((Fjunc*GCaB)*(Vm-(eca_junc)));
    GlobalData_t IbCa_sl = ((Fsl*GCaB)*(Vm-(eca_sl)));
    GlobalData_t IbNa_junc = ((Fjunc*GNaB)*(Vm-(ena_junc)));
    GlobalData_t IbNa_sl = ((Fsl*GNaB)*(Vm-(ena_sl)));
    GlobalData_t Itof = (((GtoFast*sv->xtof)*sv->ytof)*(Vm-(ek)));
    GlobalData_t aki = (1.02/(1.+(exp((0.2385*((Vm-(ek))-(59.215)))))));
    GlobalData_t bki = (((0.49124*(exp((0.08032*((Vm+5.476)-(ek))))))+(exp((0.06175*((Vm-(ek))-(594.31))))))/(1.+(exp((-0.5143*((Vm-(ek))+4.753))))));
    GlobalData_t ICa_tot_junc = (((ICa_junc+IbCa_junc)+ICap_junc)-((2.*INaCa_junc)));
    GlobalData_t ICa_tot_sl = (((ICa_sl+IbCa_sl)+ICap_sl)-((2.*INaCa_sl)));
    GlobalData_t ICl_tot = ((IClCa+IbCl)+IClCFTR);
    GlobalData_t IKp = (IKp_junc+IKp_sl);
    GlobalData_t IKs = (IKs_junc+IKs_sl);
    GlobalData_t INaK = (INaK_junc+INaK_sl);
    GlobalData_t INa_tot_junc = (((((INa_junc+IbNa_junc)+(3.*INaCa_junc))+(3.*INaK_junc))+ICaNa_junc)+INaL_junc);
    GlobalData_t INa_tot_sl = (((((INa_sl+IbNa_sl)+(3.*INaCa_sl))+(3.*INaK_sl))+ICaNa_sl)+INaL_sl);
    GlobalData_t Ito = Itof;
    GlobalData_t kiss = (aki/(aki+bki));
    GlobalData_t ICa_tot = (ICa_tot_junc+ICa_tot_sl);
    GlobalData_t IKi = ((((((p->factor_GK1*(1.+p->AF))*0.15)*0.35)*(sqrt((Ko/5.4))))*kiss)*(Vm-(ek)));
    GlobalData_t INa_tot = (INa_tot_junc+INa_tot_sl);
    GlobalData_t IK_tot = (((((((Ito+IKr)+IKs)+IKi)-((2.*INaK)))+ICaK)+IKp)+IKur);
    Iion = (((INa_tot+ICl_tot)+ICa_tot)+IK_tot);
    
    
    //Complete Forward Euler Update
    GlobalData_t diff_Vm = (-(Iion-(Iapp)));
    GlobalData_t diff_fcaBj = (((1.7*sv->Caj)*(1.-(sv->fcaBj)))-((11.9e-3*sv->fcaBj)));
    GlobalData_t diff_fcaBsl = (((1.7*sv->Casl)*(1.-(sv->fcaBsl)))-((11.9e-3*sv->fcaBsl)));
    GlobalData_t Vm_new = Vm+diff_Vm*dt;
    GlobalData_t fcaBj_new = sv->fcaBj+diff_fcaBj*dt;
    GlobalData_t fcaBsl_new = sv->fcaBsl+diff_fcaBsl*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t a_h = ((Vm>=-40.) ? 0.0 : (0.135*(exp(((Vm+80.)/-6.8)))));
    GlobalData_t a_j = ((Vm<-40.) ? ((((-127140.*(exp((0.2444*Vm))))-((3.474e-5*(exp((-0.04391*Vm))))))*(Vm+37.78))/(1.+(exp((0.311*(Vm+79.23)))))) : 0.0);
    GlobalData_t a_m = ((Vm==-47.13) ? 3.2 : ((0.32*(Vm+47.13))/(1.-((exp((-0.1*(Vm+47.13))))))));
    GlobalData_t a_ml = ((0.32*(Vm+47.13))/(1.-((exp((-0.1*(Vm+47.13)))))));
    GlobalData_t b_h = ((Vm>=-40.) ? ((1./0.13)/(1.+(exp(((-(Vm+10.66))/11.1))))) : ((3.56*(exp((0.079*Vm))))+(3.1e5*(exp((0.35*Vm))))));
    GlobalData_t b_j = ((Vm>=-40.) ? ((0.3*(exp((-2.535e-7*Vm))))/(1.+(exp((-0.1*(Vm+32.)))))) : ((0.1212*(exp((-0.01052*Vm))))/(1.+(exp((-0.1378*(Vm+40.14)))))));
    GlobalData_t b_m = (0.08*(exp(((-Vm)/11.))));
    GlobalData_t b_ml = (0.08*(exp(((-Vm)/11.))));
    GlobalData_t d_inf = (1./(1.+(exp(((-((Vm+(3.*p->ISO))+9.))/6.)))));
    GlobalData_t f_inf = ((1./(1.+(exp((((Vm+(3.*p->ISO))+30.)/7.)))))+(0.2/(1.+(exp((((50.-(Vm))-((3.*p->ISO)))/20.))))));
    GlobalData_t hl_inf = (1./(1.+(exp(((Vm+91.)/6.1)))));
    GlobalData_t rkur_inf = (1./(1.+(exp(((Vm+6.)/-8.6)))));
    GlobalData_t skur_inf = (1./(1.+(exp(((Vm+7.5)/10.)))));
    GlobalData_t tau_f = (1./((0.0197*(exp((-(pow((0.0337*((Vm+(3.*p->ISO))+25.)),2.))))))+0.02));
    GlobalData_t tau_rkur = ((9./(1.+(exp(((Vm+5.)/12.0)))))+0.5);
    GlobalData_t tau_skur = ((590./(1.+(exp(((Vm+60.)/10.0)))))+3050.);
    GlobalData_t tau_xkr = ((((550./(1.+(exp(((-22.-(Vm))/9.)))))*6.)/(1.+(exp(((Vm-(-11.))/9.)))))+(230./(1.+(exp(((Vm-(-40.))/20.))))));
    GlobalData_t tau_xks = (990.1/(1.+(exp(((-((Vm+(40.*p->ISO))+2.436))/14.12)))));
    GlobalData_t tau_xtof = ((3.5*(exp((-(pow((Vm/30.0),2.))))))+1.5);
    GlobalData_t tau_ytof = ((25.635*(exp((-(pow(((Vm+52.45)/15.8827),2.))))))+24.14);
    GlobalData_t xkr_inf = (1./(1.+(exp(((-(Vm+10.))/5.)))));
    GlobalData_t xks_inf = (1./(1.+(exp(((-((Vm+(40.*p->ISO))+3.8))/14.25)))));
    GlobalData_t xtof_inf = (1./(1.+(exp(((-(Vm+1.0))/11.0)))));
    GlobalData_t ytof_inf = (1./(1.+(exp(((Vm+40.5)/11.5)))));
    GlobalData_t f_rush_larsen_B = (exp(((-dt)/tau_f)));
    GlobalData_t f_rush_larsen_C = (expm1(((-dt)/tau_f)));
    GlobalData_t h_rush_larsen_A = (((-a_h)/(a_h+b_h))*(expm1(((-dt)*(a_h+b_h)))));
    GlobalData_t h_rush_larsen_B = (exp(((-dt)*(a_h+b_h))));
    GlobalData_t hl_rush_larsen_A = ((-hl_inf)*hl_rush_larsen_C);
    GlobalData_t j_rush_larsen_A = (((-a_j)/(a_j+b_j))*(expm1(((-dt)*(a_j+b_j)))));
    GlobalData_t j_rush_larsen_B = (exp(((-dt)*(a_j+b_j))));
    GlobalData_t m_rush_larsen_A = (((-a_m)/(a_m+b_m))*(expm1(((-dt)*(a_m+b_m)))));
    GlobalData_t m_rush_larsen_B = (exp(((-dt)*(a_m+b_m))));
    GlobalData_t ml_rush_larsen_A = (((-a_ml)/(a_ml+b_ml))*(expm1(((-dt)*(a_ml+b_ml)))));
    GlobalData_t ml_rush_larsen_B = (exp(((-dt)*(a_ml+b_ml))));
    GlobalData_t rkur_rush_larsen_B = (exp(((-dt)/tau_rkur)));
    GlobalData_t rkur_rush_larsen_C = (expm1(((-dt)/tau_rkur)));
    GlobalData_t skur_rush_larsen_B = (exp(((-dt)/tau_skur)));
    GlobalData_t skur_rush_larsen_C = (expm1(((-dt)/tau_skur)));
    GlobalData_t tau_d = ((d_inf*(1.-((exp(((-((Vm+(3.*p->ISO))+9.))/6.))))))/(0.035*((Vm+(3.*p->ISO))+9.)));
    GlobalData_t xkr_rush_larsen_B = (exp(((-dt)/tau_xkr)));
    GlobalData_t xkr_rush_larsen_C = (expm1(((-dt)/tau_xkr)));
    GlobalData_t xks_rush_larsen_B = (exp(((-dt)/tau_xks)));
    GlobalData_t xks_rush_larsen_C = (expm1(((-dt)/tau_xks)));
    GlobalData_t xtof_rush_larsen_B = (exp(((-dt)/tau_xtof)));
    GlobalData_t xtof_rush_larsen_C = (expm1(((-dt)/tau_xtof)));
    GlobalData_t ytof_rush_larsen_B = (exp(((-dt)/tau_ytof)));
    GlobalData_t ytof_rush_larsen_C = (expm1(((-dt)/tau_ytof)));
    GlobalData_t d_rush_larsen_B = (exp(((-dt)/tau_d)));
    GlobalData_t d_rush_larsen_C = (expm1(((-dt)/tau_d)));
    GlobalData_t f_rush_larsen_A = ((-f_inf)*f_rush_larsen_C);
    GlobalData_t rkur_rush_larsen_A = ((-rkur_inf)*rkur_rush_larsen_C);
    GlobalData_t skur_rush_larsen_A = ((-skur_inf)*skur_rush_larsen_C);
    GlobalData_t xkr_rush_larsen_A = ((-xkr_inf)*xkr_rush_larsen_C);
    GlobalData_t xks_rush_larsen_A = ((-xks_inf)*xks_rush_larsen_C);
    GlobalData_t xtof_rush_larsen_A = ((-xtof_inf)*xtof_rush_larsen_C);
    GlobalData_t ytof_rush_larsen_A = ((-ytof_inf)*ytof_rush_larsen_C);
    GlobalData_t d_rush_larsen_A = ((-d_inf)*d_rush_larsen_C);
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f_new = f_rush_larsen_A+f_rush_larsen_B*sv->f;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t hl_new = hl_rush_larsen_A+hl_rush_larsen_B*sv->hl;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t ml_new = ml_rush_larsen_A+ml_rush_larsen_B*sv->ml;
    GlobalData_t rkur_new = rkur_rush_larsen_A+rkur_rush_larsen_B*sv->rkur;
    GlobalData_t skur_new = skur_rush_larsen_A+skur_rush_larsen_B*sv->skur;
    GlobalData_t xkr_new = xkr_rush_larsen_A+xkr_rush_larsen_B*sv->xkr;
    GlobalData_t xks_new = xks_rush_larsen_A+xks_rush_larsen_B*sv->xks;
    GlobalData_t xtof_new = xtof_rush_larsen_A+xtof_rush_larsen_B*sv->xtof;
    GlobalData_t ytof_new = ytof_rush_larsen_A+ytof_rush_larsen_B*sv->ytof;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    GlobalData_t CaImM = (sv->Cai/1000.);
    GlobalData_t J_SRCarel = ((ks*sv->RyRo)*(sv->Ca_sr-(sv->Caj)));
    GlobalData_t J_SRleak = (K_SRleak*(sv->Ca_sr-(sv->Caj)));
    GlobalData_t diff_Csqnb = (((kon_csqn*sv->Ca_sr)*(Bmax_Csqn-(sv->Csqnb)))-((koff_csqn*sv->Csqnb)));
    GlobalData_t diff_Myom = (((kon_myomg*Mgi)*((Bmax_myosin-(sv->Myoc))-(sv->Myom)))-((koff_myomg*sv->Myom)));
    GlobalData_t diff_NaBj = (((kon_na*sv->Naj)*(Bmax_Naj-(sv->NaBj)))-((koff_na*sv->NaBj)));
    GlobalData_t diff_NaBsl = (((kon_na*sv->Nasl)*(Bmax_Nasl-(sv->NaBsl)))-((koff_na*sv->NaBsl)));
    GlobalData_t diff_Nai = ((J_na_slmyo/Volmyo)*(sv->Nasl-(sv->Nai)));
    GlobalData_t diff_SLHj = (((kon_slh*sv->Caj)*(Bmax_SLhighj-(sv->SLHj)))-((koff_slh*sv->SLHj)));
    GlobalData_t diff_SLHsl = (((kon_slh*sv->Casl)*(Bmax_SLhighsl-(sv->SLHsl)))-((koff_slh*sv->SLHsl)));
    GlobalData_t diff_SLLj = (((kon_sll*sv->Caj)*(Bmax_SLlowj-(sv->SLLj)))-((koff_sll*sv->SLLj)));
    GlobalData_t diff_SLLsl = (((kon_sll*sv->Casl)*(Bmax_SLlowsl-(sv->SLLsl)))-((koff_sll*sv->SLLsl)));
    GlobalData_t diff_TnCHm = (((kon_tnchmg*Mgi)*((Bmax_TnChigh-(sv->TnCHc))-(sv->TnCHm)))-((koff_tnchmg*sv->TnCHm)));
    GlobalData_t J_CaB_junction = (diff_SLLj+diff_SLHj);
    GlobalData_t J_CaB_sl = (diff_SLLsl+diff_SLHsl);
    GlobalData_t J_serca = ((((pow(Q10SRCaP,Qpow))*Vmax_SRCaP)*((pow((CaImM/Kmf),hillSRCaP))-((pow((sv->Ca_sr/Kmr),hillSRCaP)))))/((1.+(pow((CaImM/Kmf),hillSRCaP)))+(pow((sv->Ca_sr/Kmr),hillSRCaP))));
    GlobalData_t diff_CaM = (((kon_cam*CaImM)*(Bmax_CaM-(sv->CaM)))-((koff_cam*sv->CaM)));
    GlobalData_t diff_Myoc = (((kon_myoca*CaImM)*((Bmax_myosin-(sv->Myoc))-(sv->Myom)))-((koff_myoca*sv->Myoc)));
    GlobalData_t diff_Naj = (((((-INa_tot_junc)*Cmem)/(Voljunc*Frdy))+((J_na_juncsl/Voljunc)*(sv->Nasl-(sv->Naj))))-(diff_NaBj));
    GlobalData_t diff_Nasl = ((((((-INa_tot_sl)*Cmem)/(VolSL*Frdy))+((J_na_juncsl/VolSL)*(sv->Naj-(sv->Nasl))))+((J_na_slmyo/VolSL)*(sv->Nai-(sv->Nasl))))-(diff_NaBsl));
    GlobalData_t diff_SRB = (((kon_sr*CaImM)*(Bmax_SR-(sv->SRB)))-((koff_sr*sv->SRB)));
    GlobalData_t diff_TnCHc = (((kon_tnchca*CaImM)*((Bmax_TnChigh-(sv->TnCHc))-(sv->TnCHm)))-((koff_tnchca*sv->TnCHc)));
    GlobalData_t diff_TnCL = (((kon_tncl*CaImM)*(Bmax_TnClow-(sv->TnCL)))-((koff_tncl*sv->TnCL)));
    GlobalData_t J_CaB_cytosol = ((((((diff_TnCL+diff_TnCHc)+diff_TnCHm)+diff_CaM)+diff_Myoc)+diff_Myom)+diff_SRB);
    GlobalData_t diff_Ca_sr = ((J_serca-((((J_SRleak*Volmyo)/VolSR)+J_SRCarel)))-(diff_Csqnb));
    GlobalData_t diff_Caj = (((((((-ICa_tot_junc)*Cmem)/((Voljunc*2.)*Frdy))+((J_ca_juncsl/Voljunc)*(sv->Casl-(sv->Caj))))-(J_CaB_junction))+((J_SRCarel*VolSR)/Voljunc))+((J_SRleak*Volmyo)/Voljunc));
    GlobalData_t diff_Casl = ((((((-ICa_tot_sl)*Cmem)/((VolSL*2.)*Frdy))+((J_ca_juncsl/VolSL)*(sv->Caj-(sv->Casl))))+((J_ca_slmyo/VolSL)*(CaImM-(sv->Casl))))-(J_CaB_sl));
    GlobalData_t diff_Cai = ((((((-J_serca)*VolSR)/Volmyo)-(J_CaB_cytosol))+((J_ca_slmyo/Volmyo)*(sv->Casl-(CaImM))))*1000.);
    GlobalData_t CaM_new;
    GlobalData_t rk4_k4_CaM;
    GlobalData_t rk4_k3_CaM;
    GlobalData_t rk4_k2_CaM;
    GlobalData_t rk4_k1_CaM = diff_CaM*dt;
    GlobalData_t Ca_sr_new;
    GlobalData_t rk4_k4_Ca_sr;
    GlobalData_t rk4_k3_Ca_sr;
    GlobalData_t rk4_k2_Ca_sr;
    GlobalData_t rk4_k1_Ca_sr = diff_Ca_sr*dt;
    GlobalData_t Cai_new;
    GlobalData_t rk4_k4_Cai;
    GlobalData_t rk4_k3_Cai;
    GlobalData_t rk4_k2_Cai;
    GlobalData_t rk4_k1_Cai = diff_Cai*dt;
    GlobalData_t Caj_new;
    GlobalData_t rk4_k4_Caj;
    GlobalData_t rk4_k3_Caj;
    GlobalData_t rk4_k2_Caj;
    GlobalData_t rk4_k1_Caj = diff_Caj*dt;
    GlobalData_t Casl_new;
    GlobalData_t rk4_k4_Casl;
    GlobalData_t rk4_k3_Casl;
    GlobalData_t rk4_k2_Casl;
    GlobalData_t rk4_k1_Casl = diff_Casl*dt;
    GlobalData_t Csqnb_new;
    GlobalData_t rk4_k4_Csqnb;
    GlobalData_t rk4_k3_Csqnb;
    GlobalData_t rk4_k2_Csqnb;
    GlobalData_t rk4_k1_Csqnb = diff_Csqnb*dt;
    GlobalData_t Ki_new;
    GlobalData_t rk4_k4_Ki;
    GlobalData_t rk4_k3_Ki;
    GlobalData_t rk4_k2_Ki;
    GlobalData_t rk4_k1_Ki = diff_Ki*dt;
    GlobalData_t Myoc_new;
    GlobalData_t rk4_k4_Myoc;
    GlobalData_t rk4_k3_Myoc;
    GlobalData_t rk4_k2_Myoc;
    GlobalData_t rk4_k1_Myoc = diff_Myoc*dt;
    GlobalData_t Myom_new;
    GlobalData_t rk4_k4_Myom;
    GlobalData_t rk4_k3_Myom;
    GlobalData_t rk4_k2_Myom;
    GlobalData_t rk4_k1_Myom = diff_Myom*dt;
    GlobalData_t NaBj_new;
    GlobalData_t rk4_k4_NaBj;
    GlobalData_t rk4_k3_NaBj;
    GlobalData_t rk4_k2_NaBj;
    GlobalData_t rk4_k1_NaBj = diff_NaBj*dt;
    GlobalData_t NaBsl_new;
    GlobalData_t rk4_k4_NaBsl;
    GlobalData_t rk4_k3_NaBsl;
    GlobalData_t rk4_k2_NaBsl;
    GlobalData_t rk4_k1_NaBsl = diff_NaBsl*dt;
    GlobalData_t Nai_new;
    GlobalData_t rk4_k4_Nai;
    GlobalData_t rk4_k3_Nai;
    GlobalData_t rk4_k2_Nai;
    GlobalData_t rk4_k1_Nai = diff_Nai*dt;
    GlobalData_t Naj_new;
    GlobalData_t rk4_k4_Naj;
    GlobalData_t rk4_k3_Naj;
    GlobalData_t rk4_k2_Naj;
    GlobalData_t rk4_k1_Naj = diff_Naj*dt;
    GlobalData_t Nasl_new;
    GlobalData_t rk4_k4_Nasl;
    GlobalData_t rk4_k3_Nasl;
    GlobalData_t rk4_k2_Nasl;
    GlobalData_t rk4_k1_Nasl = diff_Nasl*dt;
    GlobalData_t SLHj_new;
    GlobalData_t rk4_k4_SLHj;
    GlobalData_t rk4_k3_SLHj;
    GlobalData_t rk4_k2_SLHj;
    GlobalData_t rk4_k1_SLHj = diff_SLHj*dt;
    GlobalData_t SLHsl_new;
    GlobalData_t rk4_k4_SLHsl;
    GlobalData_t rk4_k3_SLHsl;
    GlobalData_t rk4_k2_SLHsl;
    GlobalData_t rk4_k1_SLHsl = diff_SLHsl*dt;
    GlobalData_t SLLj_new;
    GlobalData_t rk4_k4_SLLj;
    GlobalData_t rk4_k3_SLLj;
    GlobalData_t rk4_k2_SLLj;
    GlobalData_t rk4_k1_SLLj = diff_SLLj*dt;
    GlobalData_t SLLsl_new;
    GlobalData_t rk4_k4_SLLsl;
    GlobalData_t rk4_k3_SLLsl;
    GlobalData_t rk4_k2_SLLsl;
    GlobalData_t rk4_k1_SLLsl = diff_SLLsl*dt;
    GlobalData_t SRB_new;
    GlobalData_t rk4_k4_SRB;
    GlobalData_t rk4_k3_SRB;
    GlobalData_t rk4_k2_SRB;
    GlobalData_t rk4_k1_SRB = diff_SRB*dt;
    GlobalData_t TnCHc_new;
    GlobalData_t rk4_k4_TnCHc;
    GlobalData_t rk4_k3_TnCHc;
    GlobalData_t rk4_k2_TnCHc;
    GlobalData_t rk4_k1_TnCHc = diff_TnCHc*dt;
    GlobalData_t TnCHm_new;
    GlobalData_t rk4_k4_TnCHm;
    GlobalData_t rk4_k3_TnCHm;
    GlobalData_t rk4_k2_TnCHm;
    GlobalData_t rk4_k1_TnCHm = diff_TnCHm*dt;
    GlobalData_t TnCL_new;
    GlobalData_t rk4_k4_TnCL;
    GlobalData_t rk4_k3_TnCL;
    GlobalData_t rk4_k2_TnCL;
    GlobalData_t rk4_k1_TnCL = diff_TnCL*dt;
    {
      GlobalData_t t = t + dt/2;
      GlobalData_t sv_intermediate_CaM = sv->CaM+rk4_k1_CaM/2;
      GlobalData_t sv_intermediate_Ca_sr = sv->Ca_sr+rk4_k1_Ca_sr/2;
      GlobalData_t sv_intermediate_Cai = sv->Cai+rk4_k1_Cai/2;
      GlobalData_t sv_intermediate_Caj = sv->Caj+rk4_k1_Caj/2;
      GlobalData_t sv_intermediate_Casl = sv->Casl+rk4_k1_Casl/2;
      GlobalData_t sv_intermediate_Csqnb = sv->Csqnb+rk4_k1_Csqnb/2;
      GlobalData_t sv_intermediate_Ki = sv->Ki+rk4_k1_Ki/2;
      GlobalData_t sv_intermediate_Myoc = sv->Myoc+rk4_k1_Myoc/2;
      GlobalData_t sv_intermediate_Myom = sv->Myom+rk4_k1_Myom/2;
      GlobalData_t sv_intermediate_NaBj = sv->NaBj+rk4_k1_NaBj/2;
      GlobalData_t sv_intermediate_NaBsl = sv->NaBsl+rk4_k1_NaBsl/2;
      GlobalData_t sv_intermediate_Nai = sv->Nai+rk4_k1_Nai/2;
      GlobalData_t sv_intermediate_Naj = sv->Naj+rk4_k1_Naj/2;
      GlobalData_t sv_intermediate_Nasl = sv->Nasl+rk4_k1_Nasl/2;
      GlobalData_t sv_intermediate_SLHj = sv->SLHj+rk4_k1_SLHj/2;
      GlobalData_t sv_intermediate_SLHsl = sv->SLHsl+rk4_k1_SLHsl/2;
      GlobalData_t sv_intermediate_SLLj = sv->SLLj+rk4_k1_SLLj/2;
      GlobalData_t sv_intermediate_SLLsl = sv->SLLsl+rk4_k1_SLLsl/2;
      GlobalData_t sv_intermediate_SRB = sv->SRB+rk4_k1_SRB/2;
      GlobalData_t sv_intermediate_TnCHc = sv->TnCHc+rk4_k1_TnCHc/2;
      GlobalData_t sv_intermediate_TnCHm = sv->TnCHm+rk4_k1_TnCHm/2;
      GlobalData_t sv_intermediate_TnCL = sv->TnCL+rk4_k1_TnCL/2;
      GlobalData_t CaImM = (sv_intermediate_Cai/1000.);
      GlobalData_t ICap_junc = ((((Fjunc*(pow(Q10SLCaP,Qpow)))*IbarSLCaP)*(pow(sv_intermediate_Caj,1.6)))/((pow(KmPCa,1.6))+(pow(sv_intermediate_Caj,1.6))));
      GlobalData_t ICap_sl = ((((Fsl*(pow(Q10SLCaP,Qpow)))*IbarSLCaP)*(pow(sv_intermediate_Casl,1.6)))/((pow(KmPCa,1.6))+(pow(sv_intermediate_Casl,1.6))));
      GlobalData_t INaK_junc = (((((Fjunc*IbarNaK)*fnak)*Ko)/(1.+((((KmNaip/sv_intermediate_Naj)*(KmNaip/sv_intermediate_Naj))*(KmNaip/sv_intermediate_Naj))*(KmNaip/sv_intermediate_Naj))))/(Ko+KmKo));
      GlobalData_t INaK_sl = (((((Fsl*IbarNaK)*fnak)*Ko)/(1.+((((KmNaip/sv_intermediate_Nasl)*(KmNaip/sv_intermediate_Nasl))*(KmNaip/sv_intermediate_Nasl))*(KmNaip/sv_intermediate_Nasl))))/(Ko+KmKo));
      GlobalData_t J_SRCarel = ((ks*sv->RyRo)*(sv_intermediate_Ca_sr-(sv_intermediate_Caj)));
      GlobalData_t J_SRleak = (K_SRleak*(sv_intermediate_Ca_sr-(sv_intermediate_Caj)));
      GlobalData_t Ka_junc = (1./(1.+((Kdact/sv_intermediate_Caj)*(Kdact/sv_intermediate_Caj))));
      GlobalData_t Ka_sl = (1./(1.+((Kdact/sv_intermediate_Casl)*(Kdact/sv_intermediate_Casl))));
      GlobalData_t diff_Csqnb = (((kon_csqn*sv_intermediate_Ca_sr)*(Bmax_Csqn-(sv_intermediate_Csqnb)))-((koff_csqn*sv_intermediate_Csqnb)));
      GlobalData_t diff_Myom = (((kon_myomg*Mgi)*((Bmax_myosin-(sv_intermediate_Myoc))-(sv_intermediate_Myom)))-((koff_myomg*sv_intermediate_Myom)));
      GlobalData_t diff_NaBj = (((kon_na*sv_intermediate_Naj)*(Bmax_Naj-(sv_intermediate_NaBj)))-((koff_na*sv_intermediate_NaBj)));
      GlobalData_t diff_NaBsl = (((kon_na*sv_intermediate_Nasl)*(Bmax_Nasl-(sv_intermediate_NaBsl)))-((koff_na*sv_intermediate_NaBsl)));
      GlobalData_t diff_Nai = ((J_na_slmyo/Volmyo)*(sv_intermediate_Nasl-(sv_intermediate_Nai)));
      GlobalData_t diff_SLHj = (((kon_slh*sv_intermediate_Caj)*(Bmax_SLhighj-(sv_intermediate_SLHj)))-((koff_slh*sv_intermediate_SLHj)));
      GlobalData_t diff_SLHsl = (((kon_slh*sv_intermediate_Casl)*(Bmax_SLhighsl-(sv_intermediate_SLHsl)))-((koff_slh*sv_intermediate_SLHsl)));
      GlobalData_t diff_SLLj = (((kon_sll*sv_intermediate_Caj)*(Bmax_SLlowj-(sv_intermediate_SLLj)))-((koff_sll*sv_intermediate_SLLj)));
      GlobalData_t diff_SLLsl = (((kon_sll*sv_intermediate_Casl)*(Bmax_SLlowsl-(sv_intermediate_SLLsl)))-((koff_sll*sv_intermediate_SLLsl)));
      GlobalData_t diff_TnCHm = (((kon_tnchmg*Mgi)*((Bmax_TnChigh-(sv_intermediate_TnCHc))-(sv_intermediate_TnCHm)))-((koff_tnchmg*sv_intermediate_TnCHm)));
      GlobalData_t eca_junc = (((1./FoRT)/2.)*(log((Cao/sv_intermediate_Caj))));
      GlobalData_t eca_sl = (((1./FoRT)/2.)*(log((Cao/sv_intermediate_Casl))));
      GlobalData_t ena_junc = ((1./FoRT)*(log((Nao/sv_intermediate_Naj))));
      GlobalData_t ena_sl = ((1./FoRT)*(log((Nao/sv_intermediate_Nasl))));
      GlobalData_t fcaCaMSL = ((p->usefca==1.) ? (0.1/(1.+(0.01/sv_intermediate_Casl))) : 0.);
      GlobalData_t fcaCaj = ((p->usefca==1.) ? (0.1/(1.+(0.01/sv_intermediate_Caj))) : 0.);
      GlobalData_t ibarca_j = ((((pCa*4.)*((Vm*Frdy)*FoRT))*(((0.341*sv_intermediate_Caj)*(exp(((2.*Vm)*FoRT))))-((0.341*Cao))))/((exp(((2.*Vm)*FoRT)))-(1.)));
      GlobalData_t ibarca_sl = ((((pCa*4.)*((Vm*Frdy)*FoRT))*(((0.341*sv_intermediate_Casl)*(exp(((2.*Vm)*FoRT))))-((0.341*Cao))))/((exp(((2.*Vm)*FoRT)))-(1.)));
      GlobalData_t ibarna_j = (((pNa*((Vm*Frdy)*FoRT))*(((0.75*sv_intermediate_Naj)*(exp((Vm*FoRT))))-((0.75*Nao))))/((exp((Vm*FoRT)))-(1.)));
      GlobalData_t ibarna_sl = (((pNa*((Vm*Frdy)*FoRT))*(((0.75*sv_intermediate_Nasl)*(exp((Vm*FoRT))))-((0.75*Nao))))/((exp((Vm*FoRT)))-(1.)));
      GlobalData_t s1_junc = (((((exp(((nu*Vm)*FoRT)))*sv_intermediate_Naj)*sv_intermediate_Naj)*sv_intermediate_Naj)*Cao);
      GlobalData_t s1_sl = (((((exp(((nu*Vm)*FoRT)))*sv_intermediate_Nasl)*sv_intermediate_Nasl)*sv_intermediate_Nasl)*Cao);
      GlobalData_t s2_junc = (((((exp((((nu-(1.))*Vm)*FoRT)))*Nao)*Nao)*Nao)*sv_intermediate_Caj);
      GlobalData_t s2_sl = (((((exp((((nu-(1.))*Vm)*FoRT)))*Nao)*Nao)*Nao)*sv_intermediate_Casl);
      GlobalData_t s3_junc = ((((((((KmCai*Nao)*Nao)*Nao)*(1.+(((sv_intermediate_Naj/KmNai)*(sv_intermediate_Naj/KmNai))*(sv_intermediate_Naj/KmNai))))+((((KmNao*KmNao)*KmNao)*sv_intermediate_Caj)*(1.+(sv_intermediate_Caj/KmCai))))+(((KmCao*sv_intermediate_Naj)*sv_intermediate_Naj)*sv_intermediate_Naj))+(((sv_intermediate_Naj*sv_intermediate_Naj)*sv_intermediate_Naj)*Cao))+(((Nao*Nao)*Nao)*sv_intermediate_Caj));
      GlobalData_t s3_sl = ((((((((KmCai*Nao)*Nao)*Nao)*(1.+(((sv_intermediate_Nasl/KmNai)*(sv_intermediate_Nasl/KmNai))*(sv_intermediate_Nasl/KmNai))))+((((KmNao*KmNao)*KmNao)*sv_intermediate_Casl)*(1.+(sv_intermediate_Casl/KmCai))))+(((KmCao*sv_intermediate_Nasl)*sv_intermediate_Nasl)*sv_intermediate_Nasl))+(((sv_intermediate_Nasl*sv_intermediate_Nasl)*sv_intermediate_Nasl)*Cao))+(((Nao*Nao)*Nao)*sv_intermediate_Casl));
      GlobalData_t ICaNa_junc = ((((((Fjunc_CaL*ibarna_j)*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*(pow(Q10CaL,Qpow)))*0.45);
      GlobalData_t ICaNa_sl = ((((((Fsl_CaL*ibarna_sl)*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*(pow(Q10CaL,Qpow)))*.45);
      GlobalData_t ICa_junc = ((((((Fjunc_CaL*ibarca_j)*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*(pow(Q10CaL,Qpow)))*0.45);
      GlobalData_t ICa_sl = ((((((Fsl_CaL*ibarca_sl)*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*(pow(Q10CaL,Qpow)))*0.45);
      GlobalData_t INaCa_junc = ((((((Fjunc*IbarNCX)*(pow(Q10NCX,Qpow)))*Ka_junc)*(s1_junc-(s2_junc)))/s3_junc)/(1.+(ksat*(exp((((nu-(1.))*Vm)*FoRT))))));
      GlobalData_t INaCa_sl = ((((((Fsl*IbarNCX)*(pow(Q10NCX,Qpow)))*Ka_sl)*(s1_sl-(s2_sl)))/s3_sl)/(1.+(ksat*(exp((((nu-(1.))*Vm)*FoRT))))));
      GlobalData_t INaL_junc = ((((((Fjunc*GNaL)*sv->ml)*sv->ml)*sv->ml)*sv->hl)*(Vm-(ena_junc)));
      GlobalData_t INaL_sl = ((((((Fsl*GNaL)*sv->ml)*sv->ml)*sv->ml)*sv->hl)*(Vm-(ena_sl)));
      GlobalData_t INa_junc = (((((((Fjunc*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(Vm-(ena_junc)));
      GlobalData_t INa_sl = (((((((Fsl*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(Vm-(ena_sl)));
      GlobalData_t IbCa_junc = ((Fjunc*GCaB)*(Vm-(eca_junc)));
      GlobalData_t IbCa_sl = ((Fsl*GCaB)*(Vm-(eca_sl)));
      GlobalData_t IbNa_junc = ((Fjunc*GNaB)*(Vm-(ena_junc)));
      GlobalData_t IbNa_sl = ((Fsl*GNaB)*(Vm-(ena_sl)));
      GlobalData_t J_CaB_junction = (diff_SLLj+diff_SLHj);
      GlobalData_t J_CaB_sl = (diff_SLLsl+diff_SLHsl);
      GlobalData_t J_serca = ((((pow(Q10SRCaP,Qpow))*Vmax_SRCaP)*((pow((CaImM/Kmf),hillSRCaP))-((pow((sv_intermediate_Ca_sr/Kmr),hillSRCaP)))))/((1.+(pow((CaImM/Kmf),hillSRCaP)))+(pow((sv_intermediate_Ca_sr/Kmr),hillSRCaP))));
      GlobalData_t diff_CaM = (((kon_cam*CaImM)*(Bmax_CaM-(sv_intermediate_CaM)))-((koff_cam*sv_intermediate_CaM)));
      GlobalData_t diff_Myoc = (((kon_myoca*CaImM)*((Bmax_myosin-(sv_intermediate_Myoc))-(sv_intermediate_Myom)))-((koff_myoca*sv_intermediate_Myoc)));
      GlobalData_t diff_SRB = (((kon_sr*CaImM)*(Bmax_SR-(sv_intermediate_SRB)))-((koff_sr*sv_intermediate_SRB)));
      GlobalData_t diff_TnCHc = (((kon_tnchca*CaImM)*((Bmax_TnChigh-(sv_intermediate_TnCHc))-(sv_intermediate_TnCHm)))-((koff_tnchca*sv_intermediate_TnCHc)));
      GlobalData_t diff_TnCL = (((kon_tncl*CaImM)*(Bmax_TnClow-(sv_intermediate_TnCL)))-((koff_tncl*sv_intermediate_TnCL)));
      GlobalData_t ICa_tot_junc = (((ICa_junc+IbCa_junc)+ICap_junc)-((2.*INaCa_junc)));
      GlobalData_t ICa_tot_sl = (((ICa_sl+IbCa_sl)+ICap_sl)-((2.*INaCa_sl)));
      GlobalData_t INa_tot_junc = (((((INa_junc+IbNa_junc)+(3.*INaCa_junc))+(3.*INaK_junc))+ICaNa_junc)+INaL_junc);
      GlobalData_t INa_tot_sl = (((((INa_sl+IbNa_sl)+(3.*INaCa_sl))+(3.*INaK_sl))+ICaNa_sl)+INaL_sl);
      GlobalData_t J_CaB_cytosol = ((((((diff_TnCL+diff_TnCHc)+diff_TnCHm)+diff_CaM)+diff_Myoc)+diff_Myom)+diff_SRB);
      GlobalData_t diff_Ca_sr = ((J_serca-((((J_SRleak*Volmyo)/VolSR)+J_SRCarel)))-(diff_Csqnb));
      GlobalData_t diff_Cai = ((((((-J_serca)*VolSR)/Volmyo)-(J_CaB_cytosol))+((J_ca_slmyo/Volmyo)*(sv_intermediate_Casl-(CaImM))))*1000.);
      GlobalData_t diff_Caj = (((((((-ICa_tot_junc)*Cmem)/((Voljunc*2.)*Frdy))+((J_ca_juncsl/Voljunc)*(sv_intermediate_Casl-(sv_intermediate_Caj))))-(J_CaB_junction))+((J_SRCarel*VolSR)/Voljunc))+((J_SRleak*Volmyo)/Voljunc));
      GlobalData_t diff_Casl = ((((((-ICa_tot_sl)*Cmem)/((VolSL*2.)*Frdy))+((J_ca_juncsl/VolSL)*(sv_intermediate_Caj-(sv_intermediate_Casl))))+((J_ca_slmyo/VolSL)*(CaImM-(sv_intermediate_Casl))))-(J_CaB_sl));
      GlobalData_t diff_Naj = (((((-INa_tot_junc)*Cmem)/(Voljunc*Frdy))+((J_na_juncsl/Voljunc)*(sv_intermediate_Nasl-(sv_intermediate_Naj))))-(diff_NaBj));
      GlobalData_t diff_Nasl = ((((((-INa_tot_sl)*Cmem)/(VolSL*Frdy))+((J_na_juncsl/VolSL)*(sv_intermediate_Naj-(sv_intermediate_Nasl))))+((J_na_slmyo/VolSL)*(sv_intermediate_Nai-(sv_intermediate_Nasl))))-(diff_NaBsl));
      rk4_k2_CaM = dt*diff_CaM;
      rk4_k2_Ca_sr = dt*diff_Ca_sr;
      rk4_k2_Cai = dt*diff_Cai;
      rk4_k2_Caj = dt*diff_Caj;
      rk4_k2_Casl = dt*diff_Casl;
      rk4_k2_Csqnb = dt*diff_Csqnb;
      rk4_k2_Ki = dt*diff_Ki;
      rk4_k2_Myoc = dt*diff_Myoc;
      rk4_k2_Myom = dt*diff_Myom;
      rk4_k2_NaBj = dt*diff_NaBj;
      rk4_k2_NaBsl = dt*diff_NaBsl;
      rk4_k2_Nai = dt*diff_Nai;
      rk4_k2_Naj = dt*diff_Naj;
      rk4_k2_Nasl = dt*diff_Nasl;
      rk4_k2_SLHj = dt*diff_SLHj;
      rk4_k2_SLHsl = dt*diff_SLHsl;
      rk4_k2_SLLj = dt*diff_SLLj;
      rk4_k2_SLLsl = dt*diff_SLLsl;
      rk4_k2_SRB = dt*diff_SRB;
      rk4_k2_TnCHc = dt*diff_TnCHc;
      rk4_k2_TnCHm = dt*diff_TnCHm;
      rk4_k2_TnCL = dt*diff_TnCL;
    }
    {
      GlobalData_t t = t + dt/2;
      GlobalData_t sv_intermediate_CaM = sv->CaM+rk4_k2_CaM/2;
      GlobalData_t sv_intermediate_Ca_sr = sv->Ca_sr+rk4_k2_Ca_sr/2;
      GlobalData_t sv_intermediate_Cai = sv->Cai+rk4_k2_Cai/2;
      GlobalData_t sv_intermediate_Caj = sv->Caj+rk4_k2_Caj/2;
      GlobalData_t sv_intermediate_Casl = sv->Casl+rk4_k2_Casl/2;
      GlobalData_t sv_intermediate_Csqnb = sv->Csqnb+rk4_k2_Csqnb/2;
      GlobalData_t sv_intermediate_Ki = sv->Ki+rk4_k2_Ki/2;
      GlobalData_t sv_intermediate_Myoc = sv->Myoc+rk4_k2_Myoc/2;
      GlobalData_t sv_intermediate_Myom = sv->Myom+rk4_k2_Myom/2;
      GlobalData_t sv_intermediate_NaBj = sv->NaBj+rk4_k2_NaBj/2;
      GlobalData_t sv_intermediate_NaBsl = sv->NaBsl+rk4_k2_NaBsl/2;
      GlobalData_t sv_intermediate_Nai = sv->Nai+rk4_k2_Nai/2;
      GlobalData_t sv_intermediate_Naj = sv->Naj+rk4_k2_Naj/2;
      GlobalData_t sv_intermediate_Nasl = sv->Nasl+rk4_k2_Nasl/2;
      GlobalData_t sv_intermediate_SLHj = sv->SLHj+rk4_k2_SLHj/2;
      GlobalData_t sv_intermediate_SLHsl = sv->SLHsl+rk4_k2_SLHsl/2;
      GlobalData_t sv_intermediate_SLLj = sv->SLLj+rk4_k2_SLLj/2;
      GlobalData_t sv_intermediate_SLLsl = sv->SLLsl+rk4_k2_SLLsl/2;
      GlobalData_t sv_intermediate_SRB = sv->SRB+rk4_k2_SRB/2;
      GlobalData_t sv_intermediate_TnCHc = sv->TnCHc+rk4_k2_TnCHc/2;
      GlobalData_t sv_intermediate_TnCHm = sv->TnCHm+rk4_k2_TnCHm/2;
      GlobalData_t sv_intermediate_TnCL = sv->TnCL+rk4_k2_TnCL/2;
      GlobalData_t CaImM = (sv_intermediate_Cai/1000.);
      GlobalData_t ICap_junc = ((((Fjunc*(pow(Q10SLCaP,Qpow)))*IbarSLCaP)*(pow(sv_intermediate_Caj,1.6)))/((pow(KmPCa,1.6))+(pow(sv_intermediate_Caj,1.6))));
      GlobalData_t ICap_sl = ((((Fsl*(pow(Q10SLCaP,Qpow)))*IbarSLCaP)*(pow(sv_intermediate_Casl,1.6)))/((pow(KmPCa,1.6))+(pow(sv_intermediate_Casl,1.6))));
      GlobalData_t INaK_junc = (((((Fjunc*IbarNaK)*fnak)*Ko)/(1.+((((KmNaip/sv_intermediate_Naj)*(KmNaip/sv_intermediate_Naj))*(KmNaip/sv_intermediate_Naj))*(KmNaip/sv_intermediate_Naj))))/(Ko+KmKo));
      GlobalData_t INaK_sl = (((((Fsl*IbarNaK)*fnak)*Ko)/(1.+((((KmNaip/sv_intermediate_Nasl)*(KmNaip/sv_intermediate_Nasl))*(KmNaip/sv_intermediate_Nasl))*(KmNaip/sv_intermediate_Nasl))))/(Ko+KmKo));
      GlobalData_t J_SRCarel = ((ks*sv->RyRo)*(sv_intermediate_Ca_sr-(sv_intermediate_Caj)));
      GlobalData_t J_SRleak = (K_SRleak*(sv_intermediate_Ca_sr-(sv_intermediate_Caj)));
      GlobalData_t Ka_junc = (1./(1.+((Kdact/sv_intermediate_Caj)*(Kdact/sv_intermediate_Caj))));
      GlobalData_t Ka_sl = (1./(1.+((Kdact/sv_intermediate_Casl)*(Kdact/sv_intermediate_Casl))));
      GlobalData_t diff_Csqnb = (((kon_csqn*sv_intermediate_Ca_sr)*(Bmax_Csqn-(sv_intermediate_Csqnb)))-((koff_csqn*sv_intermediate_Csqnb)));
      GlobalData_t diff_Myom = (((kon_myomg*Mgi)*((Bmax_myosin-(sv_intermediate_Myoc))-(sv_intermediate_Myom)))-((koff_myomg*sv_intermediate_Myom)));
      GlobalData_t diff_NaBj = (((kon_na*sv_intermediate_Naj)*(Bmax_Naj-(sv_intermediate_NaBj)))-((koff_na*sv_intermediate_NaBj)));
      GlobalData_t diff_NaBsl = (((kon_na*sv_intermediate_Nasl)*(Bmax_Nasl-(sv_intermediate_NaBsl)))-((koff_na*sv_intermediate_NaBsl)));
      GlobalData_t diff_Nai = ((J_na_slmyo/Volmyo)*(sv_intermediate_Nasl-(sv_intermediate_Nai)));
      GlobalData_t diff_SLHj = (((kon_slh*sv_intermediate_Caj)*(Bmax_SLhighj-(sv_intermediate_SLHj)))-((koff_slh*sv_intermediate_SLHj)));
      GlobalData_t diff_SLHsl = (((kon_slh*sv_intermediate_Casl)*(Bmax_SLhighsl-(sv_intermediate_SLHsl)))-((koff_slh*sv_intermediate_SLHsl)));
      GlobalData_t diff_SLLj = (((kon_sll*sv_intermediate_Caj)*(Bmax_SLlowj-(sv_intermediate_SLLj)))-((koff_sll*sv_intermediate_SLLj)));
      GlobalData_t diff_SLLsl = (((kon_sll*sv_intermediate_Casl)*(Bmax_SLlowsl-(sv_intermediate_SLLsl)))-((koff_sll*sv_intermediate_SLLsl)));
      GlobalData_t diff_TnCHm = (((kon_tnchmg*Mgi)*((Bmax_TnChigh-(sv_intermediate_TnCHc))-(sv_intermediate_TnCHm)))-((koff_tnchmg*sv_intermediate_TnCHm)));
      GlobalData_t eca_junc = (((1./FoRT)/2.)*(log((Cao/sv_intermediate_Caj))));
      GlobalData_t eca_sl = (((1./FoRT)/2.)*(log((Cao/sv_intermediate_Casl))));
      GlobalData_t ena_junc = ((1./FoRT)*(log((Nao/sv_intermediate_Naj))));
      GlobalData_t ena_sl = ((1./FoRT)*(log((Nao/sv_intermediate_Nasl))));
      GlobalData_t fcaCaMSL = ((p->usefca==1.) ? (0.1/(1.+(0.01/sv_intermediate_Casl))) : 0.);
      GlobalData_t fcaCaj = ((p->usefca==1.) ? (0.1/(1.+(0.01/sv_intermediate_Caj))) : 0.);
      GlobalData_t ibarca_j = ((((pCa*4.)*((Vm*Frdy)*FoRT))*(((0.341*sv_intermediate_Caj)*(exp(((2.*Vm)*FoRT))))-((0.341*Cao))))/((exp(((2.*Vm)*FoRT)))-(1.)));
      GlobalData_t ibarca_sl = ((((pCa*4.)*((Vm*Frdy)*FoRT))*(((0.341*sv_intermediate_Casl)*(exp(((2.*Vm)*FoRT))))-((0.341*Cao))))/((exp(((2.*Vm)*FoRT)))-(1.)));
      GlobalData_t ibarna_j = (((pNa*((Vm*Frdy)*FoRT))*(((0.75*sv_intermediate_Naj)*(exp((Vm*FoRT))))-((0.75*Nao))))/((exp((Vm*FoRT)))-(1.)));
      GlobalData_t ibarna_sl = (((pNa*((Vm*Frdy)*FoRT))*(((0.75*sv_intermediate_Nasl)*(exp((Vm*FoRT))))-((0.75*Nao))))/((exp((Vm*FoRT)))-(1.)));
      GlobalData_t s1_junc = (((((exp(((nu*Vm)*FoRT)))*sv_intermediate_Naj)*sv_intermediate_Naj)*sv_intermediate_Naj)*Cao);
      GlobalData_t s1_sl = (((((exp(((nu*Vm)*FoRT)))*sv_intermediate_Nasl)*sv_intermediate_Nasl)*sv_intermediate_Nasl)*Cao);
      GlobalData_t s2_junc = (((((exp((((nu-(1.))*Vm)*FoRT)))*Nao)*Nao)*Nao)*sv_intermediate_Caj);
      GlobalData_t s2_sl = (((((exp((((nu-(1.))*Vm)*FoRT)))*Nao)*Nao)*Nao)*sv_intermediate_Casl);
      GlobalData_t s3_junc = ((((((((KmCai*Nao)*Nao)*Nao)*(1.+(((sv_intermediate_Naj/KmNai)*(sv_intermediate_Naj/KmNai))*(sv_intermediate_Naj/KmNai))))+((((KmNao*KmNao)*KmNao)*sv_intermediate_Caj)*(1.+(sv_intermediate_Caj/KmCai))))+(((KmCao*sv_intermediate_Naj)*sv_intermediate_Naj)*sv_intermediate_Naj))+(((sv_intermediate_Naj*sv_intermediate_Naj)*sv_intermediate_Naj)*Cao))+(((Nao*Nao)*Nao)*sv_intermediate_Caj));
      GlobalData_t s3_sl = ((((((((KmCai*Nao)*Nao)*Nao)*(1.+(((sv_intermediate_Nasl/KmNai)*(sv_intermediate_Nasl/KmNai))*(sv_intermediate_Nasl/KmNai))))+((((KmNao*KmNao)*KmNao)*sv_intermediate_Casl)*(1.+(sv_intermediate_Casl/KmCai))))+(((KmCao*sv_intermediate_Nasl)*sv_intermediate_Nasl)*sv_intermediate_Nasl))+(((sv_intermediate_Nasl*sv_intermediate_Nasl)*sv_intermediate_Nasl)*Cao))+(((Nao*Nao)*Nao)*sv_intermediate_Casl));
      GlobalData_t ICaNa_junc = ((((((Fjunc_CaL*ibarna_j)*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*(pow(Q10CaL,Qpow)))*0.45);
      GlobalData_t ICaNa_sl = ((((((Fsl_CaL*ibarna_sl)*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*(pow(Q10CaL,Qpow)))*.45);
      GlobalData_t ICa_junc = ((((((Fjunc_CaL*ibarca_j)*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*(pow(Q10CaL,Qpow)))*0.45);
      GlobalData_t ICa_sl = ((((((Fsl_CaL*ibarca_sl)*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*(pow(Q10CaL,Qpow)))*0.45);
      GlobalData_t INaCa_junc = ((((((Fjunc*IbarNCX)*(pow(Q10NCX,Qpow)))*Ka_junc)*(s1_junc-(s2_junc)))/s3_junc)/(1.+(ksat*(exp((((nu-(1.))*Vm)*FoRT))))));
      GlobalData_t INaCa_sl = ((((((Fsl*IbarNCX)*(pow(Q10NCX,Qpow)))*Ka_sl)*(s1_sl-(s2_sl)))/s3_sl)/(1.+(ksat*(exp((((nu-(1.))*Vm)*FoRT))))));
      GlobalData_t INaL_junc = ((((((Fjunc*GNaL)*sv->ml)*sv->ml)*sv->ml)*sv->hl)*(Vm-(ena_junc)));
      GlobalData_t INaL_sl = ((((((Fsl*GNaL)*sv->ml)*sv->ml)*sv->ml)*sv->hl)*(Vm-(ena_sl)));
      GlobalData_t INa_junc = (((((((Fjunc*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(Vm-(ena_junc)));
      GlobalData_t INa_sl = (((((((Fsl*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(Vm-(ena_sl)));
      GlobalData_t IbCa_junc = ((Fjunc*GCaB)*(Vm-(eca_junc)));
      GlobalData_t IbCa_sl = ((Fsl*GCaB)*(Vm-(eca_sl)));
      GlobalData_t IbNa_junc = ((Fjunc*GNaB)*(Vm-(ena_junc)));
      GlobalData_t IbNa_sl = ((Fsl*GNaB)*(Vm-(ena_sl)));
      GlobalData_t J_CaB_junction = (diff_SLLj+diff_SLHj);
      GlobalData_t J_CaB_sl = (diff_SLLsl+diff_SLHsl);
      GlobalData_t J_serca = ((((pow(Q10SRCaP,Qpow))*Vmax_SRCaP)*((pow((CaImM/Kmf),hillSRCaP))-((pow((sv_intermediate_Ca_sr/Kmr),hillSRCaP)))))/((1.+(pow((CaImM/Kmf),hillSRCaP)))+(pow((sv_intermediate_Ca_sr/Kmr),hillSRCaP))));
      GlobalData_t diff_CaM = (((kon_cam*CaImM)*(Bmax_CaM-(sv_intermediate_CaM)))-((koff_cam*sv_intermediate_CaM)));
      GlobalData_t diff_Myoc = (((kon_myoca*CaImM)*((Bmax_myosin-(sv_intermediate_Myoc))-(sv_intermediate_Myom)))-((koff_myoca*sv_intermediate_Myoc)));
      GlobalData_t diff_SRB = (((kon_sr*CaImM)*(Bmax_SR-(sv_intermediate_SRB)))-((koff_sr*sv_intermediate_SRB)));
      GlobalData_t diff_TnCHc = (((kon_tnchca*CaImM)*((Bmax_TnChigh-(sv_intermediate_TnCHc))-(sv_intermediate_TnCHm)))-((koff_tnchca*sv_intermediate_TnCHc)));
      GlobalData_t diff_TnCL = (((kon_tncl*CaImM)*(Bmax_TnClow-(sv_intermediate_TnCL)))-((koff_tncl*sv_intermediate_TnCL)));
      GlobalData_t ICa_tot_junc = (((ICa_junc+IbCa_junc)+ICap_junc)-((2.*INaCa_junc)));
      GlobalData_t ICa_tot_sl = (((ICa_sl+IbCa_sl)+ICap_sl)-((2.*INaCa_sl)));
      GlobalData_t INa_tot_junc = (((((INa_junc+IbNa_junc)+(3.*INaCa_junc))+(3.*INaK_junc))+ICaNa_junc)+INaL_junc);
      GlobalData_t INa_tot_sl = (((((INa_sl+IbNa_sl)+(3.*INaCa_sl))+(3.*INaK_sl))+ICaNa_sl)+INaL_sl);
      GlobalData_t J_CaB_cytosol = ((((((diff_TnCL+diff_TnCHc)+diff_TnCHm)+diff_CaM)+diff_Myoc)+diff_Myom)+diff_SRB);
      GlobalData_t diff_Ca_sr = ((J_serca-((((J_SRleak*Volmyo)/VolSR)+J_SRCarel)))-(diff_Csqnb));
      GlobalData_t diff_Cai = ((((((-J_serca)*VolSR)/Volmyo)-(J_CaB_cytosol))+((J_ca_slmyo/Volmyo)*(sv_intermediate_Casl-(CaImM))))*1000.);
      GlobalData_t diff_Caj = (((((((-ICa_tot_junc)*Cmem)/((Voljunc*2.)*Frdy))+((J_ca_juncsl/Voljunc)*(sv_intermediate_Casl-(sv_intermediate_Caj))))-(J_CaB_junction))+((J_SRCarel*VolSR)/Voljunc))+((J_SRleak*Volmyo)/Voljunc));
      GlobalData_t diff_Casl = ((((((-ICa_tot_sl)*Cmem)/((VolSL*2.)*Frdy))+((J_ca_juncsl/VolSL)*(sv_intermediate_Caj-(sv_intermediate_Casl))))+((J_ca_slmyo/VolSL)*(CaImM-(sv_intermediate_Casl))))-(J_CaB_sl));
      GlobalData_t diff_Naj = (((((-INa_tot_junc)*Cmem)/(Voljunc*Frdy))+((J_na_juncsl/Voljunc)*(sv_intermediate_Nasl-(sv_intermediate_Naj))))-(diff_NaBj));
      GlobalData_t diff_Nasl = ((((((-INa_tot_sl)*Cmem)/(VolSL*Frdy))+((J_na_juncsl/VolSL)*(sv_intermediate_Naj-(sv_intermediate_Nasl))))+((J_na_slmyo/VolSL)*(sv_intermediate_Nai-(sv_intermediate_Nasl))))-(diff_NaBsl));
      rk4_k3_CaM = dt*diff_CaM;
      rk4_k3_Ca_sr = dt*diff_Ca_sr;
      rk4_k3_Cai = dt*diff_Cai;
      rk4_k3_Caj = dt*diff_Caj;
      rk4_k3_Casl = dt*diff_Casl;
      rk4_k3_Csqnb = dt*diff_Csqnb;
      rk4_k3_Ki = dt*diff_Ki;
      rk4_k3_Myoc = dt*diff_Myoc;
      rk4_k3_Myom = dt*diff_Myom;
      rk4_k3_NaBj = dt*diff_NaBj;
      rk4_k3_NaBsl = dt*diff_NaBsl;
      rk4_k3_Nai = dt*diff_Nai;
      rk4_k3_Naj = dt*diff_Naj;
      rk4_k3_Nasl = dt*diff_Nasl;
      rk4_k3_SLHj = dt*diff_SLHj;
      rk4_k3_SLHsl = dt*diff_SLHsl;
      rk4_k3_SLLj = dt*diff_SLLj;
      rk4_k3_SLLsl = dt*diff_SLLsl;
      rk4_k3_SRB = dt*diff_SRB;
      rk4_k3_TnCHc = dt*diff_TnCHc;
      rk4_k3_TnCHm = dt*diff_TnCHm;
      rk4_k3_TnCL = dt*diff_TnCL;
    }
    {
      GlobalData_t t = t + dt;
      GlobalData_t sv_intermediate_CaM = sv->CaM+rk4_k3_CaM;
      GlobalData_t sv_intermediate_Ca_sr = sv->Ca_sr+rk4_k3_Ca_sr;
      GlobalData_t sv_intermediate_Cai = sv->Cai+rk4_k3_Cai;
      GlobalData_t sv_intermediate_Caj = sv->Caj+rk4_k3_Caj;
      GlobalData_t sv_intermediate_Casl = sv->Casl+rk4_k3_Casl;
      GlobalData_t sv_intermediate_Csqnb = sv->Csqnb+rk4_k3_Csqnb;
      GlobalData_t sv_intermediate_Ki = sv->Ki+rk4_k3_Ki;
      GlobalData_t sv_intermediate_Myoc = sv->Myoc+rk4_k3_Myoc;
      GlobalData_t sv_intermediate_Myom = sv->Myom+rk4_k3_Myom;
      GlobalData_t sv_intermediate_NaBj = sv->NaBj+rk4_k3_NaBj;
      GlobalData_t sv_intermediate_NaBsl = sv->NaBsl+rk4_k3_NaBsl;
      GlobalData_t sv_intermediate_Nai = sv->Nai+rk4_k3_Nai;
      GlobalData_t sv_intermediate_Naj = sv->Naj+rk4_k3_Naj;
      GlobalData_t sv_intermediate_Nasl = sv->Nasl+rk4_k3_Nasl;
      GlobalData_t sv_intermediate_SLHj = sv->SLHj+rk4_k3_SLHj;
      GlobalData_t sv_intermediate_SLHsl = sv->SLHsl+rk4_k3_SLHsl;
      GlobalData_t sv_intermediate_SLLj = sv->SLLj+rk4_k3_SLLj;
      GlobalData_t sv_intermediate_SLLsl = sv->SLLsl+rk4_k3_SLLsl;
      GlobalData_t sv_intermediate_SRB = sv->SRB+rk4_k3_SRB;
      GlobalData_t sv_intermediate_TnCHc = sv->TnCHc+rk4_k3_TnCHc;
      GlobalData_t sv_intermediate_TnCHm = sv->TnCHm+rk4_k3_TnCHm;
      GlobalData_t sv_intermediate_TnCL = sv->TnCL+rk4_k3_TnCL;
      GlobalData_t CaImM = (sv_intermediate_Cai/1000.);
      GlobalData_t ICap_junc = ((((Fjunc*(pow(Q10SLCaP,Qpow)))*IbarSLCaP)*(pow(sv_intermediate_Caj,1.6)))/((pow(KmPCa,1.6))+(pow(sv_intermediate_Caj,1.6))));
      GlobalData_t ICap_sl = ((((Fsl*(pow(Q10SLCaP,Qpow)))*IbarSLCaP)*(pow(sv_intermediate_Casl,1.6)))/((pow(KmPCa,1.6))+(pow(sv_intermediate_Casl,1.6))));
      GlobalData_t INaK_junc = (((((Fjunc*IbarNaK)*fnak)*Ko)/(1.+((((KmNaip/sv_intermediate_Naj)*(KmNaip/sv_intermediate_Naj))*(KmNaip/sv_intermediate_Naj))*(KmNaip/sv_intermediate_Naj))))/(Ko+KmKo));
      GlobalData_t INaK_sl = (((((Fsl*IbarNaK)*fnak)*Ko)/(1.+((((KmNaip/sv_intermediate_Nasl)*(KmNaip/sv_intermediate_Nasl))*(KmNaip/sv_intermediate_Nasl))*(KmNaip/sv_intermediate_Nasl))))/(Ko+KmKo));
      GlobalData_t J_SRCarel = ((ks*sv->RyRo)*(sv_intermediate_Ca_sr-(sv_intermediate_Caj)));
      GlobalData_t J_SRleak = (K_SRleak*(sv_intermediate_Ca_sr-(sv_intermediate_Caj)));
      GlobalData_t Ka_junc = (1./(1.+((Kdact/sv_intermediate_Caj)*(Kdact/sv_intermediate_Caj))));
      GlobalData_t Ka_sl = (1./(1.+((Kdact/sv_intermediate_Casl)*(Kdact/sv_intermediate_Casl))));
      GlobalData_t diff_Csqnb = (((kon_csqn*sv_intermediate_Ca_sr)*(Bmax_Csqn-(sv_intermediate_Csqnb)))-((koff_csqn*sv_intermediate_Csqnb)));
      GlobalData_t diff_Myom = (((kon_myomg*Mgi)*((Bmax_myosin-(sv_intermediate_Myoc))-(sv_intermediate_Myom)))-((koff_myomg*sv_intermediate_Myom)));
      GlobalData_t diff_NaBj = (((kon_na*sv_intermediate_Naj)*(Bmax_Naj-(sv_intermediate_NaBj)))-((koff_na*sv_intermediate_NaBj)));
      GlobalData_t diff_NaBsl = (((kon_na*sv_intermediate_Nasl)*(Bmax_Nasl-(sv_intermediate_NaBsl)))-((koff_na*sv_intermediate_NaBsl)));
      GlobalData_t diff_Nai = ((J_na_slmyo/Volmyo)*(sv_intermediate_Nasl-(sv_intermediate_Nai)));
      GlobalData_t diff_SLHj = (((kon_slh*sv_intermediate_Caj)*(Bmax_SLhighj-(sv_intermediate_SLHj)))-((koff_slh*sv_intermediate_SLHj)));
      GlobalData_t diff_SLHsl = (((kon_slh*sv_intermediate_Casl)*(Bmax_SLhighsl-(sv_intermediate_SLHsl)))-((koff_slh*sv_intermediate_SLHsl)));
      GlobalData_t diff_SLLj = (((kon_sll*sv_intermediate_Caj)*(Bmax_SLlowj-(sv_intermediate_SLLj)))-((koff_sll*sv_intermediate_SLLj)));
      GlobalData_t diff_SLLsl = (((kon_sll*sv_intermediate_Casl)*(Bmax_SLlowsl-(sv_intermediate_SLLsl)))-((koff_sll*sv_intermediate_SLLsl)));
      GlobalData_t diff_TnCHm = (((kon_tnchmg*Mgi)*((Bmax_TnChigh-(sv_intermediate_TnCHc))-(sv_intermediate_TnCHm)))-((koff_tnchmg*sv_intermediate_TnCHm)));
      GlobalData_t eca_junc = (((1./FoRT)/2.)*(log((Cao/sv_intermediate_Caj))));
      GlobalData_t eca_sl = (((1./FoRT)/2.)*(log((Cao/sv_intermediate_Casl))));
      GlobalData_t ena_junc = ((1./FoRT)*(log((Nao/sv_intermediate_Naj))));
      GlobalData_t ena_sl = ((1./FoRT)*(log((Nao/sv_intermediate_Nasl))));
      GlobalData_t fcaCaMSL = ((p->usefca==1.) ? (0.1/(1.+(0.01/sv_intermediate_Casl))) : 0.);
      GlobalData_t fcaCaj = ((p->usefca==1.) ? (0.1/(1.+(0.01/sv_intermediate_Caj))) : 0.);
      GlobalData_t ibarca_j = ((((pCa*4.)*((Vm*Frdy)*FoRT))*(((0.341*sv_intermediate_Caj)*(exp(((2.*Vm)*FoRT))))-((0.341*Cao))))/((exp(((2.*Vm)*FoRT)))-(1.)));
      GlobalData_t ibarca_sl = ((((pCa*4.)*((Vm*Frdy)*FoRT))*(((0.341*sv_intermediate_Casl)*(exp(((2.*Vm)*FoRT))))-((0.341*Cao))))/((exp(((2.*Vm)*FoRT)))-(1.)));
      GlobalData_t ibarna_j = (((pNa*((Vm*Frdy)*FoRT))*(((0.75*sv_intermediate_Naj)*(exp((Vm*FoRT))))-((0.75*Nao))))/((exp((Vm*FoRT)))-(1.)));
      GlobalData_t ibarna_sl = (((pNa*((Vm*Frdy)*FoRT))*(((0.75*sv_intermediate_Nasl)*(exp((Vm*FoRT))))-((0.75*Nao))))/((exp((Vm*FoRT)))-(1.)));
      GlobalData_t s1_junc = (((((exp(((nu*Vm)*FoRT)))*sv_intermediate_Naj)*sv_intermediate_Naj)*sv_intermediate_Naj)*Cao);
      GlobalData_t s1_sl = (((((exp(((nu*Vm)*FoRT)))*sv_intermediate_Nasl)*sv_intermediate_Nasl)*sv_intermediate_Nasl)*Cao);
      GlobalData_t s2_junc = (((((exp((((nu-(1.))*Vm)*FoRT)))*Nao)*Nao)*Nao)*sv_intermediate_Caj);
      GlobalData_t s2_sl = (((((exp((((nu-(1.))*Vm)*FoRT)))*Nao)*Nao)*Nao)*sv_intermediate_Casl);
      GlobalData_t s3_junc = ((((((((KmCai*Nao)*Nao)*Nao)*(1.+(((sv_intermediate_Naj/KmNai)*(sv_intermediate_Naj/KmNai))*(sv_intermediate_Naj/KmNai))))+((((KmNao*KmNao)*KmNao)*sv_intermediate_Caj)*(1.+(sv_intermediate_Caj/KmCai))))+(((KmCao*sv_intermediate_Naj)*sv_intermediate_Naj)*sv_intermediate_Naj))+(((sv_intermediate_Naj*sv_intermediate_Naj)*sv_intermediate_Naj)*Cao))+(((Nao*Nao)*Nao)*sv_intermediate_Caj));
      GlobalData_t s3_sl = ((((((((KmCai*Nao)*Nao)*Nao)*(1.+(((sv_intermediate_Nasl/KmNai)*(sv_intermediate_Nasl/KmNai))*(sv_intermediate_Nasl/KmNai))))+((((KmNao*KmNao)*KmNao)*sv_intermediate_Casl)*(1.+(sv_intermediate_Casl/KmCai))))+(((KmCao*sv_intermediate_Nasl)*sv_intermediate_Nasl)*sv_intermediate_Nasl))+(((sv_intermediate_Nasl*sv_intermediate_Nasl)*sv_intermediate_Nasl)*Cao))+(((Nao*Nao)*Nao)*sv_intermediate_Casl));
      GlobalData_t ICaNa_junc = ((((((Fjunc_CaL*ibarna_j)*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*(pow(Q10CaL,Qpow)))*0.45);
      GlobalData_t ICaNa_sl = ((((((Fsl_CaL*ibarna_sl)*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*(pow(Q10CaL,Qpow)))*.45);
      GlobalData_t ICa_junc = ((((((Fjunc_CaL*ibarca_j)*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*(pow(Q10CaL,Qpow)))*0.45);
      GlobalData_t ICa_sl = ((((((Fsl_CaL*ibarca_sl)*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*(pow(Q10CaL,Qpow)))*0.45);
      GlobalData_t INaCa_junc = ((((((Fjunc*IbarNCX)*(pow(Q10NCX,Qpow)))*Ka_junc)*(s1_junc-(s2_junc)))/s3_junc)/(1.+(ksat*(exp((((nu-(1.))*Vm)*FoRT))))));
      GlobalData_t INaCa_sl = ((((((Fsl*IbarNCX)*(pow(Q10NCX,Qpow)))*Ka_sl)*(s1_sl-(s2_sl)))/s3_sl)/(1.+(ksat*(exp((((nu-(1.))*Vm)*FoRT))))));
      GlobalData_t INaL_junc = ((((((Fjunc*GNaL)*sv->ml)*sv->ml)*sv->ml)*sv->hl)*(Vm-(ena_junc)));
      GlobalData_t INaL_sl = ((((((Fsl*GNaL)*sv->ml)*sv->ml)*sv->ml)*sv->hl)*(Vm-(ena_sl)));
      GlobalData_t INa_junc = (((((((Fjunc*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(Vm-(ena_junc)));
      GlobalData_t INa_sl = (((((((Fsl*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(Vm-(ena_sl)));
      GlobalData_t IbCa_junc = ((Fjunc*GCaB)*(Vm-(eca_junc)));
      GlobalData_t IbCa_sl = ((Fsl*GCaB)*(Vm-(eca_sl)));
      GlobalData_t IbNa_junc = ((Fjunc*GNaB)*(Vm-(ena_junc)));
      GlobalData_t IbNa_sl = ((Fsl*GNaB)*(Vm-(ena_sl)));
      GlobalData_t J_CaB_junction = (diff_SLLj+diff_SLHj);
      GlobalData_t J_CaB_sl = (diff_SLLsl+diff_SLHsl);
      GlobalData_t J_serca = ((((pow(Q10SRCaP,Qpow))*Vmax_SRCaP)*((pow((CaImM/Kmf),hillSRCaP))-((pow((sv_intermediate_Ca_sr/Kmr),hillSRCaP)))))/((1.+(pow((CaImM/Kmf),hillSRCaP)))+(pow((sv_intermediate_Ca_sr/Kmr),hillSRCaP))));
      GlobalData_t diff_CaM = (((kon_cam*CaImM)*(Bmax_CaM-(sv_intermediate_CaM)))-((koff_cam*sv_intermediate_CaM)));
      GlobalData_t diff_Myoc = (((kon_myoca*CaImM)*((Bmax_myosin-(sv_intermediate_Myoc))-(sv_intermediate_Myom)))-((koff_myoca*sv_intermediate_Myoc)));
      GlobalData_t diff_SRB = (((kon_sr*CaImM)*(Bmax_SR-(sv_intermediate_SRB)))-((koff_sr*sv_intermediate_SRB)));
      GlobalData_t diff_TnCHc = (((kon_tnchca*CaImM)*((Bmax_TnChigh-(sv_intermediate_TnCHc))-(sv_intermediate_TnCHm)))-((koff_tnchca*sv_intermediate_TnCHc)));
      GlobalData_t diff_TnCL = (((kon_tncl*CaImM)*(Bmax_TnClow-(sv_intermediate_TnCL)))-((koff_tncl*sv_intermediate_TnCL)));
      GlobalData_t ICa_tot_junc = (((ICa_junc+IbCa_junc)+ICap_junc)-((2.*INaCa_junc)));
      GlobalData_t ICa_tot_sl = (((ICa_sl+IbCa_sl)+ICap_sl)-((2.*INaCa_sl)));
      GlobalData_t INa_tot_junc = (((((INa_junc+IbNa_junc)+(3.*INaCa_junc))+(3.*INaK_junc))+ICaNa_junc)+INaL_junc);
      GlobalData_t INa_tot_sl = (((((INa_sl+IbNa_sl)+(3.*INaCa_sl))+(3.*INaK_sl))+ICaNa_sl)+INaL_sl);
      GlobalData_t J_CaB_cytosol = ((((((diff_TnCL+diff_TnCHc)+diff_TnCHm)+diff_CaM)+diff_Myoc)+diff_Myom)+diff_SRB);
      GlobalData_t diff_Ca_sr = ((J_serca-((((J_SRleak*Volmyo)/VolSR)+J_SRCarel)))-(diff_Csqnb));
      GlobalData_t diff_Cai = ((((((-J_serca)*VolSR)/Volmyo)-(J_CaB_cytosol))+((J_ca_slmyo/Volmyo)*(sv_intermediate_Casl-(CaImM))))*1000.);
      GlobalData_t diff_Caj = (((((((-ICa_tot_junc)*Cmem)/((Voljunc*2.)*Frdy))+((J_ca_juncsl/Voljunc)*(sv_intermediate_Casl-(sv_intermediate_Caj))))-(J_CaB_junction))+((J_SRCarel*VolSR)/Voljunc))+((J_SRleak*Volmyo)/Voljunc));
      GlobalData_t diff_Casl = ((((((-ICa_tot_sl)*Cmem)/((VolSL*2.)*Frdy))+((J_ca_juncsl/VolSL)*(sv_intermediate_Caj-(sv_intermediate_Casl))))+((J_ca_slmyo/VolSL)*(CaImM-(sv_intermediate_Casl))))-(J_CaB_sl));
      GlobalData_t diff_Naj = (((((-INa_tot_junc)*Cmem)/(Voljunc*Frdy))+((J_na_juncsl/Voljunc)*(sv_intermediate_Nasl-(sv_intermediate_Naj))))-(diff_NaBj));
      GlobalData_t diff_Nasl = ((((((-INa_tot_sl)*Cmem)/(VolSL*Frdy))+((J_na_juncsl/VolSL)*(sv_intermediate_Naj-(sv_intermediate_Nasl))))+((J_na_slmyo/VolSL)*(sv_intermediate_Nai-(sv_intermediate_Nasl))))-(diff_NaBsl));
      rk4_k4_CaM = dt*diff_CaM;
      rk4_k4_Ca_sr = dt*diff_Ca_sr;
      rk4_k4_Cai = dt*diff_Cai;
      rk4_k4_Caj = dt*diff_Caj;
      rk4_k4_Casl = dt*diff_Casl;
      rk4_k4_Csqnb = dt*diff_Csqnb;
      rk4_k4_Ki = dt*diff_Ki;
      rk4_k4_Myoc = dt*diff_Myoc;
      rk4_k4_Myom = dt*diff_Myom;
      rk4_k4_NaBj = dt*diff_NaBj;
      rk4_k4_NaBsl = dt*diff_NaBsl;
      rk4_k4_Nai = dt*diff_Nai;
      rk4_k4_Naj = dt*diff_Naj;
      rk4_k4_Nasl = dt*diff_Nasl;
      rk4_k4_SLHj = dt*diff_SLHj;
      rk4_k4_SLHsl = dt*diff_SLHsl;
      rk4_k4_SLLj = dt*diff_SLLj;
      rk4_k4_SLLsl = dt*diff_SLLsl;
      rk4_k4_SRB = dt*diff_SRB;
      rk4_k4_TnCHc = dt*diff_TnCHc;
      rk4_k4_TnCHm = dt*diff_TnCHm;
      rk4_k4_TnCL = dt*diff_TnCL;
    }
    CaM_new = sv->CaM+(rk4_k1_CaM + 2*rk4_k2_CaM + 2*rk4_k3_CaM + rk4_k4_CaM)/6;
    Ca_sr_new = sv->Ca_sr+(rk4_k1_Ca_sr + 2*rk4_k2_Ca_sr + 2*rk4_k3_Ca_sr + rk4_k4_Ca_sr)/6;
    Cai_new = sv->Cai+(rk4_k1_Cai + 2*rk4_k2_Cai + 2*rk4_k3_Cai + rk4_k4_Cai)/6;
    Caj_new = sv->Caj+(rk4_k1_Caj + 2*rk4_k2_Caj + 2*rk4_k3_Caj + rk4_k4_Caj)/6;
    Casl_new = sv->Casl+(rk4_k1_Casl + 2*rk4_k2_Casl + 2*rk4_k3_Casl + rk4_k4_Casl)/6;
    Csqnb_new = sv->Csqnb+(rk4_k1_Csqnb + 2*rk4_k2_Csqnb + 2*rk4_k3_Csqnb + rk4_k4_Csqnb)/6;
    Ki_new = sv->Ki+(rk4_k1_Ki + 2*rk4_k2_Ki + 2*rk4_k3_Ki + rk4_k4_Ki)/6;
    Myoc_new = sv->Myoc+(rk4_k1_Myoc + 2*rk4_k2_Myoc + 2*rk4_k3_Myoc + rk4_k4_Myoc)/6;
    Myom_new = sv->Myom+(rk4_k1_Myom + 2*rk4_k2_Myom + 2*rk4_k3_Myom + rk4_k4_Myom)/6;
    NaBj_new = sv->NaBj+(rk4_k1_NaBj + 2*rk4_k2_NaBj + 2*rk4_k3_NaBj + rk4_k4_NaBj)/6;
    NaBsl_new = sv->NaBsl+(rk4_k1_NaBsl + 2*rk4_k2_NaBsl + 2*rk4_k3_NaBsl + rk4_k4_NaBsl)/6;
    Nai_new = sv->Nai+(rk4_k1_Nai + 2*rk4_k2_Nai + 2*rk4_k3_Nai + rk4_k4_Nai)/6;
    Naj_new = sv->Naj+(rk4_k1_Naj + 2*rk4_k2_Naj + 2*rk4_k3_Naj + rk4_k4_Naj)/6;
    Nasl_new = sv->Nasl+(rk4_k1_Nasl + 2*rk4_k2_Nasl + 2*rk4_k3_Nasl + rk4_k4_Nasl)/6;
    SLHj_new = sv->SLHj+(rk4_k1_SLHj + 2*rk4_k2_SLHj + 2*rk4_k3_SLHj + rk4_k4_SLHj)/6;
    SLHsl_new = sv->SLHsl+(rk4_k1_SLHsl + 2*rk4_k2_SLHsl + 2*rk4_k3_SLHsl + rk4_k4_SLHsl)/6;
    SLLj_new = sv->SLLj+(rk4_k1_SLLj + 2*rk4_k2_SLLj + 2*rk4_k3_SLLj + rk4_k4_SLLj)/6;
    SLLsl_new = sv->SLLsl+(rk4_k1_SLLsl + 2*rk4_k2_SLLsl + 2*rk4_k3_SLLsl + rk4_k4_SLLsl)/6;
    SRB_new = sv->SRB+(rk4_k1_SRB + 2*rk4_k2_SRB + 2*rk4_k3_SRB + rk4_k4_SRB)/6;
    TnCHc_new = sv->TnCHc+(rk4_k1_TnCHc + 2*rk4_k2_TnCHc + 2*rk4_k3_TnCHc + rk4_k4_TnCHc)/6;
    TnCHm_new = sv->TnCHm+(rk4_k1_TnCHm + 2*rk4_k2_TnCHm + 2*rk4_k3_TnCHm + rk4_k4_TnCHm)/6;
    TnCL_new = sv->TnCL+(rk4_k1_TnCL + 2*rk4_k2_TnCL + 2*rk4_k3_TnCL + rk4_k4_TnCL)/6;
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    GlobalData_t RI = (((1.-(sv->RyRr))-(sv->RyRo))-(sv->RyRi));
    GlobalData_t alpha = (3.98e-4*(exp(((3.61e-1*Vm)*FoRT))));
    GlobalData_t beta = (5.74e-5*(exp(((-9.23e-2*Vm)*FoRT))));
    GlobalData_t delta = (1.2e-3*(exp(((-3.3e-1*Vm)*FoRT))));
    GlobalData_t eta = (1.25e-2*(exp(((-4.81e-1*Vm)*FoRT))));
    GlobalData_t gamma = (3.41e-3*(exp(((8.68e-1*Vm)*FoRT))));
    GlobalData_t kCaSR = (MaxSR-(((MaxSR-(MinSR))/(1.+(pow((ec50SR/sv->Ca_sr),2.5))))));
    GlobalData_t omega = (4.91e-3*(exp(((-6.79e-1*Vm)*FoRT))));
    GlobalData_t psi = (6.33e-3*(exp(((1.27*Vm)*FoRT))));
    GlobalData_t diff_C1 = (((-4.*alpha)*sv->C1)+(beta*sv->C2));
    GlobalData_t diff_C10 = (((gamma*sv->C7)-((((2.*delta)+(2.*alpha))*sv->C10)))+(beta*sv->C11));
    GlobalData_t diff_C11 = ((((((2.*gamma)*sv->C8)+((2.*alpha)*sv->C10))-((((((2.*delta)+beta)+alpha)+gamma)*sv->C11)))+((2.*beta)*sv->C12))+((3.*delta)*sv->C13));
    GlobalData_t diff_C12 = (((((3.*gamma)*sv->C9)+(alpha*sv->C11))-(((((2.*delta)+(2.*beta))+(2.*gamma))*sv->C12)))+((3.*delta)*sv->C14));
    GlobalData_t diff_C13 = (((gamma*sv->C11)-((((3.*delta)+alpha)*sv->C13)))+(beta*sv->C14));
    GlobalData_t diff_C14 = (((((2.*gamma)*sv->C12)+(alpha*sv->C13))-(((((3.*delta)+beta)+gamma)*sv->C14)))+((4.*delta)*sv->C15));
    GlobalData_t diff_C15 = (((gamma*sv->C14)-((((4.*delta)+teta)*sv->C15)))+(eta*sv->O1));
    GlobalData_t diff_C2 = ((((4.*alpha)*sv->C1)-((((beta+gamma)+(3.*alpha))*sv->C2)))+((2.*beta)*sv->C3));
    GlobalData_t diff_C3 = ((((3.*alpha)*sv->C2)-(((((2.*beta)+(2.*gamma))+(2.*alpha))*sv->C3)))+((3.*beta)*sv->C4));
    GlobalData_t diff_C4 = ((((2.*alpha)*sv->C3)-(((((3.*beta)+(3.*gamma))+alpha)*sv->C4)))+((4.*beta)*sv->C5));
    GlobalData_t diff_C5 = (((alpha*sv->C3)-((((4.*beta)+(4.*gamma))*sv->C5)))+(delta*sv->C9));
    GlobalData_t diff_C6 = (((gamma*sv->C2)-(((delta+(3.*alpha))*sv->C6)))+(beta*sv->C7));
    GlobalData_t diff_C7 = ((((((2.*gamma)*sv->C3)+((3.*alpha)*sv->C6))-(((((delta+beta)+(2.*alpha))+gamma)*sv->C7)))+((2.*beta)*sv->C8))+((2.*delta)*sv->C10));
    GlobalData_t diff_C8 = ((((((3.*gamma)*sv->C4)+((2.*alpha)*sv->C7))-(((((delta+(2.*beta))+alpha)+(2.*gamma))*sv->C8)))+((3.*beta)*sv->C9))+((2.*delta)*sv->C11));
    GlobalData_t diff_C9 = (((((4.*gamma)*sv->C5)+(alpha*sv->C8))-((((delta+(3.*beta))+(3.*gamma))*sv->C9)))+((2.*delta)*sv->C12));
    GlobalData_t diff_O1 = (((teta*sv->C15)-(((eta+psi)*sv->O1)))+(omega*O2));
    GlobalData_t kiSRCa = (kiCa*kCaSR);
    GlobalData_t koSRCa = (koCa/kCaSR);
    GlobalData_t partial_diff_C10_del_C10 = (-((2.*delta)+(2.*alpha)));
    GlobalData_t partial_diff_C10_del_C11 = beta;
    GlobalData_t partial_diff_C10_del_C7 = gamma;
    GlobalData_t partial_diff_C11_del_C10 = (2.*alpha);
    GlobalData_t partial_diff_C11_del_C11 = (-((((2.*delta)+beta)+alpha)+gamma));
    GlobalData_t partial_diff_C11_del_C12 = (2.*beta);
    GlobalData_t partial_diff_C11_del_C13 = (3.*delta);
    GlobalData_t partial_diff_C11_del_C8 = (2.*gamma);
    GlobalData_t partial_diff_C12_del_C11 = alpha;
    GlobalData_t partial_diff_C12_del_C12 = (-(((2.*delta)+(2.*beta))+(2.*gamma)));
    GlobalData_t partial_diff_C12_del_C14 = (3.*delta);
    GlobalData_t partial_diff_C12_del_C9 = (3.*gamma);
    GlobalData_t partial_diff_C13_del_C11 = gamma;
    GlobalData_t partial_diff_C13_del_C13 = (-((3.*delta)+alpha));
    GlobalData_t partial_diff_C13_del_C14 = beta;
    GlobalData_t partial_diff_C14_del_C12 = (2.*gamma);
    GlobalData_t partial_diff_C14_del_C13 = alpha;
    GlobalData_t partial_diff_C14_del_C14 = (-(((3.*delta)+beta)+gamma));
    GlobalData_t partial_diff_C14_del_C15 = (4.*delta);
    GlobalData_t partial_diff_C15_del_C14 = gamma;
    GlobalData_t partial_diff_C15_del_C15 = (-((4.*delta)+teta));
    GlobalData_t partial_diff_C15_del_O1 = eta;
    GlobalData_t partial_diff_C1_del_C1 = (-4.*alpha);
    GlobalData_t partial_diff_C1_del_C2 = beta;
    GlobalData_t partial_diff_C2_del_C1 = (4.*alpha);
    GlobalData_t partial_diff_C2_del_C2 = (-((beta+gamma)+(3.*alpha)));
    GlobalData_t partial_diff_C2_del_C3 = (2.*beta);
    GlobalData_t partial_diff_C3_del_C2 = (3.*alpha);
    GlobalData_t partial_diff_C3_del_C3 = (-(((2.*beta)+(2.*gamma))+(2.*alpha)));
    GlobalData_t partial_diff_C3_del_C4 = (3.*beta);
    GlobalData_t partial_diff_C4_del_C3 = (2.*alpha);
    GlobalData_t partial_diff_C4_del_C4 = (-(((3.*beta)+(3.*gamma))+alpha));
    GlobalData_t partial_diff_C4_del_C5 = (4.*beta);
    GlobalData_t partial_diff_C5_del_C3 = alpha;
    GlobalData_t partial_diff_C5_del_C5 = (-((4.*beta)+(4.*gamma)));
    GlobalData_t partial_diff_C5_del_C9 = delta;
    GlobalData_t partial_diff_C6_del_C2 = gamma;
    GlobalData_t partial_diff_C6_del_C6 = (-(delta+(3.*alpha)));
    GlobalData_t partial_diff_C6_del_C7 = beta;
    GlobalData_t partial_diff_C7_del_C10 = (2.*delta);
    GlobalData_t partial_diff_C7_del_C3 = (2.*gamma);
    GlobalData_t partial_diff_C7_del_C6 = (3.*alpha);
    GlobalData_t partial_diff_C7_del_C7 = (-(((delta+beta)+(2.*alpha))+gamma));
    GlobalData_t partial_diff_C7_del_C8 = (2.*beta);
    GlobalData_t partial_diff_C8_del_C11 = (2.*delta);
    GlobalData_t partial_diff_C8_del_C4 = (3.*gamma);
    GlobalData_t partial_diff_C8_del_C7 = (2.*alpha);
    GlobalData_t partial_diff_C8_del_C8 = (-(((delta+(2.*beta))+alpha)+(2.*gamma)));
    GlobalData_t partial_diff_C8_del_C9 = (3.*beta);
    GlobalData_t partial_diff_C9_del_C12 = (2.*delta);
    GlobalData_t partial_diff_C9_del_C5 = (4.*gamma);
    GlobalData_t partial_diff_C9_del_C8 = alpha;
    GlobalData_t partial_diff_C9_del_C9 = (-((delta+(3.*beta))+(3.*gamma)));
    GlobalData_t partial_diff_O1_del_C1 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_C10 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_C11 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_C12 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_C13 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_C14 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_C15 = (teta+(omega*-1.));
    GlobalData_t partial_diff_O1_del_C2 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_C3 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_C4 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_C5 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_C6 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_C7 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_C8 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_C9 = (omega*-1.);
    GlobalData_t partial_diff_O1_del_O1 = ((-(eta+psi))+(omega*-1.));
    GlobalData_t diff_RyRi = ((((kiSRCa*sv->Caj)*sv->RyRo)-((kim*sv->RyRi)))-(((kom*sv->RyRi)-((((koSRCa*sv->Caj)*sv->Caj)*RI)))));
    GlobalData_t diff_RyRo = (((((koSRCa*sv->Caj)*sv->Caj)*sv->RyRr)-((kom*sv->RyRo)))-((((kiSRCa*sv->Caj)*sv->RyRo)-((kim*sv->RyRi)))));
    GlobalData_t diff_RyRr = (((kim*RI)-(((kiSRCa*sv->Caj)*sv->RyRr)))-(((((koSRCa*sv->Caj)*sv->Caj)*sv->RyRr)-((kom*sv->RyRo)))));
    GlobalData_t partial_diff_RyRi_del_RyRi = ((-kim)-((kom-((((koSRCa*sv->Caj)*sv->Caj)*-1.)))));
    GlobalData_t partial_diff_RyRi_del_RyRo = ((kiSRCa*sv->Caj)-((-(((koSRCa*sv->Caj)*sv->Caj)*-1.))));
    GlobalData_t partial_diff_RyRi_del_RyRr = (-(-(((koSRCa*sv->Caj)*sv->Caj)*-1.)));
    GlobalData_t partial_diff_RyRo_del_RyRo = ((-kom)-((kiSRCa*sv->Caj)));
    GlobalData_t partial_diff_RyRo_del_RyRr = ((koSRCa*sv->Caj)*sv->Caj);
    GlobalData_t partial_diff_RyRr_del_RyRr = (((kim*-1.)-((kiSRCa*sv->Caj)))-(((koSRCa*sv->Caj)*sv->Caj)));
    GlobalData_t C1_new = sv->C1;
    GlobalData_t C10_new = sv->C10;
    GlobalData_t C11_new = sv->C11;
    GlobalData_t C12_new = sv->C12;
    GlobalData_t C13_new = sv->C13;
    GlobalData_t C14_new = sv->C14;
    GlobalData_t C15_new = sv->C15;
    GlobalData_t C2_new = sv->C2;
    GlobalData_t C3_new = sv->C3;
    GlobalData_t C4_new = sv->C4;
    GlobalData_t C5_new = sv->C5;
    GlobalData_t C6_new = sv->C6;
    GlobalData_t C7_new = sv->C7;
    GlobalData_t C8_new = sv->C8;
    GlobalData_t C9_new = sv->C9;
    GlobalData_t O1_new = sv->O1;
    GlobalData_t RyRi_new = sv->RyRi;
    GlobalData_t RyRo_new = sv->RyRo;
    GlobalData_t RyRr_new = sv->RyRr;
    
    int __count=0;
    GlobalData_t __error=1;
    while (__error > 1e-100 && __count < 50) {
      GlobalData_t C1_markov_solve = C1_new;
      GlobalData_t C10_markov_solve = C10_new;
      GlobalData_t C11_markov_solve = C11_new;
      GlobalData_t C12_markov_solve = C12_new;
      GlobalData_t C13_markov_solve = C13_new;
      GlobalData_t C14_markov_solve = C14_new;
      GlobalData_t C15_markov_solve = C15_new;
      GlobalData_t C2_markov_solve = C2_new;
      GlobalData_t C3_markov_solve = C3_new;
      GlobalData_t C4_markov_solve = C4_new;
      GlobalData_t C5_markov_solve = C5_new;
      GlobalData_t C6_markov_solve = C6_new;
      GlobalData_t C7_markov_solve = C7_new;
      GlobalData_t C8_markov_solve = C8_new;
      GlobalData_t C9_markov_solve = C9_new;
      GlobalData_t O1_markov_solve = O1_new;
      GlobalData_t RyRi_markov_solve = RyRi_new;
      GlobalData_t RyRo_markov_solve = RyRo_new;
      GlobalData_t RyRr_markov_solve = RyRr_new;
      C10_markov_solve = ((diff_C10+(dt*((((((((((((((((((partial_diff_C10_del_C1*C1_markov_solve)+(partial_diff_C10_del_C11*C11_markov_solve))+(partial_diff_C10_del_C12*C12_markov_solve))+(partial_diff_C10_del_C13*C13_markov_solve))+(partial_diff_C10_del_C14*C14_markov_solve))+(partial_diff_C10_del_C15*C15_markov_solve))+(partial_diff_C10_del_C2*C2_markov_solve))+(partial_diff_C10_del_C3*C3_markov_solve))+(partial_diff_C10_del_C4*C4_markov_solve))+(partial_diff_C10_del_C5*C5_markov_solve))+(partial_diff_C10_del_C6*C6_markov_solve))+(partial_diff_C10_del_C7*C7_markov_solve))+(partial_diff_C10_del_C8*C8_markov_solve))+(partial_diff_C10_del_C9*C9_markov_solve))+(partial_diff_C10_del_O1*O1_markov_solve))+(partial_diff_C10_del_RyRi*RyRi_markov_solve))+(partial_diff_C10_del_RyRo*RyRo_markov_solve))+(partial_diff_C10_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C10_del_C10))));
      C11_markov_solve = ((diff_C11+(dt*((((((((((((((((((partial_diff_C11_del_C1*C1_markov_solve)+(partial_diff_C11_del_C10*C10_markov_solve))+(partial_diff_C11_del_C12*C12_markov_solve))+(partial_diff_C11_del_C13*C13_markov_solve))+(partial_diff_C11_del_C14*C14_markov_solve))+(partial_diff_C11_del_C15*C15_markov_solve))+(partial_diff_C11_del_C2*C2_markov_solve))+(partial_diff_C11_del_C3*C3_markov_solve))+(partial_diff_C11_del_C4*C4_markov_solve))+(partial_diff_C11_del_C5*C5_markov_solve))+(partial_diff_C11_del_C6*C6_markov_solve))+(partial_diff_C11_del_C7*C7_markov_solve))+(partial_diff_C11_del_C8*C8_markov_solve))+(partial_diff_C11_del_C9*C9_markov_solve))+(partial_diff_C11_del_O1*O1_markov_solve))+(partial_diff_C11_del_RyRi*RyRi_markov_solve))+(partial_diff_C11_del_RyRo*RyRo_markov_solve))+(partial_diff_C11_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C11_del_C11))));
      C12_markov_solve = ((diff_C12+(dt*((((((((((((((((((partial_diff_C12_del_C1*C1_markov_solve)+(partial_diff_C12_del_C10*C10_markov_solve))+(partial_diff_C12_del_C11*C11_markov_solve))+(partial_diff_C12_del_C13*C13_markov_solve))+(partial_diff_C12_del_C14*C14_markov_solve))+(partial_diff_C12_del_C15*C15_markov_solve))+(partial_diff_C12_del_C2*C2_markov_solve))+(partial_diff_C12_del_C3*C3_markov_solve))+(partial_diff_C12_del_C4*C4_markov_solve))+(partial_diff_C12_del_C5*C5_markov_solve))+(partial_diff_C12_del_C6*C6_markov_solve))+(partial_diff_C12_del_C7*C7_markov_solve))+(partial_diff_C12_del_C8*C8_markov_solve))+(partial_diff_C12_del_C9*C9_markov_solve))+(partial_diff_C12_del_O1*O1_markov_solve))+(partial_diff_C12_del_RyRi*RyRi_markov_solve))+(partial_diff_C12_del_RyRo*RyRo_markov_solve))+(partial_diff_C12_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C12_del_C12))));
      C13_markov_solve = ((diff_C13+(dt*((((((((((((((((((partial_diff_C13_del_C1*C1_markov_solve)+(partial_diff_C13_del_C10*C10_markov_solve))+(partial_diff_C13_del_C11*C11_markov_solve))+(partial_diff_C13_del_C12*C12_markov_solve))+(partial_diff_C13_del_C14*C14_markov_solve))+(partial_diff_C13_del_C15*C15_markov_solve))+(partial_diff_C13_del_C2*C2_markov_solve))+(partial_diff_C13_del_C3*C3_markov_solve))+(partial_diff_C13_del_C4*C4_markov_solve))+(partial_diff_C13_del_C5*C5_markov_solve))+(partial_diff_C13_del_C6*C6_markov_solve))+(partial_diff_C13_del_C7*C7_markov_solve))+(partial_diff_C13_del_C8*C8_markov_solve))+(partial_diff_C13_del_C9*C9_markov_solve))+(partial_diff_C13_del_O1*O1_markov_solve))+(partial_diff_C13_del_RyRi*RyRi_markov_solve))+(partial_diff_C13_del_RyRo*RyRo_markov_solve))+(partial_diff_C13_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C13_del_C13))));
      C14_markov_solve = ((diff_C14+(dt*((((((((((((((((((partial_diff_C14_del_C1*C1_markov_solve)+(partial_diff_C14_del_C10*C10_markov_solve))+(partial_diff_C14_del_C11*C11_markov_solve))+(partial_diff_C14_del_C12*C12_markov_solve))+(partial_diff_C14_del_C13*C13_markov_solve))+(partial_diff_C14_del_C15*C15_markov_solve))+(partial_diff_C14_del_C2*C2_markov_solve))+(partial_diff_C14_del_C3*C3_markov_solve))+(partial_diff_C14_del_C4*C4_markov_solve))+(partial_diff_C14_del_C5*C5_markov_solve))+(partial_diff_C14_del_C6*C6_markov_solve))+(partial_diff_C14_del_C7*C7_markov_solve))+(partial_diff_C14_del_C8*C8_markov_solve))+(partial_diff_C14_del_C9*C9_markov_solve))+(partial_diff_C14_del_O1*O1_markov_solve))+(partial_diff_C14_del_RyRi*RyRi_markov_solve))+(partial_diff_C14_del_RyRo*RyRo_markov_solve))+(partial_diff_C14_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C14_del_C14))));
      C15_markov_solve = ((diff_C15+(dt*((((((((((((((((((partial_diff_C15_del_C1*C1_markov_solve)+(partial_diff_C15_del_C10*C10_markov_solve))+(partial_diff_C15_del_C11*C11_markov_solve))+(partial_diff_C15_del_C12*C12_markov_solve))+(partial_diff_C15_del_C13*C13_markov_solve))+(partial_diff_C15_del_C14*C14_markov_solve))+(partial_diff_C15_del_C2*C2_markov_solve))+(partial_diff_C15_del_C3*C3_markov_solve))+(partial_diff_C15_del_C4*C4_markov_solve))+(partial_diff_C15_del_C5*C5_markov_solve))+(partial_diff_C15_del_C6*C6_markov_solve))+(partial_diff_C15_del_C7*C7_markov_solve))+(partial_diff_C15_del_C8*C8_markov_solve))+(partial_diff_C15_del_C9*C9_markov_solve))+(partial_diff_C15_del_O1*O1_markov_solve))+(partial_diff_C15_del_RyRi*RyRi_markov_solve))+(partial_diff_C15_del_RyRo*RyRo_markov_solve))+(partial_diff_C15_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C15_del_C15))));
      C1_markov_solve = ((diff_C1+(dt*((((((((((((((((((partial_diff_C1_del_C10*C10_markov_solve)+(partial_diff_C1_del_C11*C11_markov_solve))+(partial_diff_C1_del_C12*C12_markov_solve))+(partial_diff_C1_del_C13*C13_markov_solve))+(partial_diff_C1_del_C14*C14_markov_solve))+(partial_diff_C1_del_C15*C15_markov_solve))+(partial_diff_C1_del_C2*C2_markov_solve))+(partial_diff_C1_del_C3*C3_markov_solve))+(partial_diff_C1_del_C4*C4_markov_solve))+(partial_diff_C1_del_C5*C5_markov_solve))+(partial_diff_C1_del_C6*C6_markov_solve))+(partial_diff_C1_del_C7*C7_markov_solve))+(partial_diff_C1_del_C8*C8_markov_solve))+(partial_diff_C1_del_C9*C9_markov_solve))+(partial_diff_C1_del_O1*O1_markov_solve))+(partial_diff_C1_del_RyRi*RyRi_markov_solve))+(partial_diff_C1_del_RyRo*RyRo_markov_solve))+(partial_diff_C1_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C1_del_C1))));
      C2_markov_solve = ((diff_C2+(dt*((((((((((((((((((partial_diff_C2_del_C1*C1_markov_solve)+(partial_diff_C2_del_C10*C10_markov_solve))+(partial_diff_C2_del_C11*C11_markov_solve))+(partial_diff_C2_del_C12*C12_markov_solve))+(partial_diff_C2_del_C13*C13_markov_solve))+(partial_diff_C2_del_C14*C14_markov_solve))+(partial_diff_C2_del_C15*C15_markov_solve))+(partial_diff_C2_del_C3*C3_markov_solve))+(partial_diff_C2_del_C4*C4_markov_solve))+(partial_diff_C2_del_C5*C5_markov_solve))+(partial_diff_C2_del_C6*C6_markov_solve))+(partial_diff_C2_del_C7*C7_markov_solve))+(partial_diff_C2_del_C8*C8_markov_solve))+(partial_diff_C2_del_C9*C9_markov_solve))+(partial_diff_C2_del_O1*O1_markov_solve))+(partial_diff_C2_del_RyRi*RyRi_markov_solve))+(partial_diff_C2_del_RyRo*RyRo_markov_solve))+(partial_diff_C2_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C2_del_C2))));
      C3_markov_solve = ((diff_C3+(dt*((((((((((((((((((partial_diff_C3_del_C1*C1_markov_solve)+(partial_diff_C3_del_C10*C10_markov_solve))+(partial_diff_C3_del_C11*C11_markov_solve))+(partial_diff_C3_del_C12*C12_markov_solve))+(partial_diff_C3_del_C13*C13_markov_solve))+(partial_diff_C3_del_C14*C14_markov_solve))+(partial_diff_C3_del_C15*C15_markov_solve))+(partial_diff_C3_del_C2*C2_markov_solve))+(partial_diff_C3_del_C4*C4_markov_solve))+(partial_diff_C3_del_C5*C5_markov_solve))+(partial_diff_C3_del_C6*C6_markov_solve))+(partial_diff_C3_del_C7*C7_markov_solve))+(partial_diff_C3_del_C8*C8_markov_solve))+(partial_diff_C3_del_C9*C9_markov_solve))+(partial_diff_C3_del_O1*O1_markov_solve))+(partial_diff_C3_del_RyRi*RyRi_markov_solve))+(partial_diff_C3_del_RyRo*RyRo_markov_solve))+(partial_diff_C3_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C3_del_C3))));
      C4_markov_solve = ((diff_C4+(dt*((((((((((((((((((partial_diff_C4_del_C1*C1_markov_solve)+(partial_diff_C4_del_C10*C10_markov_solve))+(partial_diff_C4_del_C11*C11_markov_solve))+(partial_diff_C4_del_C12*C12_markov_solve))+(partial_diff_C4_del_C13*C13_markov_solve))+(partial_diff_C4_del_C14*C14_markov_solve))+(partial_diff_C4_del_C15*C15_markov_solve))+(partial_diff_C4_del_C2*C2_markov_solve))+(partial_diff_C4_del_C3*C3_markov_solve))+(partial_diff_C4_del_C5*C5_markov_solve))+(partial_diff_C4_del_C6*C6_markov_solve))+(partial_diff_C4_del_C7*C7_markov_solve))+(partial_diff_C4_del_C8*C8_markov_solve))+(partial_diff_C4_del_C9*C9_markov_solve))+(partial_diff_C4_del_O1*O1_markov_solve))+(partial_diff_C4_del_RyRi*RyRi_markov_solve))+(partial_diff_C4_del_RyRo*RyRo_markov_solve))+(partial_diff_C4_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C4_del_C4))));
      C5_markov_solve = ((diff_C5+(dt*((((((((((((((((((partial_diff_C5_del_C1*C1_markov_solve)+(partial_diff_C5_del_C10*C10_markov_solve))+(partial_diff_C5_del_C11*C11_markov_solve))+(partial_diff_C5_del_C12*C12_markov_solve))+(partial_diff_C5_del_C13*C13_markov_solve))+(partial_diff_C5_del_C14*C14_markov_solve))+(partial_diff_C5_del_C15*C15_markov_solve))+(partial_diff_C5_del_C2*C2_markov_solve))+(partial_diff_C5_del_C3*C3_markov_solve))+(partial_diff_C5_del_C4*C4_markov_solve))+(partial_diff_C5_del_C6*C6_markov_solve))+(partial_diff_C5_del_C7*C7_markov_solve))+(partial_diff_C5_del_C8*C8_markov_solve))+(partial_diff_C5_del_C9*C9_markov_solve))+(partial_diff_C5_del_O1*O1_markov_solve))+(partial_diff_C5_del_RyRi*RyRi_markov_solve))+(partial_diff_C5_del_RyRo*RyRo_markov_solve))+(partial_diff_C5_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C5_del_C5))));
      C6_markov_solve = ((diff_C6+(dt*((((((((((((((((((partial_diff_C6_del_C1*C1_markov_solve)+(partial_diff_C6_del_C10*C10_markov_solve))+(partial_diff_C6_del_C11*C11_markov_solve))+(partial_diff_C6_del_C12*C12_markov_solve))+(partial_diff_C6_del_C13*C13_markov_solve))+(partial_diff_C6_del_C14*C14_markov_solve))+(partial_diff_C6_del_C15*C15_markov_solve))+(partial_diff_C6_del_C2*C2_markov_solve))+(partial_diff_C6_del_C3*C3_markov_solve))+(partial_diff_C6_del_C4*C4_markov_solve))+(partial_diff_C6_del_C5*C5_markov_solve))+(partial_diff_C6_del_C7*C7_markov_solve))+(partial_diff_C6_del_C8*C8_markov_solve))+(partial_diff_C6_del_C9*C9_markov_solve))+(partial_diff_C6_del_O1*O1_markov_solve))+(partial_diff_C6_del_RyRi*RyRi_markov_solve))+(partial_diff_C6_del_RyRo*RyRo_markov_solve))+(partial_diff_C6_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C6_del_C6))));
      C7_markov_solve = ((diff_C7+(dt*((((((((((((((((((partial_diff_C7_del_C1*C1_markov_solve)+(partial_diff_C7_del_C10*C10_markov_solve))+(partial_diff_C7_del_C11*C11_markov_solve))+(partial_diff_C7_del_C12*C12_markov_solve))+(partial_diff_C7_del_C13*C13_markov_solve))+(partial_diff_C7_del_C14*C14_markov_solve))+(partial_diff_C7_del_C15*C15_markov_solve))+(partial_diff_C7_del_C2*C2_markov_solve))+(partial_diff_C7_del_C3*C3_markov_solve))+(partial_diff_C7_del_C4*C4_markov_solve))+(partial_diff_C7_del_C5*C5_markov_solve))+(partial_diff_C7_del_C6*C6_markov_solve))+(partial_diff_C7_del_C8*C8_markov_solve))+(partial_diff_C7_del_C9*C9_markov_solve))+(partial_diff_C7_del_O1*O1_markov_solve))+(partial_diff_C7_del_RyRi*RyRi_markov_solve))+(partial_diff_C7_del_RyRo*RyRo_markov_solve))+(partial_diff_C7_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C7_del_C7))));
      C8_markov_solve = ((diff_C8+(dt*((((((((((((((((((partial_diff_C8_del_C1*C1_markov_solve)+(partial_diff_C8_del_C10*C10_markov_solve))+(partial_diff_C8_del_C11*C11_markov_solve))+(partial_diff_C8_del_C12*C12_markov_solve))+(partial_diff_C8_del_C13*C13_markov_solve))+(partial_diff_C8_del_C14*C14_markov_solve))+(partial_diff_C8_del_C15*C15_markov_solve))+(partial_diff_C8_del_C2*C2_markov_solve))+(partial_diff_C8_del_C3*C3_markov_solve))+(partial_diff_C8_del_C4*C4_markov_solve))+(partial_diff_C8_del_C5*C5_markov_solve))+(partial_diff_C8_del_C6*C6_markov_solve))+(partial_diff_C8_del_C7*C7_markov_solve))+(partial_diff_C8_del_C9*C9_markov_solve))+(partial_diff_C8_del_O1*O1_markov_solve))+(partial_diff_C8_del_RyRi*RyRi_markov_solve))+(partial_diff_C8_del_RyRo*RyRo_markov_solve))+(partial_diff_C8_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C8_del_C8))));
      C9_markov_solve = ((diff_C9+(dt*((((((((((((((((((partial_diff_C9_del_C1*C1_markov_solve)+(partial_diff_C9_del_C10*C10_markov_solve))+(partial_diff_C9_del_C11*C11_markov_solve))+(partial_diff_C9_del_C12*C12_markov_solve))+(partial_diff_C9_del_C13*C13_markov_solve))+(partial_diff_C9_del_C14*C14_markov_solve))+(partial_diff_C9_del_C15*C15_markov_solve))+(partial_diff_C9_del_C2*C2_markov_solve))+(partial_diff_C9_del_C3*C3_markov_solve))+(partial_diff_C9_del_C4*C4_markov_solve))+(partial_diff_C9_del_C5*C5_markov_solve))+(partial_diff_C9_del_C6*C6_markov_solve))+(partial_diff_C9_del_C7*C7_markov_solve))+(partial_diff_C9_del_C8*C8_markov_solve))+(partial_diff_C9_del_O1*O1_markov_solve))+(partial_diff_C9_del_RyRi*RyRi_markov_solve))+(partial_diff_C9_del_RyRo*RyRo_markov_solve))+(partial_diff_C9_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_C9_del_C9))));
      O1_markov_solve = ((diff_O1+(dt*((((((((((((((((((partial_diff_O1_del_C1*C1_markov_solve)+(partial_diff_O1_del_C10*C10_markov_solve))+(partial_diff_O1_del_C11*C11_markov_solve))+(partial_diff_O1_del_C12*C12_markov_solve))+(partial_diff_O1_del_C13*C13_markov_solve))+(partial_diff_O1_del_C14*C14_markov_solve))+(partial_diff_O1_del_C15*C15_markov_solve))+(partial_diff_O1_del_C2*C2_markov_solve))+(partial_diff_O1_del_C3*C3_markov_solve))+(partial_diff_O1_del_C4*C4_markov_solve))+(partial_diff_O1_del_C5*C5_markov_solve))+(partial_diff_O1_del_C6*C6_markov_solve))+(partial_diff_O1_del_C7*C7_markov_solve))+(partial_diff_O1_del_C8*C8_markov_solve))+(partial_diff_O1_del_C9*C9_markov_solve))+(partial_diff_O1_del_RyRi*RyRi_markov_solve))+(partial_diff_O1_del_RyRo*RyRo_markov_solve))+(partial_diff_O1_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_O1_del_O1))));
      RyRi_markov_solve = ((diff_RyRi+(dt*((((((((((((((((((partial_diff_RyRi_del_C1*C1_markov_solve)+(partial_diff_RyRi_del_C10*C10_markov_solve))+(partial_diff_RyRi_del_C11*C11_markov_solve))+(partial_diff_RyRi_del_C12*C12_markov_solve))+(partial_diff_RyRi_del_C13*C13_markov_solve))+(partial_diff_RyRi_del_C14*C14_markov_solve))+(partial_diff_RyRi_del_C15*C15_markov_solve))+(partial_diff_RyRi_del_C2*C2_markov_solve))+(partial_diff_RyRi_del_C3*C3_markov_solve))+(partial_diff_RyRi_del_C4*C4_markov_solve))+(partial_diff_RyRi_del_C5*C5_markov_solve))+(partial_diff_RyRi_del_C6*C6_markov_solve))+(partial_diff_RyRi_del_C7*C7_markov_solve))+(partial_diff_RyRi_del_C8*C8_markov_solve))+(partial_diff_RyRi_del_C9*C9_markov_solve))+(partial_diff_RyRi_del_O1*O1_markov_solve))+(partial_diff_RyRi_del_RyRo*RyRo_markov_solve))+(partial_diff_RyRi_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_RyRi_del_RyRi))));
      RyRo_markov_solve = ((diff_RyRo+(dt*((((((((((((((((((partial_diff_RyRo_del_C1*C1_markov_solve)+(partial_diff_RyRo_del_C10*C10_markov_solve))+(partial_diff_RyRo_del_C11*C11_markov_solve))+(partial_diff_RyRo_del_C12*C12_markov_solve))+(partial_diff_RyRo_del_C13*C13_markov_solve))+(partial_diff_RyRo_del_C14*C14_markov_solve))+(partial_diff_RyRo_del_C15*C15_markov_solve))+(partial_diff_RyRo_del_C2*C2_markov_solve))+(partial_diff_RyRo_del_C3*C3_markov_solve))+(partial_diff_RyRo_del_C4*C4_markov_solve))+(partial_diff_RyRo_del_C5*C5_markov_solve))+(partial_diff_RyRo_del_C6*C6_markov_solve))+(partial_diff_RyRo_del_C7*C7_markov_solve))+(partial_diff_RyRo_del_C8*C8_markov_solve))+(partial_diff_RyRo_del_C9*C9_markov_solve))+(partial_diff_RyRo_del_O1*O1_markov_solve))+(partial_diff_RyRo_del_RyRi*RyRi_markov_solve))+(partial_diff_RyRo_del_RyRr*RyRr_markov_solve))))/(1.-((dt*partial_diff_RyRo_del_RyRo))));
      RyRr_markov_solve = ((diff_RyRr+(dt*((((((((((((((((((partial_diff_RyRr_del_C1*C1_markov_solve)+(partial_diff_RyRr_del_C10*C10_markov_solve))+(partial_diff_RyRr_del_C11*C11_markov_solve))+(partial_diff_RyRr_del_C12*C12_markov_solve))+(partial_diff_RyRr_del_C13*C13_markov_solve))+(partial_diff_RyRr_del_C14*C14_markov_solve))+(partial_diff_RyRr_del_C15*C15_markov_solve))+(partial_diff_RyRr_del_C2*C2_markov_solve))+(partial_diff_RyRr_del_C3*C3_markov_solve))+(partial_diff_RyRr_del_C4*C4_markov_solve))+(partial_diff_RyRr_del_C5*C5_markov_solve))+(partial_diff_RyRr_del_C6*C6_markov_solve))+(partial_diff_RyRr_del_C7*C7_markov_solve))+(partial_diff_RyRr_del_C8*C8_markov_solve))+(partial_diff_RyRr_del_C9*C9_markov_solve))+(partial_diff_RyRr_del_O1*O1_markov_solve))+(partial_diff_RyRr_del_RyRi*RyRi_markov_solve))+(partial_diff_RyRr_del_RyRo*RyRo_markov_solve))))/(1.-((dt*partial_diff_RyRr_del_RyRr))));
      __error=0;
      __error += fabs(C1_markov_solve-C1_new);
      C1_new = C1_markov_solve;
      __error += fabs(C10_markov_solve-C10_new);
      C10_new = C10_markov_solve;
      __error += fabs(C11_markov_solve-C11_new);
      C11_new = C11_markov_solve;
      __error += fabs(C12_markov_solve-C12_new);
      C12_new = C12_markov_solve;
      __error += fabs(C13_markov_solve-C13_new);
      C13_new = C13_markov_solve;
      __error += fabs(C14_markov_solve-C14_new);
      C14_new = C14_markov_solve;
      __error += fabs(C15_markov_solve-C15_new);
      C15_new = C15_markov_solve;
      __error += fabs(C2_markov_solve-C2_new);
      C2_new = C2_markov_solve;
      __error += fabs(C3_markov_solve-C3_new);
      C3_new = C3_markov_solve;
      __error += fabs(C4_markov_solve-C4_new);
      C4_new = C4_markov_solve;
      __error += fabs(C5_markov_solve-C5_new);
      C5_new = C5_markov_solve;
      __error += fabs(C6_markov_solve-C6_new);
      C6_new = C6_markov_solve;
      __error += fabs(C7_markov_solve-C7_new);
      C7_new = C7_markov_solve;
      __error += fabs(C8_markov_solve-C8_new);
      C8_new = C8_markov_solve;
      __error += fabs(C9_markov_solve-C9_new);
      C9_new = C9_markov_solve;
      __error += fabs(O1_markov_solve-O1_new);
      O1_new = O1_markov_solve;
      __error += fabs(RyRi_markov_solve-RyRi_new);
      RyRi_new = RyRi_markov_solve;
      __error += fabs(RyRo_markov_solve-RyRo_new);
      RyRo_new = RyRo_markov_solve;
      __error += fabs(RyRr_markov_solve-RyRr_new);
      RyRr_new = RyRr_markov_solve;
      __count++;
    }
    C1_new = sv->C1+dt*C1_new;
    C10_new = sv->C10+dt*C10_new;
    C11_new = sv->C11+dt*C11_new;
    C12_new = sv->C12+dt*C12_new;
    C13_new = sv->C13+dt*C13_new;
    C14_new = sv->C14+dt*C14_new;
    C15_new = sv->C15+dt*C15_new;
    C2_new = sv->C2+dt*C2_new;
    C3_new = sv->C3+dt*C3_new;
    C4_new = sv->C4+dt*C4_new;
    C5_new = sv->C5+dt*C5_new;
    C6_new = sv->C6+dt*C6_new;
    C7_new = sv->C7+dt*C7_new;
    C8_new = sv->C8+dt*C8_new;
    C9_new = sv->C9+dt*C9_new;
    O1_new = sv->O1+dt*O1_new;
    RyRi_new = sv->RyRi+dt*RyRi_new;
    RyRo_new = sv->RyRo+dt*RyRo_new;
    RyRr_new = sv->RyRr+dt*RyRr_new;
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    if (C1_new < 0) { IIF_warn(__i, "C1 < 0, clamping to 0"); sv->C1 = 0; }
    else if (C1_new > 1) { IIF_warn(__i, "C1 > 1, clamping to 1"); sv->C1 = 1; }
    else {sv->C1 = C1_new;}
    if (C10_new < 0) { IIF_warn(__i, "C10 < 0, clamping to 0"); sv->C10 = 0; }
    else if (C10_new > 1) { IIF_warn(__i, "C10 > 1, clamping to 1"); sv->C10 = 1; }
    else {sv->C10 = C10_new;}
    if (C11_new < 0) { IIF_warn(__i, "C11 < 0, clamping to 0"); sv->C11 = 0; }
    else if (C11_new > 1) { IIF_warn(__i, "C11 > 1, clamping to 1"); sv->C11 = 1; }
    else {sv->C11 = C11_new;}
    if (C12_new < 0) { IIF_warn(__i, "C12 < 0, clamping to 0"); sv->C12 = 0; }
    else if (C12_new > 1) { IIF_warn(__i, "C12 > 1, clamping to 1"); sv->C12 = 1; }
    else {sv->C12 = C12_new;}
    if (C13_new < 0) { IIF_warn(__i, "C13 < 0, clamping to 0"); sv->C13 = 0; }
    else if (C13_new > 1) { IIF_warn(__i, "C13 > 1, clamping to 1"); sv->C13 = 1; }
    else {sv->C13 = C13_new;}
    if (C14_new < 0) { IIF_warn(__i, "C14 < 0, clamping to 0"); sv->C14 = 0; }
    else if (C14_new > 1) { IIF_warn(__i, "C14 > 1, clamping to 1"); sv->C14 = 1; }
    else {sv->C14 = C14_new;}
    if (C15_new < 0) { IIF_warn(__i, "C15 < 0, clamping to 0"); sv->C15 = 0; }
    else if (C15_new > 1) { IIF_warn(__i, "C15 > 1, clamping to 1"); sv->C15 = 1; }
    else {sv->C15 = C15_new;}
    if (C2_new < 0) { IIF_warn(__i, "C2 < 0, clamping to 0"); sv->C2 = 0; }
    else if (C2_new > 1) { IIF_warn(__i, "C2 > 1, clamping to 1"); sv->C2 = 1; }
    else {sv->C2 = C2_new;}
    if (C3_new < 0) { IIF_warn(__i, "C3 < 0, clamping to 0"); sv->C3 = 0; }
    else if (C3_new > 1) { IIF_warn(__i, "C3 > 1, clamping to 1"); sv->C3 = 1; }
    else {sv->C3 = C3_new;}
    if (C4_new < 0) { IIF_warn(__i, "C4 < 0, clamping to 0"); sv->C4 = 0; }
    else if (C4_new > 1) { IIF_warn(__i, "C4 > 1, clamping to 1"); sv->C4 = 1; }
    else {sv->C4 = C4_new;}
    if (C5_new < 0) { IIF_warn(__i, "C5 < 0, clamping to 0"); sv->C5 = 0; }
    else if (C5_new > 1) { IIF_warn(__i, "C5 > 1, clamping to 1"); sv->C5 = 1; }
    else {sv->C5 = C5_new;}
    if (C6_new < 0) { IIF_warn(__i, "C6 < 0, clamping to 0"); sv->C6 = 0; }
    else if (C6_new > 1) { IIF_warn(__i, "C6 > 1, clamping to 1"); sv->C6 = 1; }
    else {sv->C6 = C6_new;}
    if (C7_new < 0) { IIF_warn(__i, "C7 < 0, clamping to 0"); sv->C7 = 0; }
    else if (C7_new > 1) { IIF_warn(__i, "C7 > 1, clamping to 1"); sv->C7 = 1; }
    else {sv->C7 = C7_new;}
    if (C8_new < 0) { IIF_warn(__i, "C8 < 0, clamping to 0"); sv->C8 = 0; }
    else if (C8_new > 1) { IIF_warn(__i, "C8 > 1, clamping to 1"); sv->C8 = 1; }
    else {sv->C8 = C8_new;}
    if (C9_new < 0) { IIF_warn(__i, "C9 < 0, clamping to 0"); sv->C9 = 0; }
    else if (C9_new > 1) { IIF_warn(__i, "C9 > 1, clamping to 1"); sv->C9 = 1; }
    else {sv->C9 = C9_new;}
    if (CaM_new < 1e-8) { IIF_warn(__i, "CaM < 1e-8, clamping to 1e-8"); sv->CaM = 1e-8; }
    else {sv->CaM = CaM_new;}
    if (Ca_sr_new < 1e-8) { IIF_warn(__i, "Ca_sr < 1e-8, clamping to 1e-8"); sv->Ca_sr = 1e-8; }
    else {sv->Ca_sr = Ca_sr_new;}
    if (Cai_new < 1e-8) { IIF_warn(__i, "Cai < 1e-8, clamping to 1e-8"); sv->Cai = 1e-8; }
    else {sv->Cai = Cai_new;}
    if (Caj_new < 1e-8) { IIF_warn(__i, "Caj < 1e-8, clamping to 1e-8"); sv->Caj = 1e-8; }
    else {sv->Caj = Caj_new;}
    if (Casl_new < 1e-8) { IIF_warn(__i, "Casl < 1e-8, clamping to 1e-8"); sv->Casl = 1e-8; }
    else {sv->Casl = Casl_new;}
    if (Csqnb_new < 1e-8) { IIF_warn(__i, "Csqnb < 1e-8, clamping to 1e-8"); sv->Csqnb = 1e-8; }
    else {sv->Csqnb = Csqnb_new;}
    Iion = Iion;
    if (Ki_new < 1e-8) { IIF_warn(__i, "Ki < 1e-8, clamping to 1e-8"); sv->Ki = 1e-8; }
    else {sv->Ki = Ki_new;}
    if (Myoc_new < 1e-8) { IIF_warn(__i, "Myoc < 1e-8, clamping to 1e-8"); sv->Myoc = 1e-8; }
    else {sv->Myoc = Myoc_new;}
    if (Myom_new < 1e-8) { IIF_warn(__i, "Myom < 1e-8, clamping to 1e-8"); sv->Myom = 1e-8; }
    else {sv->Myom = Myom_new;}
    if (NaBj_new < 1e-8) { IIF_warn(__i, "NaBj < 1e-8, clamping to 1e-8"); sv->NaBj = 1e-8; }
    else {sv->NaBj = NaBj_new;}
    if (NaBsl_new < 1e-8) { IIF_warn(__i, "NaBsl < 1e-8, clamping to 1e-8"); sv->NaBsl = 1e-8; }
    else {sv->NaBsl = NaBsl_new;}
    if (Nai_new < 1e-8) { IIF_warn(__i, "Nai < 1e-8, clamping to 1e-8"); sv->Nai = 1e-8; }
    else {sv->Nai = Nai_new;}
    if (Naj_new < 1e-8) { IIF_warn(__i, "Naj < 1e-8, clamping to 1e-8"); sv->Naj = 1e-8; }
    else {sv->Naj = Naj_new;}
    if (Nasl_new < 1e-8) { IIF_warn(__i, "Nasl < 1e-8, clamping to 1e-8"); sv->Nasl = 1e-8; }
    else {sv->Nasl = Nasl_new;}
    if (O1_new < 0) { IIF_warn(__i, "O1 < 0, clamping to 0"); sv->O1 = 0; }
    else if (O1_new > 1) { IIF_warn(__i, "O1 > 1, clamping to 1"); sv->O1 = 1; }
    else {sv->O1 = O1_new;}
    if (RyRi_new < 0) { IIF_warn(__i, "RyRi < 0, clamping to 0"); sv->RyRi = 0; }
    else if (RyRi_new > 1) { IIF_warn(__i, "RyRi > 1, clamping to 1"); sv->RyRi = 1; }
    else {sv->RyRi = RyRi_new;}
    if (RyRo_new < 0) { IIF_warn(__i, "RyRo < 0, clamping to 0"); sv->RyRo = 0; }
    else if (RyRo_new > 1) { IIF_warn(__i, "RyRo > 1, clamping to 1"); sv->RyRo = 1; }
    else {sv->RyRo = RyRo_new;}
    if (RyRr_new < 0) { IIF_warn(__i, "RyRr < 0, clamping to 0"); sv->RyRr = 0; }
    else if (RyRr_new > 1) { IIF_warn(__i, "RyRr > 1, clamping to 1"); sv->RyRr = 1; }
    else {sv->RyRr = RyRr_new;}
    if (SLHj_new < 1e-8) { IIF_warn(__i, "SLHj < 1e-8, clamping to 1e-8"); sv->SLHj = 1e-8; }
    else {sv->SLHj = SLHj_new;}
    if (SLHsl_new < 1e-8) { IIF_warn(__i, "SLHsl < 1e-8, clamping to 1e-8"); sv->SLHsl = 1e-8; }
    else {sv->SLHsl = SLHsl_new;}
    if (SLLj_new < 1e-8) { IIF_warn(__i, "SLLj < 1e-8, clamping to 1e-8"); sv->SLLj = 1e-8; }
    else {sv->SLLj = SLLj_new;}
    if (SLLsl_new < 1e-8) { IIF_warn(__i, "SLLsl < 1e-8, clamping to 1e-8"); sv->SLLsl = 1e-8; }
    else {sv->SLLsl = SLLsl_new;}
    if (SRB_new < 1e-8) { IIF_warn(__i, "SRB < 1e-8, clamping to 1e-8"); sv->SRB = 1e-8; }
    else {sv->SRB = SRB_new;}
    if (TnCHc_new < 1e-8) { IIF_warn(__i, "TnCHc < 1e-8, clamping to 1e-8"); sv->TnCHc = 1e-8; }
    else {sv->TnCHc = TnCHc_new;}
    if (TnCHm_new < 1e-8) { IIF_warn(__i, "TnCHm < 1e-8, clamping to 1e-8"); sv->TnCHm = 1e-8; }
    else {sv->TnCHm = TnCHm_new;}
    if (TnCL_new < 1e-8) { IIF_warn(__i, "TnCL < 1e-8, clamping to 1e-8"); sv->TnCL = 1e-8; }
    else {sv->TnCL = TnCL_new;}
    Vm = Vm_new;
    if (d_new < 0) { IIF_warn(__i, "d < 0, clamping to 0"); sv->d = 0; }
    else if (d_new > 1) { IIF_warn(__i, "d > 1, clamping to 1"); sv->d = 1; }
    else {sv->d = d_new;}
    if (f_new < 0) { IIF_warn(__i, "f < 0, clamping to 0"); sv->f = 0; }
    else if (f_new > 1.2) { IIF_warn(__i, "f > 1.2, clamping to 1.2"); sv->f = 1.2; }
    else {sv->f = f_new;}
    if (fcaBj_new < 0) { IIF_warn(__i, "fcaBj < 0, clamping to 0"); sv->fcaBj = 0; }
    else if (fcaBj_new > 1) { IIF_warn(__i, "fcaBj > 1, clamping to 1"); sv->fcaBj = 1; }
    else {sv->fcaBj = fcaBj_new;}
    if (fcaBsl_new < 0) { IIF_warn(__i, "fcaBsl < 0, clamping to 0"); sv->fcaBsl = 0; }
    else if (fcaBsl_new > 1) { IIF_warn(__i, "fcaBsl > 1, clamping to 1"); sv->fcaBsl = 1; }
    else {sv->fcaBsl = fcaBsl_new;}
    if (h_new < 0) { IIF_warn(__i, "h < 0, clamping to 0"); sv->h = 0; }
    else if (h_new > 1) { IIF_warn(__i, "h > 1, clamping to 1"); sv->h = 1; }
    else {sv->h = h_new;}
    if (hl_new < 0) { IIF_warn(__i, "hl < 0, clamping to 0"); sv->hl = 0; }
    else if (hl_new > 1) { IIF_warn(__i, "hl > 1, clamping to 1"); sv->hl = 1; }
    else {sv->hl = hl_new;}
    if (j_new < 0) { IIF_warn(__i, "j < 0, clamping to 0"); sv->j = 0; }
    else if (j_new > 1) { IIF_warn(__i, "j > 1, clamping to 1"); sv->j = 1; }
    else {sv->j = j_new;}
    if (m_new < 0) { IIF_warn(__i, "m < 0, clamping to 0"); sv->m = 0; }
    else if (m_new > 1) { IIF_warn(__i, "m > 1, clamping to 1"); sv->m = 1; }
    else {sv->m = m_new;}
    if (ml_new < 0) { IIF_warn(__i, "ml < 0, clamping to 0"); sv->ml = 0; }
    else if (ml_new > 1) { IIF_warn(__i, "ml > 1, clamping to 1"); sv->ml = 1; }
    else {sv->ml = ml_new;}
    if (rkur_new < 0) { IIF_warn(__i, "rkur < 0, clamping to 0"); sv->rkur = 0; }
    else if (rkur_new > 1) { IIF_warn(__i, "rkur > 1, clamping to 1"); sv->rkur = 1; }
    else {sv->rkur = rkur_new;}
    if (skur_new < 0) { IIF_warn(__i, "skur < 0, clamping to 0"); sv->skur = 0; }
    else if (skur_new > 1) { IIF_warn(__i, "skur > 1, clamping to 1"); sv->skur = 1; }
    else {sv->skur = skur_new;}
    if (xkr_new < 0) { IIF_warn(__i, "xkr < 0, clamping to 0"); sv->xkr = 0; }
    else if (xkr_new > 1) { IIF_warn(__i, "xkr > 1, clamping to 1"); sv->xkr = 1; }
    else {sv->xkr = xkr_new;}
    if (xks_new < 0) { IIF_warn(__i, "xks < 0, clamping to 0"); sv->xks = 0; }
    else if (xks_new > 1) { IIF_warn(__i, "xks > 1, clamping to 1"); sv->xks = 1; }
    else {sv->xks = xks_new;}
    if (xtof_new < 0) { IIF_warn(__i, "xtof < 0, clamping to 0"); sv->xtof = 0; }
    else if (xtof_new > 1) { IIF_warn(__i, "xtof > 1, clamping to 1"); sv->xtof = 1; }
    else {sv->xtof = xtof_new;}
    if (ytof_new < 0) { IIF_warn(__i, "ytof < 0, clamping to 0"); sv->ytof = 0; }
    else if (ytof_new > 1) { IIF_warn(__i, "ytof > 1, clamping to 1"); sv->ytof = 1; }
    else {sv->ytof = ytof_new;}
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Vm_ext[__i] = Vm;

  }


}


void trace_GrandiPanditVoigt(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("GrandiPanditVoigt_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->Cai\n"
        "sv->Caj\n"
        "sv->Casl\n"
        "ICa\n"
        "ICap\n"
        "IClCa\n"
        "IKi\n"
        "IKp\n"
        "IKr\n"
        "IKs\n"
        "IKur\n"
        "INa\n"
        "INaCa\n"
        "INaK\n"
        "INaL\n"
        "IbCa\n"
        "IbCl\n"
        "IbNa\n"
        "Ito\n"
        "Vm\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  GrandiPanditVoigt_Params *p  = (GrandiPanditVoigt_Params *)IF->params;

  GrandiPanditVoigt_state *sv_base = (GrandiPanditVoigt_state *)IF->sv_tab.y;
  GrandiPanditVoigt_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t GNa = (7.8*(1.-((0.1*p->AF))));
  GlobalData_t GNaL = (0.0025*p->AF);
  GlobalData_t Gkur = ((((1.0-((0.5*p->AF)))*(1.+(2.*p->ISO)))*0.045)*(1.+(0.2*p->RA)));
  GlobalData_t GtoFast = ((1.0-((0.7*p->AF)))*0.165);
  GlobalData_t IbarNCX = (((1.+(0.4*p->AF))*0.7)*4.5);
  GlobalData_t K_SRleak = ((p->factor_J_SRleak*(1.+(0.25*p->AF)))*5.348e-6);
  GlobalData_t KmNaip = (11.*(1.-((0.25*p->ISO))));
  GlobalData_t Kmf = ((2.5-((1.25*p->ISO)))*0.246e-3);
  GlobalData_t Vmax_SRCaP = (p->factor_Vmax_SRCaP*5.3114e-3);
  GlobalData_t gks_junc = ((p->markov_IKs==0.) ? (((1.+p->AF)+(2.*p->ISO))*0.0035) : 0.0065);
  GlobalData_t gks_sl = ((p->markov_IKs==0.) ? (((1.+p->AF)+(2.*p->ISO))*0.0035) : 0.0065);
  GlobalData_t hl_rush_larsen_B = (exp(((-dt)/tau_hl)));
  GlobalData_t hl_rush_larsen_C = (expm1(((-dt)/tau_hl)));
  GlobalData_t koCa = ((10.+(20.*p->AF))+((10.*p->ISO)*(1.-(p->AF))));
  GlobalData_t koff_tncl = ((1.+(0.5*p->ISO))*19.6e-3);
  GlobalData_t ks = (p->factor_J_SRCarel*25.);
  GlobalData_t pCa = ((((p->factor_GCaL*(1.+(0.5*p->ISO)))*(1.-((0.5*p->AF))))*0.50)*5.4e-4);
  GlobalData_t pK = ((((p->factor_GCaL*(1.+(0.5*p->ISO)))*(1.-((0.5*p->AF))))*0.50)*2.7e-7);
  GlobalData_t pNa = ((((p->factor_GCaL*(1.+(0.5*p->ISO)))*(1.-((0.5*p->AF))))*0.50)*1.5e-8);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vm_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t Vm = Vm_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t ICap_junc = ((((Fjunc*(pow(Q10SLCaP,Qpow)))*IbarSLCaP)*(pow(sv->Caj,1.6)))/((pow(KmPCa,1.6))+(pow(sv->Caj,1.6))));
  GlobalData_t ICap_sl = ((((Fsl*(pow(Q10SLCaP,Qpow)))*IbarSLCaP)*(pow(sv->Casl,1.6)))/((pow(KmPCa,1.6))+(pow(sv->Casl,1.6))));
  GlobalData_t IClCa_junc = (((Fjunc*GClCa)/(1.+(KdClCa/sv->Caj)))*(Vm-(ecl)));
  GlobalData_t IClCa_sl = (((Fsl*GClCa)/(1.+(KdClCa/sv->Casl)))*(Vm-(ecl)));
  GlobalData_t IbCl = (GClB*(Vm-(ecl)));
  GlobalData_t Ka_junc = (1./(1.+((Kdact/sv->Caj)*(Kdact/sv->Caj))));
  GlobalData_t Ka_sl = (1./(1.+((Kdact/sv->Casl)*(Kdact/sv->Casl))));
  GlobalData_t O2 = (1.-((((((((((((((((sv->C1+sv->C2)+sv->C3)+sv->C4)+sv->C5)+sv->C6)+sv->C7)+sv->C8)+sv->C9)+sv->C10)+sv->C11)+sv->C12)+sv->C13)+sv->C14)+sv->C15)+sv->O1)));
  GlobalData_t eca_junc = (((1./FoRT)/2.)*(log((Cao/sv->Caj))));
  GlobalData_t eca_sl = (((1./FoRT)/2.)*(log((Cao/sv->Casl))));
  GlobalData_t ek = ((1./FoRT)*(log((Ko/sv->Ki))));
  GlobalData_t eks = ((1./FoRT)*(log(((Ko+(pNaK*Nao))/(sv->Ki+(pNaK*sv->Nai))))));
  GlobalData_t ena_junc = ((1./FoRT)*(log((Nao/sv->Naj))));
  GlobalData_t ena_sl = ((1./FoRT)*(log((Nao/sv->Nasl))));
  GlobalData_t fcaCaMSL = ((p->usefca==1.) ? (0.1/(1.+(0.01/sv->Casl))) : 0.);
  GlobalData_t fcaCaj = ((p->usefca==1.) ? (0.1/(1.+(0.01/sv->Caj))) : 0.);
  GlobalData_t fnak = (1./((1.+(0.1245*(exp(((-0.1*Vm)*FoRT)))))+((0.0365*sigma)*(exp(((-Vm)*FoRT))))));
  GlobalData_t ibarca_j = ((((pCa*4.)*((Vm*Frdy)*FoRT))*(((0.341*sv->Caj)*(exp(((2.*Vm)*FoRT))))-((0.341*Cao))))/((exp(((2.*Vm)*FoRT)))-(1.)));
  GlobalData_t ibarca_sl = ((((pCa*4.)*((Vm*Frdy)*FoRT))*(((0.341*sv->Casl)*(exp(((2.*Vm)*FoRT))))-((0.341*Cao))))/((exp(((2.*Vm)*FoRT)))-(1.)));
  GlobalData_t kp_kp = (1./(1.+(exp((7.488-((Vm/5.98)))))));
  GlobalData_t rkr = (1./(1.+(exp(((Vm+74.)/24.)))));
  GlobalData_t s1_junc = (((((exp(((nu*Vm)*FoRT)))*sv->Naj)*sv->Naj)*sv->Naj)*Cao);
  GlobalData_t s1_sl = (((((exp(((nu*Vm)*FoRT)))*sv->Nasl)*sv->Nasl)*sv->Nasl)*Cao);
  GlobalData_t s2_junc = (((((exp((((nu-(1.))*Vm)*FoRT)))*Nao)*Nao)*Nao)*sv->Caj);
  GlobalData_t s2_sl = (((((exp((((nu-(1.))*Vm)*FoRT)))*Nao)*Nao)*Nao)*sv->Casl);
  GlobalData_t s3_junc = ((((((((KmCai*Nao)*Nao)*Nao)*(1.+(((sv->Naj/KmNai)*(sv->Naj/KmNai))*(sv->Naj/KmNai))))+((((KmNao*KmNao)*KmNao)*sv->Caj)*(1.+(sv->Caj/KmCai))))+(((KmCao*sv->Naj)*sv->Naj)*sv->Naj))+(((sv->Naj*sv->Naj)*sv->Naj)*Cao))+(((Nao*Nao)*Nao)*sv->Caj));
  GlobalData_t s3_sl = ((((((((KmCai*Nao)*Nao)*Nao)*(1.+(((sv->Nasl/KmNai)*(sv->Nasl/KmNai))*(sv->Nasl/KmNai))))+((((KmNao*KmNao)*KmNao)*sv->Casl)*(1.+(sv->Casl/KmCai))))+(((KmCao*sv->Nasl)*sv->Nasl)*sv->Nasl))+(((sv->Nasl*sv->Nasl)*sv->Nasl)*Cao))+(((Nao*Nao)*Nao)*sv->Casl));
  GlobalData_t ICa_junc = ((((((Fjunc_CaL*ibarca_j)*sv->d)*sv->f)*((1.-(sv->fcaBj))+fcaCaj))*(pow(Q10CaL,Qpow)))*0.45);
  GlobalData_t ICa_sl = ((((((Fsl_CaL*ibarca_sl)*sv->d)*sv->f)*((1.-(sv->fcaBsl))+fcaCaMSL))*(pow(Q10CaL,Qpow)))*0.45);
  GlobalData_t ICap = (ICap_junc+ICap_sl);
  GlobalData_t IClCa = (IClCa_junc+IClCa_sl);
  GlobalData_t IKp_junc = (((Fjunc*gkp)*kp_kp)*(Vm-(ek)));
  GlobalData_t IKp_sl = (((Fsl*gkp)*kp_kp)*(Vm-(ek)));
  GlobalData_t IKr = (((gkr*sv->xkr)*rkr)*(Vm-(ek)));
  GlobalData_t IKs_junc = ((p->markov_IKs==0.) ? ((((Fjunc*gks_junc)*sv->xks)*sv->xks)*(Vm-(eks))) : (((Fjunc*gks_junc)*(sv->O1+O2))*(Vm-(eks))));
  GlobalData_t IKs_sl = ((p->markov_IKs==0.) ? ((((Fsl*gks_sl)*sv->xks)*sv->xks)*(Vm-(eks))) : (((Fsl*gks_sl)*(sv->O1+O2))*(Vm-(eks))));
  GlobalData_t IKur = (((Gkur*sv->rkur)*sv->skur)*(Vm-(ek)));
  GlobalData_t INaCa_junc = ((((((Fjunc*IbarNCX)*(pow(Q10NCX,Qpow)))*Ka_junc)*(s1_junc-(s2_junc)))/s3_junc)/(1.+(ksat*(exp((((nu-(1.))*Vm)*FoRT))))));
  GlobalData_t INaCa_sl = ((((((Fsl*IbarNCX)*(pow(Q10NCX,Qpow)))*Ka_sl)*(s1_sl-(s2_sl)))/s3_sl)/(1.+(ksat*(exp((((nu-(1.))*Vm)*FoRT))))));
  GlobalData_t INaK_junc = (((((Fjunc*IbarNaK)*fnak)*Ko)/(1.+((((KmNaip/sv->Naj)*(KmNaip/sv->Naj))*(KmNaip/sv->Naj))*(KmNaip/sv->Naj))))/(Ko+KmKo));
  GlobalData_t INaK_sl = (((((Fsl*IbarNaK)*fnak)*Ko)/(1.+((((KmNaip/sv->Nasl)*(KmNaip/sv->Nasl))*(KmNaip/sv->Nasl))*(KmNaip/sv->Nasl))))/(Ko+KmKo));
  GlobalData_t INaL_junc = ((((((Fjunc*GNaL)*sv->ml)*sv->ml)*sv->ml)*sv->hl)*(Vm-(ena_junc)));
  GlobalData_t INaL_sl = ((((((Fsl*GNaL)*sv->ml)*sv->ml)*sv->ml)*sv->hl)*(Vm-(ena_sl)));
  GlobalData_t INa_junc = (((((((Fjunc*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(Vm-(ena_junc)));
  GlobalData_t INa_sl = (((((((Fsl*GNa)*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(Vm-(ena_sl)));
  GlobalData_t IbCa_junc = ((Fjunc*GCaB)*(Vm-(eca_junc)));
  GlobalData_t IbCa_sl = ((Fsl*GCaB)*(Vm-(eca_sl)));
  GlobalData_t IbNa_junc = ((Fjunc*GNaB)*(Vm-(ena_junc)));
  GlobalData_t IbNa_sl = ((Fsl*GNaB)*(Vm-(ena_sl)));
  GlobalData_t Itof = (((GtoFast*sv->xtof)*sv->ytof)*(Vm-(ek)));
  GlobalData_t aki = (1.02/(1.+(exp((0.2385*((Vm-(ek))-(59.215)))))));
  GlobalData_t bki = (((0.49124*(exp((0.08032*((Vm+5.476)-(ek))))))+(exp((0.06175*((Vm-(ek))-(594.31))))))/(1.+(exp((-0.5143*((Vm-(ek))+4.753))))));
  GlobalData_t ICa = (ICa_junc+ICa_sl);
  GlobalData_t IKp = (IKp_junc+IKp_sl);
  GlobalData_t IKs = (IKs_junc+IKs_sl);
  GlobalData_t INa = (INa_junc+INa_sl);
  GlobalData_t INaCa = (INaCa_junc+INaCa_sl);
  GlobalData_t INaK = (INaK_junc+INaK_sl);
  GlobalData_t INaL = (INaL_junc+INaL_sl);
  GlobalData_t IbCa = (IbCa_junc+IbCa_sl);
  GlobalData_t IbNa = (IbNa_junc+IbNa_sl);
  GlobalData_t Ito = Itof;
  GlobalData_t kiss = (aki/(aki+bki));
  GlobalData_t IKi = ((((((p->factor_GK1*(1.+p->AF))*0.15)*0.35)*(sqrt((Ko/5.4))))*kiss)*(Vm-(ek)));
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->Cai);
  fprintf(file, "%4.12f\t", sv->Caj);
  fprintf(file, "%4.12f\t", sv->Casl);
  fprintf(file, "%4.12f\t", ICa);
  fprintf(file, "%4.12f\t", ICap);
  fprintf(file, "%4.12f\t", IClCa);
  fprintf(file, "%4.12f\t", IKi);
  fprintf(file, "%4.12f\t", IKp);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", IKur);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", INaL);
  fprintf(file, "%4.12f\t", IbCa);
  fprintf(file, "%4.12f\t", IbCl);
  fprintf(file, "%4.12f\t", IbNa);
  fprintf(file, "%4.12f\t", Ito);
  fprintf(file, "%4.12f\t", Vm);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        