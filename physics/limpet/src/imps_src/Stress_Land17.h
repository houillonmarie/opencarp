// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Title: A model of cardiac contraction based on novel measurements of tension development in human cardiomyocytes
*  Authors: Sander Land, So-Jin Park-Holohan, Nicolas P. Smith, Cristobal G. dos Remedios
*  Year: 2017
*  Journal: Journal of Molecular and Cellular Cardiology 2017;106:68-83
*  DOI: 10.1016/j.yjmcc.2017.03.008
*  Comment: The model implements a human contraction model (plugin) at 37 degrees
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __STRESS_LAND17_H__
#define __STRESS_LAND17_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Stress_Land17_REQDAT delLambda_DATA_FLAG|Lambda_DATA_FLAG
#define Stress_Land17_MODDAT Tension_DATA_FLAG

struct Stress_Land17_Params {
    GlobalData_t TOT_A;
    GlobalData_t TRPN_n;
    GlobalData_t Tref;
    GlobalData_t beta_0;
    GlobalData_t beta_1;
    GlobalData_t dr;
    GlobalData_t gamma;
    GlobalData_t gamma_wu;
    GlobalData_t koff;
    GlobalData_t ktm_unblock;
    GlobalData_t perm50;
    GlobalData_t phi;
    GlobalData_t wfrac;

};

struct Stress_Land17_state {
    GlobalData_t TRPN;
    GlobalData_t TmBlocked;
    GlobalData_t XS;
    GlobalData_t XW;
    GlobalData_t ZETAS;
    GlobalData_t ZETAW;
    GlobalData_t __Ca_i_local;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Stress_Land17(ION_IF *);
void construct_tables_Stress_Land17(ION_IF *);
void destroy_Stress_Land17(ION_IF *);
void initialize_sv_Stress_Land17(ION_IF *, GlobalData_t**);
void compute_Stress_Land17(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
