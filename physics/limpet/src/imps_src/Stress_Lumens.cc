// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Joost Lumens, Tammo Delhaas, Borut Kirn, Theo Arts
*  Year: 2009
*  Title: Three-wall segment (TriSeg) model describing mechanics and hemodynamics of ventricular interaction
*  Journal: Ann Biomed Eng. 2009;37:2234-55
*  DOI: 10.1007/s10439-009-9774-2
*  Comment: Plugin
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Stress_Lumens.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Stress_Lumens(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Stress_Lumens( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define C_init (GlobalData_t)(0.0)
#define Crest (GlobalData_t)(0.02)
#define Lsc0 (GlobalData_t)(1.51)
#define Lseiso (GlobalData_t)(0.04)
#define Lsref (GlobalData_t)(2.0)
#define Sact (GlobalData_t)(120.)
#define Spas (GlobalData_t)(7.)
#define V_init (GlobalData_t)(-84.)
#define delta_sl_init (GlobalData_t)(0.)
#define length_init (GlobalData_t)(1.)
#define tact_init (GlobalData_t)(-1.)
#define tauD (GlobalData_t)(32.)
#define tauR (GlobalData_t)(48.)
#define tauSC (GlobalData_t)(425.)
#define tcur_init (GlobalData_t)(0.)
#define vmax (GlobalData_t)(7.)
#define Lsc_init (GlobalData_t)(Lsc0)
#define Vmp_init (GlobalData_t)(V_init)



void initialize_params_Stress_Lumens( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Stress_Lumens_Params *p = (Stress_Lumens_Params *)IF->params;

  // Compute the regional constants
  {
    p->VmThresh = -50.;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Stress_Lumens( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Lumens_Params *p = (Stress_Lumens_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.

}



void    initialize_sv_Stress_Lumens( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Lumens_Params *p = (Stress_Lumens_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Stress_Lumens_state) );
  Stress_Lumens_state *sv_base = (Stress_Lumens_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *V_ext = impdata[Vm];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Stress_Lumens_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t V = V_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->C = C_init;
    sv->Lsc = Lsc_init;
    V = V_init;
    sv->Vmp = Vmp_init;
    delta_sl = delta_sl_init;
    length = length_init;
    sv->tact = tact_init;
    sv->tcur = tcur_init;
    double epsf = length;
    double Ls = (Lsref*epsf);
    Tension = ((((Sact*sv->C)*(sv->Lsc-(Lsc0)))*(Ls-(sv->Lsc)))/Lseiso);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    V_ext[__i] = V;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Stress_Lumens(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Lumens_Params *p  = (Stress_Lumens_Params *)IF->params;
  Stress_Lumens_state *sv_base = (Stress_Lumens_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *V_ext = impdata[Vm];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Stress_Lumens_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t V = V_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t Vmp_new = V;
    delta_sl = (sv->Lsc/Lsref);
    GlobalData_t epsf = length;
    GlobalData_t tact_new = (((sv->Vmp<=p->VmThresh)&&(V>p->VmThresh)) ? sv->tcur : sv->tact);
    GlobalData_t tcur_new = (sv->tcur+dt);
    GlobalData_t Ls = (Lsref*epsf);
    Tension = ((((Sact*sv->C)*(sv->Lsc-(Lsc0)))*(Ls-(sv->Lsc)))/Lseiso);
    
    
    //Complete Forward Euler Update
    GlobalData_t Clen = (tanh(((4.*(sv->Lsc-(Lsc0)))*(sv->Lsc-(Lsc0)))));
    GlobalData_t TLsc = (tauSC*(0.29+(0.3*sv->Lsc)));
    GlobalData_t _tR = (t/tauR);
    GlobalData_t diff_Lsc = ((((Ls-(sv->Lsc))/Lseiso)-(1.))*vmax);
    GlobalData_t dtAct = ((sv->tact<0.) ? 0. : (t-(sv->tact)));
    GlobalData_t maxF = ((_tR>0.) ? _tR : 0.);
    GlobalData_t x = ((maxF<8.) ? maxF : 8.);
    GlobalData_t Frise = ((((((0.02*x)*x)*x)*(8.-(x)))*(8.-(x)))*(exp((-x))));
    GlobalData_t diff_C = ((((1./tauR)*Clen)*Frise)+(((1./tauD)*(Crest-(sv->C)))/(1.+(exp(((TLsc-(dtAct))/tauD))))));
    GlobalData_t C_new = sv->C+diff_C*dt;
    GlobalData_t Lsc_new = sv->Lsc+diff_Lsc*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    if (C_new < 0) { IIF_warn(__i, "C < 0, clamping to 0"); sv->C = 0; }
    else {sv->C = C_new;}
    if (Lsc_new < 0) { IIF_warn(__i, "Lsc < 0, clamping to 0"); sv->Lsc = 0; }
    else {sv->Lsc = Lsc_new;}
    Tension = Tension;
    sv->Vmp = Vmp_new;
    delta_sl = delta_sl;
    sv->tact = tact_new;
    sv->tcur = tcur_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    V_ext[__i] = V;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        