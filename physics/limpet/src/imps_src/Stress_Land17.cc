// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Title: A model of cardiac contraction based on novel measurements of tension development in human cardiomyocytes
*  Authors: Sander Land, So-Jin Park-Holohan, Nicolas P. Smith, Cristobal G. dos Remedios
*  Year: 2017
*  Journal: Journal of Molecular and Cellular Cardiology 2017;106:68-83
*  DOI: 10.1016/j.yjmcc.2017.03.008
*  Comment: The model implements a human contraction model (plugin) at 37 degrees
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Stress_Land17.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Stress_Land17(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Stress_Land17( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define TRPN_init (GlobalData_t)(0.)
#define TmBlocked_init (GlobalData_t)(0.)
#define XS_init (GlobalData_t)(0.)
#define XW_init (GlobalData_t)(0.)
#define ZETAS_init (GlobalData_t)(0.)
#define ZETAW_init (GlobalData_t)(0.)
#define ca50 (GlobalData_t)(0.805)
#define delta_sl_init (GlobalData_t)(0.)
#define length_init (GlobalData_t)(1.)
#define mu (GlobalData_t)(3.)
#define nperm (GlobalData_t)(5.)
#define nu (GlobalData_t)(7.)
#define partial_A_del_ZETAS (GlobalData_t)(0.)
#define partial_A_del_ZETAW (GlobalData_t)(0.)
#define partial_TOT_A_del_ZETAS (GlobalData_t)(0.)
#define partial_TOT_A_del_ZETAW (GlobalData_t)(0.)
#define partial_TRPN_n_del_TRPN (GlobalData_t)(0.)
#define partial_XSSS_del_TmBlocked (GlobalData_t)(0.)
#define partial_XU_del_TmBlocked (GlobalData_t)(-1.)
#define partial_XU_del_XW (GlobalData_t)(-1.)
#define partial_XWSS_del_TmBlocked (GlobalData_t)(0.)
#define partial_beta_1_del_TRPN (GlobalData_t)(0.)
#define partial_ca50__del_TRPN (GlobalData_t)(0.)
#define partial_ca50_del_TRPN (GlobalData_t)(0.)
#define partial_cds_del_ZETAS (GlobalData_t)(0.)
#define partial_cdw_del_ZETAW (GlobalData_t)(0.)
#define partial_dlambdadt_del_ZETAS (GlobalData_t)(0.)
#define partial_dlambdadt_del_ZETAW (GlobalData_t)(0.)
#define partial_dr_del_TmBlocked (GlobalData_t)(0.)
#define partial_dr_del_XS (GlobalData_t)(0.)
#define partial_dr_del_ZETAS (GlobalData_t)(0.)
#define partial_dr_del_ZETAW (GlobalData_t)(0.)
#define partial_gamma_del_XS (GlobalData_t)(0.)
#define partial_gamma_rate_del_XS (GlobalData_t)(0.)
#define partial_gamma_rate_w_del_XW (GlobalData_t)(0.)
#define partial_gamma_wu_del_XW (GlobalData_t)(0.)
#define partial_gr_w__del_XW (GlobalData_t)(0.)
#define partial_k_su_del_XS (GlobalData_t)(0.)
#define partial_k_uw_del_XW (GlobalData_t)(0.)
#define partial_k_uw_del_ZETAW (GlobalData_t)(0.)
#define partial_k_ws_del_XS (GlobalData_t)(0.)
#define partial_k_ws_del_XW (GlobalData_t)(0.)
#define partial_k_ws_del_ZETAS (GlobalData_t)(0.)
#define partial_k_wu_del_XW (GlobalData_t)(0.)
#define partial_koff_del_TRPN (GlobalData_t)(0.)
#define partial_ktm_block_del_TmBlocked (GlobalData_t)(0.)
#define partial_ktm_unblock_del_TmBlocked (GlobalData_t)(0.)
#define partial_lambda_del_TRPN (GlobalData_t)(0.)
#define partial_lambda_m_del_TRPN (GlobalData_t)(0.)
#define partial_mu_del_XS (GlobalData_t)(0.)
#define partial_mu_del_XW (GlobalData_t)(0.)
#define partial_mu_del_ZETAS (GlobalData_t)(0.)
#define partial_nperm_del_TmBlocked (GlobalData_t)(0.)
#define partial_nu_del_XW (GlobalData_t)(0.)
#define partial_nu_del_ZETAW (GlobalData_t)(0.)
#define partial_perm50_del_TmBlocked (GlobalData_t)(0.)
#define partial_phi_del_ZETAS (GlobalData_t)(0.)
#define partial_phi_del_ZETAW (GlobalData_t)(0.)
#define partial_trpn_np__del_TmBlocked (GlobalData_t)(0.)
#define partial_trpn_np_del_TmBlocked (GlobalData_t)(0.)
#define partial_wfrac_del_TmBlocked (GlobalData_t)(0.)
#define partial_wfrac_del_XS (GlobalData_t)(0.)
#define partial_wfrac_del_XW (GlobalData_t)(0.)
#define partial_wfrac_del_ZETAS (GlobalData_t)(0.)
#define partial_wfrac_del_ZETAW (GlobalData_t)(0.)
#define partial_xb_ws_del_XS (GlobalData_t)(0.)
#define partial_zs__del_XS (GlobalData_t)(0.)
#define partial_zs_neg_del_XS (GlobalData_t)(0.)
#define partial_zs_pos_del_XS (GlobalData_t)(0.)
#define set_TRPN_tozero_in_TRPN_n (GlobalData_t)(2.)
#define set_TRPN_tozero_in_beta_1 (GlobalData_t)(-2.4)
#define set_TRPN_tozero_in_ca50 (GlobalData_t)(0.805)
#define set_TRPN_tozero_in_koff (GlobalData_t)(0.1)
#define set_TmBlocked_tozero_in_XSSS (GlobalData_t)((0.25*0.5))
#define set_TmBlocked_tozero_in_XWSS (GlobalData_t)((((1.-(0.25))*0.5)*0.5))
#define set_TmBlocked_tozero_in_dr (GlobalData_t)(0.25)
#define set_TmBlocked_tozero_in_ktm_block (GlobalData_t)((((pow(0.35,5.))*0.5)/((0.5-((0.25*0.5)))-((((1.-(0.25))*0.5)*0.5)))))
#define set_TmBlocked_tozero_in_ktm_unblock (GlobalData_t)(1.)
#define set_TmBlocked_tozero_in_nperm (GlobalData_t)(5.)
#define set_TmBlocked_tozero_in_perm50 (GlobalData_t)(0.35)
#define set_TmBlocked_tozero_in_wfrac (GlobalData_t)(0.5)
#define set_XS_tozero_in_dr (GlobalData_t)(0.25)
#define set_XS_tozero_in_gamma (GlobalData_t)(0.0085)
#define set_XS_tozero_in_k_su (GlobalData_t)((((0.004*3.)*((1./0.25)-(1.)))*0.5))
#define set_XS_tozero_in_k_ws (GlobalData_t)((0.004*3.))
#define set_XS_tozero_in_mu (GlobalData_t)(3.)
#define set_XS_tozero_in_wfrac (GlobalData_t)(0.5)
#define set_XS_tozero_in_xb_su (GlobalData_t)(0.)
#define set_XS_tozero_in_xb_su_gamma (GlobalData_t)(0.)
#define set_XW_tozero_in_gamma_wu (GlobalData_t)(0.615)
#define set_XW_tozero_in_k_uw (GlobalData_t)((0.026*7.))
#define set_XW_tozero_in_k_ws (GlobalData_t)((0.004*3.))
#define set_XW_tozero_in_k_wu (GlobalData_t)((((0.026*7.)*((1./0.5)-(1.)))-((0.004*3.))))
#define set_XW_tozero_in_mu (GlobalData_t)(3.)
#define set_XW_tozero_in_nu (GlobalData_t)(7.)
#define set_XW_tozero_in_wfrac (GlobalData_t)(0.5)
#define set_XW_tozero_in_xb_ws (GlobalData_t)(0.)
#define set_XW_tozero_in_xb_wu (GlobalData_t)(0.)
#define set_XW_tozero_in_xb_wu_gamma (GlobalData_t)(0.)
#define set_ZETAS_tozero_in_A (GlobalData_t)((((0.25*25.)/(((1.-(0.25))*0.5)+0.25))*(0.25/0.25)))
#define set_ZETAS_tozero_in_TOT_A (GlobalData_t)(25.)
#define set_ZETAS_tozero_in_cds (GlobalData_t)(((((2.23*(0.004*3.))*(1.-(0.25)))*0.5)/0.25))
#define set_ZETAS_tozero_in_dr (GlobalData_t)(0.25)
#define set_ZETAS_tozero_in_k_ws (GlobalData_t)((0.004*3.))
#define set_ZETAS_tozero_in_mu (GlobalData_t)(3.)
#define set_ZETAS_tozero_in_phi (GlobalData_t)(2.23)
#define set_ZETAS_tozero_in_wfrac (GlobalData_t)(0.5)
#define set_ZETAW_tozero_in_A (GlobalData_t)((((0.25*25.)/(((1.-(0.25))*0.5)+0.25))*(0.25/0.25)))
#define set_ZETAW_tozero_in_TOT_A (GlobalData_t)(25.)
#define set_ZETAW_tozero_in_cdw (GlobalData_t)(((((2.23*(0.026*7.))*(1.-(0.25)))*(1.-(0.5)))/((1.-(0.25))*0.5)))
#define set_ZETAW_tozero_in_dr (GlobalData_t)(0.25)
#define set_ZETAW_tozero_in_k_uw (GlobalData_t)((0.026*7.))
#define set_ZETAW_tozero_in_nu (GlobalData_t)(7.)
#define set_ZETAW_tozero_in_phi (GlobalData_t)(2.23)
#define set_ZETAW_tozero_in_wfrac (GlobalData_t)(0.5)
#define k_uw (GlobalData_t)((0.026*nu))
#define k_ws (GlobalData_t)((0.004*mu))
#define partial_xb_uw_del_XW (GlobalData_t)((k_uw*-1.))
#define partial_xb_ws_del_XW (GlobalData_t)(k_ws)



void initialize_params_Stress_Land17( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Stress_Land17_Params *p = (Stress_Land17_Params *)IF->params;

  // Compute the regional constants
  {
    p->TOT_A = 25.;
    p->TRPN_n = 2.;
    p->Tref = 120.;
    p->beta_0 = 2.3;
    p->beta_1 = -2.4;
    p->dr = 0.25;
    p->gamma = 0.0085;
    p->gamma_wu = 0.615;
    p->koff = 0.1;
    p->ktm_unblock = 1.;
    p->perm50 = 0.35;
    p->phi = 2.23;
    p->wfrac = 0.5;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_Stress_Land17;

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Stress_Land17( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Land17_Params *p = (Stress_Land17_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double A = (((0.25*p->TOT_A)/(((1.-(p->dr))*p->wfrac)+p->dr))*(p->dr/0.25));
  double XSSS = (p->dr*0.5);
  double XWSS = (((1.-(p->dr))*p->wfrac)*0.5);
  double cds = ((((p->phi*k_ws)*(1.-(p->dr)))*p->wfrac)/p->dr);
  double cdw = ((((p->phi*k_uw)*(1.-(p->dr)))*(1.-(p->wfrac)))/((1.-(p->dr))*p->wfrac));
  double k_su = ((k_ws*((1./p->dr)-(1.)))*p->wfrac);
  double k_wu = ((k_uw*((1./p->wfrac)-(1.)))-(k_ws));
  double ktm_block = (((p->ktm_unblock*(pow(p->perm50,nperm)))*0.5)/((0.5-(XSSS))-(XWSS)));
  double partial_diff_ZETAS_del_ZETAS = (-cds);
  double partial_diff_ZETAW_del_ZETAW = (-cdw);
  double partial_xb_su_del_XS = k_su;
  double partial_xb_wu_del_XW = k_wu;
  double ZETAS_rush_larsen_B = (exp((dt*partial_diff_ZETAS_del_ZETAS)));
  double ZETAW_rush_larsen_B = (exp((dt*partial_diff_ZETAW_del_ZETAW)));

}



void    initialize_sv_Stress_Land17( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Land17_Params *p = (Stress_Land17_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Stress_Land17_state) );
  Stress_Land17_state *sv_base = (Stress_Land17_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double A = (((0.25*p->TOT_A)/(((1.-(p->dr))*p->wfrac)+p->dr))*(p->dr/0.25));
  double XSSS = (p->dr*0.5);
  double XWSS = (((1.-(p->dr))*p->wfrac)*0.5);
  double cds = ((((p->phi*k_ws)*(1.-(p->dr)))*p->wfrac)/p->dr);
  double cdw = ((((p->phi*k_uw)*(1.-(p->dr)))*(1.-(p->wfrac)))/((1.-(p->dr))*p->wfrac));
  double k_su = ((k_ws*((1./p->dr)-(1.)))*p->wfrac);
  double k_wu = ((k_uw*((1./p->wfrac)-(1.)))-(k_ws));
  double ktm_block = (((p->ktm_unblock*(pow(p->perm50,nperm)))*0.5)/((0.5-(XSSS))-(XWSS)));
  double partial_diff_ZETAS_del_ZETAS = (-cds);
  double partial_diff_ZETAW_del_ZETAW = (-cdw);
  double partial_xb_su_del_XS = k_su;
  double partial_xb_wu_del_XW = k_wu;
  double ZETAS_rush_larsen_B = (exp((dt*partial_diff_ZETAS_del_ZETAS)));
  double ZETAW_rush_larsen_B = (exp((dt*partial_diff_ZETAW_del_ZETAW)));
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Ca_i_sizeof;
  int __Ca_i_offset;
  SVgetfcn __Ca_i_SVgetfcn = get_sv_offset( IF->parent->type, "Ca_i", &__Ca_i_offset, &__Ca_i_sizeof );
  SVputfcn __Ca_i_SVputfcn = __Ca_i_SVgetfcn ? getPutSV(__Ca_i_SVgetfcn) : NULL;

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Stress_Land17_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Ca_i = __Ca_i_SVgetfcn ? __Ca_i_SVgetfcn(IF->parent, __i, __Ca_i_offset) :sv->__Ca_i_local;
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->TRPN = TRPN_init;
    sv->TmBlocked = TmBlocked_init;
    sv->XS = XS_init;
    sv->XW = XW_init;
    sv->ZETAS = ZETAS_init;
    sv->ZETAW = ZETAW_init;
    delta_sl = delta_sl_init;
    length = length_init;
    double lambda = length;
    double lambda_m = ((lambda>1.2) ? 1.2 : lambda);
    double lambda_s = ((lambda_m<0.87) ? 0.87 : lambda_m);
    double Ta = ((lambda_s*(p->Tref/p->dr))*(((sv->ZETAS+1.)*sv->XS)+(sv->ZETAW*sv->XW)));
    Tension = Ta;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Ca_i_SVputfcn ) 
    	__Ca_i_SVputfcn(IF->parent, __i, __Ca_i_offset, Ca_i);
    else
    	sv->__Ca_i_local=Ca_i;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Stress_Land17(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Land17_Params *p  = (Stress_Land17_Params *)IF->params;
  Stress_Land17_state *sv_base = (Stress_Land17_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t A = (((0.25*p->TOT_A)/(((1.-(p->dr))*p->wfrac)+p->dr))*(p->dr/0.25));
  GlobalData_t XSSS = (p->dr*0.5);
  GlobalData_t XWSS = (((1.-(p->dr))*p->wfrac)*0.5);
  GlobalData_t cds = ((((p->phi*k_ws)*(1.-(p->dr)))*p->wfrac)/p->dr);
  GlobalData_t cdw = ((((p->phi*k_uw)*(1.-(p->dr)))*(1.-(p->wfrac)))/((1.-(p->dr))*p->wfrac));
  GlobalData_t k_su = ((k_ws*((1./p->dr)-(1.)))*p->wfrac);
  GlobalData_t k_wu = ((k_uw*((1./p->wfrac)-(1.)))-(k_ws));
  GlobalData_t ktm_block = (((p->ktm_unblock*(pow(p->perm50,nperm)))*0.5)/((0.5-(XSSS))-(XWSS)));
  GlobalData_t partial_diff_ZETAS_del_ZETAS = (-cds);
  GlobalData_t partial_diff_ZETAW_del_ZETAW = (-cdw);
  GlobalData_t partial_xb_su_del_XS = k_su;
  GlobalData_t partial_xb_wu_del_XW = k_wu;
  GlobalData_t ZETAS_rush_larsen_B = (exp((dt*partial_diff_ZETAS_del_ZETAS)));
  GlobalData_t ZETAW_rush_larsen_B = (exp((dt*partial_diff_ZETAW_del_ZETAW)));
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Ca_i_sizeof;
  int __Ca_i_offset;
  SVgetfcn __Ca_i_SVgetfcn = get_sv_offset( IF->parent->type, "Ca_i", &__Ca_i_offset, &__Ca_i_sizeof );
  SVputfcn __Ca_i_SVputfcn = __Ca_i_SVgetfcn ? getPutSV(__Ca_i_SVgetfcn) : NULL;

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Stress_Land17_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Ca_i = __Ca_i_SVgetfcn ? __Ca_i_SVgetfcn(IF->parent, __i, __Ca_i_offset) :sv->__Ca_i_local;
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t lambda = length;
    GlobalData_t lambda_m = ((lambda>1.2) ? 1.2 : lambda);
    GlobalData_t lambda_s = ((lambda_m<0.87) ? 0.87 : lambda_m);
    GlobalData_t Ta = ((lambda_s*(p->Tref/p->dr))*(((sv->ZETAS+1.)*sv->XS)+(sv->ZETAW*sv->XW)));
    Tension = Ta;
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    GlobalData_t ca50_ = (ca50+(p->beta_1*(lambda_m-(1.))));
    GlobalData_t gr_w_ = ((sv->ZETAW<0.) ? (-sv->ZETAW) : sv->ZETAW);
    GlobalData_t set_TRPN_tozero_in_diff_TRPN = (0.1*(pow((Ca_i/(0.805+(-2.4*(((length>1.2) ? 1.2 : length)-(1.))))),2.)));
    GlobalData_t set_TmBlocked_tozero_in_diff_TmBlocked = (((((pow(0.35,5.))*0.5)/((0.5-((0.25*0.5)))-((((1.-(0.25))*0.5)*0.5))))*(((pow(sv->TRPN,(-5./2.)))>100.) ? 100. : (pow(sv->TRPN,(-5./2.)))))*((1.-(sv->XW))-(sv->XS)));
    GlobalData_t set_XS_tozero_in_diff_XS = ((0.004*3.)*sv->XW);
    GlobalData_t set_XW_tozero_in_diff_XW = ((0.026*7.)*((1.-(sv->TmBlocked))-(sv->XS)));
    GlobalData_t set_ZETAS_tozero_in_diff_ZETAS = ((((0.25*25.)/(((1.-(0.25))*0.5)+0.25))*(0.25/0.25))*delta_sl);
    GlobalData_t set_ZETAW_tozero_in_diff_ZETAW = ((((0.25*25.)/(((1.-(0.25))*0.5)+0.25))*(0.25/0.25))*delta_sl);
    GlobalData_t trpn_np_ = (pow(sv->TRPN,((-nperm)/2.)));
    GlobalData_t zs_neg = ((sv->ZETAS<-1.) ? ((-sv->ZETAS)-(1.)) : 0.);
    GlobalData_t zs_pos = ((sv->ZETAS>0.) ? sv->ZETAS : 0.);
    GlobalData_t ZETAS_rush_larsen_A = ((set_ZETAS_tozero_in_diff_ZETAS/partial_diff_ZETAS_del_ZETAS)*(expm1((dt*partial_diff_ZETAS_del_ZETAS))));
    GlobalData_t ZETAW_rush_larsen_A = ((set_ZETAW_tozero_in_diff_ZETAW/partial_diff_ZETAW_del_ZETAW)*(expm1((dt*partial_diff_ZETAW_del_ZETAW))));
    GlobalData_t gamma_rate_w = (p->gamma_wu*gr_w_);
    GlobalData_t partial_diff_TRPN_del_TRPN = (p->koff*(((pow((Ca_i/ca50_),p->TRPN_n))*-1.)-(1.)));
    GlobalData_t trpn_np = ((trpn_np_>100.) ? 100. : trpn_np_);
    GlobalData_t zs_ = ((zs_pos>zs_neg) ? zs_pos : zs_neg);
    GlobalData_t TRPN_rush_larsen_A = ((set_TRPN_tozero_in_diff_TRPN/partial_diff_TRPN_del_TRPN)*(expm1((dt*partial_diff_TRPN_del_TRPN))));
    GlobalData_t TRPN_rush_larsen_B = (exp((dt*partial_diff_TRPN_del_TRPN)));
    GlobalData_t gamma_rate = (p->gamma*zs_);
    GlobalData_t partial_diff_TmBlocked_del_TmBlocked = (((ktm_block*trpn_np)*-1.)-((p->ktm_unblock*(pow(sv->TRPN,(nperm/2.))))));
    GlobalData_t partial_diff_XW_del_XW = ((((k_uw*-1.)-(k_wu))-(k_ws))-(gamma_rate_w));
    GlobalData_t TmBlocked_rush_larsen_A = ((set_TmBlocked_tozero_in_diff_TmBlocked/partial_diff_TmBlocked_del_TmBlocked)*(expm1((dt*partial_diff_TmBlocked_del_TmBlocked))));
    GlobalData_t TmBlocked_rush_larsen_B = (exp((dt*partial_diff_TmBlocked_del_TmBlocked)));
    GlobalData_t XW_rush_larsen_A = ((set_XW_tozero_in_diff_XW/partial_diff_XW_del_XW)*(expm1((dt*partial_diff_XW_del_XW))));
    GlobalData_t XW_rush_larsen_B = (exp((dt*partial_diff_XW_del_XW)));
    GlobalData_t partial_diff_XS_del_XS = ((-k_su)-(gamma_rate));
    GlobalData_t XS_rush_larsen_A = ((set_XS_tozero_in_diff_XS/partial_diff_XS_del_XS)*(expm1((dt*partial_diff_XS_del_XS))));
    GlobalData_t XS_rush_larsen_B = (exp((dt*partial_diff_XS_del_XS)));
    GlobalData_t TRPN_new = TRPN_rush_larsen_A+TRPN_rush_larsen_B*sv->TRPN;
    GlobalData_t TmBlocked_new = TmBlocked_rush_larsen_A+TmBlocked_rush_larsen_B*sv->TmBlocked;
    GlobalData_t XS_new = XS_rush_larsen_A+XS_rush_larsen_B*sv->XS;
    GlobalData_t XW_new = XW_rush_larsen_A+XW_rush_larsen_B*sv->XW;
    GlobalData_t ZETAS_new = ZETAS_rush_larsen_A+ZETAS_rush_larsen_B*sv->ZETAS;
    GlobalData_t ZETAW_new = ZETAW_rush_larsen_A+ZETAW_rush_larsen_B*sv->ZETAW;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->TRPN = TRPN_new;
    Tension = Tension;
    sv->TmBlocked = TmBlocked_new;
    sv->XS = XS_new;
    sv->XW = XW_new;
    sv->ZETAS = ZETAS_new;
    sv->ZETAW = ZETAW_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Ca_i_SVputfcn ) 
    	__Ca_i_SVputfcn(IF->parent, __i, __Ca_i_offset, Ca_i);
    else
    	sv->__Ca_i_local=Ca_i;

  }


}


void trace_Stress_Land17(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Stress_Land17_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->TRPN\n"
        "sv->TmBlocked\n"
        "sv->XS\n"
        "sv->XW\n"
        "sv->ZETAS\n"
        "sv->ZETAW\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Land17_Params *p  = (Stress_Land17_Params *)IF->params;

  Stress_Land17_state *sv_base = (Stress_Land17_state *)IF->sv_tab.y;
  Stress_Land17_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t A = (((0.25*p->TOT_A)/(((1.-(p->dr))*p->wfrac)+p->dr))*(p->dr/0.25));
  GlobalData_t XSSS = (p->dr*0.5);
  GlobalData_t XWSS = (((1.-(p->dr))*p->wfrac)*0.5);
  GlobalData_t cds = ((((p->phi*k_ws)*(1.-(p->dr)))*p->wfrac)/p->dr);
  GlobalData_t cdw = ((((p->phi*k_uw)*(1.-(p->dr)))*(1.-(p->wfrac)))/((1.-(p->dr))*p->wfrac));
  GlobalData_t k_su = ((k_ws*((1./p->dr)-(1.)))*p->wfrac);
  GlobalData_t k_wu = ((k_uw*((1./p->wfrac)-(1.)))-(k_ws));
  GlobalData_t ktm_block = (((p->ktm_unblock*(pow(p->perm50,nperm)))*0.5)/((0.5-(XSSS))-(XWSS)));
  GlobalData_t partial_diff_ZETAS_del_ZETAS = (-cds);
  GlobalData_t partial_diff_ZETAW_del_ZETAW = (-cdw);
  GlobalData_t partial_xb_su_del_XS = k_su;
  GlobalData_t partial_xb_wu_del_XW = k_wu;
  GlobalData_t ZETAS_rush_larsen_B = (exp((dt*partial_diff_ZETAS_del_ZETAS)));
  GlobalData_t ZETAW_rush_larsen_B = (exp((dt*partial_diff_ZETAW_del_ZETAW)));
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Ca_i_sizeof;
  int __Ca_i_offset;
  SVgetfcn __Ca_i_SVgetfcn = get_sv_offset( IF->parent->type, "Ca_i", &__Ca_i_offset, &__Ca_i_sizeof );
  SVputfcn __Ca_i_SVputfcn = __Ca_i_SVgetfcn ? getPutSV(__Ca_i_SVgetfcn) : NULL;
  //Initialize the external vars to their current values
  GlobalData_t Tension = Tension_ext[__i];
  GlobalData_t delta_sl = delta_sl_ext[__i];
  GlobalData_t length = length_ext[__i];
  GlobalData_t Ca_i = __Ca_i_SVgetfcn ? __Ca_i_SVgetfcn(IF->parent, __i, __Ca_i_offset) :sv->__Ca_i_local;
  //Change the units of external variables as appropriate.
  
  
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->TRPN);
  fprintf(file, "%4.12f\t", sv->TmBlocked);
  fprintf(file, "%4.12f\t", sv->XS);
  fprintf(file, "%4.12f\t", sv->XW);
  fprintf(file, "%4.12f\t", sv->ZETAS);
  fprintf(file, "%4.12f\t", sv->ZETAW);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        