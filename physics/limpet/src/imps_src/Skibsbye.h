// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Title: Refractoriness in Human Atria: Time and Voltage Dependence of Sodium Channel Availability
*  Authors: L Skibsbye, T Jespersen, T Christ, MM Maleckar, J van den Brink, P Tavi, JT Koivumaeki
*  Year: 2016
*  Journal: Journal of Molecular and Cellular Cardiology, 2016, 101,HH26-HH34
*  DOI: 10.1016/j.yjmcc.2016.10.009
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __SKIBSBYE_H__
#define __SKIBSBYE_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Skibsbye_REQDAT Vm_DATA_FLAG
#define Skibsbye_MODDAT Iion_DATA_FLAG

struct Skibsbye_Params {
    GlobalData_t ACh;
    GlobalData_t cAF_PLB;
    GlobalData_t cAF_RyR;
    GlobalData_t cAF_SLN;
    GlobalData_t cAF_cpumps;
    GlobalData_t cAF_gCaL;
    GlobalData_t cAF_gK1;
    GlobalData_t cAF_gKCa;
    GlobalData_t cAF_gKs;
    GlobalData_t cAF_gNa;
    GlobalData_t cAF_gsus;
    GlobalData_t cAF_gt;
    GlobalData_t cAF_kNaCa;
    GlobalData_t cAF_lcell;
    GlobalData_t cAF_phos;
    GlobalData_t factorgcal;
    GlobalData_t factorgk1;
    GlobalData_t factorgkr;
    GlobalData_t factorgks;
    GlobalData_t factorgto;

};

struct Skibsbye_state {
    GlobalData_t CaSR1;
    GlobalData_t CaSR2;
    GlobalData_t CaSR3;
    GlobalData_t CaSR4;
    GlobalData_t Cai1;
    GlobalData_t Cai2;
    GlobalData_t Cai3;
    GlobalData_t Cai4;
    GlobalData_t Cass;
    GlobalData_t Ki;
    GlobalData_t Nai;
    GlobalData_t Nass;
    GlobalData_t i_ICaLd;
    GlobalData_t i_ICaLf1;
    GlobalData_t i_ICaLf2;
    GlobalData_t i_ICaLfca;
    GlobalData_t i_IKCa_O;
    GlobalData_t i_IKrpa;
    GlobalData_t i_IKsn;
    GlobalData_t i_INah1;
    GlobalData_t i_INah2;
    GlobalData_t i_INam;
    GlobalData_t i_Ify;
    GlobalData_t i_Isusr;
    GlobalData_t i_Isuss;
    GlobalData_t i_Itr;
    GlobalData_t i_Its;
    GlobalData_t i_RyRa1;
    GlobalData_t i_RyRa2;
    GlobalData_t i_RyRa3;
    GlobalData_t i_RyRass;
    GlobalData_t i_RyRc1;
    GlobalData_t i_RyRc2;
    GlobalData_t i_RyRc3;
    GlobalData_t i_RyRcss;
    GlobalData_t i_RyRo1;
    GlobalData_t i_RyRo2;
    GlobalData_t i_RyRo3;
    GlobalData_t i_RyRoss;
    GlobalData_t i_SERCACa1;
    GlobalData_t i_SERCACa2;
    GlobalData_t i_SERCACa3;
    GlobalData_t i_SERCACass;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Skibsbye(ION_IF *);
void construct_tables_Skibsbye(ION_IF *);
void destroy_Skibsbye(ION_IF *);
void initialize_sv_Skibsbye(ION_IF *, GlobalData_t**);
void compute_Skibsbye(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
