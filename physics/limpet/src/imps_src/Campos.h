// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Fernando O. Campos, Yohannes Shiferaw, Anton J. Prassl, Patrick M. Boyle, Edward J. Vigmond, Gernot Plank
*  Year: 2015
*  Title: Stochastic spontaneous calcium release events trigger premature ventricular complexes by overcoming electrotonic load
*  Journal: Cardiovascular Research, 107(1), 175-183
*  DOI: 10.1093/cvr/cvv149
*  Comment: Modified version of the Mahajan-Shiferaw rabbit ventricular myocyte model with spontaneous Ca release
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __CAMPOS_H__
#define __CAMPOS_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Campos_REQDAT Vm_DATA_FLAG
#define Campos_MODDAT Iion_DATA_FLAG

struct Campos_Params {
    GlobalData_t CSR;
    GlobalData_t Cao;
    GlobalData_t INaCamax;
    GlobalData_t INaKmax;
    GlobalData_t Ko;
    GlobalData_t Nao;
    GlobalData_t g_Ca;
    GlobalData_t g_K1;
    GlobalData_t g_Kr;
    GlobalData_t g_Ks;
    GlobalData_t g_Na;
    GlobalData_t g_RyR;
    GlobalData_t g_sp;
    GlobalData_t g_tof;
    GlobalData_t g_tos;
    GlobalData_t v_Na;
    GlobalData_t v_up;

};

struct Campos_state {
    GlobalData_t Cai;
    GlobalData_t Ki;
    GlobalData_t Nai;
    GlobalData_t c1;
    GlobalData_t c2;
    GlobalData_t cj;
    GlobalData_t cjp;
    GlobalData_t cp;
    GlobalData_t cs;
    Gatetype h;
    GlobalData_t i1ba;
    GlobalData_t i1ca;
    GlobalData_t i2ba;
    GlobalData_t i2ca;
    Gatetype j;
    GlobalData_t j_rel;
    Gatetype m;
    GlobalData_t tropi;
    GlobalData_t trops;
    Gatetype xKr;
    Gatetype xs1;
    Gatetype xs2;
    Gatetype xtof;
    Gatetype xtos;
    Gatetype ytof;
    Gatetype ytos;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Campos(ION_IF *);
void construct_tables_Campos(ION_IF *);
void destroy_Campos(ION_IF *);
void initialize_sv_Campos(ION_IF *, GlobalData_t**);
void compute_Campos(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
