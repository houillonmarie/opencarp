// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Aslanidi OV, Sleiman RN, Boyett MR, Hancox JC, Zhang H
*  Year: 2010
*  Title: Ionic mechanisms for electrical heterogeneity between rabbit Purkinje fiber and Ventricular cells
*  Journal: Biophys J, 98(11),2420-31
*  DOI: 10.1016/j.bpj.2010.02.033
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __ASLANIDISLEIMAN_H__
#define __ASLANIDISLEIMAN_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define AslanidiSleiman_REQDAT Ke_DATA_FLAG|Vm_DATA_FLAG
#define AslanidiSleiman_MODDAT Iion_DATA_FLAG

struct AslanidiSleiman_Params {
    GlobalData_t Ca_handling;
    GlobalData_t Cae;
    GlobalData_t Cle;
    GlobalData_t Cli;
    GlobalData_t GCaB;
    GlobalData_t GCaL;
    GlobalData_t GCaT;
    GlobalData_t GCl;
    GlobalData_t GClB;
    GlobalData_t GK1;
    GlobalData_t GKB;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t GNaB;
    GlobalData_t GNaL;
    GlobalData_t Gto;
    GlobalData_t ICaPHill;
    GlobalData_t ICaPKmf;
    GlobalData_t ICaPVmf;
    GlobalData_t INaCamax;
    GlobalData_t Ke_init;
    GlobalData_t Ki;
    GlobalData_t Mgi;
    GlobalData_t Nae;
    GlobalData_t Nai;
    GlobalData_t PNaK;
    GlobalData_t T;
    GlobalData_t kmcaact;
    GlobalData_t kmcai;
    GlobalData_t kmcao;
    GlobalData_t kmnai1;
    GlobalData_t kmnao;
    GlobalData_t ksat;
    GlobalData_t nu;

};

struct AslanidiSleiman_state {
    GlobalData_t Cai;
    GlobalData_t Y0;
    GlobalData_t Y1;
    GlobalData_t Y11;
    GlobalData_t Y2;
    GlobalData_t Y23;
    GlobalData_t Y24;
    GlobalData_t Y25;
    GlobalData_t Y3;
    GlobalData_t Y32;
    GlobalData_t Y33;
    GlobalData_t Y34;
    GlobalData_t Y35;
    GlobalData_t Y36;
    GlobalData_t Y37;
    GlobalData_t Y38;
    GlobalData_t Y4;
    GlobalData_t Y5;
    GlobalData_t Y6;
    GlobalData_t Y7;
    Gatetype b;
    Gatetype d;
    Gatetype f;
    Gatetype g;
    Gatetype h;
    Gatetype hL;
    Gatetype i;
    Gatetype j;
    Gatetype m;
    Gatetype mL;
    Gatetype x;
    Gatetype xr;
    Gatetype xs;
    Gatetype y1;
    Gatetype y2;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_AslanidiSleiman(ION_IF *);
void construct_tables_AslanidiSleiman(ION_IF *);
void destroy_AslanidiSleiman(ION_IF *);
void initialize_sv_AslanidiSleiman(ION_IF *, GlobalData_t**);
void compute_AslanidiSleiman(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
