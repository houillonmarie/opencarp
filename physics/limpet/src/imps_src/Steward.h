// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Philip Stewart, Oleg V. Aslanidi, Denis Noble, Penelope J. Noble, Mark R. Boyett and Henggui Zhang
*  Year: 2009
*  Title: Mathematical models of the electrical action potential of Purkinje fibre cells
*  Journal: Phil. Trans. R. Soc. A., 367, 2225-2255
*  DOI: 10.1098/rsta.2008.0283
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __STEWARD_H__
#define __STEWARD_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Steward_REQDAT Vm_DATA_FLAG
#define Steward_MODDAT Iion_DATA_FLAG

struct Steward_Params {
    GlobalData_t GCaL;
    GlobalData_t GK1;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t Gbca;
    GlobalData_t Gbna;
    GlobalData_t Gf_K;
    GlobalData_t Gf_Na;
    GlobalData_t GpCa;
    GlobalData_t GpK;
    GlobalData_t Gsus;
    GlobalData_t Gto;
    GlobalData_t maxINaCa;
    GlobalData_t maxINaK;

};

struct Steward_state {
    GlobalData_t Ca_SR;
    GlobalData_t Cai;
    GlobalData_t Cass;
    GlobalData_t Ki;
    GlobalData_t Nai;
    GlobalData_t R_prime;
    GlobalData_t Xr1;
    GlobalData_t Xr2;
    GlobalData_t Xs;
    Gatetype d;
    Gatetype f;
    Gatetype f2;
    Gatetype fCass;
    Gatetype h;
    Gatetype j;
    Gatetype m;
    Gatetype r;
    Gatetype s;
    Gatetype y;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Steward(ION_IF *);
void construct_tables_Steward(ION_IF *);
void destroy_Steward(ION_IF *);
void initialize_sv_Steward(ION_IF *, GlobalData_t**);
void compute_Steward(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
