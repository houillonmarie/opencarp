// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Jean-Pierre Drouhard, Fernand A. Roberge
*  Year: 1987
*  Title: Revised formulation of the Hodgkin-Huxley representation of the sodium current in cardiac cells
*  Journal: Computers and Biomedical Research, 20(4), 333-350
*  DOI: 10.1016/0010-4809(87)90048-6
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __DROUHARDROBERGE_H__
#define __DROUHARDROBERGE_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define DrouhardRoberge_REQDAT Vm_DATA_FLAG
#define DrouhardRoberge_MODDAT Iion_DATA_FLAG

struct DrouhardRoberge_Params {
    GlobalData_t APDshorten;
    GlobalData_t GNa;
    GlobalData_t Gsi;

};

struct DrouhardRoberge_state {
    GlobalData_t Cai;
    Gatetype X;
    Gatetype d;
    Gatetype f;
    Gatetype h;
    Gatetype m;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_DrouhardRoberge(ION_IF *);
void construct_tables_DrouhardRoberge(ION_IF *);
void destroy_DrouhardRoberge(ION_IF *);
void initialize_sv_DrouhardRoberge(ION_IF *, GlobalData_t**);
void compute_DrouhardRoberge(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
