// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: A. Nygren , C. Fiset , L. Firek, J. W. Clark , D. S. Lindblad , R. B. Clark , and W. R. Giles
*  Year: 1998
*  Title: Mathematical model of an adult human atrial cell: the role of K+ currents in repolarization
*  Journal: Circulation Research,9-23,82(1),63-81
*  DOI: 10.1161/01.res.82.1.63
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Nygren.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Nygren(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Nygren( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define CaB_MAX (GlobalData_t)(0.08)
#define Ca_d_init (GlobalData_t)(7.24947e-05)
#define Ca_rel_init (GlobalData_t)(0.646458)
#define Ca_up_init (GlobalData_t)(0.664564)
#define Cai_init (GlobalData_t)(6.72905e-05)
#define Cao (GlobalData_t)(1.8)
#define Cm (GlobalData_t)(1.0)
#define DOM_FRAC (GlobalData_t)(0.02)
#define Eca_app (GlobalData_t)(60.0)
#define F (GlobalData_t)(96487.0)
#define F1_init (GlobalData_t)(0.428396)
#define F2_init (GlobalData_t)(0.0028005)
#define GAMMA (GlobalData_t)(0.45)
#define ICaP_ss (GlobalData_t)(0.08)
#define IUP_SS (GlobalData_t)(56.0)
#define Kb (GlobalData_t)(5.4)
#define Kcyca (GlobalData_t)(0.0003)
#define Kd_L (GlobalData_t)(-5.8)
#define Kf_L (GlobalData_t)(7.1)
#define Kh (GlobalData_t)(5.3)
#define Ki (GlobalData_t)(129.435)
#define Km (GlobalData_t)(-8.21)
#define Kn (GlobalData_t)(-12.7)
#define Kp_a (GlobalData_t)(-6.0)
#define Kp_i (GlobalData_t)(24.0)
#define Kr (GlobalData_t)(-11.0)
#define Kr_sus (GlobalData_t)(-8.0)
#define Krel_d (GlobalData_t)(0.003)
#define Krel_i (GlobalData_t)(0.0003)
#define Ks (GlobalData_t)(11.5)
#define Ks_sus (GlobalData_t)(10.0)
#define Ksrca (GlobalData_t)(0.5)
#define Kxcs (GlobalData_t)(0.4)
#define Kxcs2_srca (GlobalData_t)(9.6e-5)
#define Kxcs_srca (GlobalData_t)(0.00024)
#define MGi (GlobalData_t)(2.5)
#define Nab (GlobalData_t)(130.0)
#define Nai (GlobalData_t)(8.55474)
#define Nao (GlobalData_t)(130.0)
#define O_TMgC_init (GlobalData_t)(0.196085)
#define O_TMgMg_init (GlobalData_t)(0.709417)
#define P_Na (GlobalData_t)(3.2e-5)
#define R (GlobalData_t)(8314.0)
#define R_RECOV (GlobalData_t)(0.815e-3)
#define T (GlobalData_t)(306.15)
#define V_half_d_L (GlobalData_t)(-9.0)
#define V_half_f_L (GlobalData_t)(-27.4)
#define V_half_h (GlobalData_t)(-63.6)
#define V_half_m (GlobalData_t)(-27.12)
#define V_half_n (GlobalData_t)(19.9)
#define V_half_p_a (GlobalData_t)(-15.0)
#define V_half_p_i (GlobalData_t)(-55.0)
#define V_half_r (GlobalData_t)(1.0)
#define V_half_r_sus (GlobalData_t)(-4.3)
#define V_half_s (GlobalData_t)(-40.5)
#define V_half_s_sus (GlobalData_t)(-20.0)
#define V_init (GlobalData_t)(-74.2525)
#define Vol_rel (GlobalData_t)(0.000882)
#define Vol_up (GlobalData_t)(0.007938)
#define Voli (GlobalData_t)(0.11768)
#define alpha_rel (GlobalData_t)(4000.0)
#define concKo (GlobalData_t)(5.4)
#define d_NaCa (GlobalData_t)(0.0003)
#define n_init (GlobalData_t)(0.00483573)
#define s_sus_init (GlobalData_t)(0.991169)
#define tau_di (GlobalData_t)(10.0)
#define tau_tr (GlobalData_t)(10.0)
#define C_ITR (GlobalData_t)((((2.0*F)*Vol_rel)/tau_tr))
#define Ca_c (GlobalData_t)(Cao)
#define Kc (GlobalData_t)(concKo)
#define Nac (GlobalData_t)(Nao)
#define Vol_d (GlobalData_t)((0.11768*DOM_FRAC))
#define C_IDI (GlobalData_t)((((2.0*F)*Vol_d)/tau_di))
#define E_K (GlobalData_t)((((R*T)/F)*(log((Kc/Ki)))))
#define E_Na (GlobalData_t)((((R*T)/F)*(log((Nac/Nai)))))



void initialize_params_Nygren( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Nygren_Params *p = (Nygren_Params *)IF->params;

  // Compute the regional constants
  {
    p->GCaB = 1.57362e-3;
    p->GCaL = 0.135;
    p->GK1 = 0.06;
    p->GKS = 0.02;
    p->GKr = 0.01;
    p->GNaB = 1.21198e-3;
    p->Gsus = 0.055;
    p->Gt = 0.15;
    p->KNaCa = 7.49684e-04;
    p->maxINaK = 1.416506;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_Nygren;

}


// Define the parameters for the lookup tables
enum Tables {
  Ca_d_TAB,
  Ca_rel_TAB,
  Cai_TAB,
  F2_TAB,
  V_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum Ca_d_TableIndex {
  f_Ca_idx,
  r_act2_idx,
  NROWS_Ca_d
};

enum Ca_rel_TableIndex {
  O_Calse_inf_idx,
  O_Calse_rush_larsen_A_idx,
  O_Calse_rush_larsen_B_idx,
  tau_O_Calse_idx,
  NROWS_Ca_rel
};

enum Cai_TableIndex {
  E_Ca_idx,
  ICaP_idx,
  INaCa_frc1_term_idx,
  INaCa_frc2_term_idx,
  O_C_inf_idx,
  O_C_rush_larsen_A_idx,
  O_C_rush_larsen_B_idx,
  O_TC_inf_idx,
  O_TC_rush_larsen_A_idx,
  O_TC_rush_larsen_B_idx,
  r_act1_idx,
  r_inact_idx,
  tau_O_C_idx,
  tau_O_TC_idx,
  NROWS_Cai
};

enum F2_TableIndex {
  C_rel_idx,
  NROWS_F2
};

enum V_TableIndex {
  IBNa_idx,
  ICaL_term_idx,
  IK1_idx,
  IKr_term_idx,
  IKs_term_idx,
  INaCa_exp1_term_idx,
  INaCa_exp2_term_idx,
  INaK_idx,
  INaterm_idx,
  Isus_term_idx,
  It_term_idx,
  d_L_rush_larsen_A_idx,
  d_L_rush_larsen_B_idx,
  f_L1_rush_larsen_A_idx,
  f_L1_rush_larsen_B_idx,
  f_L2_rush_larsen_A_idx,
  f_L2_rush_larsen_B_idx,
  h1_rush_larsen_A_idx,
  h1_rush_larsen_B_idx,
  h2_rush_larsen_A_idx,
  h2_rush_larsen_B_idx,
  m_rush_larsen_A_idx,
  m_rush_larsen_B_idx,
  n_rush_larsen_A_idx,
  n_rush_larsen_B_idx,
  p_a_rush_larsen_A_idx,
  p_a_rush_larsen_B_idx,
  r_rush_larsen_A_idx,
  r_rush_larsen_B_idx,
  r_sus_rush_larsen_A_idx,
  r_sus_rush_larsen_B_idx,
  s_rush_larsen_A_idx,
  s_rush_larsen_B_idx,
  s_sus_rush_larsen_A_idx,
  s_sus_rush_larsen_B_idx,
  NROWS_V
};



void construct_tables_Nygren( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Nygren_Params *p = (Nygren_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  
  // Create the Ca_d lookup table
  LUT* Ca_d_tab = &IF->tables[Ca_d_TAB];
  LUT_alloc(Ca_d_tab, NROWS_Ca_d, 1e-6, 100e-3, 1e-6, "Nygren Ca_d");
  for (int __i=Ca_d_tab->mn_ind; __i<=Ca_d_tab->mx_ind; __i++) {
    double Ca_d = Ca_d_tab->res*__i;
    LUT_data_t* Ca_d_row = Ca_d_tab->tab[__i];
    Ca_d_row[f_Ca_idx] = (Ca_d/(Ca_d+0.025));
    Ca_d_row[r_act2_idx] = (0.001*(203.8*(pow((Ca_d/(Ca_d+Krel_d)),4.))));
  }
  check_LUT(Ca_d_tab);
  
  
  // Create the Ca_rel lookup table
  LUT* Ca_rel_tab = &IF->tables[Ca_rel_TAB];
  LUT_alloc(Ca_rel_tab, NROWS_Ca_rel, 0, 1, 1e-5, "Nygren Ca_rel");
  for (int __i=Ca_rel_tab->mn_ind; __i<=Ca_rel_tab->mx_ind; __i++) {
    double Ca_rel = Ca_rel_tab->res*__i;
    LUT_data_t* Ca_rel_row = Ca_rel_tab->tab[__i];
    Ca_rel_row[tau_O_Calse_idx] = (1./((0.48*Ca_rel)+0.4));
    Ca_rel_row[O_Calse_inf_idx] = ((0.48*Ca_rel)*Ca_rel_row[tau_O_Calse_idx]);
    Ca_rel_row[O_Calse_rush_larsen_B_idx] = (exp(((-dt)/Ca_rel_row[tau_O_Calse_idx])));
    double O_Calse_rush_larsen_C = (expm1(((-dt)/Ca_rel_row[tau_O_Calse_idx])));
    Ca_rel_row[O_Calse_rush_larsen_A_idx] = ((-Ca_rel_row[O_Calse_inf_idx])*O_Calse_rush_larsen_C);
  }
  check_LUT(Ca_rel_tab);
  
  
  // Create the Cai lookup table
  LUT* Cai_tab = &IF->tables[Cai_TAB];
  LUT_alloc(Cai_tab, NROWS_Cai, 1e-6, 100.0e-3, 1e-6, "Nygren Cai");
  for (int __i=Cai_tab->mn_ind; __i<=Cai_tab->mx_ind; __i++) {
    double Cai = Cai_tab->res*__i;
    LUT_data_t* Cai_row = Cai_tab->tab[__i];
    Cai_row[E_Ca_idx] = (((R*T)/(2.0*F))*(log((Ca_c/Cai))));
    Cai_row[ICaP_idx] = (ICaP_ss*(Cai/(Cai+0.0002)));
    Cai_row[INaCa_frc1_term_idx] = ((p->KNaCa*((pow(Nai,3.))*Ca_c))/(1.0+(d_NaCa*(((pow(Nac,3.))*Cai)+((pow(Nai,3.))*Ca_c)))));
    Cai_row[INaCa_frc2_term_idx] = ((p->KNaCa*((pow(Nac,3.))*Cai))/(1.0+(d_NaCa*(((pow(Nac,3.))*Cai)+((pow(Nai,3.))*Ca_c)))));
    Cai_row[r_act1_idx] = (0.001*(203.8*(pow((Cai/(Cai+Krel_i)),4.))));
    Cai_row[r_inact_idx] = (0.001*(33.96+(339.6*(pow((Cai/(Cai+Krel_i)),4.)))));
    Cai_row[tau_O_C_idx] = (1./((200.*Cai)+0.476));
    Cai_row[tau_O_TC_idx] = (1./((78.4*Cai)+0.392));
    Cai_row[O_C_inf_idx] = ((200.*Cai)*Cai_row[tau_O_C_idx]);
    Cai_row[O_C_rush_larsen_B_idx] = (exp(((-dt)/Cai_row[tau_O_C_idx])));
    double O_C_rush_larsen_C = (expm1(((-dt)/Cai_row[tau_O_C_idx])));
    Cai_row[O_TC_inf_idx] = ((78.4*Cai)*Cai_row[tau_O_TC_idx]);
    Cai_row[O_TC_rush_larsen_B_idx] = (exp(((-dt)/Cai_row[tau_O_TC_idx])));
    double O_TC_rush_larsen_C = (expm1(((-dt)/Cai_row[tau_O_TC_idx])));
    Cai_row[O_C_rush_larsen_A_idx] = ((-Cai_row[O_C_inf_idx])*O_C_rush_larsen_C);
    Cai_row[O_TC_rush_larsen_A_idx] = ((-Cai_row[O_TC_inf_idx])*O_TC_rush_larsen_C);
  }
  check_LUT(Cai_tab);
  
  
  // Create the F2 lookup table
  LUT* F2_tab = &IF->tables[F2_TAB];
  LUT_alloc(F2_tab, NROWS_F2, 0, 1, 1e-5, "Nygren F2");
  for (int __i=F2_tab->mn_ind; __i<=F2_tab->mx_ind; __i++) {
    double F2 = F2_tab->res*__i;
    LUT_data_t* F2_row = F2_tab->tab[__i];
    F2_row[C_rel_idx] = (alpha_rel*(pow((F2/(F2+0.25)),2.)));
  }
  check_LUT(F2_tab);
  
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -1000, 1000, 0.01, "Nygren V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    V_row[IBNa_idx] = (p->GNaB*(V-(E_Na)));
    V_row[ICaL_term_idx] = (p->GCaL*(V-(Eca_app)));
    V_row[IK1_idx] = (((p->GK1*(pow(Kc,0.4457)))*(V-(E_K)))/(1.0+(exp((((1.5*((V-(E_K))+3.6))*F)/(R*T))))));
    V_row[IKs_term_idx] = (p->GKS*(V-(E_K)));
    V_row[INaCa_exp1_term_idx] = (exp((((GAMMA*V)*F)/(R*T))));
    V_row[INaCa_exp2_term_idx] = (exp(((((GAMMA-(1.0))*V)*F)/(R*T))));
    V_row[INaK_idx] = ((((((p->maxINaK*Kc)/(Kc+1.0))*(pow(Nai,1.5)))/((pow(Nai,1.5))+36.482873))*(V+150.0))/(V+200.0));
    V_row[INaterm_idx] = ((V==0.) ? (((P_Na*Nac)*F)*((exp((((V-(E_Na))*F)/(R*T))))-(1.0))) : (((((((P_Na*Nac)*V)*F)*F)/(R*T))*((exp((((V-(E_Na))*F)/(R*T))))-(1.0)))/((exp(((V*F)/(R*T))))-(1.0))));
    V_row[Isus_term_idx] = (p->Gsus*(V-(E_K)));
    V_row[It_term_idx] = (p->Gt*(V-(E_K)));
    double d_L_inf = (1.0/(1.0+(exp(((V-(V_half_d_L))/Kd_L)))));
    double f_L1_inf = (1.0/(1.0+(exp(((V-(V_half_f_L))/Kf_L)))));
    double f_L2_inf = (1.0/(1.0+(exp(((V-(V_half_f_L))/Kf_L)))));
    double h1_inf = (1.0/(1.0+(exp(((V-(V_half_h))/Kh)))));
    double m_inf = (1.0/(1.0+(exp(((V-(V_half_m))/Km)))));
    double n_inf = (1.0/(1.0+(exp(((V-(V_half_n))/Kn)))));
    double p_a_inf = (1.0/(1.0+(exp(((V-(V_half_p_a))/Kp_a)))));
    double p_i = (1.0/(1.0+(exp(((V-(V_half_p_i))/Kp_i)))));
    double r_inf = (1.0/(1.0+(exp(((V-(V_half_r))/Kr)))));
    double r_sus_inf = (1.0/(1.0+(exp(((V-(V_half_r_sus))/Kr_sus)))));
    double s_inf = (1.0/(1.0+(exp(((V-(V_half_s))/Ks)))));
    double s_sus_inf = ((0.4/(1.0+(exp(((V-(V_half_s_sus))/Ks_sus)))))+0.6);
    double tau_d_L = ((2.7*(exp((-(pow(((V+35.0)/30.0),2.))))))+2.0);
    double tau_f_L1 = ((161.0*(exp((-1.0*(pow(((V+40.0)/14.4),2.))))))+10.0);
    double tau_f_L2 = ((1332.3*(exp((-1.0*(pow(((V+40.0)/14.2),2.))))))+62.6);
    double tau_h1 = ((30.0/(1.0+(exp(((V+35.1)/3.2)))))+0.3);
    double tau_h2 = ((120.0/(1.0+(exp(((V+35.1)/3.2)))))+3.0);
    double tau_m = ((4.2e-2*(exp((-(pow(((V+25.57)/28.8),2.))))))+2.4e-2);
    double tau_n = (700.0+(400.0*(exp((-(pow(((V-(20.0))/20.0),2.)))))));
    double tau_p_a = (31.18+(217.18*(exp((-(pow(((V+20.1376)/22.1996),2.)))))));
    double tau_r = ((3.5*(exp((-(pow((V/30.0),2.))))))+1.5);
    double tau_r_sus = ((9.0/(1.0+(exp(((V+5.0)/12.0)))))+0.5);
    double tau_s = ((481.2*(exp((-(pow(((V+52.45)/14.97),2.))))))+14.14);
    double tau_s_sus = ((47.0/(1.0+(exp(((V+60.0)/10.0)))))+300.0);
    V_row[IKr_term_idx] = ((p->GKr*p_i)*(V-(E_K)));
    V_row[d_L_rush_larsen_B_idx] = (exp(((-dt)/tau_d_L)));
    double d_L_rush_larsen_C = (expm1(((-dt)/tau_d_L)));
    V_row[f_L1_rush_larsen_B_idx] = (exp(((-dt)/tau_f_L1)));
    double f_L1_rush_larsen_C = (expm1(((-dt)/tau_f_L1)));
    V_row[f_L2_rush_larsen_B_idx] = (exp(((-dt)/tau_f_L2)));
    double f_L2_rush_larsen_C = (expm1(((-dt)/tau_f_L2)));
    V_row[h1_rush_larsen_B_idx] = (exp(((-dt)/tau_h1)));
    double h1_rush_larsen_C = (expm1(((-dt)/tau_h1)));
    double h2_inf = h1_inf;
    V_row[h2_rush_larsen_B_idx] = (exp(((-dt)/tau_h2)));
    double h2_rush_larsen_C = (expm1(((-dt)/tau_h2)));
    V_row[m_rush_larsen_B_idx] = (exp(((-dt)/tau_m)));
    double m_rush_larsen_C = (expm1(((-dt)/tau_m)));
    V_row[n_rush_larsen_B_idx] = (exp(((-dt)/tau_n)));
    double n_rush_larsen_C = (expm1(((-dt)/tau_n)));
    V_row[p_a_rush_larsen_B_idx] = (exp(((-dt)/tau_p_a)));
    double p_a_rush_larsen_C = (expm1(((-dt)/tau_p_a)));
    V_row[r_rush_larsen_B_idx] = (exp(((-dt)/tau_r)));
    double r_rush_larsen_C = (expm1(((-dt)/tau_r)));
    V_row[r_sus_rush_larsen_B_idx] = (exp(((-dt)/tau_r_sus)));
    double r_sus_rush_larsen_C = (expm1(((-dt)/tau_r_sus)));
    V_row[s_rush_larsen_B_idx] = (exp(((-dt)/tau_s)));
    double s_rush_larsen_C = (expm1(((-dt)/tau_s)));
    V_row[s_sus_rush_larsen_B_idx] = (exp(((-dt)/tau_s_sus)));
    double s_sus_rush_larsen_C = (expm1(((-dt)/tau_s_sus)));
    V_row[d_L_rush_larsen_A_idx] = ((-d_L_inf)*d_L_rush_larsen_C);
    V_row[f_L1_rush_larsen_A_idx] = ((-f_L1_inf)*f_L1_rush_larsen_C);
    V_row[f_L2_rush_larsen_A_idx] = ((-f_L2_inf)*f_L2_rush_larsen_C);
    V_row[h1_rush_larsen_A_idx] = ((-h1_inf)*h1_rush_larsen_C);
    V_row[h2_rush_larsen_A_idx] = ((-h2_inf)*h2_rush_larsen_C);
    V_row[m_rush_larsen_A_idx] = ((-m_inf)*m_rush_larsen_C);
    V_row[n_rush_larsen_A_idx] = ((-n_inf)*n_rush_larsen_C);
    V_row[p_a_rush_larsen_A_idx] = ((-p_a_inf)*p_a_rush_larsen_C);
    V_row[r_rush_larsen_A_idx] = ((-r_inf)*r_rush_larsen_C);
    V_row[r_sus_rush_larsen_A_idx] = ((-r_sus_inf)*r_sus_rush_larsen_C);
    V_row[s_rush_larsen_A_idx] = ((-s_inf)*s_rush_larsen_C);
    V_row[s_sus_rush_larsen_A_idx] = ((-s_sus_inf)*s_sus_rush_larsen_C);
  }
  check_LUT(V_tab);
  

}



void    initialize_sv_Nygren( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Nygren_Params *p = (Nygren_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Nygren_state) );
  Nygren_state *sv_base = (Nygren_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Nygren_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Ca_d = Ca_d_init;
    sv->Ca_rel = Ca_rel_init;
    sv->Ca_up = Ca_up_init;
    sv->Cai = Cai_init;
    sv->F1 = F1_init;
    sv->F2 = F2_init;
    sv->O_TMgC = O_TMgC_init;
    sv->O_TMgMg = O_TMgMg_init;
    V = V_init;
    sv->n = n_init;
    sv->s_sus = s_sus_init;
    double E_Ca = (((R*T)/(2.0*F))*(log((Ca_c/sv->Cai))));
    double IBNa = (p->GNaB*(V-(E_Na)));
    double ICaL_term = (p->GCaL*(V-(Eca_app)));
    double ICaP = (ICaP_ss*(sv->Cai/(sv->Cai+0.0002)));
    double IK1 = (((p->GK1*(pow(Kc,0.4457)))*(V-(E_K)))/(1.0+(exp((((1.5*((V-(E_K))+3.6))*F)/(R*T))))));
    double IKs_term = (p->GKS*(V-(E_K)));
    double INaCa_exp1_term = (exp((((GAMMA*V)*F)/(R*T))));
    double INaCa_exp2_term = (exp(((((GAMMA-(1.0))*V)*F)/(R*T))));
    double INaCa_frc1_term = ((p->KNaCa*((pow(Nai,3.))*Ca_c))/(1.0+(d_NaCa*(((pow(Nac,3.))*sv->Cai)+((pow(Nai,3.))*Ca_c)))));
    double INaCa_frc2_term = ((p->KNaCa*((pow(Nac,3.))*sv->Cai))/(1.0+(d_NaCa*(((pow(Nac,3.))*sv->Cai)+((pow(Nai,3.))*Ca_c)))));
    double INaK = ((((((p->maxINaK*Kc)/(Kc+1.0))*(pow(Nai,1.5)))/((pow(Nai,1.5))+36.482873))*(V+150.0))/(V+200.0));
    double INaterm = ((V==0.) ? (((P_Na*Nac)*F)*((exp((((V-(E_Na))*F)/(R*T))))-(1.0))) : (((((((P_Na*Nac)*V)*F)*F)/(R*T))*((exp((((V-(E_Na))*F)/(R*T))))-(1.0)))/((exp(((V*F)/(R*T))))-(1.0))));
    double Isus_term = (p->Gsus*(V-(E_K)));
    double It_term = (p->Gt*(V-(E_K)));
    double d_L_inf = (1.0/(1.0+(exp(((V-(V_half_d_L))/Kd_L)))));
    double f_Ca = (sv->Ca_d/(sv->Ca_d+0.025));
    double f_L1_inf = (1.0/(1.0+(exp(((V-(V_half_f_L))/Kf_L)))));
    double f_L2_inf = (1.0/(1.0+(exp(((V-(V_half_f_L))/Kf_L)))));
    double h1_inf = (1.0/(1.0+(exp(((V-(V_half_h))/Kh)))));
    double m_inf = (1.0/(1.0+(exp(((V-(V_half_m))/Km)))));
    double p_a_inf = (1.0/(1.0+(exp(((V-(V_half_p_a))/Kp_a)))));
    double p_i = (1.0/(1.0+(exp(((V-(V_half_p_i))/Kp_i)))));
    double r_inf = (1.0/(1.0+(exp(((V-(V_half_r))/Kr)))));
    double r_sus_inf = (1.0/(1.0+(exp(((V-(V_half_r_sus))/Kr_sus)))));
    double s_inf = (1.0/(1.0+(exp(((V-(V_half_s))/Ks)))));
    double tau_O_C = (1./((200.*sv->Cai)+0.476));
    double tau_O_Calse = (1./((0.48*sv->Ca_rel)+0.4));
    double tau_O_TC = (1./((78.4*sv->Cai)+0.392));
    double IKr_term = ((p->GKr*p_i)*(V-(E_K)));
    double IKs = (sv->n*IKs_term);
    double INaCa = ((INaCa_frc1_term*INaCa_exp1_term)-((INaCa_frc2_term*INaCa_exp2_term)));
    double IbCa = (p->GCaB*(V-(E_Ca)));
    double O_C_inf = ((200.*sv->Cai)*tau_O_C);
    double O_Calse_inf = ((0.48*sv->Ca_rel)*tau_O_Calse);
    double O_TC_inf = ((78.4*sv->Cai)*tau_O_TC);
    double d_L_init = d_L_inf;
    double f_L1_init = f_L1_inf;
    double f_L2_init = f_L2_inf;
    double h1_init = h1_inf;
    double h2_inf = h1_inf;
    double m_init = m_inf;
    double p_a_init = p_a_inf;
    double r_init = r_inf;
    double r_sus_init = r_sus_inf;
    double s_init = s_inf;
    double O_C_init = O_C_inf;
    double O_Calse_init = O_Calse_inf;
    double O_TC_init = O_TC_inf;
    sv->d_L = d_L_init;
    sv->f_L1 = f_L1_init;
    sv->f_L2 = f_L2_init;
    sv->h1 = h1_init;
    double h2_init = h2_inf;
    sv->m = m_init;
    sv->p_a = p_a_init;
    sv->r = r_init;
    sv->r_sus = r_sus_init;
    sv->s = s_init;
    double ICaL = ((sv->d_L*((f_Ca*sv->f_L1)+((1.0-(f_Ca))*sv->f_L2)))*ICaL_term);
    double IKr = (sv->p_a*IKr_term);
    double Isus = ((sv->r_sus*sv->s_sus)*Isus_term);
    double It = ((sv->r*sv->s)*It_term);
    sv->O_C = O_C_init;
    sv->O_Calse = O_Calse_init;
    sv->O_TC = O_TC_init;
    sv->h2 = h2_init;
    double INa = ((((sv->m*sv->m)*sv->m)*((0.9*sv->h1)+(0.1*sv->h2)))*INaterm);
    Iion = (((((((((((INaK+IBNa)+IK1)+IKr)+IKs)+Isus)+ICaP)+IbCa)+It)+INaCa)+ICaL)+INa);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Nygren(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Nygren_Params *p  = (Nygren_Params *)IF->params;
  Nygren_state *sv_base = (Nygren_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Nygren_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Ca_d_row[NROWS_Ca_d];
    LUT_interpRow(&IF->tables[Ca_d_TAB], sv->Ca_d, __i, Ca_d_row);
    LUT_data_t Ca_rel_row[NROWS_Ca_rel];
    LUT_interpRow(&IF->tables[Ca_rel_TAB], sv->Ca_rel, __i, Ca_rel_row);
    LUT_data_t Cai_row[NROWS_Cai];
    LUT_interpRow(&IF->tables[Cai_TAB], sv->Cai, __i, Cai_row);
    LUT_data_t F2_row[NROWS_F2];
    LUT_interpRow(&IF->tables[F2_TAB], sv->F2, __i, F2_row);
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t ICaL = ((sv->d_L*((Ca_d_row[f_Ca_idx]*sv->f_L1)+((1.0-(Ca_d_row[f_Ca_idx]))*sv->f_L2)))*V_row[ICaL_term_idx]);
    GlobalData_t IKs = (sv->n*V_row[IKs_term_idx]);
    GlobalData_t INa = ((((sv->m*sv->m)*sv->m)*((0.9*sv->h1)+(0.1*sv->h2)))*V_row[INaterm_idx]);
    GlobalData_t INaCa = ((Cai_row[INaCa_frc1_term_idx]*V_row[INaCa_exp1_term_idx])-((Cai_row[INaCa_frc2_term_idx]*V_row[INaCa_exp2_term_idx])));
    GlobalData_t IbCa = (p->GCaB*(V-(Cai_row[E_Ca_idx])));
    GlobalData_t Isus = ((sv->r_sus*sv->s_sus)*V_row[Isus_term_idx]);
    GlobalData_t It = ((sv->r*sv->s)*V_row[It_term_idx]);
    GlobalData_t IKr = (sv->p_a*V_row[IKr_term_idx]);
    Iion = (((((((((((V_row[INaK_idx]+V_row[IBNa_idx])+V_row[IK1_idx])+IKr)+IKs)+Isus)+Cai_row[ICaP_idx])+IbCa)+It)+INaCa)+ICaL)+INa);
    
    
    //Complete Forward Euler Update
    GlobalData_t Idi = ((sv->Ca_d-(sv->Cai))*C_IDI);
    GlobalData_t Itr = ((sv->Ca_up-(sv->Ca_rel))*C_ITR);
    GlobalData_t Iup = ((IUP_SS*(sv->Cai-((Kxcs2_srca*sv->Ca_up))))/((sv->Cai+Kcyca)+(Kxcs_srca*(sv->Ca_up+Ksrca))));
    GlobalData_t diff_O_TMgC = (((200.*sv->Cai)*((1.-(sv->O_TMgC))-(sv->O_TMgMg)))-((0.0066*sv->O_TMgC)));
    GlobalData_t diff_O_TMgMg = (((2.*MGi)*((1.-(sv->O_TMgC))-(sv->O_TMgMg)))-((0.66*sv->O_TMgMg)));
    GlobalData_t F_temp = ((Cai_row[r_act1_idx]+Ca_d_row[r_act2_idx])*sv->F1);
    GlobalData_t ICa = (((-2.0*INaCa)+IbCa)+Cai_row[ICaP_idx]);
    GlobalData_t Irel = (F2_row[C_rel_idx]*(sv->Ca_rel-(sv->Cai)));
    GlobalData_t diff_Ca_d = ((-(ICaL+Idi))/((2.0*Vol_d)*F));
    GlobalData_t diff_Ca_up = ((Iup-(Itr))/((2.0*F)*Vol_up));
    GlobalData_t diff_F1 = ((R_RECOV*((1.0-(sv->F1))-(sv->F2)))-(F_temp));
    GlobalData_t diff_F2 = (F_temp-((Cai_row[r_inact_idx]*sv->F2)));
    GlobalData_t diff_O_C = ((Cai_row[O_C_inf_idx]-(sv->O_C))/Cai_row[tau_O_C_idx]);
    GlobalData_t diff_O_Calse = ((Ca_rel_row[O_Calse_inf_idx]-(sv->O_Calse))/Ca_rel_row[tau_O_Calse_idx]);
    GlobalData_t diff_O_TC = ((Cai_row[O_TC_inf_idx]-(sv->O_TC))/Cai_row[tau_O_TC_idx]);
    GlobalData_t ocalsedot = (31.0*diff_O_Calse);
    GlobalData_t odot = (((0.045*diff_O_C)+(0.08*diff_O_TC))+(0.16*diff_O_TMgC));
    GlobalData_t diff_Ca_rel = (((Itr-(Irel))/((2.0*Vol_rel)*F))-(ocalsedot));
    GlobalData_t diff_Cai = (((-((((-Idi)+ICa)+Iup)-(Irel)))/((2.0*Voli)*F))-(odot));
    GlobalData_t Ca_d_new = sv->Ca_d+diff_Ca_d*dt;
    GlobalData_t Ca_rel_new = sv->Ca_rel+diff_Ca_rel*dt;
    GlobalData_t Ca_up_new = sv->Ca_up+diff_Ca_up*dt;
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    GlobalData_t F1_new = sv->F1+diff_F1*dt;
    GlobalData_t F2_new = sv->F2+diff_F2*dt;
    GlobalData_t O_TMgC_new = sv->O_TMgC+diff_O_TMgC*dt;
    GlobalData_t O_TMgMg_new = sv->O_TMgMg+diff_O_TMgMg*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t O_C_rush_larsen_B = Cai_row[O_C_rush_larsen_B_idx];
    GlobalData_t O_Calse_rush_larsen_B = Ca_rel_row[O_Calse_rush_larsen_B_idx];
    GlobalData_t O_TC_rush_larsen_B = Cai_row[O_TC_rush_larsen_B_idx];
    GlobalData_t d_L_rush_larsen_B = V_row[d_L_rush_larsen_B_idx];
    GlobalData_t f_L1_rush_larsen_B = V_row[f_L1_rush_larsen_B_idx];
    GlobalData_t f_L2_rush_larsen_B = V_row[f_L2_rush_larsen_B_idx];
    GlobalData_t h1_rush_larsen_B = V_row[h1_rush_larsen_B_idx];
    GlobalData_t h2_rush_larsen_B = V_row[h2_rush_larsen_B_idx];
    GlobalData_t m_rush_larsen_B = V_row[m_rush_larsen_B_idx];
    GlobalData_t n_rush_larsen_B = V_row[n_rush_larsen_B_idx];
    GlobalData_t p_a_rush_larsen_B = V_row[p_a_rush_larsen_B_idx];
    GlobalData_t r_rush_larsen_B = V_row[r_rush_larsen_B_idx];
    GlobalData_t r_sus_rush_larsen_B = V_row[r_sus_rush_larsen_B_idx];
    GlobalData_t s_rush_larsen_B = V_row[s_rush_larsen_B_idx];
    GlobalData_t s_sus_rush_larsen_B = V_row[s_sus_rush_larsen_B_idx];
    GlobalData_t O_C_rush_larsen_A = Cai_row[O_C_rush_larsen_A_idx];
    GlobalData_t O_Calse_rush_larsen_A = Ca_rel_row[O_Calse_rush_larsen_A_idx];
    GlobalData_t O_TC_rush_larsen_A = Cai_row[O_TC_rush_larsen_A_idx];
    GlobalData_t d_L_rush_larsen_A = V_row[d_L_rush_larsen_A_idx];
    GlobalData_t f_L1_rush_larsen_A = V_row[f_L1_rush_larsen_A_idx];
    GlobalData_t f_L2_rush_larsen_A = V_row[f_L2_rush_larsen_A_idx];
    GlobalData_t h1_rush_larsen_A = V_row[h1_rush_larsen_A_idx];
    GlobalData_t h2_rush_larsen_A = V_row[h2_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_A = V_row[m_rush_larsen_A_idx];
    GlobalData_t n_rush_larsen_A = V_row[n_rush_larsen_A_idx];
    GlobalData_t p_a_rush_larsen_A = V_row[p_a_rush_larsen_A_idx];
    GlobalData_t r_rush_larsen_A = V_row[r_rush_larsen_A_idx];
    GlobalData_t r_sus_rush_larsen_A = V_row[r_sus_rush_larsen_A_idx];
    GlobalData_t s_rush_larsen_A = V_row[s_rush_larsen_A_idx];
    GlobalData_t s_sus_rush_larsen_A = V_row[s_sus_rush_larsen_A_idx];
    GlobalData_t O_C_new = O_C_rush_larsen_A+O_C_rush_larsen_B*sv->O_C;
    GlobalData_t O_Calse_new = O_Calse_rush_larsen_A+O_Calse_rush_larsen_B*sv->O_Calse;
    GlobalData_t O_TC_new = O_TC_rush_larsen_A+O_TC_rush_larsen_B*sv->O_TC;
    GlobalData_t d_L_new = d_L_rush_larsen_A+d_L_rush_larsen_B*sv->d_L;
    GlobalData_t f_L1_new = f_L1_rush_larsen_A+f_L1_rush_larsen_B*sv->f_L1;
    GlobalData_t f_L2_new = f_L2_rush_larsen_A+f_L2_rush_larsen_B*sv->f_L2;
    GlobalData_t h1_new = h1_rush_larsen_A+h1_rush_larsen_B*sv->h1;
    GlobalData_t h2_new = h2_rush_larsen_A+h2_rush_larsen_B*sv->h2;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t n_new = n_rush_larsen_A+n_rush_larsen_B*sv->n;
    GlobalData_t p_a_new = p_a_rush_larsen_A+p_a_rush_larsen_B*sv->p_a;
    GlobalData_t r_new = r_rush_larsen_A+r_rush_larsen_B*sv->r;
    GlobalData_t r_sus_new = r_sus_rush_larsen_A+r_sus_rush_larsen_B*sv->r_sus;
    GlobalData_t s_new = s_rush_larsen_A+s_rush_larsen_B*sv->s;
    GlobalData_t s_sus_new = s_sus_rush_larsen_A+s_sus_rush_larsen_B*sv->s_sus;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Ca_d = Ca_d_new;
    sv->Ca_rel = Ca_rel_new;
    sv->Ca_up = Ca_up_new;
    sv->Cai = Cai_new;
    sv->F1 = F1_new;
    sv->F2 = F2_new;
    Iion = Iion;
    sv->O_C = O_C_new;
    sv->O_Calse = O_Calse_new;
    sv->O_TC = O_TC_new;
    sv->O_TMgC = O_TMgC_new;
    sv->O_TMgMg = O_TMgMg_new;
    sv->d_L = d_L_new;
    sv->f_L1 = f_L1_new;
    sv->f_L2 = f_L2_new;
    sv->h1 = h1_new;
    sv->h2 = h2_new;
    sv->m = m_new;
    sv->n = n_new;
    sv->p_a = p_a_new;
    sv->r = r_new;
    sv->r_sus = r_sus_new;
    sv->s = s_new;
    sv->s_sus = s_sus_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_Nygren(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Nygren_trace_header.txt","wt");
    fprintf(theader->fd,
        "IBNa\n"
        "ICaL\n"
        "ICaP\n"
        "IK1\n"
        "IKr\n"
        "IKs\n"
        "INa\n"
        "INaCa\n"
        "INaK\n"
        "IbCa\n"
        "Idi\n"
        "Iion\n"
        "Irel\n"
        "Isus\n"
        "It\n"
        "Itr\n"
        "Iup\n"
        "V\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Nygren_Params *p  = (Nygren_Params *)IF->params;

  Nygren_state *sv_base = (Nygren_state *)IF->sv_tab.y;
  Nygren_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t C_rel = (alpha_rel*(pow((sv->F2/(sv->F2+0.25)),2.)));
  GlobalData_t E_Ca = (((R*T)/(2.0*F))*(log((Ca_c/sv->Cai))));
  GlobalData_t IBNa = (p->GNaB*(V-(E_Na)));
  GlobalData_t ICaL_term = (p->GCaL*(V-(Eca_app)));
  GlobalData_t ICaP = (ICaP_ss*(sv->Cai/(sv->Cai+0.0002)));
  GlobalData_t IK1 = (((p->GK1*(pow(Kc,0.4457)))*(V-(E_K)))/(1.0+(exp((((1.5*((V-(E_K))+3.6))*F)/(R*T))))));
  GlobalData_t IKs_term = (p->GKS*(V-(E_K)));
  GlobalData_t INaCa_exp1_term = (exp((((GAMMA*V)*F)/(R*T))));
  GlobalData_t INaCa_exp2_term = (exp(((((GAMMA-(1.0))*V)*F)/(R*T))));
  GlobalData_t INaCa_frc1_term = ((p->KNaCa*((pow(Nai,3.))*Ca_c))/(1.0+(d_NaCa*(((pow(Nac,3.))*sv->Cai)+((pow(Nai,3.))*Ca_c)))));
  GlobalData_t INaCa_frc2_term = ((p->KNaCa*((pow(Nac,3.))*sv->Cai))/(1.0+(d_NaCa*(((pow(Nac,3.))*sv->Cai)+((pow(Nai,3.))*Ca_c)))));
  GlobalData_t INaK = ((((((p->maxINaK*Kc)/(Kc+1.0))*(pow(Nai,1.5)))/((pow(Nai,1.5))+36.482873))*(V+150.0))/(V+200.0));
  GlobalData_t INaterm = ((V==0.) ? (((P_Na*Nac)*F)*((exp((((V-(E_Na))*F)/(R*T))))-(1.0))) : (((((((P_Na*Nac)*V)*F)*F)/(R*T))*((exp((((V-(E_Na))*F)/(R*T))))-(1.0)))/((exp(((V*F)/(R*T))))-(1.0))));
  GlobalData_t Idi = ((sv->Ca_d-(sv->Cai))*C_IDI);
  GlobalData_t Isus_term = (p->Gsus*(V-(E_K)));
  GlobalData_t It_term = (p->Gt*(V-(E_K)));
  GlobalData_t Itr = ((sv->Ca_up-(sv->Ca_rel))*C_ITR);
  GlobalData_t Iup = ((IUP_SS*(sv->Cai-((Kxcs2_srca*sv->Ca_up))))/((sv->Cai+Kcyca)+(Kxcs_srca*(sv->Ca_up+Ksrca))));
  GlobalData_t f_Ca = (sv->Ca_d/(sv->Ca_d+0.025));
  GlobalData_t p_i = (1.0/(1.0+(exp(((V-(V_half_p_i))/Kp_i)))));
  GlobalData_t ICaL = ((sv->d_L*((f_Ca*sv->f_L1)+((1.0-(f_Ca))*sv->f_L2)))*ICaL_term);
  GlobalData_t IKr_term = ((p->GKr*p_i)*(V-(E_K)));
  GlobalData_t IKs = (sv->n*IKs_term);
  GlobalData_t INa = ((((sv->m*sv->m)*sv->m)*((0.9*sv->h1)+(0.1*sv->h2)))*INaterm);
  GlobalData_t INaCa = ((INaCa_frc1_term*INaCa_exp1_term)-((INaCa_frc2_term*INaCa_exp2_term)));
  GlobalData_t IbCa = (p->GCaB*(V-(E_Ca)));
  GlobalData_t Irel = (C_rel*(sv->Ca_rel-(sv->Cai)));
  GlobalData_t Isus = ((sv->r_sus*sv->s_sus)*Isus_term);
  GlobalData_t It = ((sv->r*sv->s)*It_term);
  GlobalData_t IKr = (sv->p_a*IKr_term);
  Iion = (((((((((((INaK+IBNa)+IK1)+IKr)+IKs)+Isus)+ICaP)+IbCa)+It)+INaCa)+ICaL)+INa);
  //Output the desired variables
  fprintf(file, "%4.12f\t", IBNa);
  fprintf(file, "%4.12f\t", ICaL);
  fprintf(file, "%4.12f\t", ICaP);
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", IbCa);
  fprintf(file, "%4.12f\t", Idi);
  fprintf(file, "%4.12f\t", Iion);
  fprintf(file, "%4.12f\t", Irel);
  fprintf(file, "%4.12f\t", Isus);
  fprintf(file, "%4.12f\t", It);
  fprintf(file, "%4.12f\t", Itr);
  fprintf(file, "%4.12f\t", Iup);
  fprintf(file, "%4.12f\t", V);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        