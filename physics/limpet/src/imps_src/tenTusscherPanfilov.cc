// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: K. H. W. J. ten Tusscher and A. V. Panfilov
*  Year: 2006
*  Title: Alternans and spiral breakup in a human ventricular tissue model
*  Journal: American Journal of Physiology-Heart and Circulatory Physiology, 291(3), 1088-1100
*  DOI: 10.1152/ajpheart.00109.2006
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "tenTusscherPanfilov.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_tenTusscherPanfilov(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_tenTusscherPanfilov( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define CaSR_init (GlobalData_t)(1.3)
#define CaSS_init (GlobalData_t)(0.00007)
#define Cai_init (GlobalData_t)(0.00007)
#define EC (GlobalData_t)(1.5)
#define ENDO (GlobalData_t)(2.)
#define EPI (GlobalData_t)(0.)
#define F2_init (GlobalData_t)(1.)
#define FCaSS_init (GlobalData_t)(1.)
#define F_init (GlobalData_t)(1.)
#define H_init (GlobalData_t)(0.75)
#define J_init (GlobalData_t)(0.75)
#define Ki_init (GlobalData_t)(138.3)
#define MCELL (GlobalData_t)(1.)
#define M_init (GlobalData_t)(0.)
#define Nai_init (GlobalData_t)(7.67)
#define O_init (GlobalData_t)(0.)
#define R__init (GlobalData_t)(1.)
#define R_init (GlobalData_t)(0.)
#define S_init (GlobalData_t)(1.)
#define V_init (GlobalData_t)(-86.2)
#define Vxfer (GlobalData_t)(0.0038)
#define Xr1_init (GlobalData_t)(0.)
#define Xr2_init (GlobalData_t)(1.)
#define Xs_init (GlobalData_t)(0.)
#define d_init (GlobalData_t)(0.)
#define k1_ (GlobalData_t)(0.15)
#define k2_ (GlobalData_t)(0.045)
#define k3 (GlobalData_t)(0.060)
#define k4 (GlobalData_t)(0.005)
#define maxsr (GlobalData_t)(2.5)
#define minsr (GlobalData_t)(1.)



void initialize_params_tenTusscherPanfilov( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  tenTusscherPanfilov_Params *p = (tenTusscherPanfilov_Params *)IF->params;

  // Compute the regional constants
  {
    p->Bufc = 0.2;
    p->Bufsr = 10.;
    p->Bufss = 0.4;
    p->CAPACITANCE = 0.185;
    p->Cao = 2.0;
    p->D_CaL_off = 0.;
    p->Fconst = 96485.3415;
    p->GK1 = 5.405;
    p->GNa = 14.838;
    p->GbCa = 0.000592;
    p->GbNa = 0.00029;
    p->GpCa = 0.1238;
    p->GpK = 0.0146;
    p->Kbufc = 0.001;
    p->Kbufsr = 0.3;
    p->Kbufss = 0.00025;
    p->KmCa = 1.38;
    p->KmK = 1.0;
    p->KmNa = 40.0;
    p->KmNai = 87.5;
    p->Ko = 5.4;
    p->KpCa = 0.0005;
    p->Kup = 0.00025;
    p->Nao = 140.0;
    p->Rconst = 8314.472;
    p->T = 310.0;
    p->Vc = 0.016404;
    p->Vleak = 0.00036;
    p->Vmaxup = 0.006375;
    p->Vrel = 0.102;
    p->Vsr = 0.001094;
    p->Vss = 0.00005468;
    p->cell_type = EPI;
    if (0) ;
    else if (flag_set(p->flags, "EPI")) p->cell_type = EPI;
    else if (flag_set(p->flags, "MCELL")) p->cell_type = MCELL;
    else if (flag_set(p->flags, "ENDO")) p->cell_type = ENDO;
    p->knaca = 1000.;
    p->knak = 2.724;
    p->ksat = 0.1;
    p->n = 0.35;
    p->pKNa = 0.03;
    p->scl_tau_f = 1.;
    p->vHalfXs = 5.;
    p->xr2_off = 0.;
  }
  // Compute the regional initialization
  {
    p->GCaL = 0.00003980;
    p->GKr = 0.153;
    p->GKs = ((p->cell_type==EPI) ? 0.392 : ((p->cell_type==MCELL) ? 0.098 : 0.392));
    p->Gto = ((p->cell_type==EPI) ? 0.294 : ((p->cell_type==MCELL) ? 0.294 : 0.073));
  }
  IF->type->trace = trace_tenTusscherPanfilov;

}


// Define the parameters for the lookup tables
enum Tables {
  CaSS_TAB,
  V_TAB,
  VEk_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum CaSS_TableIndex {
  CaSS2_idx,
  FCaSS_rush_larsen_A_idx,
  FCaSS_rush_larsen_B_idx,
  NROWS_CaSS
};

enum V_TableIndex {
  D_rush_larsen_A_idx,
  D_rush_larsen_B_idx,
  F2_rush_larsen_A_idx,
  F2_rush_larsen_B_idx,
  F_rush_larsen_A_idx,
  F_rush_larsen_B_idx,
  H_rush_larsen_A_idx,
  H_rush_larsen_B_idx,
  INaCa_A_idx,
  INaCa_B_idx,
  J_rush_larsen_A_idx,
  J_rush_larsen_B_idx,
  M_rush_larsen_A_idx,
  M_rush_larsen_B_idx,
  R_rush_larsen_A_idx,
  R_rush_larsen_B_idx,
  S_rush_larsen_A_idx,
  S_rush_larsen_B_idx,
  Xr1_rush_larsen_A_idx,
  Xr1_rush_larsen_B_idx,
  Xr2_rush_larsen_A_idx,
  Xr2_rush_larsen_B_idx,
  Xs_rush_larsen_A_idx,
  Xs_rush_larsen_B_idx,
  a2_idx,
  rec_iNaK_idx,
  rec_ipK_idx,
  NROWS_V
};

enum VEk_TableIndex {
  rec_iK1_idx,
  NROWS_VEk
};



void construct_tables_tenTusscherPanfilov( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  tenTusscherPanfilov_Params *p = (tenTusscherPanfilov_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double KmNai3 = ((p->KmNai*p->KmNai)*p->KmNai);
  double Nao3 = ((p->Nao*p->Nao)*p->Nao);
  double RTONF = ((p->Rconst*p->T)/p->Fconst);
  double invKmCa_Cao = (1./(p->KmCa+p->Cao));
  double inverseVcF = (1./(p->Vc*p->Fconst));
  double inverseVcF2 = (1./((2.*p->Vc)*p->Fconst));
  double inverseVssF2 = (1./((2.*p->Vss)*p->Fconst));
  double pmf_INaK = (p->knak*(p->Ko/(p->Ko+p->KmK)));
  double sqrt_Ko = (sqrt((p->Ko/5.4)));
  double F_RT = (1./RTONF);
  double invKmNai3_Nao3 = (1./(KmNai3+Nao3));
  double invVcF_Cm = (inverseVcF*p->CAPACITANCE);
  double pmf_INaCa = ((p->knaca*invKmNai3_Nao3)*invKmCa_Cao);
  
  // Create the CaSS lookup table
  LUT* CaSS_tab = &IF->tables[CaSS_TAB];
  LUT_alloc(CaSS_tab, NROWS_CaSS, .00001, 10.0, .00001, "tenTusscherPanfilov CaSS");
  for (int __i=CaSS_tab->mn_ind; __i<=CaSS_tab->mx_ind; __i++) {
    double CaSS = CaSS_tab->res*__i;
    LUT_data_t* CaSS_row = CaSS_tab->tab[__i];
    CaSS_row[CaSS2_idx] = (CaSS*CaSS);
    double FCaSS_inf = ((0.6/(1.+((CaSS/0.05)*(CaSS/0.05))))+0.4);
    double tau_FCaSS = ((80./(1.+((CaSS/0.05)*(CaSS/0.05))))+2.);
    CaSS_row[FCaSS_rush_larsen_B_idx] = (exp(((-dt)/tau_FCaSS)));
    double FCaSS_rush_larsen_C = (expm1(((-dt)/tau_FCaSS)));
    CaSS_row[FCaSS_rush_larsen_A_idx] = ((-FCaSS_inf)*FCaSS_rush_larsen_C);
  }
  check_LUT(CaSS_tab);
  
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -800, 800, 0.05, "tenTusscherPanfilov V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    double D_inf = (1./(1.+(exp((((-8.+p->D_CaL_off)-(V))/7.5)))));
    double F2_inf = ((0.67/(1.+(exp(((V+35.)/7.)))))+0.33);
    double F_inf = (1./(1.+(exp(((V+20.)/7.)))));
    double H_inf = (1./((1.+(exp(((V+71.55)/7.43))))*(1.+(exp(((V+71.55)/7.43))))));
    double M_inf = (1./((1.+(exp(((-56.86-(V))/9.03))))*(1.+(exp(((-56.86-(V))/9.03))))));
    double R_inf = (1./(1.+(exp(((20.-(V))/6.)))));
    double S_inf = (1./(1.+(exp(((V+20.)/5.)))));
    double Xr1_inf = (1./(1.+(exp(((-26.-(V))/7.)))));
    double Xr2_inf = (1./(1.+(exp(((V-((-88.+p->xr2_off)))/24.)))));
    double Xs_inf = (1./(1.+(exp((((-p->vHalfXs)-(V))/14.)))));
    V_row[a2_idx] = (0.25*(exp(((2.*(V-(15.)))*F_RT))));
    double aa_D = ((1.4/(1.+(exp(((-35.-(V))/13.)))))+0.25);
    double aa_F = (1102.5*(exp((((-(V+27.))*(V+27.))/225.))));
    double aa_F2 = (562.*(exp((((-(V+27.))*(V+27.))/240.))));
    double aa_H = ((V>=-40.) ? 0. : (0.057*(exp(((-(V+80.))/6.8)))));
    double aa_J = ((V>=-40.) ? 0. : ((((-2.5428e4*(exp((0.2444*V))))-((6.948e-6*(exp((-0.04391*V))))))*(V+37.78))/(1.+(exp((0.311*(V+79.23)))))));
    double aa_M = (1./(1.+(exp(((-60.-(V))/5.)))));
    double aa_Xr1 = (450./(1.+(exp(((-45.-(V))/10.)))));
    double aa_Xr2 = (3./(1.+(exp(((-60.-(V))/20.)))));
    double aa_Xs = (1400./(sqrt((1.+(exp(((5.-(V))/6.)))))));
    double bb_D = (1.4/(1.+(exp(((V+5.)/5.)))));
    double bb_F = (200./(1.+(exp(((13.-(V))/10.)))));
    double bb_F2 = (31./(1.+(exp(((25.-(V))/10.)))));
    double bb_H = ((V>=-40.) ? (0.77/(0.13*(1.+(exp(((-(V+10.66))/11.1)))))) : ((2.7*(exp((0.079*V))))+(3.1e5*(exp((0.3485*V))))));
    double bb_J = ((V>=-40.) ? ((0.6*(exp((0.057*V))))/(1.+(exp((-0.1*(V+32.)))))) : ((0.02424*(exp((-0.01052*V))))/(1.+(exp((-0.1378*(V+40.14)))))));
    double bb_M = ((0.1/(1.+(exp(((V+35.)/5.)))))+(0.10/(1.+(exp(((V-(50.))/200.))))));
    double bb_Xr1 = (6./(1.+(exp(((V-(-30.))/11.5)))));
    double bb_Xr2 = (1.12/(1.+(exp(((V-(60.))/20.)))));
    double bb_Xs = (1./(1.+(exp(((V-(35.))/15.)))));
    double cc_D = (1./(1.+(exp(((50.-(V))/20.)))));
    double cc_F = ((180./(1.+(exp(((V+30.)/10.)))))+20.);
    double cc_F2 = (80./(1.+(exp(((V+30.)/10.)))));
    double den = (pmf_INaCa/(1.+(p->ksat*(exp((((p->n-(1.))*V)*F_RT))))));
    V_row[rec_iNaK_idx] = (1./((1.+(0.1245*(exp(((-0.1*V)*F_RT)))))+(0.0353*(exp(((-V)*F_RT))))));
    V_row[rec_ipK_idx] = (1./(1.+(exp(((25.-(V))/5.98)))));
    double tau_R = ((9.5*(exp((((-(V+40.))*(V+40.))/1800.))))+0.8);
    double tau_S = ((p->cell_type==ENDO) ? ((1000.*(exp((((-(V+67.))*(V+67.))/1000.))))+8.) : (((85.*(exp((((-(V+45.))*(V+45.))/320.))))+(5./(1.+(exp(((V-(20.))/5.))))))+3.));
    V_row[INaCa_A_idx] = ((den*p->Cao)*(exp(((p->n*V)*F_RT))));
    V_row[INaCa_B_idx] = (((den*(exp((((p->n-(1.))*V)*F_RT))))*Nao3)*2.5);
    double J_inf = H_inf;
    V_row[R_rush_larsen_B_idx] = (exp(((-dt)/tau_R)));
    double R_rush_larsen_C = (expm1(((-dt)/tau_R)));
    V_row[S_rush_larsen_B_idx] = (exp(((-dt)/tau_S)));
    double S_rush_larsen_C = (expm1(((-dt)/tau_S)));
    double tau_D = ((aa_D*bb_D)+cc_D);
    double tau_F2 = ((aa_F2+bb_F2)+cc_F2);
    double tau_F_factor = ((aa_F+bb_F)+cc_F);
    double tau_H = (1.0/(aa_H+bb_H));
    double tau_J = (1.0/(aa_J+bb_J));
    double tau_M = (aa_M*bb_M);
    double tau_Xr1 = (aa_Xr1*bb_Xr1);
    double tau_Xr2 = (aa_Xr2*bb_Xr2);
    double tau_Xs = ((aa_Xs*bb_Xs)+80.);
    V_row[D_rush_larsen_B_idx] = (exp(((-dt)/tau_D)));
    double D_rush_larsen_C = (expm1(((-dt)/tau_D)));
    V_row[F2_rush_larsen_B_idx] = (exp(((-dt)/tau_F2)));
    double F2_rush_larsen_C = (expm1(((-dt)/tau_F2)));
    V_row[H_rush_larsen_B_idx] = (exp(((-dt)/tau_H)));
    double H_rush_larsen_C = (expm1(((-dt)/tau_H)));
    V_row[J_rush_larsen_B_idx] = (exp(((-dt)/tau_J)));
    double J_rush_larsen_C = (expm1(((-dt)/tau_J)));
    V_row[M_rush_larsen_B_idx] = (exp(((-dt)/tau_M)));
    double M_rush_larsen_C = (expm1(((-dt)/tau_M)));
    V_row[R_rush_larsen_A_idx] = ((-R_inf)*R_rush_larsen_C);
    V_row[S_rush_larsen_A_idx] = ((-S_inf)*S_rush_larsen_C);
    V_row[Xr1_rush_larsen_B_idx] = (exp(((-dt)/tau_Xr1)));
    double Xr1_rush_larsen_C = (expm1(((-dt)/tau_Xr1)));
    V_row[Xr2_rush_larsen_B_idx] = (exp(((-dt)/tau_Xr2)));
    double Xr2_rush_larsen_C = (expm1(((-dt)/tau_Xr2)));
    V_row[Xs_rush_larsen_B_idx] = (exp(((-dt)/tau_Xs)));
    double Xs_rush_larsen_C = (expm1(((-dt)/tau_Xs)));
    double tau_F = ((V>0.) ? (tau_F_factor*p->scl_tau_f) : tau_F_factor);
    V_row[D_rush_larsen_A_idx] = ((-D_inf)*D_rush_larsen_C);
    V_row[F2_rush_larsen_A_idx] = ((-F2_inf)*F2_rush_larsen_C);
    V_row[F_rush_larsen_B_idx] = (exp(((-dt)/tau_F)));
    double F_rush_larsen_C = (expm1(((-dt)/tau_F)));
    V_row[H_rush_larsen_A_idx] = ((-H_inf)*H_rush_larsen_C);
    V_row[J_rush_larsen_A_idx] = ((-J_inf)*J_rush_larsen_C);
    V_row[M_rush_larsen_A_idx] = ((-M_inf)*M_rush_larsen_C);
    V_row[Xr1_rush_larsen_A_idx] = ((-Xr1_inf)*Xr1_rush_larsen_C);
    V_row[Xr2_rush_larsen_A_idx] = ((-Xr2_inf)*Xr2_rush_larsen_C);
    V_row[Xs_rush_larsen_A_idx] = ((-Xs_inf)*Xs_rush_larsen_C);
    V_row[F_rush_larsen_A_idx] = ((-F_inf)*F_rush_larsen_C);
  }
  check_LUT(V_tab);
  
  
  // Create the VEk lookup table
  LUT* VEk_tab = &IF->tables[VEk_TAB];
  LUT_alloc(VEk_tab, NROWS_VEk, -800, 800, 0.05, "tenTusscherPanfilov VEk");
  for (int __i=VEk_tab->mn_ind; __i<=VEk_tab->mx_ind; __i++) {
    double VEk = VEk_tab->res*__i;
    LUT_data_t* VEk_row = VEk_tab->tab[__i];
    double a_K1 = (0.1/(1.+(exp((0.06*(VEk-(200.)))))));
    double b_K1 = (((3.*(exp((0.0002*(VEk+100.)))))+(exp((0.1*(VEk-(10.))))))/(1.+(exp((-0.5*VEk)))));
    VEk_row[rec_iK1_idx] = (a_K1/(a_K1+b_K1));
  }
  check_LUT(VEk_tab);
  

}



void    initialize_sv_tenTusscherPanfilov( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  tenTusscherPanfilov_Params *p = (tenTusscherPanfilov_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(tenTusscherPanfilov_state) );
  tenTusscherPanfilov_state *sv_base = (tenTusscherPanfilov_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double KmNai3 = ((p->KmNai*p->KmNai)*p->KmNai);
  double Nao3 = ((p->Nao*p->Nao)*p->Nao);
  double RTONF = ((p->Rconst*p->T)/p->Fconst);
  double invKmCa_Cao = (1./(p->KmCa+p->Cao));
  double inverseVcF = (1./(p->Vc*p->Fconst));
  double inverseVcF2 = (1./((2.*p->Vc)*p->Fconst));
  double inverseVssF2 = (1./((2.*p->Vss)*p->Fconst));
  double pmf_INaK = (p->knak*(p->Ko/(p->Ko+p->KmK)));
  double sqrt_Ko = (sqrt((p->Ko/5.4)));
  double F_RT = (1./RTONF);
  double invKmNai3_Nao3 = (1./(KmNai3+Nao3));
  double invVcF_Cm = (inverseVcF*p->CAPACITANCE);
  double pmf_INaCa = ((p->knaca*invKmNai3_Nao3)*invKmCa_Cao);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    tenTusscherPanfilov_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    sv->GCaL = p->GCaL;
    sv->GKr = p->GKr;
    sv->GKs = p->GKs;
    sv->Gto = p->Gto;
    // Initialize the rest of the nodal variables
    sv->CaSR = CaSR_init;
    sv->CaSS = CaSS_init;
    sv->Cai = Cai_init;
    sv->F = F_init;
    sv->F2 = F2_init;
    sv->FCaSS = FCaSS_init;
    sv->H = H_init;
    sv->J = J_init;
    sv->Ki = Ki_init;
    sv->M = M_init;
    sv->Nai = Nai_init;
    sv->R = R_init;
    sv->R_ = R__init;
    sv->S = S_init;
    V = V_init;
    sv->Xr1 = Xr1_init;
    sv->Xr2 = Xr2_init;
    sv->Xs = Xs_init;
    double D_inf = (1./(1.+(exp((((-8.+p->D_CaL_off)-(V))/7.5)))));
    double Eca = ((0.5*RTONF)*(log((p->Cao/sv->Cai))));
    double Ek = (RTONF*(log((p->Ko/sv->Ki))));
    double Eks = (RTONF*(log(((p->Ko+(p->pKNa*p->Nao))/(sv->Ki+(p->pKNa*sv->Nai))))));
    double Ena = (RTONF*(log((p->Nao/sv->Nai))));
    double IpCa = ((p->GpCa*sv->Cai)/(p->KpCa+sv->Cai));
    double a1 = ((((sv->GCaL*p->Fconst)*F_RT)*4.)*((V==15.) ? ((1./2.)*F_RT) : ((V-(15.))/(expm1(((2.*(V-(15.)))*F_RT))))));
    double a2 = (0.25*(exp(((2.*(V-(15.)))*F_RT))));
    double den = (pmf_INaCa/(1.+(p->ksat*(exp((((p->n-(1.))*V)*F_RT))))));
    double rec_iNaK = (1./((1.+(0.1245*(exp(((-0.1*V)*F_RT)))))+(0.0353*(exp(((-V)*F_RT))))));
    double rec_ipK = (1./(1.+(exp(((25.-(V))/5.98)))));
    double D_init = D_inf;
    double ICaL_A = (a1*a2);
    double ICaL_B = (a1*p->Cao);
    double IKr = ((((sv->GKr*sqrt_Ko)*sv->Xr1)*sv->Xr2)*(V-(Ek)));
    double IKs = (((sv->GKs*sv->Xs)*sv->Xs)*(V-(Eks)));
    double INa = ((((((p->GNa*sv->M)*sv->M)*sv->M)*sv->H)*sv->J)*(V-(Ena)));
    double INaCa_A = ((den*p->Cao)*(exp(((p->n*V)*F_RT))));
    double INaCa_B = (((den*(exp((((p->n-(1.))*V)*F_RT))))*Nao3)*2.5);
    double INaK = ((pmf_INaK*(sv->Nai/(sv->Nai+p->KmNa)))*rec_iNaK);
    double IbCa = (p->GbCa*(V-(Eca)));
    double IbNa = (p->GbNa*(V-(Ena)));
    double IpK = ((p->GpK*rec_ipK)*(V-(Ek)));
    double Ito = (((sv->Gto*sv->R)*sv->S)*(V-(Ek)));
    double VEk = (V-(Ek));
    sv->D = D_init;
    double INaCa = ((((INaCa_A*sv->Nai)*sv->Nai)*sv->Nai)-((INaCa_B*sv->Cai)));
    double a_K1 = (0.1/(1.+(exp((0.06*(VEk-(200.)))))));
    double b_K1 = (((3.*(exp((0.0002*(VEk+100.)))))+(exp((0.1*(VEk-(10.))))))/(1.+(exp((-0.5*VEk)))));
    double ICaL = ((((sv->D*sv->F)*sv->F2)*sv->FCaSS)*((ICaL_A*sv->CaSS)-(ICaL_B)));
    double rec_iK1 = (a_K1/(a_K1+b_K1));
    double IK1 = ((p->GK1*rec_iK1)*(V-(Ek)));
    Iion = (((((((((((IKr+IKs)+IK1)+Ito)+INa)+IbNa)+ICaL)+IbCa)+INaK)+INaCa)+IpCa)+IpK);
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_tenTusscherPanfilov(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  tenTusscherPanfilov_Params *p  = (tenTusscherPanfilov_Params *)IF->params;
  tenTusscherPanfilov_state *sv_base = (tenTusscherPanfilov_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t KmNai3 = ((p->KmNai*p->KmNai)*p->KmNai);
  GlobalData_t Nao3 = ((p->Nao*p->Nao)*p->Nao);
  GlobalData_t RTONF = ((p->Rconst*p->T)/p->Fconst);
  GlobalData_t invKmCa_Cao = (1./(p->KmCa+p->Cao));
  GlobalData_t inverseVcF = (1./(p->Vc*p->Fconst));
  GlobalData_t inverseVcF2 = (1./((2.*p->Vc)*p->Fconst));
  GlobalData_t inverseVssF2 = (1./((2.*p->Vss)*p->Fconst));
  GlobalData_t pmf_INaK = (p->knak*(p->Ko/(p->Ko+p->KmK)));
  GlobalData_t sqrt_Ko = (sqrt((p->Ko/5.4)));
  GlobalData_t F_RT = (1./RTONF);
  GlobalData_t invKmNai3_Nao3 = (1./(KmNai3+Nao3));
  GlobalData_t invVcF_Cm = (inverseVcF*p->CAPACITANCE);
  GlobalData_t pmf_INaCa = ((p->knaca*invKmNai3_Nao3)*invKmCa_Cao);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    tenTusscherPanfilov_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t CaSS_row[NROWS_CaSS];
    LUT_interpRow(&IF->tables[CaSS_TAB], sv->CaSS, __i, CaSS_row);
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t Eca = ((0.5*RTONF)*(log((p->Cao/sv->Cai))));
    GlobalData_t Ek = (RTONF*(log((p->Ko/sv->Ki))));
    GlobalData_t Eks = (RTONF*(log(((p->Ko+(p->pKNa*p->Nao))/(sv->Ki+(p->pKNa*sv->Nai))))));
    GlobalData_t Ena = (RTONF*(log((p->Nao/sv->Nai))));
    GlobalData_t IpCa = ((p->GpCa*sv->Cai)/(p->KpCa+sv->Cai));
    GlobalData_t a1 = ((((sv->GCaL*p->Fconst)*F_RT)*4.)*((V==15.) ? ((1./2.)*F_RT) : ((V-(15.))/(expm1(((2.*(V-(15.)))*F_RT))))));
    GlobalData_t ICaL_A = (a1*V_row[a2_idx]);
    GlobalData_t ICaL_B = (a1*p->Cao);
    GlobalData_t IKr = ((((sv->GKr*sqrt_Ko)*sv->Xr1)*sv->Xr2)*(V-(Ek)));
    GlobalData_t IKs = (((sv->GKs*sv->Xs)*sv->Xs)*(V-(Eks)));
    GlobalData_t INa = ((((((p->GNa*sv->M)*sv->M)*sv->M)*sv->H)*sv->J)*(V-(Ena)));
    GlobalData_t INaK = ((pmf_INaK*(sv->Nai/(sv->Nai+p->KmNa)))*V_row[rec_iNaK_idx]);
    GlobalData_t IbCa = (p->GbCa*(V-(Eca)));
    GlobalData_t IbNa = (p->GbNa*(V-(Ena)));
    GlobalData_t IpK = ((p->GpK*V_row[rec_ipK_idx])*(V-(Ek)));
    GlobalData_t Ito = (((sv->Gto*sv->R)*sv->S)*(V-(Ek)));
    GlobalData_t VEk = (V-(Ek));
    LUT_data_t VEk_row[NROWS_VEk];
    LUT_interpRow(&IF->tables[VEk_TAB], VEk, __i, VEk_row);
    GlobalData_t ICaL = ((((sv->D*sv->F)*sv->F2)*sv->FCaSS)*((ICaL_A*sv->CaSS)-(ICaL_B)));
    GlobalData_t INaCa = ((((V_row[INaCa_A_idx]*sv->Nai)*sv->Nai)*sv->Nai)-((V_row[INaCa_B_idx]*sv->Cai)));
    GlobalData_t IK1 = ((p->GK1*VEk_row[rec_iK1_idx])*(V-(Ek)));
    Iion = (((((((((((IKr+IKs)+IK1)+Ito)+INa)+IbNa)+ICaL)+IbCa)+INaK)+INaCa)+IpCa)+IpK);
    
    
    //Complete Forward Euler Update
    GlobalData_t Ileak = (p->Vleak*(sv->CaSR-(sv->Cai)));
    GlobalData_t Iup = (p->Vmaxup/(1.+((p->Kup*p->Kup)/(sv->Cai*sv->Cai))));
    GlobalData_t Ixfer = (Vxfer*(sv->CaSS-(sv->Cai)));
    GlobalData_t diff_Ki = ((-(((((IK1+Ito)+IKr)+IKs)-((2.*INaK)))+IpK))*invVcF_Cm);
    GlobalData_t diff_Nai = ((-(((INa+IbNa)+(3.*INaK))+(3.*INaCa)))*invVcF_Cm);
    GlobalData_t kCaSR = (maxsr-(((maxsr-(minsr))/(1.+((EC/sv->CaSR)*(EC/sv->CaSR))))));
    GlobalData_t diff_Cai = (((Ixfer-(((((IbCa+IpCa)-((2.*INaCa)))*inverseVcF2)*p->CAPACITANCE)))-(((Iup-(Ileak))*(p->Vsr/p->Vc))))/(1.+(((p->Bufc*p->Kbufc)/(p->Kbufc+sv->Cai))/(p->Kbufc+sv->Cai))));
    GlobalData_t diff_R_ = ((k4*(1.-(sv->R_)))-((((k2_*kCaSR)*sv->CaSS)*sv->R_)));
    GlobalData_t k1 = (k1_/kCaSR);
    GlobalData_t O = (((k1*CaSS_row[CaSS2_idx])*sv->R_)/(k3+(k1*CaSS_row[CaSS2_idx])));
    GlobalData_t Irel = ((p->Vrel*O)*(sv->CaSR-(sv->CaSS)));
    GlobalData_t diff_CaSR = (((Iup-(Irel))-(Ileak))/(1.+(((p->Bufsr*p->Kbufsr)/(sv->CaSR+p->Kbufsr))/(sv->CaSR+p->Kbufsr))));
    GlobalData_t diff_CaSS = (((((-Ixfer)*(p->Vc/p->Vss))+(Irel*(p->Vsr/p->Vss)))+(((-ICaL)*inverseVssF2)*p->CAPACITANCE))/(1.+(((p->Bufss*p->Kbufss)/(sv->CaSS+p->Kbufss))/(sv->CaSS+p->Kbufss))));
    GlobalData_t CaSR_new = sv->CaSR+diff_CaSR*dt;
    GlobalData_t CaSS_new = sv->CaSS+diff_CaSS*dt;
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t R__new = sv->R_+diff_R_*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t FCaSS_rush_larsen_B = CaSS_row[FCaSS_rush_larsen_B_idx];
    GlobalData_t R_rush_larsen_B = V_row[R_rush_larsen_B_idx];
    GlobalData_t S_rush_larsen_B = V_row[S_rush_larsen_B_idx];
    GlobalData_t D_rush_larsen_B = V_row[D_rush_larsen_B_idx];
    GlobalData_t F2_rush_larsen_B = V_row[F2_rush_larsen_B_idx];
    GlobalData_t FCaSS_rush_larsen_A = CaSS_row[FCaSS_rush_larsen_A_idx];
    GlobalData_t H_rush_larsen_B = V_row[H_rush_larsen_B_idx];
    GlobalData_t J_rush_larsen_B = V_row[J_rush_larsen_B_idx];
    GlobalData_t M_rush_larsen_B = V_row[M_rush_larsen_B_idx];
    GlobalData_t R_rush_larsen_A = V_row[R_rush_larsen_A_idx];
    GlobalData_t S_rush_larsen_A = V_row[S_rush_larsen_A_idx];
    GlobalData_t Xr1_rush_larsen_B = V_row[Xr1_rush_larsen_B_idx];
    GlobalData_t Xr2_rush_larsen_B = V_row[Xr2_rush_larsen_B_idx];
    GlobalData_t Xs_rush_larsen_B = V_row[Xs_rush_larsen_B_idx];
    GlobalData_t D_rush_larsen_A = V_row[D_rush_larsen_A_idx];
    GlobalData_t F2_rush_larsen_A = V_row[F2_rush_larsen_A_idx];
    GlobalData_t F_rush_larsen_B = V_row[F_rush_larsen_B_idx];
    GlobalData_t H_rush_larsen_A = V_row[H_rush_larsen_A_idx];
    GlobalData_t J_rush_larsen_A = V_row[J_rush_larsen_A_idx];
    GlobalData_t M_rush_larsen_A = V_row[M_rush_larsen_A_idx];
    GlobalData_t Xr1_rush_larsen_A = V_row[Xr1_rush_larsen_A_idx];
    GlobalData_t Xr2_rush_larsen_A = V_row[Xr2_rush_larsen_A_idx];
    GlobalData_t Xs_rush_larsen_A = V_row[Xs_rush_larsen_A_idx];
    GlobalData_t F_rush_larsen_A = V_row[F_rush_larsen_A_idx];
    GlobalData_t D_new = D_rush_larsen_A+D_rush_larsen_B*sv->D;
    GlobalData_t F_new = F_rush_larsen_A+F_rush_larsen_B*sv->F;
    GlobalData_t F2_new = F2_rush_larsen_A+F2_rush_larsen_B*sv->F2;
    GlobalData_t FCaSS_new = FCaSS_rush_larsen_A+FCaSS_rush_larsen_B*sv->FCaSS;
    GlobalData_t H_new = H_rush_larsen_A+H_rush_larsen_B*sv->H;
    GlobalData_t J_new = J_rush_larsen_A+J_rush_larsen_B*sv->J;
    GlobalData_t M_new = M_rush_larsen_A+M_rush_larsen_B*sv->M;
    GlobalData_t R_new = R_rush_larsen_A+R_rush_larsen_B*sv->R;
    GlobalData_t S_new = S_rush_larsen_A+S_rush_larsen_B*sv->S;
    GlobalData_t Xr1_new = Xr1_rush_larsen_A+Xr1_rush_larsen_B*sv->Xr1;
    GlobalData_t Xr2_new = Xr2_rush_larsen_A+Xr2_rush_larsen_B*sv->Xr2;
    GlobalData_t Xs_new = Xs_rush_larsen_A+Xs_rush_larsen_B*sv->Xs;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->CaSR = CaSR_new;
    sv->CaSS = CaSS_new;
    sv->Cai = Cai_new;
    sv->D = D_new;
    sv->F = F_new;
    sv->F2 = F2_new;
    sv->FCaSS = FCaSS_new;
    sv->H = H_new;
    Iion = Iion;
    sv->J = J_new;
    sv->Ki = Ki_new;
    sv->M = M_new;
    sv->Nai = Nai_new;
    sv->R = R_new;
    sv->R_ = R__new;
    sv->S = S_new;
    sv->Xr1 = Xr1_new;
    sv->Xr2 = Xr2_new;
    sv->Xs = Xs_new;
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_tenTusscherPanfilov(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("tenTusscherPanfilov_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->CaSR\n"
        "sv->CaSS\n"
        "sv->Cai\n"
        "ICaL\n"
        "IK1\n"
        "IKr\n"
        "IKs\n"
        "INa\n"
        "INaCa\n"
        "INaK\n"
        "IbCa\n"
        "IbNa\n"
        "IpCa\n"
        "IpK\n"
        "Ito\n"
        "V\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  tenTusscherPanfilov_Params *p  = (tenTusscherPanfilov_Params *)IF->params;

  tenTusscherPanfilov_state *sv_base = (tenTusscherPanfilov_state *)IF->sv_tab.y;
  tenTusscherPanfilov_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t KmNai3 = ((p->KmNai*p->KmNai)*p->KmNai);
  GlobalData_t Nao3 = ((p->Nao*p->Nao)*p->Nao);
  GlobalData_t RTONF = ((p->Rconst*p->T)/p->Fconst);
  GlobalData_t invKmCa_Cao = (1./(p->KmCa+p->Cao));
  GlobalData_t inverseVcF = (1./(p->Vc*p->Fconst));
  GlobalData_t inverseVcF2 = (1./((2.*p->Vc)*p->Fconst));
  GlobalData_t inverseVssF2 = (1./((2.*p->Vss)*p->Fconst));
  GlobalData_t pmf_INaK = (p->knak*(p->Ko/(p->Ko+p->KmK)));
  GlobalData_t sqrt_Ko = (sqrt((p->Ko/5.4)));
  GlobalData_t F_RT = (1./RTONF);
  GlobalData_t invKmNai3_Nao3 = (1./(KmNai3+Nao3));
  GlobalData_t invVcF_Cm = (inverseVcF*p->CAPACITANCE);
  GlobalData_t pmf_INaCa = ((p->knaca*invKmNai3_Nao3)*invKmCa_Cao);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e-3;
  
  
  GlobalData_t Eca = ((0.5*RTONF)*(log((p->Cao/sv->Cai))));
  GlobalData_t Ek = (RTONF*(log((p->Ko/sv->Ki))));
  GlobalData_t Eks = (RTONF*(log(((p->Ko+(p->pKNa*p->Nao))/(sv->Ki+(p->pKNa*sv->Nai))))));
  GlobalData_t Ena = (RTONF*(log((p->Nao/sv->Nai))));
  GlobalData_t IpCa = ((p->GpCa*sv->Cai)/(p->KpCa+sv->Cai));
  GlobalData_t a1 = ((((sv->GCaL*p->Fconst)*F_RT)*4.)*((V==15.) ? ((1./2.)*F_RT) : ((V-(15.))/(expm1(((2.*(V-(15.)))*F_RT))))));
  GlobalData_t a2 = (0.25*(exp(((2.*(V-(15.)))*F_RT))));
  GlobalData_t den = (pmf_INaCa/(1.+(p->ksat*(exp((((p->n-(1.))*V)*F_RT))))));
  GlobalData_t rec_iNaK = (1./((1.+(0.1245*(exp(((-0.1*V)*F_RT)))))+(0.0353*(exp(((-V)*F_RT))))));
  GlobalData_t rec_ipK = (1./(1.+(exp(((25.-(V))/5.98)))));
  GlobalData_t ICaL_A = (a1*a2);
  GlobalData_t ICaL_B = (a1*p->Cao);
  GlobalData_t IKr = ((((sv->GKr*sqrt_Ko)*sv->Xr1)*sv->Xr2)*(V-(Ek)));
  GlobalData_t IKs = (((sv->GKs*sv->Xs)*sv->Xs)*(V-(Eks)));
  GlobalData_t INa = ((((((p->GNa*sv->M)*sv->M)*sv->M)*sv->H)*sv->J)*(V-(Ena)));
  GlobalData_t INaCa_A = ((den*p->Cao)*(exp(((p->n*V)*F_RT))));
  GlobalData_t INaCa_B = (((den*(exp((((p->n-(1.))*V)*F_RT))))*Nao3)*2.5);
  GlobalData_t INaK = ((pmf_INaK*(sv->Nai/(sv->Nai+p->KmNa)))*rec_iNaK);
  GlobalData_t IbCa = (p->GbCa*(V-(Eca)));
  GlobalData_t IbNa = (p->GbNa*(V-(Ena)));
  GlobalData_t IpK = ((p->GpK*rec_ipK)*(V-(Ek)));
  GlobalData_t Ito = (((sv->Gto*sv->R)*sv->S)*(V-(Ek)));
  GlobalData_t VEk = (V-(Ek));
  GlobalData_t ICaL = ((((sv->D*sv->F)*sv->F2)*sv->FCaSS)*((ICaL_A*sv->CaSS)-(ICaL_B)));
  GlobalData_t INaCa = ((((INaCa_A*sv->Nai)*sv->Nai)*sv->Nai)-((INaCa_B*sv->Cai)));
  GlobalData_t a_K1 = (0.1/(1.+(exp((0.06*(VEk-(200.)))))));
  GlobalData_t b_K1 = (((3.*(exp((0.0002*(VEk+100.)))))+(exp((0.1*(VEk-(10.))))))/(1.+(exp((-0.5*VEk)))));
  GlobalData_t rec_iK1 = (a_K1/(a_K1+b_K1));
  GlobalData_t IK1 = ((p->GK1*rec_iK1)*(V-(Ek)));
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->CaSR);
  fprintf(file, "%4.12f\t", sv->CaSS);
  fprintf(file, "%4.12f\t", sv->Cai);
  fprintf(file, "%4.12f\t", ICaL);
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", IKr);
  fprintf(file, "%4.12f\t", IKs);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", IbCa);
  fprintf(file, "%4.12f\t", IbNa);
  fprintf(file, "%4.12f\t", IpCa);
  fprintf(file, "%4.12f\t", IpK);
  fprintf(file, "%4.12f\t", Ito);
  fprintf(file, "%4.12f\t", V);
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e3;
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        