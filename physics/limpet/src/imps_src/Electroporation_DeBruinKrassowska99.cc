// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: DeBruin KA, Krassowska W
*  Year: 1999
*  Title: Modeling electroporation in a single cell. I. Effects Of field strength and rest potential
*  Journal: Biophys J.,77(3):1213-24
*  DOI: 10.1016/S0006-3495(99)76973-0
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Electroporation_DeBruinKrassowska99.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Electroporation_DeBruinKrassowska99(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Electroporation_DeBruinKrassowska99( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants



void initialize_params_Electroporation_DeBruinKrassowska99( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Electroporation_DeBruinKrassowska99_Params *p = (Electroporation_DeBruinKrassowska99_Params *)IF->params;

  // Compute the regional constants
  {
    p->G0 = 0.;
    p->alpha = 2.5e-3;
    p->beta = 2.5e-5;
    p->gamma = 1.0e-9;
    p->vrest = 0.;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Electroporation_DeBruinKrassowska99( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Electroporation_DeBruinKrassowska99_Params *p = (Electroporation_DeBruinKrassowska99_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double g_init = p->G0;

}



void    initialize_sv_Electroporation_DeBruinKrassowska99( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Electroporation_DeBruinKrassowska99_Params *p = (Electroporation_DeBruinKrassowska99_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Electroporation_DeBruinKrassowska99_state) );
  Electroporation_DeBruinKrassowska99_state *sv_base = (Electroporation_DeBruinKrassowska99_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double g_init = p->G0;
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Electroporation_DeBruinKrassowska99_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->g = g_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Electroporation_DeBruinKrassowska99(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Electroporation_DeBruinKrassowska99_Params *p  = (Electroporation_DeBruinKrassowska99_Params *)IF->params;
  Electroporation_DeBruinKrassowska99_state *sv_base = (Electroporation_DeBruinKrassowska99_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t g_init = p->G0;
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Electroporation_DeBruinKrassowska99_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    Iion = (Iion+(V*sv->g));
    
    
    //Complete Forward Euler Update
    GlobalData_t dvm = (V-(p->vrest));
    GlobalData_t dvm_2 = (dvm*dvm);
    GlobalData_t diff_g = ((p->alpha*(exp((p->beta*dvm_2))))*(1.-((exp(((-p->gamma)*dvm_2))))));
    GlobalData_t g_new = sv->g+diff_g*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    sv->g = g_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        