// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Luo CH, Rudy Y
*  Year: 1991
*  Title: A model of the ventricular cardiac action potential. Depolarization, repolarization, and their interaction
*  Journal: Circulation Research,68(6),1501-1526
*  DOI: 10.1161/01.RES.68.6.1501
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "LuoRudy91.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_LuoRudy91(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_LuoRudy91( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define ENa (GlobalData_t)(54.4)
#define Eb (GlobalData_t)(-59.87)
#define V_init (GlobalData_t)(-84.)



void initialize_params_LuoRudy91( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  LuoRudy91_Params *p = (LuoRudy91_Params *)IF->params;

  // Compute the regional constants
  {
    p->Cai_init = 2e-4;
    p->GK1bar = 0.604;
    p->GKbar = .282;
    p->GKp = 0.0183;
    p->GNa = 23.0;
    p->Gb = 0.03921;
    p->Gsi = 0.09;
    p->Ki = 145.;
    p->Ko = 5.4;
    p->Nai = 18.;
    p->Nao = 140.;
    p->tau_d_factor = 1.0;
    p->tau_f_factor = 1.0;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_LuoRudy91;

}


// Define the parameters for the lookup tables
enum Tables {
  Cai_TAB,
  V_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum Cai_TableIndex {
  Esi_idx,
  NROWS_Cai
};

enum V_TableIndex {
  IK1_idx,
  IKp_idx,
  Ib_idx,
  X_rush_larsen_A_idx,
  X_rush_larsen_B_idx,
  Xi_idx,
  d_rush_larsen_A_idx,
  d_rush_larsen_B_idx,
  f_rush_larsen_A_idx,
  f_rush_larsen_B_idx,
  h_rush_larsen_A_idx,
  h_rush_larsen_B_idx,
  j_rush_larsen_A_idx,
  j_rush_larsen_B_idx,
  m_rush_larsen_A_idx,
  m_rush_larsen_B_idx,
  NROWS_V
};



void construct_tables_LuoRudy91( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  LuoRudy91_Params *p = (LuoRudy91_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double EK = (26.516987*(log(((p->Ko+(0.01833*p->Nao))/(p->Ki+(0.01833*p->Nai))))));
  double EK1 = (26.516987*(log((p->Ko/p->Ki))));
  double EKp = (26.516987*(log((p->Ko/p->Ki))));
  double GK = (p->GKbar*(sqrt((p->Ko/5.4))));
  double GK1 = (p->GK1bar*(sqrt((p->Ko/5.4))));
  
  // Create the Cai lookup table
  LUT* Cai_tab = &IF->tables[Cai_TAB];
  LUT_alloc(Cai_tab, NROWS_Cai, 1e-6, 1e-1, 1e-6, "LuoRudy91 Cai");
  for (int __i=Cai_tab->mn_ind; __i<=Cai_tab->mx_ind; __i++) {
    double Cai = Cai_tab->res*__i;
    LUT_data_t* Cai_row = Cai_tab->tab[__i];
    Cai_row[Esi_idx] = (7.7-((13.0287*(log(Cai)))));
  }
  check_LUT(Cai_tab);
  
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -800, 800, 0.05, "LuoRudy91 V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    V_row[Ib_idx] = (p->Gb*(V-(Eb)));
    double Kp = (1./(1.+(exp(((7.488-(V))/5.98)))));
    V_row[Xi_idx] = ((V<=-100.) ? 1. : ((V==-77.) ? ((2.837*0.04)/(exp((0.04*(V+35.))))) : ((2.837*((exp((0.04*(V+77.))))-(1.)))/((V+77.)*(exp((0.04*(V+35.))))))));
    double a_X = ((0.0005*(exp((0.083*(V+50.)))))/(1.+(exp((0.057*(V+50.))))));
    double a_d = ((0.095*(exp((-0.01*(V-(5.))))))/(1.+(exp((-0.072*(V-(5.)))))));
    double a_f = ((0.012*(exp((-0.008*(V+28.)))))/(1.+(exp((0.15*(V+28.))))));
    double a_h = ((V>=-40.) ? 0. : (0.135*(exp(((80.+V)/-6.8)))));
    double a_j = ((V>=-40.) ? 0. : ((((-1.2714e5*(exp((0.2444*V))))-((3.474e-5*(exp((-0.04391*V))))))*(V+37.78))/(1.+(exp((0.311*(V+79.23)))))));
    double a_m = ((0.32*(V+47.13))/(1.0-((exp((-0.1*(V+47.13)))))));
    double aa_K1 = (1.02/(1.+(exp((0.2385*((V-(EK1))-(59.215)))))));
    double b_X = ((0.0013*(exp((-0.06*(V+20.)))))/(1.+(exp((-0.04*(V+20.))))));
    double b_d = ((0.07*(exp((-0.017*(V+44.)))))/(1.+(exp((0.05*(V+44.))))));
    double b_f = ((0.0065*(exp((-0.02*(V+30.)))))/(1.+(exp((-0.2*(V+30.))))));
    double b_h = ((V>=-40.) ? (1./(0.13*(1.+(exp(((V+10.66)/-11.1)))))) : ((3.56*(exp((0.079*V))))+(3.1e5*(exp((0.35*V))))));
    double b_j = ((V>=-40.) ? ((0.3*(exp((-2.535e-7*V))))/(1.+(exp((-0.1*(V+32.0)))))) : ((0.1212*(exp((-0.01052*V))))/(1.+(exp((-0.1378*(V+40.14)))))));
    double b_m = (0.08*(exp(((-V)/11.0))));
    double bb_K1 = (((0.49124*(exp((0.08032*((V-(EK1))+5.476)))))+(exp((0.06175*((V-(EK1))-(594.31))))))/(1.+(exp((-0.5143*((V-(EK1))+4.753))))));
    V_row[IKp_idx] = ((p->GKp*Kp)*(V-(EKp)));
    double K1_st = (aa_K1/(aa_K1+bb_K1));
    V_row[X_rush_larsen_A_idx] = (((-a_X)/(a_X+b_X))*(expm1(((-dt)*(a_X+b_X)))));
    V_row[X_rush_larsen_B_idx] = (exp(((-dt)*(a_X+b_X))));
    V_row[d_rush_larsen_A_idx] = (((-a_d)/(a_d+b_d))*(expm1(((-dt)*(a_d+b_d)))));
    V_row[d_rush_larsen_B_idx] = (exp(((-dt)*(a_d+b_d))));
    V_row[f_rush_larsen_A_idx] = (((-a_f)/(a_f+b_f))*(expm1(((-dt)*(a_f+b_f)))));
    V_row[f_rush_larsen_B_idx] = (exp(((-dt)*(a_f+b_f))));
    V_row[h_rush_larsen_A_idx] = (((-a_h)/(a_h+b_h))*(expm1(((-dt)*(a_h+b_h)))));
    V_row[h_rush_larsen_B_idx] = (exp(((-dt)*(a_h+b_h))));
    V_row[j_rush_larsen_A_idx] = (((-a_j)/(a_j+b_j))*(expm1(((-dt)*(a_j+b_j)))));
    V_row[j_rush_larsen_B_idx] = (exp(((-dt)*(a_j+b_j))));
    V_row[m_rush_larsen_A_idx] = (((-a_m)/(a_m+b_m))*(expm1(((-dt)*(a_m+b_m)))));
    V_row[m_rush_larsen_B_idx] = (exp(((-dt)*(a_m+b_m))));
    V_row[IK1_idx] = ((GK1*K1_st)*(V-(EK1)));
  }
  check_LUT(V_tab);
  

}



void    initialize_sv_LuoRudy91( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  LuoRudy91_Params *p = (LuoRudy91_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(LuoRudy91_state) );
  LuoRudy91_state *sv_base = (LuoRudy91_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double EK = (26.516987*(log(((p->Ko+(0.01833*p->Nao))/(p->Ki+(0.01833*p->Nai))))));
  double EK1 = (26.516987*(log((p->Ko/p->Ki))));
  double EKp = (26.516987*(log((p->Ko/p->Ki))));
  double GK = (p->GKbar*(sqrt((p->Ko/5.4))));
  double GK1 = (p->GK1bar*(sqrt((p->Ko/5.4))));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    LuoRudy91_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    // Initialize the rest of the nodal variables
    sv->Cai = p->Cai_init;
    V = V_init;
    double Esi = (7.7-((13.0287*(log(sv->Cai)))));
    double Ib = (p->Gb*(V-(Eb)));
    double Kp = (1./(1.+(exp(((7.488-(V))/5.98)))));
    double Xi = ((V<=-100.) ? 1. : ((V==-77.) ? ((2.837*0.04)/(exp((0.04*(V+35.))))) : ((2.837*((exp((0.04*(V+77.))))-(1.)))/((V+77.)*(exp((0.04*(V+35.))))))));
    double a_X = ((0.0005*(exp((0.083*(V+50.)))))/(1.+(exp((0.057*(V+50.))))));
    double a_d = ((0.095*(exp((-0.01*(V-(5.))))))/(1.+(exp((-0.072*(V-(5.)))))));
    double a_f = ((0.012*(exp((-0.008*(V+28.)))))/(1.+(exp((0.15*(V+28.))))));
    double a_h = ((V>=-40.) ? 0. : (0.135*(exp(((80.+V)/-6.8)))));
    double a_j = ((V>=-40.) ? 0. : ((((-1.2714e5*(exp((0.2444*V))))-((3.474e-5*(exp((-0.04391*V))))))*(V+37.78))/(1.+(exp((0.311*(V+79.23)))))));
    double a_m = ((0.32*(V+47.13))/(1.0-((exp((-0.1*(V+47.13)))))));
    double aa_K1 = (1.02/(1.+(exp((0.2385*((V-(EK1))-(59.215)))))));
    double b_X = ((0.0013*(exp((-0.06*(V+20.)))))/(1.+(exp((-0.04*(V+20.))))));
    double b_d = ((0.07*(exp((-0.017*(V+44.)))))/(1.+(exp((0.05*(V+44.))))));
    double b_f = ((0.0065*(exp((-0.02*(V+30.)))))/(1.+(exp((-0.2*(V+30.))))));
    double b_h = ((V>=-40.) ? (1./(0.13*(1.+(exp(((V+10.66)/-11.1)))))) : ((3.56*(exp((0.079*V))))+(3.1e5*(exp((0.35*V))))));
    double b_j = ((V>=-40.) ? ((0.3*(exp((-2.535e-7*V))))/(1.+(exp((-0.1*(V+32.0)))))) : ((0.1212*(exp((-0.01052*V))))/(1.+(exp((-0.1378*(V+40.14)))))));
    double b_m = (0.08*(exp(((-V)/11.0))));
    double bb_K1 = (((0.49124*(exp((0.08032*((V-(EK1))+5.476)))))+(exp((0.06175*((V-(EK1))-(594.31))))))/(1.+(exp((-0.5143*((V-(EK1))+4.753))))));
    double IKp = ((p->GKp*Kp)*(V-(EKp)));
    double K1_st = (aa_K1/(aa_K1+bb_K1));
    double X_init = (a_X/(a_X+b_X));
    double d_init = (a_d/(a_d+b_d));
    double f_init = (a_f/(a_f+b_f));
    double h_init = (a_h/(a_h+b_h));
    double j_init = (a_j/(a_j+b_j));
    double m_init = (a_m/(a_m+b_m));
    double IK1 = ((GK1*K1_st)*(V-(EK1)));
    sv->X = X_init;
    sv->d = d_init;
    sv->f = f_init;
    sv->h = h_init;
    sv->j = j_init;
    sv->m = m_init;
    double IK = (((GK*sv->X)*Xi)*(V-(EK)));
    double INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(ENa)));
    double Isi = (((p->Gsi*sv->d)*sv->f)*(V-(Esi)));
    Iion = (((((INa+Isi)+IK)+IK1)+IKp)+Ib);
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_LuoRudy91(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  LuoRudy91_Params *p  = (LuoRudy91_Params *)IF->params;
  LuoRudy91_state *sv_base = (LuoRudy91_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t EK = (26.516987*(log(((p->Ko+(0.01833*p->Nao))/(p->Ki+(0.01833*p->Nai))))));
  GlobalData_t EK1 = (26.516987*(log((p->Ko/p->Ki))));
  GlobalData_t EKp = (26.516987*(log((p->Ko/p->Ki))));
  GlobalData_t GK = (p->GKbar*(sqrt((p->Ko/5.4))));
  GlobalData_t GK1 = (p->GK1bar*(sqrt((p->Ko/5.4))));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    LuoRudy91_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e-3;
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Cai_row[NROWS_Cai];
    LUT_interpRow(&IF->tables[Cai_TAB], sv->Cai, __i, Cai_row);
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(ENa)));
    GlobalData_t IK = (((GK*sv->X)*V_row[Xi_idx])*(V-(EK)));
    GlobalData_t Isi = (((p->Gsi*sv->d)*sv->f)*(V-(Cai_row[Esi_idx])));
    Iion = (((((INa+Isi)+IK)+V_row[IK1_idx])+V_row[IKp_idx])+V_row[Ib_idx]);
    
    
    //Complete Forward Euler Update
    GlobalData_t diff_Cai = ((V<150.0) ? ((-1.e-4*Isi)+(0.07*(1.e-4-(sv->Cai)))) : 0.);
    GlobalData_t Cai_new = sv->Cai+diff_Cai*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t X_rush_larsen_A = V_row[X_rush_larsen_A_idx];
    GlobalData_t X_rush_larsen_B = V_row[X_rush_larsen_B_idx];
    GlobalData_t d_rush_larsen_A = V_row[d_rush_larsen_A_idx];
    GlobalData_t d_rush_larsen_B = V_row[d_rush_larsen_B_idx];
    GlobalData_t f_rush_larsen_A = V_row[f_rush_larsen_A_idx];
    GlobalData_t f_rush_larsen_B = V_row[f_rush_larsen_B_idx];
    GlobalData_t h_rush_larsen_A = V_row[h_rush_larsen_A_idx];
    GlobalData_t h_rush_larsen_B = V_row[h_rush_larsen_B_idx];
    GlobalData_t j_rush_larsen_A = V_row[j_rush_larsen_A_idx];
    GlobalData_t j_rush_larsen_B = V_row[j_rush_larsen_B_idx];
    GlobalData_t m_rush_larsen_A = V_row[m_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_B = V_row[m_rush_larsen_B_idx];
    GlobalData_t X_new = X_rush_larsen_A+X_rush_larsen_B*sv->X;
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f_new = f_rush_larsen_A+f_rush_larsen_B*sv->f;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Cai = Cai_new;
    Iion = Iion;
    sv->X = X_new;
    sv->d = d_new;
    sv->f = f_new;
    sv->h = h_new;
    sv->j = j_new;
    sv->m = m_new;
    //Change the units of external variables as appropriate.
    sv->Cai *= 1e3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_LuoRudy91(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("LuoRudy91_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->Cai\n"
        "IK\n"
        "IK1\n"
        "IKp\n"
        "INa\n"
        "Ib\n"
        "Isi\n"
        "V\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  LuoRudy91_Params *p  = (LuoRudy91_Params *)IF->params;

  LuoRudy91_state *sv_base = (LuoRudy91_state *)IF->sv_tab.y;
  LuoRudy91_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t EK = (26.516987*(log(((p->Ko+(0.01833*p->Nao))/(p->Ki+(0.01833*p->Nai))))));
  GlobalData_t EK1 = (26.516987*(log((p->Ko/p->Ki))));
  GlobalData_t EKp = (26.516987*(log((p->Ko/p->Ki))));
  GlobalData_t GK = (p->GKbar*(sqrt((p->Ko/5.4))));
  GlobalData_t GK1 = (p->GK1bar*(sqrt((p->Ko/5.4))));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e-3;
  
  
  GlobalData_t Esi = (7.7-((13.0287*(log(sv->Cai)))));
  GlobalData_t INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(ENa)));
  GlobalData_t Ib = (p->Gb*(V-(Eb)));
  GlobalData_t Kp = (1./(1.+(exp(((7.488-(V))/5.98)))));
  GlobalData_t Xi = ((V<=-100.) ? 1. : ((V==-77.) ? ((2.837*0.04)/(exp((0.04*(V+35.))))) : ((2.837*((exp((0.04*(V+77.))))-(1.)))/((V+77.)*(exp((0.04*(V+35.))))))));
  GlobalData_t aa_K1 = (1.02/(1.+(exp((0.2385*((V-(EK1))-(59.215)))))));
  GlobalData_t bb_K1 = (((0.49124*(exp((0.08032*((V-(EK1))+5.476)))))+(exp((0.06175*((V-(EK1))-(594.31))))))/(1.+(exp((-0.5143*((V-(EK1))+4.753))))));
  GlobalData_t IK = (((GK*sv->X)*Xi)*(V-(EK)));
  GlobalData_t IKp = ((p->GKp*Kp)*(V-(EKp)));
  GlobalData_t Isi = (((p->Gsi*sv->d)*sv->f)*(V-(Esi)));
  GlobalData_t K1_st = (aa_K1/(aa_K1+bb_K1));
  GlobalData_t IK1 = ((GK1*K1_st)*(V-(EK1)));
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->Cai);
  fprintf(file, "%4.12f\t", IK);
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", IKp);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", Ib);
  fprintf(file, "%4.12f\t", Isi);
  fprintf(file, "%4.12f\t", V);
  //Change the units of external variables as appropriate.
  sv->Cai *= 1e3;
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        