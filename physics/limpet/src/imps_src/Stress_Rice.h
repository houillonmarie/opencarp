// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Rice JJ, Winslow RL, Hunter WC
*  Year: 1999
*  Title: Comparison of putative cooperative mechanisms in cardiac muscle: length dependence and dynamic responses
*  Journal: Am J Physiol., 276(5):H1734-54
*  DOI: 10.1152/ajpheart.1999.276.5.H1734
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __STRESS_RICE_H__
#define __STRESS_RICE_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Stress_Rice_REQDAT delLambda_DATA_FLAG|Lambda_DATA_FLAG
#define Stress_Rice_MODDAT Tension_DATA_FLAG

struct Stress_Rice_Params {
    GlobalData_t SLmax;
    GlobalData_t SLmin;
    GlobalData_t failing;
    GlobalData_t force_coeff;

};

struct Stress_Rice_state {
    GlobalData_t N;
    GlobalData_t P;
    GlobalData_t TnCaH;
    GlobalData_t TnCaL;
    GlobalData_t XBpostr;
    GlobalData_t XBprer;
    GlobalData_t failing;
    GlobalData_t xXBpostr;
    GlobalData_t xXBprer;
    GlobalData_t __Ca_i_local;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Stress_Rice(ION_IF *);
void construct_tables_Stress_Rice(ION_IF *);
void destroy_Stress_Rice(ION_IF *);
void initialize_sv_Stress_Rice(ION_IF *, GlobalData_t**);
void compute_Stress_Rice(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
