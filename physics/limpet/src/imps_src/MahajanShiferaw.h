// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Aman Mahajan, Yohannes Shiferaw, Daisuke Sato, Ali Baher, Riccardo Olcese, Lai-Hua Xie, Ming-Jim Yang, Peng-Sheng Chen, Juan G. Restrepo, Alain Karma, Alan Garfinkel, Zhilin Qu, James N. Weiss
*  Year: 2008
*  Title: A Rabbit Ventricular Action Potential Model Replicating Cardiac Dynamics at Rapid Heart Rates
*  Journal: Biophysical Journal, 94(2), 392-410
*  DOI: 10.1529/biophysj.106.98160
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __MAHAJANSHIFERAW_H__
#define __MAHAJANSHIFERAW_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define MahajanShiferaw_REQDAT Vm_DATA_FLAG
#define MahajanShiferaw_MODDAT Iion_DATA_FLAG

struct MahajanShiferaw_Params {
    GlobalData_t BASE_Ki;
    GlobalData_t Cao;
    GlobalData_t GCaL;
    GlobalData_t GK1;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t GNaCa;
    GlobalData_t Gtof;
    GlobalData_t Gtos;
    GlobalData_t Ko;
    GlobalData_t Nai;
    GlobalData_t Nao;
    GlobalData_t T;
    GlobalData_t constant_Ki;
    GlobalData_t factorGKs;
    GlobalData_t gss;

};

struct MahajanShiferaw_state {
    GlobalData_t Cai;
    GlobalData_t GKr;
    GlobalData_t Ki;
    GlobalData_t Nai;
    GlobalData_t c1;
    GlobalData_t c2;
    GlobalData_t cj;
    GlobalData_t cjp;
    GlobalData_t cp;
    GlobalData_t cs;
    GlobalData_t factorGKs;
    Gatetype h;
    Gatetype j;
    Gatetype m;
    GlobalData_t tropi;
    GlobalData_t trops;
    GlobalData_t xi1ba;
    GlobalData_t xi1ca;
    GlobalData_t xi2ba;
    GlobalData_t xi2ca;
    GlobalData_t xir;
    Gatetype xr;
    Gatetype xs1;
    Gatetype xs2;
    Gatetype xtof;
    Gatetype xtos;
    Gatetype ytof;
    Gatetype ytos;
    GlobalData_t __acap_local;
    GlobalData_t __fr_myo_local;
    GlobalData_t __sl_i2c_local;
    GlobalData_t __vcell_local;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_MahajanShiferaw(ION_IF *);
void construct_tables_MahajanShiferaw(ION_IF *);
void destroy_MahajanShiferaw(ION_IF *);
void initialize_sv_MahajanShiferaw(ION_IF *, GlobalData_t**);
void compute_MahajanShiferaw(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
