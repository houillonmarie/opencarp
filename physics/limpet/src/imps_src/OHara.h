// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: O'Hara T, Virag L, Varro A, Rudy Y
*  Year: 2011
*  Title: Simulation of the Undiseased Human Cardiac Ventricular Action Potential: Model Formulation and Experimental Validation
*  Journal: PLOS Computational Biology, 7(5)
*  Journal: 10.1371/journal.pcbi.1002061
*  DOI: 10.1371/journal.pcbi.1002061
*  DOI: Sodium current can be changed by flag to the tenTusscherPanfilov formulation as done in Dutta et al. doi:10.1016/j.pbiomolbio.2017.02.007
*  Comment: Sodium current can be changed by flag to the tenTusscherPanfilov formulation as done in Dutta et al. doi:10.1016/j.pbiomolbio.2017.02.007
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __OHARA_H__
#define __OHARA_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define OHara_REQDAT Vm_DATA_FLAG
#define OHara_MODDAT Iion_DATA_FLAG

struct OHara_Params {
    GlobalData_t Cao;
    GlobalData_t GK1;
    GlobalData_t GKb;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t GNaCa;
    GlobalData_t GNaL;
    GlobalData_t GpCa;
    GlobalData_t Gto;
    GlobalData_t INa_Type;
    GlobalData_t Ko;
    GlobalData_t Nao;
    GlobalData_t celltype;
    GlobalData_t factorICaK;
    GlobalData_t factorICaL;
    GlobalData_t factorICaNa;
    GlobalData_t factorINaCa;
    GlobalData_t factorINaCass;
    GlobalData_t factorINaK;
    GlobalData_t factorIbCa;
    GlobalData_t factorIbNa;
    GlobalData_t jrel_stiff_const;
    GlobalData_t scale_tau_m;
    GlobalData_t shift_m_inf;
    GlobalData_t vHalfXs;
    char* flags;

};
static const char* OHara_flags = "TT2INa|ORdINa|ENDO|EPI|MCELL";


struct OHara_state {
    GlobalData_t CaMKt;
    GlobalData_t Cai;
    Gatetype Jrelnp;
    Gatetype Jrelp;
    GlobalData_t Ki;
    GlobalData_t Nai;
    Gatetype a;
    Gatetype ap;
    GlobalData_t cajsr;
    GlobalData_t cansr;
    GlobalData_t cass;
    Gatetype d;
    Gatetype fcaf;
    Gatetype fcafp;
    Gatetype fcas;
    Gatetype ff;
    Gatetype ffp;
    Gatetype fs;
    Gatetype hL;
    Gatetype hLp;
    Gatetype hTT2;
    Gatetype hf;
    Gatetype hs;
    Gatetype hsp;
    Gatetype iF;
    Gatetype iFp;
    Gatetype iS;
    Gatetype iSp;
    Gatetype j;
    Gatetype jTT2;
    Gatetype jca;
    Gatetype jp;
    GlobalData_t kss;
    Gatetype mL;
    Gatetype mORd;
    Gatetype mTT2;
    GlobalData_t nass;
    GlobalData_t nca;
    Gatetype xk1;
    Gatetype xrf;
    Gatetype xrs;
    Gatetype xs1;
    Gatetype xs2;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_OHara(ION_IF *);
void construct_tables_OHara(ION_IF *);
void destroy_OHara(ION_IF *);
void initialize_sv_OHara(ION_IF *, GlobalData_t**);
void compute_OHara(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
