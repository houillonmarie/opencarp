// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Di Francesco D. and Noble Denis
*  Year: 1985
*  Title: A model of cardiac electrICal actiVolity incorporating ionic pumps and concentration changes
*  Journal: Phil. Trans. R. Soc. Lond. B307353-398
*  DOI: 10.1098/rstb.1985.0001
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __DIFRANCESCONOBLE_H__
#define __DIFRANCESCONOBLE_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define DiFrancescoNoble_REQDAT Vm_DATA_FLAG
#define DiFrancescoNoble_MODDAT Iion_DATA_FLAG

struct DiFrancescoNoble_Params {
    GlobalData_t Cm;
    GlobalData_t GCaf;
    GlobalData_t GNa;
    GlobalData_t Gto;
    GlobalData_t factorIfunny;
    GlobalData_t kNaCa;

};

struct DiFrancescoNoble_state {
    GlobalData_t Cai;
    GlobalData_t Carel;
    GlobalData_t Caup;
    GlobalData_t Kc;
    GlobalData_t Ki;
    GlobalData_t Nai;
    Gatetype d;
    Gatetype f;
    Gatetype f2;
    Gatetype h;
    Gatetype m;
    Gatetype pp;
    Gatetype r;
    Gatetype x;
    Gatetype y;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_DiFrancescoNoble(ION_IF *);
void construct_tables_DiFrancescoNoble(ION_IF *);
void destroy_DiFrancescoNoble(ION_IF *);
void initialize_sv_DiFrancescoNoble(ION_IF *, GlobalData_t**);
void compute_DiFrancescoNoble(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
