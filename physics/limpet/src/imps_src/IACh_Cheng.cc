// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Cheng D; Tung L; Sobie E.
*  Year: 1999
*  Title: Nonuniform responses of transmembrane potential during electric field stimulation of single cardiac cells
*  Journal: Am J Physiol Heart Circ Physiol 277:351-362
*  DOI: 10.1152/ajpheart.1999.277.1.H351
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "IACh_Cheng.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_IACh_Cheng(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_IACh_Cheng( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define EC50 (GlobalData_t)(9.13652)
#define F (GlobalData_t)(96.4867)
#define Ki (GlobalData_t)(138.4)
#define Ko (GlobalData_t)(5.4)
#define R (GlobalData_t)(8.3143)
#define T (GlobalData_t)(310.)
#define a0 (GlobalData_t)(0.0517)
#define a1 (GlobalData_t)(0.4516)
#define a2 (GlobalData_t)(59.53)
#define a3 (GlobalData_t)(17.18)
#define n_ACh (GlobalData_t)(0.477811)
#define Ek (GlobalData_t)((((R*T)/F)*(log((Ko/Ki)))))



void initialize_params_IACh_Cheng( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  IACh_Cheng_Params *p = (IACh_Cheng_Params *)IF->params;

  // Compute the regional constants
  {
    p->ACh = 0.;
    p->E_max = 10.;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_IACh_Cheng( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IACh_Cheng_Params *p = (IACh_Cheng_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.

}



void    initialize_sv_IACh_Cheng( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IACh_Cheng_Params *p = (IACh_Cheng_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(IACh_Cheng_state) );
  IACh_Cheng_state *sv_base = (IACh_Cheng_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    IACh_Cheng_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_IACh_Cheng(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IACh_Cheng_Params *p  = (IACh_Cheng_Params *)IF->params;
  IACh_Cheng_state *sv_base = (IACh_Cheng_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    IACh_Cheng_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    Iion = (Iion+(((p->E_max/(1.+(EC50/(pow(p->ACh,n_ACh)))))*(a0+(a1/(1.+(exp(((V+a2)/a3)))))))*(V-(Ek))));
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        