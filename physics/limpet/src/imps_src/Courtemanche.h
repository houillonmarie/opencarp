// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Courtemanche, M., Ramirez, R. J., and Nattel, S.
*  Year: 1998
*  Title: Ionic mechanisms underlying human atrial action potential properties: insights from a mathematical model
*  Journal: Am. J. Physiol. 275(1 Pt 2), H301-H321
*  DOI: 10.1152/ajpheart.1998.275.1.H301
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __COURTEMANCHE_H__
#define __COURTEMANCHE_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Courtemanche_REQDAT Vm_DATA_FLAG
#define Courtemanche_MODDAT Iion_DATA_FLAG

struct Courtemanche_Params {
    GlobalData_t ACh;
    GlobalData_t Cao;
    GlobalData_t Cm;
    GlobalData_t GACh;
    GlobalData_t GCaL;
    GlobalData_t GK1;
    GlobalData_t GKr;
    GlobalData_t GKs;
    GlobalData_t GNa;
    GlobalData_t GbCa;
    GlobalData_t GbNa;
    GlobalData_t Gto;
    GlobalData_t Ko;
    GlobalData_t Nao;
    GlobalData_t factorGKur;
    GlobalData_t factorGrel;
    GlobalData_t factorGtr;
    GlobalData_t factorGup;
    GlobalData_t factorhGate;
    GlobalData_t factormGate;
    GlobalData_t factoroaGate;
    GlobalData_t factorxrGate;
    GlobalData_t maxCaup;
    GlobalData_t maxINaCa;
    GlobalData_t maxINaK;
    GlobalData_t maxIpCa;
    GlobalData_t maxIup;

};

struct Courtemanche_state {
    GlobalData_t Ca_rel;
    GlobalData_t Ca_up;
    GlobalData_t Cai;
    GlobalData_t Ki;
    Gatetype d;
    Gatetype f;
    Gatetype f_Ca;
    Gatetype h;
    Gatetype j;
    Gatetype m;
    Gatetype oa;
    Gatetype oi;
    Gatetype u;
    Gatetype ua;
    Gatetype ui;
    Gatetype v;
    Gatetype w;
    Gatetype xr;
    Gatetype xs;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Courtemanche(ION_IF *);
void construct_tables_Courtemanche(ION_IF *);
void destroy_Courtemanche(ION_IF *);
void initialize_sv_Courtemanche(ION_IF *, GlobalData_t**);
void compute_Courtemanche(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
