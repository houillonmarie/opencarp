// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Ashihara T, Trayanova NA
*  Year: 2004
*  Title: Asymmetry in membrane responses to electric shocks: insights from bidomain simulations
*  Journal: Biophys J., 87(4):2271-82
*  DOI: 10.1529/biophysj.104.043091
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __DEFIB_ASHIHARATRAYANOVA_H__
#define __DEFIB_ASHIHARATRAYANOVA_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Defib_AshiharaTrayanova_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG
#define Defib_AshiharaTrayanova_MODDAT Iion_DATA_FLAG

struct Defib_AshiharaTrayanova_Params {
    GlobalData_t VtakeOff;
    GlobalData_t slopeFac;

};

struct Defib_AshiharaTrayanova_state {
    GlobalData_t Ki;
    GlobalData_t __sl_i2c_local;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Defib_AshiharaTrayanova(ION_IF *);
void construct_tables_Defib_AshiharaTrayanova(ION_IF *);
void destroy_Defib_AshiharaTrayanova(ION_IF *);
void initialize_sv_Defib_AshiharaTrayanova(ION_IF *, GlobalData_t**);
void compute_Defib_AshiharaTrayanova(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
