// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Cheng D; Tung L; Sobie E.
*  Year: 1999
*  Title: Nonuniform responses of transmembrane potential during electric field stimulation of single cardiac cells
*  Journal: Am J Physiol Heart Circ Physiol 277:351-362
*  DOI: 10.1152/ajpheart.1999.277.1.H351
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __IACH_CHENG_H__
#define __IACH_CHENG_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define IACh_Cheng_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG
#define IACh_Cheng_MODDAT Iion_DATA_FLAG

struct IACh_Cheng_Params {
    GlobalData_t ACh;
    GlobalData_t E_max;

};

struct IACh_Cheng_state {
    char dummy; //PGI doesn't allow a structure of 0 bytes.

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_IACh_Cheng(ION_IF *);
void construct_tables_IACh_Cheng(ION_IF *);
void destroy_IACh_Cheng(ION_IF *);
void initialize_sv_IACh_Cheng(ION_IF *, GlobalData_t**);
void compute_IACh_Cheng(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
