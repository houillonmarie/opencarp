// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Hu Y, Gurev V, Constantino J, Bayer JD, Trayanova NA
*  Year: 2013
*  Title: Effects of Mechano-Electric Feedback on Scroll Wave Stability in Human Ventricular Fibrillation
*  Journal: PLOS ONE, 8(4), e60287
*  Journal: 10.1371/journal.pone.0060287
*  DOI: 10.1371/journal.pone.0060287
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "ISAC_Hu.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_ISAC_Hu(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_ISAC_Hu( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants



void initialize_params_ISAC_Hu( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  ISAC_Hu_Params *p = (ISAC_Hu_Params *)IF->params;

  // Compute the regional constants
  {
    p->Gst = 0.35;
    p->RP = -10.;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_ISAC_Hu;

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_ISAC_Hu( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  ISAC_Hu_Params *p = (ISAC_Hu_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.

}



void    initialize_sv_ISAC_Hu( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  ISAC_Hu_Params *p = (ISAC_Hu_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(ISAC_Hu_state) );
  ISAC_Hu_state *sv_base = (ISAC_Hu_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  GlobalData_t *lambda_ext = impdata[Lambda];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    ISAC_Hu_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    GlobalData_t lambda = lambda_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;
    lambda_ext[__i] = lambda;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_ISAC_Hu(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  ISAC_Hu_Params *p  = (ISAC_Hu_Params *)IF->params;
  ISAC_Hu_state *sv_base = (ISAC_Hu_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  GlobalData_t *lambda_ext = impdata[Lambda];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    ISAC_Hu_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    GlobalData_t lambda = lambda_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t lamb = (sqrt(((2.*lambda)+1.)));
    GlobalData_t Gns = ((lamb>1.) ? (p->Gst*(lamb-(1.))) : 0.);
    GlobalData_t Ins = ((Gns*V)-(p->RP));
    Iion = (Iion+Ins);
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;
    lambda_ext[__i] = lambda;

  }


}


void trace_ISAC_Hu(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("ISAC_Hu_trace_header.txt","wt");
    fprintf(theader->fd,
        "Ins\n"
        "lamb\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  ISAC_Hu_Params *p  = (ISAC_Hu_Params *)IF->params;

  ISAC_Hu_state *sv_base = (ISAC_Hu_state *)IF->sv_tab.y;
  ISAC_Hu_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  GlobalData_t *lambda_ext = impdata[Lambda];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  GlobalData_t lambda = lambda_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t lamb = (sqrt(((2.*lambda)+1.)));
  GlobalData_t Gns = ((lamb>1.) ? (p->Gst*(lamb-(1.))) : 0.);
  GlobalData_t Ins = ((Gns*V)-(p->RP));
  //Output the desired variables
  fprintf(file, "%4.12f\t", Ins);
  fprintf(file, "%4.12f\t", lamb);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        