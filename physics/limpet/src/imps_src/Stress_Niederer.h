// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Steven Niederer, Peter Hunter, Nicholas Smith
*  Year: 2006
*  Title: A Quantitative Analysis of Cardiac Myocyte Relaxation: A Simulation Study
*  Journal: Biophysical Journal 2006;90:1697-1722
*  DOI: 10.1529/biophysj.105.069534
*  Comment: Rat ventricular active stress model (plugin)
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __STRESS_NIEDERER_H__
#define __STRESS_NIEDERER_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Stress_Niederer_REQDAT Tension_DATA_FLAG|delLambda_DATA_FLAG|Lambda_DATA_FLAG
#define Stress_Niederer_MODDAT Tension_DATA_FLAG

struct Stress_Niederer_Params {
    GlobalData_t A_1;
    GlobalData_t A_2;
    GlobalData_t A_3;
    GlobalData_t Ca_50ref;
    GlobalData_t Ca_TRPN_Max;
    GlobalData_t K_z;
    GlobalData_t T_ref;
    GlobalData_t a;
    GlobalData_t alpha_0;
    GlobalData_t alpha_1;
    GlobalData_t alpha_2;
    GlobalData_t alpha_3;
    GlobalData_t alpha_r1;
    GlobalData_t alpha_r2;
    GlobalData_t beta_0;
    GlobalData_t beta_1;
    GlobalData_t gamma_trpn;
    GlobalData_t k_Ref_off;
    GlobalData_t k_on;
    GlobalData_t n_Hill;
    GlobalData_t n_Rel;
    GlobalData_t z_p;

};

struct Stress_Niederer_state {
    GlobalData_t Q_1;
    GlobalData_t Q_2;
    GlobalData_t Q_3;
    GlobalData_t TRPN;
    GlobalData_t z;
    GlobalData_t __Ca_i_local;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Stress_Niederer(ION_IF *);
void construct_tables_Stress_Niederer(ION_IF *);
void destroy_Stress_Niederer(ION_IF *);
void initialize_sv_Stress_Niederer(ION_IF *, GlobalData_t**);
void compute_Stress_Niederer(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
