// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Williams JC, Xu J, Lu Z, Klimas A, Chen X, Ambrosi CM, Cohen IS, Entcheva E.
*  Year: 2013
*  Title: Computational Optogenetics: Empirically-Derived Voltage- and Light-Sensitive Channelrhodopsin-2 Model
*  Journal: PLoS Comput Biol. 9(9):e1003220
*  Journal: 10.1371/journal.pcbi.1003220
*  DOI: 10.1371/journal.pcbi.1003220
*  Comment: Implementd by Boyle PM (pmjboyle@uw.edu), Williams JC, Ambrosi CM, Entcheva E, Trayanova NA descirbed in A comprehensive multiscale framework for simulating optogenetics in the heart
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "IChR2_WilliamsXu.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_IChR2_WilliamsXu(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_IChR2_WilliamsXu( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define C1_init (GlobalData_t)(1.)
#define C2_init (GlobalData_t)(0.)
#define O1_init (GlobalData_t)(0.)
#define O2_init (GlobalData_t)(0.)
#define Q10_Gd1 (GlobalData_t)(1.97)
#define Q10_Gd2 (GlobalData_t)(1.77)
#define Q10_Gr (GlobalData_t)(2.56)
#define Q10_e12d (GlobalData_t)(1.1)
#define Q10_e21d (GlobalData_t)(1.95)
#define Q10_qeff1 (GlobalData_t)(1.46)
#define Q10_qeff2 (GlobalData_t)(2.77)
#define Temp0 (GlobalData_t)(22.)
#define hc (GlobalData_t)(1.986446e-25)
#define pp_init (GlobalData_t)(0.)



void initialize_params_IChR2_WilliamsXu( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  IChR2_WilliamsXu_Params *p = (IChR2_WilliamsXu_Params *)IF->params;

  // Compute the regional constants
  {
    p->Acell = 3.14159265E-6;
    p->Gd2 = 0.05;
    p->Temp = 37.;
    p->c2 = 0.024;
    p->e12_c1 = 0.005;
    p->e12d = 0.011;
    p->e21_c1 = 0.004;
    p->e21d = 0.008;
    p->gam = 0.1;
    p->lambda = 470.;
    p->qeff1 = 0.8535;
    p->qeff2 = 0.1400;
    p->sigret = 12e-20;
    p->tau_ChR2 = 1.3;
    p->wloss = 1.3;
  }
  // Compute the regional initialization
  {
    p->GChR2 = (2.0*0.2);
    p->attenuation = 1.;
  }
  IF->type->trace = trace_IChR2_WilliamsXu;

}


// Define the parameters for the lookup tables
enum Tables {
  Vm_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum Vm_TableIndex {
  GV_idx,
  act_Gd1_idx,
  act_Gr_idx,
  NROWS_Vm
};



void construct_tables_IChR2_WilliamsXu( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IChR2_WilliamsXu_Params *p = (IChR2_WilliamsXu_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double Ephot = ((1e9*hc)/p->lambda);
  double act_Gd2 = (p->Gd2*(pow(Q10_Gd2,((p->Temp-(Temp0))/10.))));
  double act_e12d = (p->e12d*(pow(Q10_e12d,((p->Temp-(Temp0))/10.))));
  double act_e21d = (p->e21d*(pow(Q10_e21d,((p->Temp-(Temp0))/10.))));
  double act_qeff1 = (p->qeff1*(pow(Q10_qeff1,((p->Temp-(Temp0))/10.))));
  double act_qeff2 = (p->qeff2*(pow(Q10_qeff2,((p->Temp-(Temp0))/10.))));
  
  // Create the Vm lookup table
  LUT* Vm_tab = &IF->tables[Vm_TAB];
  LUT_alloc(Vm_tab, NROWS_Vm, -1000, 1000, 0.005, "IChR2_WilliamsXu Vm");
  for (int __i=Vm_tab->mn_ind; __i<=Vm_tab->mx_ind; __i++) {
    double Vm = Vm_tab->res*__i;
    LUT_data_t* Vm_row = Vm_tab->tab[__i];
    Vm_row[GV_idx] = (10.6408-((14.6408*(exp(((-Vm)/42.7671))))));
    double Gd1 = (0.075+(0.043*(tanh(((Vm+20.)/-20.)))));
    double Gr = (0.0000434587*(exp((-0.0211539274*Vm))));
    Vm_row[act_Gd1_idx] = (Gd1*(pow(Q10_Gd1,((p->Temp-(Temp0))/10.))));
    Vm_row[act_Gr_idx] = (Gr*(pow(Q10_Gr,((p->Temp-(Temp0))/10.))));
  }
  check_LUT(Vm_tab);
  

}



void    initialize_sv_IChR2_WilliamsXu( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IChR2_WilliamsXu_Params *p = (IChR2_WilliamsXu_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(IChR2_WilliamsXu_state) );
  IChR2_WilliamsXu_state *sv_base = (IChR2_WilliamsXu_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double Ephot = ((1e9*hc)/p->lambda);
  double act_Gd2 = (p->Gd2*(pow(Q10_Gd2,((p->Temp-(Temp0))/10.))));
  double act_e12d = (p->e12d*(pow(Q10_e12d,((p->Temp-(Temp0))/10.))));
  double act_e21d = (p->e21d*(pow(Q10_e21d,((p->Temp-(Temp0))/10.))));
  double act_qeff1 = (p->qeff1*(pow(Q10_qeff1,((p->Temp-(Temp0))/10.))));
  double act_qeff2 = (p->qeff2*(pow(Q10_qeff2,((p->Temp-(Temp0))/10.))));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vm_ext = impdata[Vm];
  GlobalData_t *illum_ext = impdata[illum];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    IChR2_WilliamsXu_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Vm = Vm_ext[__i];
    GlobalData_t illum = illum_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    sv->GChR2 = p->GChR2;
    sv->attenuation = p->attenuation;
    // Initialize the rest of the nodal variables
    sv->C1 = C1_init;
    sv->C2 = C2_init;
    sv->O2 = O2_init;
    sv->pp = pp_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Vm_ext[__i] = Vm;
    illum_ext[__i] = illum;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_IChR2_WilliamsXu(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IChR2_WilliamsXu_Params *p  = (IChR2_WilliamsXu_Params *)IF->params;
  IChR2_WilliamsXu_state *sv_base = (IChR2_WilliamsXu_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Ephot = ((1e9*hc)/p->lambda);
  GlobalData_t act_Gd2 = (p->Gd2*(pow(Q10_Gd2,((p->Temp-(Temp0))/10.))));
  GlobalData_t act_e12d = (p->e12d*(pow(Q10_e12d,((p->Temp-(Temp0))/10.))));
  GlobalData_t act_e21d = (p->e21d*(pow(Q10_e21d,((p->Temp-(Temp0))/10.))));
  GlobalData_t act_qeff1 = (p->qeff1*(pow(Q10_qeff1,((p->Temp-(Temp0))/10.))));
  GlobalData_t act_qeff2 = (p->qeff2*(pow(Q10_qeff2,((p->Temp-(Temp0))/10.))));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vm_ext = impdata[Vm];
  GlobalData_t *illum_ext = impdata[illum];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    IChR2_WilliamsXu_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Vm = Vm_ext[__i];
    GlobalData_t illum = illum_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Vm_row[NROWS_Vm];
    LUT_interpRow(&IF->tables[Vm_TAB], Vm, __i, Vm_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t O1 = (((((1.0-(sv->O2))-(sv->C1))-(sv->C2))<0.) ? 0. : (((1.-(sv->O2))-(sv->C1))-(sv->C2)));
    GlobalData_t IChR2 = ((sv->GChR2*Vm_row[GV_idx])*(O1+(p->gam*sv->O2)));
    Iion = (Iion+IChR2);
    
    
    //Complete Forward Euler Update
    GlobalData_t Th = (illum*sv->attenuation);
    GlobalData_t dO2C2 = (act_Gd2*sv->O2);
    GlobalData_t S0 = (0.5*(1.+(tanh((120.*((100.*Th)-(0.1)))))));
    GlobalData_t e12 = (act_e12d+(p->e12_c1*(log((1.+((Th*sv->pp)/p->c2))))));
    GlobalData_t e21 = (act_e21d+(p->e21_c1*(log((1.+((Th*sv->pp)/p->c2))))));
    GlobalData_t flux = ((1e3*Th)/Ephot);
    GlobalData_t FF = ((flux*p->sigret)/(1e3*p->wloss));
    GlobalData_t dC2C1 = (Vm_row[act_Gr_idx]*sv->C2);
    GlobalData_t dO1C1 = (Vm_row[act_Gd1_idx]*O1);
    GlobalData_t dO1O2 = (e12*O1);
    GlobalData_t dO2O1 = (e21*sv->O2);
    GlobalData_t diff_pp = ((S0-(sv->pp))/p->tau_ChR2);
    GlobalData_t kk1 = ((act_qeff1*FF)*sv->pp);
    GlobalData_t kk2 = ((act_qeff2*FF)*sv->pp);
    GlobalData_t dC1O1 = (kk1*sv->C1);
    GlobalData_t dC2O2 = (kk2*sv->C2);
    GlobalData_t diff_C1 = ((dO1C1+dC2C1)-(dC1O1));
    GlobalData_t diff_C2 = ((dO2C2-(dC2C1))-(dC2O2));
    GlobalData_t diff_O2 = (((dO1O2+dC2O2)-(dO2O1))-(dO2C2));
    GlobalData_t C1_new = sv->C1+diff_C1*dt;
    GlobalData_t C2_new = sv->C2+diff_C2*dt;
    GlobalData_t O2_new = sv->O2+diff_O2*dt;
    GlobalData_t pp_new = sv->pp+diff_pp*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->C1 = C1_new;
    sv->C2 = C2_new;
    Iion = Iion;
    sv->O2 = O2_new;
    sv->pp = pp_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Vm_ext[__i] = Vm;
    illum_ext[__i] = illum;

  }


}


void trace_IChR2_WilliamsXu(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("IChR2_WilliamsXu_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->C1\n"
        "sv->C2\n"
        "IChR2\n"
        "IChR2_tot\n"
        "O1\n"
        "sv->O2\n"
        "Vm\n"
        "illum\n"
        "sv->pp\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IChR2_WilliamsXu_Params *p  = (IChR2_WilliamsXu_Params *)IF->params;

  IChR2_WilliamsXu_state *sv_base = (IChR2_WilliamsXu_state *)IF->sv_tab.y;
  IChR2_WilliamsXu_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Ephot = ((1e9*hc)/p->lambda);
  GlobalData_t act_Gd2 = (p->Gd2*(pow(Q10_Gd2,((p->Temp-(Temp0))/10.))));
  GlobalData_t act_e12d = (p->e12d*(pow(Q10_e12d,((p->Temp-(Temp0))/10.))));
  GlobalData_t act_e21d = (p->e21d*(pow(Q10_e21d,((p->Temp-(Temp0))/10.))));
  GlobalData_t act_qeff1 = (p->qeff1*(pow(Q10_qeff1,((p->Temp-(Temp0))/10.))));
  GlobalData_t act_qeff2 = (p->qeff2*(pow(Q10_qeff2,((p->Temp-(Temp0))/10.))));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vm_ext = impdata[Vm];
  GlobalData_t *illum_ext = impdata[illum];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t Vm = Vm_ext[__i];
  GlobalData_t illum = illum_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t GV = (10.6408-((14.6408*(exp(((-Vm)/42.7671))))));
  GlobalData_t O1 = (((((1.0-(sv->O2))-(sv->C1))-(sv->C2))<0.) ? 0. : (((1.-(sv->O2))-(sv->C1))-(sv->C2)));
  GlobalData_t IChR2 = ((sv->GChR2*GV)*(O1+(p->gam*sv->O2)));
  GlobalData_t IChR2_tot = ((IChR2*p->Acell)*1000.);
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->C1);
  fprintf(file, "%4.12f\t", sv->C2);
  fprintf(file, "%4.12f\t", IChR2);
  fprintf(file, "%4.12f\t", IChR2_tot);
  fprintf(file, "%4.12f\t", O1);
  fprintf(file, "%4.12f\t", sv->O2);
  fprintf(file, "%4.12f\t", Vm);
  fprintf(file, "%4.12f\t", illum);
  fprintf(file, "%4.12f\t", sv->pp);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        