// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Ferrero (Jr), J.M., Saiz, J., Ferrero-Corral, J.M., Thakor, N.V.
*  Year: 1996
*  Title: Simulation of Action Potentials from Metabolically Impaired Cardiac Myocytes. Role of ATP-Sensitive Potassium Current
*  Journal: Circulation Research, 79: 208-221
*  DOI: 10.1161/01.RES.79.2.208
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __IKATP_FERRERO_H__
#define __IKATP_FERRERO_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define IKATP_Ferrero_REQDAT Iion_DATA_FLAG|Ke_DATA_FLAG|Vm_DATA_FLAG
#define IKATP_Ferrero_MODDAT Iion_DATA_FLAG|Ke_DATA_FLAG

struct IKATP_Ferrero_Params {
    GlobalData_t ADPi_init;
    GlobalData_t ATPi_init;
    GlobalData_t GAMMA0;
    GlobalData_t gamma;

};

struct IKATP_Ferrero_state {
    GlobalData_t __ADPi_local;
    GlobalData_t __ATPi_local;
    GlobalData_t __Ki_local;
    GlobalData_t __Nai_local;
    char dummy; //PGI doesn't allow a structure of 0 bytes.

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_IKATP_Ferrero(ION_IF *);
void construct_tables_IKATP_Ferrero(ION_IF *);
void destroy_IKATP_Ferrero(ION_IF *);
void initialize_sv_IKATP_Ferrero(ION_IF *, GlobalData_t**);
void compute_IKATP_Ferrero(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
