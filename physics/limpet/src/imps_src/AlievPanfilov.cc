// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Rubin R. Aliev, Alexander V. Panfilov
*  Year: 1996
*  Title: A simple two-variable model of cardiac excitation
*  Journal: Chaos, Solitons & Fractals, 7(3),293-301
*  DOI: 10.1016/0960-0779(95)00089-5
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "AlievPanfilov.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_AlievPanfilov(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_AlievPanfilov( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define K (GlobalData_t)(8.)
#define V_init (GlobalData_t)(0.)
#define a (GlobalData_t)(0.15)
#define epsilon (GlobalData_t)(0.002)
#define t_norm (GlobalData_t)(12.9)
#define touAcm2 (GlobalData_t)((100./12.9))
#define vm_norm (GlobalData_t)(100.)
#define vm_rest (GlobalData_t)(-80.)
#define Vm_init (GlobalData_t)(vm_rest)



void initialize_params_AlievPanfilov( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  AlievPanfilov_Params *p = (AlievPanfilov_Params *)IF->params;

  // Compute the regional constants
  {
    p->mu1 = 0.2;
    p->mu2 = 0.3;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_AlievPanfilov( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  AlievPanfilov_Params *p = (AlievPanfilov_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.

}



void    initialize_sv_AlievPanfilov( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  AlievPanfilov_Params *p = (AlievPanfilov_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(AlievPanfilov_state) );
  AlievPanfilov_state *sv_base = (AlievPanfilov_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vm_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    AlievPanfilov_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Vm = Vm_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->V = V_init;
    Vm = Vm_init;
    double U = ((Vm-(vm_rest))/vm_norm);
    Iion = (((((K*U)*(U-(a)))*(U-(1.)))+(U*sv->V))*touAcm2);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Vm_ext[__i] = Vm;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_AlievPanfilov(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  AlievPanfilov_Params *p  = (AlievPanfilov_Params *)IF->params;
  AlievPanfilov_state *sv_base = (AlievPanfilov_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vm_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    AlievPanfilov_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Vm = Vm_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t U = ((Vm-(vm_rest))/vm_norm);
    Iion = (((((K*U)*(U-(a)))*(U-(1.)))+(U*sv->V))*touAcm2);
    
    
    //Complete Forward Euler Update
    GlobalData_t diff_V = (((-(epsilon+((p->mu1*sv->V)/(p->mu2+U))))*(sv->V+((K*U)*((U-(a))-(1.)))))/t_norm);
    GlobalData_t V_new = sv->V+diff_V*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    sv->V = V_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Vm_ext[__i] = Vm;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        