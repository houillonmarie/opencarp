// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: DeBruin, K.A., Krassowska, W
*  Year: 1998
*  Title: Electroporation and Shock-Induced Transmembrane Potential in a Cardiac Fiber During Defibrillation Strength Shocks
*  Journal: Annals of Biomedical Engineering 26, 584-596
*  DOI: 10.1114/1.101
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Electroporation_DeBruinKrassowska98.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Electroporation_DeBruinKrassowska98(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Electroporation_DeBruinKrassowska98( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define PI (GlobalData_t)(3.14159)
#define T (GlobalData_t)(310.0)
#define e (GlobalData_t)(1.6021765e-19)
#define k (GlobalData_t)(1.38066e-20)
#define nd_f (GlobalData_t)((e/(k*T)))



void initialize_params_Electroporation_DeBruinKrassowska98( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Electroporation_DeBruinKrassowska98_Params *p = (Electroporation_DeBruinKrassowska98_Params *)IF->params;

  // Compute the regional constants
  {
    p->N0 = 1.5e5;
    p->alpha = 200.0;
    p->beta = 6.25e-5;
    p->h = 5.0e-7;
    p->nn = 0.15;
    p->q = 2.46;
    p->sigma = 13.0;
    p->w0 = 5.25;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Electroporation_DeBruinKrassowska98( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Electroporation_DeBruinKrassowska98_Params *p = (Electroporation_DeBruinKrassowska98_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double gp_f = (((PI*p->sigma)*p->h)*0.25);

}



void    initialize_sv_Electroporation_DeBruinKrassowska98( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Electroporation_DeBruinKrassowska98_Params *p = (Electroporation_DeBruinKrassowska98_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Electroporation_DeBruinKrassowska98_state) );
  Electroporation_DeBruinKrassowska98_state *sv_base = (Electroporation_DeBruinKrassowska98_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double gp_f = (((PI*p->sigma)*p->h)*0.25);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Electroporation_DeBruinKrassowska98_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    double dvm_2 = (V*V);
    double n_init = (p->N0*(exp(((p->q*p->beta)*dvm_2))));
    sv->n = n_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Electroporation_DeBruinKrassowska98(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Electroporation_DeBruinKrassowska98_Params *p  = (Electroporation_DeBruinKrassowska98_Params *)IF->params;
  Electroporation_DeBruinKrassowska98_state *sv_base = (Electroporation_DeBruinKrassowska98_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t gp_f = (((PI*p->sigma)*p->h)*0.25);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Electroporation_DeBruinKrassowska98_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t nd_vm = (V*nd_f);
    GlobalData_t nvm = (p->nn*nd_vm);
    GlobalData_t nvmm = (p->w0-(nvm));
    GlobalData_t nvmp = (p->w0+nvm);
    GlobalData_t gp = ((gp_f*((exp(nd_vm))-(1.)))/((((exp(nd_vm))*((p->w0*(exp(nvmm)))-(nvm)))/nvmm)-((((p->w0*(exp(nvmp)))+nvm)/nvmp))));
    Iion = (Iion+((gp*sv->n)*V));
    
    
    //Complete Forward Euler Update
    GlobalData_t dvm_2 = (V*V);
    GlobalData_t dN1 = (p->alpha*(exp((p->beta*dvm_2))));
    GlobalData_t dN2 = (((-dN1)/p->N0)*(exp((((-p->q)*p->beta)*dvm_2))));
    GlobalData_t diff_n = (dN1+(dN2*sv->n));
    GlobalData_t n_new = sv->n+diff_n*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    sv->n = n_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        