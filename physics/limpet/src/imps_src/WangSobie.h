// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Linda J. Wang and Eric A. Sobie
*  Year: 2008
*  Title: Mathematical model of the neonatal mouse ventricular action potential
*  Journal: American Journal of Physiology-Heart and Circulatory Physiology, 294(6), 2565-2575
*  DOI: 10.1152/ajpheart.01376.2007
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __WANGSOBIE_H__
#define __WANGSOBIE_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define WangSobie_REQDAT Vm_DATA_FLAG
#define WangSobie_MODDAT Iion_DATA_FLAG

struct WangSobie_Params {
    GlobalData_t Cai_init;
    GlobalData_t ICaL_slowdown;
    GlobalData_t Ki_init;
    GlobalData_t Nai_init;

};

struct WangSobie_state {
    GlobalData_t C2;
    GlobalData_t C3;
    GlobalData_t C4;
    GlobalData_t C_K1;
    GlobalData_t C_K2;
    GlobalData_t CaJSR;
    GlobalData_t CaNSR;
    GlobalData_t Cai;
    GlobalData_t Cass;
    GlobalData_t HTRPN_Ca;
    GlobalData_t I1;
    GlobalData_t I2;
    GlobalData_t I3;
    GlobalData_t ICaL_slowdown;
    GlobalData_t I_K;
    GlobalData_t Ki;
    GlobalData_t LTRPN_Ca;
    GlobalData_t Nai;
    GlobalData_t O;
    GlobalData_t O_K;
    GlobalData_t P_C2;
    GlobalData_t P_O1;
    GlobalData_t P_O2;
    GlobalData_t P_RyR;
    Gatetype ato_f;
    Gatetype aur;
    Gatetype b;
    Gatetype g;
    Gatetype h;
    Gatetype ito_f;
    Gatetype iur;
    Gatetype j;
    Gatetype m;
    Gatetype nKs;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_WangSobie(ION_IF *);
void construct_tables_WangSobie(ION_IF *);
void destroy_WangSobie(ION_IF *);
void initialize_sv_WangSobie(ION_IF *, GlobalData_t**);
void compute_WangSobie(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
