// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Aslanidi OV, Sleiman RN, Boyett MR, Hancox JC, Zhang H
*  Year: 2010
*  Title: Ionic mechanisms for electrical heterogeneity between rabbit Purkinje fiber and Ventricular cells
*  Journal: Biophys J, 98(11),2420-31
*  DOI: 10.1016/j.bpj.2010.02.033
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "AslanidiSleiman.h"

#ifdef _OPENMP
#include <omp.h>
#endif


#include "Rosenbrock.h"


namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_AslanidiSleiman(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_AslanidiSleiman( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define Ca_buffer_Bmax_Calsequestrin (GlobalData_t)(0.14)
#define Ca_buffer_Bmax_SLB_SL (GlobalData_t)(0.0374)
#define Ca_buffer_Bmax_SLB_jct (GlobalData_t)(0.0046)
#define Ca_buffer_Bmax_SLHigh_SL (GlobalData_t)(0.0134)
#define Ca_buffer_Bmax_SLHigh_jct (GlobalData_t)(0.00165)
#define Ca_buffer_koff_Calsequestrin (GlobalData_t)(65.0)
#define Ca_buffer_koff_SLB (GlobalData_t)(1.3)
#define Ca_buffer_koff_SLHigh (GlobalData_t)(30.0e-3)
#define Ca_buffer_kon_Calsequestrin (GlobalData_t)(100.0)
#define Ca_buffer_kon_SL (GlobalData_t)(100.0)
#define Cai_init (GlobalData_t)(9.93905e-02)
#define Cm_per_area (GlobalData_t)(2.0e-6)
#define E_CaL (GlobalData_t)(60.0)
#define E_CaT (GlobalData_t)(50.0)
#define F (GlobalData_t)(96486.7)
#define JleaKSR_KSRleak (GlobalData_t)(5.348e-6)
#define JpumPSR_H (GlobalData_t)(1.787)
#define JpumPSR_Kmf (GlobalData_t)(0.000246)
#define JpumPSR_Kmr (GlobalData_t)(1.7)
#define JpumPSR_Q10_SRCaP (GlobalData_t)(2.6)
#define JpumPSR_V_max (GlobalData_t)(286.0e-6)
#define Jrel_SR_EC50_SR (GlobalData_t)(0.45)
#define Jrel_SR_HSR (GlobalData_t)(2.5)
#define Jrel_SR_Max_SR (GlobalData_t)(15.0)
#define Jrel_SR_Min_SR (GlobalData_t)(1.0)
#define Jrel_SR_kiCa (GlobalData_t)(0.5)
#define Jrel_SR_kim (GlobalData_t)(0.005)
#define Jrel_SR_koCa (GlobalData_t)(10.0)
#define Jrel_SR_kom (GlobalData_t)(0.06)
#define Jrel_SR_ks (GlobalData_t)(25.0)
#define PI (GlobalData_t)(3.14159265359)
#define R (GlobalData_t)(8314.3)
#define V_init (GlobalData_t)(-85.49190000)
#define Y0_init (GlobalData_t)(1.28293)
#define Y11_init (GlobalData_t)(0.04)
#define Y1_init (GlobalData_t)(0.000131111)
#define Y23_init (GlobalData_t)(2.67486e-07)
#define Y24_init (GlobalData_t)(1.94911e-06)
#define Y25_init (GlobalData_t)(0.879232)
#define Y2_init (GlobalData_t)(0.0121366)
#define Y32_init (GlobalData_t)(0.000336009)
#define Y33_init (GlobalData_t)(0.00244706)
#define Y34_init (GlobalData_t)(0.00243042)
#define Y35_init (GlobalData_t)(0.00996049)
#define Y36_init (GlobalData_t)(0.12297)
#define Y37_init (GlobalData_t)(0.136961)
#define Y38_init (GlobalData_t)(0.0079682)
#define Y3_init (GlobalData_t)(0.0116031)
#define Y4_init (GlobalData_t)(0.132481)
#define Y5_init (GlobalData_t)(0.0981466)
#define Y6_init (GlobalData_t)(0.633156)
#define Y7_init (GlobalData_t)(0.00026248)
#define b_init (GlobalData_t)(8.06835e-05)
#define cell_length (GlobalData_t)(100.0)
#define cell_radius (GlobalData_t)(10.25)
#define cytosolic_Ca_buffer_Bmax_Calmodulin (GlobalData_t)(0.024)
#define cytosolic_Ca_buffer_Bmax_Myosin_Ca (GlobalData_t)(0.14)
#define cytosolic_Ca_buffer_Bmax_Myosin_Mg (GlobalData_t)(0.14)
#define cytosolic_Ca_buffer_Bmax_SRB (GlobalData_t)(0.0171)
#define cytosolic_Ca_buffer_Bmax_TroponinC (GlobalData_t)(0.07)
#define cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa (GlobalData_t)(0.14)
#define cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MgMg (GlobalData_t)(0.14)
#define cytosolic_Ca_buffer_koff_Calmodulin (GlobalData_t)(238.0e-3)
#define cytosolic_Ca_buffer_koff_Myosin_Ca (GlobalData_t)(0.46e-3)
#define cytosolic_Ca_buffer_koff_Myosin_Mg (GlobalData_t)(0.057e-3)
#define cytosolic_Ca_buffer_koff_SRB (GlobalData_t)(60.0e-3)
#define cytosolic_Ca_buffer_koff_TroponinC (GlobalData_t)(19.6e-3)
#define cytosolic_Ca_buffer_koff_TroponinC_Ca_MGCa (GlobalData_t)(0.032e-3)
#define cytosolic_Ca_buffer_koff_TroponinC_Ca_MgMg (GlobalData_t)(3.33e-3)
#define cytosolic_Ca_buffer_kon_Calmodulin (GlobalData_t)(34.0)
#define cytosolic_Ca_buffer_kon_Myosin_Ca (GlobalData_t)(13.8)
#define cytosolic_Ca_buffer_kon_Myosin_Mg (GlobalData_t)(15.7e-3)
#define cytosolic_Ca_buffer_kon_SRB (GlobalData_t)(100.0)
#define cytosolic_Ca_buffer_kon_TroponinC (GlobalData_t)(32.7)
#define cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa (GlobalData_t)(2.37)
#define cytosolic_Ca_buffer_kon_TroponinC_Ca_MgMg (GlobalData_t)(3.0e-3)
#define d_init (GlobalData_t)(1.71221e-06)
#define f_init (GlobalData_t)(0.99764600)
#define g_init (GlobalData_t)(0.97941500)
#define hL_init (GlobalData_t)(0.93722800)
#define h_init (GlobalData_t)(0.98646900)
#define ion_diffusion_A_SL_cytosol (GlobalData_t)(1.3e-4)
#define ion_diffusion_A_jct_SL (GlobalData_t)(3.01e-6)
#define ion_diffusion_D_Ca_SL_cytosol (GlobalData_t)(1.22e-6)
#define ion_diffusion_D_Ca_jct_SL (GlobalData_t)(1.64e-6)
#define ion_diffusion_D_NaSL_cytosol (GlobalData_t)(1.79e-5)
#define ion_diffusion_D_Najct_SL (GlobalData_t)(1.09e-5)
#define ion_diffusion_x_SL_cytosol (GlobalData_t)(0.45)
#define ion_diffusion_x_jct_SL (GlobalData_t)(0.5)
#define j_init (GlobalData_t)(0.99907100)
#define mL_init (GlobalData_t)(0.00142380)
#define m_init (GlobalData_t)(0.00142380)
#define partial_Ca_buffer_Bmax_Calsequestrin_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_Calsequestrin_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_Calsequestrin_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_Calsequestrin_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_Calsequestrin_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_Calsequestrin_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_Calsequestrin_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_Calsequestrin_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_SL_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_SL_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_SL_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_SL_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_SL_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_SL_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_SL_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_SL_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_jct_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_jct_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_jct_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_jct_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_jct_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_jct_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_jct_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLB_jct_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_SL_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_SL_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_SL_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_SL_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_SL_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_SL_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_SL_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_SL_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_jct_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_jct_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_jct_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_jct_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_jct_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_jct_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_jct_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_Bmax_SLHigh_jct_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICaSL_tot_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICaSL_tot_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICaSL_tot_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICaSL_tot_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICaSL_tot_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICaSL_tot_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICaSL_tot_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICa_jct_tot_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_ICa_jct_tot_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICa_jct_tot_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICa_jct_tot_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICa_jct_tot_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICa_jct_tot_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICa_jct_tot_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_ICa_jct_tot_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_SL_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_SL_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_SL_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_SL_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_SL_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_SL_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_SL_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_jct_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_jct_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_jct_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_jct_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_jct_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_jct_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLB_jct_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_SL_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_SL_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_SL_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_SL_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_SL_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_SL_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_SL_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_jct_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_jct_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_jct_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_jct_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_jct_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_jct_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SLHigh_jct_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SL_tot_bound_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SL_tot_bound_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SL_tot_bound_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SL_tot_bound_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SL_tot_bound_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SL_tot_bound_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_SL_tot_bound_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_jct_tot_bound_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_jct_tot_bound_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_jct_tot_bound_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_jct_tot_bound_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_jct_tot_bound_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_jct_tot_bound_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCa_jct_tot_bound_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCalsequestrin_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_dCalsequestrin_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCalsequestrin_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCalsequestrin_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCalsequestrin_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_dCalsequestrin_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_Calsequestrin_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_Calsequestrin_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_Calsequestrin_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_Calsequestrin_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_Calsequestrin_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_Calsequestrin_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_Calsequestrin_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_Calsequestrin_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLB_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLB_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLB_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLB_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLB_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLB_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLB_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLB_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLHigh_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLHigh_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLHigh_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLHigh_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLHigh_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLHigh_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLHigh_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_koff_SLHigh_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_Calsequestrin_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_Calsequestrin_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_Calsequestrin_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_Calsequestrin_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_Calsequestrin_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_Calsequestrin_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_Calsequestrin_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_Calsequestrin_del_Y7 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_SL_del_Cai (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_SL_del_Y0 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_SL_del_Y1 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_SL_del_Y23 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_SL_del_Y24 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_SL_del_Y25 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_SL_del_Y6 (GlobalData_t)(0.)
#define partial_Ca_buffer_kon_SL_del_Y7 (GlobalData_t)(0.)
#define partial_Cae_del_Cai (GlobalData_t)(0.)
#define partial_Cae_del_Y0 (GlobalData_t)(0.)
#define partial_Cae_del_Y1 (GlobalData_t)(0.)
#define partial_Cae_del_Y23 (GlobalData_t)(0.)
#define partial_Cae_del_Y24 (GlobalData_t)(0.)
#define partial_Cae_del_Y25 (GlobalData_t)(0.)
#define partial_Cae_del_Y6 (GlobalData_t)(0.)
#define partial_Cae_del_Y7 (GlobalData_t)(0.)
#define partial_Cai_mM_del_Cai (GlobalData_t)((1000./(1000.*1000.)))
#define partial_Cai_mM_del_Y0 (GlobalData_t)(0.)
#define partial_Cai_mM_del_Y1 (GlobalData_t)(0.)
#define partial_Cai_mM_del_Y23 (GlobalData_t)(0.)
#define partial_Cai_mM_del_Y24 (GlobalData_t)(0.)
#define partial_Cai_mM_del_Y25 (GlobalData_t)(0.)
#define partial_Cai_mM_del_Y6 (GlobalData_t)(0.)
#define partial_Cai_mM_del_Y7 (GlobalData_t)(0.)
#define partial_Cm_del_Cai (GlobalData_t)(0.)
#define partial_Cm_del_Y0 (GlobalData_t)(0.)
#define partial_Cm_del_Y1 (GlobalData_t)(0.)
#define partial_Cm_del_Y23 (GlobalData_t)(0.)
#define partial_Cm_del_Y24 (GlobalData_t)(0.)
#define partial_Cm_del_Y25 (GlobalData_t)(0.)
#define partial_Cm_del_Y6 (GlobalData_t)(0.)
#define partial_Cm_del_Y7 (GlobalData_t)(0.)
#define partial_Cm_per_area_del_Cai (GlobalData_t)(0.)
#define partial_Cm_per_area_del_Y0 (GlobalData_t)(0.)
#define partial_Cm_per_area_del_Y1 (GlobalData_t)(0.)
#define partial_Cm_per_area_del_Y23 (GlobalData_t)(0.)
#define partial_Cm_per_area_del_Y24 (GlobalData_t)(0.)
#define partial_Cm_per_area_del_Y25 (GlobalData_t)(0.)
#define partial_Cm_per_area_del_Y6 (GlobalData_t)(0.)
#define partial_Cm_per_area_del_Y7 (GlobalData_t)(0.)
#define partial_E_CaL_del_Cai (GlobalData_t)(0.)
#define partial_E_CaL_del_Y0 (GlobalData_t)(0.)
#define partial_E_CaL_del_Y1 (GlobalData_t)(0.)
#define partial_E_CaL_del_Y23 (GlobalData_t)(0.)
#define partial_E_CaL_del_Y24 (GlobalData_t)(0.)
#define partial_E_CaL_del_Y25 (GlobalData_t)(0.)
#define partial_E_CaL_del_Y6 (GlobalData_t)(0.)
#define partial_E_CaL_del_Y7 (GlobalData_t)(0.)
#define partial_E_CaT_del_Cai (GlobalData_t)(0.)
#define partial_E_CaT_del_Y0 (GlobalData_t)(0.)
#define partial_E_CaT_del_Y1 (GlobalData_t)(0.)
#define partial_E_CaT_del_Y23 (GlobalData_t)(0.)
#define partial_E_CaT_del_Y24 (GlobalData_t)(0.)
#define partial_E_CaT_del_Y25 (GlobalData_t)(0.)
#define partial_E_CaT_del_Y6 (GlobalData_t)(0.)
#define partial_E_CaT_del_Y7 (GlobalData_t)(0.)
#define partial_E_Ca_del_Y0 (GlobalData_t)(0.)
#define partial_E_Ca_del_Y1 (GlobalData_t)(0.)
#define partial_E_Ca_del_Y23 (GlobalData_t)(0.)
#define partial_E_Ca_del_Y24 (GlobalData_t)(0.)
#define partial_E_Ca_del_Y25 (GlobalData_t)(0.)
#define partial_E_Ca_del_Y6 (GlobalData_t)(0.)
#define partial_E_Ca_del_Y7 (GlobalData_t)(0.)
#define partial_F_del_Cai (GlobalData_t)(0.)
#define partial_F_del_Y0 (GlobalData_t)(0.)
#define partial_F_del_Y1 (GlobalData_t)(0.)
#define partial_F_del_Y23 (GlobalData_t)(0.)
#define partial_F_del_Y24 (GlobalData_t)(0.)
#define partial_F_del_Y25 (GlobalData_t)(0.)
#define partial_F_del_Y6 (GlobalData_t)(0.)
#define partial_F_del_Y7 (GlobalData_t)(0.)
#define partial_GCaB_del_Cai (GlobalData_t)(0.)
#define partial_GCaB_del_Y0 (GlobalData_t)(0.)
#define partial_GCaB_del_Y1 (GlobalData_t)(0.)
#define partial_GCaB_del_Y23 (GlobalData_t)(0.)
#define partial_GCaB_del_Y24 (GlobalData_t)(0.)
#define partial_GCaB_del_Y25 (GlobalData_t)(0.)
#define partial_GCaB_del_Y6 (GlobalData_t)(0.)
#define partial_GCaB_del_Y7 (GlobalData_t)(0.)
#define partial_GCaL_del_Cai (GlobalData_t)(0.)
#define partial_GCaL_del_Y0 (GlobalData_t)(0.)
#define partial_GCaL_del_Y1 (GlobalData_t)(0.)
#define partial_GCaL_del_Y23 (GlobalData_t)(0.)
#define partial_GCaL_del_Y24 (GlobalData_t)(0.)
#define partial_GCaL_del_Y25 (GlobalData_t)(0.)
#define partial_GCaL_del_Y6 (GlobalData_t)(0.)
#define partial_GCaL_del_Y7 (GlobalData_t)(0.)
#define partial_GCaT_del_Cai (GlobalData_t)(0.)
#define partial_GCaT_del_Y0 (GlobalData_t)(0.)
#define partial_GCaT_del_Y1 (GlobalData_t)(0.)
#define partial_GCaT_del_Y23 (GlobalData_t)(0.)
#define partial_GCaT_del_Y24 (GlobalData_t)(0.)
#define partial_GCaT_del_Y25 (GlobalData_t)(0.)
#define partial_GCaT_del_Y6 (GlobalData_t)(0.)
#define partial_GCaT_del_Y7 (GlobalData_t)(0.)
#define partial_ICaB_del_Y0 (GlobalData_t)(0.)
#define partial_ICaB_del_Y1 (GlobalData_t)(0.)
#define partial_ICaB_del_Y23 (GlobalData_t)(0.)
#define partial_ICaB_del_Y24 (GlobalData_t)(0.)
#define partial_ICaB_del_Y25 (GlobalData_t)(0.)
#define partial_ICaB_del_Y6 (GlobalData_t)(0.)
#define partial_ICaB_del_Y7 (GlobalData_t)(0.)
#define partial_ICaL_del_Cai (GlobalData_t)(0.)
#define partial_ICaL_del_Y0 (GlobalData_t)(0.)
#define partial_ICaL_del_Y1 (GlobalData_t)(0.)
#define partial_ICaL_del_Y23 (GlobalData_t)(0.)
#define partial_ICaL_del_Y24 (GlobalData_t)(0.)
#define partial_ICaL_del_Y25 (GlobalData_t)(0.)
#define partial_ICaL_del_Y6 (GlobalData_t)(0.)
#define partial_ICaL_del_Y7 (GlobalData_t)(0.)
#define partial_ICaPHill_del_Cai (GlobalData_t)(0.)
#define partial_ICaPHill_del_Y0 (GlobalData_t)(0.)
#define partial_ICaPHill_del_Y1 (GlobalData_t)(0.)
#define partial_ICaPHill_del_Y23 (GlobalData_t)(0.)
#define partial_ICaPHill_del_Y24 (GlobalData_t)(0.)
#define partial_ICaPHill_del_Y25 (GlobalData_t)(0.)
#define partial_ICaPHill_del_Y6 (GlobalData_t)(0.)
#define partial_ICaPHill_del_Y7 (GlobalData_t)(0.)
#define partial_ICaPKmf_del_Cai (GlobalData_t)(0.)
#define partial_ICaPKmf_del_Y0 (GlobalData_t)(0.)
#define partial_ICaPKmf_del_Y1 (GlobalData_t)(0.)
#define partial_ICaPKmf_del_Y23 (GlobalData_t)(0.)
#define partial_ICaPKmf_del_Y24 (GlobalData_t)(0.)
#define partial_ICaPKmf_del_Y25 (GlobalData_t)(0.)
#define partial_ICaPKmf_del_Y6 (GlobalData_t)(0.)
#define partial_ICaPKmf_del_Y7 (GlobalData_t)(0.)
#define partial_ICaPVmf_del_Cai (GlobalData_t)(0.)
#define partial_ICaPVmf_del_Y0 (GlobalData_t)(0.)
#define partial_ICaPVmf_del_Y1 (GlobalData_t)(0.)
#define partial_ICaPVmf_del_Y23 (GlobalData_t)(0.)
#define partial_ICaPVmf_del_Y24 (GlobalData_t)(0.)
#define partial_ICaPVmf_del_Y25 (GlobalData_t)(0.)
#define partial_ICaPVmf_del_Y6 (GlobalData_t)(0.)
#define partial_ICaPVmf_del_Y7 (GlobalData_t)(0.)
#define partial_ICaP_del_Y0 (GlobalData_t)(0.)
#define partial_ICaP_del_Y1 (GlobalData_t)(0.)
#define partial_ICaP_del_Y23 (GlobalData_t)(0.)
#define partial_ICaP_del_Y24 (GlobalData_t)(0.)
#define partial_ICaP_del_Y25 (GlobalData_t)(0.)
#define partial_ICaP_del_Y6 (GlobalData_t)(0.)
#define partial_ICaP_del_Y7 (GlobalData_t)(0.)
#define partial_ICaT_del_Cai (GlobalData_t)(0.)
#define partial_ICaT_del_Y0 (GlobalData_t)(0.)
#define partial_ICaT_del_Y1 (GlobalData_t)(0.)
#define partial_ICaT_del_Y23 (GlobalData_t)(0.)
#define partial_ICaT_del_Y24 (GlobalData_t)(0.)
#define partial_ICaT_del_Y25 (GlobalData_t)(0.)
#define partial_ICaT_del_Y6 (GlobalData_t)(0.)
#define partial_ICaT_del_Y7 (GlobalData_t)(0.)
#define partial_ICa_del_Cai (GlobalData_t)(0.)
#define partial_ICa_del_Y0 (GlobalData_t)(0.)
#define partial_ICa_del_Y1 (GlobalData_t)(0.)
#define partial_ICa_del_Y23 (GlobalData_t)(0.)
#define partial_ICa_del_Y24 (GlobalData_t)(0.)
#define partial_ICa_del_Y25 (GlobalData_t)(0.)
#define partial_ICa_del_Y6 (GlobalData_t)(0.)
#define partial_ICa_del_Y7 (GlobalData_t)(0.)
#define partial_INCX_del_Y0 (GlobalData_t)(0.)
#define partial_INCX_del_Y1 (GlobalData_t)(0.)
#define partial_INCX_del_Y23 (GlobalData_t)(0.)
#define partial_INCX_del_Y24 (GlobalData_t)(0.)
#define partial_INCX_del_Y25 (GlobalData_t)(0.)
#define partial_INCX_del_Y6 (GlobalData_t)(0.)
#define partial_INCX_del_Y7 (GlobalData_t)(0.)
#define partial_INaCamax_del_Cai (GlobalData_t)(0.)
#define partial_INaCamax_del_Y0 (GlobalData_t)(0.)
#define partial_INaCamax_del_Y1 (GlobalData_t)(0.)
#define partial_INaCamax_del_Y23 (GlobalData_t)(0.)
#define partial_INaCamax_del_Y24 (GlobalData_t)(0.)
#define partial_INaCamax_del_Y25 (GlobalData_t)(0.)
#define partial_INaCamax_del_Y6 (GlobalData_t)(0.)
#define partial_INaCamax_del_Y7 (GlobalData_t)(0.)
#define partial_JleaKSR_KSRleak_del_Cai (GlobalData_t)(0.)
#define partial_JleaKSR_KSRleak_del_Y0 (GlobalData_t)(0.)
#define partial_JleaKSR_KSRleak_del_Y1 (GlobalData_t)(0.)
#define partial_JleaKSR_KSRleak_del_Y23 (GlobalData_t)(0.)
#define partial_JleaKSR_KSRleak_del_Y24 (GlobalData_t)(0.)
#define partial_JleaKSR_KSRleak_del_Y25 (GlobalData_t)(0.)
#define partial_JleaKSR_KSRleak_del_Y6 (GlobalData_t)(0.)
#define partial_JleaKSR_KSRleak_del_Y7 (GlobalData_t)(0.)
#define partial_JleaKSR_j_leaKSR_del_Cai (GlobalData_t)(0.)
#define partial_JleaKSR_j_leaKSR_del_Y0 (GlobalData_t)(0.)
#define partial_JleaKSR_j_leaKSR_del_Y1 (GlobalData_t)(0.)
#define partial_JleaKSR_j_leaKSR_del_Y23 (GlobalData_t)(0.)
#define partial_JleaKSR_j_leaKSR_del_Y24 (GlobalData_t)(0.)
#define partial_JleaKSR_j_leaKSR_del_Y25 (GlobalData_t)(0.)
#define partial_JpumPSR_H_del_Cai (GlobalData_t)(0.)
#define partial_JpumPSR_H_del_Y0 (GlobalData_t)(0.)
#define partial_JpumPSR_H_del_Y1 (GlobalData_t)(0.)
#define partial_JpumPSR_H_del_Y23 (GlobalData_t)(0.)
#define partial_JpumPSR_H_del_Y24 (GlobalData_t)(0.)
#define partial_JpumPSR_H_del_Y25 (GlobalData_t)(0.)
#define partial_JpumPSR_H_del_Y6 (GlobalData_t)(0.)
#define partial_JpumPSR_H_del_Y7 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmf_del_Cai (GlobalData_t)(0.)
#define partial_JpumPSR_Kmf_del_Y0 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmf_del_Y1 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmf_del_Y23 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmf_del_Y24 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmf_del_Y25 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmf_del_Y6 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmf_del_Y7 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmr_del_Cai (GlobalData_t)(0.)
#define partial_JpumPSR_Kmr_del_Y0 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmr_del_Y1 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmr_del_Y23 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmr_del_Y24 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmr_del_Y25 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmr_del_Y6 (GlobalData_t)(0.)
#define partial_JpumPSR_Kmr_del_Y7 (GlobalData_t)(0.)
#define partial_JpumPSR_V_max_del_Cai (GlobalData_t)(0.)
#define partial_JpumPSR_V_max_del_Y0 (GlobalData_t)(0.)
#define partial_JpumPSR_V_max_del_Y1 (GlobalData_t)(0.)
#define partial_JpumPSR_V_max_del_Y23 (GlobalData_t)(0.)
#define partial_JpumPSR_V_max_del_Y24 (GlobalData_t)(0.)
#define partial_JpumPSR_V_max_del_Y25 (GlobalData_t)(0.)
#define partial_JpumPSR_V_max_del_Y6 (GlobalData_t)(0.)
#define partial_JpumPSR_V_max_del_Y7 (GlobalData_t)(0.)
#define partial_JpumPSR_j_pumPSR_del_Y0 (GlobalData_t)(0.)
#define partial_JpumPSR_j_pumPSR_del_Y1 (GlobalData_t)(0.)
#define partial_JpumPSR_j_pumPSR_del_Y23 (GlobalData_t)(0.)
#define partial_JpumPSR_j_pumPSR_del_Y24 (GlobalData_t)(0.)
#define partial_JpumPSR_j_pumPSR_del_Y25 (GlobalData_t)(0.)
#define partial_JpumPSR_j_pumPSR_del_Y7 (GlobalData_t)(0.)
#define partial_Jrel_SR_EC50_SR_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_EC50_SR_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_EC50_SR_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_EC50_SR_del_Y23 (GlobalData_t)(0.)
#define partial_Jrel_SR_EC50_SR_del_Y24 (GlobalData_t)(0.)
#define partial_Jrel_SR_EC50_SR_del_Y25 (GlobalData_t)(0.)
#define partial_Jrel_SR_EC50_SR_del_Y6 (GlobalData_t)(0.)
#define partial_Jrel_SR_EC50_SR_del_Y7 (GlobalData_t)(0.)
#define partial_Jrel_SR_HSR_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_HSR_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_HSR_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_HSR_del_Y23 (GlobalData_t)(0.)
#define partial_Jrel_SR_HSR_del_Y24 (GlobalData_t)(0.)
#define partial_Jrel_SR_HSR_del_Y25 (GlobalData_t)(0.)
#define partial_Jrel_SR_HSR_del_Y6 (GlobalData_t)(0.)
#define partial_Jrel_SR_HSR_del_Y7 (GlobalData_t)(0.)
#define partial_Jrel_SR_Max_SR_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_Max_SR_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_Max_SR_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_Max_SR_del_Y23 (GlobalData_t)(0.)
#define partial_Jrel_SR_Max_SR_del_Y24 (GlobalData_t)(0.)
#define partial_Jrel_SR_Max_SR_del_Y25 (GlobalData_t)(0.)
#define partial_Jrel_SR_Max_SR_del_Y6 (GlobalData_t)(0.)
#define partial_Jrel_SR_Max_SR_del_Y7 (GlobalData_t)(0.)
#define partial_Jrel_SR_Min_SR_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_Min_SR_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_Min_SR_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_Min_SR_del_Y23 (GlobalData_t)(0.)
#define partial_Jrel_SR_Min_SR_del_Y24 (GlobalData_t)(0.)
#define partial_Jrel_SR_Min_SR_del_Y25 (GlobalData_t)(0.)
#define partial_Jrel_SR_Min_SR_del_Y6 (GlobalData_t)(0.)
#define partial_Jrel_SR_Min_SR_del_Y7 (GlobalData_t)(0.)
#define partial_Jrel_SR_RI_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_RI_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_RI_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_RI_del_Y23 (GlobalData_t)(-1.)
#define partial_Jrel_SR_RI_del_Y24 (GlobalData_t)(-1.)
#define partial_Jrel_SR_RI_del_Y25 (GlobalData_t)(-1.)
#define partial_Jrel_SR_RI_del_Y6 (GlobalData_t)(0.)
#define partial_Jrel_SR_RI_del_Y7 (GlobalData_t)(0.)
#define partial_Jrel_SR_j_rel_SR_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_j_rel_SR_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_j_rel_SR_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_j_rel_SR_del_Y23 (GlobalData_t)(0.)
#define partial_Jrel_SR_j_rel_SR_del_Y25 (GlobalData_t)(0.)
#define partial_Jrel_SR_kCaSR_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_kCaSR_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_kCaSR_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_kCaSR_del_Y23 (GlobalData_t)(0.)
#define partial_Jrel_SR_kCaSR_del_Y24 (GlobalData_t)(0.)
#define partial_Jrel_SR_kCaSR_del_Y25 (GlobalData_t)(0.)
#define partial_Jrel_SR_kCaSR_del_Y7 (GlobalData_t)(0.)
#define partial_Jrel_SR_kiCa_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_kiCa_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_kiCa_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_kiCa_del_Y23 (GlobalData_t)(0.)
#define partial_Jrel_SR_kiCa_del_Y24 (GlobalData_t)(0.)
#define partial_Jrel_SR_kiCa_del_Y25 (GlobalData_t)(0.)
#define partial_Jrel_SR_kiCa_del_Y6 (GlobalData_t)(0.)
#define partial_Jrel_SR_kiCa_del_Y7 (GlobalData_t)(0.)
#define partial_Jrel_SR_kiSRCa_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_kiSRCa_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_kiSRCa_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_kiSRCa_del_Y23 (GlobalData_t)(0.)
#define partial_Jrel_SR_kiSRCa_del_Y24 (GlobalData_t)(0.)
#define partial_Jrel_SR_kiSRCa_del_Y25 (GlobalData_t)(0.)
#define partial_Jrel_SR_kiSRCa_del_Y7 (GlobalData_t)(0.)
#define partial_Jrel_SR_kim_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_kim_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_kim_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_kim_del_Y23 (GlobalData_t)(0.)
#define partial_Jrel_SR_kim_del_Y24 (GlobalData_t)(0.)
#define partial_Jrel_SR_kim_del_Y25 (GlobalData_t)(0.)
#define partial_Jrel_SR_kim_del_Y6 (GlobalData_t)(0.)
#define partial_Jrel_SR_kim_del_Y7 (GlobalData_t)(0.)
#define partial_Jrel_SR_koCa_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_koCa_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_koCa_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_koCa_del_Y23 (GlobalData_t)(0.)
#define partial_Jrel_SR_koCa_del_Y24 (GlobalData_t)(0.)
#define partial_Jrel_SR_koCa_del_Y25 (GlobalData_t)(0.)
#define partial_Jrel_SR_koCa_del_Y6 (GlobalData_t)(0.)
#define partial_Jrel_SR_koCa_del_Y7 (GlobalData_t)(0.)
#define partial_Jrel_SR_koSRCa_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_koSRCa_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_koSRCa_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_koSRCa_del_Y23 (GlobalData_t)(0.)
#define partial_Jrel_SR_koSRCa_del_Y24 (GlobalData_t)(0.)
#define partial_Jrel_SR_koSRCa_del_Y25 (GlobalData_t)(0.)
#define partial_Jrel_SR_koSRCa_del_Y7 (GlobalData_t)(0.)
#define partial_Jrel_SR_kom_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_kom_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_kom_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_kom_del_Y23 (GlobalData_t)(0.)
#define partial_Jrel_SR_kom_del_Y24 (GlobalData_t)(0.)
#define partial_Jrel_SR_kom_del_Y25 (GlobalData_t)(0.)
#define partial_Jrel_SR_kom_del_Y6 (GlobalData_t)(0.)
#define partial_Jrel_SR_kom_del_Y7 (GlobalData_t)(0.)
#define partial_Jrel_SR_ks_del_Cai (GlobalData_t)(0.)
#define partial_Jrel_SR_ks_del_Y0 (GlobalData_t)(0.)
#define partial_Jrel_SR_ks_del_Y1 (GlobalData_t)(0.)
#define partial_Jrel_SR_ks_del_Y23 (GlobalData_t)(0.)
#define partial_Jrel_SR_ks_del_Y24 (GlobalData_t)(0.)
#define partial_Jrel_SR_ks_del_Y25 (GlobalData_t)(0.)
#define partial_Jrel_SR_ks_del_Y6 (GlobalData_t)(0.)
#define partial_Jrel_SR_ks_del_Y7 (GlobalData_t)(0.)
#define partial_Mgi_del_Cai (GlobalData_t)(0.)
#define partial_Mgi_del_Y0 (GlobalData_t)(0.)
#define partial_Mgi_del_Y1 (GlobalData_t)(0.)
#define partial_Mgi_del_Y23 (GlobalData_t)(0.)
#define partial_Mgi_del_Y24 (GlobalData_t)(0.)
#define partial_Mgi_del_Y25 (GlobalData_t)(0.)
#define partial_Mgi_del_Y6 (GlobalData_t)(0.)
#define partial_Mgi_del_Y7 (GlobalData_t)(0.)
#define partial_Nae3_del_Cai (GlobalData_t)(0.)
#define partial_Nae3_del_Y0 (GlobalData_t)(0.)
#define partial_Nae3_del_Y1 (GlobalData_t)(0.)
#define partial_Nae3_del_Y23 (GlobalData_t)(0.)
#define partial_Nae3_del_Y24 (GlobalData_t)(0.)
#define partial_Nae3_del_Y25 (GlobalData_t)(0.)
#define partial_Nae3_del_Y6 (GlobalData_t)(0.)
#define partial_Nae3_del_Y7 (GlobalData_t)(0.)
#define partial_Nae_del_Cai (GlobalData_t)(0.)
#define partial_Nae_del_Y0 (GlobalData_t)(0.)
#define partial_Nae_del_Y1 (GlobalData_t)(0.)
#define partial_Nae_del_Y23 (GlobalData_t)(0.)
#define partial_Nae_del_Y24 (GlobalData_t)(0.)
#define partial_Nae_del_Y25 (GlobalData_t)(0.)
#define partial_Nae_del_Y6 (GlobalData_t)(0.)
#define partial_Nae_del_Y7 (GlobalData_t)(0.)
#define partial_Nai3_del_Cai (GlobalData_t)(0.)
#define partial_Nai3_del_Y0 (GlobalData_t)(0.)
#define partial_Nai3_del_Y1 (GlobalData_t)(0.)
#define partial_Nai3_del_Y23 (GlobalData_t)(0.)
#define partial_Nai3_del_Y24 (GlobalData_t)(0.)
#define partial_Nai3_del_Y25 (GlobalData_t)(0.)
#define partial_Nai3_del_Y6 (GlobalData_t)(0.)
#define partial_Nai3_del_Y7 (GlobalData_t)(0.)
#define partial_Nai_del_Cai (GlobalData_t)(0.)
#define partial_Nai_del_Y0 (GlobalData_t)(0.)
#define partial_Nai_del_Y1 (GlobalData_t)(0.)
#define partial_Nai_del_Y23 (GlobalData_t)(0.)
#define partial_Nai_del_Y24 (GlobalData_t)(0.)
#define partial_Nai_del_Y25 (GlobalData_t)(0.)
#define partial_Nai_del_Y6 (GlobalData_t)(0.)
#define partial_Nai_del_Y7 (GlobalData_t)(0.)
#define partial_PI_del_Cai (GlobalData_t)(0.)
#define partial_PI_del_Y0 (GlobalData_t)(0.)
#define partial_PI_del_Y1 (GlobalData_t)(0.)
#define partial_PI_del_Y23 (GlobalData_t)(0.)
#define partial_PI_del_Y24 (GlobalData_t)(0.)
#define partial_PI_del_Y25 (GlobalData_t)(0.)
#define partial_PI_del_Y6 (GlobalData_t)(0.)
#define partial_PI_del_Y7 (GlobalData_t)(0.)
#define partial_RTonF_del_Cai (GlobalData_t)(0.)
#define partial_RTonF_del_Y0 (GlobalData_t)(0.)
#define partial_RTonF_del_Y1 (GlobalData_t)(0.)
#define partial_RTonF_del_Y23 (GlobalData_t)(0.)
#define partial_RTonF_del_Y24 (GlobalData_t)(0.)
#define partial_RTonF_del_Y25 (GlobalData_t)(0.)
#define partial_RTonF_del_Y6 (GlobalData_t)(0.)
#define partial_RTonF_del_Y7 (GlobalData_t)(0.)
#define partial_R_del_Cai (GlobalData_t)(0.)
#define partial_R_del_Y0 (GlobalData_t)(0.)
#define partial_R_del_Y1 (GlobalData_t)(0.)
#define partial_R_del_Y23 (GlobalData_t)(0.)
#define partial_R_del_Y24 (GlobalData_t)(0.)
#define partial_R_del_Y25 (GlobalData_t)(0.)
#define partial_R_del_Y6 (GlobalData_t)(0.)
#define partial_R_del_Y7 (GlobalData_t)(0.)
#define partial_T_del_Cai (GlobalData_t)(0.)
#define partial_T_del_Y0 (GlobalData_t)(0.)
#define partial_T_del_Y1 (GlobalData_t)(0.)
#define partial_T_del_Y23 (GlobalData_t)(0.)
#define partial_T_del_Y24 (GlobalData_t)(0.)
#define partial_T_del_Y25 (GlobalData_t)(0.)
#define partial_T_del_Y6 (GlobalData_t)(0.)
#define partial_T_del_Y7 (GlobalData_t)(0.)
#define partial_Vol_Cell_del_Cai (GlobalData_t)(0.)
#define partial_Vol_Cell_del_Y0 (GlobalData_t)(0.)
#define partial_Vol_Cell_del_Y1 (GlobalData_t)(0.)
#define partial_Vol_Cell_del_Y23 (GlobalData_t)(0.)
#define partial_Vol_Cell_del_Y24 (GlobalData_t)(0.)
#define partial_Vol_Cell_del_Y25 (GlobalData_t)(0.)
#define partial_Vol_Cell_del_Y6 (GlobalData_t)(0.)
#define partial_Vol_Cell_del_Y7 (GlobalData_t)(0.)
#define partial_Vol_SL_del_Cai (GlobalData_t)(0.)
#define partial_Vol_SL_del_Y0 (GlobalData_t)(0.)
#define partial_Vol_SL_del_Y1 (GlobalData_t)(0.)
#define partial_Vol_SL_del_Y23 (GlobalData_t)(0.)
#define partial_Vol_SL_del_Y24 (GlobalData_t)(0.)
#define partial_Vol_SL_del_Y25 (GlobalData_t)(0.)
#define partial_Vol_SL_del_Y6 (GlobalData_t)(0.)
#define partial_Vol_SL_del_Y7 (GlobalData_t)(0.)
#define partial_Vol_SR_del_Cai (GlobalData_t)(0.)
#define partial_Vol_SR_del_Y0 (GlobalData_t)(0.)
#define partial_Vol_SR_del_Y1 (GlobalData_t)(0.)
#define partial_Vol_SR_del_Y23 (GlobalData_t)(0.)
#define partial_Vol_SR_del_Y24 (GlobalData_t)(0.)
#define partial_Vol_SR_del_Y25 (GlobalData_t)(0.)
#define partial_Vol_SR_del_Y6 (GlobalData_t)(0.)
#define partial_Vol_SR_del_Y7 (GlobalData_t)(0.)
#define partial_Vol_cytosol_del_Cai (GlobalData_t)(0.)
#define partial_Vol_cytosol_del_Y0 (GlobalData_t)(0.)
#define partial_Vol_cytosol_del_Y1 (GlobalData_t)(0.)
#define partial_Vol_cytosol_del_Y23 (GlobalData_t)(0.)
#define partial_Vol_cytosol_del_Y24 (GlobalData_t)(0.)
#define partial_Vol_cytosol_del_Y25 (GlobalData_t)(0.)
#define partial_Vol_cytosol_del_Y6 (GlobalData_t)(0.)
#define partial_Vol_cytosol_del_Y7 (GlobalData_t)(0.)
#define partial_Vol_jct_del_Cai (GlobalData_t)(0.)
#define partial_Vol_jct_del_Y0 (GlobalData_t)(0.)
#define partial_Vol_jct_del_Y1 (GlobalData_t)(0.)
#define partial_Vol_jct_del_Y23 (GlobalData_t)(0.)
#define partial_Vol_jct_del_Y24 (GlobalData_t)(0.)
#define partial_Vol_jct_del_Y25 (GlobalData_t)(0.)
#define partial_Vol_jct_del_Y6 (GlobalData_t)(0.)
#define partial_Vol_jct_del_Y7 (GlobalData_t)(0.)
#define partial_allo_del_Y0 (GlobalData_t)(0.)
#define partial_allo_del_Y1 (GlobalData_t)(0.)
#define partial_allo_del_Y23 (GlobalData_t)(0.)
#define partial_allo_del_Y24 (GlobalData_t)(0.)
#define partial_allo_del_Y25 (GlobalData_t)(0.)
#define partial_allo_del_Y6 (GlobalData_t)(0.)
#define partial_allo_del_Y7 (GlobalData_t)(0.)
#define partial_cell_length_del_Cai (GlobalData_t)(0.)
#define partial_cell_length_del_Y0 (GlobalData_t)(0.)
#define partial_cell_length_del_Y1 (GlobalData_t)(0.)
#define partial_cell_length_del_Y23 (GlobalData_t)(0.)
#define partial_cell_length_del_Y24 (GlobalData_t)(0.)
#define partial_cell_length_del_Y25 (GlobalData_t)(0.)
#define partial_cell_length_del_Y6 (GlobalData_t)(0.)
#define partial_cell_length_del_Y7 (GlobalData_t)(0.)
#define partial_cell_radius_del_Cai (GlobalData_t)(0.)
#define partial_cell_radius_del_Y0 (GlobalData_t)(0.)
#define partial_cell_radius_del_Y1 (GlobalData_t)(0.)
#define partial_cell_radius_del_Y23 (GlobalData_t)(0.)
#define partial_cell_radius_del_Y24 (GlobalData_t)(0.)
#define partial_cell_radius_del_Y25 (GlobalData_t)(0.)
#define partial_cell_radius_del_Y6 (GlobalData_t)(0.)
#define partial_cell_radius_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Calmodulin_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Calmodulin_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Calmodulin_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Calmodulin_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Calmodulin_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Calmodulin_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Calmodulin_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Calmodulin_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Ca_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Ca_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Ca_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Ca_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Ca_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Ca_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Ca_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Ca_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Mg_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Mg_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Mg_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Mg_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Mg_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Mg_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Mg_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_Myosin_Mg_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_SRB_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_SRB_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_SRB_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_SRB_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_SRB_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_SRB_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_SRB_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_SRB_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MgMg_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MgMg_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MgMg_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MgMg_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MgMg_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MgMg_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MgMg_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MgMg_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_Bmax_TroponinC_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Calmodulin_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Calmodulin_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Calmodulin_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Calmodulin_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Calmodulin_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Calmodulin_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Calmodulin_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Myosin_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Myosin_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Myosin_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Myosin_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Myosin_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Myosin_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_Myosin_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_SRB_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_SRB_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_SRB_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_SRB_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_SRB_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_SRB_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_SRB_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_TroponinC_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_cytosol_tot_bound_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_cytosol_tot_bound_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_cytosol_tot_bound_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_cytosol_tot_bound_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_cytosol_tot_bound_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_cytosol_tot_bound_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dCa_cytosol_tot_bound_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgMyosin_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgMyosin_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgMyosin_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgMyosin_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgMyosin_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgMyosin_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgMyosin_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgMyosin_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Calmodulin_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Calmodulin_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Calmodulin_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Calmodulin_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Calmodulin_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Calmodulin_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Calmodulin_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Calmodulin_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Ca_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Ca_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Ca_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Ca_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Ca_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Ca_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Ca_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Ca_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Mg_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Mg_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Mg_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Mg_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Mg_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Mg_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Mg_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_Myosin_Mg_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_SRB_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_SRB_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_SRB_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_SRB_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_SRB_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_SRB_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_SRB_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_SRB_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MGCa_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MGCa_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MGCa_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MGCa_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MGCa_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MGCa_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MGCa_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MGCa_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MgMg_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MgMg_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MgMg_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MgMg_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MgMg_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MgMg_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MgMg_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_Ca_MgMg_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_koff_TroponinC_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Calmodulin_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Calmodulin_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Calmodulin_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Calmodulin_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Calmodulin_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Calmodulin_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Calmodulin_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Calmodulin_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Ca_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Ca_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Ca_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Ca_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Ca_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Ca_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Ca_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Ca_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Mg_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Mg_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Mg_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Mg_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Mg_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Mg_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Mg_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_Myosin_Mg_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_SRB_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_SRB_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_SRB_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_SRB_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_SRB_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_SRB_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_SRB_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_SRB_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MgMg_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MgMg_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MgMg_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MgMg_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MgMg_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MgMg_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MgMg_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_Ca_MgMg_del_Y7 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_del_Cai (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_del_Y0 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_del_Y1 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_del_Y23 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_del_Y24 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_del_Y25 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_del_Y6 (GlobalData_t)(0.)
#define partial_cytosolic_Ca_buffer_kon_TroponinC_del_Y7 (GlobalData_t)(0.)
#define partial_deltaE_del_Y0 (GlobalData_t)(0.)
#define partial_deltaE_del_Y1 (GlobalData_t)(0.)
#define partial_deltaE_del_Y23 (GlobalData_t)(0.)
#define partial_deltaE_del_Y24 (GlobalData_t)(0.)
#define partial_deltaE_del_Y25 (GlobalData_t)(0.)
#define partial_deltaE_del_Y6 (GlobalData_t)(0.)
#define partial_deltaE_del_Y7 (GlobalData_t)(0.)
#define partial_denommult_del_Cai (GlobalData_t)(0.)
#define partial_denommult_del_Y0 (GlobalData_t)(0.)
#define partial_denommult_del_Y1 (GlobalData_t)(0.)
#define partial_denommult_del_Y23 (GlobalData_t)(0.)
#define partial_denommult_del_Y24 (GlobalData_t)(0.)
#define partial_denommult_del_Y25 (GlobalData_t)(0.)
#define partial_denommult_del_Y6 (GlobalData_t)(0.)
#define partial_denommult_del_Y7 (GlobalData_t)(0.)
#define partial_denomterm1_del_Y0 (GlobalData_t)(0.)
#define partial_denomterm1_del_Y1 (GlobalData_t)(0.)
#define partial_denomterm1_del_Y23 (GlobalData_t)(0.)
#define partial_denomterm1_del_Y24 (GlobalData_t)(0.)
#define partial_denomterm1_del_Y25 (GlobalData_t)(0.)
#define partial_denomterm1_del_Y6 (GlobalData_t)(0.)
#define partial_denomterm1_del_Y7 (GlobalData_t)(0.)
#define partial_denomterm2_del_Y0 (GlobalData_t)(0.)
#define partial_denomterm2_del_Y1 (GlobalData_t)(0.)
#define partial_denomterm2_del_Y23 (GlobalData_t)(0.)
#define partial_denomterm2_del_Y24 (GlobalData_t)(0.)
#define partial_denomterm2_del_Y25 (GlobalData_t)(0.)
#define partial_denomterm2_del_Y6 (GlobalData_t)(0.)
#define partial_denomterm2_del_Y7 (GlobalData_t)(0.)
#define partial_diff_Cai_del_Y0 (GlobalData_t)(0.)
#define partial_diff_Cai_del_Y23 (GlobalData_t)(0.)
#define partial_diff_Cai_del_Y24 (GlobalData_t)(0.)
#define partial_diff_Cai_del_Y25 (GlobalData_t)(0.)
#define partial_diff_Cai_del_Y7 (GlobalData_t)(0.)
#define partial_diff_Y0_del_Cai (GlobalData_t)(0.)
#define partial_diff_Y0_del_Y1 (GlobalData_t)(0.)
#define partial_diff_Y0_del_Y23 (GlobalData_t)(0.)
#define partial_diff_Y0_del_Y24 (GlobalData_t)(0.)
#define partial_diff_Y0_del_Y25 (GlobalData_t)(0.)
#define partial_diff_Y0_del_Y7 (GlobalData_t)(0.)
#define partial_diff_Y1_del_Y0 (GlobalData_t)(0.)
#define partial_diff_Y1_del_Y23 (GlobalData_t)(0.)
#define partial_diff_Y1_del_Y24 (GlobalData_t)(0.)
#define partial_diff_Y1_del_Y25 (GlobalData_t)(0.)
#define partial_diff_Y1_del_Y6 (GlobalData_t)(0.)
#define partial_diff_Y23_del_Cai (GlobalData_t)(0.)
#define partial_diff_Y23_del_Y0 (GlobalData_t)(0.)
#define partial_diff_Y23_del_Y1 (GlobalData_t)(0.)
#define partial_diff_Y24_del_Cai (GlobalData_t)(0.)
#define partial_diff_Y24_del_Y0 (GlobalData_t)(0.)
#define partial_diff_Y24_del_Y1 (GlobalData_t)(0.)
#define partial_diff_Y25_del_Cai (GlobalData_t)(0.)
#define partial_diff_Y25_del_Y0 (GlobalData_t)(0.)
#define partial_diff_Y25_del_Y1 (GlobalData_t)(0.)
#define partial_diff_Y6_del_Y1 (GlobalData_t)(0.)
#define partial_diff_Y6_del_Y23 (GlobalData_t)(0.)
#define partial_diff_Y6_del_Y25 (GlobalData_t)(0.)
#define partial_diff_Y7_del_Cai (GlobalData_t)(0.)
#define partial_diff_Y7_del_Y0 (GlobalData_t)(0.)
#define partial_diff_Y7_del_Y23 (GlobalData_t)(0.)
#define partial_diff_Y7_del_Y25 (GlobalData_t)(0.)
#define partial_exPnuVFRT_del_Cai (GlobalData_t)(0.)
#define partial_exPnuVFRT_del_Y0 (GlobalData_t)(0.)
#define partial_exPnuVFRT_del_Y1 (GlobalData_t)(0.)
#define partial_exPnuVFRT_del_Y23 (GlobalData_t)(0.)
#define partial_exPnuVFRT_del_Y24 (GlobalData_t)(0.)
#define partial_exPnuVFRT_del_Y25 (GlobalData_t)(0.)
#define partial_exPnuVFRT_del_Y6 (GlobalData_t)(0.)
#define partial_exPnuVFRT_del_Y7 (GlobalData_t)(0.)
#define partial_exPnum1VFRT_del_Cai (GlobalData_t)(0.)
#define partial_exPnum1VFRT_del_Y0 (GlobalData_t)(0.)
#define partial_exPnum1VFRT_del_Y1 (GlobalData_t)(0.)
#define partial_exPnum1VFRT_del_Y23 (GlobalData_t)(0.)
#define partial_exPnum1VFRT_del_Y24 (GlobalData_t)(0.)
#define partial_exPnum1VFRT_del_Y25 (GlobalData_t)(0.)
#define partial_exPnum1VFRT_del_Y6 (GlobalData_t)(0.)
#define partial_exPnum1VFRT_del_Y7 (GlobalData_t)(0.)
#define partial_ion_diffusion_J_Ca_SL_cytosol_del_Cai (GlobalData_t)(((-(1000./(1000.*1000.)))*3.7243e-12))
#define partial_ion_diffusion_J_Ca_SL_cytosol_del_Y0 (GlobalData_t)(0.)
#define partial_ion_diffusion_J_Ca_SL_cytosol_del_Y1 (GlobalData_t)(3.7243e-12)
#define partial_ion_diffusion_J_Ca_SL_cytosol_del_Y23 (GlobalData_t)(0.)
#define partial_ion_diffusion_J_Ca_SL_cytosol_del_Y24 (GlobalData_t)(0.)
#define partial_ion_diffusion_J_Ca_SL_cytosol_del_Y25 (GlobalData_t)(0.)
#define partial_ion_diffusion_J_Ca_SL_cytosol_del_Y6 (GlobalData_t)(0.)
#define partial_ion_diffusion_J_Ca_SL_cytosol_del_Y7 (GlobalData_t)(0.)
#define partial_ion_diffusion_J_Ca_jct_SL_del_Cai (GlobalData_t)(0.)
#define partial_ion_diffusion_J_Ca_jct_SL_del_Y0 (GlobalData_t)(0.)
#define partial_ion_diffusion_J_Ca_jct_SL_del_Y1 (GlobalData_t)((-1.*8.2413e-13))
#define partial_ion_diffusion_J_Ca_jct_SL_del_Y23 (GlobalData_t)(0.)
#define partial_ion_diffusion_J_Ca_jct_SL_del_Y24 (GlobalData_t)(0.)
#define partial_ion_diffusion_J_Ca_jct_SL_del_Y25 (GlobalData_t)(0.)
#define partial_ion_diffusion_J_Ca_jct_SL_del_Y6 (GlobalData_t)(0.)
#define partial_ion_diffusion_J_Ca_jct_SL_del_Y7 (GlobalData_t)(8.2413e-13)
#define partial_kmcaact_del_Cai (GlobalData_t)(0.)
#define partial_kmcaact_del_Y0 (GlobalData_t)(0.)
#define partial_kmcaact_del_Y1 (GlobalData_t)(0.)
#define partial_kmcaact_del_Y23 (GlobalData_t)(0.)
#define partial_kmcaact_del_Y24 (GlobalData_t)(0.)
#define partial_kmcaact_del_Y25 (GlobalData_t)(0.)
#define partial_kmcaact_del_Y6 (GlobalData_t)(0.)
#define partial_kmcaact_del_Y7 (GlobalData_t)(0.)
#define partial_kmcai_del_Cai (GlobalData_t)(0.)
#define partial_kmcai_del_Y0 (GlobalData_t)(0.)
#define partial_kmcai_del_Y1 (GlobalData_t)(0.)
#define partial_kmcai_del_Y23 (GlobalData_t)(0.)
#define partial_kmcai_del_Y24 (GlobalData_t)(0.)
#define partial_kmcai_del_Y25 (GlobalData_t)(0.)
#define partial_kmcai_del_Y6 (GlobalData_t)(0.)
#define partial_kmcai_del_Y7 (GlobalData_t)(0.)
#define partial_kmcao_del_Cai (GlobalData_t)(0.)
#define partial_kmcao_del_Y0 (GlobalData_t)(0.)
#define partial_kmcao_del_Y1 (GlobalData_t)(0.)
#define partial_kmcao_del_Y23 (GlobalData_t)(0.)
#define partial_kmcao_del_Y24 (GlobalData_t)(0.)
#define partial_kmcao_del_Y25 (GlobalData_t)(0.)
#define partial_kmcao_del_Y6 (GlobalData_t)(0.)
#define partial_kmcao_del_Y7 (GlobalData_t)(0.)
#define partial_kmnai13_del_Cai (GlobalData_t)(0.)
#define partial_kmnai13_del_Y0 (GlobalData_t)(0.)
#define partial_kmnai13_del_Y1 (GlobalData_t)(0.)
#define partial_kmnai13_del_Y23 (GlobalData_t)(0.)
#define partial_kmnai13_del_Y24 (GlobalData_t)(0.)
#define partial_kmnai13_del_Y25 (GlobalData_t)(0.)
#define partial_kmnai13_del_Y6 (GlobalData_t)(0.)
#define partial_kmnai13_del_Y7 (GlobalData_t)(0.)
#define partial_kmnai1_del_Cai (GlobalData_t)(0.)
#define partial_kmnai1_del_Y0 (GlobalData_t)(0.)
#define partial_kmnai1_del_Y1 (GlobalData_t)(0.)
#define partial_kmnai1_del_Y23 (GlobalData_t)(0.)
#define partial_kmnai1_del_Y24 (GlobalData_t)(0.)
#define partial_kmnai1_del_Y25 (GlobalData_t)(0.)
#define partial_kmnai1_del_Y6 (GlobalData_t)(0.)
#define partial_kmnai1_del_Y7 (GlobalData_t)(0.)
#define partial_kmnao3_del_Cai (GlobalData_t)(0.)
#define partial_kmnao3_del_Y0 (GlobalData_t)(0.)
#define partial_kmnao3_del_Y1 (GlobalData_t)(0.)
#define partial_kmnao3_del_Y23 (GlobalData_t)(0.)
#define partial_kmnao3_del_Y24 (GlobalData_t)(0.)
#define partial_kmnao3_del_Y25 (GlobalData_t)(0.)
#define partial_kmnao3_del_Y6 (GlobalData_t)(0.)
#define partial_kmnao3_del_Y7 (GlobalData_t)(0.)
#define partial_kmnao_del_Cai (GlobalData_t)(0.)
#define partial_kmnao_del_Y0 (GlobalData_t)(0.)
#define partial_kmnao_del_Y1 (GlobalData_t)(0.)
#define partial_kmnao_del_Y23 (GlobalData_t)(0.)
#define partial_kmnao_del_Y24 (GlobalData_t)(0.)
#define partial_kmnao_del_Y25 (GlobalData_t)(0.)
#define partial_kmnao_del_Y6 (GlobalData_t)(0.)
#define partial_kmnao_del_Y7 (GlobalData_t)(0.)
#define partial_ksat_del_Cai (GlobalData_t)(0.)
#define partial_ksat_del_Y0 (GlobalData_t)(0.)
#define partial_ksat_del_Y1 (GlobalData_t)(0.)
#define partial_ksat_del_Y23 (GlobalData_t)(0.)
#define partial_ksat_del_Y24 (GlobalData_t)(0.)
#define partial_ksat_del_Y25 (GlobalData_t)(0.)
#define partial_ksat_del_Y6 (GlobalData_t)(0.)
#define partial_ksat_del_Y7 (GlobalData_t)(0.)
#define partial_nu_del_Cai (GlobalData_t)(0.)
#define partial_nu_del_Y0 (GlobalData_t)(0.)
#define partial_nu_del_Y1 (GlobalData_t)(0.)
#define partial_nu_del_Y23 (GlobalData_t)(0.)
#define partial_nu_del_Y24 (GlobalData_t)(0.)
#define partial_nu_del_Y25 (GlobalData_t)(0.)
#define partial_nu_del_Y6 (GlobalData_t)(0.)
#define partial_nu_del_Y7 (GlobalData_t)(0.)
#define partial_num_del_Y0 (GlobalData_t)(0.)
#define partial_num_del_Y1 (GlobalData_t)(0.)
#define partial_num_del_Y23 (GlobalData_t)(0.)
#define partial_num_del_Y24 (GlobalData_t)(0.)
#define partial_num_del_Y25 (GlobalData_t)(0.)
#define partial_num_del_Y6 (GlobalData_t)(0.)
#define partial_num_del_Y7 (GlobalData_t)(0.)
#define x_init (GlobalData_t)(0.00036627)
#define xr_init (GlobalData_t)(0.00195127)
#define xs_init (GlobalData_t)(0.03065770)
#define y1_init (GlobalData_t)(0.99816200)
#define y2_init (GlobalData_t)(0.99816200)
#define Cm (GlobalData_t)(((((((Cm_per_area*2.0)*cell_radius)/10000.0)*PI)*cell_length)/10000.0))
#define Vol_Cell (GlobalData_t)((((PI*(square((cell_radius/1000.0))))*cell_length)/(cube(1000.0))))
#define partial_JleaKSR_j_leaKSR_del_Y6 (GlobalData_t)((0.5*JleaKSR_KSRleak))
#define partial_JleaKSR_j_leaKSR_del_Y7 (GlobalData_t)(((0.5*JleaKSR_KSRleak)*-1.))
#define partial_diff_Y24_del_Y23 (GlobalData_t)((-(-Jrel_SR_kim)))
#define partial_diff_Y25_del_Y23 (GlobalData_t)((Jrel_SR_kim*-1.))
#define partial_diff_Y25_del_Y24 (GlobalData_t)(((Jrel_SR_kim*-1.)-((-Jrel_SR_kom))))
#define Vol_SL (GlobalData_t)((0.02*Vol_Cell))
#define Vol_SR (GlobalData_t)((0.035*Vol_Cell))
#define Vol_cytosol (GlobalData_t)((0.65*Vol_Cell))
#define Vol_jct (GlobalData_t)((0.00051*Vol_Cell))
#define partial_diff_Y1_del_Y7 (GlobalData_t)(((Vol_SL*8.2413e-13)/(Vol_SL*Vol_SL)))
#define partial_diff_Y7_del_Y1 (GlobalData_t)((-((Vol_jct*(-1.*8.2413e-13))/(Vol_jct*Vol_jct))))



void initialize_params_AslanidiSleiman( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  AslanidiSleiman_Params *p = (AslanidiSleiman_Params *)IF->params;

  // Compute the regional constants
  {
    p->Ca_handling = 1.;
    p->Cae = 1.8;
    p->Cle = 150.0;
    p->Cli = 30.0;
    p->GCaB = 3.51820e-04;
    p->GCaL = 0.27;
    p->GCaT = 0.2;
    p->GCl = 0.3;
    p->GClB = 0.0009;
    p->GK1 = 0.5;
    p->GKB = 5.00000e-05;
    p->GKr = 0.015583333333333;
    p->GKs = 0.011738183745949;
    p->GNa = 20.;
    p->GNaB = 2.97000e-05;
    p->GNaL = 0.0162;
    p->Gto = 0.112;
    p->ICaPHill = 1.6;
    p->ICaPKmf = 0.5;
    p->ICaPVmf = (0.0538*1.25);
    p->INaCamax = 4.5;
    p->Ke_init = 5.4;
    p->Ki = 135.0;
    p->Mgi = 1.0;
    p->Nae = 140.0;
    p->Nai = 8.8;
    p->PNaK = 0.01833;
    p->T = 310.0;
    p->kmcaact = 0.000125;
    p->kmcai = 0.0036;
    p->kmcao = 1.3;
    p->kmnai1 = 12.3;
    p->kmnao = 87.5;
    p->ksat = 0.27;
    p->nu = 0.35;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_AslanidiSleiman;

}


// Define the parameters for the lookup tables
enum Tables {
  Cai_mM_TAB,
  V_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum Cai_mM_TableIndex {
  E_Ca_idx,
  ICaP_idx,
  allo_idx,
  denomterm1_idx,
  denomterm2_idx,
  NROWS_Cai_mM
};

enum V_TableIndex {
  IClB_idx,
  IKPKp_idx,
  INaB_idx,
  a_inf_idx,
  b_rush_larsen_A_idx,
  b_rush_larsen_B_idx,
  d_rush_larsen_A_idx,
  d_rush_larsen_B_idx,
  denommult_idx,
  exPnuVFRT_idx,
  exPnum1VFRT_idx,
  fNaK_idx,
  f_rush_larsen_A_idx,
  f_rush_larsen_B_idx,
  g_rush_larsen_A_idx,
  g_rush_larsen_B_idx,
  hL_rush_larsen_A_idx,
  hL_rush_larsen_B_idx,
  h_rush_larsen_A_idx,
  h_rush_larsen_B_idx,
  i_rush_larsen_A_idx,
  i_rush_larsen_B_idx,
  j_rush_larsen_A_idx,
  j_rush_larsen_B_idx,
  mL_rush_larsen_A_idx,
  mL_rush_larsen_B_idx,
  m_rush_larsen_A_idx,
  m_rush_larsen_B_idx,
  r_infty_idx,
  x_rush_larsen_A_idx,
  x_rush_larsen_B_idx,
  xr_rush_larsen_A_idx,
  xr_rush_larsen_B_idx,
  xs_rush_larsen_A_idx,
  xs_rush_larsen_B_idx,
  y1_rush_larsen_A_idx,
  y1_rush_larsen_B_idx,
  y2_rush_larsen_A_idx,
  y2_rush_larsen_B_idx,
  NROWS_V
};



void construct_tables_AslanidiSleiman( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  AslanidiSleiman_Params *p = (AslanidiSleiman_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double Nae3 = ((p->Nae*p->Nae)*p->Nae);
  double Nai3 = ((p->Nai*p->Nai)*p->Nai);
  double RTonF = ((R*p->T)/F);
  double kmnai13 = ((p->kmnai1*p->kmnai1)*p->kmnai1);
  double kmnao3 = ((p->kmnao*p->kmnao)*p->kmnao);
  double partial_diff_Cai_del_Y1 = ((p->Ca_handling==1.) ? (((Vol_cytosol*3.7243e-12)/(Vol_cytosol*Vol_cytosol))*1000.) : 0.);
  double sigma = (((exp((p->Nae/67.3)))-(1.0))/7.0);
  double E_Cl = (RTonF*(log((p->Cli/p->Cle))));
  double E_Na = (RTonF*(log((p->Nae/p->Nai))));
  double partial_denomterm1_del_Cai = (((kmnao3*1.5)*(1000./(1000.*1000.)))+((kmnai13*p->Cae)*((p->kmcai*(1.5*(1000./(1000.*1000.))))/(p->kmcai*p->kmcai))));
  double partial_denomterm2_del_Cai = ((Nae3*1.5)*(1000./(1000.*1000.)));
  
  // Create the Cai_mM lookup table
  LUT* Cai_mM_tab = &IF->tables[Cai_mM_TAB];
  LUT_alloc(Cai_mM_tab, NROWS_Cai_mM, 1e-5, 20, 1e-5, "AslanidiSleiman Cai_mM");
  for (int __i=Cai_mM_tab->mn_ind; __i<=Cai_mM_tab->mx_ind; __i++) {
    double Cai_mM = Cai_mM_tab->res*__i;
    LUT_data_t* Cai_mM_row = Cai_mM_tab->tab[__i];
    Cai_mM_row[E_Ca_idx] = ((RTonF/2.0)*(log((p->Cae/Cai_mM))));
    Cai_mM_row[ICaP_idx] = ((0.5*p->ICaPVmf)/(1.0+(pow((p->ICaPKmf/(Cai_mM*1000.)),p->ICaPHill))));
    Cai_mM_row[allo_idx] = (1./(1.+(square((p->kmcaact/(1.5*Cai_mM))))));
    Cai_mM_row[denomterm1_idx] = (((p->kmcao*Nai3)+((kmnao3*1.5)*Cai_mM))+((kmnai13*p->Cae)*(1.+((1.5*Cai_mM)/p->kmcai))));
    Cai_mM_row[denomterm2_idx] = ((((p->kmcai*Nae3)*(1.+(Nai3/kmnai13)))+(Nai3*p->Cae))+((Nae3*1.5)*Cai_mM));
  }
  check_LUT(Cai_mM_tab);
  
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -1500, 1500, 0.05, "AslanidiSleiman V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    V_row[IClB_idx] = (p->GClB*(V-(E_Cl)));
    V_row[IKPKp_idx] = (1.0/(1.0+(exp(((7.488-(V))/5.98)))));
    V_row[INaB_idx] = (p->GNaB*(V-(E_Na)));
    double a_h = ((V>=-40.) ? 0. : (0.135*(exp(((80.+V)/-6.8)))));
    V_row[a_inf_idx] = (1.0/(1.0+(exp(((-(V+5.0))/10.0)))));
    double a_j = ((V>=-40.) ? 0. : ((((-1.2714*10e5)*(exp((0.2444*V))))-((((3.474*10e-5)*(exp((-0.0439*V))))*(V+37.78))))/(1.+(exp((0.311*(V+79.23)))))));
    double a_m = ((0.32*(V+47.13))/(1.-((exp((-0.1*(V+47.13)))))));
    double a_mL = (((fabs((V+47.13)))<0.05) ? (0.32/0.1) : ((0.32*(V+47.13))/(1.-((exp((-0.1*(V+47.13))))))));
    double aa_b = (1.068*(exp(((V+16.3)/30.0))));
    double aa_g = (0.015*(exp(((-(V+71.7))/83.3))));
    double aa_x = (0.0451*(exp(((0.85*0.03577)*V))));
    double aa_y = ((0.05415*(exp(((-(V+12.5))/15.))))/(1.+(0.051335*(exp(((-(V+12.5))/15.))))));
    double b_h = ((V>=-40.) ? (1./(0.13*(1.+(exp(((V+10.66)/-11.1)))))) : ((0.3*(exp(((-2.535*10e-7)*V))))/(1.+(exp((-0.1*(V+32.)))))));
    double b_inf = (1./(1.+(exp(((-(V+28.))/6.1)))));
    double b_j = ((V>=-40.) ? ((0.3*(exp(((-2.535*10e-7)*V))))/(1.+(exp((-0.1*(V+32.)))))) : ((0.1212*(exp((-0.01052*V))))/(1.+(exp((-0.1378*(V+40.14)))))));
    double b_m = (0.08*(exp(((-V)/11.))));
    double b_mL = (0.08*(exp(((-V)/11.))));
    double bb_b = (1.068*(exp(((-(V+16.3))/30.0))));
    double bb_g = (0.015*(exp(((V+71.7)/15.4))));
    double bb_x = (0.0989*(exp(((-0.85*0.0623)*V))));
    double bb_y = ((0.05415*(exp(((V+33.5)/15.))))/(1.+(0.051335*(exp(((V+33.5)/15.))))));
    double d_inf = (1./(1.0+(exp(((-(V-(4.0)))/6.74)))));
    V_row[denommult_idx] = (1.+(p->ksat*(exp(((((p->nu-(1.))*V)*F)/(R*p->T))))));
    V_row[exPnuVFRT_idx] = (exp((((p->nu*V)*F)/(R*p->T))));
    V_row[exPnum1VFRT_idx] = (exp(((((p->nu-(1.))*V)*F)/(R*p->T))));
    V_row[fNaK_idx] = (1.0/((1.0+(0.1245*(exp(((-0.1*V)/RTonF)))))+((0.0365*sigma)*(exp(((-1.0*V)/RTonF))))));
    double f_inf = (1./(1.0+(exp(((V+25.)/10.)))));
    double g_inf = (1./(1.+(exp(((V+60.)/6.6)))));
    double hL_inf = (1./(1.+(exp((((V-(22.0))+91.0)/6.1)))));
    double i_inf = (1.0/(1.0+(exp(((V+75.0)/10.0)))));
    V_row[r_infty_idx] = (1.0/(1.0+(exp((V/50.)))));
    double tau_d = (0.59+((0.8*(exp((0.052*(V+13.)))))/(1.+(exp((0.132*(V+13.)))))));
    double tau_f = ((0.005*(square((V-(2.5)))))+4.0);
    double tau_hL = (132.4+(112.8*(exp((0.02325*V)))));
    double tau_i = (((10.0/(1.0+(exp(((V+33.5)/10.0)))))+10.0)*2.);
    double tau_xr = (1.0/(((fabs((V+7.)))<0.05) ? (0.00138/0.123) : (((0.00138*(V+7.0))/(1.0-((exp((-0.123*(V+7.0)))))))+(((fabs((V+10.)))<0.05) ? (0.00061/0.145) : ((0.00061*(V+10.0))/((exp((0.145*(V+10.0))))-(1.0)))))));
    double tau_xs = ((600.0/(1.0+(exp(((V-(20.))/15.0)))))+250.0);
    double xr_inf = (1.0/(1.0+(exp(((-1.0*(V+20.0))/10.5)))));
    double xs_inf = (1.1/(1.0+(exp(((-1.0*(V-(1.5)))/20.)))));
    V_row[d_rush_larsen_B_idx] = (exp(((-dt)/tau_d)));
    double d_rush_larsen_C = (expm1(((-dt)/tau_d)));
    V_row[f_rush_larsen_B_idx] = (exp(((-dt)/tau_f)));
    double f_rush_larsen_C = (expm1(((-dt)/tau_f)));
    V_row[hL_rush_larsen_B_idx] = (exp(((-dt)/tau_hL)));
    double hL_rush_larsen_C = (expm1(((-dt)/tau_hL)));
    V_row[h_rush_larsen_A_idx] = (((-a_h)/(a_h+b_h))*(expm1(((-dt)*(a_h+b_h)))));
    V_row[h_rush_larsen_B_idx] = (exp(((-dt)*(a_h+b_h))));
    V_row[i_rush_larsen_B_idx] = (exp(((-dt)/tau_i)));
    double i_rush_larsen_C = (expm1(((-dt)/tau_i)));
    V_row[j_rush_larsen_A_idx] = (((-a_j)/(a_j+b_j))*(expm1(((-dt)*(a_j+b_j)))));
    V_row[j_rush_larsen_B_idx] = (exp(((-dt)*(a_j+b_j))));
    V_row[mL_rush_larsen_A_idx] = (((-a_mL)/(a_mL+b_mL))*(expm1(((-dt)*(a_mL+b_mL)))));
    V_row[mL_rush_larsen_B_idx] = (exp(((-dt)*(a_mL+b_mL))));
    V_row[m_rush_larsen_A_idx] = (((-a_m)/(a_m+b_m))*(expm1(((-dt)*(a_m+b_m)))));
    V_row[m_rush_larsen_B_idx] = (exp(((-dt)*(a_m+b_m))));
    double tau_b = (1.0/(aa_b+bb_b));
    double tau_g = (1.0/(aa_g+bb_g));
    double tau_x = (0.2/(aa_x+bb_x));
    double tau_y1 = (0.7*(15.+(20.0/(aa_y+bb_y))));
    double tau_y2 = (4.0/(aa_y+bb_y));
    double x_inf = (aa_x/(aa_x+bb_x));
    V_row[xr_rush_larsen_B_idx] = (exp(((-dt)/tau_xr)));
    double xr_rush_larsen_C = (expm1(((-dt)/tau_xr)));
    V_row[xs_rush_larsen_B_idx] = (exp(((-dt)/tau_xs)));
    double xs_rush_larsen_C = (expm1(((-dt)/tau_xs)));
    double y1_inf = (aa_y/(aa_y+bb_y));
    V_row[b_rush_larsen_B_idx] = (exp(((-dt)/tau_b)));
    double b_rush_larsen_C = (expm1(((-dt)/tau_b)));
    V_row[d_rush_larsen_A_idx] = ((-d_inf)*d_rush_larsen_C);
    V_row[f_rush_larsen_A_idx] = ((-f_inf)*f_rush_larsen_C);
    V_row[g_rush_larsen_B_idx] = (exp(((-dt)/tau_g)));
    double g_rush_larsen_C = (expm1(((-dt)/tau_g)));
    V_row[hL_rush_larsen_A_idx] = ((-hL_inf)*hL_rush_larsen_C);
    V_row[i_rush_larsen_A_idx] = ((-i_inf)*i_rush_larsen_C);
    V_row[x_rush_larsen_B_idx] = (exp(((-dt)/tau_x)));
    double x_rush_larsen_C = (expm1(((-dt)/tau_x)));
    V_row[xr_rush_larsen_A_idx] = ((-xr_inf)*xr_rush_larsen_C);
    V_row[xs_rush_larsen_A_idx] = ((-xs_inf)*xs_rush_larsen_C);
    V_row[y1_rush_larsen_B_idx] = (exp(((-dt)/tau_y1)));
    double y1_rush_larsen_C = (expm1(((-dt)/tau_y1)));
    double y2_inf = y1_inf;
    V_row[y2_rush_larsen_B_idx] = (exp(((-dt)/tau_y2)));
    double y2_rush_larsen_C = (expm1(((-dt)/tau_y2)));
    V_row[b_rush_larsen_A_idx] = ((-b_inf)*b_rush_larsen_C);
    V_row[g_rush_larsen_A_idx] = ((-g_inf)*g_rush_larsen_C);
    V_row[x_rush_larsen_A_idx] = ((-x_inf)*x_rush_larsen_C);
    V_row[y1_rush_larsen_A_idx] = ((-y1_inf)*y1_rush_larsen_C);
    V_row[y2_rush_larsen_A_idx] = ((-y2_inf)*y2_rush_larsen_C);
  }
  check_LUT(V_tab);
  

}

struct AslanidiSleiman_Regional_Constants {
  GlobalData_t E_Cl;
  GlobalData_t E_Na;
  GlobalData_t Nae3;
  GlobalData_t Nai3;
  GlobalData_t RTonF;
  GlobalData_t kmnai13;
  GlobalData_t kmnao3;
  GlobalData_t partial_denomterm1_del_Cai;
  GlobalData_t partial_denomterm2_del_Cai;
  GlobalData_t partial_diff_Cai_del_Y1;
  GlobalData_t sigma;

};

struct AslanidiSleiman_Nodal_Req {
  GlobalData_t Ke;
  GlobalData_t V;

};

struct AslanidiSleiman_Private {
  ION_IF *IF;
  int   node_number;
  void* cvode_mem;
  bool  trace_init;
  AslanidiSleiman_Regional_Constants rc;
  AslanidiSleiman_Nodal_Req nr;
};

//Printing out the Rosenbrock declarations
void AslanidiSleiman_rosenbrock_f(float*, float*, void*);
void AslanidiSleiman_rosenbrock_jacobian(float**, float*, void*, int);

enum Rosenbrock {
  ROSEN_Cai,
  ROSEN_Y0,
  ROSEN_Y1,
  ROSEN_Y23,
  ROSEN_Y24,
  ROSEN_Y25,
  ROSEN_Y6,
  ROSEN_Y7,

  N_ROSEN
};

void rbStepX  ( float *X,
                void (*calcDX) (float*,  float*, void*),
                void (*calcJ)  (float**, float*, void*, int ),
                void *params, float h, int N );




void    initialize_sv_AslanidiSleiman( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  AslanidiSleiman_Params *p = (AslanidiSleiman_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(AslanidiSleiman_state) );
  AslanidiSleiman_state *sv_base = (AslanidiSleiman_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double Nae3 = ((p->Nae*p->Nae)*p->Nae);
  double Nai3 = ((p->Nai*p->Nai)*p->Nai);
  double RTonF = ((R*p->T)/F);
  double kmnai13 = ((p->kmnai1*p->kmnai1)*p->kmnai1);
  double kmnao3 = ((p->kmnao*p->kmnao)*p->kmnao);
  double partial_diff_Cai_del_Y1 = ((p->Ca_handling==1.) ? (((Vol_cytosol*3.7243e-12)/(Vol_cytosol*Vol_cytosol))*1000.) : 0.);
  double sigma = (((exp((p->Nae/67.3)))-(1.0))/7.0);
  double E_Cl = (RTonF*(log((p->Cli/p->Cle))));
  double E_Na = (RTonF*(log((p->Nae/p->Nai))));
  double partial_denomterm1_del_Cai = (((kmnao3*1.5)*(1000./(1000.*1000.)))+((kmnai13*p->Cae)*((p->kmcai*(1.5*(1000./(1000.*1000.))))/(p->kmcai*p->kmcai))));
  double partial_denomterm2_del_Cai = ((Nae3*1.5)*(1000./(1000.*1000.)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Ke_ext = impdata[Ke];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    AslanidiSleiman_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Ke = Ke_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Cai = Cai_init;
    Ke = p->Ke_init;
    V = V_init;
    sv->Y0 = Y0_init;
    sv->Y1 = Y1_init;
    sv->Y11 = Y11_init;
    sv->Y2 = Y2_init;
    sv->Y23 = Y23_init;
    sv->Y24 = Y24_init;
    sv->Y25 = Y25_init;
    sv->Y3 = Y3_init;
    sv->Y32 = Y32_init;
    sv->Y33 = Y33_init;
    sv->Y34 = Y34_init;
    sv->Y35 = Y35_init;
    sv->Y36 = Y36_init;
    sv->Y37 = Y37_init;
    sv->Y38 = Y38_init;
    sv->Y4 = Y4_init;
    sv->Y5 = Y5_init;
    sv->Y6 = Y6_init;
    sv->Y7 = Y7_init;
    sv->b = b_init;
    sv->d = d_init;
    sv->f = f_init;
    sv->g = g_init;
    sv->h = h_init;
    sv->hL = hL_init;
    sv->j = j_init;
    sv->m = m_init;
    sv->mL = mL_init;
    sv->x = x_init;
    sv->xr = xr_init;
    sv->xs = xs_init;
    sv->y1 = y1_init;
    sv->y2 = y2_init;
    double Cai_mM = (sv->Cai/1000.);
    double E_K = (RTonF*(log((Ke/p->Ki))));
    double E_Ks = (RTonF*(log(((Ke+(p->PNaK*p->Nae))/(p->Ki+(p->PNaK*p->Nai))))));
    double ICaL = ((((p->GCaL*sv->d)*sv->f)*(1.-(sv->Y11)))*(V-(E_CaL)));
    double ICaT = (((p->GCaT*sv->b)*sv->g)*(V-(E_CaT)));
    double IClB = (p->GClB*(V-(E_Cl)));
    double IKPKp = (1.0/(1.0+(exp(((7.488-(V))/5.98)))));
    double INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(E_Na)));
    double INaB = (p->GNaB*(V-(E_Na)));
    double INaL = (((p->GNaL*sv->mL)*sv->hL)*(V-(E_Na)));
    double a_inf = (1.0/(1.0+(exp(((-(V+5.0))/10.0)))));
    double denommult = (1.+(p->ksat*(exp(((((p->nu-(1.))*V)*F)/(R*p->T))))));
    double exPnuVFRT = (exp((((p->nu*V)*F)/(R*p->T))));
    double exPnum1VFRT = (exp(((((p->nu-(1.))*V)*F)/(R*p->T))));
    double fNaK = (1.0/((1.0+(0.1245*(exp(((-0.1*V)/RTonF)))))+((0.0365*sigma)*(exp(((-1.0*V)/RTonF))))));
    double i_inf = (1.0/(1.0+(exp(((V+75.0)/10.0)))));
    double r_infty = (1.0/(1.0+(exp((V/50.)))));
    double E_Ca = ((RTonF/2.0)*(log((p->Cae/Cai_mM))));
    double ICaP = ((0.5*p->ICaPVmf)/(1.0+(pow((p->ICaPKmf/(Cai_mM*1000.)),p->ICaPHill))));
    double IKB = (p->GKB*(V-(E_K)));
    double IKp = ((0.001*IKPKp)*(V-(E_K)));
    double IKr = (((p->GKr*sv->xr)*r_infty)*(V-(E_K)));
    double IKs = ((p->GKs*sv->xs)*(V-(E_Ks)));
    double INaK = (((0.61875*fNaK)/(1.+(square((10./p->Nai)))))*(Ke/(Ke+1.5)));
    double Ito = (((p->Gto*sv->x)*((sv->y1*0.75)+(sv->y2*0.25)))*(V-(E_K)));
    double aa_k1 = (0.3/(1.0+(exp((0.2385*((V-(E_K))-(59.215)))))));
    double allo = (1./(1.+(square((p->kmcaact/(1.5*Cai_mM))))));
    double bb_k1 = (((0.49124*(exp((0.08032*((V-(E_K))+5.476)))))+(exp((0.06175*((V-(E_K))-(594.31))))))/(1.0+(exp((-0.5143*((V-(E_K))+4.753))))));
    double denomterm1 = (((p->kmcao*Nai3)+((kmnao3*1.5)*Cai_mM))+((kmnai13*p->Cae)*(1.+((1.5*Cai_mM)/p->kmcai))));
    double denomterm2 = ((((p->kmcai*Nae3)*(1.+(Nai3/kmnai13)))+(Nai3*p->Cae))+((Nae3*1.5)*Cai_mM));
    double i_init = i_inf;
    double num = ((0.4*p->INaCamax)*(((Nai3*p->Cae)*exPnuVFRT)-((((Nae3*1.5)*Cai_mM)*exPnum1VFRT))));
    double ICaB = (p->GCaB*(V-(E_Ca)));
    double deltaE = (num/(denommult*(denomterm1+denomterm2)));
    sv->i = i_init;
    double k1_infty = (aa_k1/(aa_k1+bb_k1));
    double ICl = ((((p->GCl*a_inf)*sv->i)/(1.0+(0.1/(Cai_mM*1000.))))*(V-(E_Cl)));
    double IK1 = ((p->GK1*(k1_infty+0.008))*(V-(E_K)));
    double INCX = (allo*deltaE);
    Iion = ((((((((((((((((INa+INaL)+IKr)+IKs)+IK1)+IKp)+Ito)+ICaL)+ICaT)+ICl)+IClB)+ICaB)+INaB)+IKB)+INaK)+ICaP)+INCX);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Ke_ext[__i] = Ke;
    V_ext[__i] = V;

  }


  int num_thread = 1;
#ifdef _OPENMP
  num_thread = omp_get_max_threads();
#endif
  AslanidiSleiman_Private* userdata = (AslanidiSleiman_Private*)IMP_malloc(num_thread,sizeof(AslanidiSleiman_Private));
  for( int j=0; j<num_thread; j++ ){
    userdata[j].IF = IF;
    userdata[j].node_number = 0;
  }
  IF->ion_private = userdata;
  IF->private_sz  = sizeof(userdata[0]);


}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_AslanidiSleiman(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  AslanidiSleiman_Params *p  = (AslanidiSleiman_Params *)IF->params;
  AslanidiSleiman_state *sv_base = (AslanidiSleiman_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Nae3 = ((p->Nae*p->Nae)*p->Nae);
  GlobalData_t Nai3 = ((p->Nai*p->Nai)*p->Nai);
  GlobalData_t RTonF = ((R*p->T)/F);
  GlobalData_t kmnai13 = ((p->kmnai1*p->kmnai1)*p->kmnai1);
  GlobalData_t kmnao3 = ((p->kmnao*p->kmnao)*p->kmnao);
  GlobalData_t partial_diff_Cai_del_Y1 = ((p->Ca_handling==1.) ? (((Vol_cytosol*3.7243e-12)/(Vol_cytosol*Vol_cytosol))*1000.) : 0.);
  GlobalData_t sigma = (((exp((p->Nae/67.3)))-(1.0))/7.0);
  GlobalData_t E_Cl = (RTonF*(log((p->Cli/p->Cle))));
  GlobalData_t E_Na = (RTonF*(log((p->Nae/p->Nai))));
  GlobalData_t partial_denomterm1_del_Cai = (((kmnao3*1.5)*(1000./(1000.*1000.)))+((kmnai13*p->Cae)*((p->kmcai*(1.5*(1000./(1000.*1000.))))/(p->kmcai*p->kmcai))));
  GlobalData_t partial_denomterm2_del_Cai = ((Nae3*1.5)*(1000./(1000.*1000.)));
  
  //Initializing the userdata structures.
  AslanidiSleiman_Private* ion_private = (AslanidiSleiman_Private*) IF->ion_private;
  int nthread = 1;
  #ifdef _OPENMP
  nthread = omp_get_max_threads();
  #endif
  for( int j=0; j<nthread; j++ ) {
    ion_private[j].rc.E_Cl = E_Cl;
    ion_private[j].rc.E_Na = E_Na;
    ion_private[j].rc.Nae3 = Nae3;
    ion_private[j].rc.Nai3 = Nai3;
    ion_private[j].rc.RTonF = RTonF;
    ion_private[j].rc.kmnai13 = kmnai13;
    ion_private[j].rc.kmnao3 = kmnao3;
    ion_private[j].rc.partial_denomterm1_del_Cai = partial_denomterm1_del_Cai;
    ion_private[j].rc.partial_denomterm2_del_Cai = partial_denomterm2_del_Cai;
    ion_private[j].rc.partial_diff_Cai_del_Y1 = partial_diff_Cai_del_Y1;
    ion_private[j].rc.sigma = sigma;
  }
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Ke_ext = impdata[Ke];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    AslanidiSleiman_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Ke = Ke_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t Cai_mM = (sv->Cai/1000.);
    LUT_data_t Cai_mM_row[NROWS_Cai_mM];
    LUT_interpRow(&IF->tables[Cai_mM_TAB], Cai_mM, __i, Cai_mM_row);
    GlobalData_t E_K = (RTonF*(log((Ke/p->Ki))));
    GlobalData_t E_Ks = (RTonF*(log(((Ke+(p->PNaK*p->Nae))/(p->Ki+(p->PNaK*p->Nai))))));
    GlobalData_t ICaL = ((((p->GCaL*sv->d)*sv->f)*(1.-(sv->Y11)))*(V-(E_CaL)));
    GlobalData_t ICaT = (((p->GCaT*sv->b)*sv->g)*(V-(E_CaT)));
    GlobalData_t INa = ((((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*sv->j)*(V-(E_Na)));
    GlobalData_t INaL = (((p->GNaL*sv->mL)*sv->hL)*(V-(E_Na)));
    GlobalData_t ICl = ((((p->GCl*V_row[a_inf_idx])*sv->i)/(1.0+(0.1/(Cai_mM*1000.))))*(V-(E_Cl)));
    GlobalData_t IKB = (p->GKB*(V-(E_K)));
    GlobalData_t IKp = ((0.001*V_row[IKPKp_idx])*(V-(E_K)));
    GlobalData_t IKr = (((p->GKr*sv->xr)*V_row[r_infty_idx])*(V-(E_K)));
    GlobalData_t IKs = ((p->GKs*sv->xs)*(V-(E_Ks)));
    GlobalData_t INaK = (((0.61875*V_row[fNaK_idx])/(1.+(square((10./p->Nai)))))*(Ke/(Ke+1.5)));
    GlobalData_t Ito = (((p->Gto*sv->x)*((sv->y1*0.75)+(sv->y2*0.25)))*(V-(E_K)));
    GlobalData_t aa_k1 = (0.3/(1.0+(exp((0.2385*((V-(E_K))-(59.215)))))));
    GlobalData_t bb_k1 = (((0.49124*(exp((0.08032*((V-(E_K))+5.476)))))+(exp((0.06175*((V-(E_K))-(594.31))))))/(1.0+(exp((-0.5143*((V-(E_K))+4.753))))));
    GlobalData_t num = ((0.4*p->INaCamax)*(((Nai3*p->Cae)*V_row[exPnuVFRT_idx])-((((Nae3*1.5)*Cai_mM)*V_row[exPnum1VFRT_idx]))));
    GlobalData_t ICaB = (p->GCaB*(V-(Cai_mM_row[E_Ca_idx])));
    GlobalData_t deltaE = (num/(V_row[denommult_idx]*(Cai_mM_row[denomterm1_idx]+Cai_mM_row[denomterm2_idx])));
    GlobalData_t k1_infty = (aa_k1/(aa_k1+bb_k1));
    GlobalData_t IK1 = ((p->GK1*(k1_infty+0.008))*(V-(E_K)));
    GlobalData_t INCX = (Cai_mM_row[allo_idx]*deltaE);
    Iion = ((((((((((((((((INa+INaL)+IKr)+IKs)+IK1)+IKp)+Ito)+ICaL)+ICaT)+ICl)+V_row[IClB_idx])+ICaB)+V_row[INaB_idx])+IKB)+INaK)+Cai_mM_row[ICaP_idx])+INCX);
    
    
    //Complete Forward Euler Update
    GlobalData_t Ca_buffer_dCa_SLB_SL = (((Ca_buffer_kon_SL*sv->Y1)*(((Ca_buffer_Bmax_SLB_SL*Vol_cytosol)/Vol_SL)-(sv->Y2)))-((Ca_buffer_koff_SLB*sv->Y2)));
    GlobalData_t Ca_buffer_dCa_SLB_jct = (((Ca_buffer_kon_SL*sv->Y7)*((((Ca_buffer_Bmax_SLB_jct*0.1)*Vol_cytosol)/Vol_jct)-(sv->Y3)))-((Ca_buffer_koff_SLB*sv->Y3)));
    GlobalData_t Ca_buffer_dCa_SLHigh_SL = (((Ca_buffer_kon_SL*sv->Y1)*(((Ca_buffer_Bmax_SLHigh_SL*Vol_cytosol)/Vol_SL)-(sv->Y4)))-((Ca_buffer_koff_SLHigh*sv->Y4)));
    GlobalData_t Ca_buffer_dCa_SLHigh_jct = (((Ca_buffer_kon_SL*sv->Y7)*((((Ca_buffer_Bmax_SLHigh_jct*0.1)*Vol_cytosol)/Vol_jct)-(sv->Y5)))-((Ca_buffer_koff_SLHigh*sv->Y5)));
    GlobalData_t cytosolic_Ca_buffer_dCa_Calmodulin = (((cytosolic_Ca_buffer_kon_Calmodulin*Cai_mM)*(cytosolic_Ca_buffer_Bmax_Calmodulin-(sv->Y32)))-((cytosolic_Ca_buffer_koff_Calmodulin*sv->Y32)));
    GlobalData_t cytosolic_Ca_buffer_dCa_Myosin = (((cytosolic_Ca_buffer_kon_Myosin_Ca*Cai_mM)*(cytosolic_Ca_buffer_Bmax_Myosin_Ca-((sv->Y33+sv->Y37))))-((cytosolic_Ca_buffer_koff_Myosin_Ca*sv->Y33)));
    GlobalData_t cytosolic_Ca_buffer_dCa_SRB = (((cytosolic_Ca_buffer_kon_SRB*Cai_mM)*(cytosolic_Ca_buffer_Bmax_SRB-(sv->Y34)))-((cytosolic_Ca_buffer_koff_SRB*sv->Y34)));
    GlobalData_t cytosolic_Ca_buffer_dCa_TroponinC = (((cytosolic_Ca_buffer_kon_TroponinC*Cai_mM)*(cytosolic_Ca_buffer_Bmax_TroponinC-(sv->Y35)))-((cytosolic_Ca_buffer_koff_TroponinC*sv->Y35)));
    GlobalData_t cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg = (((cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa*Cai_mM)*(cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa-((sv->Y36+sv->Y38))))-((cytosolic_Ca_buffer_koff_TroponinC_Ca_MGCa*sv->Y36)));
    GlobalData_t cytosolic_Ca_buffer_dMgMyosin = (((cytosolic_Ca_buffer_kon_Myosin_Mg*p->Mgi)*(cytosolic_Ca_buffer_Bmax_Myosin_Mg-((sv->Y33+sv->Y37))))-((cytosolic_Ca_buffer_koff_Myosin_Mg*sv->Y37)));
    GlobalData_t cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg = (((cytosolic_Ca_buffer_kon_TroponinC_Ca_MgMg*p->Mgi)*(cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MgMg-((sv->Y36+sv->Y38))))-((cytosolic_Ca_buffer_koff_TroponinC_Ca_MgMg*sv->Y38)));
    GlobalData_t diff_Y11 = ((p->Ca_handling==1.) ? (((0.7*sv->Y7)*(1.0-(sv->Y11)))-((11.9e-3*sv->Y11))) : 0.);
    GlobalData_t diff_Y2 = Ca_buffer_dCa_SLB_SL;
    GlobalData_t diff_Y3 = Ca_buffer_dCa_SLB_jct;
    GlobalData_t diff_Y32 = cytosolic_Ca_buffer_dCa_Calmodulin;
    GlobalData_t diff_Y33 = cytosolic_Ca_buffer_dCa_Myosin;
    GlobalData_t diff_Y34 = cytosolic_Ca_buffer_dCa_SRB;
    GlobalData_t diff_Y35 = cytosolic_Ca_buffer_dCa_TroponinC;
    GlobalData_t diff_Y36 = cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg;
    GlobalData_t diff_Y37 = cytosolic_Ca_buffer_dMgMyosin;
    GlobalData_t diff_Y38 = cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg;
    GlobalData_t diff_Y4 = Ca_buffer_dCa_SLHigh_SL;
    GlobalData_t diff_Y5 = Ca_buffer_dCa_SLHigh_jct;
    GlobalData_t Y11_new = sv->Y11+diff_Y11*dt;
    GlobalData_t Y2_new = sv->Y2+diff_Y2*dt;
    GlobalData_t Y3_new = sv->Y3+diff_Y3*dt;
    GlobalData_t Y32_new = sv->Y32+diff_Y32*dt;
    GlobalData_t Y33_new = sv->Y33+diff_Y33*dt;
    GlobalData_t Y34_new = sv->Y34+diff_Y34*dt;
    GlobalData_t Y35_new = sv->Y35+diff_Y35*dt;
    GlobalData_t Y36_new = sv->Y36+diff_Y36*dt;
    GlobalData_t Y37_new = sv->Y37+diff_Y37*dt;
    GlobalData_t Y38_new = sv->Y38+diff_Y38*dt;
    GlobalData_t Y4_new = sv->Y4+diff_Y4*dt;
    GlobalData_t Y5_new = sv->Y5+diff_Y5*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t d_rush_larsen_B = V_row[d_rush_larsen_B_idx];
    GlobalData_t f_rush_larsen_B = V_row[f_rush_larsen_B_idx];
    GlobalData_t hL_rush_larsen_B = V_row[hL_rush_larsen_B_idx];
    GlobalData_t h_rush_larsen_A = V_row[h_rush_larsen_A_idx];
    GlobalData_t h_rush_larsen_B = V_row[h_rush_larsen_B_idx];
    GlobalData_t i_rush_larsen_B = V_row[i_rush_larsen_B_idx];
    GlobalData_t j_rush_larsen_A = V_row[j_rush_larsen_A_idx];
    GlobalData_t j_rush_larsen_B = V_row[j_rush_larsen_B_idx];
    GlobalData_t mL_rush_larsen_A = V_row[mL_rush_larsen_A_idx];
    GlobalData_t mL_rush_larsen_B = V_row[mL_rush_larsen_B_idx];
    GlobalData_t m_rush_larsen_A = V_row[m_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_B = V_row[m_rush_larsen_B_idx];
    GlobalData_t xr_rush_larsen_B = V_row[xr_rush_larsen_B_idx];
    GlobalData_t xs_rush_larsen_B = V_row[xs_rush_larsen_B_idx];
    GlobalData_t b_rush_larsen_B = V_row[b_rush_larsen_B_idx];
    GlobalData_t d_rush_larsen_A = V_row[d_rush_larsen_A_idx];
    GlobalData_t f_rush_larsen_A = V_row[f_rush_larsen_A_idx];
    GlobalData_t g_rush_larsen_B = V_row[g_rush_larsen_B_idx];
    GlobalData_t hL_rush_larsen_A = V_row[hL_rush_larsen_A_idx];
    GlobalData_t i_rush_larsen_A = V_row[i_rush_larsen_A_idx];
    GlobalData_t x_rush_larsen_B = V_row[x_rush_larsen_B_idx];
    GlobalData_t xr_rush_larsen_A = V_row[xr_rush_larsen_A_idx];
    GlobalData_t xs_rush_larsen_A = V_row[xs_rush_larsen_A_idx];
    GlobalData_t y1_rush_larsen_B = V_row[y1_rush_larsen_B_idx];
    GlobalData_t y2_rush_larsen_B = V_row[y2_rush_larsen_B_idx];
    GlobalData_t b_rush_larsen_A = V_row[b_rush_larsen_A_idx];
    GlobalData_t g_rush_larsen_A = V_row[g_rush_larsen_A_idx];
    GlobalData_t x_rush_larsen_A = V_row[x_rush_larsen_A_idx];
    GlobalData_t y1_rush_larsen_A = V_row[y1_rush_larsen_A_idx];
    GlobalData_t y2_rush_larsen_A = V_row[y2_rush_larsen_A_idx];
    GlobalData_t b_new = b_rush_larsen_A+b_rush_larsen_B*sv->b;
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f_new = f_rush_larsen_A+f_rush_larsen_B*sv->f;
    GlobalData_t g_new = g_rush_larsen_A+g_rush_larsen_B*sv->g;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t hL_new = hL_rush_larsen_A+hL_rush_larsen_B*sv->hL;
    GlobalData_t i_new = i_rush_larsen_A+i_rush_larsen_B*sv->i;
    GlobalData_t j_new = j_rush_larsen_A+j_rush_larsen_B*sv->j;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t mL_new = mL_rush_larsen_A+mL_rush_larsen_B*sv->mL;
    GlobalData_t x_new = x_rush_larsen_A+x_rush_larsen_B*sv->x;
    GlobalData_t xr_new = xr_rush_larsen_A+xr_rush_larsen_B*sv->xr;
    GlobalData_t xs_new = xs_rush_larsen_A+xs_rush_larsen_B*sv->xs;
    GlobalData_t y1_new = y1_rush_larsen_A+y1_rush_larsen_B*sv->y1;
    GlobalData_t y2_new = y2_rush_larsen_A+y2_rush_larsen_B*sv->y2;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    int thread=0;
    #ifdef _OPENMP
    thread = omp_get_thread_num();
    #endif
    float Rosenbrock_X[N_ROSEN];
    Rosenbrock_X[ROSEN_Cai] = sv->Cai;
    Rosenbrock_X[ROSEN_Y0] = sv->Y0;
    Rosenbrock_X[ROSEN_Y1] = sv->Y1;
    Rosenbrock_X[ROSEN_Y23] = sv->Y23;
    Rosenbrock_X[ROSEN_Y24] = sv->Y24;
    Rosenbrock_X[ROSEN_Y25] = sv->Y25;
    Rosenbrock_X[ROSEN_Y6] = sv->Y6;
    Rosenbrock_X[ROSEN_Y7] = sv->Y7;
    ion_private[thread].nr.Ke = Ke;
    ion_private[thread].nr.V = V;
    ion_private[thread].node_number = __i;
    
    rbStepX(Rosenbrock_X, AslanidiSleiman_rosenbrock_f, AslanidiSleiman_rosenbrock_jacobian, (void*)(ion_private+thread), dt, N_ROSEN);
    GlobalData_t Cai_new = Rosenbrock_X[ROSEN_Cai];
    GlobalData_t Y0_new = Rosenbrock_X[ROSEN_Y0];
    GlobalData_t Y1_new = Rosenbrock_X[ROSEN_Y1];
    GlobalData_t Y23_new = Rosenbrock_X[ROSEN_Y23];
    GlobalData_t Y24_new = Rosenbrock_X[ROSEN_Y24];
    GlobalData_t Y25_new = Rosenbrock_X[ROSEN_Y25];
    GlobalData_t Y6_new = Rosenbrock_X[ROSEN_Y6];
    GlobalData_t Y7_new = Rosenbrock_X[ROSEN_Y7];
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Cai = Cai_new;
    Iion = Iion;
    sv->Y0 = Y0_new;
    sv->Y1 = Y1_new;
    sv->Y11 = Y11_new;
    sv->Y2 = Y2_new;
    sv->Y23 = Y23_new;
    sv->Y24 = Y24_new;
    sv->Y25 = Y25_new;
    sv->Y3 = Y3_new;
    sv->Y32 = Y32_new;
    sv->Y33 = Y33_new;
    sv->Y34 = Y34_new;
    sv->Y35 = Y35_new;
    sv->Y36 = Y36_new;
    sv->Y37 = Y37_new;
    sv->Y38 = Y38_new;
    sv->Y4 = Y4_new;
    sv->Y5 = Y5_new;
    sv->Y6 = Y6_new;
    sv->Y7 = Y7_new;
    sv->b = b_new;
    sv->d = d_new;
    sv->f = f_new;
    sv->g = g_new;
    sv->h = h_new;
    sv->hL = hL_new;
    sv->i = i_new;
    sv->j = j_new;
    sv->m = m_new;
    sv->mL = mL_new;
    sv->x = x_new;
    sv->xr = xr_new;
    sv->xs = xs_new;
    sv->y1 = y1_new;
    sv->y2 = y2_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Ke_ext[__i] = Ke;
    V_ext[__i] = V;

  }


}

void AslanidiSleiman_rosenbrock_f(float* Rosenbrock_DX, float* Rosenbrock_X, void* user_data) {
  AslanidiSleiman_Private* ion_private = (AslanidiSleiman_Private*)user_data;
  AslanidiSleiman_Regional_Constants* rc = &ion_private->rc;
  ION_IF* IF = ion_private->IF;
  int __i = ((AslanidiSleiman_Private*)user_data)->node_number;
  AslanidiSleiman_Nodal_Req* nr = &ion_private->nr;
  cell_geom *region = &IF->cgeom;
  AslanidiSleiman_Params *p = (AslanidiSleiman_Params *)IF->params;
  AslanidiSleiman_state *sv_base = (AslanidiSleiman_state *)IF->sv_tab.y;
  AslanidiSleiman_state *sv = sv_base+__i;

  LUT_data_t V_row[NROWS_V];
  LUT_interpRow(&IF->tables[V_TAB], nr->V, __i, V_row);
  GlobalData_t Ca_buffer_dCa_SLB_SL = (((Ca_buffer_kon_SL*Rosenbrock_X[ROSEN_Y1])*(((Ca_buffer_Bmax_SLB_SL*Vol_cytosol)/Vol_SL)-(sv->Y2)))-((Ca_buffer_koff_SLB*sv->Y2)));
  GlobalData_t Ca_buffer_dCa_SLB_jct = (((Ca_buffer_kon_SL*Rosenbrock_X[ROSEN_Y7])*((((Ca_buffer_Bmax_SLB_jct*0.1)*Vol_cytosol)/Vol_jct)-(sv->Y3)))-((Ca_buffer_koff_SLB*sv->Y3)));
  GlobalData_t Ca_buffer_dCa_SLHigh_SL = (((Ca_buffer_kon_SL*Rosenbrock_X[ROSEN_Y1])*(((Ca_buffer_Bmax_SLHigh_SL*Vol_cytosol)/Vol_SL)-(sv->Y4)))-((Ca_buffer_koff_SLHigh*sv->Y4)));
  GlobalData_t Ca_buffer_dCa_SLHigh_jct = (((Ca_buffer_kon_SL*Rosenbrock_X[ROSEN_Y7])*((((Ca_buffer_Bmax_SLHigh_jct*0.1)*Vol_cytosol)/Vol_jct)-(sv->Y5)))-((Ca_buffer_koff_SLHigh*sv->Y5)));
  GlobalData_t Ca_buffer_dCalsequestrin = (((Ca_buffer_kon_Calsequestrin*Rosenbrock_X[ROSEN_Y6])*(((Ca_buffer_Bmax_Calsequestrin*Vol_cytosol)/Vol_SR)-(Rosenbrock_X[ROSEN_Y0])))-((Ca_buffer_koff_Calsequestrin*Rosenbrock_X[ROSEN_Y0])));
  GlobalData_t Cai_mM = (Rosenbrock_X[ROSEN_Cai]/1000.);
  LUT_data_t Cai_mM_row[NROWS_Cai_mM];
  LUT_interpRow(&IF->tables[Cai_mM_TAB], Cai_mM, __i, Cai_mM_row);
  GlobalData_t ICaL = ((((p->GCaL*sv->d)*sv->f)*(1.-(sv->Y11)))*(nr->V-(E_CaL)));
  GlobalData_t ICaT = (((p->GCaT*sv->b)*sv->g)*(nr->V-(E_CaT)));
  GlobalData_t JleaKSR_j_leaKSR = ((0.5*JleaKSR_KSRleak)*(Rosenbrock_X[ROSEN_Y6]-(Rosenbrock_X[ROSEN_Y7])));
  GlobalData_t Jrel_SR_RI = (((1.0-(Rosenbrock_X[ROSEN_Y25]))-(Rosenbrock_X[ROSEN_Y24]))-(Rosenbrock_X[ROSEN_Y23]));
  GlobalData_t Jrel_SR_j_rel_SR = (((2.0*Jrel_SR_ks)*Rosenbrock_X[ROSEN_Y24])*(Rosenbrock_X[ROSEN_Y6]-(Rosenbrock_X[ROSEN_Y7])));
  GlobalData_t Jrel_SR_kCaSR = (Jrel_SR_Max_SR-(((Jrel_SR_Max_SR-(Jrel_SR_Min_SR))/(1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR))))));
  GlobalData_t cytosolic_Ca_buffer_dMgMyosin = (((cytosolic_Ca_buffer_kon_Myosin_Mg*p->Mgi)*(cytosolic_Ca_buffer_Bmax_Myosin_Mg-((sv->Y33+sv->Y37))))-((cytosolic_Ca_buffer_koff_Myosin_Mg*sv->Y37)));
  GlobalData_t cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg = (((cytosolic_Ca_buffer_kon_TroponinC_Ca_MgMg*p->Mgi)*(cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MgMg-((sv->Y36+sv->Y38))))-((cytosolic_Ca_buffer_koff_TroponinC_Ca_MgMg*sv->Y38)));
  GlobalData_t ion_diffusion_J_Ca_jct_SL = ((Rosenbrock_X[ROSEN_Y7]-(Rosenbrock_X[ROSEN_Y1]))*8.2413e-13);
  GlobalData_t Ca_buffer_dCa_SL_tot_bound = (Ca_buffer_dCa_SLB_SL+Ca_buffer_dCa_SLHigh_SL);
  GlobalData_t Ca_buffer_dCa_jct_tot_bound = (Ca_buffer_dCa_SLB_jct+Ca_buffer_dCa_SLHigh_jct);
  GlobalData_t ICa = (ICaL+ICaT);
  GlobalData_t JpumPSR_j_pumPSR = (((((2.0*JpumPSR_V_max)*Vol_cytosol)/Vol_SR)*((pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H))-((pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))))/((1.0+(pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H)))+(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H))));
  GlobalData_t Jrel_SR_kiSRCa = (Jrel_SR_kiCa*Jrel_SR_kCaSR);
  GlobalData_t Jrel_SR_koSRCa = (Jrel_SR_koCa/Jrel_SR_kCaSR);
  GlobalData_t cytosolic_Ca_buffer_dCa_Calmodulin = (((cytosolic_Ca_buffer_kon_Calmodulin*Cai_mM)*(cytosolic_Ca_buffer_Bmax_Calmodulin-(sv->Y32)))-((cytosolic_Ca_buffer_koff_Calmodulin*sv->Y32)));
  GlobalData_t cytosolic_Ca_buffer_dCa_Myosin = (((cytosolic_Ca_buffer_kon_Myosin_Ca*Cai_mM)*(cytosolic_Ca_buffer_Bmax_Myosin_Ca-((sv->Y33+sv->Y37))))-((cytosolic_Ca_buffer_koff_Myosin_Ca*sv->Y33)));
  GlobalData_t cytosolic_Ca_buffer_dCa_SRB = (((cytosolic_Ca_buffer_kon_SRB*Cai_mM)*(cytosolic_Ca_buffer_Bmax_SRB-(sv->Y34)))-((cytosolic_Ca_buffer_koff_SRB*sv->Y34)));
  GlobalData_t cytosolic_Ca_buffer_dCa_TroponinC = (((cytosolic_Ca_buffer_kon_TroponinC*Cai_mM)*(cytosolic_Ca_buffer_Bmax_TroponinC-(sv->Y35)))-((cytosolic_Ca_buffer_koff_TroponinC*sv->Y35)));
  GlobalData_t cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg = (((cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa*Cai_mM)*(cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa-((sv->Y36+sv->Y38))))-((cytosolic_Ca_buffer_koff_TroponinC_Ca_MGCa*sv->Y36)));
  GlobalData_t diff_Y0 = Ca_buffer_dCalsequestrin;
  GlobalData_t ion_diffusion_J_Ca_SL_cytosol = ((Rosenbrock_X[ROSEN_Y1]-(Cai_mM))*3.7243e-12);
  GlobalData_t num = ((0.4*p->INaCamax)*(((rc->Nai3*p->Cae)*V_row[exPnuVFRT_idx])-((((rc->Nae3*1.5)*Cai_mM)*V_row[exPnum1VFRT_idx]))));
  GlobalData_t Ca_buffer_ICa_jct_tot = ICa;
  GlobalData_t ICaB = (p->GCaB*(nr->V-(Cai_mM_row[E_Ca_idx])));
  GlobalData_t cytosolic_Ca_buffer_dCa_cytosol_tot_bound = ((((((cytosolic_Ca_buffer_dCa_TroponinC+cytosolic_Ca_buffer_dCa_TroponinC_Ca_Mg)+cytosolic_Ca_buffer_dMgTroponinC_Ca_Mg)+cytosolic_Ca_buffer_dCa_Calmodulin)+cytosolic_Ca_buffer_dCa_Myosin)+cytosolic_Ca_buffer_dMgMyosin)+cytosolic_Ca_buffer_dCa_SRB);
  GlobalData_t deltaE = (num/(V_row[denommult_idx]*(Cai_mM_row[denomterm1_idx]+Cai_mM_row[denomterm2_idx])));
  GlobalData_t diff_Y23 = ((((Jrel_SR_kiSRCa*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y24])-((Jrel_SR_kim*Rosenbrock_X[ROSEN_Y23])))-(((Jrel_SR_kom*Rosenbrock_X[ROSEN_Y23])-((((Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y7])*Jrel_SR_RI)))));
  GlobalData_t diff_Y24 = (((((Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y25])-((Jrel_SR_kom*Rosenbrock_X[ROSEN_Y24])))-((((Jrel_SR_kiSRCa*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y24])-((Jrel_SR_kim*Rosenbrock_X[ROSEN_Y23])))));
  GlobalData_t diff_Y25 = (((Jrel_SR_kim*Jrel_SR_RI)-(((Jrel_SR_kiSRCa*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y25])))-(((((Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y25])-((Jrel_SR_kom*Rosenbrock_X[ROSEN_Y24])))));
  GlobalData_t diff_Y6 = ((JpumPSR_j_pumPSR-((((JleaKSR_j_leaKSR*Vol_cytosol)/Vol_SR)+Jrel_SR_j_rel_SR)))-(Ca_buffer_dCalsequestrin));
  GlobalData_t INCX = (Cai_mM_row[allo_idx]*deltaE);
  GlobalData_t diff_Cai = ((p->Ca_handling==1.) ? ((((((-JpumPSR_j_pumPSR)*Vol_SR)/Vol_cytosol)+(ion_diffusion_J_Ca_SL_cytosol/Vol_cytosol))-(cytosolic_Ca_buffer_dCa_cytosol_tot_bound))*1000.) : 0.);
  GlobalData_t diff_Y7 = (((((((-0.5*Ca_buffer_ICa_jct_tot)*Cm)/((Vol_jct*2.0)*F))-((ion_diffusion_J_Ca_jct_SL/Vol_jct)))+((Jrel_SR_j_rel_SR*Vol_SR)/Vol_jct))+((JleaKSR_j_leaKSR*Vol_cytosol)/Vol_jct))-(Ca_buffer_dCa_jct_tot_bound));
  GlobalData_t Ca_buffer_ICaSL_tot = (((-2.0*INCX)+ICaB)+Cai_mM_row[ICaP_idx]);
  GlobalData_t diff_Y1 = (((((-0.5*Ca_buffer_ICaSL_tot)*Cm)/((Vol_SL*2.0)*F))+((ion_diffusion_J_Ca_jct_SL-(ion_diffusion_J_Ca_SL_cytosol))/Vol_SL))-(Ca_buffer_dCa_SL_tot_bound));
  Rosenbrock_DX[ROSEN_Cai] = diff_Cai;
  Rosenbrock_DX[ROSEN_Y0] = diff_Y0;
  Rosenbrock_DX[ROSEN_Y1] = diff_Y1;
  Rosenbrock_DX[ROSEN_Y23] = diff_Y23;
  Rosenbrock_DX[ROSEN_Y24] = diff_Y24;
  Rosenbrock_DX[ROSEN_Y25] = diff_Y25;
  Rosenbrock_DX[ROSEN_Y6] = diff_Y6;
  Rosenbrock_DX[ROSEN_Y7] = diff_Y7;

}

void AslanidiSleiman_rosenbrock_jacobian(float** Rosenbrock_jacobian, float* Rosenbrock_X, void* user_data, int dddd) {
  AslanidiSleiman_Private* ion_private = (AslanidiSleiman_Private*)user_data;
  AslanidiSleiman_Regional_Constants* rc = &ion_private->rc;
  ION_IF* IF = ion_private->IF;
  int __i = ((AslanidiSleiman_Private*)user_data)->node_number;
  AslanidiSleiman_Nodal_Req* nr = &ion_private->nr;
  cell_geom *region = &IF->cgeom;
  AslanidiSleiman_Params *p = (AslanidiSleiman_Params *)IF->params;
  AslanidiSleiman_state *sv_base = (AslanidiSleiman_state *)IF->sv_tab.y;
  AslanidiSleiman_state *sv = sv_base+__i;
  LUT_data_t V_row[NROWS_V];
  LUT_interpRow(&IF->tables[V_TAB], nr->V, __i, V_row);
  GlobalData_t Cai_mM = (Rosenbrock_X[ROSEN_Cai]/1000.);
  LUT_data_t Cai_mM_row[NROWS_Cai_mM];
  LUT_interpRow(&IF->tables[Cai_mM_TAB], Cai_mM, __i, Cai_mM_row);
  GlobalData_t Jrel_SR_RI = (((1.0-(Rosenbrock_X[ROSEN_Y25]))-(Rosenbrock_X[ROSEN_Y24]))-(Rosenbrock_X[ROSEN_Y23]));
  GlobalData_t Jrel_SR_kCaSR = (Jrel_SR_Max_SR-(((Jrel_SR_Max_SR-(Jrel_SR_Min_SR))/(1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR))))));
  GlobalData_t partial_diff_Y0_del_Y0 = (((Ca_buffer_kon_Calsequestrin*Rosenbrock_X[ROSEN_Y6])*-1.)-(Ca_buffer_koff_Calsequestrin));
  GlobalData_t partial_diff_Y0_del_Y6 = (Ca_buffer_kon_Calsequestrin*(((Ca_buffer_Bmax_Calsequestrin*Vol_cytosol)/Vol_SR)-(Rosenbrock_X[ROSEN_Y0])));
  GlobalData_t partial_diff_Y1_del_Y1 = (((Vol_SL*((-1.*8.2413e-13)-(3.7243e-12)))/(Vol_SL*Vol_SL))-(((Ca_buffer_kon_SL*(((Ca_buffer_Bmax_SLB_SL*Vol_cytosol)/Vol_SL)-(sv->Y2)))+(Ca_buffer_kon_SL*(((Ca_buffer_Bmax_SLHigh_SL*Vol_cytosol)/Vol_SL)-(sv->Y4))))));
  GlobalData_t partial_diff_Y6_del_Y0 = (-(((Ca_buffer_kon_Calsequestrin*Rosenbrock_X[ROSEN_Y6])*-1.)-(Ca_buffer_koff_Calsequestrin)));
  GlobalData_t partial_diff_Y6_del_Y24 = (-((2.0*Jrel_SR_ks)*(Rosenbrock_X[ROSEN_Y6]-(Rosenbrock_X[ROSEN_Y7]))));
  GlobalData_t partial_diff_Y6_del_Y7 = (-(((Vol_SR*(((0.5*JleaKSR_KSRleak)*-1.)*Vol_cytosol))/(Vol_SR*Vol_SR))+(((2.0*Jrel_SR_ks)*Rosenbrock_X[ROSEN_Y24])*-1.)));
  GlobalData_t partial_diff_Y7_del_Y24 = ((Vol_jct*(((2.0*Jrel_SR_ks)*(Rosenbrock_X[ROSEN_Y6]-(Rosenbrock_X[ROSEN_Y7])))*Vol_SR))/(Vol_jct*Vol_jct));
  GlobalData_t partial_diff_Y7_del_Y6 = (((Vol_jct*(((2.0*Jrel_SR_ks)*Rosenbrock_X[ROSEN_Y24])*Vol_SR))/(Vol_jct*Vol_jct))+((Vol_jct*((0.5*JleaKSR_KSRleak)*Vol_cytosol))/(Vol_jct*Vol_jct)));
  GlobalData_t partial_diff_Y7_del_Y7 = ((((-((Vol_jct*8.2413e-13)/(Vol_jct*Vol_jct)))+((Vol_jct*((((2.0*Jrel_SR_ks)*Rosenbrock_X[ROSEN_Y24])*-1.)*Vol_SR))/(Vol_jct*Vol_jct)))+((Vol_jct*(((0.5*JleaKSR_KSRleak)*-1.)*Vol_cytosol))/(Vol_jct*Vol_jct)))-(((Ca_buffer_kon_SL*((((Ca_buffer_Bmax_SLB_jct*0.1)*Vol_cytosol)/Vol_jct)-(sv->Y3)))+(Ca_buffer_kon_SL*((((Ca_buffer_Bmax_SLHigh_jct*0.1)*Vol_cytosol)/Vol_jct)-(sv->Y5))))));
  GlobalData_t Jrel_SR_kiSRCa = (Jrel_SR_kiCa*Jrel_SR_kCaSR);
  GlobalData_t Jrel_SR_koSRCa = (Jrel_SR_koCa/Jrel_SR_kCaSR);
  GlobalData_t num = ((0.4*p->INaCamax)*(((rc->Nai3*p->Cae)*V_row[exPnuVFRT_idx])-((((rc->Nae3*1.5)*Cai_mM)*V_row[exPnum1VFRT_idx]))));
  GlobalData_t partial_diff_Cai_del_Cai = ((p->Ca_handling==1.) ? (((((Vol_cytosol*((-(((((1.0+(pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H)))+(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))*((((2.0*JpumPSR_V_max)*Vol_cytosol)/Vol_SR)*(JpumPSR_H*(((JpumPSR_Kmf*(1000./(1000.*1000.)))/(JpumPSR_Kmf*JpumPSR_Kmf))*(pow((Cai_mM/JpumPSR_Kmf),(JpumPSR_H-(1.))))))))-((((((2.0*JpumPSR_V_max)*Vol_cytosol)/Vol_SR)*((pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H))-((pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))))*(JpumPSR_H*(((JpumPSR_Kmf*(1000./(1000.*1000.)))/(JpumPSR_Kmf*JpumPSR_Kmf))*(pow((Cai_mM/JpumPSR_Kmf),(JpumPSR_H-(1.)))))))))/(((1.0+(pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H)))+(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))*((1.0+(pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H)))+(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H))))))*Vol_SR))/(Vol_cytosol*Vol_cytosol))+((Vol_cytosol*((-(1000./(1000.*1000.)))*3.7243e-12))/(Vol_cytosol*Vol_cytosol)))-(((((((cytosolic_Ca_buffer_kon_TroponinC*(1000./(1000.*1000.)))*(cytosolic_Ca_buffer_Bmax_TroponinC-(sv->Y35)))+((cytosolic_Ca_buffer_kon_TroponinC_Ca_MGCa*(1000./(1000.*1000.)))*(cytosolic_Ca_buffer_Bmax_TroponinC_Ca_MGCa-((sv->Y36+sv->Y38)))))+((cytosolic_Ca_buffer_kon_Calmodulin*(1000./(1000.*1000.)))*(cytosolic_Ca_buffer_Bmax_Calmodulin-(sv->Y32))))+((cytosolic_Ca_buffer_kon_Myosin_Ca*(1000./(1000.*1000.)))*(cytosolic_Ca_buffer_Bmax_Myosin_Ca-((sv->Y33+sv->Y37)))))+((cytosolic_Ca_buffer_kon_SRB*(1000./(1000.*1000.)))*(cytosolic_Ca_buffer_Bmax_SRB-(sv->Y34))))))*1000.) : 0.);
  GlobalData_t partial_diff_Cai_del_Y6 = ((p->Ca_handling==1.) ? (((Vol_cytosol*((-(((((1.0+(pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H)))+(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))*((((2.0*JpumPSR_V_max)*Vol_cytosol)/Vol_SR)*(-(JpumPSR_H*((JpumPSR_Kmr/(JpumPSR_Kmr*JpumPSR_Kmr))*(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),(JpumPSR_H-(1.)))))))))-((((((2.0*JpumPSR_V_max)*Vol_cytosol)/Vol_SR)*((pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H))-((pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))))*(JpumPSR_H*((JpumPSR_Kmr/(JpumPSR_Kmr*JpumPSR_Kmr))*(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),(JpumPSR_H-(1.)))))))))/(((1.0+(pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H)))+(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))*((1.0+(pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H)))+(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H))))))*Vol_SR))/(Vol_cytosol*Vol_cytosol))*1000.) : 0.);
  GlobalData_t partial_diff_Y23_del_Y6 = ((((Jrel_SR_kiCa*(-((-((Jrel_SR_Max_SR-(Jrel_SR_Min_SR))*(Jrel_SR_HSR*(((-Jrel_SR_EC50_SR)/(Rosenbrock_X[ROSEN_Y6]*Rosenbrock_X[ROSEN_Y6]))*(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),(Jrel_SR_HSR-(1.))))))))/((1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR)))*(1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR)))))))*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y24])-((-(((((-(Jrel_SR_koCa*(-((-((Jrel_SR_Max_SR-(Jrel_SR_Min_SR))*(Jrel_SR_HSR*(((-Jrel_SR_EC50_SR)/(Rosenbrock_X[ROSEN_Y6]*Rosenbrock_X[ROSEN_Y6]))*(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),(Jrel_SR_HSR-(1.))))))))/((1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR)))*(1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR))))))))/(Jrel_SR_kCaSR*Jrel_SR_kCaSR))*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y7])*Jrel_SR_RI))));
  GlobalData_t partial_diff_Y24_del_Y6 = ((((((-(Jrel_SR_koCa*(-((-((Jrel_SR_Max_SR-(Jrel_SR_Min_SR))*(Jrel_SR_HSR*(((-Jrel_SR_EC50_SR)/(Rosenbrock_X[ROSEN_Y6]*Rosenbrock_X[ROSEN_Y6]))*(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),(Jrel_SR_HSR-(1.))))))))/((1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR)))*(1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR))))))))/(Jrel_SR_kCaSR*Jrel_SR_kCaSR))*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y25])-((((Jrel_SR_kiCa*(-((-((Jrel_SR_Max_SR-(Jrel_SR_Min_SR))*(Jrel_SR_HSR*(((-Jrel_SR_EC50_SR)/(Rosenbrock_X[ROSEN_Y6]*Rosenbrock_X[ROSEN_Y6]))*(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),(Jrel_SR_HSR-(1.))))))))/((1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR)))*(1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR)))))))*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y24])));
  GlobalData_t partial_diff_Y25_del_Y6 = ((-(((Jrel_SR_kiCa*(-((-((Jrel_SR_Max_SR-(Jrel_SR_Min_SR))*(Jrel_SR_HSR*(((-Jrel_SR_EC50_SR)/(Rosenbrock_X[ROSEN_Y6]*Rosenbrock_X[ROSEN_Y6]))*(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),(Jrel_SR_HSR-(1.))))))))/((1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR)))*(1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR)))))))*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y25]))-((((((-(Jrel_SR_koCa*(-((-((Jrel_SR_Max_SR-(Jrel_SR_Min_SR))*(Jrel_SR_HSR*(((-Jrel_SR_EC50_SR)/(Rosenbrock_X[ROSEN_Y6]*Rosenbrock_X[ROSEN_Y6]))*(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),(Jrel_SR_HSR-(1.))))))))/((1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR)))*(1.0+(pow((Jrel_SR_EC50_SR/Rosenbrock_X[ROSEN_Y6]),Jrel_SR_HSR))))))))/(Jrel_SR_kCaSR*Jrel_SR_kCaSR))*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y25])));
  GlobalData_t partial_diff_Y6_del_Cai = (((((1.0+(pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H)))+(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))*((((2.0*JpumPSR_V_max)*Vol_cytosol)/Vol_SR)*(JpumPSR_H*(((JpumPSR_Kmf*(1000./(1000.*1000.)))/(JpumPSR_Kmf*JpumPSR_Kmf))*(pow((Cai_mM/JpumPSR_Kmf),(JpumPSR_H-(1.))))))))-((((((2.0*JpumPSR_V_max)*Vol_cytosol)/Vol_SR)*((pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H))-((pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))))*(JpumPSR_H*(((JpumPSR_Kmf*(1000./(1000.*1000.)))/(JpumPSR_Kmf*JpumPSR_Kmf))*(pow((Cai_mM/JpumPSR_Kmf),(JpumPSR_H-(1.)))))))))/(((1.0+(pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H)))+(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))*((1.0+(pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H)))+(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))));
  GlobalData_t partial_diff_Y6_del_Y6 = (((((((1.0+(pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H)))+(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))*((((2.0*JpumPSR_V_max)*Vol_cytosol)/Vol_SR)*(-(JpumPSR_H*((JpumPSR_Kmr/(JpumPSR_Kmr*JpumPSR_Kmr))*(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),(JpumPSR_H-(1.)))))))))-((((((2.0*JpumPSR_V_max)*Vol_cytosol)/Vol_SR)*((pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H))-((pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))))*(JpumPSR_H*((JpumPSR_Kmr/(JpumPSR_Kmr*JpumPSR_Kmr))*(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),(JpumPSR_H-(1.)))))))))/(((1.0+(pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H)))+(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))*((1.0+(pow((Cai_mM/JpumPSR_Kmf),JpumPSR_H)))+(pow((Rosenbrock_X[ROSEN_Y6]/JpumPSR_Kmr),JpumPSR_H)))))-((((Vol_SR*((0.5*JleaKSR_KSRleak)*Vol_cytosol))/(Vol_SR*Vol_SR))+((2.0*Jrel_SR_ks)*Rosenbrock_X[ROSEN_Y24]))))-((Ca_buffer_kon_Calsequestrin*(((Ca_buffer_Bmax_Calsequestrin*Vol_cytosol)/Vol_SR)-(Rosenbrock_X[ROSEN_Y0])))));
  GlobalData_t deltaE = (num/(V_row[denommult_idx]*(Cai_mM_row[denomterm1_idx]+Cai_mM_row[denomterm2_idx])));
  GlobalData_t partial_diff_Y23_del_Y23 = ((-Jrel_SR_kim)-((Jrel_SR_kom-((((Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y7])*-1.)))));
  GlobalData_t partial_diff_Y23_del_Y24 = ((Jrel_SR_kiSRCa*Rosenbrock_X[ROSEN_Y7])-((-(((Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y7])*-1.))));
  GlobalData_t partial_diff_Y23_del_Y25 = (-(-(((Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y7])*-1.)));
  GlobalData_t partial_diff_Y23_del_Y7 = ((Jrel_SR_kiSRCa*Rosenbrock_X[ROSEN_Y24])-((-(((Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7])+(Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7]))*Jrel_SR_RI))));
  GlobalData_t partial_diff_Y24_del_Y24 = ((-Jrel_SR_kom)-((Jrel_SR_kiSRCa*Rosenbrock_X[ROSEN_Y7])));
  GlobalData_t partial_diff_Y24_del_Y25 = ((Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y7]);
  GlobalData_t partial_diff_Y24_del_Y7 = ((((Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7])+(Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7]))*Rosenbrock_X[ROSEN_Y25])-((Jrel_SR_kiSRCa*Rosenbrock_X[ROSEN_Y24])));
  GlobalData_t partial_diff_Y25_del_Y25 = (((Jrel_SR_kim*-1.)-((Jrel_SR_kiSRCa*Rosenbrock_X[ROSEN_Y7])))-(((Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7])*Rosenbrock_X[ROSEN_Y7])));
  GlobalData_t partial_diff_Y25_del_Y7 = ((-(Jrel_SR_kiSRCa*Rosenbrock_X[ROSEN_Y25]))-((((Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7])+(Jrel_SR_koSRCa*Rosenbrock_X[ROSEN_Y7]))*Rosenbrock_X[ROSEN_Y25])));
  GlobalData_t partial_diff_Y1_del_Cai = (((((Vol_SL*2.0)*F)*((-0.5*(((-2.0*((((-((2.*(p->kmcaact/(1.5*Cai_mM)))*((-(p->kmcaact*(1.5*(1000./(1000.*1000.)))))/((1.5*Cai_mM)*(1.5*Cai_mM)))))/((1.+(square((p->kmcaact/(1.5*Cai_mM)))))*(1.+(square((p->kmcaact/(1.5*Cai_mM)))))))*deltaE)+(Cai_mM_row[allo_idx]*((((V_row[denommult_idx]*(Cai_mM_row[denomterm1_idx]+Cai_mM_row[denomterm2_idx]))*((0.4*p->INaCamax)*(-(((rc->Nae3*1.5)*(1000./(1000.*1000.)))*V_row[exPnum1VFRT_idx]))))-((num*(V_row[denommult_idx]*((((rc->kmnao3*1.5)*(1000./(1000.*1000.)))+((rc->kmnai13*p->Cae)*((p->kmcai*(1.5*(1000./(1000.*1000.))))/(p->kmcai*p->kmcai))))+((rc->Nae3*1.5)*(1000./(1000.*1000.))))))))/((V_row[denommult_idx]*(Cai_mM_row[denomterm1_idx]+Cai_mM_row[denomterm2_idx]))*(V_row[denommult_idx]*(Cai_mM_row[denomterm1_idx]+Cai_mM_row[denomterm2_idx])))))))+(p->GCaB*(-((rc->RTonF/2.0)*(((-(p->Cae*(1000./(1000.*1000.))))/(Cai_mM*Cai_mM))/(p->Cae/Cai_mM))))))+((-((0.5*p->ICaPVmf)*(p->ICaPHill*(((-(p->ICaPKmf*((1000./(1000.*1000.))*1000.)))/((Cai_mM*1000.)*(Cai_mM*1000.)))*(pow((p->ICaPKmf/(Cai_mM*1000.)),(p->ICaPHill-(1.))))))))/((1.0+(pow((p->ICaPKmf/(Cai_mM*1000.)),p->ICaPHill)))*(1.0+(pow((p->ICaPKmf/(Cai_mM*1000.)),p->ICaPHill)))))))*Cm))/(((Vol_SL*2.0)*F)*((Vol_SL*2.0)*F)))+((Vol_SL*(-((-(1000./(1000.*1000.)))*3.7243e-12)))/(Vol_SL*Vol_SL)));
  Rosenbrock_jacobian[ROSEN_Cai][ROSEN_Cai] = partial_diff_Cai_del_Cai;
  Rosenbrock_jacobian[ROSEN_Cai][ROSEN_Y0] = partial_diff_Cai_del_Y0;
  Rosenbrock_jacobian[ROSEN_Cai][ROSEN_Y1] = rc->partial_diff_Cai_del_Y1;
  Rosenbrock_jacobian[ROSEN_Cai][ROSEN_Y23] = partial_diff_Cai_del_Y23;
  Rosenbrock_jacobian[ROSEN_Cai][ROSEN_Y24] = partial_diff_Cai_del_Y24;
  Rosenbrock_jacobian[ROSEN_Cai][ROSEN_Y25] = partial_diff_Cai_del_Y25;
  Rosenbrock_jacobian[ROSEN_Cai][ROSEN_Y6] = partial_diff_Cai_del_Y6;
  Rosenbrock_jacobian[ROSEN_Cai][ROSEN_Y7] = partial_diff_Cai_del_Y7;
  Rosenbrock_jacobian[ROSEN_Y0][ROSEN_Cai] = partial_diff_Y0_del_Cai;
  Rosenbrock_jacobian[ROSEN_Y0][ROSEN_Y0] = partial_diff_Y0_del_Y0;
  Rosenbrock_jacobian[ROSEN_Y0][ROSEN_Y1] = partial_diff_Y0_del_Y1;
  Rosenbrock_jacobian[ROSEN_Y0][ROSEN_Y23] = partial_diff_Y0_del_Y23;
  Rosenbrock_jacobian[ROSEN_Y0][ROSEN_Y24] = partial_diff_Y0_del_Y24;
  Rosenbrock_jacobian[ROSEN_Y0][ROSEN_Y25] = partial_diff_Y0_del_Y25;
  Rosenbrock_jacobian[ROSEN_Y0][ROSEN_Y6] = partial_diff_Y0_del_Y6;
  Rosenbrock_jacobian[ROSEN_Y0][ROSEN_Y7] = partial_diff_Y0_del_Y7;
  Rosenbrock_jacobian[ROSEN_Y1][ROSEN_Cai] = partial_diff_Y1_del_Cai;
  Rosenbrock_jacobian[ROSEN_Y1][ROSEN_Y0] = partial_diff_Y1_del_Y0;
  Rosenbrock_jacobian[ROSEN_Y1][ROSEN_Y1] = partial_diff_Y1_del_Y1;
  Rosenbrock_jacobian[ROSEN_Y1][ROSEN_Y23] = partial_diff_Y1_del_Y23;
  Rosenbrock_jacobian[ROSEN_Y1][ROSEN_Y24] = partial_diff_Y1_del_Y24;
  Rosenbrock_jacobian[ROSEN_Y1][ROSEN_Y25] = partial_diff_Y1_del_Y25;
  Rosenbrock_jacobian[ROSEN_Y1][ROSEN_Y6] = partial_diff_Y1_del_Y6;
  Rosenbrock_jacobian[ROSEN_Y1][ROSEN_Y7] = partial_diff_Y1_del_Y7;
  Rosenbrock_jacobian[ROSEN_Y23][ROSEN_Cai] = partial_diff_Y23_del_Cai;
  Rosenbrock_jacobian[ROSEN_Y23][ROSEN_Y0] = partial_diff_Y23_del_Y0;
  Rosenbrock_jacobian[ROSEN_Y23][ROSEN_Y1] = partial_diff_Y23_del_Y1;
  Rosenbrock_jacobian[ROSEN_Y23][ROSEN_Y23] = partial_diff_Y23_del_Y23;
  Rosenbrock_jacobian[ROSEN_Y23][ROSEN_Y24] = partial_diff_Y23_del_Y24;
  Rosenbrock_jacobian[ROSEN_Y23][ROSEN_Y25] = partial_diff_Y23_del_Y25;
  Rosenbrock_jacobian[ROSEN_Y23][ROSEN_Y6] = partial_diff_Y23_del_Y6;
  Rosenbrock_jacobian[ROSEN_Y23][ROSEN_Y7] = partial_diff_Y23_del_Y7;
  Rosenbrock_jacobian[ROSEN_Y24][ROSEN_Cai] = partial_diff_Y24_del_Cai;
  Rosenbrock_jacobian[ROSEN_Y24][ROSEN_Y0] = partial_diff_Y24_del_Y0;
  Rosenbrock_jacobian[ROSEN_Y24][ROSEN_Y1] = partial_diff_Y24_del_Y1;
  Rosenbrock_jacobian[ROSEN_Y24][ROSEN_Y23] = partial_diff_Y24_del_Y23;
  Rosenbrock_jacobian[ROSEN_Y24][ROSEN_Y24] = partial_diff_Y24_del_Y24;
  Rosenbrock_jacobian[ROSEN_Y24][ROSEN_Y25] = partial_diff_Y24_del_Y25;
  Rosenbrock_jacobian[ROSEN_Y24][ROSEN_Y6] = partial_diff_Y24_del_Y6;
  Rosenbrock_jacobian[ROSEN_Y24][ROSEN_Y7] = partial_diff_Y24_del_Y7;
  Rosenbrock_jacobian[ROSEN_Y25][ROSEN_Cai] = partial_diff_Y25_del_Cai;
  Rosenbrock_jacobian[ROSEN_Y25][ROSEN_Y0] = partial_diff_Y25_del_Y0;
  Rosenbrock_jacobian[ROSEN_Y25][ROSEN_Y1] = partial_diff_Y25_del_Y1;
  Rosenbrock_jacobian[ROSEN_Y25][ROSEN_Y23] = partial_diff_Y25_del_Y23;
  Rosenbrock_jacobian[ROSEN_Y25][ROSEN_Y24] = partial_diff_Y25_del_Y24;
  Rosenbrock_jacobian[ROSEN_Y25][ROSEN_Y25] = partial_diff_Y25_del_Y25;
  Rosenbrock_jacobian[ROSEN_Y25][ROSEN_Y6] = partial_diff_Y25_del_Y6;
  Rosenbrock_jacobian[ROSEN_Y25][ROSEN_Y7] = partial_diff_Y25_del_Y7;
  Rosenbrock_jacobian[ROSEN_Y6][ROSEN_Cai] = partial_diff_Y6_del_Cai;
  Rosenbrock_jacobian[ROSEN_Y6][ROSEN_Y0] = partial_diff_Y6_del_Y0;
  Rosenbrock_jacobian[ROSEN_Y6][ROSEN_Y1] = partial_diff_Y6_del_Y1;
  Rosenbrock_jacobian[ROSEN_Y6][ROSEN_Y23] = partial_diff_Y6_del_Y23;
  Rosenbrock_jacobian[ROSEN_Y6][ROSEN_Y24] = partial_diff_Y6_del_Y24;
  Rosenbrock_jacobian[ROSEN_Y6][ROSEN_Y25] = partial_diff_Y6_del_Y25;
  Rosenbrock_jacobian[ROSEN_Y6][ROSEN_Y6] = partial_diff_Y6_del_Y6;
  Rosenbrock_jacobian[ROSEN_Y6][ROSEN_Y7] = partial_diff_Y6_del_Y7;
  Rosenbrock_jacobian[ROSEN_Y7][ROSEN_Cai] = partial_diff_Y7_del_Cai;
  Rosenbrock_jacobian[ROSEN_Y7][ROSEN_Y0] = partial_diff_Y7_del_Y0;
  Rosenbrock_jacobian[ROSEN_Y7][ROSEN_Y1] = partial_diff_Y7_del_Y1;
  Rosenbrock_jacobian[ROSEN_Y7][ROSEN_Y23] = partial_diff_Y7_del_Y23;
  Rosenbrock_jacobian[ROSEN_Y7][ROSEN_Y24] = partial_diff_Y7_del_Y24;
  Rosenbrock_jacobian[ROSEN_Y7][ROSEN_Y25] = partial_diff_Y7_del_Y25;
  Rosenbrock_jacobian[ROSEN_Y7][ROSEN_Y6] = partial_diff_Y7_del_Y6;
  Rosenbrock_jacobian[ROSEN_Y7][ROSEN_Y7] = partial_diff_Y7_del_Y7;

}


void trace_AslanidiSleiman(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("AslanidiSleiman_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->Cai\n"
        "INCX\n"
        "V\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  AslanidiSleiman_Params *p  = (AslanidiSleiman_Params *)IF->params;

  AslanidiSleiman_state *sv_base = (AslanidiSleiman_state *)IF->sv_tab.y;
  AslanidiSleiman_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Nae3 = ((p->Nae*p->Nae)*p->Nae);
  GlobalData_t Nai3 = ((p->Nai*p->Nai)*p->Nai);
  GlobalData_t RTonF = ((R*p->T)/F);
  GlobalData_t kmnai13 = ((p->kmnai1*p->kmnai1)*p->kmnai1);
  GlobalData_t kmnao3 = ((p->kmnao*p->kmnao)*p->kmnao);
  GlobalData_t partial_diff_Cai_del_Y1 = ((p->Ca_handling==1.) ? (((Vol_cytosol*3.7243e-12)/(Vol_cytosol*Vol_cytosol))*1000.) : 0.);
  GlobalData_t sigma = (((exp((p->Nae/67.3)))-(1.0))/7.0);
  GlobalData_t E_Cl = (RTonF*(log((p->Cli/p->Cle))));
  GlobalData_t E_Na = (RTonF*(log((p->Nae/p->Nai))));
  GlobalData_t partial_denomterm1_del_Cai = (((kmnao3*1.5)*(1000./(1000.*1000.)))+((kmnai13*p->Cae)*((p->kmcai*(1.5*(1000./(1000.*1000.))))/(p->kmcai*p->kmcai))));
  GlobalData_t partial_denomterm2_del_Cai = ((Nae3*1.5)*(1000./(1000.*1000.)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Ke_ext = impdata[Ke];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t Ke = Ke_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t Cai_mM = (sv->Cai/1000.);
  GlobalData_t denommult = (1.+(p->ksat*(exp(((((p->nu-(1.))*V)*F)/(R*p->T))))));
  GlobalData_t exPnuVFRT = (exp((((p->nu*V)*F)/(R*p->T))));
  GlobalData_t exPnum1VFRT = (exp(((((p->nu-(1.))*V)*F)/(R*p->T))));
  GlobalData_t allo = (1./(1.+(square((p->kmcaact/(1.5*Cai_mM))))));
  GlobalData_t denomterm1 = (((p->kmcao*Nai3)+((kmnao3*1.5)*Cai_mM))+((kmnai13*p->Cae)*(1.+((1.5*Cai_mM)/p->kmcai))));
  GlobalData_t denomterm2 = ((((p->kmcai*Nae3)*(1.+(Nai3/kmnai13)))+(Nai3*p->Cae))+((Nae3*1.5)*Cai_mM));
  GlobalData_t num = ((0.4*p->INaCamax)*(((Nai3*p->Cae)*exPnuVFRT)-((((Nae3*1.5)*Cai_mM)*exPnum1VFRT))));
  GlobalData_t deltaE = (num/(denommult*(denomterm1+denomterm2)));
  GlobalData_t INCX = (allo*deltaE);
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->Cai);
  fprintf(file, "%4.12f\t", INCX);
  fprintf(file, "%4.12f\t", V);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        