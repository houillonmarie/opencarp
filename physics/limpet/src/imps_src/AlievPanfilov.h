// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Rubin R. Aliev, Alexander V. Panfilov
*  Year: 1996
*  Title: A simple two-variable model of cardiac excitation
*  Journal: Chaos, Solitons & Fractals, 7(3),293-301
*  DOI: 10.1016/0960-0779(95)00089-5
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __ALIEVPANFILOV_H__
#define __ALIEVPANFILOV_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define AlievPanfilov_REQDAT Vm_DATA_FLAG
#define AlievPanfilov_MODDAT Iion_DATA_FLAG

struct AlievPanfilov_Params {
    GlobalData_t mu1;
    GlobalData_t mu2;

};

struct AlievPanfilov_state {
    GlobalData_t V;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_AlievPanfilov(ION_IF *);
void construct_tables_AlievPanfilov(ION_IF *);
void destroy_AlievPanfilov(ION_IF *);
void initialize_sv_AlievPanfilov(ION_IF *, GlobalData_t**);
void compute_AlievPanfilov(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
