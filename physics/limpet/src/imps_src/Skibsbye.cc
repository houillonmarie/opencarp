// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Title: Refractoriness in Human Atria: Time and Voltage Dependence of Sodium Channel Availability
*  Authors: L Skibsbye, T Jespersen, T Christ, MM Maleckar, J van den Brink, P Tavi, JT Koivumaeki
*  Year: 2016
*  Journal: Journal of Molecular and Cellular Cardiology, 2016, 101,HH26-HH34
*  DOI: 10.1016/j.yjmcc.2016.10.009
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Skibsbye.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Skibsbye(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Skibsbye( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define BNa (GlobalData_t)((0.49*2.31))
#define Bcmdn (GlobalData_t)(24e-3)
#define Begta (GlobalData_t)(0.0)
#define CSQN (GlobalData_t)(6.7)
#define CaSR1_init (GlobalData_t)(0.638748720)
#define CaSR2_init (GlobalData_t)(0.634047320)
#define CaSR3_init (GlobalData_t)(0.628196540)
#define CaSR4_init (GlobalData_t)(0.625374330)
#define Cai1_init (GlobalData_t)(1.75744430e-04)
#define Cai2_init (GlobalData_t)(1.73900720e-04)
#define Cai3_init (GlobalData_t)(1.70529930e-04)
#define Cai4_init (GlobalData_t)(1.6508230e-04)
#define Cao (GlobalData_t)(1.8)
#define Cass_init (GlobalData_t)(1.6232667e-04)
#define EC50_ACh (GlobalData_t)(0.125e-3)
#define F (GlobalData_t)(96487.)
#define ICaLfcatau (GlobalData_t)(2e-3)
#define KCa_off (GlobalData_t)(13.)
#define KCa_on (GlobalData_t)(47e6)
#define KdBNa (GlobalData_t)(10.)
#define KdBcmdn (GlobalData_t)(2.38e-3)
#define KdBegta (GlobalData_t)(0.12e-3)
#define KdCSQN (GlobalData_t)(0.8)
#define KdSLhigh (GlobalData_t)(13e-3)
#define KdSLlow (GlobalData_t)(1.1)
#define Ki_init (GlobalData_t)(136.18271)
#define Kmf_PLB (GlobalData_t)(0.12e-3)
#define Kmf_PLBKO (GlobalData_t)(0.15e-3)
#define Kmf_SLN (GlobalData_t)(0.07e-3)
#define Kmr_PLB (GlobalData_t)(0.88)
#define Kmr_PLBKO (GlobalData_t)(2.5)
#define Kmr_SLN (GlobalData_t)(0.5)
#define Ko (GlobalData_t)(5.4)
#define Nai_init (GlobalData_t)(7.9725199)
#define Nao (GlobalData_t)(130.0)
#define Nass_init (GlobalData_t)(7.7869498)
#define R (GlobalData_t)(8314.)
#define SLhigh (GlobalData_t)(13.)
#define SLlow (GlobalData_t)(165.)
#define T (GlobalData_t)(310.15)
#define Vm_init (GlobalData_t)(-74.637177)
#define dNaCa (GlobalData_t)(0.0003)
#define fCaNCX (GlobalData_t)(1.)
#define gam (GlobalData_t)(0.45)
#define hill_ACh (GlobalData_t)(1.)
#define i_ICaLd_init (GlobalData_t)(7.8635639e-05)
#define i_ICaLf1_init (GlobalData_t)(0.646754280)
#define i_ICaLf2_init (GlobalData_t)(9.9729388e-01)
#define i_ICaLfca_init (GlobalData_t)(8.1175942e-01)
#define i_IKCa_O_init (GlobalData_t)(8.7242497e-02)
#define i_IKrpa_init (GlobalData_t)(1.5016193e-03)
#define i_IKsn_init (GlobalData_t)(6.8486085e-03)
#define i_INah1_init (GlobalData_t)(8.0428032e-01)
#define i_INah2_init (GlobalData_t)(8.0489836e-01)
#define i_INam_init (GlobalData_t)(7.2340777e-03)
#define i_Ify_init (GlobalData_t)(5.5244363e-02)
#define i_Isusr_init (GlobalData_t)(3.3840493e-04)
#define i_Isuss_init (GlobalData_t)(9.6521348e-01)
#define i_Itr_init (GlobalData_t)(1.0242750e-03)
#define i_Its_init (GlobalData_t)(9.5151073e-01)
#define i_RyRa1_init (GlobalData_t)(0.197919250)
#define i_RyRa2_init (GlobalData_t)(0.203344430)
#define i_RyRa3_init (GlobalData_t)(0.211860810)
#define i_RyRass_init (GlobalData_t)(0.225821920)
#define i_RyRc1_init (GlobalData_t)(0.988420060)
#define i_RyRc2_init (GlobalData_t)(0.994663530)
#define i_RyRc3_init (GlobalData_t)(0.998449480)
#define i_RyRcss_init (GlobalData_t)(0.999787960)
#define i_RyRo1_init (GlobalData_t)(3.0612530e-04)
#define i_RyRo2_init (GlobalData_t)(2.38994540e-04)
#define i_RyRo3_init (GlobalData_t)(1.59499840e-04)
#define i_RyRoss_init (GlobalData_t)(7.77169440e-05)
#define i_SERCACa1_init (GlobalData_t)(0.0062481438)
#define i_SERCACa2_init (GlobalData_t)(0.0061641335)
#define i_SERCACa3_init (GlobalData_t)(0.0060511271)
#define i_SERCACass_init (GlobalData_t)(0.0059550415)
#define kCa (GlobalData_t)(0.7e-3)
#define kCaP (GlobalData_t)(0.0005)
#define kNaKK (GlobalData_t)(1.)
#define kNaKNa (GlobalData_t)(11.)
#define kSRleak (GlobalData_t)(6e-3)
#define pi (GlobalData_t)(3.14159265359)
#define q10_CaP (GlobalData_t)(2.35)
#define q10_DCaBmSR (GlobalData_t)(1.425)
#define q10_DCaNa (GlobalData_t)(1.18)
#define q10_ICaL (GlobalData_t)(2.6)
#define q10_IK1 (GlobalData_t)(1.4)
#define q10_INa (GlobalData_t)(3.)
#define q10_Isus (GlobalData_t)(2.2)
#define q10_Ito (GlobalData_t)(2.6)
#define q10_NCX (GlobalData_t)(1.57)
#define q10_NKA (GlobalData_t)(1.63)
#define q10_RyR (GlobalData_t)(1.5)
#define q10_SERCA (GlobalData_t)(2.)
#define BCa (GlobalData_t)((Bcmdn+Begta))
#define ECa_app (GlobalData_t)((60.+(29.2*(log10((Cao/1.8))))))
#define KdBCa (GlobalData_t)((((Bcmdn*KdBcmdn)+(Begta*KdBegta))/(Bcmdn+Begta)))
#define q10exp (GlobalData_t)(((T-(310.15))/10.))
#define DCa (GlobalData_t)(((pow(q10_DCaNa,q10exp))*833.))
#define DCaBm (GlobalData_t)(((pow(q10_DCaBmSR,q10exp))*28.8))
#define DCaSR (GlobalData_t)(((pow(q10_DCaBmSR,q10exp))*50.7))
#define DNa (GlobalData_t)(((pow(q10_DCaNa,q10exp))*0.146))
#define INaL_tauh (GlobalData_t)(((1./(pow(q10_INa,q10exp)))*0.200))
#define INa_V_shift_act (GlobalData_t)((-4.65*q10exp))
#define INa_V_shift_inact (GlobalData_t)((-7.85*q10exp))
#define RyRtauact (GlobalData_t)(((1./(pow(q10_RyR,q10exp)))*16e-3))
#define RyRtauactss (GlobalData_t)(((1./(pow(q10_RyR,q10exp)))*4.3e-3))
#define RyRtauadapt (GlobalData_t)(((1./(pow(q10_RyR,q10exp)))*0.85))
#define RyRtauinact (GlobalData_t)(((1./(pow(q10_RyR,q10exp)))*74e-3))
#define RyRtauinactss (GlobalData_t)(((1./(pow(q10_RyR,q10exp)))*13e-3))
#define k4 (GlobalData_t)(((pow(q10_SERCA,q10exp))*17.))
#define k1 (GlobalData_t)(((1000.*1000.)*k4))



void initialize_params_Skibsbye( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Skibsbye_Params *p = (Skibsbye_Params *)IF->params;

  // Compute the regional constants
  {
    p->ACh = 0.;
    p->cAF_PLB = 1.;
    p->cAF_RyR = 1.;
    p->cAF_SLN = 1.;
    p->cAF_cpumps = 1.;
    p->cAF_gCaL = 1.;
    p->cAF_gK1 = 1.;
    p->cAF_gKCa = 1.;
    p->cAF_gKs = 1.;
    p->cAF_gNa = 1.;
    p->cAF_gsus = 1.;
    p->cAF_gt = 1.;
    p->cAF_kNaCa = 1.;
    p->cAF_lcell = 1.;
    p->cAF_phos = 1.;
    p->factorgcal = 1.;
    p->factorgk1 = 1.;
    p->factorgkr = 1.;
    p->factorgks = 1.;
    p->factorgto = 1.;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_Skibsbye;

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Skibsbye( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e-3;
  cell_geom *region = &IF->cgeom;
  Skibsbye_Params *p = (Skibsbye_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double Ddcell = (((p->cAF_lcell-(1.))*(20./10.))+1.);
  double PLB_SERCA_ratio = p->cAF_PLB;
  double SLN_SERCA_ratio = p->cAF_SLN;
  double base_phos = (0.1*p->cAF_phos);
  double gCaL = ((6.5*p->cAF_gCaL)*p->factorgcal);
  double lcell = (122.051*p->cAF_lcell);
  double Dacell = (p->cAF_lcell*Ddcell);
  double Dvcell = ((p->cAF_lcell*Ddcell)*Ddcell);
  double SERCAKmf = ((Kmf_PLBKO+((Kmf_PLB*PLB_SERCA_ratio)*(1.-(base_phos))))+((Kmf_SLN*SLN_SERCA_ratio)*(1.-(base_phos))));
  double SERCAKmr = ((Kmr_PLBKO-(((Kmr_PLB*PLB_SERCA_ratio)*(1.-(base_phos)))))-(((Kmr_SLN*SLN_SERCA_ratio)*(1.-(base_phos)))));
  double dx = (1.625*Ddcell);
  double rjunct = (6.5*Ddcell);
  double Aj_nj = ((((pi*rjunct)*2.)*lcell)*0.5);
  double Cm = (0.050*Dacell);
  double ICaPmax = (((pow(q10_CaP,q10exp))*2.0)*Dacell);
  double INaKmax = (((pow(q10_NKA,q10exp))*70.8253)*Dacell);
  double Vnonjunct1 = (((((pi*lcell)*1e-6)*0.5)*dx)*dx);
  double Vss = (4.99232e-5*Dvcell);
  double cpumps = ((30e-3/Dvcell)*p->cAF_cpumps);
  double gCab = (0.078*Dacell);
  double gIf = Dacell;
  double gK1 = (((2.8*p->cAF_gK1)*Dacell)*p->factorgk1);
  double gKACh = ((6.375/(1.+(pow((EC50_ACh/p->ACh),hill_ACh))))*Dacell);
  double gKCa = ((3.6*p->cAF_gKCa)*Dacell);
  double gKr = (((5.2*(sqrt((Ko/5.4))))*Dacell)*p->factorgkr);
  double gKs = (((0.175*p->cAF_gKs)*Dacell)*p->factorgks);
  double gNa = (((0.9*640.)*p->cAF_gNa)*Dacell);
  double gNaL = ((0.9*0.39)*Dacell);
  double gNab = (0.060599*Dacell);
  double gsus = ((2.25*p->cAF_gsus)*Dacell);
  double gt = (((11.*p->cAF_gt)*Dacell)*p->factorgto);
  double k2 = (k1*(SERCAKmf*SERCAKmf));
  double k3 = (k4/(SERCAKmr*SERCAKmr));
  double kNaCa = ((((pow(q10_NCX,q10exp))*0.0081)*p->cAF_kNaCa)*Dacell);
  double xj_nj = (((0.02/2.)*Ddcell)+(dx/2.));
  double xj_nj_Nai = (((0.02/2.)*Ddcell)+(2.*dx));
  double VSR1 = ((((0.05*Vnonjunct1)/2.)*0.9)/Dvcell);
  double Vcytosol = ((16.*Vnonjunct1)+Vss);
  double Vnonjunct2 = (3.*Vnonjunct1);
  double Vnonjunct3 = (5.*Vnonjunct1);
  double Vnonjunct4 = (7.*Vnonjunct1);
  double Vnonjunct_Nai = (16.*Vnonjunct1);
  double nu1 = ((1.6*Vnonjunct1)/Dvcell);
  double nuss = ((900.*Vss)/Dvcell);
  double VSR2 = ((((0.05*Vnonjunct2)/2.)*0.9)/Dvcell);
  double VSR3 = ((((0.05*Vnonjunct3)/2.)*0.9)/Dvcell);
  double VSR4 = ((((0.05*Vnonjunct4)/2.)*0.9)/Dvcell);
  double nu2 = ((1.6*Vnonjunct2)/Dvcell);
  double nu3 = ((1.6*Vnonjunct3)/Dvcell);

}



void    initialize_sv_Skibsbye( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e-3;
  cell_geom *region = &IF->cgeom;
  Skibsbye_Params *p = (Skibsbye_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Skibsbye_state) );
  Skibsbye_state *sv_base = (Skibsbye_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double Ddcell = (((p->cAF_lcell-(1.))*(20./10.))+1.);
  double PLB_SERCA_ratio = p->cAF_PLB;
  double SLN_SERCA_ratio = p->cAF_SLN;
  double base_phos = (0.1*p->cAF_phos);
  double gCaL = ((6.5*p->cAF_gCaL)*p->factorgcal);
  double lcell = (122.051*p->cAF_lcell);
  double Dacell = (p->cAF_lcell*Ddcell);
  double Dvcell = ((p->cAF_lcell*Ddcell)*Ddcell);
  double SERCAKmf = ((Kmf_PLBKO+((Kmf_PLB*PLB_SERCA_ratio)*(1.-(base_phos))))+((Kmf_SLN*SLN_SERCA_ratio)*(1.-(base_phos))));
  double SERCAKmr = ((Kmr_PLBKO-(((Kmr_PLB*PLB_SERCA_ratio)*(1.-(base_phos)))))-(((Kmr_SLN*SLN_SERCA_ratio)*(1.-(base_phos)))));
  double dx = (1.625*Ddcell);
  double rjunct = (6.5*Ddcell);
  double Aj_nj = ((((pi*rjunct)*2.)*lcell)*0.5);
  double Cm = (0.050*Dacell);
  double ICaPmax = (((pow(q10_CaP,q10exp))*2.0)*Dacell);
  double INaKmax = (((pow(q10_NKA,q10exp))*70.8253)*Dacell);
  double Vnonjunct1 = (((((pi*lcell)*1e-6)*0.5)*dx)*dx);
  double Vss = (4.99232e-5*Dvcell);
  double cpumps = ((30e-3/Dvcell)*p->cAF_cpumps);
  double gCab = (0.078*Dacell);
  double gIf = Dacell;
  double gK1 = (((2.8*p->cAF_gK1)*Dacell)*p->factorgk1);
  double gKACh = ((6.375/(1.+(pow((EC50_ACh/p->ACh),hill_ACh))))*Dacell);
  double gKCa = ((3.6*p->cAF_gKCa)*Dacell);
  double gKr = (((5.2*(sqrt((Ko/5.4))))*Dacell)*p->factorgkr);
  double gKs = (((0.175*p->cAF_gKs)*Dacell)*p->factorgks);
  double gNa = (((0.9*640.)*p->cAF_gNa)*Dacell);
  double gNaL = ((0.9*0.39)*Dacell);
  double gNab = (0.060599*Dacell);
  double gsus = ((2.25*p->cAF_gsus)*Dacell);
  double gt = (((11.*p->cAF_gt)*Dacell)*p->factorgto);
  double k2 = (k1*(SERCAKmf*SERCAKmf));
  double k3 = (k4/(SERCAKmr*SERCAKmr));
  double kNaCa = ((((pow(q10_NCX,q10exp))*0.0081)*p->cAF_kNaCa)*Dacell);
  double xj_nj = (((0.02/2.)*Ddcell)+(dx/2.));
  double xj_nj_Nai = (((0.02/2.)*Ddcell)+(2.*dx));
  double VSR1 = ((((0.05*Vnonjunct1)/2.)*0.9)/Dvcell);
  double Vcytosol = ((16.*Vnonjunct1)+Vss);
  double Vnonjunct2 = (3.*Vnonjunct1);
  double Vnonjunct3 = (5.*Vnonjunct1);
  double Vnonjunct4 = (7.*Vnonjunct1);
  double Vnonjunct_Nai = (16.*Vnonjunct1);
  double nu1 = ((1.6*Vnonjunct1)/Dvcell);
  double nuss = ((900.*Vss)/Dvcell);
  double VSR2 = ((((0.05*Vnonjunct2)/2.)*0.9)/Dvcell);
  double VSR3 = ((((0.05*Vnonjunct3)/2.)*0.9)/Dvcell);
  double VSR4 = ((((0.05*Vnonjunct4)/2.)*0.9)/Dvcell);
  double nu2 = ((1.6*Vnonjunct2)/Dvcell);
  double nu3 = ((1.6*Vnonjunct3)/Dvcell);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vm_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Skibsbye_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Vm = Vm_ext[__i];
    //Change the units of external variables as appropriate.
    Iion *= 1e3;
    
    
    // Initialize the rest of the nodal variables
    sv->CaSR1 = CaSR1_init;
    sv->CaSR2 = CaSR2_init;
    sv->CaSR3 = CaSR3_init;
    sv->CaSR4 = CaSR4_init;
    sv->Cai1 = Cai1_init;
    sv->Cai2 = Cai2_init;
    sv->Cai3 = Cai3_init;
    sv->Cai4 = Cai4_init;
    sv->Cass = Cass_init;
    sv->Ki = Ki_init;
    sv->Nai = Nai_init;
    sv->Nass = Nass_init;
    Vm = Vm_init;
    sv->i_ICaLd = i_ICaLd_init;
    sv->i_ICaLf1 = i_ICaLf1_init;
    sv->i_ICaLf2 = i_ICaLf2_init;
    sv->i_ICaLfca = i_ICaLfca_init;
    sv->i_IKCa_O = i_IKCa_O_init;
    sv->i_IKrpa = i_IKrpa_init;
    sv->i_IKsn = i_IKsn_init;
    sv->i_INah1 = i_INah1_init;
    sv->i_INah2 = i_INah2_init;
    sv->i_INam = i_INam_init;
    sv->i_Ify = i_Ify_init;
    sv->i_Isusr = i_Isusr_init;
    sv->i_Isuss = i_Isuss_init;
    sv->i_Itr = i_Itr_init;
    sv->i_Its = i_Its_init;
    sv->i_RyRa1 = i_RyRa1_init;
    sv->i_RyRa2 = i_RyRa2_init;
    sv->i_RyRa3 = i_RyRa3_init;
    sv->i_RyRass = i_RyRass_init;
    sv->i_RyRc1 = i_RyRc1_init;
    sv->i_RyRc2 = i_RyRc2_init;
    sv->i_RyRc3 = i_RyRc3_init;
    sv->i_RyRcss = i_RyRcss_init;
    sv->i_RyRo1 = i_RyRo1_init;
    sv->i_RyRo2 = i_RyRo2_init;
    sv->i_RyRo3 = i_RyRo3_init;
    sv->i_RyRoss = i_RyRoss_init;
    sv->i_SERCACa1 = i_SERCACa1_init;
    sv->i_SERCACa2 = i_SERCACa2_init;
    sv->i_SERCACa3 = i_SERCACa3_init;
    sv->i_SERCACass = i_SERCACass_init;
    double ECa = ((((R*T)/F)/2.)*(log((Cao/sv->Cass))));
    double EK = (((R*T)/F)*(log((Ko/sv->Ki))));
    double ENa = (((R*T)/F)*(log((Nao/sv->Nass))));
    double ICaL = ((((gCaL*sv->i_ICaLd)*sv->i_ICaLf2)*sv->i_ICaLfca)*(Vm-(ECa_app)));
    double ICaP = ((ICaPmax*sv->Cass)/(kCaP+sv->Cass));
    double IKrpi = (1./(1.+(exp(((Vm+74.)/24.)))));
    double INaCa = (kNaCa*(((((exp(((((gam*Vm)*F)/R)/T)))*(pow(sv->Nass,3.)))*Cao)-(((((exp((((((gam-(1.))*Vm)*F)/R)/T)))*(pow(Nao,3.)))*sv->Cass)*fCaNCX)))/(1.+(dNaCa*((((pow(Nao,3.))*sv->Cass)*fCaNCX)+((pow(sv->Nass,3.))*Cao))))));
    double INaK = ((((((INaKmax*Ko)/(Ko+kNaKK))*(pow(sv->Nass,1.5)))/((pow(sv->Nass,1.5))+(pow(kNaKNa,1.5))))*(Vm+150.))/(Vm+200.));
    double ICab = (gCab*(Vm-(ECa)));
    double IK1 = (((((pow(q10_IK1,q10exp))*gK1)*(pow(Ko,0.4457)))*(Vm-(EK)))/(1.+(exp(((((1.5*((Vm-(EK))+3.6))*F)/R)/T)))));
    double IKACh = (((gKACh*(sqrt((Ko/5.4))))*(Vm-(EK)))*(0.055+(0.400/(1.+(exp((((Vm-(EK))+9.53)/17.18)))))));
    double IKCa = (((gKCa*sv->i_IKCa_O)*(1./(1.+(exp((((Vm-(EK))+120.)/45.))))))*(Vm-(EK)));
    double IKr = (((gKr*sv->i_IKrpa)*IKrpi)*(Vm-(EK)));
    double IKs = (((gKs*sv->i_IKsn)*sv->i_IKsn)*(Vm-(EK)));
    double INa = ((((((gNa*sv->i_INam)*sv->i_INam)*sv->i_INam)*sv->i_INah1)*sv->i_INah2)*(Vm-(ENa)));
    double INaL = (((((gNaL*sv->i_INam)*sv->i_INam)*sv->i_INam)*sv->i_ICaLf1)*(Vm-(ENa)));
    double INab = (gNab*(Vm-(ENa)));
    double IfK = ((gIf*sv->i_Ify)*((1.-(0.2677))*(Vm-(EK))));
    double IfNa = ((gIf*sv->i_Ify)*(0.2677*(Vm-(ENa))));
    double Isus = (((gsus*sv->i_Isusr)*sv->i_Isuss)*(Vm-(EK)));
    double It = (((gt*sv->i_Itr)*sv->i_Its)*(Vm-(EK)));
    double If = (IfK+IfNa);
    Iion = ((((((((((((((((INa+INaL)+ICaL)+It)+Isus)+IK1)+IKACh)+IKr)+IKs)+INab)+ICab)+INaK)+ICaP)+INaCa)+If)+IKCa)/Cm);
    //Change the units of external variables as appropriate.
    Iion *= 1e-3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Vm_ext[__i] = Vm;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Skibsbye(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e-3;
  cell_geom *region = &IF->cgeom;
  Skibsbye_Params *p  = (Skibsbye_Params *)IF->params;
  Skibsbye_state *sv_base = (Skibsbye_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Ddcell = (((p->cAF_lcell-(1.))*(20./10.))+1.);
  GlobalData_t PLB_SERCA_ratio = p->cAF_PLB;
  GlobalData_t SLN_SERCA_ratio = p->cAF_SLN;
  GlobalData_t base_phos = (0.1*p->cAF_phos);
  GlobalData_t gCaL = ((6.5*p->cAF_gCaL)*p->factorgcal);
  GlobalData_t lcell = (122.051*p->cAF_lcell);
  GlobalData_t Dacell = (p->cAF_lcell*Ddcell);
  GlobalData_t Dvcell = ((p->cAF_lcell*Ddcell)*Ddcell);
  GlobalData_t SERCAKmf = ((Kmf_PLBKO+((Kmf_PLB*PLB_SERCA_ratio)*(1.-(base_phos))))+((Kmf_SLN*SLN_SERCA_ratio)*(1.-(base_phos))));
  GlobalData_t SERCAKmr = ((Kmr_PLBKO-(((Kmr_PLB*PLB_SERCA_ratio)*(1.-(base_phos)))))-(((Kmr_SLN*SLN_SERCA_ratio)*(1.-(base_phos)))));
  GlobalData_t dx = (1.625*Ddcell);
  GlobalData_t rjunct = (6.5*Ddcell);
  GlobalData_t Aj_nj = ((((pi*rjunct)*2.)*lcell)*0.5);
  GlobalData_t Cm = (0.050*Dacell);
  GlobalData_t ICaPmax = (((pow(q10_CaP,q10exp))*2.0)*Dacell);
  GlobalData_t INaKmax = (((pow(q10_NKA,q10exp))*70.8253)*Dacell);
  GlobalData_t Vnonjunct1 = (((((pi*lcell)*1e-6)*0.5)*dx)*dx);
  GlobalData_t Vss = (4.99232e-5*Dvcell);
  GlobalData_t cpumps = ((30e-3/Dvcell)*p->cAF_cpumps);
  GlobalData_t gCab = (0.078*Dacell);
  GlobalData_t gIf = Dacell;
  GlobalData_t gK1 = (((2.8*p->cAF_gK1)*Dacell)*p->factorgk1);
  GlobalData_t gKACh = ((6.375/(1.+(pow((EC50_ACh/p->ACh),hill_ACh))))*Dacell);
  GlobalData_t gKCa = ((3.6*p->cAF_gKCa)*Dacell);
  GlobalData_t gKr = (((5.2*(sqrt((Ko/5.4))))*Dacell)*p->factorgkr);
  GlobalData_t gKs = (((0.175*p->cAF_gKs)*Dacell)*p->factorgks);
  GlobalData_t gNa = (((0.9*640.)*p->cAF_gNa)*Dacell);
  GlobalData_t gNaL = ((0.9*0.39)*Dacell);
  GlobalData_t gNab = (0.060599*Dacell);
  GlobalData_t gsus = ((2.25*p->cAF_gsus)*Dacell);
  GlobalData_t gt = (((11.*p->cAF_gt)*Dacell)*p->factorgto);
  GlobalData_t k2 = (k1*(SERCAKmf*SERCAKmf));
  GlobalData_t k3 = (k4/(SERCAKmr*SERCAKmr));
  GlobalData_t kNaCa = ((((pow(q10_NCX,q10exp))*0.0081)*p->cAF_kNaCa)*Dacell);
  GlobalData_t xj_nj = (((0.02/2.)*Ddcell)+(dx/2.));
  GlobalData_t xj_nj_Nai = (((0.02/2.)*Ddcell)+(2.*dx));
  GlobalData_t VSR1 = ((((0.05*Vnonjunct1)/2.)*0.9)/Dvcell);
  GlobalData_t Vcytosol = ((16.*Vnonjunct1)+Vss);
  GlobalData_t Vnonjunct2 = (3.*Vnonjunct1);
  GlobalData_t Vnonjunct3 = (5.*Vnonjunct1);
  GlobalData_t Vnonjunct4 = (7.*Vnonjunct1);
  GlobalData_t Vnonjunct_Nai = (16.*Vnonjunct1);
  GlobalData_t nu1 = ((1.6*Vnonjunct1)/Dvcell);
  GlobalData_t nuss = ((900.*Vss)/Dvcell);
  GlobalData_t VSR2 = ((((0.05*Vnonjunct2)/2.)*0.9)/Dvcell);
  GlobalData_t VSR3 = ((((0.05*Vnonjunct3)/2.)*0.9)/Dvcell);
  GlobalData_t VSR4 = ((((0.05*Vnonjunct4)/2.)*0.9)/Dvcell);
  GlobalData_t nu2 = ((1.6*Vnonjunct2)/Dvcell);
  GlobalData_t nu3 = ((1.6*Vnonjunct3)/Dvcell);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vm_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Skibsbye_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Vm = Vm_ext[__i];
    //Change the units of external variables as appropriate.
    Iion *= 1e3;
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t ECa = ((((R*T)/F)/2.)*(log((Cao/sv->Cass))));
    GlobalData_t EK = (((R*T)/F)*(log((Ko/sv->Ki))));
    GlobalData_t ENa = (((R*T)/F)*(log((Nao/sv->Nass))));
    GlobalData_t ICaL = ((((gCaL*sv->i_ICaLd)*sv->i_ICaLf2)*sv->i_ICaLfca)*(Vm-(ECa_app)));
    GlobalData_t ICaP = ((ICaPmax*sv->Cass)/(kCaP+sv->Cass));
    GlobalData_t IKrpi = (1./(1.+(exp(((Vm+74.)/24.)))));
    GlobalData_t INaCa = (kNaCa*(((((exp(((((gam*Vm)*F)/R)/T)))*(pow(sv->Nass,3.)))*Cao)-(((((exp((((((gam-(1.))*Vm)*F)/R)/T)))*(pow(Nao,3.)))*sv->Cass)*fCaNCX)))/(1.+(dNaCa*((((pow(Nao,3.))*sv->Cass)*fCaNCX)+((pow(sv->Nass,3.))*Cao))))));
    GlobalData_t INaK = ((((((INaKmax*Ko)/(Ko+kNaKK))*(pow(sv->Nass,1.5)))/((pow(sv->Nass,1.5))+(pow(kNaKNa,1.5))))*(Vm+150.))/(Vm+200.));
    GlobalData_t ICab = (gCab*(Vm-(ECa)));
    GlobalData_t IK1 = (((((pow(q10_IK1,q10exp))*gK1)*(pow(Ko,0.4457)))*(Vm-(EK)))/(1.+(exp(((((1.5*((Vm-(EK))+3.6))*F)/R)/T)))));
    GlobalData_t IKACh = (((gKACh*(sqrt((Ko/5.4))))*(Vm-(EK)))*(0.055+(0.400/(1.+(exp((((Vm-(EK))+9.53)/17.18)))))));
    GlobalData_t IKCa = (((gKCa*sv->i_IKCa_O)*(1./(1.+(exp((((Vm-(EK))+120.)/45.))))))*(Vm-(EK)));
    GlobalData_t IKr = (((gKr*sv->i_IKrpa)*IKrpi)*(Vm-(EK)));
    GlobalData_t IKs = (((gKs*sv->i_IKsn)*sv->i_IKsn)*(Vm-(EK)));
    GlobalData_t INa = ((((((gNa*sv->i_INam)*sv->i_INam)*sv->i_INam)*sv->i_INah1)*sv->i_INah2)*(Vm-(ENa)));
    GlobalData_t INaL = (((((gNaL*sv->i_INam)*sv->i_INam)*sv->i_INam)*sv->i_ICaLf1)*(Vm-(ENa)));
    GlobalData_t INab = (gNab*(Vm-(ENa)));
    GlobalData_t IfK = ((gIf*sv->i_Ify)*((1.-(0.2677))*(Vm-(EK))));
    GlobalData_t IfNa = ((gIf*sv->i_Ify)*(0.2677*(Vm-(ENa))));
    GlobalData_t Isus = (((gsus*sv->i_Isusr)*sv->i_Isuss)*(Vm-(EK)));
    GlobalData_t It = (((gt*sv->i_Itr)*sv->i_Its)*(Vm-(EK)));
    GlobalData_t If = (IfK+IfNa);
    Iion = ((((((((((((((((INa+INaL)+ICaL)+It)+Isus)+IK1)+IKACh)+IKr)+IKs)+INab)+ICab)+INaK)+ICaP)+INaCa)+If)+IKCa)/Cm);
    
    
    //Complete Forward Euler Update
    GlobalData_t ICaLdinf = (1./(1.+(exp(((Vm+9.5)/-6.9)))));
    GlobalData_t ICaLdtau = ((1./(pow(q10_ICaL,q10exp)))*((0.00065*(exp((-(pow(((Vm+35.)/30.),2.))))))+0.0005));
    GlobalData_t ICaLf2tau = ((1./(pow(q10_ICaL,q10exp)))*((1.34*(exp((-(pow(((Vm+40.)/14.2),2.))))))+0.04));
    GlobalData_t ICaLfcainf = (1./(1.+(sv->Cass/kCa)));
    GlobalData_t ICaLfinf = ((0.04+(0.96/(1.+(exp(((Vm+25.5)/8.4))))))+(1./(1.+(exp(((-(Vm-(60.)))/8.0))))));
    GlobalData_t IKrpainf = (1./(1.+(exp(((-(Vm+10.))/5.)))));
    GlobalData_t IKrpatau = ((((0.550/(1.+(exp(((-22.-(Vm))/9.)))))*6.)/(1.+(exp(((Vm-(-11.))/9.)))))+(0.230/(1.+(exp(((Vm-(-40.))/20.))))));
    GlobalData_t IKsninf = (1./(1.+(exp(((-(Vm+3.8))/14.25)))));
    GlobalData_t IKsntau = (0.9901/(1.+(exp(((-(Vm+2.436))/14.12)))));
    GlobalData_t INaL_hinf = (1./(1.+(exp((((Vm+71.8)+INa_V_shift_inact)/4.7)))));
    GlobalData_t INah1tau = ((1./(pow(q10_INa,q10exp)))*((0.00007+(0.034/((1.+(exp((((Vm+41.)+INa_V_shift_inact)/5.5))))+(exp(((-((Vm+41.)+INa_V_shift_inact))/14.))))))+(0.0002/(1.+(exp(((-((Vm+79.)+INa_V_shift_inact))/14.)))))));
    GlobalData_t INah2tau = ((1./(pow(q10_INa,q10exp)))*((0.0007+(0.15/((1.+(exp((((Vm+41.)+INa_V_shift_inact)/5.5))))+(exp(((-((Vm+41.)+INa_V_shift_inact))/14.))))))+(0.002/(1.+(exp(((-((Vm+79.)+INa_V_shift_inact))/14.)))))));
    GlobalData_t INahinf = (1./(1.+(exp((((Vm+66.8)+INa_V_shift_inact)/5.6)))));
    GlobalData_t INaminf = (1./(1.+(exp((((Vm+36.3)+INa_V_shift_act)/-7.8)))));
    GlobalData_t INamtau = ((1./(pow(q10_INa,q10exp)))*((0.00001+(0.00013*(exp((-(pow((((Vm+48.)+INa_V_shift_act)/15.),2.)))))))+(0.000045/(1.+(exp((((Vm+42.)+INa_V_shift_act)/-5.)))))));
    GlobalData_t Ifyinf = (1./(1.+(exp(((Vm+97.82874)/12.48025)))));
    GlobalData_t Ifytau = (1./((0.00332*(exp(((-Vm)/16.54103))))+(23.71839*(exp((Vm/16.54103))))));
    GlobalData_t Isusrinf = (1./(1.+(exp(((Vm+6.)/-8.6)))));
    GlobalData_t Isusrtau = ((1./(pow(q10_Isus,q10exp)))*((0.0066/(1.+(exp(((Vm+5.)/12.)))))+0.00036));
    GlobalData_t Isussinf = (1./(1.+(exp(((Vm+7.5)/10.)))));
    GlobalData_t Isusstau = ((1./(pow(q10_Isus,q10exp)))*((0.43/(1.+(exp(((Vm+60.)/10.)))))+2.2));
    GlobalData_t Itrinf = (1./(1.+(exp(((Vm-(1.))/-11.)))));
    GlobalData_t Itrtau = ((1./(pow(q10_Ito,q10exp)))*((0.0024*(exp((-(pow((Vm/30.),2.))))))+0.0010));
    GlobalData_t Itsinf = (1./(1.+(exp(((Vm+40.5)/11.5)))));
    GlobalData_t Itstau = ((1./(pow(q10_Ito,q10exp)))*((0.018*(exp((-(pow(((Vm+52.45)/15.88),2.))))))+0.0096));
    GlobalData_t JNa = ((((DNa*Aj_nj)/xj_nj_Nai)*(sv->Nass-(sv->Nai)))*1e-6);
    GlobalData_t JSRCaleak1 = (((kSRleak*(sv->CaSR1-(sv->Cai1)))*Vnonjunct1)/Dvcell);
    GlobalData_t JSRCaleak2 = (((kSRleak*(sv->CaSR2-(sv->Cai2)))*Vnonjunct2)/Dvcell);
    GlobalData_t JSRCaleak3 = (((kSRleak*(sv->CaSR3-(sv->Cai3)))*Vnonjunct3)/Dvcell);
    GlobalData_t JSRCaleakss = (((kSRleak*(sv->CaSR4-(sv->Cass)))*Vss)/Dvcell);
    GlobalData_t J_SERCASR1 = ((((((-k3)*(pow(sv->CaSR1,2.)))*(cpumps-(sv->i_SERCACa1)))+(k4*sv->i_SERCACa1))*Vnonjunct1)*2.);
    GlobalData_t J_SERCASR2 = ((((((-k3)*(pow(sv->CaSR2,2.)))*(cpumps-(sv->i_SERCACa2)))+(k4*sv->i_SERCACa2))*Vnonjunct2)*2.);
    GlobalData_t J_SERCASR3 = ((((((-k3)*(pow(sv->CaSR3,2.)))*(cpumps-(sv->i_SERCACa3)))+(k4*sv->i_SERCACa3))*Vnonjunct3)*2.);
    GlobalData_t J_SERCASRss = ((((((-k3)*(pow(sv->CaSR4,2.)))*(cpumps-(sv->i_SERCACass)))+(k4*sv->i_SERCACass))*Vss)*2.);
    GlobalData_t J_bulkSERCA1 = (((((k1*(pow(sv->Cai1,2.)))*(cpumps-(sv->i_SERCACa1)))-((k2*sv->i_SERCACa1)))*Vnonjunct1)*2.);
    GlobalData_t J_bulkSERCA2 = (((((k1*(pow(sv->Cai2,2.)))*(cpumps-(sv->i_SERCACa2)))-((k2*sv->i_SERCACa2)))*Vnonjunct2)*2.);
    GlobalData_t J_bulkSERCA3 = (((((k1*(pow(sv->Cai3,2.)))*(cpumps-(sv->i_SERCACa3)))-((k2*sv->i_SERCACa3)))*Vnonjunct3)*2.);
    GlobalData_t J_bulkSERCAss = (((((k1*(pow(sv->Cass,2.)))*(cpumps-(sv->i_SERCACass)))-((k2*sv->i_SERCACass)))*Vss)*2.);
    GlobalData_t Jj_nj = ((((DCa*Aj_nj)/xj_nj)*(sv->Cass-(sv->Cai4)))*1e-6);
    GlobalData_t RyRSRCa1 = (1.-((1./(1.+(exp(((sv->CaSR1-((0.3/p->cAF_RyR)))/0.1)))))));
    GlobalData_t RyRSRCa2 = (1.-((1./(1.+(exp(((sv->CaSR2-((0.3/p->cAF_RyR)))/0.1)))))));
    GlobalData_t RyRSRCa3 = (1.-((1./(1.+(exp(((sv->CaSR3-((0.3/p->cAF_RyR)))/0.1)))))));
    GlobalData_t RyRSRCass = (1.-((1./(1.+(exp(((sv->CaSR4-((0.3/p->cAF_RyR)))/0.1)))))));
    GlobalData_t RyRainf1 = (0.505-((0.427/(1.+(exp((((sv->Cai1*1000.)-(0.29))/0.082)))))));
    GlobalData_t RyRainf2 = (0.505-((0.427/(1.+(exp((((sv->Cai2*1000.)-(0.29))/0.082)))))));
    GlobalData_t RyRainf3 = (0.505-((0.427/(1.+(exp((((sv->Cai3*1000.)-(0.29))/0.082)))))));
    GlobalData_t RyRainfss = (0.505-((0.427/(1.+(exp((((sv->Cass*1000.)-(0.29))/0.082)))))));
    GlobalData_t RyRcinf1 = (1./(1.+(exp((((sv->Cai1*1000.)-((sv->i_RyRa1+0.02)))/0.01)))));
    GlobalData_t RyRcinf2 = (1./(1.+(exp((((sv->Cai2*1000.)-((sv->i_RyRa2+0.02)))/0.01)))));
    GlobalData_t RyRcinf3 = (1./(1.+(exp((((sv->Cai3*1000.)-((sv->i_RyRa3+0.02)))/0.01)))));
    GlobalData_t RyRcinfss = (1./(1.+(exp((((sv->Cass*1000.)-((sv->i_RyRass+0.02)))/0.01)))));
    GlobalData_t RyRoinf1 = (1.-((1./(1.+(exp((((sv->Cai1*1000.)-((sv->i_RyRa1+(0.22/p->cAF_RyR))))/0.03)))))));
    GlobalData_t RyRoinf2 = (1.-((1./(1.+(exp((((sv->Cai2*1000.)-((sv->i_RyRa2+(0.22/p->cAF_RyR))))/0.03)))))));
    GlobalData_t RyRoinf3 = (1.-((1./(1.+(exp((((sv->Cai3*1000.)-((sv->i_RyRa3+(0.22/p->cAF_RyR))))/0.03)))))));
    GlobalData_t RyRoinfss = (1.-((1./(1.+(exp((((sv->Cass*1000.)-((sv->i_RyRass+(0.22/p->cAF_RyR))))/0.03)))))));
    GlobalData_t betaNass = (1./(1.+((BNa*KdBNa)/(pow((sv->Nass+KdBNa),2.)))));
    GlobalData_t betaSR1 = (1./(1.+((CSQN*KdCSQN)/(pow((sv->CaSR1+KdCSQN),2.)))));
    GlobalData_t betaSR2 = (1./(1.+((CSQN*KdCSQN)/(pow((sv->CaSR2+KdCSQN),2.)))));
    GlobalData_t betaSR3 = (1./(1.+((CSQN*KdCSQN)/(pow((sv->CaSR3+KdCSQN),2.)))));
    GlobalData_t betaSR4 = (1./(1.+((CSQN*KdCSQN)/(pow((sv->CaSR4+KdCSQN),2.)))));
    GlobalData_t betai1 = (1./(1.+((BCa*KdBCa)/(pow((sv->Cai1+KdBCa),2.)))));
    GlobalData_t betai2 = (1./(1.+((BCa*KdBCa)/(pow((sv->Cai2+KdBCa),2.)))));
    GlobalData_t betai3 = (1./(1.+((BCa*KdBCa)/(pow((sv->Cai3+KdBCa),2.)))));
    GlobalData_t betai4 = (1./(1.+((BCa*KdBCa)/(pow((sv->Cai4+KdBCa),2.)))));
    GlobalData_t betass = (1./(((1.+((SLlow*KdSLlow)/(pow((sv->Cass+KdSLlow),2.))))+((SLhigh*KdSLhigh)/(pow((sv->Cass+KdSLhigh),2.))))+((BCa*KdBCa)/(pow((sv->Cass+KdBCa),2.)))));
    GlobalData_t diff_Ki = ((-((((((((It+Isus)+IK1)+IKACh)+IKCa)+IKr)+IKs)-((2.*INaK)))+IfK))/(Vcytosol*F));
    GlobalData_t diff_i_IKCa_O = (((((1.-(sv->i_IKCa_O))*KCa_on)*sv->Cass)*sv->Cass)-((sv->i_IKCa_O*KCa_off)));
    GlobalData_t gammai1 = ((BCa*KdBCa)/(pow((sv->Cai1+KdBCa),2.)));
    GlobalData_t gammai2 = ((BCa*KdBCa)/(pow((sv->Cai2+KdBCa),2.)));
    GlobalData_t gammai3 = ((BCa*KdBCa)/(pow((sv->Cai3+KdBCa),2.)));
    GlobalData_t gammai4 = ((BCa*KdBCa)/(pow((sv->Cai4+KdBCa),2.)));
    GlobalData_t INah1inf = INahinf;
    GlobalData_t INah2inf = INahinf;
    GlobalData_t JCa4 = Jj_nj;
    GlobalData_t Jrel1 = ((((nu1*sv->i_RyRo1)*sv->i_RyRc1)*RyRSRCa1)*(sv->CaSR1-(sv->Cai1)));
    GlobalData_t Jrel2 = ((((nu2*sv->i_RyRo2)*sv->i_RyRc2)*RyRSRCa2)*(sv->CaSR2-(sv->Cai2)));
    GlobalData_t Jrel3 = ((((nu3*sv->i_RyRo3)*sv->i_RyRc3)*RyRSRCa3)*(sv->CaSR3-(sv->Cai3)));
    GlobalData_t Jrelss = ((((nuss*sv->i_RyRoss)*sv->i_RyRcss)*RyRSRCass)*(sv->CaSR4-(sv->Cass)));
    GlobalData_t diff_Nai = (JNa/Vnonjunct_Nai);
    GlobalData_t diff_Nass = (betaNass*(((-JNa)/Vss)-(((((((INa+INaL)+INab)+(3.*INaK))+(3.*INaCa))+IfNa)/(Vss*F)))));
    GlobalData_t diff_i_ICaLd = ((ICaLdinf-(sv->i_ICaLd))/ICaLdtau);
    GlobalData_t diff_i_ICaLf1 = ((INaL_hinf-(sv->i_ICaLf1))/INaL_tauh);
    GlobalData_t diff_i_ICaLf2 = ((ICaLfinf-(sv->i_ICaLf2))/ICaLf2tau);
    GlobalData_t diff_i_ICaLfca = ((ICaLfcainf-(sv->i_ICaLfca))/ICaLfcatau);
    GlobalData_t diff_i_IKrpa = ((IKrpainf-(sv->i_IKrpa))/IKrpatau);
    GlobalData_t diff_i_IKsn = ((IKsninf-(sv->i_IKsn))/IKsntau);
    GlobalData_t diff_i_INam = ((INaminf-(sv->i_INam))/INamtau);
    GlobalData_t diff_i_Ify = ((Ifyinf-(sv->i_Ify))/Ifytau);
    GlobalData_t diff_i_Isusr = ((Isusrinf-(sv->i_Isusr))/Isusrtau);
    GlobalData_t diff_i_Isuss = ((Isussinf-(sv->i_Isuss))/Isusstau);
    GlobalData_t diff_i_Itr = ((Itrinf-(sv->i_Itr))/Itrtau);
    GlobalData_t diff_i_Its = ((Itsinf-(sv->i_Its))/Itstau);
    GlobalData_t diff_i_RyRa1 = ((RyRainf1-(sv->i_RyRa1))/RyRtauadapt);
    GlobalData_t diff_i_RyRa2 = ((RyRainf2-(sv->i_RyRa2))/RyRtauadapt);
    GlobalData_t diff_i_RyRa3 = ((RyRainf3-(sv->i_RyRa3))/RyRtauadapt);
    GlobalData_t diff_i_RyRass = ((RyRainfss-(sv->i_RyRass))/RyRtauadapt);
    GlobalData_t diff_i_RyRc1 = ((RyRcinf1-(sv->i_RyRc1))/RyRtauinact);
    GlobalData_t diff_i_RyRc2 = ((RyRcinf2-(sv->i_RyRc2))/RyRtauinact);
    GlobalData_t diff_i_RyRc3 = ((RyRcinf3-(sv->i_RyRc3))/RyRtauinact);
    GlobalData_t diff_i_RyRcss = ((RyRcinfss-(sv->i_RyRcss))/RyRtauinactss);
    GlobalData_t diff_i_RyRo1 = ((RyRoinf1-(sv->i_RyRo1))/RyRtauact);
    GlobalData_t diff_i_RyRo2 = ((RyRoinf2-(sv->i_RyRo2))/RyRtauact);
    GlobalData_t diff_i_RyRo3 = ((RyRoinf3-(sv->i_RyRo3))/RyRtauact);
    GlobalData_t diff_i_RyRoss = ((RyRoinfss-(sv->i_RyRoss))/RyRtauactss);
    GlobalData_t diff_i_SERCACa1 = ((0.5*((-J_SERCASR1)+J_bulkSERCA1))/Vnonjunct1);
    GlobalData_t diff_i_SERCACa2 = ((0.5*((-J_SERCASR2)+J_bulkSERCA2))/Vnonjunct2);
    GlobalData_t diff_i_SERCACa3 = ((0.5*((-J_SERCASR3)+J_bulkSERCA3))/Vnonjunct3);
    GlobalData_t diff_i_SERCACass = ((0.5*((-J_SERCASRss)+J_bulkSERCAss))/Vss);
    GlobalData_t JCa1 = (((-J_bulkSERCA1)+JSRCaleak1)+Jrel1);
    GlobalData_t JCa2 = (((-J_bulkSERCA2)+JSRCaleak2)+Jrel2);
    GlobalData_t JCa3 = (((-J_bulkSERCA3)+JSRCaleak3)+Jrel3);
    GlobalData_t JCass = ((((-Jj_nj)+JSRCaleakss)-(J_bulkSERCAss))+Jrelss);
    GlobalData_t JSRCa1 = ((J_SERCASR1-(JSRCaleak1))-(Jrel1));
    GlobalData_t JSRCa2 = ((J_SERCASR2-(JSRCaleak2))-(Jrel2));
    GlobalData_t JSRCa3 = ((J_SERCASR3-(JSRCaleak3))-(Jrel3));
    GlobalData_t JSRCa4 = ((J_SERCASRss-(JSRCaleakss))-(Jrelss));
    GlobalData_t diff_Cai4 = ((((betai4*(DCa+(gammai4*DCaBm)))*((((sv->Cai4-((2.*sv->Cai4)))+sv->Cai3)/(pow(dx,2.)))+((sv->Cai4-(sv->Cai3))/((2.*4.)*(pow(dx,2.))))))-((((((2.*betai4)*gammai4)*DCaBm)/(KdBCa+sv->Cai4))*(pow(((sv->Cai4-(sv->Cai3))/(2.*dx)),2.)))))+((JCa4/Vnonjunct4)*betai4));
    GlobalData_t diff_i_INah1 = ((INah1inf-(sv->i_INah1))/INah1tau);
    GlobalData_t diff_i_INah2 = ((INah2inf-(sv->i_INah2))/INah2tau);
    GlobalData_t diff_CaSR1 = (((betaSR1*DCaSR)*((((sv->CaSR2-((2.*sv->CaSR1)))+sv->CaSR1)/(pow(dx,2.)))+((sv->CaSR2-(sv->CaSR1))/(2.*(pow(dx,2.))))))+((JSRCa1/VSR1)*betaSR1));
    GlobalData_t diff_CaSR2 = (((betaSR2*DCaSR)*((((sv->CaSR3-((2.*sv->CaSR2)))+sv->CaSR1)/(pow(dx,2.)))+((sv->CaSR3-(sv->CaSR1))/((2.*2.)*(pow(dx,2.))))))+((JSRCa2/VSR2)*betaSR2));
    GlobalData_t diff_CaSR3 = (((betaSR3*DCaSR)*((((sv->CaSR4-((2.*sv->CaSR3)))+sv->CaSR2)/(pow(dx,2.)))+((sv->CaSR4-(sv->CaSR2))/((2.*3.)*(pow(dx,2.))))))+((JSRCa3/VSR3)*betaSR3));
    GlobalData_t diff_CaSR4 = (((betaSR4*DCaSR)*((((sv->CaSR4-((2.*sv->CaSR4)))+sv->CaSR3)/(pow(dx,2.)))+((sv->CaSR4-(sv->CaSR3))/((2.*4.)*(pow(dx,2.))))))+((JSRCa4/VSR4)*betaSR4));
    GlobalData_t diff_Cai1 = ((((betai1*(DCa+(gammai1*DCaBm)))*((((sv->Cai2-((2.*sv->Cai1)))+sv->Cai1)/(pow(dx,2.)))+((sv->Cai2-(sv->Cai1))/(2.*(pow(dx,2.))))))-((((((2.*betai1)*gammai1)*DCaBm)/(KdBCa+sv->Cai1))*(pow(((sv->Cai2-(sv->Cai1))/(2.*dx)),2.)))))+((JCa1/Vnonjunct1)*betai1));
    GlobalData_t diff_Cai2 = ((((betai2*(DCa+(gammai2*DCaBm)))*((((sv->Cai3-((2.*sv->Cai2)))+sv->Cai1)/(pow(dx,2.)))+((sv->Cai3-(sv->Cai1))/((2.*2.)*(pow(dx,2.))))))-((((((2.*betai2)*gammai2)*DCaBm)/(KdBCa+sv->Cai2))*(pow(((sv->Cai3-(sv->Cai1))/(2.*dx)),2.)))))+((JCa2/Vnonjunct2)*betai2));
    GlobalData_t diff_Cai3 = ((((betai3*(DCa+(gammai3*DCaBm)))*((((sv->Cai4-((2.*sv->Cai3)))+sv->Cai2)/(pow(dx,2.)))+((sv->Cai4-(sv->Cai2))/((2.*3.)*(pow(dx,2.))))))-((((((2.*betai3)*gammai3)*DCaBm)/(KdBCa+sv->Cai3))*(pow(((sv->Cai4-(sv->Cai2))/(2.*dx)),2.)))))+((JCa3/Vnonjunct3)*betai3));
    GlobalData_t diff_Cass = (betass*((JCass/Vss)+(((((-ICaL)-(ICab))-(ICaP))+(2.*INaCa))/((2.*Vss)*F))));
    GlobalData_t CaSR1_new = sv->CaSR1+diff_CaSR1*dt;
    GlobalData_t CaSR2_new = sv->CaSR2+diff_CaSR2*dt;
    GlobalData_t CaSR3_new = sv->CaSR3+diff_CaSR3*dt;
    GlobalData_t CaSR4_new = sv->CaSR4+diff_CaSR4*dt;
    GlobalData_t Cai1_new = sv->Cai1+diff_Cai1*dt;
    GlobalData_t Cai2_new = sv->Cai2+diff_Cai2*dt;
    GlobalData_t Cai3_new = sv->Cai3+diff_Cai3*dt;
    GlobalData_t Cai4_new = sv->Cai4+diff_Cai4*dt;
    GlobalData_t Cass_new = sv->Cass+diff_Cass*dt;
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t Nass_new = sv->Nass+diff_Nass*dt;
    GlobalData_t i_ICaLd_new = sv->i_ICaLd+diff_i_ICaLd*dt;
    GlobalData_t i_ICaLf1_new = sv->i_ICaLf1+diff_i_ICaLf1*dt;
    GlobalData_t i_ICaLf2_new = sv->i_ICaLf2+diff_i_ICaLf2*dt;
    GlobalData_t i_ICaLfca_new = sv->i_ICaLfca+diff_i_ICaLfca*dt;
    GlobalData_t i_IKCa_O_new = sv->i_IKCa_O+diff_i_IKCa_O*dt;
    GlobalData_t i_IKrpa_new = sv->i_IKrpa+diff_i_IKrpa*dt;
    GlobalData_t i_IKsn_new = sv->i_IKsn+diff_i_IKsn*dt;
    GlobalData_t i_INah1_new = sv->i_INah1+diff_i_INah1*dt;
    GlobalData_t i_INah2_new = sv->i_INah2+diff_i_INah2*dt;
    GlobalData_t i_INam_new = sv->i_INam+diff_i_INam*dt;
    GlobalData_t i_Ify_new = sv->i_Ify+diff_i_Ify*dt;
    GlobalData_t i_Isusr_new = sv->i_Isusr+diff_i_Isusr*dt;
    GlobalData_t i_Isuss_new = sv->i_Isuss+diff_i_Isuss*dt;
    GlobalData_t i_Itr_new = sv->i_Itr+diff_i_Itr*dt;
    GlobalData_t i_Its_new = sv->i_Its+diff_i_Its*dt;
    GlobalData_t i_RyRa1_new = sv->i_RyRa1+diff_i_RyRa1*dt;
    GlobalData_t i_RyRa2_new = sv->i_RyRa2+diff_i_RyRa2*dt;
    GlobalData_t i_RyRa3_new = sv->i_RyRa3+diff_i_RyRa3*dt;
    GlobalData_t i_RyRass_new = sv->i_RyRass+diff_i_RyRass*dt;
    GlobalData_t i_RyRc1_new = sv->i_RyRc1+diff_i_RyRc1*dt;
    GlobalData_t i_RyRc2_new = sv->i_RyRc2+diff_i_RyRc2*dt;
    GlobalData_t i_RyRc3_new = sv->i_RyRc3+diff_i_RyRc3*dt;
    GlobalData_t i_RyRcss_new = sv->i_RyRcss+diff_i_RyRcss*dt;
    GlobalData_t i_RyRo1_new = sv->i_RyRo1+diff_i_RyRo1*dt;
    GlobalData_t i_RyRo2_new = sv->i_RyRo2+diff_i_RyRo2*dt;
    GlobalData_t i_RyRo3_new = sv->i_RyRo3+diff_i_RyRo3*dt;
    GlobalData_t i_RyRoss_new = sv->i_RyRoss+diff_i_RyRoss*dt;
    GlobalData_t i_SERCACa1_new = sv->i_SERCACa1+diff_i_SERCACa1*dt;
    GlobalData_t i_SERCACa2_new = sv->i_SERCACa2+diff_i_SERCACa2*dt;
    GlobalData_t i_SERCACa3_new = sv->i_SERCACa3+diff_i_SERCACa3*dt;
    GlobalData_t i_SERCACass_new = sv->i_SERCACass+diff_i_SERCACass*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->CaSR1 = CaSR1_new;
    sv->CaSR2 = CaSR2_new;
    sv->CaSR3 = CaSR3_new;
    sv->CaSR4 = CaSR4_new;
    sv->Cai1 = Cai1_new;
    sv->Cai2 = Cai2_new;
    sv->Cai3 = Cai3_new;
    sv->Cai4 = Cai4_new;
    sv->Cass = Cass_new;
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->Nass = Nass_new;
    sv->i_ICaLd = i_ICaLd_new;
    sv->i_ICaLf1 = i_ICaLf1_new;
    sv->i_ICaLf2 = i_ICaLf2_new;
    sv->i_ICaLfca = i_ICaLfca_new;
    sv->i_IKCa_O = i_IKCa_O_new;
    sv->i_IKrpa = i_IKrpa_new;
    sv->i_IKsn = i_IKsn_new;
    sv->i_INah1 = i_INah1_new;
    sv->i_INah2 = i_INah2_new;
    sv->i_INam = i_INam_new;
    sv->i_Ify = i_Ify_new;
    sv->i_Isusr = i_Isusr_new;
    sv->i_Isuss = i_Isuss_new;
    sv->i_Itr = i_Itr_new;
    sv->i_Its = i_Its_new;
    sv->i_RyRa1 = i_RyRa1_new;
    sv->i_RyRa2 = i_RyRa2_new;
    sv->i_RyRa3 = i_RyRa3_new;
    sv->i_RyRass = i_RyRass_new;
    sv->i_RyRc1 = i_RyRc1_new;
    sv->i_RyRc2 = i_RyRc2_new;
    sv->i_RyRc3 = i_RyRc3_new;
    sv->i_RyRcss = i_RyRcss_new;
    sv->i_RyRo1 = i_RyRo1_new;
    sv->i_RyRo2 = i_RyRo2_new;
    sv->i_RyRo3 = i_RyRo3_new;
    sv->i_RyRoss = i_RyRoss_new;
    sv->i_SERCACa1 = i_SERCACa1_new;
    sv->i_SERCACa2 = i_SERCACa2_new;
    sv->i_SERCACa3 = i_SERCACa3_new;
    sv->i_SERCACass = i_SERCACass_new;
    //Change the units of external variables as appropriate.
    Iion *= 1e-3;
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Vm_ext[__i] = Vm;

  }


}


void trace_Skibsbye(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Skibsbye_trace_header.txt","wt");
    fprintf(theader->fd,
        "Vm\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e-3;
  cell_geom *region = &IF->cgeom;
  Skibsbye_Params *p  = (Skibsbye_Params *)IF->params;

  Skibsbye_state *sv_base = (Skibsbye_state *)IF->sv_tab.y;
  Skibsbye_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Ddcell = (((p->cAF_lcell-(1.))*(20./10.))+1.);
  GlobalData_t PLB_SERCA_ratio = p->cAF_PLB;
  GlobalData_t SLN_SERCA_ratio = p->cAF_SLN;
  GlobalData_t base_phos = (0.1*p->cAF_phos);
  GlobalData_t gCaL = ((6.5*p->cAF_gCaL)*p->factorgcal);
  GlobalData_t lcell = (122.051*p->cAF_lcell);
  GlobalData_t Dacell = (p->cAF_lcell*Ddcell);
  GlobalData_t Dvcell = ((p->cAF_lcell*Ddcell)*Ddcell);
  GlobalData_t SERCAKmf = ((Kmf_PLBKO+((Kmf_PLB*PLB_SERCA_ratio)*(1.-(base_phos))))+((Kmf_SLN*SLN_SERCA_ratio)*(1.-(base_phos))));
  GlobalData_t SERCAKmr = ((Kmr_PLBKO-(((Kmr_PLB*PLB_SERCA_ratio)*(1.-(base_phos)))))-(((Kmr_SLN*SLN_SERCA_ratio)*(1.-(base_phos)))));
  GlobalData_t dx = (1.625*Ddcell);
  GlobalData_t rjunct = (6.5*Ddcell);
  GlobalData_t Aj_nj = ((((pi*rjunct)*2.)*lcell)*0.5);
  GlobalData_t Cm = (0.050*Dacell);
  GlobalData_t ICaPmax = (((pow(q10_CaP,q10exp))*2.0)*Dacell);
  GlobalData_t INaKmax = (((pow(q10_NKA,q10exp))*70.8253)*Dacell);
  GlobalData_t Vnonjunct1 = (((((pi*lcell)*1e-6)*0.5)*dx)*dx);
  GlobalData_t Vss = (4.99232e-5*Dvcell);
  GlobalData_t cpumps = ((30e-3/Dvcell)*p->cAF_cpumps);
  GlobalData_t gCab = (0.078*Dacell);
  GlobalData_t gIf = Dacell;
  GlobalData_t gK1 = (((2.8*p->cAF_gK1)*Dacell)*p->factorgk1);
  GlobalData_t gKACh = ((6.375/(1.+(pow((EC50_ACh/p->ACh),hill_ACh))))*Dacell);
  GlobalData_t gKCa = ((3.6*p->cAF_gKCa)*Dacell);
  GlobalData_t gKr = (((5.2*(sqrt((Ko/5.4))))*Dacell)*p->factorgkr);
  GlobalData_t gKs = (((0.175*p->cAF_gKs)*Dacell)*p->factorgks);
  GlobalData_t gNa = (((0.9*640.)*p->cAF_gNa)*Dacell);
  GlobalData_t gNaL = ((0.9*0.39)*Dacell);
  GlobalData_t gNab = (0.060599*Dacell);
  GlobalData_t gsus = ((2.25*p->cAF_gsus)*Dacell);
  GlobalData_t gt = (((11.*p->cAF_gt)*Dacell)*p->factorgto);
  GlobalData_t k2 = (k1*(SERCAKmf*SERCAKmf));
  GlobalData_t k3 = (k4/(SERCAKmr*SERCAKmr));
  GlobalData_t kNaCa = ((((pow(q10_NCX,q10exp))*0.0081)*p->cAF_kNaCa)*Dacell);
  GlobalData_t xj_nj = (((0.02/2.)*Ddcell)+(dx/2.));
  GlobalData_t xj_nj_Nai = (((0.02/2.)*Ddcell)+(2.*dx));
  GlobalData_t VSR1 = ((((0.05*Vnonjunct1)/2.)*0.9)/Dvcell);
  GlobalData_t Vcytosol = ((16.*Vnonjunct1)+Vss);
  GlobalData_t Vnonjunct2 = (3.*Vnonjunct1);
  GlobalData_t Vnonjunct3 = (5.*Vnonjunct1);
  GlobalData_t Vnonjunct4 = (7.*Vnonjunct1);
  GlobalData_t Vnonjunct_Nai = (16.*Vnonjunct1);
  GlobalData_t nu1 = ((1.6*Vnonjunct1)/Dvcell);
  GlobalData_t nuss = ((900.*Vss)/Dvcell);
  GlobalData_t VSR2 = ((((0.05*Vnonjunct2)/2.)*0.9)/Dvcell);
  GlobalData_t VSR3 = ((((0.05*Vnonjunct3)/2.)*0.9)/Dvcell);
  GlobalData_t VSR4 = ((((0.05*Vnonjunct4)/2.)*0.9)/Dvcell);
  GlobalData_t nu2 = ((1.6*Vnonjunct2)/Dvcell);
  GlobalData_t nu3 = ((1.6*Vnonjunct3)/Dvcell);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vm_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t Vm = Vm_ext[__i];
  //Change the units of external variables as appropriate.
  Iion *= 1e3;
  
  
  //Output the desired variables
  fprintf(file, "%4.12f\t", Vm);
  //Change the units of external variables as appropriate.
  Iion *= 1e-3;
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        