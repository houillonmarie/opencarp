// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Grandi E, Pasqualini FS & Bers DM
*  Year: 2010
*  Title: A novel computationalmodel of the human ventricular action potential and Ca transient
*  Journal: JMol Cell Cardiol 48, 112-121
*  DOI: 10.1016/j.yjmcc.2009.09.019
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __GRANDI_H__
#define __GRANDI_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Grandi_REQDAT Vm_DATA_FLAG
#define Grandi_MODDAT Iion_DATA_FLAG

struct Grandi_Params {
    GlobalData_t Cai_init;
    GlobalData_t Cao;
    GlobalData_t Cli;
    GlobalData_t Clo;
    GlobalData_t Ki_init;
    GlobalData_t Ko;
    GlobalData_t Mgi;
    GlobalData_t Nai_init;
    GlobalData_t Nao;
    GlobalData_t cell_type;
    GlobalData_t markov_iks;
    char* flags;

};
static const char* Grandi_flags = "EPI|MCELL|ENDO";


struct Grandi_state {
    GlobalData_t C1;
    GlobalData_t C10;
    GlobalData_t C11;
    GlobalData_t C12;
    GlobalData_t C13;
    GlobalData_t C14;
    GlobalData_t C15;
    GlobalData_t C2;
    GlobalData_t C3;
    GlobalData_t C4;
    GlobalData_t C5;
    GlobalData_t C6;
    GlobalData_t C7;
    GlobalData_t C8;
    GlobalData_t C9;
    GlobalData_t CaM;
    GlobalData_t Ca_sr;
    GlobalData_t Cai;
    GlobalData_t Caj;
    GlobalData_t Casl;
    GlobalData_t Csqnb;
    GlobalData_t Ki;
    GlobalData_t Myoc;
    GlobalData_t Myom;
    GlobalData_t NaBj;
    GlobalData_t NaBsl;
    GlobalData_t Nai;
    GlobalData_t Naj;
    GlobalData_t Nasl;
    GlobalData_t O1;
    GlobalData_t RyRi;
    GlobalData_t RyRo;
    GlobalData_t RyRr;
    GlobalData_t SLHj;
    GlobalData_t SLHsl;
    GlobalData_t SLLj;
    GlobalData_t SLLsl;
    GlobalData_t SRB;
    GlobalData_t TnCHc;
    GlobalData_t TnCHm;
    GlobalData_t TnCL;
    Gatetype d;
    Gatetype f;
    GlobalData_t fcaBj;
    GlobalData_t fcaBsl;
    Gatetype h;
    Gatetype j;
    Gatetype m;
    Gatetype xkr;
    Gatetype xks;
    Gatetype xtof;
    Gatetype xtos;
    Gatetype ytof;
    Gatetype ytos;
    GlobalData_t __Cai_mM_local;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Grandi(ION_IF *);
void construct_tables_Grandi(ION_IF *);
void destroy_Grandi(ION_IF *);
void initialize_sv_Grandi(ION_IF *, GlobalData_t**);
void compute_Grandi(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
