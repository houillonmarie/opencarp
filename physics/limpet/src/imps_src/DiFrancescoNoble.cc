// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Di Francesco D. and Noble Denis
*  Year: 1985
*  Title: A model of cardiac electrICal actiVolity incorporating ionic pumps and concentration changes
*  Journal: Phil. Trans. R. Soc. Lond. B307353-398
*  DOI: 10.1098/rstb.1985.0001
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "DiFrancescoNoble.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_DiFrancescoNoble(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_DiFrancescoNoble( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define Cai_init (GlobalData_t)(0.00005)
#define Cao (GlobalData_t)(2.)
#define Carel_init (GlobalData_t)(1.)
#define Caup_bar (GlobalData_t)(5.)
#define Caup_init (GlobalData_t)(2.)
#define F (GlobalData_t)(96.487)
#define GK1 (GlobalData_t)(920.)
#define GbCa (GlobalData_t)(0.02)
#define GbNa (GlobalData_t)(0.18)
#define GfK (GlobalData_t)(3.)
#define GfNa (GlobalData_t)(3.)
#define IKmax (GlobalData_t)(180.)
#define Ipmax (GlobalData_t)(125.)
#define Kact4 (GlobalData_t)(0.0005)
#define Kb (GlobalData_t)(4.)
#define Kc_init (GlobalData_t)(4.)
#define Ki_init (GlobalData_t)(140.)
#define Km1 (GlobalData_t)(210.)
#define KmCa (GlobalData_t)(.001)
#define KmK (GlobalData_t)(1.)
#define KmNa (GlobalData_t)(40.)
#define Kmf (GlobalData_t)(45.)
#define Kmf2 (GlobalData_t)(.001)
#define Kmto (GlobalData_t)(10.)
#define Nai_init (GlobalData_t)(8.)
#define Nao (GlobalData_t)(140.)
#define P (GlobalData_t)(0.7)
#define Psi (GlobalData_t)(15.)
#define R (GlobalData_t)(8.3143)
#define SMALL (GlobalData_t)(10e-18)
#define T (GlobalData_t)(307.73)
#define V_DFN_0 (GlobalData_t)(-75.)
#define alpha_f2 (GlobalData_t)((10./1000.))
#define bb_Carel (GlobalData_t)(0.)
#define bb_Catr (GlobalData_t)(0.)
#define bb_Caup (GlobalData_t)(0.)
#define dNaCa (GlobalData_t)(0.001)
#define fVolecs (GlobalData_t)(0.1)
#define gamma (GlobalData_t)(0.5)
#define length (GlobalData_t)(0.2)
#define nNaCa (GlobalData_t)(3.)
#define radius (GlobalData_t)(0.005)
#define rr (GlobalData_t)(2.)
#define tau_rel (GlobalData_t)(0.050)
#define tau_rep (GlobalData_t)(2.)
#define tau_up (GlobalData_t)(0.025)
#define FRT (GlobalData_t)(((F/R)/T))
#define RTF (GlobalData_t)(((R*T)/F))
#define V_init (GlobalData_t)(V_DFN_0)
#define Voli (GlobalData_t)((((((1.-(fVolecs))*3.14159)*radius)*radius)*length))
#define surface_area (GlobalData_t)((((2.*3.14159)*radius)*length))
#define Vole (GlobalData_t)(((Voli*fVolecs)/(1.-(fVolecs))))
#define Volrel (GlobalData_t)((.02*Voli))
#define Volup (GlobalData_t)((.05*Voli))
#define aa_Caup (GlobalData_t)((((2.e6*F)*Voli)/(tau_up*Caup_bar)))
#define exp50FRT (GlobalData_t)((exp((50.*FRT))))
#define aa_Carel (GlobalData_t)((((2.e6*F)*Volrel)/tau_rel))
#define aa_Catr (GlobalData_t)((((2.e6*F)*Volrel)/tau_rep))



void initialize_params_DiFrancescoNoble( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  DiFrancescoNoble_Params *p = (DiFrancescoNoble_Params *)IF->params;

  // Compute the regional constants
  {
    p->Cm = 12.;
    p->GCaf = 1.;
    p->GNa = 750.;
    p->Gto = 1.;
    p->factorIfunny = 1.;
    p->kNaCa = 0.02;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_DiFrancescoNoble;

}


// Define the parameters for the lookup tables
enum Tables {
  V_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum V_TableIndex {
  d_rush_larsen_A_idx,
  d_rush_larsen_B_idx,
  f_rush_larsen_A_idx,
  f_rush_larsen_B_idx,
  h_rush_larsen_A_idx,
  h_rush_larsen_B_idx,
  m_rush_larsen_A_idx,
  m_rush_larsen_B_idx,
  pp_rush_larsen_A_idx,
  pp_rush_larsen_B_idx,
  r_rush_larsen_A_idx,
  r_rush_larsen_B_idx,
  vrow_19_idx,
  vrow_20_idx,
  vrow_21_idx,
  vrow_22_idx,
  vrow_23_idx,
  vrow_24_idx,
  vrow_25_idx,
  x_rush_larsen_A_idx,
  x_rush_larsen_B_idx,
  y_rush_larsen_A_idx,
  y_rush_larsen_B_idx,
  NROWS_V
};



void construct_tables_DiFrancescoNoble( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  DiFrancescoNoble_Params *p = (DiFrancescoNoble_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double C = (p->Cm*surface_area);
  
  // Create the V lookup table
  LUT* V_tab = &IF->tables[V_TAB];
  LUT_alloc(V_tab, NROWS_V, -1000, 1000, 0.05, "DiFrancescoNoble V");
  for (int __i=V_tab->mn_ind; __i<=V_tab->mx_ind; __i++) {
    double V = V_tab->res*__i;
    LUT_data_t* V_row = V_tab->tab[__i];
    double aa_h = (20.*(exp((-.125*(V+75.)))));
    double alpha_d = (((fabs((V+24.)))<.001) ? (120./1000.) : (((-30.*(V+24.))/(expm1(((-(V+24.))/4.))))/1000.));
    double alpha_m = (((fabs((V+41.)))<.001) ? (2000./1000.) : (((200.*(V+41.))/(1.-((exp((-.1*(V+41.)))))))/1000.));
    double alpha_r = ((.033*(exp(((-V)/17.))))/1000.);
    double alpha_x = (((.5*(exp((.0826*(V+50.)))))/(1.+(exp((.057*(V+50.))))))/1000.);
    double alpha_y = ((.025*(exp((-.067*(V+52.)))))/1000.);
    double bb_h = (2000./(1.+(320.*(exp((-.1*(V+75.)))))));
    double beta_d = (((fabs((V+24.)))<.001) ? (120./1000.) : (((12.*(V+24.))/(expm1(((V+24.)/10.))))/1000.));
    double beta_m = ((8000.*(exp((-.056*(V+66.)))))/1000.);
    double beta_r = ((33./(1.+(exp(((-(V+10.))/8.)))))/1000.);
    double beta_x = (((1.3*(exp((-.06*(V+20.)))))/(1.+(exp((-.04*(V+20.))))))/1000.);
    double beta_y = (((fabs((V+52.)))<.001) ? (2.5/1000.) : (((0.5*(V+52.))/(1.-((exp((-.2*(V+52.)))))))/1000.));
    double f_tmp = (V+34.);
    double p_tmp = (V+34.);
    V_row[vrow_19_idx] = (exp(((-V)*FRT)));
    V_row[vrow_20_idx] = (exp((.02*V)));
    V_row[vrow_22_idx] = ((V!=50.) ? ((((-4.*Psi)*(V-(50.)))*FRT)/(expm1((((-(V-(50.)))*2.)*FRT)))) : (2.*Psi));
    V_row[vrow_23_idx] = ((V!=50.) ? ((((-0.001*Psi)*(V-(50.)))*FRT)/(expm1(((-(V-(50.)))*FRT)))) : (0.001*Psi));
    V_row[vrow_24_idx] = (exp(((-(V-(50.)))*FRT)));
    V_row[vrow_25_idx] = (exp(((((gamma*(nNaCa-(2.)))*V)*FRT)/2.)));
    double vterm1 = (((fabs((V+10.)))<0.001) ? 5. : ((V+10.)/(1.-((exp((-.2*(V+10.))))))));
    double alpha_f = (((fabs(f_tmp))<.001) ? (25./1000.) : (((6.25*f_tmp)/(expm1((f_tmp/4.))))/1000.));
    double alpha_pp = (((fabs(p_tmp))<0.001) ? ((4.*60.25)/1000.) : (((62.5*p_tmp)/(expm1((p_tmp/4.))))/1000.));
    double beta_f = ((50./(1.+(exp(((-f_tmp)/4.)))))/1000.);
    double beta_pp = ((500./(1.+(exp(((-p_tmp)/4.)))))/1000.);
    V_row[d_rush_larsen_A_idx] = (((-alpha_d)/(alpha_d+beta_d))*(expm1(((-dt)*(alpha_d+beta_d)))));
    V_row[d_rush_larsen_B_idx] = (exp(((-dt)*(alpha_d+beta_d))));
    double h_inf = (1.-((bb_h/(aa_h+bb_h))));
    V_row[m_rush_larsen_A_idx] = (((-alpha_m)/(alpha_m+beta_m))*(expm1(((-dt)*(alpha_m+beta_m)))));
    V_row[m_rush_larsen_B_idx] = (exp(((-dt)*(alpha_m+beta_m))));
    V_row[r_rush_larsen_A_idx] = (((-alpha_r)/(alpha_r+beta_r))*(expm1(((-dt)*(alpha_r+beta_r)))));
    V_row[r_rush_larsen_B_idx] = (exp(((-dt)*(alpha_r+beta_r))));
    double tau_h = (1000./(aa_h+bb_h));
    V_row[vrow_21_idx] = vterm1;
    V_row[x_rush_larsen_A_idx] = (((-alpha_x)/(alpha_x+beta_x))*(expm1(((-dt)*(alpha_x+beta_x)))));
    V_row[x_rush_larsen_B_idx] = (exp(((-dt)*(alpha_x+beta_x))));
    V_row[y_rush_larsen_A_idx] = (((-alpha_y)/(alpha_y+beta_y))*(expm1(((-dt)*(alpha_y+beta_y)))));
    V_row[y_rush_larsen_B_idx] = (exp(((-dt)*(alpha_y+beta_y))));
    V_row[f_rush_larsen_A_idx] = (((-alpha_f)/(alpha_f+beta_f))*(expm1(((-dt)*(alpha_f+beta_f)))));
    V_row[f_rush_larsen_B_idx] = (exp(((-dt)*(alpha_f+beta_f))));
    V_row[h_rush_larsen_B_idx] = (exp(((-dt)/tau_h)));
    double h_rush_larsen_C = (expm1(((-dt)/tau_h)));
    V_row[pp_rush_larsen_A_idx] = (((-alpha_pp)/(alpha_pp+beta_pp))*(expm1(((-dt)*(alpha_pp+beta_pp)))));
    V_row[pp_rush_larsen_B_idx] = (exp(((-dt)*(alpha_pp+beta_pp))));
    V_row[h_rush_larsen_A_idx] = ((-h_inf)*h_rush_larsen_C);
  }
  check_LUT(V_tab);
  

}



void    initialize_sv_DiFrancescoNoble( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  DiFrancescoNoble_Params *p = (DiFrancescoNoble_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(DiFrancescoNoble_state) );
  DiFrancescoNoble_state *sv_base = (DiFrancescoNoble_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double C = (p->Cm*surface_area);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    DiFrancescoNoble_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Cai = Cai_init;
    sv->Carel = Carel_init;
    sv->Caup = Caup_init;
    sv->Kc = Kc_init;
    sv->Ki = Ki_init;
    sv->Nai = Nai_init;
    V = V_init;
    double ECa = ((RTF*(log((Cao/sv->Cai))))/2.);
    double EK = (RTF*(log((sv->Kc/sv->Ki))));
    double ENa = (RTF*(log((Nao/sv->Nai))));
    double Emh = (RTF*(log(((Nao+(.12*sv->Kc))/(sv->Nai+(.12*sv->Ki))))));
    double Ip = ((Ipmax*(sv->Kc/(KmK+sv->Kc)))*(sv->Nai/(KmNa+sv->Nai)));
    double aa_h = (20.*(exp((-.125*(V+75.)))));
    double alpha_d = (((fabs((V+24.)))<.001) ? (120./1000.) : (((-30.*(V+24.))/(expm1(((-(V+24.))/4.))))/1000.));
    double alpha_m = (((fabs((V+41.)))<.001) ? (2000./1000.) : (((200.*(V+41.))/(1.-((exp((-.1*(V+41.)))))))/1000.));
    double alpha_r = ((.033*(exp(((-V)/17.))))/1000.);
    double alpha_x = (((.5*(exp((.0826*(V+50.)))))/(1.+(exp((.057*(V+50.))))))/1000.);
    double alpha_y = ((.025*(exp((-.067*(V+52.)))))/1000.);
    double bb_h = (2000./(1.+(320.*(exp((-.1*(V+75.)))))));
    double beta_d = (((fabs((V+24.)))<.001) ? (120./1000.) : (((12.*(V+24.))/(expm1(((V+24.)/10.))))/1000.));
    double beta_f2 = ((alpha_f2*sv->Cai)/Kmf2);
    double beta_m = ((8000.*(exp((-.056*(V+66.)))))/1000.);
    double beta_r = ((33./(1.+(exp(((-(V+10.))/8.)))))/1000.);
    double beta_x = (((1.3*(exp((-.06*(V+20.)))))/(1.+(exp((-.04*(V+20.))))))/1000.);
    double beta_y = (((fabs((V+52.)))<.001) ? (2.5/1000.) : (((0.5*(V+52.))/(1.-((exp((-.2*(V+52.)))))))/1000.));
    double f_tmp = (V+34.);
    double p_tmp = (V+34.);
    double vrow_19 = (exp(((-V)*FRT)));
    double vrow_20 = (exp((.02*V)));
    double vrow_22 = ((V!=50.) ? ((((-4.*Psi)*(V-(50.)))*FRT)/(expm1((((-(V-(50.)))*2.)*FRT)))) : (2.*Psi));
    double vrow_23 = ((V!=50.) ? ((((-0.001*Psi)*(V-(50.)))*FRT)/(expm1(((-(V-(50.)))*FRT)))) : (0.001*Psi));
    double vrow_24 = (exp(((-(V-(50.)))*FRT)));
    double vrow_25 = (exp(((((gamma*(nNaCa-(2.)))*V)*FRT)/2.)));
    double vterm1 = (((fabs((V+10.)))<0.001) ? 5. : ((V+10.)/(1.-((exp((-.2*(V+10.))))))));
    double IK1 = ((GK1*(sv->Kc/(sv->Kc+Km1)))*((V-(EK))/(1.+(exp(((2.*FRT)*((V-(EK))+10.)))))));
    double INaCa = (((p->kNaCa*vrow_25)*((((sv->Nai*sv->Nai)*sv->Nai)*Cao)-(((vrow_19*((Nao*Nao)*Nao))*sv->Cai))))/(1.+(dNaCa*((sv->Cai*((Nao*Nao)*Nao))+(Cao*((sv->Nai*sv->Nai)*sv->Nai))))));
    double IbCa = (GbCa*(V-(ECa)));
    double IbNa = (GbNa*(V-(ENa)));
    double alpha_f = (((fabs(f_tmp))<.001) ? (25./1000.) : (((6.25*f_tmp)/(expm1((f_tmp/4.))))/1000.));
    double alpha_pp = (((fabs(p_tmp))<0.001) ? ((4.*60.25)/1000.) : (((62.5*p_tmp)/(expm1((p_tmp/4.))))/1000.));
    double beta_f = ((50./(1.+(exp(((-f_tmp)/4.)))))/1000.);
    double beta_pp = ((500./(1.+(exp(((-p_tmp)/4.)))))/1000.);
    double d_init = (alpha_d/(alpha_d+beta_d));
    double f2_init = (alpha_f2/(alpha_f2+beta_f2));
    double h_inf = (1.-((bb_h/(aa_h+bb_h))));
    double m_init = (alpha_m/(alpha_m+beta_m));
    double r_init = (alpha_r/(alpha_r+beta_r));
    double vrow_21 = vterm1;
    double x_init = (alpha_x/(alpha_x+beta_x));
    double y_init = (alpha_y/(alpha_y+beta_y));
    sv->d = d_init;
    sv->f2 = f2_init;
    double f_init = (alpha_f/(alpha_f+beta_f));
    double h_init = h_inf;
    sv->m = m_init;
    double pp_init = (alpha_pp/(alpha_pp+beta_pp));
    sv->r = r_init;
    sv->x = x_init;
    sv->y = y_init;
    double IK = (((sv->x*IKmax)*(sv->Ki-((sv->Kc*vrow_19))))/140.);
    double If = (((p->factorIfunny*sv->y)*(sv->Kc/(sv->Kc+Kmf)))*((GfK*(V-(EK)))+(GfNa*(V-(ENa)))));
    double Ito = (((((((2.*sv->r)*0.28)*((0.2+sv->Kc)/(Kmto+sv->Kc)))*(sv->Cai/(Kact4+sv->Cai)))*vrow_21)*((sv->Ki*vrow_20)-((sv->Kc/vrow_20))))*p->Gto);
    sv->f = f_init;
    sv->h = h_init;
    sv->pp = pp_init;
    double INa = (((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*(V-(Emh)));
    double isICa = (((fabs((V-(50.))))<.001) ? ((((sv->Cai*exp50FRT)*exp50FRT)-(((Cao*vrow_24)*vrow_24)))*(2.*Psi)) : ((((sv->Cai*exp50FRT)*exp50FRT)-(((Cao*vrow_24)*vrow_24)))*(((sv->d*sv->f)*sv->f2)*vrow_22)));
    double isIK = (((fabs((V-(50.))))<.001) ? (((sv->Ki*exp50FRT)-((sv->Kc*vrow_24)))*(.01*Psi)) : (((sv->Ki*exp50FRT)-((sv->Kc*vrow_24)))*(((sv->d*sv->f)*sv->f2)*vrow_23)));
    double isINa = (((fabs((V-(50.))))<.001) ? (((sv->Nai*exp50FRT)-((Nao*vrow_24)))*(.01*Psi)) : (((sv->Nai*exp50FRT)-((Nao*vrow_24)))*(((sv->d*sv->f)*sv->f2)*vrow_23)));
    double ICa = (p->GCaf*((isICa+isIK)+isINa));
    Iion = (((((((((((If+IK)+IK1)+Ito)+IbNa)+IbCa)+Ip)+INaCa)+INa)+ICa)/C)/1.e3);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_DiFrancescoNoble(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  DiFrancescoNoble_Params *p  = (DiFrancescoNoble_Params *)IF->params;
  DiFrancescoNoble_state *sv_base = (DiFrancescoNoble_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t C = (p->Cm*surface_area);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    DiFrancescoNoble_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t V_row[NROWS_V];
    LUT_interpRow(&IF->tables[V_TAB], V, __i, V_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t ECa = ((RTF*(log((Cao/sv->Cai))))/2.);
    GlobalData_t EK = (RTF*(log((sv->Kc/sv->Ki))));
    GlobalData_t ENa = (RTF*(log((Nao/sv->Nai))));
    GlobalData_t Emh = (RTF*(log(((Nao+(.12*sv->Kc))/(sv->Nai+(.12*sv->Ki))))));
    GlobalData_t Ip = ((Ipmax*(sv->Kc/(KmK+sv->Kc)))*(sv->Nai/(KmNa+sv->Nai)));
    GlobalData_t IK = (((sv->x*IKmax)*(sv->Ki-((sv->Kc*V_row[vrow_19_idx]))))/140.);
    GlobalData_t IK1 = ((GK1*(sv->Kc/(sv->Kc+Km1)))*((V-(EK))/(1.+(exp(((2.*FRT)*((V-(EK))+10.)))))));
    GlobalData_t INa = (((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*(V-(Emh)));
    GlobalData_t INaCa = (((p->kNaCa*V_row[vrow_25_idx])*((((sv->Nai*sv->Nai)*sv->Nai)*Cao)-(((V_row[vrow_19_idx]*((Nao*Nao)*Nao))*sv->Cai))))/(1.+(dNaCa*((sv->Cai*((Nao*Nao)*Nao))+(Cao*((sv->Nai*sv->Nai)*sv->Nai))))));
    GlobalData_t IbCa = (GbCa*(V-(ECa)));
    GlobalData_t IbNa = (GbNa*(V-(ENa)));
    GlobalData_t If = (((p->factorIfunny*sv->y)*(sv->Kc/(sv->Kc+Kmf)))*((GfK*(V-(EK)))+(GfNa*(V-(ENa)))));
    GlobalData_t isICa = (((fabs((V-(50.))))<.001) ? ((((sv->Cai*exp50FRT)*exp50FRT)-(((Cao*V_row[vrow_24_idx])*V_row[vrow_24_idx])))*(2.*Psi)) : ((((sv->Cai*exp50FRT)*exp50FRT)-(((Cao*V_row[vrow_24_idx])*V_row[vrow_24_idx])))*(((sv->d*sv->f)*sv->f2)*V_row[vrow_22_idx])));
    GlobalData_t isIK = (((fabs((V-(50.))))<.001) ? (((sv->Ki*exp50FRT)-((sv->Kc*V_row[vrow_24_idx])))*(.01*Psi)) : (((sv->Ki*exp50FRT)-((sv->Kc*V_row[vrow_24_idx])))*(((sv->d*sv->f)*sv->f2)*V_row[vrow_23_idx])));
    GlobalData_t isINa = (((fabs((V-(50.))))<.001) ? (((sv->Nai*exp50FRT)-((Nao*V_row[vrow_24_idx])))*(.01*Psi)) : (((sv->Nai*exp50FRT)-((Nao*V_row[vrow_24_idx])))*(((sv->d*sv->f)*sv->f2)*V_row[vrow_23_idx])));
    GlobalData_t ICa = (p->GCaf*((isICa+isIK)+isINa));
    GlobalData_t Ito = (((((((2.*sv->r)*0.28)*((0.2+sv->Kc)/(Kmto+sv->Kc)))*(sv->Cai/(Kact4+sv->Cai)))*V_row[vrow_21_idx])*((sv->Ki*V_row[vrow_20_idx])-((sv->Kc/V_row[vrow_20_idx]))))*p->Gto);
    Iion = (((((((((((If+IK)+IK1)+Ito)+IbNa)+IbCa)+Ip)+INaCa)+INa)+ICa)/C)/1.e3);
    
    
    //Complete Forward Euler Update
    GlobalData_t Irel = (((aa_Carel*sv->Carel)*(sv->Cai*sv->Cai))/((sv->Cai*sv->Cai)+(KmCa*KmCa)));
    GlobalData_t Itr = ((aa_Catr*sv->pp)*(sv->Caup-(sv->Carel)));
    GlobalData_t Iup = (((aa_Caup*sv->Cai)*(Caup_bar-(sv->Caup)))-((bb_Caup*sv->Caup)));
    GlobalData_t diff_Nai = (((-((((INa+IbNa)+(If/2.))+(3.*Ip))+((nNaCa/(nNaCa-(2.)))*INaCa)))*1.e-9)/(Voli*F));
    GlobalData_t imK = (((IK1+IK)+(If/2.))-((2.*Ip)));
    GlobalData_t diff_Carel = (((Itr-(Irel))/((4.e6*Volrel)*F))/1000.);
    GlobalData_t diff_Caup = (((Iup-(Itr))/((4.e6*Volup)*F))/1000.);
    GlobalData_t diff_Kc = ((((-P)*(sv->Kc-(Kb)))+(imK/((1.e6*Vole)*F)))/1000.);
    GlobalData_t diff_Ki = (((-imK)/((1.e6*Vole)*F))/1000.);
    GlobalData_t Carel_new = sv->Carel+diff_Carel*dt;
    GlobalData_t Caup_new = sv->Caup+diff_Caup*dt;
    GlobalData_t Kc_new = sv->Kc+diff_Kc*dt;
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t beta_f2 = ((alpha_f2*sv->Cai)/Kmf2);
    GlobalData_t d_rush_larsen_A = V_row[d_rush_larsen_A_idx];
    GlobalData_t d_rush_larsen_B = V_row[d_rush_larsen_B_idx];
    GlobalData_t f2_rush_larsen_A = (((-alpha_f2)/(alpha_f2+beta_f2))*(expm1(((-dt)*(alpha_f2+beta_f2)))));
    GlobalData_t f2_rush_larsen_B = (exp(((-dt)*(alpha_f2+beta_f2))));
    GlobalData_t m_rush_larsen_A = V_row[m_rush_larsen_A_idx];
    GlobalData_t m_rush_larsen_B = V_row[m_rush_larsen_B_idx];
    GlobalData_t r_rush_larsen_A = V_row[r_rush_larsen_A_idx];
    GlobalData_t r_rush_larsen_B = V_row[r_rush_larsen_B_idx];
    GlobalData_t x_rush_larsen_A = V_row[x_rush_larsen_A_idx];
    GlobalData_t x_rush_larsen_B = V_row[x_rush_larsen_B_idx];
    GlobalData_t y_rush_larsen_A = V_row[y_rush_larsen_A_idx];
    GlobalData_t y_rush_larsen_B = V_row[y_rush_larsen_B_idx];
    GlobalData_t f_rush_larsen_A = V_row[f_rush_larsen_A_idx];
    GlobalData_t f_rush_larsen_B = V_row[f_rush_larsen_B_idx];
    GlobalData_t h_rush_larsen_B = V_row[h_rush_larsen_B_idx];
    GlobalData_t pp_rush_larsen_A = V_row[pp_rush_larsen_A_idx];
    GlobalData_t pp_rush_larsen_B = V_row[pp_rush_larsen_B_idx];
    GlobalData_t h_rush_larsen_A = V_row[h_rush_larsen_A_idx];
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f_new = f_rush_larsen_A+f_rush_larsen_B*sv->f;
    GlobalData_t f2_new = f2_rush_larsen_A+f2_rush_larsen_B*sv->f2;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t pp_new = pp_rush_larsen_A+pp_rush_larsen_B*sv->pp;
    GlobalData_t r_new = r_rush_larsen_A+r_rush_larsen_B*sv->r;
    GlobalData_t x_new = x_rush_larsen_A+x_rush_larsen_B*sv->x;
    GlobalData_t y_new = y_rush_larsen_A+y_rush_larsen_B*sv->y;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    GlobalData_t diff_Cai = (((-((((isICa+IbCa)-(((2.*INaCa)/(nNaCa-(2.)))))+Iup)-(Irel)))/((2.e6*Voli)*F))/1000.);
    GlobalData_t Cai_new;
    GlobalData_t rk4_k4_Cai;
    GlobalData_t rk4_k3_Cai;
    GlobalData_t rk4_k2_Cai;
    GlobalData_t rk4_k1_Cai = diff_Cai*dt;
    {
      GlobalData_t t = t + dt/2;
      GlobalData_t sv_intermediate_Cai = sv->Cai+rk4_k1_Cai/2;
      GlobalData_t ECa = ((RTF*(log((Cao/sv_intermediate_Cai))))/2.);
      GlobalData_t Irel = (((aa_Carel*sv->Carel)*(sv_intermediate_Cai*sv_intermediate_Cai))/((sv_intermediate_Cai*sv_intermediate_Cai)+(KmCa*KmCa)));
      GlobalData_t Iup = (((aa_Caup*sv_intermediate_Cai)*(Caup_bar-(sv->Caup)))-((bb_Caup*sv->Caup)));
      GlobalData_t INaCa = (((p->kNaCa*V_row[vrow_25_idx])*((((sv->Nai*sv->Nai)*sv->Nai)*Cao)-(((V_row[vrow_19_idx]*((Nao*Nao)*Nao))*sv_intermediate_Cai))))/(1.+(dNaCa*((sv_intermediate_Cai*((Nao*Nao)*Nao))+(Cao*((sv->Nai*sv->Nai)*sv->Nai))))));
      GlobalData_t IbCa = (GbCa*(V-(ECa)));
      GlobalData_t isICa = (((fabs((V-(50.))))<.001) ? ((((sv_intermediate_Cai*exp50FRT)*exp50FRT)-(((Cao*V_row[vrow_24_idx])*V_row[vrow_24_idx])))*(2.*Psi)) : ((((sv_intermediate_Cai*exp50FRT)*exp50FRT)-(((Cao*V_row[vrow_24_idx])*V_row[vrow_24_idx])))*(((sv->d*sv->f)*sv->f2)*V_row[vrow_22_idx])));
      GlobalData_t diff_Cai = (((-((((isICa+IbCa)-(((2.*INaCa)/(nNaCa-(2.)))))+Iup)-(Irel)))/((2.e6*Voli)*F))/1000.);
      rk4_k2_Cai = dt*diff_Cai;
    }
    {
      GlobalData_t t = t + dt/2;
      GlobalData_t sv_intermediate_Cai = sv->Cai+rk4_k2_Cai/2;
      GlobalData_t ECa = ((RTF*(log((Cao/sv_intermediate_Cai))))/2.);
      GlobalData_t Irel = (((aa_Carel*sv->Carel)*(sv_intermediate_Cai*sv_intermediate_Cai))/((sv_intermediate_Cai*sv_intermediate_Cai)+(KmCa*KmCa)));
      GlobalData_t Iup = (((aa_Caup*sv_intermediate_Cai)*(Caup_bar-(sv->Caup)))-((bb_Caup*sv->Caup)));
      GlobalData_t INaCa = (((p->kNaCa*V_row[vrow_25_idx])*((((sv->Nai*sv->Nai)*sv->Nai)*Cao)-(((V_row[vrow_19_idx]*((Nao*Nao)*Nao))*sv_intermediate_Cai))))/(1.+(dNaCa*((sv_intermediate_Cai*((Nao*Nao)*Nao))+(Cao*((sv->Nai*sv->Nai)*sv->Nai))))));
      GlobalData_t IbCa = (GbCa*(V-(ECa)));
      GlobalData_t isICa = (((fabs((V-(50.))))<.001) ? ((((sv_intermediate_Cai*exp50FRT)*exp50FRT)-(((Cao*V_row[vrow_24_idx])*V_row[vrow_24_idx])))*(2.*Psi)) : ((((sv_intermediate_Cai*exp50FRT)*exp50FRT)-(((Cao*V_row[vrow_24_idx])*V_row[vrow_24_idx])))*(((sv->d*sv->f)*sv->f2)*V_row[vrow_22_idx])));
      GlobalData_t diff_Cai = (((-((((isICa+IbCa)-(((2.*INaCa)/(nNaCa-(2.)))))+Iup)-(Irel)))/((2.e6*Voli)*F))/1000.);
      rk4_k3_Cai = dt*diff_Cai;
    }
    {
      GlobalData_t t = t + dt;
      GlobalData_t sv_intermediate_Cai = sv->Cai+rk4_k3_Cai;
      GlobalData_t ECa = ((RTF*(log((Cao/sv_intermediate_Cai))))/2.);
      GlobalData_t Irel = (((aa_Carel*sv->Carel)*(sv_intermediate_Cai*sv_intermediate_Cai))/((sv_intermediate_Cai*sv_intermediate_Cai)+(KmCa*KmCa)));
      GlobalData_t Iup = (((aa_Caup*sv_intermediate_Cai)*(Caup_bar-(sv->Caup)))-((bb_Caup*sv->Caup)));
      GlobalData_t INaCa = (((p->kNaCa*V_row[vrow_25_idx])*((((sv->Nai*sv->Nai)*sv->Nai)*Cao)-(((V_row[vrow_19_idx]*((Nao*Nao)*Nao))*sv_intermediate_Cai))))/(1.+(dNaCa*((sv_intermediate_Cai*((Nao*Nao)*Nao))+(Cao*((sv->Nai*sv->Nai)*sv->Nai))))));
      GlobalData_t IbCa = (GbCa*(V-(ECa)));
      GlobalData_t isICa = (((fabs((V-(50.))))<.001) ? ((((sv_intermediate_Cai*exp50FRT)*exp50FRT)-(((Cao*V_row[vrow_24_idx])*V_row[vrow_24_idx])))*(2.*Psi)) : ((((sv_intermediate_Cai*exp50FRT)*exp50FRT)-(((Cao*V_row[vrow_24_idx])*V_row[vrow_24_idx])))*(((sv->d*sv->f)*sv->f2)*V_row[vrow_22_idx])));
      GlobalData_t diff_Cai = (((-((((isICa+IbCa)-(((2.*INaCa)/(nNaCa-(2.)))))+Iup)-(Irel)))/((2.e6*Voli)*F))/1000.);
      rk4_k4_Cai = dt*diff_Cai;
    }
    Cai_new = sv->Cai+(rk4_k1_Cai + 2*rk4_k2_Cai + 2*rk4_k3_Cai + rk4_k4_Cai)/6;
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Cai = Cai_new;
    sv->Carel = Carel_new;
    sv->Caup = Caup_new;
    Iion = Iion;
    sv->Kc = Kc_new;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->d = d_new;
    sv->f = f_new;
    sv->f2 = f2_new;
    sv->h = h_new;
    sv->m = m_new;
    sv->pp = pp_new;
    sv->r = r_new;
    sv->x = x_new;
    sv->y = y_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_DiFrancescoNoble(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("DiFrancescoNoble_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->Cai\n"
        "sv->Carel\n"
        "sv->Caup\n"
        "ICa\n"
        "IK\n"
        "IK1\n"
        "INa\n"
        "INaCa\n"
        "IbCa\n"
        "IbNa\n"
        "If\n"
        "Ip\n"
        "Irel\n"
        "Ito\n"
        "Itr\n"
        "Iup\n"
        "sv->Kc\n"
        "sv->Ki\n"
        "sv->Nai\n"
        "V\n"
        "sv->d\n"
        "sv->f\n"
        "sv->f2\n"
        "sv->h\n"
        "imK\n"
        "sv->m\n"
        "sv->pp\n"
        "sv->r\n"
        "sv->x\n"
        "sv->y\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  DiFrancescoNoble_Params *p  = (DiFrancescoNoble_Params *)IF->params;

  DiFrancescoNoble_state *sv_base = (DiFrancescoNoble_state *)IF->sv_tab.y;
  DiFrancescoNoble_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t C = (p->Cm*surface_area);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t ECa = ((RTF*(log((Cao/sv->Cai))))/2.);
  GlobalData_t EK = (RTF*(log((sv->Kc/sv->Ki))));
  GlobalData_t ENa = (RTF*(log((Nao/sv->Nai))));
  GlobalData_t Emh = (RTF*(log(((Nao+(.12*sv->Kc))/(sv->Nai+(.12*sv->Ki))))));
  GlobalData_t Ip = ((Ipmax*(sv->Kc/(KmK+sv->Kc)))*(sv->Nai/(KmNa+sv->Nai)));
  GlobalData_t Irel = (((aa_Carel*sv->Carel)*(sv->Cai*sv->Cai))/((sv->Cai*sv->Cai)+(KmCa*KmCa)));
  GlobalData_t Itr = ((aa_Catr*sv->pp)*(sv->Caup-(sv->Carel)));
  GlobalData_t Iup = (((aa_Caup*sv->Cai)*(Caup_bar-(sv->Caup)))-((bb_Caup*sv->Caup)));
  GlobalData_t vrow_19 = (exp(((-V)*FRT)));
  GlobalData_t vrow_20 = (exp((.02*V)));
  GlobalData_t vrow_22 = ((V!=50.) ? ((((-4.*Psi)*(V-(50.)))*FRT)/(expm1((((-(V-(50.)))*2.)*FRT)))) : (2.*Psi));
  GlobalData_t vrow_23 = ((V!=50.) ? ((((-0.001*Psi)*(V-(50.)))*FRT)/(expm1(((-(V-(50.)))*FRT)))) : (0.001*Psi));
  GlobalData_t vrow_24 = (exp(((-(V-(50.)))*FRT)));
  GlobalData_t vrow_25 = (exp(((((gamma*(nNaCa-(2.)))*V)*FRT)/2.)));
  GlobalData_t vterm1 = (((fabs((V+10.)))<0.001) ? 5. : ((V+10.)/(1.-((exp((-.2*(V+10.))))))));
  GlobalData_t IK = (((sv->x*IKmax)*(sv->Ki-((sv->Kc*vrow_19))))/140.);
  GlobalData_t IK1 = ((GK1*(sv->Kc/(sv->Kc+Km1)))*((V-(EK))/(1.+(exp(((2.*FRT)*((V-(EK))+10.)))))));
  GlobalData_t INa = (((((p->GNa*sv->m)*sv->m)*sv->m)*sv->h)*(V-(Emh)));
  GlobalData_t INaCa = (((p->kNaCa*vrow_25)*((((sv->Nai*sv->Nai)*sv->Nai)*Cao)-(((vrow_19*((Nao*Nao)*Nao))*sv->Cai))))/(1.+(dNaCa*((sv->Cai*((Nao*Nao)*Nao))+(Cao*((sv->Nai*sv->Nai)*sv->Nai))))));
  GlobalData_t IbCa = (GbCa*(V-(ECa)));
  GlobalData_t IbNa = (GbNa*(V-(ENa)));
  GlobalData_t If = (((p->factorIfunny*sv->y)*(sv->Kc/(sv->Kc+Kmf)))*((GfK*(V-(EK)))+(GfNa*(V-(ENa)))));
  GlobalData_t isICa = (((fabs((V-(50.))))<.001) ? ((((sv->Cai*exp50FRT)*exp50FRT)-(((Cao*vrow_24)*vrow_24)))*(2.*Psi)) : ((((sv->Cai*exp50FRT)*exp50FRT)-(((Cao*vrow_24)*vrow_24)))*(((sv->d*sv->f)*sv->f2)*vrow_22)));
  GlobalData_t isIK = (((fabs((V-(50.))))<.001) ? (((sv->Ki*exp50FRT)-((sv->Kc*vrow_24)))*(.01*Psi)) : (((sv->Ki*exp50FRT)-((sv->Kc*vrow_24)))*(((sv->d*sv->f)*sv->f2)*vrow_23)));
  GlobalData_t isINa = (((fabs((V-(50.))))<.001) ? (((sv->Nai*exp50FRT)-((Nao*vrow_24)))*(.01*Psi)) : (((sv->Nai*exp50FRT)-((Nao*vrow_24)))*(((sv->d*sv->f)*sv->f2)*vrow_23)));
  GlobalData_t vrow_21 = vterm1;
  GlobalData_t ICa = (p->GCaf*((isICa+isIK)+isINa));
  GlobalData_t Ito = (((((((2.*sv->r)*0.28)*((0.2+sv->Kc)/(Kmto+sv->Kc)))*(sv->Cai/(Kact4+sv->Cai)))*vrow_21)*((sv->Ki*vrow_20)-((sv->Kc/vrow_20))))*p->Gto);
  GlobalData_t imK = (((IK1+IK)+(If/2.))-((2.*Ip)));
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->Cai);
  fprintf(file, "%4.12f\t", sv->Carel);
  fprintf(file, "%4.12f\t", sv->Caup);
  fprintf(file, "%4.12f\t", ICa);
  fprintf(file, "%4.12f\t", IK);
  fprintf(file, "%4.12f\t", IK1);
  fprintf(file, "%4.12f\t", INa);
  fprintf(file, "%4.12f\t", INaCa);
  fprintf(file, "%4.12f\t", IbCa);
  fprintf(file, "%4.12f\t", IbNa);
  fprintf(file, "%4.12f\t", If);
  fprintf(file, "%4.12f\t", Ip);
  fprintf(file, "%4.12f\t", Irel);
  fprintf(file, "%4.12f\t", Ito);
  fprintf(file, "%4.12f\t", Itr);
  fprintf(file, "%4.12f\t", Iup);
  fprintf(file, "%4.12f\t", sv->Kc);
  fprintf(file, "%4.12f\t", sv->Ki);
  fprintf(file, "%4.12f\t", sv->Nai);
  fprintf(file, "%4.12f\t", V);
  fprintf(file, "%4.12f\t", sv->d);
  fprintf(file, "%4.12f\t", sv->f);
  fprintf(file, "%4.12f\t", sv->f2);
  fprintf(file, "%4.12f\t", sv->h);
  fprintf(file, "%4.12f\t", imK);
  fprintf(file, "%4.12f\t", sv->m);
  fprintf(file, "%4.12f\t", sv->pp);
  fprintf(file, "%4.12f\t", sv->r);
  fprintf(file, "%4.12f\t", sv->x);
  fprintf(file, "%4.12f\t", sv->y);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        