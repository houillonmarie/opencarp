// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: DeBruin KA, Krassowska W
*  Year: 1999
*  Title: Modeling electroporation in a single cell. I. Effects Of field strength and rest potential
*  Journal: Biophys J.,77(3):1213-24
*  DOI: 10.1016/S0006-3495(99)76973-0
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __ELECTROPORATION_DEBRUINKRASSOWSKA99_H__
#define __ELECTROPORATION_DEBRUINKRASSOWSKA99_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Electroporation_DeBruinKrassowska99_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG
#define Electroporation_DeBruinKrassowska99_MODDAT Iion_DATA_FLAG

struct Electroporation_DeBruinKrassowska99_Params {
    GlobalData_t G0;
    GlobalData_t alpha;
    GlobalData_t beta;
    GlobalData_t gamma;
    GlobalData_t vrest;

};

struct Electroporation_DeBruinKrassowska99_state {
    GlobalData_t g;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Electroporation_DeBruinKrassowska99(ION_IF *);
void construct_tables_Electroporation_DeBruinKrassowska99(ION_IF *);
void destroy_Electroporation_DeBruinKrassowska99(ION_IF *);
void initialize_sv_Electroporation_DeBruinKrassowska99(ION_IF *, GlobalData_t**);
void compute_Electroporation_DeBruinKrassowska99(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
