// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Gentaro Iribe, Peter Kohl
*  Year: 2008
*  Title: Axial stretch enhances sarcoplasmic reticulum Ca2+ leak and cellular Ca2+ reuptake in guinea pig ventricular myocytes: Experiments and models
*  Journal: Progress in Biophysics and Molecular Biology,97(2-3),298-311
*  DOI: 10.1016/j.pbiomolbio.2008.02.012
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "IribeKohl.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_IribeKohl(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_IribeKohl( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define CaSR_init (GlobalData_t)(0.24886)
#define Cai_init (GlobalData_t)(9.91e-6)
#define Cao (GlobalData_t)(2.)
#define Cm (GlobalData_t)(9.5e-2)
#define Cmdn_Ca_init (GlobalData_t)(3.9636e-6)
#define Cmdn_tot (GlobalData_t)(0.02)
#define F (GlobalData_t)(96485.3415)
#define F_1_init (GlobalData_t)(0.5268)
#define F_2_init (GlobalData_t)(8.7508e-6)
#define F_CaMK_init (GlobalData_t)(1.028)
#define F_SRCa_RyR_init (GlobalData_t)(0.25089)
#define GK1 (GlobalData_t)(1.)
#define GNa (GlobalData_t)(2.5)
#define GXB (GlobalData_t)((30./1000.))
#define Gb_Ca (GlobalData_t)(0.00025)
#define Gb_K (GlobalData_t)(0.0006)
#define Gb_Na (GlobalData_t)(0.0006)
#define Gto (GlobalData_t)(0.005)
#define IKmax (GlobalData_t)(1.)
#define INaCa_max (GlobalData_t)(0.0005)
#define INaK_max (GlobalData_t)(1.36)
#define K_leak_rate (GlobalData_t)(0.)
#define K_mK (GlobalData_t)(1.)
#define K_mNa (GlobalData_t)(21.7)
#define K_mk1 (GlobalData_t)(10.)
#define K_rel_max (GlobalData_t)(500.)
#define Ko (GlobalData_t)(4.)
#define N_0_init (GlobalData_t)(0.99917)
#define Nai_init (GlobalData_t)(5.8041)
#define Nao (GlobalData_t)(140.)
#define P_0_init (GlobalData_t)(9.8593e-5)
#define P_1_init (GlobalData_t)(1.3331e-4)
#define P_2_init (GlobalData_t)(2.3505e-4)
#define P_3_init (GlobalData_t)(1.5349e-4)
#define P_CaLCa (GlobalData_t)(0.25)
#define R (GlobalData_t)(8314.472)
#define SL (GlobalData_t)(2.15)
#define T (GlobalData_t)(310.)
#define Trpn_Ca_init (GlobalData_t)(2.7661e-4)
#define Trpn_tot (GlobalData_t)(0.07)
#define V_init (GlobalData_t)(-92.849333)
#define V_max_f (GlobalData_t)(0.292)
#define V_max_r (GlobalData_t)(0.391)
#define alpha_cmdn (GlobalData_t)(10000.)
#define alpha_trpn (GlobalData_t)(80000.)
#define beta_cmdn (GlobalData_t)(500.)
#define beta_tm (GlobalData_t)((40./1000.))
#define beta_trpn (GlobalData_t)(200.)
#define d_init (GlobalData_t)(1.7908e-8)
#define delta_m (GlobalData_t)(1e-5)
#define f_XB (GlobalData_t)((10./1000.))
#define f_init (GlobalData_t)(1.)
#define gain_k1 (GlobalData_t)(1.)
#define gain_k2 (GlobalData_t)(1.)
#define gain_k3 (GlobalData_t)(1.)
#define gain_k4 (GlobalData_t)(1.)
#define gamma (GlobalData_t)(0.5)
#define h_init (GlobalData_t)(0.99569)
#define m_init (GlobalData_t)(0.0013809)
#define r_init (GlobalData_t)(1.5185e-8)
#define s_init (GlobalData_t)(0.95854)
#define shift_h (GlobalData_t)(0.)
#define speed_d (GlobalData_t)(3.)
#define speed_f (GlobalData_t)(0.5)
#define tau_F_CaMK (GlobalData_t)((0.8*1000.))
#define tau_SRCa_RyR (GlobalData_t)((0.05*1000.))
#define v_SR (GlobalData_t)(3.3477e-6)
#define v_i (GlobalData_t)(1.6404e-5)
#define x_init (GlobalData_t)(5.1127e-2)
#define zeta (GlobalData_t)(0.1)
#define SL_norm (GlobalData_t)(((SL-(1.7))/0.7))
#define f_01 (GlobalData_t)((3.*f_XB))
#define f_12 (GlobalData_t)((10.*f_XB))
#define f_23 (GlobalData_t)((7.*f_XB))
#define k_4 (GlobalData_t)((gain_k4*1.8))
#define phISL (GlobalData_t)(((SL<1.7) ? (1.1/1.4) : ((SL<=2.) ? ((SL-(0.6))/1.4) : ((SL<=2.2) ? 1. : ((SL<=2.3) ? ((3.6-(SL))/1.4) : (1.3/1.4))))))
#define G01 (GlobalData_t)((GXB*(2.-(SL_norm))))
#define G12 (GlobalData_t)(((2.*GXB)*(2.-(SL_norm))))
#define G23 (GlobalData_t)(((3.*GXB)*(2.-(SL_norm))))
#define K_tm (GlobalData_t)((1./(1.+((beta_trpn/alpha_trpn)/(0.0017-((0.0009*SL_norm)))))))
#define N_tm (GlobalData_t)((3.5+(2.5*SL_norm)))
#define sigma_paths (GlobalData_t)((((((((GXB*2.)*GXB)*3.)*GXB)+((((f_01*2.)*GXB)*3.)*GXB))+(((f_01*f_12)*3.)*GXB))+((f_01*f_12)*f_23)))
#define P_1_max (GlobalData_t)((((((f_01*2.)*GXB)*3.)*GXB)/sigma_paths))
#define P_2_max (GlobalData_t)(((((f_01*f_12)*3.)*GXB)/sigma_paths))
#define P_3_max (GlobalData_t)((((f_01*f_12)*f_23)/sigma_paths))
#define Force_max (GlobalData_t)(((P_1_max+(2.*P_2_max))+(3.*P_3_max)))



void initialize_params_IribeKohl( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  IribeKohl_Params *p = (IribeKohl_Params *)IF->params;

  // Compute the regional constants
  {
    p->Ki_init = 138.22;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_IribeKohl;

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_IribeKohl( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IribeKohl_Params *p = (IribeKohl_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double F_CaMK_rush_larsen_B = (exp(((-dt)/tau_F_CaMK)));
  double F_CaMK_rush_larsen_C = (expm1(((-dt)/tau_F_CaMK)));

}



void    initialize_sv_IribeKohl( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IribeKohl_Params *p = (IribeKohl_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(IribeKohl_state) );
  IribeKohl_state *sv_base = (IribeKohl_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double F_CaMK_rush_larsen_B = (exp(((-dt)/tau_F_CaMK)));
  double F_CaMK_rush_larsen_C = (expm1(((-dt)/tau_F_CaMK)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    IribeKohl_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->CaSR = CaSR_init;
    sv->Cai = Cai_init;
    sv->Cmdn_Ca = Cmdn_Ca_init;
    sv->F_1 = F_1_init;
    sv->F_2 = F_2_init;
    sv->F_CaMK = F_CaMK_init;
    sv->F_SRCa_RyR = F_SRCa_RyR_init;
    sv->Ki = p->Ki_init;
    sv->N_0 = N_0_init;
    sv->Nai = Nai_init;
    sv->P_0 = P_0_init;
    sv->P_1 = P_1_init;
    sv->P_2 = P_2_init;
    sv->P_3 = P_3_init;
    sv->Trpn_Ca = Trpn_Ca_init;
    V = V_init;
    sv->d = d_init;
    sv->f = f_init;
    sv->h = h_init;
    sv->m = m_init;
    sv->r = r_init;
    sv->s = s_init;
    sv->x = x_init;
    double E_Ca = ((((0.5*R)*T)/F)*(log((Cao/sv->Cai))));
    double E_K = (((R*T)/F)*(log((Ko/sv->Ki))));
    double E_Na = (((R*T)/F)*(log((Nao/sv->Nai))));
    double E_mh = (((R*T)/F)*(log(((Nao+(0.12*Ko))/(sv->Nai+(0.12*sv->Ki))))));
    double ICaLCa = ((V==50.) ? (((((4.*sv->d)*sv->f)*P_CaLCa)/2.)*((sv->Cai*(exp(((100.*F)/(R*T)))))-(Cao))) : (((((((4.*sv->d)*sv->f)*P_CaLCa)*(V-(50.)))*F)/((R*T)*(1.-((exp((((-2.*(V-(50.)))*F)/(R*T))))))))*((sv->Cai*(exp(((100.*F)/(R*T)))))-((Cao*(exp((((-2.*(V-(50.)))*F)/(R*T)))))))));
    double ICaLK = ((V==50.) ? ((((0.002*sv->d)*sv->f)*P_CaLCa)*((sv->Ki*(exp(((50.*F)/(R*T)))))-(Ko))) : (((((((0.002*sv->d)*sv->f)*P_CaLCa)*(V-(50.)))*F)/((R*T)*(1.-((exp((((-(V-(50.)))*F)/(R*T))))))))*((sv->Ki*(exp(((50.*F)/(R*T)))))-((Ko*(exp((((-(V-(50.)))*F)/(R*T)))))))));
    double ICaLNa = ((V==50.) ? ((((0.01*sv->d)*sv->f)*P_CaLCa)*((sv->Nai*(exp(((50.*F)/(R*T)))))-(Nao))) : (((((((0.01*sv->d)*sv->f)*P_CaLCa)*(V-(50.)))*F)/((R*T)*(1.-((exp((((-(V-(50.)))*F)/(R*T))))))))*((sv->Nai*(exp(((50.*F)/(R*T)))))-((Nao*(exp((((-(V-(50.)))*F)/(R*T)))))))));
    double IK = (((IKmax*sv->x)*(sv->Ki-((Ko*(exp((((-V)*F)/(R*T))))))))/140.);
    double INaCa = ((INaCa_max*((((exp((((gamma*V)*F)/(R*T))))*(cube(sv->Nai)))*Cao)-((((exp(((((gamma-(1.))*V)*F)/(R*T))))*(cube(Nao)))*sv->Cai))))/(1.+(sv->Cai/0.0069)));
    double INaK = (((((INaK_max*Ko)/(K_mK+Ko))*sv->Nai)/(K_mNa+sv->Nai))/((1.+(0.1245*(exp((((-0.1*V)*F)/(R*T))))))+(0.0353*(exp((((-V)*F)/(R*T)))))));
    double ICa_L = ((ICaLCa+ICaLK)+ICaLNa);
    double IK1 = ((((GK1*Ko)/(Ko+K_mk1))*(V-(E_K)))/(1.+(exp((((2.*F)*((V-(E_K))-(10.)))/(R*T))))));
    double INa = (((GNa*(cube(sv->m)))*sv->h)*(V-(E_mh)));
    double Ib_Ca = (Gb_Ca*(V-(E_Ca)));
    double Ib_K = (Gb_K*(V-(E_K)));
    double Ib_Na = (Gb_Na*(V-(E_Na)));
    double Ito = (((Gto*sv->s)*sv->r)*(V-(E_K)));
    Iion = ((((((((((INa+Ib_Na)+IK1)+IK)+Ito)+Ib_K)+ICa_L)+Ib_Ca)+INaCa)+INaK)/Cm);
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_IribeKohl(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IribeKohl_Params *p  = (IribeKohl_Params *)IF->params;
  IribeKohl_state *sv_base = (IribeKohl_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t F_CaMK_rush_larsen_B = (exp(((-dt)/tau_F_CaMK)));
  GlobalData_t F_CaMK_rush_larsen_C = (expm1(((-dt)/tau_F_CaMK)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    IribeKohl_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t E_Ca = ((((0.5*R)*T)/F)*(log((Cao/sv->Cai))));
    GlobalData_t E_K = (((R*T)/F)*(log((Ko/sv->Ki))));
    GlobalData_t E_Na = (((R*T)/F)*(log((Nao/sv->Nai))));
    GlobalData_t E_mh = (((R*T)/F)*(log(((Nao+(0.12*Ko))/(sv->Nai+(0.12*sv->Ki))))));
    GlobalData_t ICaLCa = ((V==50.) ? (((((4.*sv->d)*sv->f)*P_CaLCa)/2.)*((sv->Cai*(exp(((100.*F)/(R*T)))))-(Cao))) : (((((((4.*sv->d)*sv->f)*P_CaLCa)*(V-(50.)))*F)/((R*T)*(1.-((exp((((-2.*(V-(50.)))*F)/(R*T))))))))*((sv->Cai*(exp(((100.*F)/(R*T)))))-((Cao*(exp((((-2.*(V-(50.)))*F)/(R*T)))))))));
    GlobalData_t ICaLK = ((V==50.) ? ((((0.002*sv->d)*sv->f)*P_CaLCa)*((sv->Ki*(exp(((50.*F)/(R*T)))))-(Ko))) : (((((((0.002*sv->d)*sv->f)*P_CaLCa)*(V-(50.)))*F)/((R*T)*(1.-((exp((((-(V-(50.)))*F)/(R*T))))))))*((sv->Ki*(exp(((50.*F)/(R*T)))))-((Ko*(exp((((-(V-(50.)))*F)/(R*T)))))))));
    GlobalData_t ICaLNa = ((V==50.) ? ((((0.01*sv->d)*sv->f)*P_CaLCa)*((sv->Nai*(exp(((50.*F)/(R*T)))))-(Nao))) : (((((((0.01*sv->d)*sv->f)*P_CaLCa)*(V-(50.)))*F)/((R*T)*(1.-((exp((((-(V-(50.)))*F)/(R*T))))))))*((sv->Nai*(exp(((50.*F)/(R*T)))))-((Nao*(exp((((-(V-(50.)))*F)/(R*T)))))))));
    GlobalData_t IK = (((IKmax*sv->x)*(sv->Ki-((Ko*(exp((((-V)*F)/(R*T))))))))/140.);
    GlobalData_t INaCa = ((INaCa_max*((((exp((((gamma*V)*F)/(R*T))))*(cube(sv->Nai)))*Cao)-((((exp(((((gamma-(1.))*V)*F)/(R*T))))*(cube(Nao)))*sv->Cai))))/(1.+(sv->Cai/0.0069)));
    GlobalData_t INaK = (((((INaK_max*Ko)/(K_mK+Ko))*sv->Nai)/(K_mNa+sv->Nai))/((1.+(0.1245*(exp((((-0.1*V)*F)/(R*T))))))+(0.0353*(exp((((-V)*F)/(R*T)))))));
    GlobalData_t ICa_L = ((ICaLCa+ICaLK)+ICaLNa);
    GlobalData_t IK1 = ((((GK1*Ko)/(Ko+K_mk1))*(V-(E_K)))/(1.+(exp((((2.*F)*((V-(E_K))-(10.)))/(R*T))))));
    GlobalData_t INa = (((GNa*(cube(sv->m)))*sv->h)*(V-(E_mh)));
    GlobalData_t Ib_Ca = (Gb_Ca*(V-(E_Ca)));
    GlobalData_t Ib_K = (Gb_K*(V-(E_K)));
    GlobalData_t Ib_Na = (Gb_Na*(V-(E_Na)));
    GlobalData_t Ito = (((Gto*sv->s)*sv->r)*(V-(E_K)));
    Iion = ((((((((((INa+Ib_Na)+IK1)+IK)+Ito)+Ib_K)+ICa_L)+Ib_Ca)+INaCa)+INaK)/Cm);
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    GlobalData_t E0_d = ((V+24.)-(5.));
    GlobalData_t E0_f = (V+34.);
    GlobalData_t E0_m = (V+41.);
    GlobalData_t F_CaMK_inf = (sv->Cmdn_Ca/0.00005);
    GlobalData_t alpha_h = ((20.*(exp((-0.125*((V+75.)-(shift_h))))))/1000.);
    GlobalData_t alpha_s = ((0.033*(exp(((-V)/17.))))/1000.);
    GlobalData_t alpha_x = (((0.5*(exp((0.0826*(V+50.)))))/(1.+(exp((0.057*(V+50.))))))/1000.);
    GlobalData_t beta_h = ((2000./(1.+(320.*(exp((-0.1*((V+75.)-(shift_h))))))))/1000.);
    GlobalData_t beta_m = ((8000.*(exp((-0.056*(V+66.)))))/1000.);
    GlobalData_t beta_s = ((33./(1.+(exp((-0.125*(V+10.))))))/1000.);
    GlobalData_t beta_x = (((1.3*(exp((-0.06*(V+20.)))))/(1.+(exp((-0.04*(V+20.))))))/1000.);
    GlobalData_t F_CaMK_rush_larsen_A = ((-F_CaMK_inf)*F_CaMK_rush_larsen_C);
    GlobalData_t alpha_d = ((((fabs(E0_d))<0.00001) ? (speed_d*120.) : (((speed_d*30.)*E0_d)/(1.-((exp(((-E0_d)/4.)))))))/1000.);
    GlobalData_t alpha_f = ((((fabs(E0_f))<0.00001) ? (speed_f*25.) : (((speed_f*6.25)*E0_f)/(-1.+(exp((E0_f/4.))))))/1000.);
    GlobalData_t alpha_m = ((((fabs(E0_m))<delta_m) ? 2000. : ((200.*E0_m)/(1.-((exp((-0.1*E0_m)))))))/1000.);
    GlobalData_t beta_d = ((((fabs(E0_d))<0.00001) ? (speed_d*120.) : (((speed_d*-12.)*E0_d)/(1.-((exp((E0_d/10.)))))))/1000.);
    GlobalData_t beta_f = (((speed_f*50.)/(1.+(exp(((-E0_f)/4.)))))/1000.);
    GlobalData_t h_rush_larsen_A = (((-alpha_h)/(alpha_h+beta_h))*(expm1(((-dt)*(alpha_h+beta_h)))));
    GlobalData_t h_rush_larsen_B = (exp(((-dt)*(alpha_h+beta_h))));
    GlobalData_t s_rush_larsen_A = (((-alpha_s)/(alpha_s+beta_s))*(expm1(((-dt)*(alpha_s+beta_s)))));
    GlobalData_t s_rush_larsen_B = (exp(((-dt)*(alpha_s+beta_s))));
    GlobalData_t x_rush_larsen_A = (((-alpha_x)/(alpha_x+beta_x))*(expm1(((-dt)*(alpha_x+beta_x)))));
    GlobalData_t x_rush_larsen_B = (exp(((-dt)*(alpha_x+beta_x))));
    GlobalData_t d_rush_larsen_A = (((-alpha_d)/(alpha_d+beta_d))*(expm1(((-dt)*(alpha_d+beta_d)))));
    GlobalData_t d_rush_larsen_B = (exp(((-dt)*(alpha_d+beta_d))));
    GlobalData_t f_rush_larsen_A = (((-alpha_f)/(alpha_f+beta_f))*(expm1(((-dt)*(alpha_f+beta_f)))));
    GlobalData_t f_rush_larsen_B = (exp(((-dt)*(alpha_f+beta_f))));
    GlobalData_t m_rush_larsen_A = (((-alpha_m)/(alpha_m+beta_m))*(expm1(((-dt)*(alpha_m+beta_m)))));
    GlobalData_t m_rush_larsen_B = (exp(((-dt)*(alpha_m+beta_m))));
    GlobalData_t F_CaMK_new = F_CaMK_rush_larsen_A+F_CaMK_rush_larsen_B*sv->F_CaMK;
    GlobalData_t d_new = d_rush_larsen_A+d_rush_larsen_B*sv->d;
    GlobalData_t f_new = f_rush_larsen_A+f_rush_larsen_B*sv->f;
    GlobalData_t h_new = h_rush_larsen_A+h_rush_larsen_B*sv->h;
    GlobalData_t m_new = m_rush_larsen_A+m_rush_larsen_B*sv->m;
    GlobalData_t s_new = s_rush_larsen_A+s_rush_larsen_B*sv->s;
    GlobalData_t x_new = x_rush_larsen_A+x_rush_larsen_B*sv->x;
    
    
    //Complete RK2 Update
    GlobalData_t F_3 = (1.-((sv->F_1+sv->F_2)));
    GlobalData_t F_rel = (square((sv->F_2/(sv->F_2+0.25))));
    GlobalData_t K_rel = ((K_rel_max*sv->F_SRCa_RyR)/(sv->F_SRCa_RyR+0.2));
    GlobalData_t N_1 = (1.-(((((sv->N_0+sv->P_0)+sv->P_1)+sv->P_2)+sv->P_3)));
    GlobalData_t N_CaMK = (square((sv->F_CaMK/0.7)));
    GlobalData_t alpha_tm = (beta_tm*(pow((sv->Trpn_Ca/(Trpn_tot*K_tm)),N_tm)));
    GlobalData_t diff_Cmdn_Ca = ((((alpha_cmdn*(Cmdn_tot-(sv->Cmdn_Ca)))*sv->Cai)-((beta_cmdn*sv->Cmdn_Ca)))/1000.);
    GlobalData_t diff_F_SRCa_RyR = ((sv->CaSR-(sv->F_SRCa_RyR))/tau_SRCa_RyR);
    GlobalData_t diff_Ki = (((-(((((IK1+IK)+Ito)+Ib_K)+ICaLK)-((2.*INaK))))/(v_i*F))/1000.);
    GlobalData_t diff_Nai = (((-((((INa+Ib_Na)+ICaLNa)+(3.*INaCa))+(3.*INaK)))/(v_i*F))/1000.);
    GlobalData_t diff_P_2 = ((((-(f_23+G12))*sv->P_2)+(f_12*sv->P_1))+(G23*sv->P_3));
    GlobalData_t diff_P_3 = (((-G23)*sv->P_3)+(f_23*sv->P_2));
    GlobalData_t diff_r = ((333.*((1./(1.+(exp(((-(V+4.))/5.)))))-(sv->r)))/1000.);
    GlobalData_t f_b = (square((sv->Cai/0.00024)));
    GlobalData_t k_1 = (gain_k1*((30625000.*(square(sv->Cai)))-((245.*ICa_L))));
    GlobalData_t k_2 = ((gain_k2*450.)/(1.+(0.36/sv->CaSR)));
    GlobalData_t r_b = (square((sv->CaSR/1.64)));
    GlobalData_t Force_norm = ((phISL*(((sv->P_1+N_1)+(2.*sv->P_2))+(3.*sv->P_3)))/Force_max);
    GlobalData_t dCmdn_Ca_dtime = diff_Cmdn_Ca;
    GlobalData_t diff_F_2 = (((k_1*sv->F_1)-((k_2*sv->F_2)))/1000.);
    GlobalData_t diff_N_0 = (((beta_tm*sv->P_0)-((alpha_tm*sv->N_0)))+(G01*N_1));
    GlobalData_t diff_P_0 = ((((-(beta_tm+f_01))*sv->P_0)+(alpha_tm*sv->N_0))+(G01*sv->P_1));
    GlobalData_t diff_P_1 = (((((-((beta_tm+f_12)+G01))*sv->P_1)+(alpha_tm*N_1))+(f_01*sv->P_0))+(G12*sv->P_2));
    GlobalData_t j_rel = (((K_rel*F_rel)+K_leak_rate)*(sv->CaSR-(sv->Cai)));
    GlobalData_t j_up = ((((sv->F_CaMK*V_max_f)*f_b)-((V_max_r*r_b)))/((1.+f_b)+r_b));
    GlobalData_t k_3 = ((gain_k3*1.885)*(pow((sv->F_SRCa_RyR/0.22),N_CaMK)));
    GlobalData_t diff_CaSR = ((((j_up*v_i)/v_SR)-(j_rel))/1000.);
    GlobalData_t diff_F_1 = ((((k_3*F_3)-((k_4*sv->F_1)))-((k_1*sv->F_1)))/1000.);
    GlobalData_t diff_Trpn_Ca = ((((alpha_trpn*(Trpn_tot-(sv->Trpn_Ca)))*sv->Cai)-((((beta_trpn*(1.+(2.*(1.-(Force_norm)))))/3.)*sv->Trpn_Ca)))/1000.);
    GlobalData_t dTrpn_Ca_dtime = diff_Trpn_Ca;
    GlobalData_t diff_Cai = (((((((-((ICaLCa+Ib_Ca)-((2.*INaCa))))/((2.*v_i)*F))-(j_up))+((j_rel*v_SR)/v_i))/1000.)-(dCmdn_Ca_dtime))-(dTrpn_Ca_dtime));
    GlobalData_t CaSR_new;
    GlobalData_t Cai_new;
    GlobalData_t Cmdn_Ca_new;
    GlobalData_t F_1_new;
    GlobalData_t F_2_new;
    GlobalData_t F_SRCa_RyR_new;
    GlobalData_t Ki_new;
    GlobalData_t N_0_new;
    GlobalData_t Nai_new;
    GlobalData_t P_0_new;
    GlobalData_t P_1_new;
    GlobalData_t P_2_new;
    GlobalData_t P_3_new;
    GlobalData_t Trpn_Ca_new;
    GlobalData_t r_new;
    {
      GlobalData_t t = t + dt/2;
      GlobalData_t sv_intermediate_CaSR = sv->CaSR+dt/2*diff_CaSR;
      GlobalData_t sv_intermediate_Cai = sv->Cai+dt/2*diff_Cai;
      GlobalData_t sv_intermediate_Cmdn_Ca = sv->Cmdn_Ca+dt/2*diff_Cmdn_Ca;
      GlobalData_t sv_intermediate_F_1 = sv->F_1+dt/2*diff_F_1;
      GlobalData_t sv_intermediate_F_2 = sv->F_2+dt/2*diff_F_2;
      GlobalData_t sv_intermediate_F_SRCa_RyR = sv->F_SRCa_RyR+dt/2*diff_F_SRCa_RyR;
      GlobalData_t sv_intermediate_Ki = sv->Ki+dt/2*diff_Ki;
      GlobalData_t sv_intermediate_N_0 = sv->N_0+dt/2*diff_N_0;
      GlobalData_t sv_intermediate_Nai = sv->Nai+dt/2*diff_Nai;
      GlobalData_t sv_intermediate_P_0 = sv->P_0+dt/2*diff_P_0;
      GlobalData_t sv_intermediate_P_1 = sv->P_1+dt/2*diff_P_1;
      GlobalData_t sv_intermediate_P_2 = sv->P_2+dt/2*diff_P_2;
      GlobalData_t sv_intermediate_P_3 = sv->P_3+dt/2*diff_P_3;
      GlobalData_t sv_intermediate_Trpn_Ca = sv->Trpn_Ca+dt/2*diff_Trpn_Ca;
      GlobalData_t sv_intermediate_r = sv->r+dt/2*diff_r;
      GlobalData_t E_Ca = ((((0.5*R)*T)/F)*(log((Cao/sv_intermediate_Cai))));
      GlobalData_t E_K = (((R*T)/F)*(log((Ko/sv_intermediate_Ki))));
      GlobalData_t E_Na = (((R*T)/F)*(log((Nao/sv_intermediate_Nai))));
      GlobalData_t E_mh = (((R*T)/F)*(log(((Nao+(0.12*Ko))/(sv_intermediate_Nai+(0.12*sv_intermediate_Ki))))));
      GlobalData_t F_3 = (1.-((sv_intermediate_F_1+sv_intermediate_F_2)));
      GlobalData_t F_rel = (square((sv_intermediate_F_2/(sv_intermediate_F_2+0.25))));
      GlobalData_t ICaLCa = ((V==50.) ? (((((4.*sv->d)*sv->f)*P_CaLCa)/2.)*((sv_intermediate_Cai*(exp(((100.*F)/(R*T)))))-(Cao))) : (((((((4.*sv->d)*sv->f)*P_CaLCa)*(V-(50.)))*F)/((R*T)*(1.-((exp((((-2.*(V-(50.)))*F)/(R*T))))))))*((sv_intermediate_Cai*(exp(((100.*F)/(R*T)))))-((Cao*(exp((((-2.*(V-(50.)))*F)/(R*T)))))))));
      GlobalData_t ICaLK = ((V==50.) ? ((((0.002*sv->d)*sv->f)*P_CaLCa)*((sv_intermediate_Ki*(exp(((50.*F)/(R*T)))))-(Ko))) : (((((((0.002*sv->d)*sv->f)*P_CaLCa)*(V-(50.)))*F)/((R*T)*(1.-((exp((((-(V-(50.)))*F)/(R*T))))))))*((sv_intermediate_Ki*(exp(((50.*F)/(R*T)))))-((Ko*(exp((((-(V-(50.)))*F)/(R*T)))))))));
      GlobalData_t ICaLNa = ((V==50.) ? ((((0.01*sv->d)*sv->f)*P_CaLCa)*((sv_intermediate_Nai*(exp(((50.*F)/(R*T)))))-(Nao))) : (((((((0.01*sv->d)*sv->f)*P_CaLCa)*(V-(50.)))*F)/((R*T)*(1.-((exp((((-(V-(50.)))*F)/(R*T))))))))*((sv_intermediate_Nai*(exp(((50.*F)/(R*T)))))-((Nao*(exp((((-(V-(50.)))*F)/(R*T)))))))));
      GlobalData_t IK = (((IKmax*sv->x)*(sv_intermediate_Ki-((Ko*(exp((((-V)*F)/(R*T))))))))/140.);
      GlobalData_t INaCa = ((INaCa_max*((((exp((((gamma*V)*F)/(R*T))))*(cube(sv_intermediate_Nai)))*Cao)-((((exp(((((gamma-(1.))*V)*F)/(R*T))))*(cube(Nao)))*sv_intermediate_Cai))))/(1.+(sv_intermediate_Cai/0.0069)));
      GlobalData_t INaK = (((((INaK_max*Ko)/(K_mK+Ko))*sv_intermediate_Nai)/(K_mNa+sv_intermediate_Nai))/((1.+(0.1245*(exp((((-0.1*V)*F)/(R*T))))))+(0.0353*(exp((((-V)*F)/(R*T)))))));
      GlobalData_t K_rel = ((K_rel_max*sv_intermediate_F_SRCa_RyR)/(sv_intermediate_F_SRCa_RyR+0.2));
      GlobalData_t N_1 = (1.-(((((sv_intermediate_N_0+sv_intermediate_P_0)+sv_intermediate_P_1)+sv_intermediate_P_2)+sv_intermediate_P_3)));
      GlobalData_t alpha_tm = (beta_tm*(pow((sv_intermediate_Trpn_Ca/(Trpn_tot*K_tm)),N_tm)));
      GlobalData_t diff_Cmdn_Ca = ((((alpha_cmdn*(Cmdn_tot-(sv_intermediate_Cmdn_Ca)))*sv_intermediate_Cai)-((beta_cmdn*sv_intermediate_Cmdn_Ca)))/1000.);
      GlobalData_t diff_F_SRCa_RyR = ((sv_intermediate_CaSR-(sv_intermediate_F_SRCa_RyR))/tau_SRCa_RyR);
      GlobalData_t diff_P_2 = ((((-(f_23+G12))*sv_intermediate_P_2)+(f_12*sv_intermediate_P_1))+(G23*sv_intermediate_P_3));
      GlobalData_t diff_P_3 = (((-G23)*sv_intermediate_P_3)+(f_23*sv_intermediate_P_2));
      GlobalData_t diff_r = ((333.*((1./(1.+(exp(((-(V+4.))/5.)))))-(sv_intermediate_r)))/1000.);
      GlobalData_t f_b = (square((sv_intermediate_Cai/0.00024)));
      GlobalData_t k_2 = ((gain_k2*450.)/(1.+(0.36/sv_intermediate_CaSR)));
      GlobalData_t k_3 = ((gain_k3*1.885)*(pow((sv_intermediate_F_SRCa_RyR/0.22),N_CaMK)));
      GlobalData_t r_b = (square((sv_intermediate_CaSR/1.64)));
      GlobalData_t Force_norm = ((phISL*(((sv_intermediate_P_1+N_1)+(2.*sv_intermediate_P_2))+(3.*sv_intermediate_P_3)))/Force_max);
      GlobalData_t ICa_L = ((ICaLCa+ICaLK)+ICaLNa);
      GlobalData_t IK1 = ((((GK1*Ko)/(Ko+K_mk1))*(V-(E_K)))/(1.+(exp((((2.*F)*((V-(E_K))-(10.)))/(R*T))))));
      GlobalData_t INa = (((GNa*(cube(sv->m)))*sv->h)*(V-(E_mh)));
      GlobalData_t Ib_Ca = (Gb_Ca*(V-(E_Ca)));
      GlobalData_t Ib_K = (Gb_K*(V-(E_K)));
      GlobalData_t Ib_Na = (Gb_Na*(V-(E_Na)));
      GlobalData_t Ito = (((Gto*sv->s)*sv_intermediate_r)*(V-(E_K)));
      GlobalData_t dCmdn_Ca_dtime = diff_Cmdn_Ca;
      GlobalData_t diff_N_0 = (((beta_tm*sv_intermediate_P_0)-((alpha_tm*sv_intermediate_N_0)))+(G01*N_1));
      GlobalData_t diff_P_0 = ((((-(beta_tm+f_01))*sv_intermediate_P_0)+(alpha_tm*sv_intermediate_N_0))+(G01*sv_intermediate_P_1));
      GlobalData_t diff_P_1 = (((((-((beta_tm+f_12)+G01))*sv_intermediate_P_1)+(alpha_tm*N_1))+(f_01*sv_intermediate_P_0))+(G12*sv_intermediate_P_2));
      GlobalData_t j_rel = (((K_rel*F_rel)+K_leak_rate)*(sv_intermediate_CaSR-(sv_intermediate_Cai)));
      GlobalData_t j_up = ((((sv->F_CaMK*V_max_f)*f_b)-((V_max_r*r_b)))/((1.+f_b)+r_b));
      GlobalData_t diff_CaSR = ((((j_up*v_i)/v_SR)-(j_rel))/1000.);
      GlobalData_t diff_Ki = (((-(((((IK1+IK)+Ito)+Ib_K)+ICaLK)-((2.*INaK))))/(v_i*F))/1000.);
      GlobalData_t diff_Nai = (((-((((INa+Ib_Na)+ICaLNa)+(3.*INaCa))+(3.*INaK)))/(v_i*F))/1000.);
      GlobalData_t diff_Trpn_Ca = ((((alpha_trpn*(Trpn_tot-(sv_intermediate_Trpn_Ca)))*sv_intermediate_Cai)-((((beta_trpn*(1.+(2.*(1.-(Force_norm)))))/3.)*sv_intermediate_Trpn_Ca)))/1000.);
      GlobalData_t k_1 = (gain_k1*((30625000.*(square(sv_intermediate_Cai)))-((245.*ICa_L))));
      GlobalData_t dTrpn_Ca_dtime = diff_Trpn_Ca;
      GlobalData_t diff_F_1 = ((((k_3*F_3)-((k_4*sv_intermediate_F_1)))-((k_1*sv_intermediate_F_1)))/1000.);
      GlobalData_t diff_F_2 = (((k_1*sv_intermediate_F_1)-((k_2*sv_intermediate_F_2)))/1000.);
      GlobalData_t diff_Cai = (((((((-((ICaLCa+Ib_Ca)-((2.*INaCa))))/((2.*v_i)*F))-(j_up))+((j_rel*v_SR)/v_i))/1000.)-(dCmdn_Ca_dtime))-(dTrpn_Ca_dtime));
      CaSR_new = sv->CaSR+dt*diff_CaSR;
      Cai_new = sv->Cai+dt*diff_Cai;
      Cmdn_Ca_new = sv->Cmdn_Ca+dt*diff_Cmdn_Ca;
      F_1_new = sv->F_1+dt*diff_F_1;
      F_2_new = sv->F_2+dt*diff_F_2;
      F_SRCa_RyR_new = sv->F_SRCa_RyR+dt*diff_F_SRCa_RyR;
      Ki_new = sv->Ki+dt*diff_Ki;
      N_0_new = sv->N_0+dt*diff_N_0;
      Nai_new = sv->Nai+dt*diff_Nai;
      P_0_new = sv->P_0+dt*diff_P_0;
      P_1_new = sv->P_1+dt*diff_P_1;
      P_2_new = sv->P_2+dt*diff_P_2;
      P_3_new = sv->P_3+dt*diff_P_3;
      Trpn_Ca_new = sv->Trpn_Ca+dt*diff_Trpn_Ca;
      r_new = sv->r+dt*diff_r;
    }
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->CaSR = CaSR_new;
    sv->Cai = Cai_new;
    sv->Cmdn_Ca = Cmdn_Ca_new;
    sv->F_1 = F_1_new;
    sv->F_2 = F_2_new;
    sv->F_CaMK = F_CaMK_new;
    sv->F_SRCa_RyR = F_SRCa_RyR_new;
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->N_0 = N_0_new;
    sv->Nai = Nai_new;
    sv->P_0 = P_0_new;
    sv->P_1 = P_1_new;
    sv->P_2 = P_2_new;
    sv->P_3 = P_3_new;
    sv->Trpn_Ca = Trpn_Ca_new;
    sv->d = d_new;
    sv->f = f_new;
    sv->h = h_new;
    sv->m = m_new;
    sv->r = r_new;
    sv->s = s_new;
    sv->x = x_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_IribeKohl(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("IribeKohl_trace_header.txt","wt");
    fprintf(theader->fd,
        "sv->Cai\n"
        "sv->Cmdn_Ca\n"
        "sv->F_CaMK\n"
        "ICa_L\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  IribeKohl_Params *p  = (IribeKohl_Params *)IF->params;

  IribeKohl_state *sv_base = (IribeKohl_state *)IF->sv_tab.y;
  IribeKohl_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t F_CaMK_rush_larsen_B = (exp(((-dt)/tau_F_CaMK)));
  GlobalData_t F_CaMK_rush_larsen_C = (expm1(((-dt)/tau_F_CaMK)));
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t ICaLCa = ((V==50.) ? (((((4.*sv->d)*sv->f)*P_CaLCa)/2.)*((sv->Cai*(exp(((100.*F)/(R*T)))))-(Cao))) : (((((((4.*sv->d)*sv->f)*P_CaLCa)*(V-(50.)))*F)/((R*T)*(1.-((exp((((-2.*(V-(50.)))*F)/(R*T))))))))*((sv->Cai*(exp(((100.*F)/(R*T)))))-((Cao*(exp((((-2.*(V-(50.)))*F)/(R*T)))))))));
  GlobalData_t ICaLK = ((V==50.) ? ((((0.002*sv->d)*sv->f)*P_CaLCa)*((sv->Ki*(exp(((50.*F)/(R*T)))))-(Ko))) : (((((((0.002*sv->d)*sv->f)*P_CaLCa)*(V-(50.)))*F)/((R*T)*(1.-((exp((((-(V-(50.)))*F)/(R*T))))))))*((sv->Ki*(exp(((50.*F)/(R*T)))))-((Ko*(exp((((-(V-(50.)))*F)/(R*T)))))))));
  GlobalData_t ICaLNa = ((V==50.) ? ((((0.01*sv->d)*sv->f)*P_CaLCa)*((sv->Nai*(exp(((50.*F)/(R*T)))))-(Nao))) : (((((((0.01*sv->d)*sv->f)*P_CaLCa)*(V-(50.)))*F)/((R*T)*(1.-((exp((((-(V-(50.)))*F)/(R*T))))))))*((sv->Nai*(exp(((50.*F)/(R*T)))))-((Nao*(exp((((-(V-(50.)))*F)/(R*T)))))))));
  GlobalData_t ICa_L = ((ICaLCa+ICaLK)+ICaLNa);
  //Output the desired variables
  fprintf(file, "%4.12f\t", sv->Cai);
  fprintf(file, "%4.12f\t", sv->Cmdn_Ca);
  fprintf(file, "%4.12f\t", sv->F_CaMK);
  fprintf(file, "%4.12f\t", ICa_L);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        