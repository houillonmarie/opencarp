// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Title: An analysis of deformation-dependent electromechanical coupling in the mouse heart
*  Authors: Sander Land, Steven A Niederer, Jan Magnus Aronsen, Emil K S Espe, Lili Zhang, William E Louch, Ivar Sjaastad, Ole M Sejersted, Nicolas P Smith
*  Year: 2012
*  Journal: Journal of Physiology 2012;590:4553-69
*  DOI: 10.1113/jphysiol.2012.231928
*  Comment: Rat ventricular active stress model (plugin)
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Stress_Land12.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Stress_Land12(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Stress_Land12( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define Q_1_init (GlobalData_t)(0.)
#define Q_2_init (GlobalData_t)(0.)
#define TRPN_init (GlobalData_t)(0.0752)
#define delta_sl_init (GlobalData_t)(0.)
#define length_init (GlobalData_t)(1.)
#define partial_A_1_del_Q_1 (GlobalData_t)(0.)
#define partial_A_2_del_Q_2 (GlobalData_t)(0.)
#define partial_Ca_50_del_TRPN (GlobalData_t)(0.)
#define partial_Ca_50ref_del_TRPN (GlobalData_t)(0.)
#define partial_TRPN_50_del_xb (GlobalData_t)(0.)
#define partial_alpha_1_del_Q_1 (GlobalData_t)(0.)
#define partial_alpha_2_del_Q_2 (GlobalData_t)(0.)
#define partial_beta_1_del_TRPN (GlobalData_t)(0.)
#define partial_dlambdadt_del_Q_1 (GlobalData_t)(0.)
#define partial_dlambdadt_del_Q_2 (GlobalData_t)(0.)
#define partial_k_TRPN_del_TRPN (GlobalData_t)(0.)
#define partial_k_xb_del_xb (GlobalData_t)(0.)
#define partial_lambda_del_TRPN (GlobalData_t)(0.)
#define partial_lambda_m_del_TRPN (GlobalData_t)(0.)
#define partial_n_TRPN_del_TRPN (GlobalData_t)(0.)
#define partial_n_xb_del_xb (GlobalData_t)(0.)
#define partial_permtot_del_xb (GlobalData_t)(0.)
#define set_Q_1_tozero_in_A_1 (GlobalData_t)(-29.)
#define set_Q_1_tozero_in_alpha_1 (GlobalData_t)(0.1)
#define set_Q_2_tozero_in_A_2 (GlobalData_t)(116.)
#define set_Q_2_tozero_in_alpha_2 (GlobalData_t)(0.5)
#define set_TRPN_tozero_in_Ca_50ref (GlobalData_t)(0.8)
#define set_TRPN_tozero_in_beta_1 (GlobalData_t)(-1.5)
#define set_TRPN_tozero_in_k_TRPN (GlobalData_t)(0.1)
#define set_TRPN_tozero_in_n_TRPN (GlobalData_t)(2.)
#define set_xb_tozero_in_TRPN_50 (GlobalData_t)(0.35)
#define set_xb_tozero_in_k_xb (GlobalData_t)(0.1)
#define set_xb_tozero_in_n_xb (GlobalData_t)(5.)
#define xb_init (GlobalData_t)(0.00046)



void initialize_params_Stress_Land12( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Stress_Land12_Params *p = (Stress_Land12_Params *)IF->params;

  // Compute the regional constants
  {
    p->A_1 = -29.;
    p->A_2 = 116.;
    p->Ca_50ref = 0.8;
    p->TRPN_50 = 0.35;
    p->T_ref = 120.;
    p->a = 0.35;
    p->alpha_1 = 0.1;
    p->alpha_2 = 0.5;
    p->beta_0 = 1.65;
    p->beta_1 = -1.5;
    p->k_TRPN = 0.1;
    p->k_xb = 0.1;
    p->n_TRPN = 2.;
    p->n_xb = 5.;
  }
  // Compute the regional initialization
  {
  }

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_Stress_Land12( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Land12_Params *p = (Stress_Land12_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double partial_diff_Q_1_del_Q_1 = (-p->alpha_1);
  double partial_diff_Q_2_del_Q_2 = (-p->alpha_2);
  double Q_1_rush_larsen_B = (exp((dt*partial_diff_Q_1_del_Q_1)));
  double Q_2_rush_larsen_B = (exp((dt*partial_diff_Q_2_del_Q_2)));

}



void    initialize_sv_Stress_Land12( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Land12_Params *p = (Stress_Land12_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Stress_Land12_state) );
  Stress_Land12_state *sv_base = (Stress_Land12_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double partial_diff_Q_1_del_Q_1 = (-p->alpha_1);
  double partial_diff_Q_2_del_Q_2 = (-p->alpha_2);
  double Q_1_rush_larsen_B = (exp((dt*partial_diff_Q_1_del_Q_1)));
  double Q_2_rush_larsen_B = (exp((dt*partial_diff_Q_2_del_Q_2)));
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Ca_i_sizeof;
  int __Ca_i_offset;
  SVgetfcn __Ca_i_SVgetfcn = get_sv_offset( IF->parent->type, "Ca_i", &__Ca_i_offset, &__Ca_i_sizeof );
  SVputfcn __Ca_i_SVputfcn = __Ca_i_SVgetfcn ? getPutSV(__Ca_i_SVgetfcn) : NULL;

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Stress_Land12_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Ca_i = __Ca_i_SVgetfcn ? __Ca_i_SVgetfcn(IF->parent, __i, __Ca_i_offset) :sv->__Ca_i_local;
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Q_1 = Q_1_init;
    sv->Q_2 = Q_2_init;
    sv->TRPN = TRPN_init;
    delta_sl = delta_sl_init;
    length = length_init;
    sv->xb = xb_init;
    double Q = (sv->Q_1+sv->Q_2);
    double lambda = length;
    double lambda_m = ((lambda>1.2) ? 1.2 : lambda);
    double lambda_s = ((lambda_m>0.87) ? 0.87 : lambda_m);
    double overlap = (1.+(p->beta_0*((lambda_m+lambda_s)-(1.87))));
    double T_0 = ((p->T_ref*sv->xb)*overlap);
    Tension = ((Q<0.) ? ((T_0*((p->a*Q)+1.))/(1.-(Q))) : ((T_0*(1.+((p->a+2.)*Q)))/(1.+Q)));
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Ca_i_SVputfcn ) 
    	__Ca_i_SVputfcn(IF->parent, __i, __Ca_i_offset, Ca_i);
    else
    	sv->__Ca_i_local=Ca_i;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Stress_Land12(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  Stress_Land12_Params *p  = (Stress_Land12_Params *)IF->params;
  Stress_Land12_state *sv_base = (Stress_Land12_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t partial_diff_Q_1_del_Q_1 = (-p->alpha_1);
  GlobalData_t partial_diff_Q_2_del_Q_2 = (-p->alpha_2);
  GlobalData_t Q_1_rush_larsen_B = (exp((dt*partial_diff_Q_1_del_Q_1)));
  GlobalData_t Q_2_rush_larsen_B = (exp((dt*partial_diff_Q_2_del_Q_2)));
  //Prepare all the public arrays.
  GlobalData_t *Tension_ext = impdata[Tension];
  GlobalData_t *delta_sl_ext = impdata[delLambda];
  GlobalData_t *length_ext = impdata[Lambda];
  //Prepare all the private functions.
  
  int __Ca_i_sizeof;
  int __Ca_i_offset;
  SVgetfcn __Ca_i_SVgetfcn = get_sv_offset( IF->parent->type, "Ca_i", &__Ca_i_offset, &__Ca_i_sizeof );
  SVputfcn __Ca_i_SVputfcn = __Ca_i_SVgetfcn ? getPutSV(__Ca_i_SVgetfcn) : NULL;

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Stress_Land12_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Tension = Tension_ext[__i];
    GlobalData_t delta_sl = delta_sl_ext[__i];
    GlobalData_t length = length_ext[__i];
    GlobalData_t Ca_i = __Ca_i_SVgetfcn ? __Ca_i_SVgetfcn(IF->parent, __i, __Ca_i_offset) :sv->__Ca_i_local;
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t Q = (sv->Q_1+sv->Q_2);
    GlobalData_t lambda = length;
    GlobalData_t lambda_m = ((lambda>1.2) ? 1.2 : lambda);
    GlobalData_t lambda_s = ((lambda_m>0.87) ? 0.87 : lambda_m);
    GlobalData_t overlap = (1.+(p->beta_0*((lambda_m+lambda_s)-(1.87))));
    GlobalData_t T_0 = ((p->T_ref*sv->xb)*overlap);
    Tension = ((Q<0.) ? ((T_0*((p->a*Q)+1.))/(1.-(Q))) : ((T_0*(1.+((p->a+2.)*Q)))/(1.+Q)));
    
    
    //Complete Forward Euler Update
    
    
    //Complete Rush Larsen Update
    GlobalData_t Ca_50 = (p->Ca_50ref*(1.+(p->beta_1*(lambda_m-(1.)))));
    GlobalData_t permtot = (sqrt((pow((sv->TRPN/p->TRPN_50),p->n_xb))));
    GlobalData_t set_Q_1_tozero_in_diff_Q_1 = (-29.*delta_sl);
    GlobalData_t set_Q_2_tozero_in_diff_Q_2 = (116.*delta_sl);
    GlobalData_t set_TRPN_tozero_in_diff_TRPN = (0.1*(pow((Ca_i/(0.8*(1.+(-1.5*(((length>1.2) ? 1.2 : length)-(1.)))))),2.)));
    GlobalData_t set_xb_tozero_in_diff_xb = (0.1*(sqrt((pow((sv->TRPN/0.35),5.)))));
    GlobalData_t Q_1_rush_larsen_A = ((set_Q_1_tozero_in_diff_Q_1/partial_diff_Q_1_del_Q_1)*(expm1((dt*partial_diff_Q_1_del_Q_1))));
    GlobalData_t Q_2_rush_larsen_A = ((set_Q_2_tozero_in_diff_Q_2/partial_diff_Q_2_del_Q_2)*(expm1((dt*partial_diff_Q_2_del_Q_2))));
    GlobalData_t partial_diff_TRPN_del_TRPN = (p->k_TRPN*(((pow((Ca_i/Ca_50),p->n_TRPN))*-1.)-(1.)));
    GlobalData_t partial_diff_xb_del_xb = (p->k_xb*((permtot*-1.)-((1./permtot))));
    GlobalData_t TRPN_rush_larsen_A = ((set_TRPN_tozero_in_diff_TRPN/partial_diff_TRPN_del_TRPN)*(expm1((dt*partial_diff_TRPN_del_TRPN))));
    GlobalData_t TRPN_rush_larsen_B = (exp((dt*partial_diff_TRPN_del_TRPN)));
    GlobalData_t xb_rush_larsen_A = ((set_xb_tozero_in_diff_xb/partial_diff_xb_del_xb)*(expm1((dt*partial_diff_xb_del_xb))));
    GlobalData_t xb_rush_larsen_B = (exp((dt*partial_diff_xb_del_xb)));
    GlobalData_t Q_1_new = Q_1_rush_larsen_A+Q_1_rush_larsen_B*sv->Q_1;
    GlobalData_t Q_2_new = Q_2_rush_larsen_A+Q_2_rush_larsen_B*sv->Q_2;
    GlobalData_t TRPN_new = TRPN_rush_larsen_A+TRPN_rush_larsen_B*sv->TRPN;
    GlobalData_t xb_new = xb_rush_larsen_A+xb_rush_larsen_B*sv->xb;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    sv->Q_1 = Q_1_new;
    sv->Q_2 = Q_2_new;
    sv->TRPN = TRPN_new;
    Tension = Tension;
    sv->xb = xb_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Tension_ext[__i] = Tension;
    delta_sl_ext[__i] = delta_sl;
    length_ext[__i] = length;
    if( __Ca_i_SVputfcn ) 
    	__Ca_i_SVputfcn(IF->parent, __i, __Ca_i_offset, Ca_i);
    else
    	sv->__Ca_i_local=Ca_i;

  }


}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        