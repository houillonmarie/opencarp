// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Williams JC, Xu J, Lu Z, Klimas A, Chen X, Ambrosi CM, Cohen IS, Entcheva E.
*  Year: 2013
*  Title: Computational Optogenetics: Empirically-Derived Voltage- and Light-Sensitive Channelrhodopsin-2 Model
*  Journal: PLoS Comput Biol. 9(9):e1003220
*  Journal: 10.1371/journal.pcbi.1003220
*  DOI: 10.1371/journal.pcbi.1003220
*  Comment: Implementd by Boyle PM (pmjboyle@uw.edu), Williams JC, Ambrosi CM, Entcheva E, Trayanova NA descirbed in A comprehensive multiscale framework for simulating optogenetics in the heart
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __ICHR2_WILLIAMSXU_H__
#define __ICHR2_WILLIAMSXU_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define IChR2_WilliamsXu_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG|illum_DATA_FLAG
#define IChR2_WilliamsXu_MODDAT Iion_DATA_FLAG

struct IChR2_WilliamsXu_Params {
    GlobalData_t Acell;
    GlobalData_t GChR2;
    GlobalData_t Gd2;
    GlobalData_t Temp;
    GlobalData_t attenuation;
    GlobalData_t c2;
    GlobalData_t e12_c1;
    GlobalData_t e12d;
    GlobalData_t e21_c1;
    GlobalData_t e21d;
    GlobalData_t gam;
    GlobalData_t lambda;
    GlobalData_t qeff1;
    GlobalData_t qeff2;
    GlobalData_t sigret;
    GlobalData_t tau_ChR2;
    GlobalData_t wloss;

};

struct IChR2_WilliamsXu_state {
    GlobalData_t C1;
    GlobalData_t C2;
    GlobalData_t GChR2;
    GlobalData_t O2;
    GlobalData_t attenuation;
    GlobalData_t pp;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_IChR2_WilliamsXu(ION_IF *);
void construct_tables_IChR2_WilliamsXu(ION_IF *);
void destroy_IChR2_WilliamsXu(ION_IF *);
void initialize_sv_IChR2_WilliamsXu(ION_IF *, GlobalData_t**);
void compute_IChR2_WilliamsXu(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
