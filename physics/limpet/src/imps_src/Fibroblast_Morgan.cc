// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Morgan R, Colman MA, Chubb H, Seemann G and Aslanidi OV
*  Year: 2016
*  Title: Slow Conduction in the Border Zones of Patchy Fibrosis Stabilizes the Drivers for Atrial Fibrillation: Insights from Multi-Scale Human Atrial Modeling
*  Journal: Front. Physiol. 7,474
*  DOI: 10.3389/fphys.2016.00474
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "Fibroblast_Morgan.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_Fibroblast_Morgan(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_Fibroblast_Morgan( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define B (GlobalData_t)(-200.0)
#define Cm (GlobalData_t)(6.3)
#define F (GlobalData_t)(96487.0)
#define GK1 (GlobalData_t)(0.4822)
#define K_Q10 (GlobalData_t)(3.)
#define K_mK (GlobalData_t)(1.0)
#define K_mNa (GlobalData_t)(11.0)
#define K_mNa_p15 (GlobalData_t)(36.482872694)
#define R (GlobalData_t)(8314.0)
#define T (GlobalData_t)(306.15)
#define V_rev (GlobalData_t)(-150.0)
#define Voli (GlobalData_t)(0.00137)
#define Volo (GlobalData_t)(0.0008)
#define oa_init (GlobalData_t)(3.04e-2)
#define oi_init (GlobalData_t)(0.999)
#define ua_init (GlobalData_t)(4.96e-3)
#define ui_init (GlobalData_t)(0.999)



void initialize_params_Fibroblast_Morgan( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  Fibroblast_Morgan_Params *p = (Fibroblast_Morgan_Params *)IF->params;

  // Compute the regional constants
  {
    p->GbNa = 0.0095;
    p->Ggj = 0.5;
    p->Gto = 0.1652;
    p->Ki_init = 140.045865;
    p->Ko = 5.4;
    p->Nai_init = 9.954213;
    p->Nao = 130.0;
    p->Vfb_init = -42.5;
    p->fact_gkur = 0.6;
    p->fact_gt0 = 0.1;
    p->fb_myo_surf_ratio = (6.3/185.);
    p->fb_surf = 630.;
    p->maxINaK = 1.4;
    p->num_fb = 2.;
  }
  // Compute the regional initialization
  {
  }
  IF->type->trace = trace_Fibroblast_Morgan;

}


// Define the parameters for the lookup tables
enum Tables {
  Ki_TAB,
  Nai_TAB,
  Vfb_TAB,
  Vmyo_TAB,

  N_TABS
};

// Define the indices into the lookup tables.
enum Ki_TableIndex {
  E_K_idx,
  NROWS_Ki
};

enum Nai_TableIndex {
  E_Na_idx,
  Nai_15_idx,
  NROWS_Nai
};

enum Vfb_TableIndex {
  Ins_idx,
  i_K1_f_idx,
  oa_rush_larsen_A_idx,
  oa_rush_larsen_B_idx,
  oi_rush_larsen_A_idx,
  oi_rush_larsen_B_idx,
  ua_rush_larsen_A_idx,
  ua_rush_larsen_B_idx,
  ui_rush_larsen_A_idx,
  ui_rush_larsen_B_idx,
  NROWS_Vfb
};

enum Vmyo_TableIndex {
  gKur_idx,
  NROWS_Vmyo
};



void construct_tables_Fibroblast_Morgan( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e-3;
  cell_geom *region = &IF->cgeom;
  Fibroblast_Morgan_Params *p = (Fibroblast_Morgan_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.
  double Ggj_density = ((p->Ggj*100.)/p->fb_surf);
  double Gto_f = (p->fact_gt0*p->Gto);
  
  // Create the Ki lookup table
  LUT* Ki_tab = &IF->tables[Ki_TAB];
  LUT_alloc(Ki_tab, NROWS_Ki, 0.01, 500, 0.01, "Fibroblast_Morgan Ki");
  for (int __i=Ki_tab->mn_ind; __i<=Ki_tab->mx_ind; __i++) {
    double Ki = Ki_tab->res*__i;
    LUT_data_t* Ki_row = Ki_tab->tab[__i];
    Ki_row[E_K_idx] = (((R*T)/F)*(log((p->Ko/Ki))));
  }
  check_LUT(Ki_tab);
  
  
  // Create the Nai lookup table
  LUT* Nai_tab = &IF->tables[Nai_TAB];
  LUT_alloc(Nai_tab, NROWS_Nai, 0.01, 1000, 0.01, "Fibroblast_Morgan Nai");
  for (int __i=Nai_tab->mn_ind; __i<=Nai_tab->mx_ind; __i++) {
    double Nai = Nai_tab->res*__i;
    LUT_data_t* Nai_row = Nai_tab->tab[__i];
    Nai_row[E_Na_idx] = (((R*T)/F)*(log((p->Nao/Nai))));
    Nai_row[Nai_15_idx] = (pow(Nai,1.5));
  }
  check_LUT(Nai_tab);
  
  
  // Create the Vfb lookup table
  LUT* Vfb_tab = &IF->tables[Vfb_TAB];
  LUT_alloc(Vfb_tab, NROWS_Vfb, -100, 100, 0.005, "Fibroblast_Morgan Vfb");
  for (int __i=Vfb_tab->mn_ind; __i<=Vfb_tab->mx_ind; __i++) {
    double Vfb = Vfb_tab->res*__i;
    LUT_data_t* Vfb_row = Vfb_tab->tab[__i];
    Vfb_row[Ins_idx] = (0.018*Vfb);
    double V_EK = (Vfb+86.75);
    double aa_oa = (0.65/((exp(((Vfb+10.)/-8.5)))+(exp(((30.-(Vfb))/59.0)))));
    double aa_oi = (1./(18.53+(exp(((Vfb+113.7)/10.95)))));
    double aa_ua = (0.65/((exp(((Vfb+10.)/-8.5)))+(exp(((Vfb-(30.))/-59.0)))));
    double aa_ui = (1./(21.+(exp(((Vfb-(185.))/-28.)))));
    double bb_oa = (0.65/(2.5+(exp(((Vfb+82.)/17.)))));
    double bb_oi = (1./(35.56+(exp(((-(Vfb+1.26))/7.44)))));
    double bb_ua = (0.65/(2.5+(exp(((Vfb+82.)/17.)))));
    double bb_ui = (exp(((Vfb-(158.))/16.)));
    double fact_R = (1.+(exp((0.05*(Vfb+20.0)))));
    double oa_inf = (1./(1.+(exp(((-(Vfb+20.47))/17.54)))));
    double oi_inf = (1./(1.+(exp(((Vfb+43.1)/5.3)))));
    double ua_inf = (1./(1.+(exp(((Vfb+30.3)/-9.6)))));
    double ui_inf = (1./(1.+(exp(((Vfb-(99.45))/27.48)))));
    Vfb_row[i_K1_f_idx] = ((0.03*V_EK)/fact_R);
    double tau_oa = ((1./(aa_oa+bb_oa))/K_Q10);
    double tau_oi = ((1./(aa_oi+bb_oi))/K_Q10);
    double tau_ua = ((1./(aa_ua+bb_ua))/K_Q10);
    double tau_ui = ((1./(aa_ui+bb_ui))/K_Q10);
    Vfb_row[oa_rush_larsen_B_idx] = (exp(((-dt)/tau_oa)));
    double oa_rush_larsen_C = (expm1(((-dt)/tau_oa)));
    Vfb_row[oi_rush_larsen_B_idx] = (exp(((-dt)/tau_oi)));
    double oi_rush_larsen_C = (expm1(((-dt)/tau_oi)));
    Vfb_row[ua_rush_larsen_B_idx] = (exp(((-dt)/tau_ua)));
    double ua_rush_larsen_C = (expm1(((-dt)/tau_ua)));
    Vfb_row[ui_rush_larsen_B_idx] = (exp(((-dt)/tau_ui)));
    double ui_rush_larsen_C = (expm1(((-dt)/tau_ui)));
    Vfb_row[oa_rush_larsen_A_idx] = ((-oa_inf)*oa_rush_larsen_C);
    Vfb_row[oi_rush_larsen_A_idx] = ((-oi_inf)*oi_rush_larsen_C);
    Vfb_row[ua_rush_larsen_A_idx] = ((-ua_inf)*ua_rush_larsen_C);
    Vfb_row[ui_rush_larsen_A_idx] = ((-ui_inf)*ui_rush_larsen_C);
  }
  check_LUT(Vfb_tab);
  
  
  // Create the Vmyo lookup table
  LUT* Vmyo_tab = &IF->tables[Vmyo_TAB];
  LUT_alloc(Vmyo_tab, NROWS_Vmyo, -100, 100, 0.005, "Fibroblast_Morgan Vmyo");
  for (int __i=Vmyo_tab->mn_ind; __i<=Vmyo_tab->mx_ind; __i++) {
    double Vmyo = Vmyo_tab->res*__i;
    LUT_data_t* Vmyo_row = Vmyo_tab->tab[__i];
    double g_Kur = (0.005+(0.05/(1.+(exp(((15.-(Vmyo))/13.))))));
    Vmyo_row[gKur_idx] = (p->fact_gkur*g_Kur);
  }
  check_LUT(Vmyo_tab);
  

}



void    initialize_sv_Fibroblast_Morgan( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e-3;
  cell_geom *region = &IF->cgeom;
  Fibroblast_Morgan_Params *p = (Fibroblast_Morgan_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(Fibroblast_Morgan_state) );
  Fibroblast_Morgan_state *sv_base = (Fibroblast_Morgan_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  double Ggj_density = ((p->Ggj*100.)/p->fb_surf);
  double Gto_f = (p->fact_gt0*p->Gto);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vmyo_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    Fibroblast_Morgan_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Vmyo = Vmyo_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    // Initialize the rest of the nodal variables
    sv->Ki = p->Ki_init;
    sv->Nai = p->Nai_init;
    sv->Vfb = p->Vfb_init;
    sv->oa = oa_init;
    sv->oi = oi_init;
    sv->ua = ua_init;
    sv->ui = ui_init;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Vmyo_ext[__i] = Vmyo;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_Fibroblast_Morgan(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e-3;
  cell_geom *region = &IF->cgeom;
  Fibroblast_Morgan_Params *p  = (Fibroblast_Morgan_Params *)IF->params;
  Fibroblast_Morgan_state *sv_base = (Fibroblast_Morgan_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Ggj_density = ((p->Ggj*100.)/p->fb_surf);
  GlobalData_t Gto_f = (p->fact_gt0*p->Gto);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vmyo_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    Fibroblast_Morgan_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t Vmyo = Vmyo_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    LUT_data_t Ki_row[NROWS_Ki];
    LUT_interpRow(&IF->tables[Ki_TAB], sv->Ki, __i, Ki_row);
    LUT_data_t Nai_row[NROWS_Nai];
    LUT_interpRow(&IF->tables[Nai_TAB], sv->Nai, __i, Nai_row);
    LUT_data_t Vfb_row[NROWS_Vfb];
    LUT_interpRow(&IF->tables[Vfb_TAB], sv->Vfb, __i, Vfb_row);
    LUT_data_t Vmyo_row[NROWS_Vmyo];
    LUT_interpRow(&IF->tables[Vmyo_TAB], Vmyo, __i, Vmyo_row);
    
    
    //Compute storevars and external modvars
    GlobalData_t I_gap = (Ggj_density*(Vmyo-(sv->Vfb)));
    GlobalData_t Igj = ((I_gap*p->num_fb)*p->fb_myo_surf_ratio);
    Iion = (Iion+Igj);
    
    
    //Complete Forward Euler Update
    GlobalData_t INaK = ((((p->maxINaK*(p->Ko/(p->Ko+K_mK)))*(Nai_row[Nai_15_idx]/(Nai_row[Nai_15_idx]+K_mNa_p15)))*(sv->Vfb-(V_rev)))/(sv->Vfb-(B)));
    GlobalData_t V_FK = (sv->Vfb-(Ki_row[E_K_idx]));
    GlobalData_t V_FNa = (sv->Vfb-(Nai_row[E_Na_idx]));
    GlobalData_t IKur = (((((Vmyo_row[gKur_idx]*sv->ua)*sv->ua)*sv->ua)*sv->ui)*V_FK);
    GlobalData_t IbNa = (p->GbNa*V_FNa);
    GlobalData_t Ito = (((((Gto_f*sv->oa)*sv->oa)*sv->oa)*sv->oi)*V_FK);
    GlobalData_t Ikv = (IKur+Ito);
    GlobalData_t diff_Ki = (Cm*((-(((Vfb_row[i_K1_f_idx]+Ito)+IKur)-((2.*INaK))))/(Voli*F)));
    GlobalData_t diff_Nai = (Cm*(((-IbNa)-((3.*INaK)))/(Voli*F)));
    GlobalData_t Itot = ((((INaK+IbNa)+Ikv)+Vfb_row[Ins_idx])+Vfb_row[i_K1_f_idx]);
    GlobalData_t Ifb = (Itot-(I_gap));
    GlobalData_t diff_Vfb = (-Ifb);
    GlobalData_t Ki_new = sv->Ki+diff_Ki*dt;
    GlobalData_t Nai_new = sv->Nai+diff_Nai*dt;
    GlobalData_t Vfb_new = sv->Vfb+diff_Vfb*dt;
    
    
    //Complete Rush Larsen Update
    GlobalData_t oa_rush_larsen_B = Vfb_row[oa_rush_larsen_B_idx];
    GlobalData_t oi_rush_larsen_B = Vfb_row[oi_rush_larsen_B_idx];
    GlobalData_t ua_rush_larsen_B = Vfb_row[ua_rush_larsen_B_idx];
    GlobalData_t ui_rush_larsen_B = Vfb_row[ui_rush_larsen_B_idx];
    GlobalData_t oa_rush_larsen_A = Vfb_row[oa_rush_larsen_A_idx];
    GlobalData_t oi_rush_larsen_A = Vfb_row[oi_rush_larsen_A_idx];
    GlobalData_t ua_rush_larsen_A = Vfb_row[ua_rush_larsen_A_idx];
    GlobalData_t ui_rush_larsen_A = Vfb_row[ui_rush_larsen_A_idx];
    GlobalData_t oa_new = oa_rush_larsen_A+oa_rush_larsen_B*sv->oa;
    GlobalData_t oi_new = oi_rush_larsen_A+oi_rush_larsen_B*sv->oi;
    GlobalData_t ua_new = ua_rush_larsen_A+ua_rush_larsen_B*sv->ua;
    GlobalData_t ui_new = ui_rush_larsen_A+ui_rush_larsen_B*sv->ui;
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    sv->Ki = Ki_new;
    sv->Nai = Nai_new;
    sv->Vfb = Vfb_new;
    sv->oa = oa_new;
    sv->oi = oi_new;
    sv->ua = ua_new;
    sv->ui = ui_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    Vmyo_ext[__i] = Vmyo;

  }


}


void trace_Fibroblast_Morgan(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("Fibroblast_Morgan_trace_header.txt","wt");
    fprintf(theader->fd,
        "IKur\n"
        "INaK\n"
        "IbNa\n"
        "Ifb\n"
        "Igj\n"
        "Iion\n"
        "Ikv\n"
        "Ins\n"
        "Ito\n"
        "sv->Vfb\n"
        "Vmyo\n"
        "i_K1_f\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e-3;
  cell_geom *region = &IF->cgeom;
  Fibroblast_Morgan_Params *p  = (Fibroblast_Morgan_Params *)IF->params;

  Fibroblast_Morgan_state *sv_base = (Fibroblast_Morgan_state *)IF->sv_tab.y;
  Fibroblast_Morgan_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  GlobalData_t Ggj_density = ((p->Ggj*100.)/p->fb_surf);
  GlobalData_t Gto_f = (p->fact_gt0*p->Gto);
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *Vmyo_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t Vmyo = Vmyo_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t E_K = (((R*T)/F)*(log((p->Ko/sv->Ki))));
  GlobalData_t E_Na = (((R*T)/F)*(log((p->Nao/sv->Nai))));
  GlobalData_t I_gap = (Ggj_density*(Vmyo-(sv->Vfb)));
  GlobalData_t Ins = (0.018*sv->Vfb);
  GlobalData_t Nai_15 = (pow(sv->Nai,1.5));
  GlobalData_t V_EK = (sv->Vfb+86.75);
  GlobalData_t fact_R = (1.+(exp((0.05*(sv->Vfb+20.0)))));
  GlobalData_t g_Kur = (0.005+(0.05/(1.+(exp(((15.-(Vmyo))/13.))))));
  GlobalData_t INaK = ((((p->maxINaK*(p->Ko/(p->Ko+K_mK)))*(Nai_15/(Nai_15+K_mNa_p15)))*(sv->Vfb-(V_rev)))/(sv->Vfb-(B)));
  GlobalData_t Igj = ((I_gap*p->num_fb)*p->fb_myo_surf_ratio);
  GlobalData_t V_FK = (sv->Vfb-(E_K));
  GlobalData_t V_FNa = (sv->Vfb-(E_Na));
  GlobalData_t gKur = (p->fact_gkur*g_Kur);
  GlobalData_t i_K1_f = ((0.03*V_EK)/fact_R);
  GlobalData_t IKur = (((((gKur*sv->ua)*sv->ua)*sv->ua)*sv->ui)*V_FK);
  GlobalData_t IbNa = (p->GbNa*V_FNa);
  GlobalData_t Ito = (((((Gto_f*sv->oa)*sv->oa)*sv->oa)*sv->oi)*V_FK);
  GlobalData_t Ikv = (IKur+Ito);
  GlobalData_t Itot = ((((INaK+IbNa)+Ikv)+Ins)+i_K1_f);
  GlobalData_t Ifb = (Itot-(I_gap));
  //Output the desired variables
  fprintf(file, "%4.12f\t", IKur);
  fprintf(file, "%4.12f\t", INaK);
  fprintf(file, "%4.12f\t", IbNa);
  fprintf(file, "%4.12f\t", Ifb);
  fprintf(file, "%4.12f\t", Igj);
  fprintf(file, "%4.12f\t", Iion);
  fprintf(file, "%4.12f\t", Ikv);
  fprintf(file, "%4.12f\t", Ins);
  fprintf(file, "%4.12f\t", Ito);
  fprintf(file, "%4.12f\t", sv->Vfb);
  fprintf(file, "%4.12f\t", Vmyo);
  fprintf(file, "%4.12f\t", i_K1_f);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        