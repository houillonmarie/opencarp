// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Maleckar MM, Greenstein JL, Giles WR, Trayanova NA
*  Year: 2009
*  Title: Electrotonic coupling between human atrial myocytes and fibroblasts alters myocyte excitability and repolarization
*  Journal: Biophys J., 97(8), 2179-90
*  DOI: 10.1016/j.bpj.2009.07.054
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __MALECKAR_H__
#define __MALECKAR_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Maleckar_REQDAT Vm_DATA_FLAG
#define Maleckar_MODDAT Iion_DATA_FLAG

struct Maleckar_Params {
    GlobalData_t type;

};

struct Maleckar_state {
    GlobalData_t Ki;
    GlobalData_t Nai;
    Gatetype r_Kv;
    Gatetype s_Kv;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Maleckar(ION_IF *);
void construct_tables_Maleckar(ION_IF *);
void destroy_Maleckar(ION_IF *);
void initialize_sv_Maleckar(ION_IF *, GlobalData_t**);
void compute_Maleckar(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
