// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: DeBruin, K.A., Krassowska, W
*  Year: 1998
*  Title: Electroporation and Shock-Induced Transmembrane Potential in a Cardiac Fiber During Defibrillation Strength Shocks
*  Journal: Annals of Biomedical Engineering 26, 584-596
*  DOI: 10.1114/1.101
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __ELECTROPORATION_DEBRUINKRASSOWSKA98_H__
#define __ELECTROPORATION_DEBRUINKRASSOWSKA98_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Electroporation_DeBruinKrassowska98_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG
#define Electroporation_DeBruinKrassowska98_MODDAT Iion_DATA_FLAG

struct Electroporation_DeBruinKrassowska98_Params {
    GlobalData_t N0;
    GlobalData_t alpha;
    GlobalData_t beta;
    GlobalData_t h;
    GlobalData_t nn;
    GlobalData_t q;
    GlobalData_t sigma;
    GlobalData_t w0;

};

struct Electroporation_DeBruinKrassowska98_state {
    GlobalData_t n;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Electroporation_DeBruinKrassowska98(ION_IF *);
void construct_tables_Electroporation_DeBruinKrassowska98(ION_IF *);
void destroy_Electroporation_DeBruinKrassowska98(ION_IF *);
void initialize_sv_Electroporation_DeBruinKrassowska98(ION_IF *, GlobalData_t**);
void compute_Electroporation_DeBruinKrassowska98(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
