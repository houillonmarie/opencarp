// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Inada S, Shibata Md PhD N, Iwata PhD M, Haraguchi PhD R, Ashihara Md PhD T, Ikeda Md PhD T, Mitsui PhD K, Dobrzynski PhD H, Boyett PhD MR, Nakazawa PhD K.
*  Year: 2017
*  Title: Simulation of ventricular rate control during atrial fibrillation using ionic channel blockers
*  Journal: J Arrhythm. 33(4):302-309
*  DOI: 10.1016/j.joa.2016.12.002
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __INADA_H__
#define __INADA_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Inada_REQDAT Vm_DATA_FLAG
#define Inada_MODDAT Iion_DATA_FLAG

struct Inada_Params {
    GlobalData_t Cm;
    GlobalData_t E_CaL;
    GlobalData_t E_b;
    GlobalData_t GCaL;
    GlobalData_t GK1;
    GlobalData_t GKr;
    GlobalData_t GNa;
    GlobalData_t Gb;
    GlobalData_t Gf;
    GlobalData_t Gst;
    GlobalData_t Gto;
    GlobalData_t cell_type;
    GlobalData_t kNaCa;
    GlobalData_t p_max;
    GlobalData_t p_rel;
    char* flags;

};
static const char* Inada_flags = "AN|N|NH";


struct Inada_state {
    GlobalData_t Cai;
    GlobalData_t Carel;
    GlobalData_t Casub;
    GlobalData_t Caup;
    GlobalData_t GNa;
    Gatetype d;
    Gatetype f1;
    Gatetype f2;
    GlobalData_t f_cmi;
    GlobalData_t f_cms;
    GlobalData_t f_cq;
    GlobalData_t f_csl;
    GlobalData_t f_tc;
    GlobalData_t f_tmc;
    GlobalData_t f_tmm;
    Gatetype h1;
    Gatetype h2;
    Gatetype m;
    Gatetype paf;
    Gatetype pas;
    Gatetype pi;
    Gatetype qa;
    Gatetype qi;
    Gatetype x;
    Gatetype y;
    Gatetype yf;
    Gatetype ys;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Inada(ION_IF *);
void construct_tables_Inada(ION_IF *);
void destroy_Inada(ION_IF *);
void initialize_sv_Inada(ION_IF *, GlobalData_t**);
void compute_Inada(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
