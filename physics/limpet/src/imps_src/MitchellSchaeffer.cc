// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Mitchell, C.C., Schaeffer, D.G
*  Year: 2003
*  Title: A two-current model for the dynamics of cardiac membrane
*  Journal: Bull. Math. Biol. 65, 767-793
*  DOI: 10.1016/S0092-8240(03)00041-7
*
*/
        

// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

#include "ION_IF.h"
#include "MitchellSchaeffer.h"

#ifdef _OPENMP
#include <omp.h>
#endif



namespace limpet {

using ::opencarp::f_open;
using ::opencarp::FILE_SPEC;

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void trace_MitchellSchaeffer(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata);

void destroy_MitchellSchaeffer( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
  // rarely need to do anything else
}

// Define all constants
#define h_init (GlobalData_t)(1.0)



void initialize_params_MitchellSchaeffer( ION_IF *IF )
{
  cell_geom *region = &IF->cgeom;
  MitchellSchaeffer_Params *p = (MitchellSchaeffer_Params *)IF->params;

  // Compute the regional constants
  {
  }
  // Compute the regional initialization
  {
    p->V_gate = 0.13;
    p->V_max = 1.0;
    p->V_min = 0.0;
    p->a_crit = 0.0;
    p->tau_close = 150.0;
    p->tau_in = 0.3;
    p->tau_open = 120.0;
    p->tau_out = 5.0;
  }
  IF->type->trace = trace_MitchellSchaeffer;

}


// Define the parameters for the lookup tables
enum Tables {

  N_TABS
};

// Define the indices into the lookup tables.


void construct_tables_MitchellSchaeffer( ION_IF *IF )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  MitchellSchaeffer_Params *p = (MitchellSchaeffer_Params *)IF->params;

  IF->numLUT = N_TABS;
  IF->tables = (LUT *)IMP_malloc( N_TABS, sizeof(LUT) );

  // Define the constants that depend on the parameters.

}



void    initialize_sv_MitchellSchaeffer( ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  MitchellSchaeffer_Params *p = (MitchellSchaeffer_Params *)IF->params;

  SV_alloc( &IF->sv_tab, IF->numNode, sizeof(MitchellSchaeffer_state) );
  MitchellSchaeffer_state *sv_base = (MitchellSchaeffer_state *)IF->sv_tab.y;
  GlobalData_t t = 0;
  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

  //set the initial values
  for(int __i=0; __i<IF->sv_tab.numSeg; __i++ ){
    MitchellSchaeffer_state *sv = sv_base+__i;

    // Initialize nodal variables that have been declared with param
    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    sv->V_gate = p->V_gate;
    sv->V_max = p->V_max;
    sv->V_min = p->V_min;
    sv->a_crit = p->a_crit;
    sv->tau_close = p->tau_close;
    sv->tau_in = p->tau_in;
    sv->tau_open = p->tau_open;
    sv->tau_out = p->tau_out;
    // Initialize the rest of the nodal variables
    double Uamp = (sv->V_max-(sv->V_min));
    double V_init = sv->V_min;
    sv->h = h_init;
    V = V_init;
    double Jin = ((((sv->h*((V-(sv->V_min))/Uamp))*(((V-(sv->V_min))/Uamp)-(sv->a_crit)))*((sv->V_max-(V))/Uamp))/sv->tau_in);
    double Jout = (-(((V-(sv->V_min))/Uamp)/sv->tau_out));
    Iion = ((-Uamp)*(Jin+Jout));
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }

}

/** compute the  current
 *
 * param start   index of first node
 * param end     index of last node
 * param IF      IMP
 * param plgdata external data needed by IMP
 */
void compute_MitchellSchaeffer(int start, int end, ION_IF *IF, GlobalData_t **impdata )
{
  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  MitchellSchaeffer_Params *p  = (MitchellSchaeffer_Params *)IF->params;
  MitchellSchaeffer_state *sv_base = (MitchellSchaeffer_state *)IF->sv_tab.y;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.

#pragma omp parallel for schedule(static)
  for (int __i=start; __i<end; __i++) {
    MitchellSchaeffer_state *sv = sv_base+__i;

    //Initialize the external vars to their current values
    GlobalData_t Iion = Iion_ext[__i];
    GlobalData_t V = V_ext[__i];
    //Change the units of external variables as appropriate.
    
    
    //Compute lookup tables for things that have already been defined.
    
    
    //Compute storevars and external modvars
    GlobalData_t Uamp = (sv->V_max-(sv->V_min));
    GlobalData_t Jin = ((((sv->h*((V-(sv->V_min))/Uamp))*(((V-(sv->V_min))/Uamp)-(sv->a_crit)))*((sv->V_max-(V))/Uamp))/sv->tau_in);
    GlobalData_t Jout = (-(((V-(sv->V_min))/Uamp)/sv->tau_out));
    Iion = ((-Uamp)*(Jin+Jout));
    
    
    //Complete Forward Euler Update
    GlobalData_t U = ((V-(sv->V_min))/Uamp);
    GlobalData_t diff_h = ((U<sv->V_gate) ? ((1.-(sv->h))/sv->tau_open) : ((-sv->h)/sv->tau_close));
    GlobalData_t h_new = sv->h+diff_h*dt;
    
    
    //Complete Rush Larsen Update
    
    
    //Complete RK2 Update
    
    
    //Complete RK4 Update
    
    
    //Complete Sundnes Update
    
    
    //Complete Markov Backward Euler method
    
    
    //Complete Rosenbrock Update
    
    
    //Complete Cvode Update
    
    
    //Finish the update
    Iion = Iion;
    sv->h = h_new;
    //Change the units of external variables as appropriate.
    
    
    //Save all external vars
    Iion_ext[__i] = Iion;
    V_ext[__i] = V;

  }


}


void trace_MitchellSchaeffer(ION_IF* IF, int node, FILE* file, GlobalData_t** impdata)
{
  static bool first = true;
  if (first) {
    first = false;
    FILE_SPEC theader = f_open("MitchellSchaeffer_trace_header.txt","wt");
    fprintf(theader->fd,
        "Iion\n"
        "V\n"
      );

    f_close(theader);
  }

  GlobalData_t dt = IF->dt*1e0;
  cell_geom *region = &IF->cgeom;
  MitchellSchaeffer_Params *p  = (MitchellSchaeffer_Params *)IF->params;

  MitchellSchaeffer_state *sv_base = (MitchellSchaeffer_state *)IF->sv_tab.y;
  MitchellSchaeffer_state *sv = sv_base+node;
  int __i = node;

  GlobalData_t t = IF->tstp.cnt*dt;

  // Define the constants that depend on the parameters.
  //Prepare all the public arrays.
  GlobalData_t *Iion_ext = impdata[Iion];
  GlobalData_t *V_ext = impdata[Vm];
  //Prepare all the private functions.
  //Initialize the external vars to their current values
  GlobalData_t Iion = Iion_ext[__i];
  GlobalData_t V = V_ext[__i];
  //Change the units of external variables as appropriate.
  
  
  GlobalData_t Uamp = (sv->V_max-(sv->V_min));
  GlobalData_t Jin = ((((sv->h*((V-(sv->V_min))/Uamp))*(((V-(sv->V_min))/Uamp)-(sv->a_crit)))*((sv->V_max-(V))/Uamp))/sv->tau_in);
  GlobalData_t Jout = (-(((V-(sv->V_min))/Uamp)/sv->tau_out));
  Iion = ((-Uamp)*(Jin+Jout));
  //Output the desired variables
  fprintf(file, "%4.12f\t", Iion);
  fprintf(file, "%4.12f\t", V);
  //Change the units of external variables as appropriate.
  
  

}

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet
        