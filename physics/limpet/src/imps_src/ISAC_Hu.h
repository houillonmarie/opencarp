// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Hu Y, Gurev V, Constantino J, Bayer JD, Trayanova NA
*  Year: 2013
*  Title: Effects of Mechano-Electric Feedback on Scroll Wave Stability in Human Ventricular Fibrillation
*  Journal: PLOS ONE, 8(4), e60287
*  Journal: 10.1371/journal.pone.0060287
*  DOI: 10.1371/journal.pone.0060287
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __ISAC_HU_H__
#define __ISAC_HU_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define ISAC_Hu_REQDAT Iion_DATA_FLAG|Vm_DATA_FLAG|Lambda_DATA_FLAG
#define ISAC_Hu_MODDAT Iion_DATA_FLAG

struct ISAC_Hu_Params {
    GlobalData_t Gst;
    GlobalData_t RP;

};

struct ISAC_Hu_state {
    char dummy; //PGI doesn't allow a structure of 0 bytes.

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_ISAC_Hu(ION_IF *);
void construct_tables_ISAC_Hu(ION_IF *);
void destroy_ISAC_Hu(ION_IF *);
void initialize_sv_ISAC_Hu(ION_IF *, GlobalData_t**);
void compute_ISAC_Hu(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
