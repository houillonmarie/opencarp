// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------


/*
*
*  Authors: Pras Pathmanathan, Richard A. Gray
*  Year: 2013
*  Title: Verification of computational models of cardiac electro-physiology
*  Journal: International Journal for Numerical Methods in Biomedical Engineering, 30(5):525-544
*  DOI: 10.1002/cnm.2615
*
*/
        
//// HEADER GUARD ///////////////////////////
// If automatically generated, keep above
// comment as first line in file.
#ifndef __PATHMANATHAN_H__
#define __PATHMANATHAN_H__
//// HEADER GUARD ///////////////////////////
// DO NOT EDIT THIS SOURCE CODE FILE
// ANY CHANGES TO THIS FILE WILL BE OVERWRITTEN!!!!

namespace limpet {

#define Pathmanathan_REQDAT Vm_DATA_FLAG
#define Pathmanathan_MODDAT Iion_DATA_FLAG

struct Pathmanathan_Params {
    GlobalData_t Cm;
    GlobalData_t beta;
    GlobalData_t xi;

};

struct Pathmanathan_state {
    GlobalData_t u1;
    GlobalData_t u2;
    GlobalData_t u3;

};

#ifdef __cplusplus
extern "C"
{
#endif  // ifdef __cplusplus

void initialize_params_Pathmanathan(ION_IF *);
void construct_tables_Pathmanathan(ION_IF *);
void destroy_Pathmanathan(ION_IF *);
void initialize_sv_Pathmanathan(ION_IF *, GlobalData_t**);
void compute_Pathmanathan(int, int, ION_IF *, GlobalData_t**);

#ifdef __cplusplus
}
#endif  // ifdef __cplusplus

}  // namespace limpet

//// HEADER GUARD ///////////////////////////
#endif
//// HEADER GUARD ///////////////////////////
