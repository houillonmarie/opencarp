#!/usr/bin/perl
# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

#
# This PERL script creates a header file by parsing IMPs.h
#
#   To add a new ionic model or plugin (an IMP), add the name of the file
#   to either the IONIC_MODELS or PLUGINS list in IMPs.h.
#   Also, update the Makefile.
#   If a new data type is required, add it to IMPDATATYPES.
#
#   Note, the IMP name must contain at least one uppercase character
#   When adding an IMP called IMP, the following functions must be declared:
#   initialize_params_IMP() // modifiable IMP parameters
#   initialize_sv_IMP()     // allocate and initialize the SV table entries
#   construct_tables_IMP()  // allocate memory for and fill lookup table
#   compute_IMP()           // compute desired quantities
#   destroy_IMP()           // free tables used by IMP
#   and
#   #define IMP_REQDAT mask // in the IMP.h, required data
#   #define IMP_MODDAT mask // in the IMP.h, modified data
#   and
#   define a structure IMP_Params which contains IMP specific
#   parameters in IMP.h
#
#   The parameter \\b is a special parameter signifying a list of flags
#   which define model specific behaviour. This parameter is meant to be
#   used to signal that a specific set of parameter values should be used,
#   adjusted for a particular behaviour.
#
#   Ionic models and plugins use data not provided by the ionic models.
#   You must define a mask specifying what data must be passed in.
#   The mask is OR'ed together from the IMP data type flags
#   These data MUST be passed in the IMPDataStruct vector. The length
#   of these data vectors must be equal to the total #nodes even if
#   not all nodes use all data types. Data
#   passed like this typically signify interaction between IMPS and other
#   procedures, beyond the scope of an IMP, in the program.
#
#   Plug-ins may access parent data and modify it. Two helper functions
#   return pointers to functions that perform these two functions:
#   get_sv_offset()
#   getPutSV()
#
#

require "common.pl";

#get the list of IMPs
($i,$p,$idat) = readIMPs();
@IMPS = @$i;
@PLUGS = @$p;
@IMPdataTypes = @$idat;

####################
# Start writing ION_IF.h here
####################
open $cfile, ">", "ION_IF.h";
select $cfile;
print $cfile <<EOF
/** \\File DO NOT EDIT this .h file.
 * This header file was produced by executing \"ION_IF.h.pl\"
 *
 * \\note All numerical IDs and flags are automatically generated
 *
 * Units for all IMPS must be
 *
 * vm        - mV (transmembrane potential)
 * [Ca]_i    - micromolar (intracellular calcium)
 * [Ca]_b    - micromolar (troponin bound calcium)
 * t         - ms (time)
 * Lambda    - normalized (stretch ratio)
 * delLambda - 1/ms (stretch rate)
 * Tension   - kPa (active stress)
 * [Na]_e    - millimolar (extracellular sodium)
 * [K]_e     - millimolar (extracellular potassium)
 * [Ca]_e    - micromolar (extracellular calcium)
 * Iion      - uA/cm^2 (ionic current)
 */
#ifndef IONIC_IF_H
#define IONIC_IF_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include "LUT.h"
#include "ODEint.h"
#include <math.h>
#include <assert.h>

#include "basics.h"

#ifdef I
#undef I
#endif

namespace limpet {

#ifndef M_PI  //C99 does not define M_PI
#define M_PI  3.14159265358979323846264338327
#endif

//Defines used in Slava's LIMPET model '
#define heav(x) ( (x)<0 ? 0 : 1)
#define sign(x) ( (x)<0 ? -1 : 1 )
#define square(x) ((x)*(x))
#define cube(x) ((x)*(x)*(x))

template<class T>
inline T max(T a, T b ){ return a>b?a:b;}
template<class T>
inline T min(T a, T b ){ return a<b?a:b;}

// MPI message tags used in the IMP world
#define MPI_RESTORE_IMP_POLL_BUFSIZE_TAG  10
#define MPI_RESTORE_IMP_SNDRCV_BUFFER_TAG 11
#define MPI_DUMP_IMP_POLL_BUFSIZE_TAG     20
#define MPI_DUMP_IMP_SNDRCV_BUFFER_TAG    21
#define MPI_DUMP_SVS_POLL_BUFSIZE_TAG     30
#define MPI_DUMP_SVS_SNDRCV_BUFFER_TAG    31


/*
MODEL SPECIFIC DEFINITIONS

Each ionic model must have a model_id, a name and a data structure
containing all its adjustable parameters. This data structure is
defined in the header files of the respective ionic models.
*/

/* define model IDs for different ionic models */
EOF
;

print "typedef enum {";
foreach $idt (@I_DATA_T)
{
	print "$idt,";
}
print "bogusIDT} IMPDataType;\n";
print "#define NUM_IMP_DATA_TYPES ".($#I_DATA_T+1)."\n";
printf "/* define IMP data type flags */\n";
$val = 1;
foreach $idt (@I_DATA_T)
{
  print "#define $idt"."_DATA_FLAG $val\n";
  $val *=2;
}

print "\n//! data types needed by IMPs\n";
print "static const int imp_data_flag[] = {\n";
foreach $idt (@I_DATA_T)
{
  print "  $idt"."_DATA_FLAG,\n";
}
print " 0\n};\n";

print "//!strings for each enum type\n";
print "static const char *imp_data_names[] = { ";
foreach $idt (@I_DATA_T)
{
	print "\"$idt\", ";
}
print "\"BOGUS\" };\n";

print $cfile <<EOF


/** \\brief time constant groups
 *
 *  The time constant group structure will hold all the information for
 *  integrating the svs using different time steps depending on the time
 *  constant that governs the process.
 */
struct tc_grp {
  float dt;
  int   skp;
  int   rat;
  int   update;
};

/** \\brief time stepper
 *
 *  Holds all data to control time stepping
 */
struct ts {
  int     cnt;  /**< linear step counter                            */
  int     ng;   /**< number of time constant groups in this ION_IF  */
  tc_grp *tcg;  /**< array of structures holding the tcg data       */
};


struct SV_TAB {
	int	           svSize;    //!< size of structure holding SV\'s for a node
	int	           numSeg;    //!< number of unknowns
	void  	      *y;         //!< pointer to the sv array
};

/** \\brief definition of cell geometry

   Each ionic model is based on an underlying cell geometry.
   Several factors depend on the geometry like conversion factors
   how currents crossing the membrane translate into a change
   in ionic concentration. This structure is defined to provide a
   standardized mechanisms to define cell geometry and subdomain fractions
   to compute all dependent factors automatically. These factors can be
   accessed by plugins to derive factors for concentration updates.
 */
#define NDEF       -1
struct cell_geom {
    float      SVratio;      //!< single cell surface-to-volume ratio (per um)
    float      v_cell;       //!< cell volume
    float      a_cap;        //!< capacitive cell surface
    float      fr_myo;       //!< volume of myoplasm
    float      sl_i2c;       //!< convert sl-currents in uA/cm^2 to mM/L without valence
};

// Ionic model type structure.  Information that is shared across
typedef struct ion_type *ION_TYPE;
typedef struct ion_if ION_IF;
typedef float Gatetype;
typedef GlobalData_t (*SVgetfcn)(ION_IF*,int,int);
typedef void (*SVputfcn)(ION_IF*,int,int,GlobalData_t);
typedef char   IIF_Mask_t;

EOF
;

print_type_decl();

print $cfile <<EOF


struct IMPinfo {
  char    *name;          //!< IMP name
  int      sz;            //!< storage required
  int      nplug;         //!< number of plugins
  IMPinfo *plug;          //!< plugins
  bool     compatible;    //!< does IM match stored IM
  int      map;           //!< which plugin does this IMO match
  int      offset;        //!< offset into node data
};


//** Ionic model and plug-in (IMP) data structure
struct ion_if {
  ION_TYPE   type; //!< information about the model type.
  int         miifIdx;              //!< imp index within miif
  int         numNode;              //!< number of nodes using model
  cell_geom   cgeom;                //!< cell geometry data
  float       dt;                   //!< basic integration time step
  bool        lut_alloc;            //!< set after lut memory allocation
  ts          tstp;                 //!< control time stepping
  SV_TAB      sv_tab;               //!< state variable table
  int         numLUT;               //!< #LUTs
  LUT        *tables;               //!< lookup table list
  void*       params;               //!< model specific parameters
  int         n_plugins;            //!< number of plugins
  ION_IF    **plugins;              //!< plugin list
  ION_IF     *parent;               //!< parent IMP
  void       *ion_private;          //!< persistent private data workspace
  long        private_sz;           //!< size of private data workspace
  GlobalData_t **ldata;
  //! data that must be externally allocated
  unsigned int reqdat;
  //! externally allocated data that is modified by ionic_model and plugins
  unsigned int moddat;
};

/*
   function prototypes
 */

ION_IF*  alloc_IIF(ION_IF *pIF, ION_TYPE type, int num_plugs, ION_TYPE *plug_types, int numNode);
void     initialize_ts(ts *tstp, int ng, int *skp, double dt);
void     update_ts(ts *tstp);
void     SV_alloc( SV_TAB *psv, int numSeg, int struct_size );
void     SV_free( SV_TAB *psv );
void     initialize_params( ION_IF * pIF );
void     initialize_IIF( ION_IF *, double, GlobalData_t ** );
void     initialize_SV( ION_IF *pIF, GlobalData_t **impdat );
void     initialize_sv_dump( ION_IF *pIF, double tm, double intv, char **svLst );
void     free_sv_table( void * );
char*    IIF_fill_buf( ION_IF *iif, char *buf, int* n, opencarp::Salt_list *l );
int      restore_iif(ION_IF *iif, opencarp::FILE_SPEC in, int n, const int *pos, IIF_Mask_t *mask,
                     size_t *offset, IMPinfo *impinfo, const int* loc2canon);
void     free_IIF( ION_IF * );
int      dump_luts( ION_IF *pIF, bool zipped );
void     destroy_luts( ION_IF *pIF );
void     tune_IMPs( ION_IF *, const char *, const char *, const char * );
void     print_IMPs(void);
bool     flag_set( const char *flags, const char *target );
ION_TYPE get_ION_TYPE(const char *);
void     print_models(bool );
SVgetfcn get_sv_offset(ION_TYPE, const char*, int*, int*);
int      get_sv_list(ION_TYPE, char ***);
int      read_svs(ION_IF*, FILE*);
int      write_svs(ION_IF*, FILE*, int);
float    modify_param( float a, char *expr );
int      process_param_mod( char *pstr, char *par, char *mod );
char*    get_typename(int type);
bool     verify_flags( const char *flags, const char* given );
int      load_ionic_module(const char*);
char    *get_next_list( char *lst, char delimiter );
void     copy_IMP_SVs( ION_IF* to, ION_IF *from, bool alloc );
void*    IMP_malloc( size_t, size_t );
void     IMP_free( void * );

// Functions for getting SVs in sv_init.c
SVputfcn getPutSV( SVgetfcn );


/** get the value of a state variable
 *
 * \\param sv     pointer to ionic model
 * \\param num    index of state variable
 * \\param offset offset of the state variable into the structure
 *
 * \\return the value of the SV converted to a GlobalData_t
 */
EOF
;
foreach $svtype ( keys %scanchar ) {
  print "GlobalData_t get",$svtype,"SV( ION_IF *sv, int num, int offset );\n";
}

foreach $svtype ( keys %scanchar ) {
  print "void put",$svtype,"SV( ION_IF *sv, int num, int offset, GlobalData_t v );\n";
}

print $cfile <<EOF

#define CHANGE_PARAM( T, P, V, F )  do { \\
	((T##_Params *)P)->V = modify_param( ((T##_Params *)P)->V, F ); \\
	log_msg( _nc_logf,0, 0, "\t%-20s modifier: %-15s value: %g",\\
	#V,F,(float)((T##_Params *)P)->V);} while (0)

}  // namespace limpet

#endif

EOF
;
