#!/usr/bin/perl
# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------


if( $#ARGV==-1 ) {
  print "Usage: imp_skeleton.pl IMP_name\n";
  exit;
}

$lcname = lc $ARGV[0];

open $cfile, ">", "$ARGV[0].c";

print $cfile <<EOF
#include "ION_IF.h"
#include "$ARGV[0].h"
#include <time.h>

/*****************************************************************************
 * #define constants for $ARGV[0] 
 *****************************************************************************/

// #define def_G_Na    1.0667
// #define def_G_to    0.0438
// #define def_G_CaL   0.0341
// #define init_vm   -80.50146
// #define init_m      4.164108e-3
// #define init_h      6.735613e-1
// #define init_j      6.729362e-1

/*****************************************************************************
 * lookup tables for $ARGV[0]
 *
 * Define the number of tables and, for each table, define an index,
 * a number of columns, upper and lower boundaries, and resolution.
 *****************************************************************************/
// #define N_TABS          1        //!< number of lookup tables
// #define Vtab            0        //!< index of Vm-dependent table
// #define VtabCOLS        29       //!< number of Vm-dependent calcs
// #define VtabMIN         (-1000.) //!< lowest expected value of Vm
// #define VtabMAX         (1000.)  //!< highest expected value of Vm
// #define VtabRES         0.05     //!< resolution of Vm lookup table

/*****************************************************************************
 * macros for $ARGV[0] 
 *
 * Some useful macros are included by default. Avoid math.h at all costs!
 *****************************************************************************/
#define square(x)       ((x)*(x))
#define cube(x)         ((x)*(x)*(x))
#define fourth(x)       ((x)*(x)*(x)*(x))

/*****************************************************************************
 * function prototypes for $ARGV[0]
 *
 * Helper functions not to be accessed from outside this IM. 
 * Please use doxygen-style comments throughout any added code.
 *****************************************************************************/

/*****************************************************************************
 * function: initialize parameters for $ARGV[0]
 *
 * Assign default values to all parameters in $ARGV[0]_Params,
 * which is reserved for user-modifiable parameters of this IM.
 *
 * \\param IF     pointer to the associated ION_IF structure
 *****************************************************************************/
void initialize_params_$ARGV[0]( ION_IF *IF )
{
    $ARGV[0]_Params *pp = ($ARGV[0]_Params *)IF->params;
//    pp->G_Na  = def_G_Na;
//    pp->G_to  = def_G_to;
//    pp->G_CaL = def_G_CaL;
}

/*****************************************************************************
 * function: initialize state variables for $ARGV[0]
 *
 * The body of the for loop should assign initial conditions for each node
 * to all state variables in $ARGV[0]_state.
 * 
 * \\param IF      pointer to the associated ION_IF structure
 * \\param dt	  time step
 * \\param impdat  external data required by the IM
 *****************************************************************************/
void initialize_sv_$ARGV[0]( ION_IF *IF, Globaldata_t **impdat )
{
    $ARGV[0]_state  *sv = ($ARGV[0]_state *) IF->sv_tab.y;
    $ARGV[0]_Params *pp = ($ARGV[0]_Params *)IF->params;

    GlobalData_t *vm = impdat[Vm];
    
    SV_alloc( &IF->sv_tab, 1, IF->numNode, sizeof($ARGV[0]_state) );
    sv = ($ARGV[0]_state *) IF->sv_tab.y;
    
    for( int i = 0; i < IF->sv_tab.numSeg; i++ ) {
//        vm[i]   = init_vm;
//        sv[i].m = init_m;
//        sv[i].h = init_h;
//        sv[i].j = init_j;
    }
}

/*****************************************************************************
 * function: construct lookup tables for $ARGV[0]
 *
 * For each lookup table, call LUT_alloc() and populate the table according
 * to lower/upper boundaries and resolution. Call check_LUT() for each table.
 * 
 * \\param dt	  time step
 * \\param IF      pointer to the associated ION_IF structure
 *****************************************************************************/
void construct_tables_$ARGV[0]( ION_IF *IF )
{
  $ARGV[0]_Params *pp = ($ARGV[0]_Params *)IF->params;
    
  IF->numLUT = N_TABS;
  IF->tables = (LUT *) calloc( N_TABS, sizeof(LUT) );
    
//  LUT  *vtab  = &IF->tables[Vtab];
//  LUT_alloc( vtab, VtabCOLS, VtabMIN, VtabMAX, VtabRES, "$ARGV[0] Vm" );
//    
//  for( int i = vtab->mn_ind; i <= vtab->mx_ind; i++ ) {
//      float Vm = i * vtab->res;
//      LUT_entry(vtab, i, 0) = Vm;
//  }
//    
   for( int i=0; i<IF->numLUT; i++ )
     check_LUT( IF->tables+i );
}

/*****************************************************************************
 * function: compute change during one time step for $ARGV[0]
 *
 * The body of the for loop should calculate the updated values of each state
 * variable for the current time step. Extensive use of lookup tables is 
 * recommended to improve computational efficiency.
 * 
 * \\param start   index of first node
 * \\param end     index of last node
 * \\param IF      pointer to the associated ION_IF structure
 * \\param impdat  external data required by the IM
 *****************************************************************************/
GLOBAL void 
compute_$ARGV[0](int start, int end, ION_IF *IF, Globaldata_t **impdat )
{
  $ARGV[0]_Params *pp = ($ARGV[0]_Params *)IF->params;
    
  Globaldata_t   *vm    = impdat[Vm];
  Globaldata_t   *ion   = impdat[Iion];
    
//  LUT    *vtab  = &IF->tables[Vtab];
 
#ifdef __CUDACC__
  int i = blockDim.x * blockIdx.x + threadIdx.x;
  if( i>=end ) return;
  {
#else
 
#pragma omp parallel for schedule(static)
  for ( int i = start; i < end; i++ ) {
#endif

      $ARGV[0]_state *sv = ($ARGV[0]_state *)IF->sv_tab.y + i;
//    LUT_data_t* vrow = LUT_row(vtab, vm[i], i)];

    ion[i] += ;
  }
}

/*****************************************************************************
 * function: post-execution cleanup for $ARGV[0]
 *
 * Destroy LUTs and free SV memory. Other operations are seldom necessary.
 * 
 * \\param IF      pointer to the associated ION_IF structure
 *****************************************************************************/
void destroy_$ARGV[0]( ION_IF *IF )
{
  destroy_luts( IF );
  SV_free( &IF->sv_tab );
}

#undef N_TABS
EOF
;

close $cfile;

open $hfile, ">", "$ARGV[0].h";

print $hfile <<EOF
/*****************************************************************************
 * $ARGV[0] ionic model
 *
 * Add description/journal reference here
 *
 *****************************************************************************/
#ifndef $ARGV[0]_H
#define $ARGV[0]_H

// REQDAt is needed but unchanged by the IMP, while MODDAT is modified by the IMP
#define $ARGV[0]_REQDAT Vm_DATA_FLAG
#define $ARGV[0]_MODDAT Iion_DATA_FLAG

/*****************************************************************************
 * user-modifiable parameters for $ARGV[0]
 *****************************************************************************/
typedef struct $ARGV[0]_params {
//  float G_Na;
//  float G_to;
//  float G_CaL;
    // UNMODIFIABLE BELOW HERE
	// internal working variables
} $ARGV[0]_Params;


/*****************************************************************************
 * state variables for $ARGV[0]
 *****************************************************************************/
typedef struct $ARGV[0]_state_struct {
//  Gatetype m;
//  Gatetype h;
//  Gatetype j;
} $ARGV[0]_state;

#ifdef __CUDACC__
extern "C" {
#endif

void initialize_params_$ARGV[0](ION_IF *);
void construct_tables_$ARGV[0](ION_IF *);
void destroy_$ARGV[0](ION_IF *);
void initialize_sv_$ARGV[0](ION_IF *, GlobalData_t**);
GLOBAL void compute_$ARGV[0](int, int, ION_IF *, GlobalData_t**);

#ifdef __CUDACC__
};
#endif

#endif
EOF
;

close $hfile;

print "Created $ARGV[0].c and $ARGV[0].h\n\n";
print "**************************************************************\n";
print "Edit \"IMPs.h\" by adding $ARGV[0] to either IONIC_MODELS or PLUGINS\n".
      "and in $ARGV[0].h\n".
	  "define $ARGV[0]_PARAMS & $ARGV[0]_state\n";
print "**************************************************************\n";
