group { 
  #Authors ::= Jeffrey J. Fox, Jennifer L. McHarg, and Robert F. Gilmour Jr
  #Year ::= 2002
  #Title ::= Ionic mechanism of electrical alternans
  #Journal ::= American Journal of Physiology: Heart and Circulatory Physiology, 282, H516-H530
  #DOI ::= 10.1152/ajpheart.00612.2001
} .reference();

V; .external(Vm); .nodal();
Iion; .external(); .nodal();

V; .lookup(-800, 800, 0.05);
Cai; .lookup(1e-4, 100, 1e-4);

R = 8.314;
T = 310.;
F = 96.5;

group {
  GNa = 12.8;
  GKr = 0.0136;
  GKs = 0.0245;
  PCa = 0.0000226;
}.param();

GK1 = 2.8;
K_mK1 = 13.;
Gto = 0.23815;
GKp = 0.002216;
INaK_max = 0.693;
K_mNai = 10.;
K_mKo = 1.5;
K_mCa = 1380.;
K_mNa = 87.5;
K_NaCa = 1500.;
K_sat = 0.2;
eta = 0.35;
K_mpCa = 0.05;
ICap_max = 0.05;
GbCa = 0.0003842;
GbNa = 0.0031;
PCaK = 0.000000579;
ICa_half = -0.265;
C_sc = 1.;
K_mfCa = 0.18;
K_mCMDN = 2.;
CMDN_tot = 10.;
V_myo = 0.00002584;
A_Cap = 0.0001534;
P_rel = 6.;
P_leak = 0.000001;
K_mCSQN = 600.;
CSQN_tot = 10000.;
V_SR = 0.000002;
V_up = 0.1;
K_mup = 0.32;
Nai = 10.;
Nao = 138.;
Cao = 2000.;
Ki = 149.4;
Ko = 4.;

V_init = -94.7;
Ca_SR_init = 320.;
Cai_init = 0.0472;
f_init = 0.983;
d_init = 0.0001;
m_init = 2.4676*pow(10,-4);
h_init = 0.99869;
j_init = 0.99887;
f_Ca_init = 0.942;
X_kr_init = 0.229;
X_ks_init = 0.0001;
X_to_init = 3.742*pow(10,-5);
Y_to_init = 1.;

Iion  =  INa+ICa+ICaK+IKr+IKs+Ito+IK1+IKp+INaCa+INaK+ICap+IbNa+IbCa;

E_Na  = ((R*T)/F*log(Nao/Nai));
INa = (GNa*m*m*m*h*j*(V-E_Na));
E0_m = (V+47.13);
alpha_m = (0.32*E0_m)/(1-exp((-(0.1)*E0_m)));
beta_m = (0.08*exp(-(V)/11));
alpha_h = (0.135*exp((V+80)/(-6.8)));
beta_h = 7.5/(1+exp(((-0.1)*(V+11))));
alpha_j = (0.175*exp((V+100)/(-23)))/(1+exp((0.15*(V+79))));
beta_j = 0.3/(1+exp(((-0.1)*(V+32))));

IK1 = ((GK1*K1_infinity*Ko)/(Ko+K_mK1)*(V-E_K));
K1_infinity = 1/(2+exp(((1.62*F)/(R*T)*(V-E_K))));
E_K = ((R*T)/F*log(Ko/Ki));

R_V = 1/(1+(2.5*exp((0.1*(V+28)))));
sq_rt_Ko_4  =  sqrt(Ko/4);
IKr = (GKr*R_V*X_kr*sq_rt_Ko_4*(V-E_K));
X_kr_inf = 1/(1+exp(((-2.182)-(0.1819*V))));
tau_X_kr = (43+1/(exp(((-5.495)+(0.1691*V)))+exp(((-7.677)-(0.0128*V)))));

E_Ks = ((R*T)/F*log((Ko+(0.01833*Nao))/(Ki+(0.01833*Nai))));
IKs = (GKs*X_ks*X_ks*(V-E_Ks));
X_ks_infinity = 1./(1+exp((V-16)/(-13.6)));
tau_X_ks = ((V  ==  10) 
          ? 1/(0.0000719/-0.148+0.000131/0.0687)
          : 1/((0.0000719*(V-10))/(1-exp(((-0.148)*(V-10))))+(0.000131*(V-10))/(exp((0.0687*(V-10)))-1))
          );
Ito = (Gto*X_to*Y_to*(V-E_K));
alpha_X_to = (0.04516*exp((0.03577*V)));
beta_X_to = (0.0989*exp(((-0.06237)*V)));
alpha_Y_to = (0.005415*exp((V+33.5)/(-5)))/(1+(0.051335*exp((V+33.5)/(-5))));
beta_Y_to = (0.005415*exp((V+33.5)/5))/(1+(0.051335*exp((V+33.5)/5)));

IKp = (GKp*Kp_V*(V-E_K));
Kp_V = 1/(1+exp((7.488-V)/5.98));

f_NaK = 1/(1+(0.1245*exp(((-0.1)*V*F)/(R*T)))+(0.0365*sigma*exp(((-V)*F)/(R*T))));
sigma = (1./7*(exp(Nao/67.3)-1));
INaK = ((INaK_max*f_NaK)/(1+pow(K_mNai/Nai, 1.5))*Ko)/(Ko+K_mKo);

Nao3  =  Nao*Nao*Nao;
Nai3  =  Nai*Nai*Nai;
K_mNa3  =  K_mNa*K_mNa*K_mNa;
e_etaVFRT  =  exp((eta*V*F)/(R*T));
e_eta1VFRT  =  exp(((eta-1)*V*F)/(R*T));
INaCa = (K_NaCa/((K_mNa3+Nao3)*(K_mCa+Cao)*(1+(K_sat*e_eta1VFRT)))*((e_etaVFRT*Nai3*Cao)-(e_eta1VFRT*Nao3*Cai)));
ICap = (ICap_max*Cai)/(K_mpCa+Cai);
E_Ca = ((R*T)/(2*F)*log(Cao/Cai));
IbCa = (GbCa*(V-E_Ca));
IbNa = (GbNa*(V-E_Na));

ICa = ICa_max*f*d*f_Ca;
ICa_max = ((PCa/C_sc*4*V*F*F)/(R*T)*((Cai*e_2VFRT)-(0.341*Cao)))/(e_2VFRT-1);

e_VFRT  =  exp((V*F)/(R*T));
e_2VFRT  =  exp((2*V*F)/(R*T));
ICaK = (((PCaK/C_sc*f*d*f_Ca)/(1+ICa_max/ICa_half)*1000*V*F*F)/(R*T)*((Ki*e_VFRT)-Ko))/(e_VFRT-1);

f_infinity = 1/(1+exp((V+12.5)/5));
tau_f = (30+200/(1+exp((V+20)/9.5)));
d_infinity = 1/(1+exp((V+10)/(-6.24)));
E0_d = (V+40);
tau_d_temp  =  (0.07*exp(((-0.05)*E0_d)))/(1+exp((0.05*E0_d)));
tau_d = 1/((0.25*exp(((-0.01)*V)))/(1+exp(((-0.07)*V)))+tau_d_temp);

CaIK_mfCa  =   Cai/K_mfCa;
f_Ca_infinity = 1/(1+CaIK_mfCa*CaIK_mfCa*CaIK_mfCa);
tau_f_Ca = 30;

K_muPCai2  =  K_mup/Cai*K_mup/Cai;
J_up = V_up/(1+K_muPCai2);
Ca_SR_2000_3  =  2000/Ca_SR * 2000/Ca_SR * 2000/Ca_SR;
gamma = 1/(1+Ca_SR_2000_3);
J_rel_temp  =  (1+(1.65*exp(V/20)));
J_rel = (P_rel*f*d*f_Ca*((gamma*Ca_SR)-Cai))/J_rel_temp;
J_leak = (P_leak*(Ca_SR-Cai));
K_mCSQN_Ca_SR2  =  (K_mCSQN+Ca_SR)*(K_mCSQN+Ca_SR);
beta_SR = 1/(1+(CSQN_tot*K_mCSQN)/K_mCSQN_Ca_SR2);
diff_Ca_SR = (beta_SR*((J_up-J_leak)-J_rel)*V_myo)/V_SR;
K_mCMDN_Cai2  =  (K_mCMDN+Cai)*(K_mCMDN+Cai);
beta_i = 1/(1+(CMDN_tot*K_mCMDN)/K_mCMDN_Cai2);
diff_Cai = (beta_i*(((J_rel+J_leak)-J_up)-((A_Cap*C_sc)/(2*F*V_myo)*((ICa+IbCa+ICap)-(2*INaCa)))));

group {
    INa;
    m;
    h;
    j;
    E_Na;
    Cai;
    J_up;
    J_rel;
    Ca_SR;
    INa;
    ICa;
    IKr;
    IKs;
    Ito;
    IK1;
    IKp;
    INaCa;
    INaK;
    ICap;
    IbNa;
    IbCa;
    ICaK;
    V;
} .trace();
