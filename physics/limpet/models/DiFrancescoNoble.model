group { 
  #Authors ::= Di Francesco D. and Noble Denis
  #Year ::= 1985
  #Title ::= A model of cardiac electrICal actiVolity incorporating ionic pumps and concentration changes
  #Journal ::= Phil. Trans. R. Soc. Lond. B307353-398
  #DOI ::= 10.1098/rstb.1985.0001
} .reference();


V; .external(Vm); .nodal(); 
Iion; .external(); .nodal();
V;.lookup(-1000,1000,0.05);


group {
  Cm = 12;              // (uF/cm2)
  GNa = 750.;           //!< sodium conductance (uS)
  Gto = 1.;             //!< factor by which to modify transient outward current
  GCaf = 1.;            //!< factor by which to modify Ca,f current
  factorIfunny = 1.;    //!< factor by which to modify funny current (If)
  kNaCa;                //!< Na-Ca exchanger rate
} .param();

group {
  If;
  IK;
  IK1;
  Ito;
  IbNa;
  IbCa;
  Ip;
  INaCa;
  INa;
  ICa;
  Iup;
  Itr;
  Irel;
  imK;
  V;
  y;
  x;
  r;
  m;
  h;
  d;
  f;
  f2;
  pp;
  Nai;
  Cai;
  Caup;
  Carel;
  Kc;
  Ki;
} .trace();

R = 8.3143; .units(J/K/mol);
T = 307.73;       //             .unit(K)
F = 96.487;
RTF = (R*T/F);
FRT = (F/R/T); 

radius = 0.005;   //             .unit(cm)
length = 0.2;     //             .unit(cm)
fVolecs = 0.1; 
Nao = 140.;       //             (mM)
Cao = 2.;         //             (mM)
Kb = 4.;          //             (mM)
Kmf = 45.;        //             (mM)
GfNa = 3.;        //             (uS)
GfK = 3.;         //             (uS)
IKmax = 180.;     //             (nA)
GK1 = 920.;       //             (uS)
Km1 = 210.;       //             (mM)
Kmto = 10.;       //             (mM)
Kact4 = 0.0005;    //             (mM)
GbNa = 0.18;       //             (uS)
GbCa = 0.02;       //             (uS)
Ipmax = 125.;     //             (nA)
KmK = 1.;         //             (mM)
KmNa = 40.;       //             (mM)
kNaCa = 0.02;      //             (mM)
nNaCa = 3.; 
dNaCa = 0.001; 
gamma = 0.5; 
Psi = 15.; 
Kmf2 = .001;      //             (mM)
Caup_bar = 5.;    //             (mM)
KmCa = .001;      //             (mM)
rr = 2.; 
tau_rel = 0.050;  //             (sec)
tau_rep = 2.;     //             (sec)
tau_up  = 0.025;   //             (sec)
P = 0.7;           //             (1/sec)
V_DFN_0 = -75;    //             (mV)
SMALL = 10e-18;

Nai_init  = 8.;
Cai_init  = 0.00005;
Caup_init  = 2.;
Carel_init = 1.;
Kc_init    = 4.;
Ki_init   = 140.;
V_init = V_DFN_0;


surface_area = 2. * 3.14159 * radius * length;
Voli = ((1. - fVolecs) * 3.14159 * radius * radius * length);
Vole = (Voli * fVolecs / (1. - fVolecs));
Volup = (.05 * Voli);
Volrel = (.02 * Voli); 



//  float 		DT     = IF->dt/1000.; // conVolert to sec
if ( fabs(V + 10) < 0.001) {
  vterm1 = 5;
 } else {
  vterm1 = (V + 10.) / (1. - exp(-.2 * (V + 10.)));
 }

vrow_19 = exp( -V *FRT );
vrow_20 = exp( .02*V );
vrow_21 = vterm1;
if ( V!=50. ) {
  vrow_22 = -4.*Psi*(V-50.)*FRT/expm1(-(V-50.)*2.*FRT);
  vrow_23 = -0.001*Psi*(V-50.)*FRT/expm1(-(V-50.)*FRT);
 } else {
  vrow_22 = 2.*Psi;
  vrow_23 = 0.001*Psi;
 }
vrow_24 = exp(-(V-50.)*FRT);
vrow_25 = exp(gamma*(nNaCa-2.)*V*FRT/2.);


exp50FRT = exp(50*FRT);


EK    = RTF * log(Kc / Ki);
ENa   = RTF * log(Nao / Nai);
ECa   = RTF * log(Cao / Cai) / 2.;
Emh   = RTF * log((Nao + .12 * Kc) / (Nai + .12 * Ki));

y; .units(unitless);
diff_y; .units(unitless/ms);
alpha_y = .025 * exp(- .067 * (V + 52))/1000.;
if ( fabs(V+52.) < .001 ) {
  beta_y = 2.5/1000.;
} else {
  beta_y = 0.5*(V + 52) / (1. - exp(-.2 * (V + 52)))/1000.;
}
If = factorIfunny * y * (Kc / (Kc + Kmf)) *
  (GfK * (V - EK) + GfNa * (V - ENa));

alpha_x = .5 * exp(.0826 * (V + 50.)) / (1. + exp(.057 * (V + 50.)))/1000.;
beta_x = 1.3 * exp(-.06 * (V + 20.)) / (1. + exp(-.04 * (V + 20.)))/1000.;
IK = x * IKmax * (Ki - Kc * vrow_19) / 140.;

// Background K current ;
IK1 = GK1 * (Kc/(Kc + Km1)) *
  ((V - EK)/(1. + exp(2. * FRT * (V - EK + 10.))));

alpha_r = .033 * exp(-V / 17.)/1000.;
beta_r = 33. / (1. + exp( - (V + 10.) / 8.))/1000.;
Ito = ( 2 * r * 0.28 * ((0.2 + Kc) / (Kmto + Kc)) *
        (Cai / (Kact4 + Cai)) * vrow_21 *
        (Ki * vrow_20 - Kc /vrow_20) ) * Gto;


// Background sodium current
IbNa = GbNa * (V - ENa);

// Resting Ca leak
IbCa = GbCa * (V - ECa);

// Na,K pump current
Ip = Ipmax*(Kc / (KmK + Kc)) * (Nai / (KmNa + Nai));

// Na,Ca exchange current
INaCa = kNaCa * vrow_25*
  ( (Nai*Nai*Nai)*Cao-vrow_19*(Nao*Nao*Nao)*Cai)/
  (1.+dNaCa*(Cai*(Nao*Nao*Nao)+Cao*((Nai*Nai*Nai))));

if (fabs(V + 41.) < .001) {
  alpha_m = 2000./1000.;
 } else {
  alpha_m = 200. * (V + 41.) / (1. - exp(- .1 * (V + 41.)))/1000.;
 }
beta_m = 8000. * exp( - .056 * (V + 66.))/1000.;

aa_h = 20. * exp(-.125 * (V + 75.));
bb_h = 2000. / (1. + 320. * exp(-.1 * (V + 75.)));
tau_h = 1000./(aa_h+bb_h);
h_inf = 1 - bb_h/(aa_h+bb_h);


INa   = GNa * m * m * m * h * (V - Emh);


if (fabs(V+24) < .001) {
  alpha_d = 120./1000.;
  beta_d = 120./1000.;
} else {
  alpha_d = -30. * (V+24) / expm1(- (V+24) / 4.)/1000.;
  beta_d = 12. * (V+24) / expm1((V+24) / 10.)/1000.;
}

f_tmp = V + 34.;
if (fabs(f_tmp) < .001) {
  alpha_f = 25./1000.;
 } else {
  alpha_f = 6.25 * f_tmp / expm1( f_tmp / 4.)/1000.;
 }
beta_f = 50. / (1. + exp(- f_tmp / 4.))/1000.;

alpha_f2 = 10./1000.;	//.1;
beta_f2 = alpha_f2 * Cai / Kmf2;

isICa = (Cai*exp50FRT*exp50FRT - Cao*vrow_24*vrow_24);
isIK  = (Ki*exp50FRT   - Kc*vrow_24);
isINa = (Nai*exp50FRT  - Nao*vrow_24);

if (fabs(V-50.) < .001) {
  isICa *= 2.*Psi;
  isIK  *= .01 * Psi;
  isINa *= .01 * Psi;
 } else {
  isICa *= d*f*f2*vrow_22;
  isIK  *= d*f*f2*vrow_23;
  isINa *= d*f*f2*vrow_23;
 }

// Ca,f
ICa   = GCaf*(isICa + isIK + isINa);
isi   = ICa + INaCa;


// Internal sodium concentration ;
diff_Nai = - (INa + IbNa + If/2. + 3 * Ip + (nNaCa/(nNaCa - 2)) * INaCa)*
1.e-9 / (Voli * F);

// Internal calcium concentration ;
aa_Caup = 2.e6 * F * Voli / (tau_up * Caup_bar);
bb_Caup = 0.;
Iup  = aa_Caup * Cai * (Caup_bar - Caup) - bb_Caup * Caup;

p_tmp = V+34;
if ( fabs(p_tmp) < 0.001 ) {
  alpha_pp = 4*60.25/1000.;
 } else {
  alpha_pp = 62.5*p_tmp/expm1(p_tmp/4.)/1000.;
}
beta_pp = 500/(1.+exp(-p_tmp/4.))/1000.;

aa_Catr = 2.e6 * F * Volrel / tau_rep;
bb_Catr = 0.;
Itr  = aa_Catr * pp * (Caup - Carel);

aa_Carel = 2.e6 * F * Volrel / tau_rel;
bb_Carel = 0.;
Irel = aa_Carel*Carel*(Cai*Cai)/(Cai*Cai+KmCa*KmCa);
diff_Caup  = (Iup - Itr) / (4.e6 * Volup * F)/1000.;
diff_Carel = (Itr - Irel) / (4.e6 * Volrel * F)/1000.;
diff_Cai = - (isICa + IbCa - 2 * INaCa / (nNaCa - 2.) + Iup - Irel) /
  (2.e6 * Voli * F)/1000.;
  Cai;.method(rk4);
//*/

// Cleft potassium concentration ;
// Uniform Ki; equation 63       ;
imK = IK1 + IK + If/2. - 2. * Ip;
diff_Kc = (- P * (Kc - Kb) + imK / (1.e6 * Vole * F))/1000.;

// Internal potassium concentration ;
diff_Ki = - imK / (1.e6 * Vole * F)/1000.;

Iion = (If+ IK+ IK1+ Ito+ IbNa+ IbCa+ Ip+ INaCa+ INa+ ICa)/C/1.e3;
C = Cm * surface_area;


