# Ionic models and plugins (IMP)

## Adding a new model
- Implement your model in EasyML as described in
[this example](https://opencarp.org/documentation/examples/01_ep_single_cell/05_easyml)
or even better import it directly from CellML as described
[here](https://opencarp.org/documentation/examples/01_ep_single_cell/10_fromcellml).
- Follow the naming conventions given below.
- Make sure to add the reference metadata on top of the .model file (see existing models for examples)
- Put your .model files in your `physics/limpet/models` folder.
- In openCARP's top level folder,
  - configure CMake with updated imp_list.txt via `cmake -S. -B_build -DUPDATE_IMPLIST=ON`.
  - run the CMake building process `cmake --build _build`. This will generate the .h and .cc files for your model in `physics/limpet/src/imps_src`

**Note:** If you want to add or modify a model file after openCARP was compiled, it is possible to first clean the previous generated files during compilation by running `make clean` before recompiling openCARP.


## Model and plugin naming conventions
Model name conventions:
- All authors if <= 2, otherwise just one
- All equal contribution first authors
- Add two-digit year if model name is already taken

Plugins are prefixed with a name reflecting their function, then
separated by a _ author name(s) as for models.

## Variable naming conventions
- Ion channel conductance: GIonname → `GKr`, `GNa`, `GCaL`, `Gto` 
- Ionic currents: `IName` → `IKr`, `INa`, `ICaL`, `Ito`
- Ionic concentration: `Ioncompartment` → `Nai`, `Ke`, `Cai`


## Old model names
Models were renamed at the end of 2021 to increase consistency. Here you can find a mapping from the old names to the new ones:
* ARPF → AslanidiSleiman
* AlievPanfilov → AlievPanfilov
* COURTEMANCHE → Courtemanche
* DiFranNoble → DiFrancescoNoble
* ECME → Cortassa → dropped
* EP → Electroporation_DeBruinKrassowska99
* EP_RS → Electroporation_DeBruinKrassowska98
* FOX → Fox
* GPB → Grandi
* GPB_Land → Augustin
* GPVAtria → GrandPanditVoigt
* GTT2_fast → dropped
* GTT2_fast_PURK → dropped
* HH → HodgkinHuxley
* HunterNash_Stress → dropped
* IA → AshiharaTrayanova
* IChR2 → IChR2_WilliamsXu
* INADA_AVN → Inada
* INa_Bond_Markov → INa_Bondarenko
* I_ACh → IACh_Cheng
* I_KATP → IKATP_Ferrero
* Iribe → IribeKohl
* JB_COURTEMANCHE → dropped
* LR1 → LuoRudy91
* LRDII_F → LuoRudy94
* LandHumanStress → Stress_Land17
* LandStress → Stress_Land12
* Loewe → Loewe
* Lumens → Stress_Lumens
* MBRDR → DrouhardRoberge
* MM_ATRIAL_FB → Maleckar
* MacCannell_Fb → MacCannell
* MacCannell_Fb_plug → Fibroblast_MacCannell
* MitchellSchaeffer → MitchellSchaeffer
* Morgan_Fb → Fibroblast_Morgan
* MurineMouse → Bondarenko
* NHSstress → Stress_Niederer
* NYGREN → Nygren
* ORd → OHara
* PASSIVE → Plonsey
* Pathmanathan → Pathmanathan
* RNC → Ramirez
* Rice → Stress_Rice
* SHANNON → Shannon
* Steward → Steward
* TT2 → tenTusscherPanfilov
* UCLA_RAB → MahajanShiferaw
* UCLA_RAB_SCR → Campos
* WANG_NNR → WangSobie
* YHuSAC → ISAC_Hu
* hAMr → dropped
* koivumaki → Skibsbye
* kurata → Kurata
* mMS → dropped
* myMBRDR → dropped
