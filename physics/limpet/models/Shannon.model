group { 
  #Authors ::= Thomas R. Shannon, Fei Wang, Jose Puglisi, Christopher Weber, Donald M. Bers
  #Year ::= 2004
  #Title ::= A Mathematical Treatment of Integrated Ca Dynamics within the Ventricular Myocyte
  #Journal ::= Biophysical Journal, 87(5), 3351-3371
  #DOI ::= 10.1529/biophysj.104.047449
} .reference();

V; .external(Vm); .nodal();
Iion; .external(); .nodal();

# Constants
Ko = 5.4; .units(mmol/L);
Nao = 140; .units(mmol/L);
Cao = 1.8; .units(mmol/L);
Clo = 150; .units(mmol/L);
Ki = 135; .units(mmol/L);
Mgi = 1; .units(mmol/L);
Cli = 15; .units(mmol/L);
R = 8314.3; .units(J/kmol/K);
T = 310; .units(K);
F = 96485; .units(C/mol);
Cm = 1.381e-10; .units(F);
cell_length = 100; .units(um);
cell_radius = 10.25; .units(um);
GINa = 16; .units(mS/uF);
Fx_Na_jct = 0.11; .units(unitless);
Fx_Na_SL = 0.89; .units(unitless);
GNaBk = 0.297e-3; .units(mS/uF);
Fx_NaBk_jct = 0.11; .units(unitless);
Fx_NaBk_SL = 0.89; .units(unitless);
H_NaK = 4; .units(unitless);
Km_Nai = 11; .units(mmol/L);
Km_Ko = 1.5; .units(mmol/L);
INaK_max = 1.90719; .units(uA/uF);
Fx_NaK_jct = 0.11; .units(unitless);
Fx_NaK_SL = 0.89; .units(unitless);
Fx_Ks_jct = 0.11; .units(unitless);
Fx_Ks_SL = 0.89; .units(unitless);
pKNa = 0.01833; .units(unitless);
GKp = 0.001; .units(mS/uF);
Gtos = 0.06; .units(mS/uF);
Gtof = 0.02; .units(mS/uF);
GCl = 0.109625; .units(mS/uF);
Kd_ClCa = 0.1; .units(mmol/L);
Fx_Cl_jct = 0.11; .units(unitless);
Fx_Cl_SL = 0.89; .units(unitless);
GClBk = 0.009; .units(mS/uF);
PCa = 5.4e-4; .units(L/ms/F);
PNa = 1.5e-8; .units(L/ms/F);
PK = 2.7e-7; .units(L/ms/F);
Fx_ICaL_jct = 0.9; .units(unitless);
Fx_ICaL_SL = 0.1; .units(unitless);
gamma_Cai = 0.341; .units(unitless);
gamma_Cao = 0.341; .units(unitless);
gamma_Nai = 0.75; .units(unitless);
gamma_Nao = 0.75; .units(unitless);
gamma_Ki = 0.75; .units(unitless);
gamma_Ko = 0.75; .units(unitless);
Q10_CaL = 1.8; .units(unitless);
V_max = 9; .units(uA/uF);
Fx_NCX_jct = 0.11; .units(unitless);
Fx_NCX_SL = 0.89; .units(unitless);
Q10_NCX = 1.57; .units(unitless);
K_mNai = 12.29; .units(mmol/L);
K_mCao = 1.3; .units(mmol/L);
K_mNao = 87.5; .units(mmol/L);
K_mCai = 0.00359; .units(mmol/L);
Kd_act = 0.000256; .units(mmol/L);
ksat = 0.27; .units(unitless);
eta = 0.35; .units(unitless);
HNa = 3; .units(unitless);
Fx_SLCaP_jct = 0.11; .units(unitless);
Fx_SLCaP_SL = 0.89; .units(unitless);
Q10_SLCaP = 2.35; .units(unitless);
Km = 0.0005; .units(mmol/L);
H = 1.6; .units(unitless);
V_maxAF = 0.0673; .units(uA/uF);
GCaBk = 0.0002513; .units(mS/uF);
Fx_CaBk_jct = 0.11; .units(unitless);
Fx_CaBk_SL = 0.89; .units(unitless);
Max_SR = 15; .units(unitless);
Min_SR = 1; .units(unitless);
EC50_SR = 0.45; .units(mmol/L);
ks = 25; .units(unitless/ms);
koCa = 10; .units(L^2/mmol^2/ms);
kom = 0.06; .units(unitless/ms);
kiCa = 0.5; .units(L/mmol/ms);
kim = 0.005; .units(unitless/ms);
HSR = 2.5; .units(unitless);
KSRleak = 5.348e-6; .units(unitless/ms);
V_max_cp0 = 5.3114e-3; .units(mmol/L/ms);
Q10_SRCaP = 2.6; .units(unitless);
Kmf = 0.000246; .units(mmol/L);
Kmr = 1.7; .units(mmol/L);
H_cp0 = 1.787; .units(unitless);
Bmax_SL = 1.65; .units(mmol/L);
Bmax_jct = 7.561; .units(mmol/L);
kon = 0.0001; .units(L/mmol/ms);
koff = 1e-3; .units(unitless/ms);
Bmax_SLB_SL = 0.0374; .units(mmol/L);
Bmax_SLB_jct = 0.0046; .units(mmol/L);
Bmax_SLHigh_SL = 0.0134; .units(mmol/L);
Bmax_SLHigh_jct = 0.00165; .units(mmol/L);
Bmax_Calsequestrin = 0.14; .units(mmol/L);
kon_SL = 100; .units(L/mmol/ms);
kon_Calsequestrin = 100; .units(L/mmol/ms);
koff_SLB = 1.3; .units(unitless/ms);
koff_SLHigh = 30e-3; .units(unitless/ms);
koff_Calsequestrin = 65; .units(unitless/ms);
Bmax_TroponinC = 0.07; .units(mmol/L);
Bmax_TroponinC_Ca_MGCa = 0.14; .units(mmol/L);
Bmax_TroponinC_Ca_MGMg = 0.14; .units(mmol/L);
Bmax_Calmodulin = 0.024; .units(mmol/L);
Bmax_Myosin_Ca = 0.14; .units(mmol/L);
Bmax_Myosin_Mg = 0.14; .units(mmol/L);
Bmax_SRB = 0.0171; .units(mmol/L);
kon_TroponinC = 32.7; .units(L/mmol/ms);
kon_TroponinC_Ca_MGCa = 2.37; .units(L/mmol/ms);
kon_TroponinC_Ca_MGMg = 3e-3; .units(L/mmol/ms);
kon_Calmodulin = 34; .units(L/mmol/ms);
kon_Myosin_Ca = 13.8; .units(L/mmol/ms);
kon_Myosin_Mg = 15.7e-3; .units(L/mmol/ms);
kon_SRB = 100; .units(L/mmol/ms);
koff_TroponinC = 19.6e-3; .units(unitless/ms);
koff_TroponinC_Ca_MGCa = 0.032e-3; .units(unitless/ms);
koff_TroponinC_Ca_MGMg = 3.33e-3; .units(unitless/ms);
koff_Calmodulin = 238e-3; .units(unitless/ms);
koff_Myosin_Ca = 0.46e-3; .units(unitless/ms);
koff_Myosin_Mg = 0.057e-3; .units(unitless/ms);
koff_SRB = 60e-3; .units(unitless/ms);

# Initial values
V_init = -8.556885e1; .units(mV);
h_init = 9.867005e-1; .units(unitless);
j_init = 9.91562e-1; .units(unitless);
m_init = 1.405627e-3; .units(unitless);
Xr_init = 8.641386e-3; .units(unitless);
Xs_init = 5.412034e-3; .units(unitless);
X_tos_init = 4.051574e-3; .units(unitless);
Y_tos_init = 9.945511e-1; .units(unitless);
R_tos_init = 0.9946; .units(unitless);
X_tof_init = 4.051574e-3; .units(unitless);
Y_tof_init = 9.945511e-1; .units(unitless);
d_init = 7.175662e-6; .units(unitless);
f_init = 1.000681; .units(unitless);
fCaB_SL_init = 1.452605e-2; .units(unitless);
fCaB_jct_init = 2.421991e-2; .units(unitless);
R_cp0_init = 8.884332e-1; .units(unitless);
I_init = 1.024274e-7; .units(unitless);
O_init = 8.156628e-7; .units(unitless);
Na_SL_init = 8.80733; .units(mmol/L);
Na_jct_init = 8.80329; .units(mmol/L);
Na_SL_buf_init = 7.720854e-1; .units(mmol/L);
Na_jct_buf_init = 3.539892; .units(mmol/L);
Nai_init = 8.80853; .units(mmol/L);
Ca_SR_init = 5.545201e-1; .units(mmol/L);
Ca_SL_init = 1.031812e-4; .units(mmol/L);
Ca_jct_init = 1.737475e-4; .units(mmol/L);
Cai_init = 8.597401e-5; .units(mmol/L);
Ca_SLB_SL_init = 1.110363e-1; .units(mmol/L);
Ca_SLB_jct_init = 9.566355e-3; .units(mmol/L);
Ca_SLHigh_SL_init = 7.297378e-2; .units(mmol/L);
Ca_SLHigh_jct_init = 7.347888e-3; .units(mmol/L);
Ca_Calsequestrin_init = 1.242988; .units(mmol/L);
Ca_TroponinC_init = 8.773191e-3; .units(mmol/L);
Ca_TroponinC_Ca_Mg_init = 1.078283e-1; .units(mmol/L);
MGTroponinC_Ca_Mg_init = 1.524002e-2; .units(mmol/L);
Ca_Calmodulin_init = 2.911916e-4; .units(mmol/L);
Ca_Myosin_init = 1.298754e-3; .units(mmol/L);
MGMyosin_init = 1.381982e-1; .units(mmol/L);
Ca_SRB_init = 2.143165e-3; .units(mmol/L);

group {
    Cai;
    Ca_SR;
    Ca_SRB;
    Ca_SL;
    Ca_jct;
}.method(rosenbrock);

group{
    V;
    Cai;
    Ca_SR;
    Ca_jct;
    Ca_SRB;
}.trace();

#cell
Iion = (INa+IbNa+INaK+IKr+IKs+Itos+Itof+IK1+INaCa+IClCa+IbCl+ICaL+IbCa+IpCa+IKp); .units(mV/ms);

#model_parameters
VolSL = (0.02*VolCell); .units(L);
Voljct = (0.0539*0.01*VolCell); .units(L);
Volmyo = (0.65*VolCell); .units(L);
VolCell = ((3.141592654*((cell_radius/1000.)*(cell_radius/1000.))*cell_length)/(1000.*1000.*1000.)); .units(L);
VolSR = (0.035*VolCell); .units(L);

#reversal_potentials
E_Na_SL = (((R*T)/F)*log((Nao/Na_SL))); .units(mV);
E_Ca_SL = (((R*T)/(2.*F))*log((Cao/Ca_SL))); .units(mV);
E_Cl = (((R*T)/F)*log((Cli/Clo))); .units(mV);
E_Na_jct = (((R*T)/F)*log((Nao/Na_jct))); .units(mV);
E_Ca_jct = (((R*T)/(2.*F))*log((Cao/Ca_jct))); .units(mV);
E_K = (((R*T)/F)*log((Ko/Ki))); .units(mV);

#INa
openProb = ((m*m*m)*h*j); .units(unitless);
INa = (INa_jct+INa_SL); .units(uA/uF);
INa_SL = (Fx_Na_SL*GINa*openProb*(V - E_Na_SL)); .units(uA/uF);
INa_jct = (Fx_Na_jct*GINa*openProb*(V - E_Na_jct)); .units(uA/uF);

#INa_h_gate
beta_h = ((V<-40.) ? ((3.56*exp((0.079*V)))+(3.1e5*exp((0.35*V)))) : (1./(0.13*(1.+exp(((V+10.66)/-11.1)))))); .units(unitless/ms);
alpha_h = ((V<-40.) ? (0.135*exp(((80.+V)/-6.8))) : 0.); .units(unitless/ms);

#INa_j_gate
beta_j = ((V<-40.) ? ((0.1212*exp((-0.01052*V)))/(1.+exp((-0.1378*(V+40.14))))) : ((0.3*exp((-2.535e-7*V)))/(1.+exp((-0.1*(V+32.)))))); .units(unitless/ms);
alpha_j = ((V<-40.) ? (((((-1.2714e5*exp((0.2444*V))) - (3.474e-5*exp((-0.04391*V))))*(V+37.78))/1.)/(1.+exp((0.311*(V+79.23))))) : 0.); .units(unitless/ms);

#INa_m_gate
alpha_m = (((0.32*(V+47.13))/1.)/(1. - exp((-0.1*(V+47.13))))); .units(unitless/ms);
beta_m = (0.08*exp((-V/11.))); .units(unitless/ms);

#IbNa
IbNa_jct = (Fx_NaBk_jct*GNaBk*(V - E_Na_jct)); .units(uA/uF);
IbNa = (IbNa_jct+IbNa_SL); .units(uA/uF);
IbNa_SL = (Fx_NaBk_SL*GNaBk*(V - E_Na_SL)); .units(uA/uF);

#INaK
f_NaK = (1./(1.+(0.1245*exp(((-0.1*V*F)/(R*T))))+(0.0365*sigma*exp(((-V*F)/(R*T)))))); .units(unitless);
INaK_jct = ((((Fx_NaK_jct*INaK_max*f_NaK)/(1.+pow((Km_Nai/Na_jct),H_NaK)))*Ko)/(Ko+Km_Ko)); .units(uA/uF);
INaK = (INaK_jct+INaK_SL); .units(uA/uF);
sigma = ((exp((Nao/67.3)) - 1.)/7.); .units(unitless);
INaK_SL = ((((Fx_NaK_SL*INaK_max*f_NaK)/(1.+pow((Km_Nai/Na_SL),H_NaK)))*Ko)/(Ko+Km_Ko)); .units(uA/uF);

#IKr
IKr = (GIKr*Xr*Rr*(V - E_K)); .units(uA/uF);
GIKr = (0.03*sqrt((Ko/5.4))); .units(mS/uF);

#IKr_Xr_gate
Xr_infinity = (1./(1.+exp((-(50.+V)/7.5)))); .units(unitless);
tau_Xr = (1./(((0.00138*(V+7.))/(1. - exp((-0.123*(V+7.)))))+((0.00061*(V+10.))/(exp((0.145*(V+10.))) - 1.)))); .units(ms);

#IKr_Rr_gate
Rr = (1./(1.+exp(((33.+V)/22.4)))); .units(unitless);

#IKs
IKs = (IKs_jct+IKs_SL); .units(uA/uF);
E_Ks = (((R*T)/F)*log(((Ko+(pKNa*Nao))/(Ki+(pKNa*Nai))))); .units(mV);
pCa_SL = (-log10((Ca_SL/1.))+3.); .units(unitless);
GKs_jct = (0.07*(0.057+(0.19/(1.+exp(((-7.2+pCa_jct)/0.6)))))); .units(mS/uF);
GKs_SL = (0.07*(0.057+(0.19/(1.+exp(((-7.2+pCa_SL)/0.6)))))); .units(mS/uF);
IKs_jct = (Fx_Ks_jct*GKs_jct*(Xs*Xs)*(V - E_Ks)); .units(uA/uF);
IKs_SL = (Fx_Ks_SL*GKs_SL*(Xs*Xs)*(V - E_Ks)); .units(uA/uF);
pCa_jct = (-log10((Ca_jct/1.))+3.); .units(unitless);

#IKs_Xs_gate
tau_Xs = (1./(((7.19e-5*(V+30.))/(1. - exp((-0.148*(V+30.)))))+((1.31e-4*(V+30.))/(-1.+exp((0.0687*(V+30.))))))); .units(ms);
Xs_infinity = (1./(1.+exp((-(V - 1.5)/16.7)))); .units(unitless);

#IKp
IKp = ((GKp*(V - E_K))/(1.+exp((7.488 - (V/5.98))))); .units(uA/uF);

#Itos
Itos = (Gtos*X_tos*(Y_tos+(0.5*R_tos))*(V - E_K)); .units(uA/uF);

#Itos_X_gate
X_tos_infinity = (1./(1.+exp((-(V+3.)/15.)))); .units(unitless);
tau_X_tos = ((9./(1.+exp(((V+3.)/15.))))+0.5); .units(ms);

#Itos_Y_gate
tau_Y_tos = ((3000./(1.+exp(((V+60.)/10.))))+30.); .units(ms);
Y_tos_infinity = (1./(1.+exp(((V+33.5)/10.)))); .units(unitless);

#Itos_R_gate
R_tos_infinity = (1./(1.+exp(((V+33.5)/10.)))); .units(unitless);
tau_R_tos = ((2.8e3/(1.+exp(((V+60.)/10.))))+220.); .units(ms);

#Itof
Itof = (Gtof*X_tof*Y_tof*(V - E_K)); .units(uA/uF);

#Itof_X_gate
tau_X_tof = ((3.5*exp(-((V/30.)*(V/30.))))+1.5); .units(ms);
X_tof_infinity = (1./(1.+exp((-(V+3.)/15.)))); .units(unitless);

#Itof_Y_gate
Y_tof_infinity = (1./(1.+exp(((V+33.5)/10.)))); .units(unitless);
tau_Y_tof = ((20./(1.+exp(((V+33.5)/10.))))+20.); .units(ms);

#IK1
IK1 = (GK1*K1_infinity*(V - E_K)); .units(uA/uF);
GK1 = (0.9*sqrt((Ko/5.4))); .units(mS/uF);

#IK1_K1_gate
K1_infinity = (alpha_K1/(alpha_K1+beta_K1)); .units(unitless);
beta_K1 = (((0.49124*exp((0.08032*((V - E_K)+5.476))))+(1.*exp((0.06175*(V - (E_K+594.31))))))/(1.+exp((-0.5143*((V - E_K)+4.753))))); .units(unitless/ms);
alpha_K1 = (1.02/(1.+exp((0.2385*(V - (E_K+59.215)))))); .units(unitless/ms);

#ICl_Ca
IClCa = (GCl*(V - E_Cl)*((Fx_Cl_jct/(1.+(Kd_ClCa/Ca_jct)))+(Fx_Cl_SL/(1.+(Kd_ClCa/Ca_SL))))); .units(uA/uF);

#IClb
IbCl = (GClBk*(V - E_Cl)); .units(uA/uF);

#ICaL
temp = ((0.45*d*f*Q_CaL*V*(F*F))/(R*T)); .units(C/mol);
ICaL_Ca_jct = ((temp*fCa_jct*Fx_ICaL_jct*PCa*4.*((gamma_Cai*Ca_jct*exp(((2.*V*F)/(R*T)))) - (gamma_Cao*Cao)))/(exp(((2.*V*F)/(R*T))) - 1.)); .units(uA/uF);
ICaL = (ICaL_Ca_SL+ICaL_Ca_jct+ICaL_Na_SL+ICaL_Na_jct+ICaL_K); .units(uA/uF);
ICaL_Ca_SL = ((temp*fCa_SL*Fx_ICaL_SL*PCa*4.*((gamma_Cai*Ca_SL*exp(((2.*V*F)/(R*T)))) - (gamma_Cao*Cao)))/(exp(((2.*V*F)/(R*T))) - 1.)); .units(uA/uF);
Q_CaL = pow(Q10_CaL,((T - 310.)/10.)); .units(unitless);
ICaL_K = ((temp*((fCa_SL*Fx_ICaL_SL)+(fCa_jct*Fx_ICaL_jct))*PK*((gamma_Ki*Ki*exp(((V*F)/(R*T)))) - (gamma_Ko*Ko)))/(exp(((V*F)/(R*T))) - 1.)); .units(uA/uF);
ICaL_Na_jct = ((temp*fCa_jct*Fx_ICaL_jct*PNa*((gamma_Nai*Na_jct*exp(((V*F)/(R*T)))) - (gamma_Nao*Nao)))/(exp(((V*F)/(R*T))) - 1.)); .units(uA/uF);
ICaL_Na_SL = ((temp*fCa_SL*Fx_ICaL_SL*PNa*((gamma_Nai*Na_SL*exp(((V*F)/(R*T)))) - (gamma_Nao*Nao)))/(exp(((V*F)/(R*T))) - 1.)); .units(uA/uF);

#ICaL_d_gate
tau_d = ((1.*d_infinity*(1. - exp((-(V+14.5)/6.))))/(0.035*(V+14.5))); .units(ms);
d_infinity = (1./(1.+exp((-(V+14.5)/6.)))); .units(unitless);

#ICaL_f_gate
tau_f = (1./((0.0197*exp(-((0.0337*(V+14.5))*(0.0337*(V+14.5)))))+0.02)); .units(ms);
f_infinity = ((1./(1.+exp(((V+35.06)/3.6))))+(0.6/(1.+exp(((50. - V)/20.))))); .units(unitless);

#ICaL_fCa_gate
fCa_SL = (1. - fCaB_SL); .units(unitless);
fCa_jct = (1. - fCaB_jct); .units(unitless);
diff_fCaB_SL = ((1.7*Ca_SL*(1. - fCaB_SL)) - (11.9e-3*fCaB_SL)); .units(unitless/ms);
diff_fCaB_jct = ((1.7*Ca_jct*(1. - fCaB_jct)) - (11.9e-3*fCaB_jct)); .units(unitless/ms);

#INaCa
Ka_jct = (1./(1.+((Kd_act/Ca_jct)*(Kd_act/Ca_jct)*(Kd_act/Ca_jct)))); .units(unitless);
temp_SL = (((exp(((eta*V*F)/(R*T)))*pow(Na_SL,HNa)*Cao) - (exp((((eta - 1.)*V*F)/(R*T)))*pow(Nao,HNa)*Ca_SL))/(1.+(ksat*exp((((eta - 1.)*V*F)/(R*T)))))); .units(mmol^4/L^4);
INaCa = (INaCa_jct+INaCa_SL); .units(uA/uF);
Q_NCX = pow(Q10_NCX,((T - 310.)/10.)); .units(unitless);
INaCa_jct = ((Fx_NCX_jct*V_max*Ka_jct*Q_NCX*temp_jct)/((K_mCai*pow(Nao,HNa)*(1.+pow((Na_jct/K_mNai),HNa)))+(pow(K_mNao,HNa)*Ca_jct*(1.+(Ca_jct/K_mCai)))+(K_mCao*pow(Na_jct,HNa))+(pow(Na_jct,HNa)*Cao)+(pow(Nao,HNa)*Ca_jct))); .units(uA/uF);
Ka_SL = (1./(1.+((Kd_act/Ca_SL)*(Kd_act/Ca_SL)*(Kd_act/Ca_SL)))); .units(unitless);
INaCa_SL = ((Fx_NCX_SL*V_max*Ka_SL*Q_NCX*temp_SL)/((K_mCai*pow(Nao,HNa)*(1.+pow((Na_SL/K_mNai),HNa)))+(pow(K_mNao,HNa)*Ca_SL*(1.+(Ca_SL/K_mCai)))+(K_mCao*pow(Na_SL,HNa))+(pow(Na_SL,HNa)*Cao)+(pow(Nao,HNa)*Ca_SL))); .units(uA/uF);
temp_jct = (((exp(((eta*V*F)/(R*T)))*pow(Na_jct,HNa)*Cao) - (exp((((eta - 1.)*V*F)/(R*T)))*pow(Nao,HNa)*Ca_jct))/(1.+(ksat*exp((((eta - 1.)*V*F)/(R*T)))))); .units(mmol^4/L^4);

#ICap
IpCa = (IpCa_jct+IpCa_SL); .units(uA/uF);
IpCa_SL = ((Q_SLCaP*V_maxAF*Fx_SLCaP_SL)/(1.+pow((Km/Ca_SL),H))); .units(uA/uF);
IpCa_jct = ((Q_SLCaP*V_maxAF*Fx_SLCaP_jct)/(1.+pow((Km/Ca_jct),H))); .units(uA/uF);
Q_SLCaP = pow(Q10_SLCaP,((T - 310.)/10.)); .units(unitless);

#ICab
IbCa_SL = (GCaBk*Fx_CaBk_SL*(V - E_Ca_SL)); .units(uA/uF);
IbCa_jct = (GCaBk*Fx_CaBk_jct*(V - E_Ca_jct)); .units(uA/uF);
IbCa = (IbCa_SL+IbCa_jct); .units(uA/uF);

#Jrel_SR
kCaSR = (Max_SR - ((Max_SR - Min_SR)/(1.+pow((EC50_SR/Ca_SR),HSR)))); .units(unitless);
kiSRCa = (kiCa*kCaSR); .units(L/mmol/ms);
diff_R_cp0 = (((kim*RI) - (kiSRCa*Ca_jct*R_cp0)) - ((koSRCa*(Ca_jct*Ca_jct)*R_cp0) - (kom*O))); .units(unitless/ms);
diff_O = (((koSRCa*(Ca_jct*Ca_jct)*R_cp0) - (kom*O)) - ((kiSRCa*Ca_jct*O) - (kim*I))); .units(unitless/ms);
diff_I = (((kiSRCa*Ca_jct*O) - (kim*I)) - ((kom*I) - (koSRCa*(Ca_jct*Ca_jct)*RI))); .units(unitless/ms);
RI = (((1. - R_cp0) - O) - I); .units(unitless);
j_rel_SR = (ks*O*(Ca_SR - Ca_jct)); .units(mmol/L/ms);
koSRCa = (koCa/kCaSR); .units(L^2/mmol^2/ms);

#Jleak_SR
j_leak_SR = (KSRleak*(Ca_SR - Ca_jct)); .units(mmol/L/ms);

#Jpump_SR
j_pump_SR = ((Q_SRCaP*V_max_cp0*(pow((Cai/Kmf),H_cp0) - pow((Ca_SR/Kmr),H_cp0)))/(1.+pow((Cai/Kmf),H_cp0)+pow((Ca_SR/Kmr),H_cp0))); .units(mmol/L/ms);
Q_SRCaP = pow(Q10_SRCaP,((T - 310.)/10.)); .units(unitless);

#ion_diffusion
J_Ca_jct_SL = ((Ca_jct - Ca_SL)*8.2413e-13); .units(mmol/ms);
J_Ca_SL_myo = ((Ca_SL - Cai)*3.7243e-12); .units(mmol/ms);
J_Na_jct_SL = ((Na_jct - Na_SL)*1.8313e-14); .units(mmol/ms);
J_Na_SL_myo = ((Na_SL - Nai)*1.6386e-12); .units(mmol/ms);

#Na_buffer
diff_Na_jct_buf = dNa_jct_buf; .units(mmol/L/ms);
diff_Na_SL_buf = dNa_SL_buf; .units(mmol/L/ms);
diff_Nai = (J_Na_SL_myo/Volmyo); .units(mmol/L/ms);
diff_Na_jct = ((((-Cm*(INa_jct+(3.*INaCa_jct)+IbNa_jct+(3.*INaK_jct)+ICaL_Na_jct))/(Voljct*F)) - (J_Na_jct_SL/Voljct)) - dNa_jct_buf); .units(mmol/L/ms);
dNa_jct_buf = ((kon*Na_jct*(Bmax_jct - Na_jct_buf)) - (koff*Na_jct_buf)); .units(mmol/L/ms);
diff_Na_SL = ((((-Cm*(INa_SL+(3.*INaCa_SL)+IbNa_SL+(3.*INaK_SL)+ICaL_Na_SL))/(VolSL*F))+((J_Na_jct_SL - J_Na_SL_myo)/VolSL)) - dNa_SL_buf); .units(mmol/L/ms);
dNa_SL_buf = ((kon*Na_SL*(Bmax_SL - Na_SL_buf)) - (koff*Na_SL_buf)); .units(mmol/L/ms);

#Ca_buffer
dCa_SLHigh_SL = ((kon_SL*Ca_SL*(((Bmax_SLHigh_SL*Volmyo)/VolSL) - Ca_SLHigh_SL)) - (koff_SLHigh*Ca_SLHigh_SL)); .units(mmol/L/ms);
dCa_SLHigh_jct = ((kon_SL*Ca_jct*(((Bmax_SLHigh_jct*0.1*Volmyo)/Voljct) - Ca_SLHigh_jct)) - (koff_SLHigh*Ca_SLHigh_jct)); .units(mmol/L/ms);
diff_Ca_SLHigh_SL = dCa_SLHigh_SL; .units(mmol/L/ms);
i_Ca_SL_tot = ((ICaL_Ca_SL - (2.*INaCa_SL))+IbCa_SL+IpCa_SL); .units(uA/uF);
diff_Ca_SLB_SL = dCa_SLB_SL; .units(mmol/L/ms);
diff_Ca_SLB_jct = dCa_SLB_jct; .units(mmol/L/ms);
dCa_jct_tot_bound = (dCa_SLB_jct+dCa_SLHigh_jct); .units(mmol/L/ms);
diff_Ca_SR = ((j_pump_SR - (((j_leak_SR*Volmyo)/VolSR)+j_rel_SR)) - dCalsequestrin); .units(mmol/L/ms);
i_Ca_jct_tot = ((ICaL_Ca_jct - (2.*INaCa_jct))+IbCa_jct+IpCa_jct); .units(uA/uF);
diff_Ca_jct = (((((-i_Ca_jct_tot*Cm)/(Voljct*2.*F)) - (J_Ca_jct_SL/Voljct))+((j_rel_SR*VolSR)/Voljct)+((j_leak_SR*Volmyo)/Voljct)) - (1.*dCa_jct_tot_bound)); .units(mmol/L/ms);
dCa_SLB_SL = ((kon_SL*Ca_SL*(((Bmax_SLB_SL*Volmyo)/VolSL) - Ca_SLB_SL)) - (koff_SLB*Ca_SLB_SL)); .units(mmol/L/ms);
diff_Cai = ((((-j_pump_SR*VolSR)/Volmyo)+(J_Ca_SL_myo/Volmyo)) - (1.*dCa_cytosol_tot_bound)); .units(mmol/L/ms);
diff_Ca_SLHigh_jct = dCa_SLHigh_jct; .units(mmol/L/ms);
dCa_SL_tot_bound = (dCa_SLB_SL+dCa_SLHigh_SL); .units(mmol/L/ms);
dCalsequestrin = ((kon_Calsequestrin*Ca_SR*(((Bmax_Calsequestrin*Volmyo)/VolSR) - Ca_Calsequestrin)) - (koff_Calsequestrin*Ca_Calsequestrin)); .units(mmol/L/ms);
dCa_SLB_jct = ((kon_SL*Ca_jct*(((Bmax_SLB_jct*0.1*Volmyo)/Voljct) - Ca_SLB_jct)) - (koff_SLB*Ca_SLB_jct)); .units(mmol/L/ms);
diff_Ca_SL = ((((-i_Ca_SL_tot*Cm)/(VolSL*2.*F))+((J_Ca_jct_SL - J_Ca_SL_myo)/VolSL)) - (1.*dCa_SL_tot_bound)); .units(mmol/L/ms);
diff_Ca_Calsequestrin = dCalsequestrin; .units(mmol/L/ms);

#cytosolic_Ca_buffer
dMGTroponinC_Ca_Mg = ((kon_TroponinC_Ca_MGMg*Mgi*(Bmax_TroponinC_Ca_MGMg - (Ca_TroponinC_Ca_Mg+MGTroponinC_Ca_Mg))) - (koff_TroponinC_Ca_MGMg*MGTroponinC_Ca_Mg)); .units(mmol/L/ms);
dCa_Calmodulin = ((kon_Calmodulin*Cai*(Bmax_Calmodulin - Ca_Calmodulin)) - (koff_Calmodulin*Ca_Calmodulin)); .units(mmol/L/ms);
diff_Ca_TroponinC = dCa_TroponinC; .units(mmol/L/ms);
diff_MGMyosin = dMGMyosin; .units(mmol/L/ms);
diff_Ca_SRB = dCa_SRB; .units(mmol/L/ms);
dCa_Myosin = ((kon_Myosin_Ca*Cai*(Bmax_Myosin_Ca - (Ca_Myosin+MGMyosin))) - (koff_Myosin_Ca*Ca_Myosin)); .units(mmol/L/ms);
diff_Ca_Calmodulin = dCa_Calmodulin; .units(mmol/L/ms);
diff_MGTroponinC_Ca_Mg = dMGTroponinC_Ca_Mg; .units(mmol/L/ms);
diff_Ca_TroponinC_Ca_Mg = dCa_TroponinC_Ca_Mg; .units(mmol/L/ms);
dMGMyosin = ((kon_Myosin_Mg*Mgi*(Bmax_Myosin_Mg - (Ca_Myosin+MGMyosin))) - (koff_Myosin_Mg*MGMyosin)); .units(mmol/L/ms);
diff_Ca_Myosin = dCa_Myosin; .units(mmol/L/ms);
dCa_cytosol_tot_bound = (dCa_TroponinC+dCa_TroponinC_Ca_Mg+dMGTroponinC_Ca_Mg+dCa_Calmodulin+dCa_Myosin+dMGMyosin+dCa_SRB); .units(mmol/L/ms);
dCa_TroponinC_Ca_Mg = ((kon_TroponinC_Ca_MGCa*Cai*(Bmax_TroponinC_Ca_MGCa - (Ca_TroponinC_Ca_Mg+MGTroponinC_Ca_Mg))) - (koff_TroponinC_Ca_MGCa*Ca_TroponinC_Ca_Mg)); .units(mmol/L/ms);
dCa_SRB = ((kon_SRB*Cai*(Bmax_SRB - Ca_SRB)) - (koff_SRB*Ca_SRB)); .units(mmol/L/ms);
dCa_TroponinC = ((kon_TroponinC*Cai*(Bmax_TroponinC - Ca_TroponinC)) - (koff_TroponinC*Ca_TroponinC)); .units(mmol/L/ms);
