// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

#ifndef PrMTYPES
#define PrMTYPES
/*
    Definition des types List, bytes, Char, Double, complex d_complex
    et rgba
*/
typedef struct List {
  long  nitems;
  char* items;
} List;
typedef unsigned char byte;
#ifndef __GL_GL_H__
typedef unsigned char Byte;
typedef char*         String;
#endif
typedef signed char      Char;
typedef struct S_Complex S_Complex;
typedef double           Double;
typedef struct D_Complex D_Complex;
typedef float            Float;
typedef int              Int;
typedef long             Long;
typedef unsigned int     UInt;
typedef short            Short;
typedef int              BooleaN;
typedef int              Flag;
typedef char*            RDir;
typedef char*            RFile;
typedef char*            RWDir;
typedef char*            RWFile;
typedef char*            WDir;
typedef char*            WFile;
typedef char**           Text;
typedef void             Any;
struct S_Complex {
  Float real, imag;
};
struct D_Complex {
  Double real, imag;
};
#endif
