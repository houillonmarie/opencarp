// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

  param.h

  Author: Andre Bleau, eng.

  Laboratoire de Modelisation Biomedicale
  Institut de Genie Biomedical
  Ecole Polytechnique / Faculte de Medecine
  Universite de Montreal

  Revision: April 3, 2000

\*---------------------------------------------------------------------------*/

#include "sys_param.h"
#include <stdlib.h>

#define VERSION "2.4"

#define NALLOC 100 /* Number of lines allocated at a time */

#define PrMMAXINT 2147483647

/* Parser states */
#define IN_UNKNOWN    0
#define IN_FUNCTIOND  1
#define IN_STRUCTURE  2
#define IN_MEMBER     3
#define IN_PARAMGRP   4
#define IN_LOCATION   5
#define IN_VARIABLE   6
#define IN_MASK       127
#define IN_DESCRIPTOR 128
#define IN_FUNCTION   256
#define IN_ARGUMENT   512

/* Sections */
#define DEFINITIONS  33
#define DECLARATIONS 34

/* Allocation classes */
#define ALLOCATION_BASE 65
#define REGISTER        65
#define AUTO            66
#define STATIC          67
#define EXTERN          68
#define GLOBAL          69
#define ALLOCATION_MAX  69

/* Kinds */
#define KIND_BASE 81
#define COMPILED  81
#define RUNTIME   82
#define KIND_MAX  82

/* Storage classes */
#define STORAGE_BASE   97
#define SCALAR         97
#define STATIC_VECTOR  98
#define DYNAMIC_VECTOR 99
#define STORAGE_MAX    99

/* Views */
#define VIEW_BASE 113
#define LARGE     113
#define SMALL     114
#define VIEW_MAX  114

/* Parameter types */
#define TYPE_BASE 129
#define BYTE      129
#define CHAR      130
#define COMPLEX   131
#define DOUBLE    132
#define D_COMPLEX 133
#define FLOAT     134
#define INT       135
#define LONG      136
#define SHORT     137
#define BOOLEAN   138
#define RDIR      139
#define RFILE     140
#define RWDIR     141
#define RWFILE    142
#define STRING    143
#define WDIR      144
#define WFILE     145
#define FLAG      146
#define VOID      147
#define ANY       148
#define EXPR      149
#define TYPE_MAX  150

#define LVALUE   255
#define FUNCTION 256

#define TRUE  1
#define FALSE 0

/* Language of programmer's main() function */
#define C       0
#define FORTRAN 1
#define MATLAB  2

/* C dialects */
#define ANSI 0
#define KR   1 /* Kernighan & Ritchie original definition \
     (1978); no function prototypes */

/* Graphic interface */
#define NONE  0
#define MOTIF 1

typedef unsigned char Byte;
#if (SYSTEME == SGI4D)
typedef signed char Char;
#else
typedef char Char;
#endif
typedef struct Complex   Complex;
typedef double           Double;
typedef struct D_Complex D_Complex;
typedef float            Float;
typedef int              Int;
typedef long             Long;
typedef short            Short;
typedef int              Boolean;
typedef char*            RDir;
typedef char*            RFile;
typedef char*            RWDir;
typedef char*            RWFile;
typedef char*            String;
typedef char*            WDir;
typedef char*            WFile;
typedef int              Flag;

typedef char** Text;

typedef struct Arg         Arg;
typedef struct Description Description;
typedef struct Descriptor  Descriptor;
typedef struct Function    Function;
typedef struct Member      Member;
typedef struct Parameters  Parameters;
typedef struct Paramgrp    Paramgrp;
typedef struct Pgrp_item   Pgrp_item;
typedef struct State       State;
typedef struct Structure   Structure;
typedef struct Value       Value;
typedef struct Variable    Variable;

struct Complex {
  Float real, imag;
};

struct D_Complex {
  Double real, imag;
};

struct Arg {
  char* str;
  int   type;
  Arg*  next;
};

struct Function {
  char*     name;
  Value*    type;
  Arg*      arg_list;
  Function* next;
};

struct Value {
  int      type;
  int      intdata;
  char*    data;
  char*    label;     /* Label in menus */
  char*    sensitive; /* In menus */
  Text     dependences;
  Boolean* related;
  Value*   next;
};

struct Descriptor {
  int         type;
  Value*      value;
  Descriptor* next;
};

#ifndef output
/* Avoid conflicts with lex output macro */
struct Description {
  int        allocation;
  Boolean    auxiliary;
  Value*     default_value;
  Value*     dir;
  Function*  display;
  Value*     ext;
  Boolean    hidden;
  char*      key;
  int        kind;
  Text       l_desc;
  int        length;
  Boolean    mandatory;
  Value*     max;
  Value*     menu;
  Value*     min;
  Value*     output;
  int        storage;
  Structure* structure;
  char*      s_desc;
  Value*     type;
  Value*     units;
  Function*  validf;
  int        view;
  Boolean    volatil;
  Text       dependences;
  Boolean*   related;
  Boolean    error;
};
#endif

struct Variable {
  /* Fields set by the parser */
  char*        name;
  Description* description;
  Structure*   structure;
  Text         text;
  Variable*    next_in_pgrp;
  /* Fields set by restructure() */
  int       index;
  int       array_level;
  int       bottom_level;
  int       dependence_level;
  int       n_dependents;
  Boolean   dyn_ancestor;
  Boolean   related;
  Boolean   static_root;
  Value*    bounds;
  Value*    bottom_bound;
  char*     ancestry;
  char*     boolean;
  char*     father;
  char*     format;
  char*     func_name;
  char*     match;
  char*     reference;
  Paramgrp* paramgrp;
  Variable* default_ctl;
  Variable* dependents;
  Variable* influenced;
  Variable* max_ctl;
  Variable* min_ctl;
  Variable* next;
};

struct Member {
  /* Fields set by the parser */
  char*        name;
  Description* description;
  Member*      next;
  /* Fields set by restructure() */
  Variable* associate_var;
};

struct Structure {
  char*      name;
  Member*    member_list;
  int        n_members;
  Structure* next;
};

struct Pgrp_item {
  int        type;
  char*      item;
  Pgrp_item* next;
};

struct Paramgrp {
  char*      name;
  Text       l_desc;
  Paramgrp*  paramsgrp_list;
  Paramgrp*  father;
  char*      s_desc;
  Function*  validf;
  Variable*  variable_list;
  Variable*  location_list;
  Variable*  textbox_list;
  Pgrp_item* pgrp_item_list;
  Boolean    error;
  int        n_items;
  int        view;
  Paramgrp*  next;
  Paramgrp*  next_in_sgrp;
  Boolean    hidden;
};

struct Parameters {
  /* Fields set by the parser */
  int        gallocation;
  int        n_arrays;
  int        n_auxiliaries;
  int        n_compiled;
  int        n_dirs;
  int        n_files;
  int        n_locations;
  int        n_mandatories;
  int        n_outputs;
  int        n_short_ints;
  int        n_strings;
  int        n_structures;
  int        n_struct_arrays;
  int        n_textboxes;
  int        n_variables;
  int        n_volatils;
  Boolean    error;
  Boolean    acegr; /* TRUE if $ACEGR_VEC is used at least
       once as a display function */
  Boolean    trail_args;
  Text       gl_desc;
  Text       inc_code;
  char*      gs_desc;
  char*      icon;
  Function*  end_function;
  Function*  gvalidf;
  Function*  function_list;
  Structure* structure_list;
  Paramgrp*  main_paramgrp;
  Paramgrp*  paramgrp_list;

  /* Fields set by restructure() */
  int        max_array_level;
  int        max_dependents;
  int        max_depend_level;
  int        n_leafs;
  int        n_nodes;
  int        n_param;      /* n_leafs + n_nodes */
  int        n_parameters; /* n_variables + n_locations */
  int        n_scalars;
  Boolean    run_on_comp;  /* TRUE if runtime parameters depend
       on compiled ones */
  Variable** array_vector; /* Vector of n_arrays Variable* */
  Variable*  assign_list;  /* List of variables to assign in
       main */
  Variable*  dictionary;
  Variable*  dyn_vec_list;        /* List of dynamic variables */
  Variable** location_vector;     /* Vector of n_locations Variable* */
  Variable*  node_list;           /* List of node variables */
  Variable*  param_list;          /* List of leaf variables */
  Variable** param_vector;        /* Vector of n_param Variable* */
  Variable** scalar_vector;       /* Vector of n_scalars Variable* */
  Variable** string_vector;       /* Vector of n_strings Variable* */
  Variable** structure_vector;    /* Vector of n_structures Variable* */
  Variable** struct_array_vector; /* Vector of n_struct_arrays
       Variable* */
  Variable** variable_vector;     /* Vector of n_variables Variable* */
};

struct State {
  int   last_state;
  int   state;
  int   section;
  int   word;
  char* id_argument;
  char* id_descriptor;
  char* id_function;
  char* id_member;
  char* id_paramgrp;
  char* id_structure;
  Text  id_functiond;
  Text  id_location;
  Text  id_variable;
};

void MotifDirCode(FILE*, Variable*);
void MotifFileCode(FILE*, Variable*);
void MotifMainCode(FILE*, Parameters*);
void MotifParamsgrpCode(FILE*, Paramgrp*);
void MotifStructureCode(FILE*, Variable*);
void MotifVariableCode(FILE*, Variable*);
void MotifTextboxCode(FILE*, Variable*);

void build_c(const char*, Parameters*, char*, char*, char*, char*, char*);
void build_dummy(char*, Parameters*, char*, char*, char*, char*, Boolean, char*, char*, char*);
void build_f(char*, char*, char*, Parameters*, char*);
void build_m(char*, Parameters*, char*, char*);
void build_make(char*, char*, char*, char*, char*, char*, Boolean, char*);
void build_mex(char*, char*, char*, Parameters*, char*);
void display(Parameters*);
void initialize(Parameters*);

/* c_code */
void initial_code(FILE* file, Variable* variable, const char* initial_tabs, int dependence_level);
void alloc_code(FILE* file, Variable* variable, const char* initial_tabs, int dependence_level);
void realloc_code(FILE*, Variable*, const char*, int);
void reinitial_code(FILE*, Variable*, const char*);
void file_name_code(FILE* file, Variable* variable);
void save_code(FILE* file, Variable* variable, int dependence_level, Boolean compiled);
void strip_name_code(FILE* file, Variable* variable);
void assign_code(FILE* file, Variable* variable);
void voloutaux_code(FILE* file, Variable* variable);
void clean_code(FILE* file, Variable* variable, const char* initial_tabs, int dependence_level);

void         restructure(Parameters*, int);
Description* substitute_father(Description*, char*);
Description* substitute_index(Description*, char*);

/* Functions called by the parser */
Arg*         Farg(int, char*);
Arg*         Farg_id(Arg*);
Arg*         Farg_list(Arg*, Arg*);
Description* Fdescription(Descriptor*);
Descriptor*  Fallocation(int);
Descriptor*  Fauxiliary(void);
Descriptor*  Fdefault_value(Value*);
Descriptor*  Fdesc_list(Descriptor*, Descriptor*);
Descriptor*  Fdir_value(Value*);
Descriptor*  Fdisplay(Function* function);
Descriptor*  Fext_value(Value*);
Descriptor*  Fhidden(void);
Descriptor*  Fkey(char*);
Descriptor*  Fkind(int);
Descriptor*  Fl_desc(Text);
Descriptor*  Flength(char*);
Descriptor*  Fline_desc(void);
Descriptor*  Flocation_list(Variable*);
Descriptor*  Fmandatory(void);
Descriptor*  Fmax_value(Value*);
Descriptor*  Fmenu(Value*);
Descriptor*  Fmin_value(Value*);
Descriptor*  Foutput(Value* value);
Descriptor*  Fs_desc(char*);
Descriptor*  Ftype(Value*);
Descriptor*  Funits_value(Value*);
Descriptor*  Fvalidf(Function*);
Descriptor*  Fparamsgrp(Text);
Descriptor*  Ftextbox_list(Variable*);
Descriptor*  Fvariable_list(Variable*);
Descriptor*  Fview(int);
Descriptor*  Fvolatil(void);
Function*    Ffunction(char*, Arg*);
Member*      Fline_member(void);
Member*      Fmember(char*, Description*);
Member*      Fmember_list(Member*, Member*);
Paramgrp*    Fparamgrp(Parameters*, char*, Descriptor*);
Text         Ffuncd_id_list(Text);
Text         Floc_id_list(Text);
Text         Ftext(Text, const char*);
Text         Fvar_id_list(Text);
Value*       Fany(int, char*);
Value*       Fc_exp(char*, Value*);
Value*       Fitem(Value*, Value*, Value*);
Value*       Fstruct_value(char*);
Value*       Fstruct_vector(char*, Value*);
Value*       Ftype_value(int);
Value*       Ftype_vector(int, Value*);
Value*       Fvalue(int, char*);
Value*       Fvalue_func(Function*);
Value*       Fvalue_vector(char*, Value*, Value*, Value*);
Value*       Fvector(Value*, Value*);
Variable*    Flocation(Text, Description*);
Variable*    Ftextbox(Text);
Variable*    Fvariable(Text, Description*);
char*        Farray(char*, char*);
char*        Farrow(char*, char*);
char*        Ffunc_id(char*);
char*        Fmem_id(char*);
char*        Fpgrp_id(char*);
char*        Fpoint(char*, char*);
char*        Fstring(char*, char*);
char*        Fstruct_id(char*);
const char*  Fword_to_string(int);
int          Fadd_state(int);
int          Fdesc_id(int);
int          Fget_last_state(void);
int          Fget_section(void);
int          Fget_state(void);
int          Fget_word(void);
int          Fset_section(int);
int          Fset_state(int);
int          Fsub_state(int);
int          Fset_word(int);
void         Fend_function(Parameters*, Function*);
void         Ffunc_declare(Parameters*, Text, Descriptor*);
void         Fgallocation(Parameters*, int);
void         Fgl_desc(Parameters*, Text);
void         Fglocation(Parameters*, Variable*);
void         Fgparamsgrp(Parameters*, Paramgrp*);
void         Fgs_desc(Parameters*, char*);
void         Fgtextbox(Parameters*, Variable*);
void         Fgvalidf(Parameters*, Function*);
void         Fgvariable(Parameters*, Variable*);
void         Ficon(Parameters*, char*);
void         Finc_code(Parameters*, Text);
void         Fline_spec(char*, char*);
void         Fml_desc(Parameters*, Descriptor*);
void         Fms_desc(Parameters*, Descriptor*);
void         Fmvalidf(Parameters*, Descriptor*);
void         Fmview(Parameters*, Descriptor*);
void         Fmhide(Parameters* parameters, Descriptor* descriptor);
void         Finit_state(void);
void         Fstructure(Parameters*, char*, Member*);
void         Ftrail_args(Parameters*);

/* Utility functions */
Variable*    add_entry(Variable*, Variable*);
Variable*    copy_variable(Variable*);
Variable*    variable_named(Variable*, char*);
Description* copy_description(Description*);
Function*    copy_function(Function*);
Function*    function_named(char*);
Structure*   copy_structure(Structure*);
Value*       copy_value(Value*);
Text         copy_text(Text);
Text         newtext(void);
Text         addline(Text, const char*);
Text         addnewline(Text, const char*);
Text         merge(Text, Text);
char*        chartoint(char*);
char*        escquotes(char*);
char*        newstr(const char*);
char*        new_textbox_name(void);
char*        substr(const char*, const char*);
char*        substitute_first(char*, const char*, const char*);
char*        substitute_all(char*, const char*, const char*);
char*        unquote(char*);
void         add_dependence(Text*, Boolean**, char*, Boolean);
void         cast_type(Value*, int);
void         freetext(Text);
void         init_lex(void);
void         merge_dependences(Text*, Boolean**, Text, Boolean*);
void         skipcomment(void);
void         substitute_in_text(Text, const char*, const char*);
Boolean*     addboolean(Boolean*, Boolean);
Boolean*     addrelated(Boolean*, const char*);
Boolean      always(void);
Boolean      constant_value(Value*);
Boolean*     copy_related(Boolean*);
Boolean      invalid_value(Value*, int);
Boolean      match(Variable*, char*);
Boolean*     newrelated(void);
Boolean      never(void);
Structure*   structure_named(char*);
Paramgrp*    paramgrp_named(char*);
