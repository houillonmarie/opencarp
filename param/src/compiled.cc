// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

    compiled:  Prints code for reading and saving compiled parameters.

    Author:  Andre Bleau, eng.

    Laboratoire de Modelisation Biomedicale
    Institut de Genie Biomedical
    Ecole Polytechnique / Faculte de Medecine
    Universite de Montreal

    Revision:  October 5, 1998

\*---------------------------------------------------------------------------*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

void read_file_code(FILE*, Variable*, const char*, Boolean);
void save_code(FILE*, Variable*, int, Boolean);

void compiled(FILE* file, Parameters* parameters)
{
  extern int c_dialect;
  int        level;
  Variable*  variable;
  Paramgrp*  paramgrp;
  Function*  function;
  Arg*       arg;

  fprintf(file, "/* PrMreadcompiled()\t");
  fprintf(file, "*****************************************************/\n\n");
  if (c_dialect == ANSI) {
    fprintf(file, "static int\tPrMreadcompiled(char *PrMfilename)\n\n");
  } else {
    fprintf(file, "static int\tPrMreadcompiled(PrMfilename)\n\n");
    fprintf(file, "char\t*PrMfilename;\n\n");
  }

  fprintf(file, "{\n\n");

  fprintf(file, "\t    int\n");
  fprintf(file, "\tPrMint_arg,\n");
  for (level = 1; level <= parameters->max_array_level; level++)
    fprintf(file, "\tPrMelem%d,\n", level);
  fprintf(file, "\tPrMlineno,\n");
  for (level = 0; level < parameters->max_dependents; level++)
    fprintf(file, "\tPrMold_size%d,\n", level);
  fprintf(file, "\tPrMnew_size,\n");
  fprintf(file, "\tPrMnwords;\n");
  fprintf(file, "\t    BooleaN\n");
  fprintf(file, "\tPrMcontinuation,\n");
  fprintf(file, "\tPrMerror = PrMFALSE;\n");
  fprintf(file, "\t    Text\n");
  fprintf(file, "\tPrMwords;\n");
  fprintf(file, "\t    char\n");
  fprintf(file, "\t*PrMcomment,\n");
  fprintf(file, "\t*PrMeol,\n");
  fprintf(file, "\tPrMlasterror[257],\n");
  fprintf(file, "\tPrMline[257],\n");
  fprintf(file, "\t*PrMmore_lines,\n");
  fprintf(file, "\tPrMspace[81],\n");
  fprintf(file, "\t*PrMword;\n");
  fprintf(file, "\t    FILE\n");
  fprintf(file, "\t*PrMfile;\n");

  fprintf(file, "PrMclearerrors();\n");
  fprintf(file, "PrMwords = PrMnewtext();\n\n");

  fprintf(file, "if (strcmp(PrMfilename, \"-\")==0)\n");
  fprintf(file, "    PrMfile = stdin;\n");
  fprintf(file, "else\n");
  fprintf(file, "    PrMfile = fopen(PrMfilename, \"r\");\n");
  fprintf(file, "if (PrMfile==NULL) {\n");
  fprintf(file, "    return(PrMTRUE);\n");
  fprintf(file, "}\n");
  fprintf(file, "PrMlineno = 0;\n");
  fprintf(file, "while (PrMTRUE) {\n");
  fprintf(file, "    PrMnwords = 0;\n");
  fprintf(file, "    PrMwords[0] = NULL;\n");
  fprintf(file, "    do {\n");
  fprintf(file, "\tPrMcontinuation = PrMFALSE;\n");
  fprintf(file, "\tPrMmore_lines = fgets(PrMline, 256, PrMfile);\n");
  fprintf(file, "\tPrMeol = strrchr(PrMline, '\\n');\n");
  fprintf(file, "\tPrMcomment = strchr(PrMline, '#');\n");
  fprintf(file, "\tif (PrMcomment)\n");
  fprintf(file, "\t    PrMeol = PrMcomment;\n");
  fprintf(file, "\tif (PrMeol) {\n");
  fprintf(file, "\t    *PrMeol-- = '\\000';\n");
  fprintf(file, "\t    if (PrMeol==PrMline-1 || *PrMeol=='\\\\')\n");
  fprintf(file, "\t    PrMcontinuation = PrMTRUE;\n");
  fprintf(file, "\t}\n");
  fprintf(file, "\tPrMquote(PrMline);\n");
  fprintf(file, "\tPrMword = strtok(PrMline, \" \\t=,\\\\\");\n");
  fprintf(file, "\twhile (PrMword) {\n");
  fprintf(file, "\t    PrMunquote(PrMword);\n");
  fprintf(file, "\t    PrMnwords++;\n");
  fprintf(file, "\t    PrMwords = PrMaddline(PrMwords, PrMword);\n");
  fprintf(file, "\t    if (PrMnwords==1) PrMword = strtok(NULL, \" \\t=,\\\\\");\n");
  fprintf(file, "\t    else              PrMword = strtok(NULL, \" \\t,\\\\\");\n");
  fprintf(file, "\t}\n");
  fprintf(file, "\tPrMlineno++;\n");
  fprintf(file, "    } while ((PrMcontinuation || !PrMwords[0]) && PrMmore_lines);\n");
  fprintf(file, "    if (NULL==PrMmore_lines) break;\n");

  /*
      Loop to print code that tries to match each parameter in turn
      to words read in a file
  */
  variable = parameters->param_list;
  while (variable) {
    read_file_code(file, variable, "    ", 1);
    variable = variable->next;
  }

  fprintf(file, "    else {\n");
  fprintf(file, "\tsprintf(PrMlasterror,\n");
  fprintf(file, "\t \"\\n*** Unrecognized keyword %%s\\n\\n\", PrMwords[0]);\n");
  fprintf(file, "\tPrMadderror(PrMlasterror);\n");
  fprintf(file, "\treturn(PrMTRUE);\n");
  fprintf(file, "    }\n");
  fprintf(file, "    for (int i = 0; i<PrMnwords; i++)\n");
  fprintf(file, "      free(PrMwords[i]);\n");
  fprintf(file, "} /* while (PrMTRUE) */\n\n");

  fprintf(file, "if (PrMfile!=stdin) fclose(PrMfile);\n\n");
  fprintf(file, "free(PrMwords);\n");

  fprintf(file, "return(PrMerror);\n\n");

  fprintf(file, "}\n\n");

  fprintf(file, "/* PrMsavecompiled()\t");
  fprintf(file, "*****************************************************/\n\n");
  if (c_dialect == ANSI) {
    fprintf(file, "static int\tPrMsavecompiled(char *PrMfilename, char *PrMprogname)\n\n");
  } else {
    fprintf(file, "static int\tPrMsavecompiled(PrMfilename, PrMprogname)\n\n");
    fprintf(file, "char\t*PrMfilename;\n");
    fprintf(file, "char\t*PrMprogname;\n\n");
  }

  fprintf(file, "{\n\n");

  fprintf(file, "\t    int\n");
  for (level = 1; level <= parameters->max_array_level; level++)
    fprintf(file, "\tPrMelem%d,\n", level);
  fprintf(file, "\tPrMerror;\n");
  fprintf(file, "\t    time_t\n");
  fprintf(file, "\tPrMtime;\n");
  fprintf(file, "\t    char\n");
  fprintf(file, "\tPrMlasterror[257];\n");
  fprintf(file, "\t    FILE\n");
  fprintf(file, "\t*PrMsave_file;\n\n");

  fprintf(file, "if (strcmp(PrMfilename, \"-\")==0)\n");
  fprintf(file, "    PrMsave_file = stdout;\n");
  fprintf(file, "else\n");
  fprintf(file, "    PrMsave_file = fopen(PrMfilename, \"w\");\n");
  fprintf(file, "if (PrMsave_file==NULL) {\n");
  fprintf(file, "    sprintf(PrMlasterror, \n");
  fprintf(file, "     \"\\n*** Error opening save file %%s for writing\\n\",\n");
  fprintf(file, "     PrMfilename);\n");
  fprintf(file, "    PrMadderror(PrMlasterror);\n");
#if (SYSTEME == SUNOS)
  fprintf(file, "    sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n");
#else
  fprintf(file, "    sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n");
#endif
  fprintf(file, "    PrMadderror(PrMlasterror);\n");
  fprintf(file, "    return(PrMTRUE);\n");
  fprintf(file, "}\n");
  fprintf(file, "PrMtime = time(NULL);\n");
  fprintf(file, "fprintf(PrMsave_file, \"#\\n\");\n");
  fprintf(file, "fprintf(PrMsave_file, \"# %%s\\n\", PrMprogname);\n");
  fprintf(file, "fprintf(PrMsave_file, \"#\\n\");\n");
  fprintf(file, "fprintf(PrMsave_file, \"# %%s\", ctime(&PrMtime));\n");
  fprintf(file, "fprintf(PrMsave_file, \"#\\n\");\n");
  level = 0;
  while (level <= parameters->max_depend_level) {
    fprintf(file, "/*  Dependence level %d */\n", level);
    variable = parameters->param_list;
    while (variable) {
      save_code(file, variable, level, 1);
      variable = variable->next;
    }
    level++;
  }
  fprintf(file, "if (PrMsave_file!=stdout) fclose(PrMsave_file);\n\n");

  fprintf(file, "    return(PrMFALSE);\n\n");

  fprintf(file, "}\n\n");
}
