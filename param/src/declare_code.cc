// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

    declare_code:  Build declarations file for param() function.

    Author:  Andre Bleau, eng.

    Laboratoire de Modelisation Biomedicale
    Institut de Genie Biomedical
    Ecole Polytechnique / Faculte de Medecine
    Universite de Montreal

    Revision:  December 5, 1994

\*---------------------------------------------------------------------------*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

/* declare_code()  *****************************************************/

void declare_code(FILE* file, Parameters* parameters)
{
  const char * allocation, *type;
  int          var_index;
  Variable*    variable;
  extern char *Allocation_Names[], *Type_Names[];

  // open namespace
  fprintf(file, "namespace param_globals {\n\n");

  fprintf(file, "/* Parameter variables */\n");
  for (var_index = 0; var_index < parameters->n_variables; var_index++) {
    variable = parameters->variable_vector[var_index];
    if (variable->description->allocation)
      allocation = Allocation_Names[variable->description->allocation - ALLOCATION_BASE];
    else if (parameters->gallocation)
      allocation = Allocation_Names[parameters->gallocation - ALLOCATION_BASE];
    else
      allocation = "";
    if (variable->description->structure || variable->description->type->type == ANY)
      type = variable->description->type->data;
    else if (variable->description->length)
      type = "char";
    else
      type = Type_Names[variable->description->type->type - TYPE_BASE];
    switch (variable->description->storage) {
      case SCALAR: fprintf(file, "%s\t    %s\t %s", allocation, type, variable->name); break;
      case STATIC_VECTOR:
        fprintf(file, "%s\t    %s\t %s[%s]", allocation, type, variable->name,
                variable->description->type->next->data);
        break;
      case DYNAMIC_VECTOR:
        fprintf(file, "%s\t    %s\t*%s", allocation, type, variable->name);
        break;
    }
    if (variable->description->length) fprintf(file, "[%d]", variable->description->length + 1);
    fprintf(file, ";\n");
  }
  fprintf(file, "\n");

  // close namespace
  fprintf(file, "}\n\n");
}
