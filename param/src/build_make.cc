// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

    build_make:  Build makefile for C dummy program.

    Author:  Andre Bleau, eng.

    Laboratoire de Modelisation Biomedicale
    Institut de Genie Biomedical
    Ecole Polytechnique / Faculte de Medecine
    Universite de Montreal

    Revision:  March 22, 2000

\*---------------------------------------------------------------------------*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

/* build_make()    ******************************************************/

void build_make(char*   root_name,
                char*   mkf_name,
                char*   prog_name,
                char*   dummy_name,
                char*   autosave,
                char*   command,
                Boolean loop,
                char*   extension)
{
  extern int  interface, language;
  char *      m_file_name, *plain_name; /*  Root name without path */
  const char* save_opt;
  FILE*       file;

  /*
      Create file names from root name and dummy name
  */
  m_file_name = (char*)malloc(strlen(dummy_name) + 5);
  strcpy(m_file_name, dummy_name);
  strcat(m_file_name, ".mkf");
  plain_name = strrchr(root_name, '/');
  if (plain_name)
    plain_name++;
  else
    plain_name = root_name;
  if (autosave[0])
    save_opt = "-DAUTOSAVE";
  else
    save_opt = "";

  file = fopen(m_file_name, "w");
  if (file == NULL) {
    fprintf(stderr, "\n*** Unable to open file '%s'\n", m_file_name);
    perror("*** ");
    fprintf(stderr, "\n\n");
    exit(-1);
  }

  fprintf(file, "I=%s\n", PRM_INCLUDE_PATH);
  fprintf(file, "L=%s\n", PRM_LIB_PATH);
  fprintf(file, "S=.\n");
  fprintf(file, "O=.\n\n");
#if (SYSTEME == SUNOS)
  fprintf(file, "CC_OPT=-g -I$I -temp=.\n");
#else
  fprintf(file, "CC_OPT=-g -I$I\n");
#endif

  if (interface == MOTIF) {
    fprintf(file, "$O/%s: %s.o %s_pd.o %s_m.o\n", dummy_name, dummy_name, plain_name, plain_name);
#if (SYSTEME == SUNOS)
    fprintf(file, "\t$(CC) -o $O/%s %s.o %s_pd.o %s_m.o ${MISC} \\\n", dummy_name, dummy_name,
            plain_name, plain_name);
    fprintf(file, "\t -L$L -lPrMmotif -Bstatic -lXm -lXt -lX11\n");
#else
#if (SYSTEME == SOLARIS)
    fprintf(file, "\t$(CC) -o $O/%s %s.o %s_pd.o %s_m.o ${MISC} \\\n", dummy_name, dummy_name,
            plain_name, plain_name);
    fprintf(file, "\t -L$L -lPrMmotif -lXm -lXt -lX11\n");
#else
    fprintf(file, "\t$(CC) -o $O/%s %s.o %s_pd.o %s_m.o ${MISC} \\\n", dummy_name, dummy_name,
            plain_name, plain_name);
    fprintf(file, "\t -L$L -lPrMmotif -lXm -lXt -lX11 -lPW\n");
#endif
#endif
#if (SYSTEME == SGI4D)
    fprintf(file, "\ttag 0x00820001 $O/%s\n\n", dummy_name);
#endif
    fprintf(file, "%s.o: %s.c %s_p.h %s_d.h\n", dummy_name, dummy_name, plain_name, plain_name);
    fprintf(file, "\t$(CC) -c ${CC_OPT} %s.c\n\n", dummy_name);
    fprintf(file, "%s_pd.o: %s_p.c %s_p.h\n", plain_name, plain_name, plain_name);
    fprintf(file, "\t$(CC) -c ${CC_OPT} -o %s_pd.o %s_p.c -DMOTIF %s\n\n", plain_name, plain_name,
            save_opt);
    fprintf(file, "%s_m.o: %s_m.c %s_p.h\n", plain_name, plain_name, plain_name);
#if (SYSTEME == SUNOS || SYSTEME == LINUX || SYSTEME == WINNT)
    if (loop)
      fprintf(file, "\t$(CC) -c ${CC_OPT} -DPrMSTAY %s_m.c\n\n", plain_name);
    else
      fprintf(file, "\t$(CC) -c ${CC_OPT} %s_m.c\n\n", plain_name);
#else
    if (loop)
      fprintf(file, "\tenv TMPDIR=/usr/tmp $(CC) -c ${CC_OPT} -DPrMSTAY %s_m.c\n\n", plain_name);
    else
      fprintf(file, "\tenv TMPDIR=/usr/tmp $(CC) -c ${CC_OPT} %s_m.c\n\n", plain_name);
#endif
    fprintf(file, "%s.c:   %s_p.c\n", dummy_name, plain_name);
    fprintf(file, "%s_m.c: %s_p.c\n", plain_name, plain_name);
    fprintf(file, "%s_p.h: %s_p.c\n", plain_name, plain_name);
    fprintf(file, "%s_d.h: %s_p.c\n", plain_name, plain_name);
    fprintf(file, "%s_p.c: $S/%s%s\n", plain_name, plain_name, extension);
    if (language == FORTRAN)
      fprintf(file, "\tprm $S/%s -motif -fortran -dummy %s", plain_name, dummy_name);
    else
      fprintf(file, "\tprm $S/%s -motif -dummy %s", plain_name, dummy_name);
  } else {
    fprintf(file, "$O/%s: %s.o %s_pd.o\n", dummy_name, dummy_name, plain_name);
    fprintf(file, "\t$(CC) -o $O/%s %s.o %s_pd.o ${MISC}\n\n", dummy_name, dummy_name, plain_name);
    fprintf(file, "%s.o: %s.c %s_p.h %s_d.h\n", dummy_name, dummy_name, plain_name, plain_name);
    fprintf(file, "\t$(CC) -c ${CC_OPT} %s.c\n\n", dummy_name);
    fprintf(file, "%s_pd.o: %s_p.c %s_p.h\n", plain_name, plain_name, plain_name);
    fprintf(file, "\t$(CC) -c ${CC_OPT} -o %s_pd.o %s_p.c %s\n\n", plain_name, plain_name,
            save_opt);
    fprintf(file, "%s.c:   %s_p.c\n", dummy_name, plain_name);
    fprintf(file, "%s_p.h: %s_p.c\n", plain_name, plain_name);
    fprintf(file, "%s_d.h: %s_p.c\n", plain_name, plain_name);
    fprintf(file, "%s_p.c: $S/%s%s\n", plain_name, plain_name, extension);
    if (language == FORTRAN)
      fprintf(file, "\tprm $S/%s -fortran -dummy %s", plain_name, dummy_name);
    else
      fprintf(file, "\tprm $S/%s -dummy %s", plain_name, dummy_name);
  }

  /*
      Add options to complete the prm command
  */
  if (loop) fprintf(file, " -loop");
  if (0 != strcmp(autosave, "")) fprintf(file, " -save %s", autosave);
  if (0 != strcmp(mkf_name, "")) fprintf(file, " \\\n\t -mkf %s", mkf_name);
  if (0 != strcmp(prog_name, "")) fprintf(file, " -prog %s", prog_name);
  if (command) fprintf(file, " \\\n\t -command \"%s\"", command);
  fprintf(file, "\n\n");

  fclose(file);

  return;
}
