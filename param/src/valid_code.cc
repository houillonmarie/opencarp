// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

    valid_code:  Prints code for parameter validation.

    Author:  Andre Bleau, eng.

    Laboratoire de Modelisation Biomedicale
    Institut de Genie Biomedical
    Ecole Polytechnique / Faculte de Medecine
    Universite de Montreal

    Revision:  October 2, 1998

\*---------------------------------------------------------------------------*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

/* valid_code()    ******************************************************/
/*
    Prints code for parameter validation.
*/
void valid_code(FILE* file, Variable* variable)
{
  int          a_level, bound_level, level;
  char *       boolean, *format, *name;
  Arg*         arg;
  Value *      bound, *item;
  static char  tabs[81];
  extern char* Type_Formats[];

  if (variable->description->output || variable->description->auxiliary) return;

  if (variable->description->min == NULL && variable->description->max == NULL &&
      variable->description->menu == NULL && variable->description->validf == NULL &&
      !variable->description->mandatory) {
    switch (variable->description->type->type) {
      case RDIR:
      case RWDIR:
      case WDIR:
      case RFILE:
      case RWFILE:
      case WFILE:
        /* Variables of these types are always validated */
        break;
      default:
        /* No validation criteria; return */
        return;
    }
  }

  if (variable->description->key)
    name = variable->description->key;
  else
    name = variable->name;
  boolean = variable->boolean;
  format  = Type_Formats[variable->description->type->type - TYPE_BASE];
  /* Skip initial " in format */
  format++;

  tabs[0] = '\000';

  switch (variable->description->storage) {
    case DYNAMIC_VECTOR:
    case STATIC_VECTOR:
      for (level = 1; level <= variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      level--;
      if (variable->description->validf) {
        fprintf(file, "%sif (%s(", tabs, variable->description->validf->name);
        arg = variable->description->validf->arg_list;
        while (arg) {
          if (arg->next)
            fprintf(file, "%s,", arg->str);
          else
            fprintf(file, "%s", arg->str);
          arg = arg->next;
        }
        fprintf(file, ")) {\n");
        fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
        fprintf(file, "%s     \"\\n*** %s[%%d]: Invalid value %.2s\\n\\n\", \n", tabs, name,
                format);
        fprintf(file, "%s     PrMelem%d, %s[PrMelem%d]);\n", tabs, level, name, level);
        fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
        fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
        fprintf(file, "%s}\n", tabs);
      }
      /*
    Code dependent of the data type
      */
      switch (variable->description->type->type) {
        case RDIR:
          if (variable->description->mandatory) {
            fprintf(file, "%sif (0==strlen(%s[PrMelem%d])) {\n", tabs, name, level);
            fprintf(file,
                    "%s    sprintf(PrMlasterror, \"\\n%s[%%d] cannot be empty\\n\", PrMelem%d);\n",
                    tabs, name, level);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          fprintf(file, "%sif (stat(%s[PrMelem%d], &PrMbuf)) {\n", tabs, name, level);
          fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s     \"\\n*** %s[%%d]: Unable to access directory %%s\\n\", \n", tabs,
                  variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);

#if (SYSTEME == SUNOS)
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n", tabs);
#else
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n", tabs);
#endif
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "}\n");
          fprintf(file, "%sif ((PrMbuf.st_mode&0170000)!=0040000) {\n", tabs);
          fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s     \"\\n*** %s[%%d]: %%s is not a directory\", \n", tabs,
                  variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s}\n", tabs);
          fprintf(file, "%sif (access(%s[PrMelem%d], 05)) {\n", tabs, name, level);
          fprintf(file, "%s    sprintf(PrMlasterror,\n", tabs);
          fprintf(file, "%s     \"\\n*** %s[%%d]: Unable to read in directory %%s\\n\", \n", tabs,
                  variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n", tabs);
#else
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n", tabs);
#endif
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s}\n", tabs);
          break;
        case WDIR:
          if (variable->description->mandatory) {
            fprintf(file, "%sif (0==strlen(%s[PrMelem%d])) {\n", tabs, name, level);
            fprintf(file,
                    "%s    sprintf(PrMlasterror, \"\\n%s[%%d] cannot be empty\\n\", PrMelem%d);\n",
                    tabs, name, level);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          fprintf(file, "%sif (stat(%s[PrMelem%d], &PrMbuf)) {\n", tabs, name, level);
          fprintf(file, "%s    if (mkdir(%s[PrMelem%d], 0755)) {\n", tabs, name, level);
          fprintf(file, "%s        sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s         \"\\n*** %s[%%d]: Unable to create directory %%s\\n\",\n", tabs,
                  variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s        PrMadderror(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s        PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s        PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s}\n", tabs);
          fprintf(file, "%selse {\n", tabs);
          fprintf(file, "%s    if ((PrMbuf.st_mode&0170000)!=0040000) {\n", tabs);
          fprintf(file, "%s         \"\\n*** %s[%%d]: %%s is not a directory\", \n", tabs,
                  variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s        PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s        PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s    if (access(%s[PrMelem%d], 03)) {\n", tabs, name, level);
          fprintf(file, "%s        sprintf(PrMlasterror,\n", tabs);
          fprintf(file, "%s         \"\\n*** %s[%%d]: Unable to write in directory %%s\\n\",\n",
                  tabs, variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s        PrMadderror(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s        PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s        PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s}\n", tabs);
          break;
        case RWDIR:
          if (variable->description->mandatory) {
            fprintf(file, "%sif (0==strlen(%s[PrMelem%d])) {\n", tabs, name, level);
            fprintf(file,
                    "%s    sprintf(PrMlasterror, \"\\n%s[%%d] cannot be empty\\n\", PrMelem%d);\n",
                    tabs, name, level);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          fprintf(file, "%sif (stat(%s[PrMelem%d], &PrMbuf)) {\n", tabs, name, level);
          fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s     \"\\n*** %s[%%d]: Unable to access directory %%s\\n\", \n", tabs,
                  name);
          fprintf(file, "%s     PrMelem%d, %s[PrMelem%d]);\n", tabs, level, name, level);
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n", tabs);
#else
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n", tabs);
#endif
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "}\n");
          fprintf(file, "%sif ((PrMbuf.st_mode&0170000)!=0040000) {\n", tabs);
          fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s     \"\\n*** %s[%%d]: %%s is not a directory\", \n", tabs,
                  variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s}\n", tabs);
          fprintf(file, "%sif (access(%s[PrMelem%d], 07)) {\n", tabs, name, level);
          fprintf(file, "%s    sprintf(PrMlasterror,\n", tabs);
          fprintf(file,
                  "%s     \"\\n*** %s[%%d]: Unable to read or write in directory %%s\\n\", \n",
                  tabs, variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n", tabs);
#else
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n", tabs);
#endif
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s}\n", tabs);
          break;
        case RFILE:
          // Unsatisfying checks on RFile parameters do not raise errors, but only warnings
          fprintf(file, "%sif (strlen(%s[PrMelem%d])>0) {\n", tabs, name, level);
          fprintf(file, "%s    if (stat(%s[PrMelem%d], &PrMbuf)) {\n", tabs, name, level);
          fprintf(file, "%s        sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s     \"\\n*** %s[%%d]: Unable to access file %%s\\n\", \n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s    if ((PrMbuf.st_mode&0170000)!=0100000 && \n", tabs);
          fprintf(file, "%s     (PrMbuf.st_mode&0170000)!=0000000) {\n", tabs);
          fprintf(file, "%s        sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s         \"\\n*** %s[%%d]: %%s is not an ordinary file\", \n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s    if (access(%s[PrMelem%d], 04)) {\n", tabs, name, level);
          fprintf(file, "%s        sprintf(PrMlasterror,\n", tabs);
          fprintf(file, "%s         \"\\n*** %s[%%d]: Unable to read file %%s\\n\", \n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s}\n", tabs);
          break;
        case WFILE:
          fprintf(file, "%sif (strlen(%s[PrMelem%d])>0) {\n", tabs, name, level);
          fprintf(file, "%s    if (!stat(%s[PrMelem%d], &PrMbuf)) {\n", tabs, name, level);
          fprintf(file, "%s        if ((PrMbuf.st_mode&0170000)!=0100000 && \n", tabs);
          fprintf(file, "%s         (PrMbuf.st_mode&0170000)!=0000000) {\n", tabs);
          fprintf(file, "%s            sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s             \"\\n*** %s[%%d]: %%s is not an ordinary file\", \n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s            PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s        }\n", tabs);
          fprintf(file, "%s        if (access(%s[PrMelem%d], 02)) {\n", tabs, name, level);
          fprintf(file, "%s            sprintf(PrMlasterror,\n", tabs);
          fprintf(file, "%s             \"\\n*** %s[%%d]: Unable to write file %%s\\n\", \n", tabs,
                  variable->format);
          fprintf(file, "%s             ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s            PrMprintwarning(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file,
                  "%s            sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s            sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s            PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s        }\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s}\n", tabs);
          break;
        case RWFILE:
          fprintf(file, "%sif (strlen(%s[PrMelem%d])>0) {\n", tabs, name, level);
          fprintf(file, "%s    if (stat(%s[PrMelem%d], &PrMbuf)) {\n", tabs, name, level);
          fprintf(file, "%s        sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s         \"\\n*** %s[%%d]: Unable to access file %%s\\n\", \n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s    if ((PrMbuf.st_mode&0170000)!=0100000 && \n", tabs);
          fprintf(file, "%s     (PrMbuf.st_mode&0170000)!=0000000) {\n", tabs);
          fprintf(file, "%s        sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s         \"\\n*** %s[%%d]: %%s is not an ordinary file\", \n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s    if (access(%s[PrMelem%d], 04)) {\n", tabs, name, level);
          fprintf(file, "%s        sprintf(PrMlasterror,\n", tabs);
          fprintf(file, "%s         \"\\n*** %s[%%d]: Unable to read file %%s\\n\", \n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s[PrMelem%d]);\n", name, level);
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s}\n", tabs);
          fprintf(file, "%s}\n", tabs);
          break;
        case STRING:
          if (variable->description->mandatory) {
            fprintf(file, "%sif (0==strlen(%s[PrMelem%d])) {\n", tabs, name, level);
            fprintf(file,
                    "%s    sprintf(PrMlasterror, \"\\n%s[%%d] cannot be empty\\n\", PrMelem%d);\n",
                    tabs, name, level);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          break;
        case EXPR:
        case BOOLEAN:
        case COMPLEX:
        case D_COMPLEX:
          /* There is little to check for these types of variable */
          break;
        case DOUBLE:
        case LONG:
        case SHORT:
          if (variable->description->min) {
            fprintf(file, "%sif (%s[PrMelem%d] < %s) {\n", tabs, name, level,
                    variable->description->min->data);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s[%%d] = %.3s is below the %.3s minimum\\n\\n\", \n",
                    tabs, variable->format, format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s[PrMelem%d], %s);\n", name, level, variable->description->min->data);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          if (variable->description->max) {
            fprintf(file, "%sif (%s[PrMelem%d] > %s) {\n", tabs, name, level,
                    variable->description->max->data);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s[%%d] = %.3s is above the %.3s maximum\\n\\n\", \n",
                    tabs, variable->format, format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s[PrMelem%d], %s);\n", name, level, variable->description->max->data);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          break;
        default:
          if (variable->description->min) {
            fprintf(file, "%sif (%s[PrMelem%d] < %s) {\n", tabs, name, level,
                    variable->description->min->data);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s[%%d] = %.2s is below the %.2s minimum\\n\\n\", \n",
                    tabs, variable->format, format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s[PrMelem%d], %s);\n", name, level, variable->description->min->data);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          if (variable->description->max) {
            fprintf(file, "%sif (%s[PrMelem%d] > %s) {\n", tabs, name, level,
                    variable->description->max->data);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s[%%d] = %.2s is above the %.2s maximum\\n\\n\", \n",
                    tabs, variable->format, format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s[PrMelem%d], %s);\n", name, level, variable->description->max->data);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          break;
      }
      /*
    Code for menus
      */
      if (item = variable->description->menu) {
        switch (variable->description->type->type) {
          case RDIR:
          case RWDIR:
          case WDIR:
          case RFILE:
          case RWFILE:
          case WFILE:
          case STRING:
          case EXPR:
            if (item->sensitive)
              fprintf(file, "%sif (0==strcmp(%s[PrMelem%d], %s) && (%s)) ;", tabs, name, level,
                      item->data, item->sensitive);
            else
              fprintf(file, "%sif (0==strcmp(%s[PrMelem%d], %s)) ;", tabs, name, level, item->data);
            if (item->label) fprintf(file, "\t/* %s */", item->label);
            fprintf(file, "\n");
            while (item = item->next) {
              if (item->sensitive)
                fprintf(file, "%selse if (0==strcmp(%s[PrMelem%d], %s) && (%s)) ;", tabs, name,
                        level, item->data, item->sensitive);
              else
                fprintf(file, "%selse if (0==strcmp(%s[PrMelem%d], %s)) ;", tabs, name, level,
                        item->data);
              if (item->label) fprintf(file, "\t/* %s */", item->label);
              fprintf(file, "\n");
            }
            fprintf(file, "%selse {\n", tabs);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s[%%d] %.2s out of valid set {\\n\",\n", tabs,
                    variable->format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s[PrMelem%d]);\n", name, level);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            item = variable->description->menu;
            while (item) {
              if (item->label) {
                if (item->sensitive) {
                  fprintf(file, "%s    if (%s) {\n", tabs, item->sensitive);
                  fprintf(file, "%s\tsprintf(PrMlasterror, \"%%s\t(%%s)\\n\", %s, %s);\n", tabs,
                          item->data, item->label);
                  fprintf(file, "%s\tPrMadderror(PrMlasterror);\n", tabs);
                  fprintf(file, "%s    }\n", tabs);
                } else {
                  fprintf(file, "%s    sprintf(PrMlasterror, \"%%s\t(%%s)\\n\", %s, %s);\n", tabs,
                          item->data, item->label);
                  fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
                }
              } else {
                if (item->sensitive) {
                  fprintf(file, "%s    if (%s) {\n", tabs, item->sensitive);
                  fprintf(file, "%s\tsprintf(PrMlasterror, \"%%s\\n\", %s);\n", tabs, item->data);
                  fprintf(file, "%s\tPrMadderror(PrMlasterror);\n", tabs);
                  fprintf(file, "%s    }\n", tabs);
                } else {
                  fprintf(file, "%s    sprintf(PrMlasterror, \"%%s\\n\", %s);\n", tabs, item->data);
                  fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
                }
              }
              item = item->next;
            } /* while (item) */
            fprintf(file, "%s    sprintf(PrMlasterror, \"\\n*** }\\n\\n\");\n", tabs);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
            break;
          case BOOLEAN:
          case COMPLEX:
          case D_COMPLEX:
            /* There is no menu for these types of variable */
            break;
          case DOUBLE:
          case LONG:
          case SHORT:
            if (item->sensitive)
              fprintf(file, "%sif (%s[PrMelem%d]==%s && (%s)) ;", tabs, name, level, item->data,
                      item->sensitive);
            else
              fprintf(file, "%sif (%s[PrMelem%d]==%s) ;", tabs, name, level, item->data);
            if (item->label) fprintf(file, "\t/* %s */", item->label);
            fprintf(file, "\n");
            while (item = item->next) {
              if (item->sensitive)
                fprintf(file, "%selse if (%s[PrMelem%d]==%s && (%s)) ;", tabs, name, level,
                        item->data, item->sensitive);
              else
                fprintf(file, "%selse if (%s[PrMelem%d]==%s) ;", tabs, name, level, item->data);
              if (item->label) fprintf(file, "\t/* %s */", item->label);
              fprintf(file, "\n");
            }
            fprintf(file, "%selse {\n", tabs);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s[%%d] %.3s out of valid set {\\n\",\n", tabs,
                    variable->format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s[PrMelem%d]);\n", name, level);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            item = variable->description->menu;
            while (item) {
              if (item->label) {
                if (item->sensitive) {
                  fprintf(file, "%s    if (%s) {\n", tabs, item->sensitive);
                  fprintf(file, "%s\tsprintf(PrMlasterror, \"%s\t(%%s)\\n\", %s);\n", tabs,
                          item->data, item->label);
                  fprintf(file, "%s\tPrMadderror(PrMlasterror);\n", tabs);
                  fprintf(file, "%s    }\n", tabs);
                } else {
                  fprintf(file, "%s    sprintf(PrMlasterror, \"%s\t(%%s)\\n\", %s);\n", tabs,
                          item->data, item->label);
                  fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
                }
              } else {
                if (item->sensitive) {
                  fprintf(file, "%s    if (%s) {\n", tabs, item->sensitive);
                  fprintf(file, "%s\tsprintf(PrMlasterror, \"%s\\n\");\n", tabs, item->data);
                  fprintf(file, "%s\tPrMadderror(PrMlasterror);\n", tabs);
                  fprintf(file, "%s    }\n", tabs);
                } else {
                  fprintf(file, "%s    sprintf(PrMlasterror, \"%s\\n\");\n", tabs, item->data);
                  fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
                }
              }
              item = item->next;
            } /* while (item) */
            fprintf(file, "%s    sprintf(PrMlasterror, \"\\n*** }\\n\\n\");\n", tabs);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
            break;
          default:
            if (item->sensitive)
              fprintf(file, "%sif (%s[PrMelem%d]==%s && (%s)) ;", tabs, name, level, item->data,
                      item->sensitive);
            else
              fprintf(file, "%sif (%s[PrMelem%d]==%s) ;", tabs, name, level, item->data);
            if (item->label) fprintf(file, "\t/* %s */", item->label);
            fprintf(file, "\n");
            while (item = item->next) {
              if (item->sensitive)
                fprintf(file, "%selse if (%s[PrMelem%d]==%s && (%s)) ;", tabs, name, level,
                        item->data, item->sensitive);
              else
                fprintf(file, "%selse if (%s[PrMelem%d]==%s) ;", tabs, name, level, item->data);
              if (item->label) fprintf(file, "\t/* %s */", item->label);
              fprintf(file, "\n");
            }
            fprintf(file, "%selse {\n", tabs);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s[%%d] %.2s out of valid set {\\n\",\n", tabs,
                    variable->format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s[PrMelem%d]);\n", name, level);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            item = variable->description->menu;
            while (item) {
              if (item->label) {
                if (item->sensitive) {
                  fprintf(file, "%s    if (%s) {\n", tabs, item->sensitive);
                  fprintf(file, "%s\tsprintf(PrMlasterror, \"%s\t(%%s)\\n\", %s);\n", tabs,
                          item->data, item->label);
                  fprintf(file, "%s\tPrMadderror(PrMlasterror);\n", tabs);
                  fprintf(file, "%s    }\n", tabs);
                } else {
                  fprintf(file, "%s    sprintf(PrMlasterror, \"%s\t(%%s)\\n\", %s);\n", tabs,
                          item->data, item->label);
                  fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
                }
              } else {
                if (item->sensitive) {
                  fprintf(file, "%s    if (%s) {\n", tabs, item->sensitive);
                  fprintf(file, "%s\tsprintf(PrMlasterror, \"%s\\n\");\n", tabs, item->data);
                  fprintf(file, "%s\tPrMadderror(PrMlasterror);\n", tabs);
                  fprintf(file, "%s    }\n", tabs);
                } else {
                  fprintf(file, "%s    sprintf(PrMlasterror, \"%s\\n\");\n", tabs, item->data);
                  fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
                }
              }
              item = item->next;
            } /* while (item) */
            fprintf(file, "%s    sprintf(PrMlasterror, \"\\n*** }\\n\\n\");\n", tabs);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
            break;
        }
      }
      for (level = 1; level <= variable->array_level; level++) {
        fprintf(file, "%s}\n", &tabs[4 * level]);
      }
      break;
    case SCALAR:
      for (level = 1; level <= variable->array_level; level++) {
        bound = variable->bounds;
        for (bound_level = variable->array_level; bound_level > level; bound_level--) {
          bound = bound->next;
        }
        fprintf(file, "%sfor (PrMelem%d=0; PrMelem%d<%s; PrMelem%d++) {\n", tabs, level, level,
                bound->data, level);
        strcat(tabs, "    ");
      }
      if (variable->description->validf) {
        fprintf(file, "%sif (%s(", tabs, variable->description->validf->name);
        arg = variable->description->validf->arg_list;
        while (arg) {
          if (arg->next)
            fprintf(file, "%s,", arg->str);
          else
            fprintf(file, "%s", arg->str);
          arg = arg->next;
        }
        fprintf(file, ")) {\n");
        fprintf(file, "%s     sprintf(PrMlasterror, \n", tabs);
        fprintf(file, "%s     \"\\n*** %s: Invalid value %.2s\\n\\n\", %s);\n", tabs, name, format,
                name);
        fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
        fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
        fprintf(file, "%s}\n", tabs);
      }
      /*
    Code dependent of the data type
      */
      switch (variable->description->type->type) {
        case RDIR:
          if (variable->description->mandatory) {
            fprintf(file, "%sif (0==strlen(%s)) {\n", tabs, name);
            fprintf(file, "%s    sprintf(PrMlasterror, \"\\n%s cannot be empty\\n\");\n", tabs,
                    name);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          fprintf(file, "%sif (stat(%s, &PrMbuf)) {\n", tabs, name);
          fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s     \"\\n*** %s: Unable to access directory %%s\\n\",\n", tabs,
                  variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n", tabs);
#else
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n", tabs);
#endif
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "}\n");
          fprintf(file, "%sif ((PrMbuf.st_mode&0170000)!=0040000) {\n", tabs);
          fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s     \"\\n*** %s: %%s is not a directory\",\n", tabs, variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s}\n", tabs);
          fprintf(file, "%sif (access(%s, 05)) {\n", tabs, name);
          fprintf(file, "%s    sprintf(PrMlasterror,\n", tabs);
          fprintf(file, "%s     \"\\n*** %s: Unable to read in directory %%s\\n\",\n", tabs,
                  variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n", tabs);
#else
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n", tabs);
#endif
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s}\n", tabs);
          break;
        case WDIR:
          if (variable->description->mandatory) {
            fprintf(file, "%sif (0==strlen(%s)) {\n", tabs, name);
            fprintf(file, "%s    sprintf(PrMlasterror, \"\\n%s cannot be empty\\n\");\n", tabs,
                    name);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          fprintf(file, "%sif (stat(%s, &PrMbuf)) {\n", tabs, name);
          fprintf(file, "%s    if (mkdir(%s, 0755)) {\n", tabs, name);
          fprintf(file, "%s        sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s         \"\\n*** %s: Unable to create directory %%s\\n\",\n", tabs,
                  variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s        PrMadderror(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s        PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s        PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s}\n", tabs);
          fprintf(file, "%selse {\n", tabs);
          fprintf(file, "%s    if ((PrMbuf.st_mode&0170000)!=0040000) {\n", tabs);
          fprintf(file, "%s        sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s         \"\\n*** %s: %%s is not a directory\",\n", tabs,
                  variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s        PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s        PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s    if (access(%s, 03)) {\n", tabs, name);
          fprintf(file, "%s        sprintf(PrMlasterror,\n", tabs);
          fprintf(file, "%s         \"\\n*** %s: Unable to write in directory %%s\\n\",\n", tabs,
                  variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s        PrMadderror(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s        PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s        PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s}\n", tabs);
          break;
        case RWDIR:
          if (variable->description->mandatory) {
            fprintf(file, "%sif (0==strlen(%s)) {\n", tabs, name);
            fprintf(file, "%s    sprintf(PrMlasterror, \"\\n%s cannot be empty\\n\");\n", tabs,
                    name);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          fprintf(file, "%sif (stat(%s, &PrMbuf)) {\n", tabs, name);
          fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s     \"\\n*** %s: Unable to access directory %%s\\n\",\n", tabs,
                  variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n", tabs);
#else
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n", tabs);
#endif
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "}\n");
          fprintf(file, "%sif ((PrMbuf.st_mode&0170000)!=0040000) {\n", tabs);
          fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s     \"\\n*** %s: %%s is not a directory\",\n", tabs, variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s}\n", tabs);
          fprintf(file, "%sif (access(%s, 07)) {\n", tabs, name);
          fprintf(file, "%s    sprintf(PrMlasterror,\n", tabs);
          fprintf(file, "%s     \"\\n*** %s: Unable to read or write in directory %%s\\n\",\n",
                  tabs, variable->format);
          fprintf(file, "%s     ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n", tabs);
#else
          fprintf(file, "%s    sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n", tabs);
#endif
          fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
          fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
          fprintf(file, "%s}\n", tabs);
          break;
        case RFILE:
          // Unsatisfying checks on RFile parameters do not raise errors, but only warnings
          fprintf(file, "%sif (strlen(%s)>0) {\n", tabs, name);
          fprintf(file, "%s    if (stat(%s, &PrMbuf)) {\n", tabs, name);
          fprintf(file, "%s        sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s         \"\\n*** %s: Unable to access file %%s\\n\",\n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s    if ((PrMbuf.st_mode&0170000)!=0100000 && \n", tabs);
          fprintf(file, "%s     (PrMbuf.st_mode&0170000)!=0000000) {\n", tabs);
          fprintf(file, "%s        sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s         \"\\n*** %s: %%s is not an ordinary file\",\n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s    if (access(%s, 04)) {\n", tabs, name);
          fprintf(file, "%s        sprintf(PrMlasterror,\n", tabs);
          fprintf(file, "%s         \"\\n*** %s: Unable to read file %%s\\n\",\n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s}\n", tabs);
          break;
        case WFILE:
          fprintf(file, "%sif (strlen(%s)>0) {\n", tabs, name);
          fprintf(file, "%s    if (!stat(%s, &PrMbuf)) {\n", tabs, name);
          fprintf(file, "%s        if ((PrMbuf.st_mode&0170000)!=0100000 && \n", tabs);
          fprintf(file, "%s         (PrMbuf.st_mode&0170000)!=0000000) {\n", tabs);
          fprintf(file, "%s            sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s             \"\\n*** %s: %%s is not an ordinary file\",\n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s            PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s        }\n", tabs);
          fprintf(file, "%s        if (access(%s, 02)) {\n", tabs, name);
          fprintf(file, "%s            sprintf(PrMlasterror,\n", tabs);
          fprintf(file, "%s             \"\\n*** %s: Unable to write file %%s\\n\",\n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s            PrMprintwarning(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file,
                  "%s            sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s            sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s            PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s        }\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s}\n", tabs);
          break;
        case RWFILE:
          fprintf(file, "%sif (strlen(%s)>0) {\n", tabs, name);
          fprintf(file, "%s    if (stat(%s, &PrMbuf)) {\n", tabs, name);
          fprintf(file, "%s        sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s         \"\\n*** %s: Unable to access file %%s\\n\",\n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s    if ((PrMbuf.st_mode&0170000)!=0100000 && \n", tabs);
          fprintf(file, "%s     (PrMbuf.st_mode&0170000)!=0000000) {\n", tabs);
          fprintf(file, "%s        sprintf(PrMlasterror, \n", tabs);
          fprintf(file, "%s         \"\\n*** %s: %%s is not an ordinary file\",\n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s    if (access(%s, 06)) {\n", tabs, name);
          fprintf(file, "%s        sprintf(PrMlasterror,\n", tabs);
          fprintf(file, "%s         \"\\n*** %s: Unable to read or write file %%s\\n\",\n", tabs,
                  variable->format);
          fprintf(file, "%s         ", tabs);
          for (a_level = 1; a_level <= variable->array_level; a_level++)
            fprintf(file, "PrMelem%d, ", a_level);
          fprintf(file, "%s);\n", name);
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
#if (SYSTEME == SUNOS)
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", sys_errlist[errno]);\n",
                  tabs);
#else
          fprintf(file, "%s        sprintf(PrMlasterror, \"*** %%s\\n\", strerror(errno));\n",
                  tabs);
#endif
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s        sprintf(PrMlasterror, \"\\n\");\n", tabs);
          fprintf(file, "%s        PrMprintwarning(PrMlasterror);\n", tabs);
          fprintf(file, "%s    }\n", tabs);
          fprintf(file, "%s}\n", tabs);
          break;
        case STRING:
          if (variable->description->mandatory) {
            fprintf(file, "%sif (0==strlen(%s)) {\n", tabs, name);
            fprintf(file, "%s    sprintf(PrMlasterror, \"\\n%s cannot be empty\\n\");\n", tabs,
                    name);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          break;
        case EXPR:
        case BOOLEAN:
        case COMPLEX:
        case D_COMPLEX:
          /* There is little to check for these types of variable */
          break;
        case DOUBLE:
        case LONG:
        case SHORT:
          if (variable->description->min) {
            fprintf(file, "%sif (%s < %s) {\n", tabs, name, variable->description->min->data);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s = %.3s is below the %.3s minimum\\n\\n\", \n", tabs,
                    variable->format, format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s, %s);\n", name, variable->description->min->data);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          if (variable->description->max) {
            fprintf(file, "%sif (%s > %s) {\n", tabs, name, variable->description->max->data);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s = %.3s is above the %.3s maximum\\n\\n\", \n", tabs,
                    variable->format, format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s, %s);\n", name, variable->description->max->data);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          break;
        default:
          if (variable->description->min) {
            fprintf(file, "%sif (%s < %s) {\n", tabs, name, variable->description->min->data);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s = %.2s is below the %.2s minimum\\n\\n\", \n", tabs,
                    variable->format, format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s, %s);\n", name, variable->description->min->data);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          if (variable->description->max) {
            fprintf(file, "%sif (%s > %s) {\n", tabs, name, variable->description->max->data);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s = %.2s is above the %.2s maximum\\n\\n\", \n", tabs,
                    variable->format, format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s, %s);\n", name, variable->description->max->data);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
          }
          break;
      }
      /*
    Code for menus
      */
      if (item = variable->description->menu) {
        switch (variable->description->type->type) {
          case RDIR:
          case RWDIR:
          case WDIR:
          case RFILE:
          case RWFILE:
          case WFILE:
          case STRING:
          case EXPR:
            if (item->sensitive)
              fprintf(file, "%sif (0==strcmp(%s, %s) && (%s)) ;", tabs, name, item->data,
                      item->sensitive);
            else
              fprintf(file, "%sif (0==strcmp(%s, %s)) ;", tabs, name, item->data);
            if (item->label) fprintf(file, "\t/* %s */", item->label);
            fprintf(file, "\n");
            while (item = item->next) {
              if (item->sensitive)
                fprintf(file, "%selse if (0==strcmp(%s, %s) && (%s)) ;", tabs, name, item->data,
                        item->sensitive);
              else
                fprintf(file, "%selse if (0==strcmp(%s, %s)) ;", tabs, name, item->data);
              if (item->label) fprintf(file, "\t/* %s */", item->label);
              fprintf(file, "\n");
            }
            fprintf(file, "%selse {\n", tabs);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s %.2s out of valid set {\\n\",\n", tabs,
                    variable->format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s);\n", name);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            item = variable->description->menu;
            while (item) {
              if (item->label) {
                if (item->sensitive) {
                  fprintf(file, "%s    if (%s) {\n", tabs, item->sensitive);
                  fprintf(file, "%s\tsprintf(PrMlasterror, \"%%s\t(%%s)\\n\", %s, %s);\n", tabs,
                          item->data, item->label);
                  fprintf(file, "%s\tPrMadderror(PrMlasterror);\n", tabs);
                  fprintf(file, "%s    }\n", tabs);
                } else {
                  fprintf(file, "%s    sprintf(PrMlasterror, \"%%s\t(%%s)\\n\", %s, %s);\n", tabs,
                          item->data, item->label);
                  fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
                }
              } else {
                if (item->sensitive) {
                  fprintf(file, "%s    if (%s) {\n", tabs, item->sensitive);
                  fprintf(file, "%s\tsprintf(PrMlasterror, \"%%s\\n\", %s);\n", tabs, item->data);
                  fprintf(file, "%s\tPrMadderror(PrMlasterror);\n", tabs);
                  fprintf(file, "%s    }\n", tabs);
                } else {
                  fprintf(file, "%s    sprintf(PrMlasterror, \"%%s\\n\", %s);\n", tabs, item->data);
                  fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
                }
              }
              item = item->next;
            } /* while (item) */
            fprintf(file, "%s    sprintf(PrMlasterror, \"\\n*** }\\n\\n\");\n", tabs);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
            break;
          case BOOLEAN:
          case COMPLEX:
          case D_COMPLEX:
            /* There is no menu for these types of variable */
            break;
          case DOUBLE:
          case LONG:
          case SHORT:
            if (item->sensitive)
              fprintf(file, "%sif (%s==%s && (%s)) ;", tabs, name, item->data, item->sensitive);
            else
              fprintf(file, "%sif (%s==%s) ;", tabs, name, item->data);
            if (item->label) fprintf(file, "\t/* %s */", item->label);
            fprintf(file, "\n");
            while (item = item->next) {
              if (item->sensitive)
                fprintf(file, "%selse if (%s==%s && (%s)) ;", tabs, name, item->data,
                        item->sensitive);
              else
                fprintf(file, "%selse if (%s==%s) ;", tabs, name, item->data);
              if (item->label) fprintf(file, "\t/* %s */", item->label);
              fprintf(file, "\n");
            }
            fprintf(file, "%selse {\n", tabs);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s %.3s out of valid set {\\n\",\n", tabs,
                    variable->format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s);\n", name);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            item = variable->description->menu;
            while (item) {
              if (item->label) {
                if (item->sensitive) {
                  fprintf(file, "%s    if (%s) {\n", tabs, item->sensitive);
                  fprintf(file, "%s\tsprintf(PrMlasterror, \"%s\t(%%s)\\n\", %s);\n", tabs,
                          item->data, item->label);
                  fprintf(file, "%s\tPrMadderror(PrMlasterror);\n", tabs);
                  fprintf(file, "%s    }\n", tabs);
                } else {
                  fprintf(file, "%s    sprintf(PrMlasterror, \"%s\t(%%s)\\n\", %s);\n", tabs,
                          item->data, item->label);
                  fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
                }
              } else {
                if (item->sensitive) {
                  fprintf(file, "%s    if (%s) {\n", tabs, item->sensitive);
                  fprintf(file, "%s\tsprintf(PrMlasterror, \"%s\\n\");\n", tabs, item->data);
                  fprintf(file, "%s\tPrMadderror(PrMlasterror);\n", tabs);
                  fprintf(file, "%s    }\n", tabs);
                } else {
                  fprintf(file, "%s    sprintf(PrMlasterror, \"%s\\n\");\n", tabs, item->data);
                  fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
                }
              }
              item = item->next;
            } /* while (item) */
            fprintf(file, "%s    sprintf(PrMlasterror, \"\\n*** }\\n\\n\");\n", tabs);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
            break;
          default:
            if (item->sensitive)
              fprintf(file, "%sif (%s==%s && (%s)) ;", tabs, name, item->data, item->sensitive);
            else
              fprintf(file, "%sif (%s==%s) ;", tabs, name, item->data);
            if (item->label) fprintf(file, "\t/* %s */", item->label);
            fprintf(file, "\n");
            while (item = item->next) {
              if (item->sensitive)
                fprintf(file, "%selse if (%s==%s && (%s)) ;", tabs, name, item->data,
                        item->sensitive);
              else
                fprintf(file, "%selse if (%s==%s) ;", tabs, name, item->data);
              if (item->label) fprintf(file, "\t/* %s */", item->label);
              fprintf(file, "\n");
            }
            fprintf(file, "%selse {\n", tabs);
            fprintf(file, "%s    sprintf(PrMlasterror, \n", tabs);
            fprintf(file, "%s     \"\\n*** %s %.2s out of valid set {\\n\",\n", tabs,
                    variable->format, format);
            fprintf(file, "%s     ", tabs);
            for (a_level = 1; a_level <= variable->array_level; a_level++)
              fprintf(file, "PrMelem%d, ", a_level);
            fprintf(file, "%s);\n", name);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            item = variable->description->menu;
            while (item) {
              if (item->label) {
                if (item->sensitive) {
                  fprintf(file, "%s    if (%s) {\n", tabs, item->sensitive);
                  fprintf(file, "%s\tsprintf(PrMlasterror, \"%s\t(%%s)\\n\", %s);\n", tabs,
                          item->data, item->label);
                  fprintf(file, "%s\tPrMadderror(PrMlasterror);\n", tabs);
                  fprintf(file, "%s    }\n", tabs);
                } else {
                  fprintf(file, "%s    sprintf(PrMlasterror, \"%s\t(%%s)\\n\", %s);\n", tabs,
                          item->data, item->label);
                  fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
                }
              } else {
                if (item->sensitive) {
                  fprintf(file, "%s    if (%s) {\n", tabs, item->sensitive);
                  fprintf(file, "%s\tsprintf(PrMlasterror, \"%s\\n\");\n", tabs, item->data);
                  fprintf(file, "%s\tPrMadderror(PrMlasterror);\n", tabs);
                  fprintf(file, "%s    }\n", tabs);
                } else {
                  fprintf(file, "%s    sprintf(PrMlasterror, \"%s\\n\");\n", tabs, item->data);
                  fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
                }
              }
              item = item->next;
            } /* while (item) */
            fprintf(file, "%s    sprintf(PrMlasterror, \"\\n*** }\\n\\n\");\n", tabs);
            fprintf(file, "%s    PrMadderror(PrMlasterror);\n", tabs);
            fprintf(file, "%s    PrMerror = PrMTRUE;\n", tabs);
            fprintf(file, "%s}\n", tabs);
            break;
        }
      }
      for (level = 1; level <= variable->array_level; level++) {
        fprintf(file, "%s}\n", &tabs[4 * level]);
      }
      break;
  } /* switch (variable->storage) */
}
