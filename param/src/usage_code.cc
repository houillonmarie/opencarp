// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

usage_code:  Contains usage_code() and short_desc_code() functions.

Author:  Andre Bleau, eng.

Laboratoire de Modelisation Biomedicale
Institut de Genie Biomedical
Ecole Polytechnique / Faculte de Medecine
Universite de Montreal

Revision:  April 25, 1996

\*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

/* usage_code()    *****************************************************/
/*
   Prints code for displaying help messages about a variable
   */

void usage_code(FILE* file, Variable* variable)
{
  int          a_level, bound_level, index, level, line;
  char *       dname, format[257], *match, name[257], scan_format[257], *string, *vname;
  Value *      bound, *value;
  Variable*    dep_variable;
  extern char* Type_Names[];

  if (variable->description->output || variable->description->auxiliary ||
      variable->description->hidden)
    return;

  index   = variable->description->type->type - TYPE_BASE;
  a_level = variable->array_level;
  strcpy(format, variable->format);
  if (variable->description->key)
    vname = variable->description->key;
  else
    vname = variable->name;
  if (variable->description->storage != SCALAR) {
    strcat(format, "[%d]");
    sprintf(name, "%s[PrMelem%d]", vname, a_level);
  } else
    strcpy(name, vname);
  match = substitute_all(format, "%d", "%*d");
  strcpy(scan_format, match);
  if (match != format) free(match);
  strcat(scan_format, "%[ ]");
  match = scan_format;

  /*
     Code for scalar or array element topics
     */
  if (a_level) {
    fprintf(file, "if (1==sscanf(PrMtopic, \"%s\", PrMspace)) {\n", scan_format);
  } else {
    fprintf(file, "if (0==strcmp(PrMtopic, \"%s \") || PrMall) {\n", vname);
  }

  /*
     Print a default value vector
     if default value is different for each element
     */
  if (variable->description->default_value->next) {
    fprintf(file, "    static const char* default_value[] = {\n");
    value = variable->description->default_value;
    while (value) {
      fprintf(file, "\t\"%s\",\n", escquotes(value->data));
      value = value->next;
    }
    fprintf(file, "\t\"\"\n");
    fprintf(file, "    };\n");
  }

  if (a_level) {
    fprintf(file, "    sscanf(PrMtopic, \"%s\"\n     ", format);
    for (level = 1; level <= a_level; level++) fprintf(file, ", &PrMelem%d", level);
    fprintf(file, ");\n");
    for (level = 1; level <= a_level; level++) {
      bound = variable->bounds;
      for (bound_level = a_level; bound_level > level; bound_level--) { bound = bound->next; }
      fprintf(file, "    if (PrMall) PrMelem%d = 0;\n", level);
      if (bound->type == INUM) {
        fprintf(file, "    if (PrMelem%d>=%s || PrMelem%d<0) {\n", level, bound->data, level);
        fprintf(file, "\tfprintf(stderr,\n");
        fprintf(file, "\t \"\\n*** Index #%d (%%d) in %%s is out of bounds [0-%%d]\\n\\n\",\n",
                level);
        fprintf(file, "\t PrMelem%d, PrMtopic, %s-1);\n", level, bound->data);
        fprintf(file, "\treturn;\n");
        fprintf(file, "    }\n");
      }
    }
    /*
       Code to print variable name (with indexes if there are some)
       */
    fprintf(file, "    printf(\"\\n%s:\\n\" \n     ", format);
    for (level = 1; level <= a_level; level++) fprintf(file, ", PrMelem%d", level);
    fprintf(file, ");\n");
  } else {
    /*
       Code to print variable name (with indexes if there are some)
       */
    fprintf(file, "    printf(\"\\n%s:\\n\");\n", format);
  }

  /*
     Code to print variable short description
     */
  if (variable->description->s_desc) {
    fprintf(file, "    printf(\"\\t%%s\\n\\n\",\n");
    fprintf(file, "     %s);\n", variable->description->s_desc);
  }

  /*
     Code to print variable type
     */
  if (variable->description->type->type == ANY)
    fprintf(file, "    printf(\"\\ttype:\\t%s\\n\");\n", variable->description->type->data);
  else
    fprintf(file, "    printf(\"\\ttype:\\t%s\\n\");\n", Type_Names[index]);

  /*
     Code to print variable default value
     */
  if (variable->description->default_value) {
    if (variable->description->default_value->next) {
      fprintf(file, "    printf(\"\\tdefault:%%s\\n\", default_value[PrMelem%d]);\n", a_level);
    } else {
      fprintf(file, "    printf(\"\\tdefault:%s\\n\");\n",
              escquotes(variable->description->default_value->data));
    }
  }

  /*
     Code to print variable minimum value
     */
  if (variable->description->min)
    fprintf(file, "    printf(\"\\tmin:\\t%s\\n\");\n",
            escquotes(variable->description->min->data));

  /*
     Code to print variable maximum value
     */
  if (variable->description->max)
    fprintf(file, "    printf(\"\\tmax:\\t%s\\n\");\n",
            escquotes(variable->description->max->data));

  /*
     Code to print variable menu of allowed values
     */
  if (variable->description->menu) {
    fprintf(file, "    printf(\"\\tmenu: {\\n\");\n");
    value = variable->description->menu;
    while (value) {
      if (value->label) {
        fprintf(file, "    printf(\"\\t\\t%s\\t%%s\\n\", %s);\n", escquotes(value->data),
                value->label);
      } else {
        fprintf(file, "    printf(\"\\t\\t%s\\n\");\n", escquotes(value->data));
      }
      value = value->next;
    }
    fprintf(file, "    printf(\"\\t}\\n\");\n");
  }

  /*
     Code to print variable units
     */
  if (variable->description->units) {
    if (variable->description->units->type == STR)
      fprintf(file, "    printf(\"\\tunits:\\t%%s\\n\", %s);\n",
              variable->description->units->data);
    else
      fprintf(file, "    printf(\"\\tunits:\\t%s\\n\");\n", variable->description->units->data);
  }

  /*
     Code to print variable directory (FILES)
     */
  if (variable->description->dir) {
    if (variable->description->dir->type == STR)
      fprintf(file, "    printf(\"\\tdir:\\t%%s\\n\", %s);\n", variable->description->dir->data);
    else
      fprintf(file, "    printf(\"\\tdir:\\t%s\\n\");\n", variable->description->dir->data);
  }

  /*
     Code to print variable extension (FILES)
     */
  if (variable->description->ext) {
    if (variable->description->ext->type == STR)
      fprintf(file, "    printf(\"\\text:\\t%%s\\n\", %s);\n", variable->description->ext->data);
    else
      fprintf(file, "    printf(\"\\text:\\t%s\\n\");\n", variable->description->ext->data);
  }

  /*
     Code to print variable allocation class
     */
  switch (variable->description->allocation) {
    case 0: break;
    case AUTO: fprintf(file, "    printf(\"\\tAllocation: Auto\\n\");\n"); break;
    case STATIC: fprintf(file, "    printf(\"\\tAllocation: Static\\n\");\n"); break;
    case EXTERN: fprintf(file, "    printf(\"\\tAllocation: Extern\\n\");\n"); break;
    case GLOBAL: fprintf(file, "    printf(\"\\tAllocation: Global\\n\");\n"); break;
  }

  /*
     Code to print variable dependences
     */
  if (variable->description->dependences[0]) {
    fprintf(file, "    printf(\"\\tDepends on: {\\n\");\n");
    line = 0;
    while (string = variable->description->dependences[line++]) {
      fprintf(file, "    printf(\"\\t\\t%s\\n\");\n", string);
    }
    fprintf(file, "    printf(\"\\t}\\n\");\n");
  }

  /*
     Code to print variable dependents
     */
  if (variable->dependents) {
    fprintf(file, "    printf(\"\\tChanges the allocation of: {\\n\");\n");
    dep_variable = variable->dependents;
    while (dep_variable) {
      if (dep_variable->description->key)
        dname = dep_variable->description->key;
      else
        dname = dep_variable->name;
      fprintf(file, "    printf(\"\\t\\t%s\\n\");\n", dname);
      dep_variable = dep_variable->next;
    }
    fprintf(file, "    printf(\"\\t}\\n\");\n");
  }

  /*
     Code to print variables whose default value is controled by this one
     */
  if (variable->default_ctl) {
    fprintf(file, "    printf(\"\\tChanges the default value of: {\\n\");\n");
    dep_variable = variable->default_ctl;
    while (dep_variable) {
      if (dep_variable->description->key)
        dname = dep_variable->description->key;
      else
        dname = dep_variable->name;
      fprintf(file, "    printf(\"\\t\\t%s\\n\");\n", dname);
      dep_variable = dep_variable->next;
    }
    fprintf(file, "    printf(\"\\t}\\n\");\n");
  }

  /*
     Code to print variables influenced by this one
     */
  if (variable->dependents) {
    fprintf(file, "    printf(\"\\tInfluences the value of: {\\n\");\n");
    dep_variable = variable->influenced;
    while (dep_variable) {
      if (dep_variable->description->key)
        dname = dep_variable->description->key;
      else
        dname = dep_variable->name;
      fprintf(file, "    printf(\"\\t\\t%s\\n\");\n", dname);
      dep_variable = dep_variable->next;
    }
    fprintf(file, "    printf(\"\\t}\\n\");\n");
  }

  /*
     Code to print variable long description
     */
  if (variable->description->l_desc) {
    line = 0;
    while (string = variable->description->l_desc[line++]) {
      fprintf(file, "    printf(\n");
      fprintf(file, "     \"%s\\n\");\n", escquotes(string));
    }
    fprintf(file, "    printf(\"\\n\");\n");
  }

  fprintf(file, "    printf(\"\\n\");\n");
  fprintf(file, "    if (!PrMall) return;\n");
  fprintf(file, "}\n");

  if (variable->description->storage == SCALAR) return;

  /*
     Additional code for array topics
     */

  if (a_level > 1) {
    fprintf(file, "else if (1==sscanf(PrMtopic, \"%s\", PrMspace) || PrMall) {\n", scan_format);
  } else {
    fprintf(file, "else if (0==strcmp(PrMtopic, \"%s \") || PrMall) {\n", vname);
  }

  if (a_level > 1) {
    fprintf(file, "    sscanf(PrMtopic, \"%s\"\n     ", variable->format);
    for (level = 1; level < a_level; level++) fprintf(file, ", &PrMelem%d", level);
    fprintf(file, ");\n");
    for (level = 1; level < a_level; level++) {
      bound = variable->bounds;
      for (bound_level = a_level; bound_level > level; bound_level--) { bound = bound->next; }
      fprintf(file, "    if (PrMall) PrMelem%d = 0;\n", level);
      if (bound->type == INUM) {
        fprintf(file, "    if (PrMelem%d>=%s || PrMelem%d<0) {\n", level, bound->data, level);
        fprintf(file, "\tfprintf(stderr,\n");
        fprintf(file, "\t \"\\n*** Index #%d (%%d) in %%s is out of bounds [0-%%d]\\n\\n\",\n",
                level);
        fprintf(file, "\t PrMelem%d, PrMtopic, %s-1);\n", level, bound->data);
        fprintf(file, "\treturn;\n");
        fprintf(file, "    }\n");
      }
    }
    /*
       Code to print variable name (with indexes if there are some)
       */
    fprintf(file, "    printf(\"\\n%s[ ]:\\n\"\n     ", variable->format);
    for (level = 1; level < a_level; level++) fprintf(file, ", PrMelem%d", level);
    fprintf(file, ");\n");
  } else {
    /*
       Code to print variable name (with indexes if there are some)
       */
    fprintf(file, "    printf(\"\\n%s[ ]:\\n\");\n", variable->format);
  }

  /*
     Code to print variable short description
     */
  if (variable->description->s_desc) {
    fprintf(file, "    printf(\"\\t%%s\\n\\n\",\n");
    fprintf(file, "     %s);\n", variable->description->s_desc);
  }

  /*
     Code to print variable type
     */
  if (variable->description->type->type == ANY)
    fprintf(file, "    printf(\"\\ttype:\\t%s[%s]\\n\");\n", variable->description->type->data,
            variable->description->type->next->data);
  else
    fprintf(file, "    printf(\"\\ttype:\\t%s[%s]\\n\");\n", Type_Names[index],
            variable->description->type->next->data);

  /*
     Code to print variable default value or default vector
     */
  if (variable->description->default_value) {
    if (variable->description->default_value->next) {
      fprintf(file, "    printf(\"\\tdefault: {\\n\");\n");
      value = variable->description->default_value;
      while (value) {
        fprintf(file, "    printf(\"\\t\\t%s\\n\");\n", escquotes(value->data));
        value = value->next;
      }
      fprintf(file, "    printf(\"\\t}\\n\");\n");
    } else {
      fprintf(file, "    printf(\"\\tdefault:%s\\n\");\n",
              escquotes(variable->description->default_value->data));
    }
  }

  /*
     Code to print variable minimum value
     */
  if (variable->description->min)
    fprintf(file, "    printf(\"\\tmin:\\t%s\\n\");\n",
            escquotes(variable->description->min->data));

  /*
     Code to print variable maximum value
     */
  if (variable->description->max)
    fprintf(file, "    printf(\"\\tmax:\\t%s\\n\");\n",
            escquotes(variable->description->max->data));

  /*
     Code to print variable menu of allowed values
     */
  if (variable->description->menu) {
    fprintf(file, "    printf(\"\\tmenu: {\\n\");\n");
    value = variable->description->menu;
    while (value) {
      if (value->label) {
        fprintf(file, "    printf(\"\\t\\t%s\\t%%s\\n\", %s);\n", escquotes(value->data),
                value->label);
      } else {
        fprintf(file, "    printf(\"\\t\\t%s\\n\");\n", escquotes(value->data));
      }
      value = value->next;
    }
    fprintf(file, "    printf(\"\\t}\\n\");\n");
  }

  /*
     Code to print variable units
     */
  if (variable->description->units) {
    if (variable->description->units->type == STR)
      fprintf(file, "    printf(\"\\tunits:\\t%%s\\n\", %s);\n",
              variable->description->units->data);
    else
      fprintf(file, "    printf(\"\\tunits:\\t%s\\n\");\n", variable->description->units->data);
  }

  /*
     Code to print variable directory (FILES)
     */
  if (variable->description->dir) {
    if (variable->description->dir->type == STR)
      fprintf(file, "    printf(\"\\tdir:\\t%%s\\n\", %s);\n", variable->description->dir->data);
    else
      fprintf(file, "    printf(\"\\tdir:\\t%s\\n\");\n", variable->description->dir->data);
  }

  /*
     Code to print variable extension (FILES)
     */
  if (variable->description->ext) {
    if (variable->description->ext->type == STR)
      fprintf(file, "    printf(\"\\text:\\t%%s\\n\", %s);\n", variable->description->ext->data);
    else
      fprintf(file, "    printf(\"\\text:\\t%s\\n\");\n", variable->description->ext->data);
  }

  /*
     Code to print variable allocation class
     */
  switch (variable->description->allocation) {
    case 0: break;
    case AUTO: fprintf(file, "    printf(\"\\tAllocation: Auto\\n\");\n"); break;
    case STATIC: fprintf(file, "    printf(\"\\tAllocation: Static\\n\");\n"); break;
    case EXTERN: fprintf(file, "    printf(\"\\tAllocation: Extern\\n\");\n"); break;
    case GLOBAL: fprintf(file, "    printf(\"\\tAllocation: Global\\n\");\n"); break;
  }

  /*
     Code to print variable dependences
     */
  if (variable->description->dependences[0]) {
    fprintf(file, "    printf(\"\\tDepends on: {\\n\");\n");
    line = 0;
    while (string = variable->description->dependences[line++]) {
      fprintf(file, "    printf(\"\\t\\t%s\\n\");\n", string);
    }
    fprintf(file, "    printf(\"\\t}\\n\");\n");
  }

  /*
     Code to print variable dependents
     */
  if (variable->dependents) {
    fprintf(file, "    printf(\"\\tChanges the allocation of: {\\n\");\n");
    dep_variable = variable->dependents;
    while (dep_variable) {
      if (dep_variable->description->key)
        dname = dep_variable->description->key;
      else
        dname = dep_variable->name;
      fprintf(file, "    printf(\"\\t\\t%s\\n\");\n", dname);
      dep_variable = dep_variable->next;
    }
    fprintf(file, "    printf(\"\\t}\\n\");\n");
  }

  /*
     Code to print variables whose default value is controled by this one
     */
  if (variable->default_ctl) {
    fprintf(file, "    printf(\"\\tChanges the default value of: {\\n\");\n");
    dep_variable = variable->default_ctl;
    while (dep_variable) {
      if (dep_variable->description->key)
        dname = dep_variable->description->key;
      else
        dname = dep_variable->name;
      fprintf(file, "    printf(\"\\t\\t%s\\n\");\n", dname);
      dep_variable = dep_variable->next;
    }
    fprintf(file, "    printf(\"\\t}\\n\");\n");
  }

  /*
     Code to print variables influenced by this one
     */
  if (variable->dependents) {
    fprintf(file, "    printf(\"\\tInfluences the value of: {\\n\");\n");
    dep_variable = variable->influenced;
    while (dep_variable) {
      if (dep_variable->description->key)
        dname = dep_variable->description->key;
      else
        dname = dep_variable->name;
      fprintf(file, "    printf(\"\\t\\t%s\\n\");\n", dname);
      dep_variable = dep_variable->next;
    }
    fprintf(file, "    printf(\"\\t}\\n\");\n");
  }

  /*
     Code to print variable long description
     */
  if (variable->description->l_desc) {
    line = 0;
    while (string = variable->description->l_desc[line++]) {
      fprintf(file, "    printf(\n");
      fprintf(file, "     \"%s\\n\");\n", escquotes(string));
    }
    fprintf(file, "    printf(\"\\n\");\n");
  }

  fprintf(file, "    printf(\"\\n\");\n");
  fprintf(file, "    if (!PrMall) return;\n");
  fprintf(file, "}\n");
}

/* short_desc_code()  *****************************************************/
/*
   Prints code for displaying a short description of a variable
   */

void short_desc_code(FILE* file, Variable* variable)
{
  char *       match, name[257], *type;
  extern char* Type_Names[];

  /*
     Code for scalars and array elements
     */

  if (variable->description->output || variable->description->auxiliary ||
      variable->description->hidden)
    return;

  if (variable->description->type->type == ANY)
    type = variable->description->type->data;
  else
    type = Type_Names[variable->description->type->type - TYPE_BASE];

  match = substitute_all(variable->format, "%d", "Int");
  strcpy(name, match);
  if (variable->description->storage != SCALAR) strcat(name, "[Int]");

  if (variable->array_level)
    fprintf(file, "    printf(\"\\t'-%s' %s\\n\");\n", name, type);
  else
    fprintf(file, "    printf(\"\\t -%s %s\\n\");\n", name, type);

  if (variable->description->storage == SCALAR) {
    if (match != variable->format) free(match);
    return;
  }

  /*
     Additional code for arrays
     */

  strcpy(name, match);
  if (match != variable->format) free(match);

  if (variable->array_level > 1)
    fprintf(file, "    printf(\"\\t'-%s' '{ %s x %s }'\\n\");\n", name,
            variable->description->type->next->data, type);
  else
    fprintf(file, "    printf(\"\\t -%s '{ %s x %s }'\\n\");\n", name,
            variable->description->type->next->data, type);
}

/* escquotes()    *****************************************************/
/*
   Returns a copy of the input string with all double quotes (") escaped
   by a \;
   the output string uses static storage and is overwritten by each call.
   */
char* escquotes(char* in)
{
  static char out[257];
  char *      p_in, *p_out;
  int         out_len;
  Boolean     escaped = FALSE;

  if (in == NULL) return (NULL);

  out_len = strlen(in);

  for (p_in = in, p_out = out; *p_in; p_in++, p_out++) {
    if (out_len > 256) return (NULL); /* Too long */
    switch (*p_in) {
      case '\\': escaped = TRUE; break;
      case '"':
        if (!escaped) {
          *p_out++ = '\\';
          out_len++;
        }
        escaped = FALSE;
        break;
      default: escaped = FALSE; break;
    }
    *p_out = *p_in;
  }

  *p_out = '\000';

  return (out);
}
