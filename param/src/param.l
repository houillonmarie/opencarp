 /*--------------------------------------------------------------------------*\

    param.l:  Lexical structure of param.

    Author:  Andre Bleau, eng.

    Laboratoire de Modelisation Biomedicale
    Institut de Genie Biomedical
    Ecole Polytechnique / Faculte de Medecine
    Universite de Montreal

    Revision:  November 12, 1998

 \*--------------------------------------------------------------------------*/

%a 2500
%e 2000
%k 1500
%p 4000

 /* Integer */
I  [-+]?[0-9]+
 /* Exponent for floats */
E  [Ee]{I}
 /* Float */
F  {I}"."[0-9]*({E})?|[-+]?[0-9]*"."[0-9]+({E})?
 /* Complex */
C  {F}[ \t]*[-+][ \t]*{F}[ \t]*[iI]
 /* Boolean */
B  FALSE|TRUE|\.FALSE\.|\.TRUE\.|VRAI|FAUX

BYTE    \$[Bb][Yy][Tt][Ee]
CHAR    \$[Cc][Hh][Aa][Rr]
COMPLEX    \$[Cc][Oo][Mm][Pp][Ll][Ee][Xx]
DOUBLE    \$[Dd][Oo][Uu][Bb][Ll][Ee]
D_COMPLEX  \$[Dd][Oo][Uu][Bb][Ll][Ee]_[Cc][Oo][Mm][Pp][Ll][Ee][Xx]
FLOAT    \$[Ff][Ll][Oo][Aa][Tt]|\$[Rr][Ee][Aa][Ll]
INT    \$[Ii][Nn][Tt]|\$[Ii][Nn][Tt][Ee][Gg][Ee][Rr]
LONG    \$[Ll][Oo][Nn][Gg]|\$[Ii][Nn][Tt][Ee][Gg][Ee][Rr]\*4
SHORT    \$[Ss][Hh][Oo][Rr][Tt]|\$[Ii][Nn][Tt][Ee][Gg][Ee][Rr]\*2
BOOLEAN    \$[Bb][Oo][Oo][Ll][Ee][Aa][Nn]|\$[Ll][Oo][Gg][Ii][Cc][Aa][Ll]
RDIR    \$[Rr][Dd][Ii][Rr]
RFILE    \$[Rr][Ff][Ii][Ll][Ee]
RWDIR    \$[Rr][Ww][Dd][Ii][Rr]
RWFILE    \$[Rr][Ww][Ff][Ii][Ll][Ee]
STRING    \$[Ss][Tt][Rr][Ii][Nn][Gg]
WDIR    \$[Ww][Dd][Ii][Rr]
WFILE    \$[Ww][Ff][Ii][Ll][Ee]
FLAG    \$[Ff][Ll][Aa][Gg]
VOID    \$[Vv][Oo][Ii][Dd]
ANY    \$[Aa][Nn][Yy]
EXPR    \$[Ee][Xx][Pp][Rr]

%{
#include <string.h>
#include "param.h"
#include "y.tab.h"
Text  get_text(void);
char  *getstr(void);
extern int last_pound_lineno, lineno;
%}

%%

\n        {
          lineno++;  /*  Count lines */
#if (SYSTEME!=LINUX)
          /*
          The next action is introduced only to
               add a reference to the yyfussy label
          */
          if (never()) goto yyfussy;
#endif
        }

\$[Ll]_[Dd][Ee][Ss][Cc][ \t]*=[ \t]*\{[ \t]*$    {
          yylval.text = get_text();
          return(L_DESC); }

\$[Gg][Ll]_[Dd][Ee][Ss][Cc][ \t]*=[ \t]*\{[ \t]*$  {
          yylval.text = get_text();
          return(GL_DESC); }

\$[Ii][Nn][Cc]_[Cc][Oo][Dd][Ee][ \t]*=[ \t]*\{[ \t]*$  {
          yylval.text = get_text();
          return(INC_CODE); }

\$[Tt][Ee][Xx][Tt][Bb][Oo][Xx][ \t]*=[ \t]*\{[ \t]*$  {
          yylval.text = get_text();
          return(TEXTBOX); }

\"      { yylval.str = getstr();
        return(STR); }
\'[^']+\'    { yylval.str = newstr(chartoint(yytext));
        return(INUM); }
\'\\\'\'    { yylval.str = newstr(chartoint(yytext));
        return(INUM); }

[ \t]      ;    /*  Eliminate spaces and tabs */
[^\n]#      skipcomment();  /*  Eliminate comment */
^#line      {
        last_pound_lineno = lineno;
        if (Fget_section()==DEFINITIONS)
            return(DEF_POUND);
        else
            return(DEC_POUND);
      }
^#      {
        last_pound_lineno = lineno;
        if (Fget_section()==DEFINITIONS)
            return(DEF_POUND);
        else
            return(DEC_POUND);
      }

\$[Mm][Ee][Mm][Bb][Ee][Rr]      return(MEMBER);
\$[Pp][Aa][Rr][Aa][Mm][Gg][Rr][Pp]    return(PARAMGRP);
\$[Pp][Aa][Rr][Aa][Mm][Ss][Gg][Rr][Pp]    return(PARAMSGRP);
\$[Ss][Tt][Rr][Uu][Cc][Tt][Uu][Rr][Ee]    return(STRUCTURE);
\$[Vv][Aa][Rr][Ii][Aa][Bb][Ll][Ee]    return(VARIABLE);
\$[Ll][Oo][Cc][Aa][Tt][Ii][Oo][Nn]    return(LOCATION);
\$[Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn]    return(FUNCTIOND);

{BYTE}        { yylval.ival = BYTE;      return(E_TYPE); }
{CHAR}        { yylval.ival = CHAR;      return(E_TYPE); }
{COMPLEX}     { yylval.ival = COMPLEX;   return(E_TYPE); }
{DOUBLE}      { yylval.ival = DOUBLE;    return(E_TYPE); }
{D_COMPLEX}   { yylval.ival = D_COMPLEX; return(E_TYPE); }
{FLOAT}       { yylval.ival = FLOAT;     return(E_TYPE); }
{INT}         { yylval.ival = INT;       return(E_TYPE); }
{LONG}        { yylval.ival = LONG;      return(E_TYPE); }
{SHORT}       { yylval.ival = SHORT;     return(E_TYPE); }
{BOOLEAN}     { yylval.ival = BOOLEAN;   return(E_TYPE); }
{RDIR}        { yylval.ival = RDIR;      return(E_TYPE); }
{RFILE}       { yylval.ival = RFILE;     return(E_TYPE); }
{RWDIR}       { yylval.ival = RWDIR;     return(E_TYPE); }
{RWFILE}      { yylval.ival = RWFILE;    return(E_TYPE); }
{STRING}      { yylval.ival = STRING;    return(E_TYPE); }
{WDIR}        { yylval.ival = WDIR;      return(E_TYPE); }
{WFILE}       { yylval.ival = WFILE;     return(E_TYPE); }
{FLAG}        { yylval.ival = FLAG;      return(E_TYPE); }
{VOID}        { yylval.ival = VOID;      return(E_TYPE); }
{ANY}         { yylval.ival = ANY;       return(E_TYPE); }
{EXPR}        { yylval.ival = EXPR;      return(E_TYPE); }

\$[Rr][Ee][Gg][Ii][Ss][Tt][Ee][Rr]    { yylval.ival = REGISTER;
              return(ALLOCATION_TYPE); }
\$[Aa][Uu][Tt][Oo]        { yylval.ival = AUTO;
              return(ALLOCATION_TYPE); }
\$[Ss][Tt][Aa][Tt][Ii][Cc]      { yylval.ival = STATIC;
              return(ALLOCATION_TYPE); }
\$[Ee][Xx][Tt][Ee][Rr][Nn]      { yylval.ival = EXTERN;
              return(ALLOCATION_TYPE); }
\$[Gg][Ll][Oo][Bb][Aa][Ll]      { yylval.ival = GLOBAL;
              return(ALLOCATION_TYPE); }

\$[Cc][Oo][Mm][Pp][Ii][Ll][Ee][Dd]    { yylval.ival = COMPILED;
              return(KIND); }

\$[Ll][Aa][Rr][Gg][Ee]        { yylval.ival = LARGE;
              return(VIEW_TYPE); }
\$[Ss][Mm][Aa][Ll][Ll]        { yylval.ival = SMALL;
              return(VIEW_TYPE); }

\$[Aa][Ll][Ll][Oo][Cc][Aa][Tt][Ii][Oo][Nn]  return(ALLOCATION);
\$[Aa][Uu][Xx][Ii][Ll][Ii][Aa][Rr][Yy]    return(AUXILIARY);
\$[Dd][Ee][Ff][Aa][Uu][Ll][Tt]      return(DEFAULT);
\$[Dd][Ii][Ss][Pp][Ll][Aa][Yy]      return(DISPLAY);
\$[Dd][Ii][Rr]          return(DIR);
\$[Ee][Xx][Tt]          return(EXT);
\$[Hh][Ii][Dd][Dd][Ee][Nn]      return(HIDDEN);
\$[Kk][Ee][Yy]          return(KEY);
\$[Ll][Ee][Nn][Gg][Tt][Hh]      return(LENGTH);
\$[Mm][Aa][Nn][Dd][Aa][Tt][Oo][Rr][Yy]    return(MANDATORY);
\$[Mm][Aa][Xx]          return(MAX);
\$[Mm][Ee][Nn][Uu]        return(MENU);
\$[Mm][Ii][Nn]          return(MIN);
\$[Oo][Uu][Tt][Pp][Uu][Tt]      return(OUTPUT);
\$[Ss]_[Dd][Ee][Ss][Cc]        return(S_DESC);
\$[Tt][Yy][Pp][Ee]        return(TYPE);
\$[Uu][Nn][Ii][Tt][Ss]        return(UNITS);
\$[Vv][Aa][Ll][Ii][Dd][Ff]      return(VALIDF);
\$[Vv][Ii][Ee][Ww]        return(VIEW);
\$[Vv][Oo][Ll][Aa][Tt][Ii][Ll]      return(VOLATIL);
\$[Vv][Ee][Cc][Tt][Oo][Rr]      return(VECTOR);
\$[Aa][Cc][Ee][Gg][Rr]_[Cc][Uu][Rr][Vv]    return(ACEGR_CURV);
\$[Aa][Cc][Ee][Gg][Rr]_[Hh][Ii][Ss][Tt]    return(ACEGR_HIST);

\$[Ee][Nn][Dd]_[Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn]  return(END_FUNCTION);
\$[Gg][Aa][Ll][Ll][Oo][Cc][Aa][Tt][Ii][Oo][Nn]  return(GALLOCATION);
\$[Gg][Ss]_[Dd][Ee][Ss][Cc]      return(GS_DESC);
\$[Gg][Vv][Aa][Ll][Ii][Dd][Ff]      return(GVALIDF);
\$[Ii][Cc][Oo][Nn]        return(ICON);
\$[Tt][Rr][Aa][Ii][Ll]_[Aa][Rr][Gg][Ss]    return(TRAIL_ARGS);

\(        return(LEFTPAREN);
\)        return(RIGHTPAREN);
\[        return(LEFTBRACKET);
\]        return(RIGHTBRACKET);
\{        return(LEFTBRACE);
\}        return(RIGHTBRACE);
=        return(EQUAL);
\,        return(COMA);
\.        return(POINT);
\-\>        return(ARROW);

\$[Ff][Aa][Tt][Hh][Ee][Rr]  return(FATHER);
\$[Ii][Nn][Dd][Ee][Xx]    return(INDEX);
\$[Cc][_][Ee][Xx][Pp]    return(C_EXP);

\$[a-zA-Z0-9_]*      { yylval.str = newstr(yytext);
          return(UNKNOWN_KEYWORD); }

{I}        { yylval.str = newstr(yytext); return(INUM); }
{F}        { yylval.str = newstr(yytext); return(FNUM); }
{C}        { yylval.str = newstr(yytext); return(CNUM); }
{B}        { yylval.str = newstr(yytext); return(BNUM); }

PrM[a-zA-Z_][a-zA-Z0-9_]*  { yylval.str = newstr(yytext);
          return(PRMID); }

[a-zA-Z_][a-zA-Z0-9_]*    { yylval.str = newstr(yytext);
          return(IDENTIFIER); }

%%

/* getstr()  *****************************************************/
char *getstr()
{
  extern int lineno;
  int i = 0;
  static char buf[1025], c;

  buf[i++] = '"';

  while (1) {
    c = yyinput();
    if (c=='"') {
      break;
    }
    else if (c=='\n') {
      fprintf(stderr, "\nNewline in string line %d, use \\n instead\n\n",
              lineno);
      exit(1);
    }
    else if (i==1025) {
      fprintf(stderr, "\nString too long, line %d\n\n", lineno);
      exit(1);
    }
    else {
      buf[i++] = c;
    }
  }

  buf[i++] = '"';
  buf[i++] = '\000';

  return(newstr(buf));
}

/* get_text()    *****************************************************/
Text get_text()
{
  extern int lineno;
  Text text;
  int allblank = 1, i = 0;
  static char buf[1025], c;

  text = newtext();

  while (yyinput() != '\n')
    lineno++;

  while (1) {
    c = yyinput();
    if (c=='}' && allblank) {
      break;
    }
    else if (c=='\n' || i==1025) {
      buf[i++] = '\000';
      text = addline(text, buf);
      lineno++;
      i = 0;
      allblank = 1;
    }
    else {
      if (c!=' ' && c!='\t') allblank = 0;
        buf[i++] = c;
    }
  }
  return(text);
}

/* skipcomment()  *****************************************************/
void skipcomment()
{
  char c;
  c = yyinput();
  while (c != '\n')
    c = yyinput();

  unput(c);
}

/* yywrap()    *****************************************************/
int yywrap()
{
return(1);
}
