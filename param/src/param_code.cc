// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

param_code: Print code of fortran interface (param_() function).

Author: Andre Bleau, eng.

Laboratoire de Modelisation Biomedicale
Institut de Genie Biomedical
Ecole Polytechnique / Faculte de Medecine
Universite de Montreal

Revision: October 2, 1998

\*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

/* param_code()   *****************************************************/

void param_code(FILE* file, char* func_name, Parameters* parameters)
{
  extern char* Type_Names[];
  int          loc_index, par_index, str_index, type, var_index;
  const char * length, *star, *tab1, *tab2;
  char *       type_name, *vname;
  Variable *   location, *variable;

  fprintf(file, "/* %s_()\t\t", func_name);
  fprintf(file, "*****************************************************/\n");
  fprintf(file, "/*\n");
  fprintf(file, "     Fortran interface\n");
  fprintf(file, "*/\n\n");

  /*
     Print param_ function header and parameter list
     */
  fprintf(file, "void\t%s_(\n\t", func_name);
  for (par_index = 0; par_index < parameters->n_parameters; par_index++) {
    fprintf(file, "PrMp%.3d, ", par_index);
    if (0 == (par_index + 1) % 8 || par_index == parameters->n_parameters - 1)
      fprintf(file, "\n\t");
  }
  fprintf(file, "PrMargc, PrMargm, PrMerror,\n\t");
  for (str_index = 0; str_index < parameters->n_strings; str_index++) {
    variable = parameters->string_vector[str_index];
    fprintf(file, "PrMl%.3d, ", variable->index);
    if (0 == (str_index + 1) % 8 || str_index == parameters->n_strings - 1) fprintf(file, "\n\t");
  }
  fprintf(file, "PrMargml)\n\n");
  if (parameters->n_variables) fprintf(file, "/* Variables */\n");
  for (par_index = 0, var_index = 0; var_index < parameters->n_variables;
       par_index++, var_index++) {
    variable = parameters->variable_vector[var_index];
    if (variable->description->key)
      vname = variable->description->key;
    else
      vname = variable->name;
    type = variable->description->type->type;
    if (variable->description->structure) {
      type_name = variable->description->type->data;
      length    = "";
      star      = "*";
      tab2      = "\t\t";
    } else {
      switch (type) {
        case RDIR:
        case RWDIR:
        case WDIR:
        case RFILE:
        case RWFILE:
        case WFILE:
        case STRING:
        case EXPR:
          type_name = (char*)"char";
          length    = "[STRLEN]";
          star      = " ";
          tab2      = "\t";
          break;
        default:
          type_name = Type_Names[variable->description->type->type - TYPE_BASE];
          length    = "";
          star      = "*";
          tab2      = "\t\t";
          break;
      }
    }
    if (strlen(type_name) > 7)
      tab1 = "\t";
    else
      tab1 = "\t\t";
    switch (variable->description->storage) {
      case STATIC_VECTOR:
        fprintf(file, "%s%s PrMp%.3d[%s]%s;%s/* %s */\n", type_name, tab1, par_index,
                variable->description->type->next->data, length, tab2, vname);
        break;
      case SCALAR:
        fprintf(file, "%s%s%sPrMp%.3d%s;\t%s/* &%s */\n", type_name, tab1, star, par_index, length,
                tab2, vname);
        break;
    }
  }
  if (parameters->n_locations) fprintf(file, "/* Locations */\n");
  for (loc_index = 0; loc_index < parameters->n_locations; par_index++, loc_index++) {
    location = parameters->location_vector[loc_index];
    type     = location->description->type->type;
    if (location->description->structure) {
      type_name = location->description->type->data;
      length    = "";
      star      = "*";
      tab2      = "\t\t";
    } else {
      switch (type) {
        case RDIR:
        case RWDIR:
        case WDIR:
        case RFILE:
        case RWFILE:
        case WFILE:
        case STRING:
        case EXPR:
          type_name = (char*)"char";
          length    = "[STRLEN]";
          star      = " ";
          tab2      = "\t";
          break;
        default:
          type_name = Type_Names[location->description->type->type - TYPE_BASE];
          length    = "";
          star      = "*";
          tab2      = "\t\t";
          break;
      }
    }
    if (strlen(type_name) > 7)
      tab1 = "\t";
    else
      tab1 = "\t\t";
    switch (location->description->storage) {
      case STATIC_VECTOR:
        fprintf(file, "%s%s PrMp%.3d[%s]%s;%s/* %s */\n", type_name, tab1, par_index,
                location->description->type->next->data, length, tab2, location->name);
        break;
      case SCALAR:
        fprintf(file, "%s%s%sPrMp%.3d%s;\t%s/* &%s */\n", type_name, tab1, star, par_index, length,
                tab2, location->name);
        break;
    }
  }
  fprintf(file, "int\t\t*PrMargc;\n");
  fprintf(file, "char\t\t PrMargm[NARGMAX][STRLEN];\n");
  fprintf(file, "int\t\t*PrMerror;\n");
  for (str_index = 0; str_index < parameters->n_strings; str_index++) {
    variable = parameters->string_vector[str_index];
    fprintf(file, "int\t\t PrMl%.3d;\n", variable->index);
  }
  fprintf(file, "int\t\t PrMargml;\n\n");
  fprintf(file, "{\n\n");

  /*
     Print param_ local variables declarations
     */
  fprintf(file, "\t    int\n");
  fprintf(file, "\tPrMarg,\n");
  fprintf(file, "\tPrMc,\n");
  fprintf(file, "\tPrMelem,\n");
  fprintf(file, "\tPrMlength;\n\n");
  fprintf(file, " char\n");
  for (str_index = 0; str_index < parameters->n_strings; str_index++) {
    variable = parameters->string_vector[str_index];
    switch (variable->description->storage) {
      case STATIC_VECTOR:
        fprintf(file, "\t*PrMs%.3d[%s],\n", variable->index,
                variable->description->type->next->data);
        break;
      case SCALAR: fprintf(file, "\t*PrMs%.3d,\n", variable->index); break;
    }
  }
  fprintf(file, "\t**PrMargv;\n\n");

  /*
     Print code to rebuild argument list so that it is suitable for param()
     */
  fprintf(file, "PrMargv = (char **)malloc(*PrMargc * sizeof(char *));\n");
  fprintf(file, "for (PrMarg=0; PrMarg<*PrMargc; PrMarg++) {\n");
  fprintf(file, "    for (PrMc=PrMargml-1; PrMc>=0&&PrMargm[PrMarg][PrMc]==' '; PrMc--) \n");
  fprintf(file, "\tPrMargm[PrMarg][PrMc] = '\\000';\n");
  fprintf(file, "    PrMargv[PrMarg] = &PrMargm[PrMarg][0];\n\n");
  fprintf(file, "}\n");

  /*
     Print code to call param()
     */
  fprintf(file, "*PrMerror = %s(\n\t", func_name);
  for (par_index = 0, var_index = 0; var_index < parameters->n_variables;
       par_index++, var_index++) {
    variable = parameters->variable_vector[var_index];
    type     = variable->description->type->type;
    switch (type) {
      case RDIR:
      case RWDIR:
      case WDIR:
      case RFILE:
      case RWFILE:
      case WFILE:
      case STRING:
      case EXPR:
        if (variable->description->storage == SCALAR)
          fprintf(file, "&PrMs%.3d, ", par_index);
        else
          fprintf(file, " PrMs%.3d, ", par_index);
        break;
      default: fprintf(file, " PrMp%.3d, ", par_index); break;
    }
    if (0 == (par_index + 1) % 7 || par_index == parameters->n_parameters - 1)
      fprintf(file, "\n\t");
  }
  for (loc_index = 0; loc_index < parameters->n_locations; loc_index++, loc_index++) {
    location = parameters->location_vector[loc_index];
    type     = location->description->type->type;
    switch (type) {
      case RDIR:
      case RWDIR:
      case WDIR:
      case RFILE:
      case RWFILE:
      case WFILE:
      case STRING:
      case EXPR: fprintf(file, "&PrMs%.3d, ", par_index); break;
      default: fprintf(file, " PrMp%.3d, ", par_index); break;
    }
    if (0 == (par_index + 1) % 7 || par_index == parameters->n_parameters - 1)
      fprintf(file, "\n\t");
  }
  fprintf(file, " PrMargc,  PrMargv);\n\n");
  fprintf(file, "free(PrMargv);\n\n");
  fprintf(file, "if (*PrMerror==PrMFATAL || *PrMerror==PrMHELP) return;\n\n");

  /*
     Print code to copy dynamic strings into static ones
     */
  for (str_index = 0; str_index < parameters->n_strings; str_index++) {
    variable = parameters->string_vector[str_index];
    if (variable->description->key)
      vname = variable->description->key;
    else
      vname = variable->name;
    switch (variable->description->storage) {
      case STATIC_VECTOR:
        fprintf(file, "for (PrMelem=0; PrMelem<%s; PrMelem++) {\n",
                variable->description->type->next->data);
        fprintf(file, "    PrMlength = strlen(PrMs%.3d[PrMelem]);\n", variable->index);
        fprintf(file, "    if (PrMlength>PrMl%.3d) {\n", variable->index);
        fprintf(file, "\tfprintf(stderr,\n");
        fprintf(file, "\t \"%s[%%d] longer than %%d characters; truncated\\n\",\n", vname);
        fprintf(file, "\t PrMelem, STRLEN);\n");
        fprintf(file, "\tPrMlength = PrMl%.3d;\n", variable->index);
        fprintf(file, "    }\n");
        fprintf(file, "    for (PrMc=0; PrMc<PrMlength; PrMc++)\n");
        fprintf(file, "\tPrMp%.3d[PrMelem][PrMc] = PrMs%.3d[PrMelem][PrMc];\n", variable->index,
                variable->index);
        fprintf(file, "    for (PrMc=PrMlength; PrMc<PrMl%.3d; PrMc++)\n", variable->index);
        fprintf(file, "\tPrMp%.3d[PrMelem][PrMc] = ' ';\n", variable->index);
        fprintf(file, "}\n");
        break;
      case SCALAR:
        fprintf(file, "PrMlength = strlen(PrMs%.3d);\n", variable->index);
        fprintf(file, "if (PrMlength>PrMl%.3d) {\n", variable->index);
        fprintf(file, "    fprintf(stderr,\n");
        fprintf(file, "     \"%s longer than %%d characters; truncated\\n\",\n", vname);
        fprintf(file, "     STRLEN);\n");
        fprintf(file, "    PrMlength = PrMl%.3d;\n", variable->index);
        fprintf(file, "}\n");
        fprintf(file, "for (PrMc=0; PrMc<PrMlength; PrMc++)\n");
        fprintf(file, "    PrMp%.3d[PrMc] = PrMs%.3d[PrMc];\n", variable->index, variable->index);
        fprintf(file, "for (PrMc=PrMlength; PrMc<PrMl%.3d; PrMc++)\n", variable->index);
        fprintf(file, "    PrMp%.3d[PrMc] = ' ';\n", variable->index);
        break;
    }
  }
  fprintf(file, "\n");

  /*
     Print code to rebuild remaining argument list
     so that it is suitable for fortran and return to main.
     */
  fprintf(file, "for (PrMarg=0; PrMarg<*PrMargc; PrMarg++) {\n");
  fprintf(file, "    for (PrMc=strlen(PrMargv[PrMarg]); PrMc>PrMargml; PrMc--)\n");
  fprintf(file, "\tPrMargv[PrMarg][PrMc] = ' ';\n");
  fprintf(file, "}\n\n");
  fprintf(file, "return;\n\n");
  fprintf(file, "}\n\n");
}
