// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

    display.c:  Display parameter groups, variables and location after
    the parsing is done.

    Author:  Andre Bleau, eng.

    Laboratoire de Modelisation Biomedicale
    Institut de Genie Biomedical
    Ecole Polytechnique / Faculte de Medecine
    Universite de Montreal

    Revision:  June 17, 1997

\*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

void Dtext(Text text, int indent);
void Dfunction(Function*);
void Dstructure(Structure*);
void Ddescription(Description*);
void Dvalue(Value*);
void Dparamgrp(Paramgrp*);

/* display()    *****************************************************/
void display(Parameters* parameters)
{
  Structure*   structure;
  Paramgrp*    paramgrp;
  Function*    function;
  char*        type_name;
  extern char* Type_Names[];

  switch (parameters->gallocation) {
    case 0:
    case AUTO: printf("\n*** GAllocation: Auto\n"); break;
    case STATIC: printf("\n*** GAllocation: Static\n"); break;
    case EXTERN: printf("\n*** GAllocation: Extern\n"); break;
    case GLOBAL: printf("\n*** GAllocation: Global\n"); break;
  }
  if (parameters->gs_desc)
    printf("\n*** GS_desc:\n%s\n", parameters->gs_desc);
  else
    printf("\n*** GS_desc: NONE\n");
  if (parameters->gl_desc) {
    printf("\n*** GL_desc:\n");
    Dtext(parameters->gl_desc, 0);
  } else
    printf("\n*** GL_desc: NONE\n");
  if (parameters->gvalidf) {
    printf("\n*** GValidf:\n");
    Dfunction(parameters->gvalidf);
  } else
    printf("\n*** GValidf: NONE\n");
  if (parameters->icon)
    printf("\n*** Icon: %s\n", parameters->icon);
  else
    printf("\n*** Icon: NONE\n");
  if (parameters->function_list) {
    printf("\n*** Functions:\n");
    function = parameters->function_list;
    while (function) {
      if (function->type->data)
        type_name = function->type->data;
      else
        type_name = Type_Names[function->type->type - TYPE_BASE];
      printf("\t%s (%s)\n", function->name, type_name);
      function = function->next;
    }
  }
  printf("\n");
  structure = parameters->structure_list;
  while (structure) {
    Dstructure(structure);
    structure = structure->next;
  }
  paramgrp = parameters->paramgrp_list;
  while (paramgrp) {
    Dparamgrp(paramgrp);
    paramgrp = paramgrp->next;
  }
}

/* Dtext()    *****************************************************/

void Dtext(Text text, int indent)
{
  int i, l = 0;

  while (text[l]) {
    i = 0;
    while (indent > i++) printf("\t");
    printf("%s\n", text[l]);
    l++;
  }
}

/* Dfunction()    *****************************************************/

void Dfunction(Function* function)
{
  Arg* arg;

  printf("Name: %s Arg_list: {\n", function->name);
  arg = function->arg_list;
  while (arg) {
    switch (arg->type) {
      case MEMBER: printf("MEMBER\t%s\n", arg->str); break;
      case IDENTIFIER: printf("IDENTIFIER\t%s\n", arg->str); break;
      case STR: printf("STR\t%s\n", arg->str); break;
      case BNUM: printf("BNUM\t%s\n", arg->str); break;
      case INUM: printf("INUM\t%s\n", arg->str); break;
      case FNUM: printf("FNUM\t%s\n", arg->str); break;
      case CNUM: printf("CNUM\t%s\n", arg->str); break;
      case C_EXP: printf("C_EXP\t%s\n", arg->str); break;
      default: printf("UNKNOWN\t%s\n", arg->str); break;
    }
    arg = arg->next;
  }
  printf("}\n");
}

/* Dstructure()    *****************************************************/

void Dstructure(Structure* structure)
{
  Member* member;

  printf("Structure %s; number of members: %d Members: {\n", structure->name, structure->n_members);
  member = structure->member_list;
  while (member) {
    printf(" %s is {\n", member->name);
    Ddescription(member->description);
    printf(" }\n");
    member = member->next;
  }
  printf("}\n");
}

/* Ddescription()  *****************************************************/

void Ddescription(Description* description)
{
  Value* value;

  if (description->type->next) {
    if (description->type->data)
      printf("\tVector of type %s\n", description->type->data);
    else {
      printf("\tVector of type ");
      Dvalue(description->type);
      printf("\n");
    }
    printf("\tNumber of elements: ");
    Dvalue(description->type->next);
    printf("\n");
  } else {
    if (description->type->data)
      printf("\tType %s\n", description->type->data);
    else {
      printf("\tType ");
      Dvalue(description->type);
      printf("\n");
    }
  }
  switch (description->allocation) {
    case 0: break;
    case AUTO: printf("\tAllocation: Auto\n"); break;
    case STATIC: printf("\tAllocation: Static\n"); break;
    case EXTERN: printf("\tAllocation: Extern\n"); break;
    case GLOBAL: printf("\tAllocation: Global\n"); break;
  }
  value = description->default_value;
  if (value) {
    if (value->next) {
      printf("\tDefault vector : {\n");
      while (value) {
        printf("\t\t");
        Dvalue(value);
        value = value->next;
        printf("\n");
      }
      printf("\t}\n");
    } else {
      printf("\tDefault :\t");
      Dvalue(value);
      printf("\n");
    }
  }
  if (description->dir) {
    printf("\tDir :\t");
    Dvalue(description->dir);
    printf("\n");
  }
  if (description->display) {
    printf("\tDisplay :\t");
    Dfunction(description->display);
    printf("\n");
  }
  if (description->ext) {
    printf("\tExt :\t");
    Dvalue(description->ext);
    printf("\n");
  }
  if (description->key) { printf("\tKey :%s\n", description->key); }
  if (description->l_desc) {
    printf("\tL_desc {\n");
    Dtext(description->l_desc, 0);
    printf("\t}\n");
  }
  if (description->mandatory) { printf("\tMandatory\n"); }
  if (description->max) {
    printf("\tMax :\t");
    Dvalue(description->max);
    printf("\n");
  }
  if (description->menu) {
    printf("\tMenu : {\n");
    value = description->menu;
    while (value) {
      printf("\t\t");
      Dvalue(value);
      value = value->next;
      printf("\n");
    }
    printf("\t}\n");
  }
  if (description->min) {
    printf("\tMin :\t");
    Dvalue(description->min);
    printf("\n");
  }
  if (description->s_desc) { printf("\tS_desc :%s\n", description->s_desc); }
  if (description->units) {
    printf("\tUnits :\t");
    Dvalue(description->units);
    printf("\n");
  }
  if (description->validf) {
    printf("\tValidf\n");
    Dfunction(description->validf);
  }
  switch (description->view) {
    case 0: break;
    case LARGE: printf("\tView: Large\n"); break;
    case SMALL: printf("\tView: Small\n"); break;
  }
  if (description->dependences) {
    if (description->dependences[0]) {
      printf("\tDependences {\n");
      Dtext(description->dependences, 2);
      printf("\t}\n");
    }
  }
}

/* Dvalue()    *****************************************************/

void Dvalue(Value* value)
{
  switch (value->type) {
    case FUNCTION: printf("FUNCTION\t%s", value->data); break;
    case LOCATION: printf("LOCATION\t%s", value->data); break;
    case IDENTIFIER: printf("IDENTIFIER\t%s", value->data); break;
    case STR: printf("STR\t%s", value->data); break;
    case BNUM: printf("BNUM\t%s", value->data); break;
    case INUM: printf("INUM\t%s", value->data); break;
    case FNUM: printf("FNUM\t%s", value->data); break;
    case CNUM: printf("CNUM\t%s", value->data); break;
    case C_EXP: printf("C_EXP\t%s", value->data); break;
    case BYTE: printf("byte"); break;
    case CHAR: printf("char"); break;
    case COMPLEX: printf("complex"); break;
    case DOUBLE: printf("double"); break;
    case D_COMPLEX: printf("double_complex"); break;
    case FLOAT: printf("float"); break;
    case INT: printf("int"); break;
    case LONG: printf("long"); break;
    case SHORT: printf("short"); break;
    case BOOLEAN: printf("Boolean"); break;
    case RDIR: printf("RDir"); break;
    case RFILE: printf("RFile"); break;
    case RWDIR: printf("RWDir"); break;
    case RWFILE: printf("RWFile"); break;
    case STRING: printf("String"); break;
    case WDIR: printf("WDir"); break;
    case WFILE: printf("WFile"); break;
    case FLAG: printf("Flag"); break;
    case VOID: printf("void"); break;
    case ANY: printf("Any\t%s", value->data); break;
    case EXPR: printf("Expr"); break;
    default: printf("UNKNOWN\t%s", value->data); break;
  }
  if (value->label) printf("\t%s", value->label);
  if (value->sensitive) printf("\t%s", value->sensitive);
}

/* Dparamgrp()    *****************************************************/

void Dparamgrp(Paramgrp* paramgrp)
{
  Variable *location, *variable;
  Paramgrp* paramsgrp;

  printf("ParamGrp %s number of items: %d\n", paramgrp->name, paramgrp->n_items);
  if (paramgrp->l_desc) {
    printf(" L_desc {\n");
    Dtext(paramgrp->l_desc, 0);
    printf(" }\n");
  }
  if (paramgrp->s_desc) { printf(" S_desc :%s\n", paramgrp->s_desc); }
  if (paramgrp->validf) {
    printf(" Validf\n");
    Dfunction(paramgrp->validf);
  }
  location = paramgrp->location_list;
  while (location) {
    printf(" Location %s is {\n", location->name);
    Ddescription(location->description);
    printf(" }\n");
    location = location->next_in_pgrp;
  }
  variable = paramgrp->variable_list;
  while (variable) {
    printf(" Variable %s is {\n", variable->name);
    Ddescription(variable->description);
    printf(" }\n");
    variable = variable->next_in_pgrp;
  }
  paramsgrp = paramgrp->paramsgrp_list;
  if (paramsgrp) {
    printf(" ParamSGrp {\n");
    while (paramsgrp) {
      printf("\t%s\n", paramsgrp->name);
      paramsgrp = paramsgrp->next_in_sgrp;
    }
    printf(" }\n");
  }
}
