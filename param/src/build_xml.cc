// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------
/*---------------------------------------------------------------------------*\

usage_code:  Contains usage_code() and short_desc_code() functions.

Author:  Andre Bleau, eng.

Laboratoire de Modelisation Biomedicale
Institut de Genie Biomedical
Ecole Polytechnique / Faculte de Medecine
Universite de Montreal

Revision:  April 25, 1996
Modified:  December 1, 2019

\*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

/**
 * @brief Prints code for displaying help messages about a variable
 *
 * @param file
 * @param variable
 */
void build_xml(FILE* file, Variable* variable)
{
  int          a_level, bound_level, index, level, line;
  char *       dname, format[257], *match, name[257], scan_format[257], *string, *vname;
  Value *      bound, *value;
  Variable*    dep_variable;
  extern char* Type_Names[];

  if (variable->description->output || variable->description->auxiliary ||
      variable->description->hidden)
    return;

  index   = variable->description->type->type - TYPE_BASE;
  a_level = variable->array_level;
  strcpy(format, variable->format);

  if (variable->description->key)
    vname = variable->description->key;
  else
    vname = variable->name;

  if (variable->description->storage != SCALAR) {
    strcat(format, "[%d]");
    sprintf(name, "%s[PrMelem%d]", vname, a_level);
  } else
    strcpy(name, vname);

  match = substitute_all(format, "%d", "%*d");
  strcpy(scan_format, match);
  if (match != format) free(match);
  strcat(scan_format, "%[ ]");
  match = scan_format;

  // Code for scalar or array element topics
  if (a_level) {
    fprintf(file, "\t\t\t<struct-ref ");
  } else {
    fprintf(file, "\t\t<program name= \"%s\">\n",
            vname);
    fprintf(file, "\t\t\t<param ");
  }

  if (a_level) {
    // Code to print variable name (with indexes if there are some)
    fprintf(file, " father-name= \"%s\" ", variable->name);
  }

  else {
    // Code to print variable name (with indexes if there are some)
    fprintf(file, " name= \"%s\" ", format);
  }

  // Code to print variable type
  if (variable->description->type->type == ANY) {
    fprintf(file, " type=");
    fprintf(file, " \"%s\" ", variable->description->type->data);
  } else {
    if (Type_Names[index]) {
      fprintf(file, " type=");
      fprintf(file, " \"%s\" ", Type_Names[index]);
    } else {
      fprintf(file, " name=");
      fprintf(file, "\"%s\"", variable->description->type->data);
    }
  }

  // Code to print variable default value
  if (variable->description->default_value) {
    if (variable->description->default_value->next) {
      fprintf(file, " default=");
      fprintf(file, " \"default_value[PrMelem%d]\"", a_level);
    } else {
      fprintf(file, " default=");
      fprintf(file, " \"%s\" ", escquotes(variable->description->default_value->data));
    }
  }

  // Code to print variable units
  if (variable->description->units) {
    if (variable->description->units->type == STR) {
      fprintf(file, " units=");
      fprintf(file, " %s ", variable->description->units->data);
    } else {
      fprintf(file, " units=");
      fprintf(file, " %s ", variable->description->units->data);
    }
  }

  // Code to print variable extension (FILES)
  if (variable->description->ext) {
    if (variable->description->ext->type == STR) {
      fprintf(file, " extension=");
      fprintf(file, "%s ", variable->description->ext->data);
    } else {
      fprintf(file, " extension=");
      fprintf(file, "%s ", variable->description->ext->data);
    }
  }

  fprintf(file, ">\n");

  // Code to print variable short description
  if (variable->description->s_desc) {
    fprintf(file, "\t\t\t\t\t<description> ");
    fprintf(file, " %s ", variable->description->s_desc);
    fprintf(file, " </description>\n");
  }

  // Code to print variable maximum or minimum value
  if ((variable->description->min) != NULL || (variable->description->max) != NULL) {
    fprintf(file, "\t\t\t\t\t<validation type=\"range\">\n");
    // Code to print variable maximum value
    if (variable->description->max) {
      fprintf(file, "\t\t\t\t\t\t<max>");
      fprintf(file, " \"%s\" ", escquotes(variable->description->max->data));
      fprintf(file, " </max>\n");
    }
    // Code to print variable minimum value
    if (variable->description->min) {
      fprintf(file, "\t\t\t\t\t\t<min>");
      fprintf(file, " \"%s\" ", escquotes(variable->description->min->data));
      fprintf(file, " </min>\n");
    }
    fprintf(file, "\t\t\t\t\t</validation>\n");
  }

  // Code to print variable menu of allowed values
  if (variable->description->menu) {
    fprintf(file, "\t\t\t\t<validation type=\"options\">\n");
    value = variable->description->menu;
    while (value) {
      if (value->label) {
        fprintf(file, "\t\t\t\t\t\t<option");
        fprintf(file, " value=\"%s\" name=%s/>\n", escquotes(value->data), value->label);
        // fprintf(file, "    </enumerate>\n");
      } else {
        fprintf(file, "\t\t\t\t\t\t<option>");
        fprintf(file, "printf(\"\\t\\t%s\\n\");", escquotes(value->data));
        fprintf(file, "\t\t\t\t\t\t</option>\n");
      }
      value = value->next;
    }
    fprintf(file, "\t\t\t\t\t</validation>\n");
  }

  // Code to print variable dependents
  if (variable->dependents) {
    // fprintf(file, "    <dependents>\n");
    dep_variable = variable->dependents;

    while (dep_variable) {
      if (dep_variable->description->key) {
        dname = dep_variable->description->key;
      } else {
        fprintf(file, "\t\t\t\t\t<relation to-name=");
        dname = dep_variable->name;
        fprintf(file, " \"%s\" ", dname);
        fprintf(file, " />\n");
      }
      dep_variable = dep_variable->next;
    }
  }

  if (a_level) {
  } else {
    fprintf(file, "\t\t\t</param>\n");
  }

  // Code for scalar or array element topics
  if (a_level) {
    fprintf(file, "\t\t\t</struct-ref>\n");
  } else {
    fprintf(file, "\t\t</program>\n",
            vname);
  }

  fprintf(file, "\n");

  if (variable->description->storage == SCALAR) return;
}
