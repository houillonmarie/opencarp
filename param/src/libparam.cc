// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/*---------------------------------------------------------------------------*\

  libparam.c:  C functions called by param's parser and utility functions.

Author:  Andre Bleau, eng.

Laboratoire de Modelisation Biomedicale
Institut de Genie Biomedical
Ecole Polytechnique / Faculte de Medecine
Universite de Montreal

Revision:  April 3, 2000

\*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "param.h"
#include "y.tab.h"

Parameters  parameters;
State       state;
int         c_dialect, interface, language, last_pound_lineno, lineno;
Boolean     parser_error;
char*       pdffile                   = NULL;
const char* Type_Names[]              = {"Byte", "Char", "S_Complex", "Double", "D_Complex", "Float",
                            "Int", "Long", "Short", "BooleaN", "RDir", "RFile",
                            "RWDir", "RWFile", "String", "WDir", "WFile", "Flag",
                            "void", "$any", "String"};
const char* Type_Fortran_Names[]      = {"",
                                    "byte",
                                    "complex",
                                    "double precision",
                                    "complex*16",
                                    "real",
                                    "integer",
                                    "integer*4",
                                    "integer*2",
                                    "logical",
                                    "character*(*)",
                                    "character*(*)",
                                    "character*(*)",
                                    "character*(*)",
                                    "character*(*)",
                                    "character*(*)",
                                    "character*(*)",
                                    "logical",
                                    "",
                                    "$any",
                                    "character*(*)"};
const char* Type_Fortran_Main_Names[] = {"",
                                         "byte",
                                         "complex",
                                         "double precision",
                                         "complex*16",
                                         "real",
                                         "integer",
                                         "integer*4",
                                         "integer*2",
                                         "logical",
                                         "character*STRLEN",
                                         "character*STRLEN",
                                         "character*STRLEN",
                                         "character*STRLEN",
                                         "character*STRLEN",
                                         "character*STRLEN",
                                         "character*STRLEN",
                                         "logical",
                                         "",
                                         "$any",
                                         "character*STRLEN"};
const char* ACEgr_Type_Names[]        = {"BYTE", "CHAR", "", "DOUBLE", "", "FLOAT", "LONG",
                                  "LONG", "SHORT", "", "", "", "", "",
                                  "", "", "", "", "", "$any", ""};
const char* Type_Formats[]            = {
    "\"%d\"",              /* Byte */
    "\"%d\"",              /* Char */
    "\"%g + %g %[iI]\"",   /* S_Complex */
    "\"%lg\"",             /* Double */
    "\"%lg + %lg %[iI]\"", /* D_Complex */
    "\"%g\"",              /* Float */
    "\"%d\"",              /* Int */
    "\"%ld\"",             /* Long */
    "\"%hd\"",             /* Short */
    "\"%s\"",              /* Boolean */
    "\"%s\"",              /* RDir */
    "\"%s\"",              /* RFile */
    "\"%s\"",              /* RWDir */
    "\"%s\"",              /* RWFile */
    "\"%s\"",              /* String */
    "\"%s\"",              /* WDir */
    "\"%s\"",              /* WFile */
    "\"%s\"",              /* Flag */
    "",                    /* void */
    "$any",                /* any (unusable) */
    "\"%s\"",              /* Expr */
};
const char* Allocation_Names[] = {
    "", "", "static", "extern", "extern" /* Global */
};

/* yyerror()    ******************************************************/

int yyerror(const char* s)
{
  int line, word;

  fprintf(stderr, "%s line %d in %s\n", s, lineno, pdffile);
  switch (Fget_section()) {
    case DEFINITIONS:
      fprintf(stderr, "in DEFINITIONS section\n");
      switch (Fget_state() & IN_MASK) {
        case IN_FUNCTIOND:
          if (state.id_functiond[0]) {
            if (state.id_functiond[1]) {
              fprintf(stderr, "    in definition of functions:\n");
              for (line = 0; state.id_functiond[line]; line++) {
                fprintf(stderr, "\t%s\n", state.id_functiond[line]);
              }
            } else {
              fprintf(stderr, "    in definition of function %s\n", state.id_functiond[0]);
            }
          } else {
            word = Fget_word();
            if (word != 0) fprintf(stderr, "    after keyword %s\n", Fword_to_string(word));
          }
          break;
        case IN_STRUCTURE:
          if (state.id_structure) {
            fprintf(stderr, "    in definition of structure %s\n", state.id_structure);
            if ((Fget_last_state() & IN_MASK) == IN_MEMBER && state.id_member) {
              fprintf(stderr, "\tafter definition of member %s\n", state.id_member);
            } else {
              word = Fget_word();
              if (word != 0) fprintf(stderr, "\tafter keyword %s\n", Fword_to_string(word));
            }
          } else {
            word = Fget_word();
            if (word != 0) fprintf(stderr, "    after keyword %s\n", Fword_to_string(word));
          }
          break;
        case IN_MEMBER:
          if (state.id_structure) {
            fprintf(stderr, "    in definition of structure %s\n", state.id_structure);
          }
          if (state.id_member) {
            fprintf(stderr, "\tin definition of member %s\n", state.id_member);
          } else {
            word = Fget_word();
            if (word != 0) fprintf(stderr, "\tafter keyword %s\n", Fword_to_string(word));
          }
          if ((Fget_state() & IN_DESCRIPTOR) && state.id_descriptor) {
            fprintf(stderr, "\t    in descriptor %s\n", state.id_descriptor);
          } else if ((Fget_last_state() & IN_DESCRIPTOR) && state.id_descriptor) {
            fprintf(stderr, "\t    after descriptor %s\n", state.id_descriptor);
          }
          break;
        default:
          word = Fget_word();
          if (word != 0) fprintf(stderr, "    after keyword %s\n", Fword_to_string(word));
          break;
      }      /* switch(Fget_state()&IN_MASK) */
      break; /* case DEFINITIONS: */
    case DECLARATIONS:
      fprintf(stderr, "in DECLARATIONS section\n");
      if (state.id_paramgrp) {
        fprintf(stderr, "    in definition of parameter group %s\n", state.id_paramgrp);
      }
      switch (Fget_state() & IN_MASK) {
        case IN_LOCATION:
          if (state.id_location[0]) {
            if (state.id_location[1]) {
              fprintf(stderr, "\tin definition of locations:\n");
              for (line = 0; state.id_location[line]; line++) {
                fprintf(stderr, "\t    %s\n", state.id_location[line]);
              }
            } else {
              fprintf(stderr, "\tin definition of location %s\n", state.id_location[0]);
            }
          } else {
            word = Fget_word();
            if (word != 0) fprintf(stderr, "\tafter keyword %s\n", Fword_to_string(word));
          }
          if ((Fget_state() & IN_DESCRIPTOR) && state.id_descriptor) {
            fprintf(stderr, "\t    in descriptor %s\n", state.id_descriptor);
          } else if ((Fget_last_state() & IN_DESCRIPTOR) && state.id_descriptor) {
            fprintf(stderr, "\t    after descriptor %s\n", state.id_descriptor);
          }
          break;
        case IN_PARAMGRP:
          if (state.id_paramgrp) {
            if ((Fget_last_state() & IN_MASK) == IN_LOCATION && state.id_location[0]) {
              if (state.id_location[1]) {
                fprintf(stderr, "\tafter definition of locations:\n");
                for (line = 0; state.id_location[line]; line++) {
                  fprintf(stderr, "\t    %s\n", state.id_location[line]);
                }
              } else {
                fprintf(stderr, "\tafter definition of location %s\n", state.id_location[0]);
              }
            } else if ((Fget_last_state() & IN_MASK) == IN_VARIABLE && state.id_variable[0]) {
              if (state.id_variable[1]) {
                fprintf(stderr, "\tafter definition of variables:\n");
                for (line = 0; state.id_variable[line]; line++) {
                  fprintf(stderr, "\t    %s\n", state.id_variable[line]);
                }
              } else {
                fprintf(stderr, "\tafter definition of variable %s\n", state.id_variable[0]);
              }
            } else {
              word = Fget_word();
              if (word != 0) fprintf(stderr, "\tafter keyword %s\n", Fword_to_string(word));
            }
          } else {
            word = Fget_word();
            if (word != 0) fprintf(stderr, "    after keyword %s\n", Fword_to_string(word));
          }
          break;
        case IN_VARIABLE:
          if (state.id_variable[0]) {
            if (state.id_variable[1]) {
              fprintf(stderr, "\tin definition of variables:\n");
              for (line = 0; state.id_variable[line]; line++) {
                fprintf(stderr, "\t    %s\n", state.id_variable[line]);
              }
            } else {
              fprintf(stderr, "\tin definition of variable %s\n", state.id_variable[0]);
            }
          } else {
            word = Fget_word();
            if (word != 0) fprintf(stderr, "\tafter keyword %s\n", Fword_to_string(word));
          }
          if ((Fget_state() & IN_DESCRIPTOR) && state.id_descriptor) {
            fprintf(stderr, "\t    in descriptor %s\n", state.id_descriptor);
          } else if ((Fget_last_state() & IN_DESCRIPTOR) && state.id_descriptor) {
            fprintf(stderr, "\t    after descriptor %s\n", state.id_descriptor);
          }
          break;
        default:
          word = Fget_word();
          if (word != 0) fprintf(stderr, "    after keyword %s\n", Fword_to_string(word));
          break;
      }      /* switch(Fget_state()&IN_MASK) */
      break; /* case DECLARATIONS: */
  }          /* switch(Fget_section()) */

  if ((Fget_state() & IN_FUNCTION) && state.id_function) {
    fprintf(stderr, "\t\tin function call to %s\n", state.id_function);
  } else if ((Fget_last_state() & IN_FUNCTION) && state.id_function) {
    fprintf(stderr, "\t\tafter function call to %s\n", state.id_function);
  }
  if ((Fget_state() & IN_ARGUMENT) && state.id_function) {
    fprintf(stderr, "\t\t    after argument %s\n", state.id_argument);
  }

  parser_error = TRUE;

  return (0);
}

/* initialize()    ******************************************************/

void initialize(Parameters* parameters)
{
  Text        predefined_functions;
  Descriptor* predefined_type;

  parameters->gallocation     = 0;
  parameters->n_arrays        = 0;
  parameters->n_auxiliaries   = 0;
  parameters->n_compiled      = 0;
  parameters->n_dirs          = 0;
  parameters->n_files         = 0;
  parameters->n_leafs         = 0;
  parameters->n_locations     = 0;
  parameters->n_mandatories   = 0;
  parameters->n_nodes         = 0;
  parameters->n_outputs       = 0;
  parameters->n_short_ints    = 0;
  parameters->n_strings       = 0;
  parameters->n_structures    = 0;
  parameters->n_struct_arrays = 0;
  parameters->n_textboxes     = 0;
  parameters->n_variables     = 0;
  parameters->n_volatils      = 0;
  parameters->acegr           = FALSE;
  parameters->error           = FALSE;
  parameters->run_on_comp     = FALSE;
  parameters->trail_args      = FALSE;
  parameters->end_function    = NULL;
  parameters->gl_desc         = NULL;
  parameters->gs_desc         = NULL;
  parameters->gvalidf         = NULL;
  parameters->icon            = NULL;
  parameters->inc_code        = NULL;
  /*  Initialize the defined function list */
  parameters->function_list = NULL;
  predefined_functions      = Ftext(NULL, "$ACEGR_CURV");
  predefined_functions      = Ftext(predefined_functions, "$ACEGR_HIST");
  predefined_type           = Ftype(Ftype_value(VOID));
  Ffunc_declare(parameters, predefined_functions, predefined_type);
  /*  Initialize the defined structure list */
  parameters->structure_list        = NULL;
  parameters->main_paramgrp         = (Paramgrp*)calloc(1, sizeof(Paramgrp));
  parameters->main_paramgrp->name   = (char*)"Main";
  parameters->main_paramgrp->father = parameters->main_paramgrp;
  parameters->paramgrp_list         = parameters->main_paramgrp;
  lineno                            = 1;
  parser_error                      = FALSE;

  Finit_state();
  Fset_state(IN_UNKNOWN);
  Fset_section(DEFINITIONS);
  Fset_word(0);
}

/* Fget_section()    **********************************************/

int Fget_section() { return (state.section); }

/* Fset_section()    **********************************************/

int Fset_section(int new_section)
{
  int old_section;

  old_section = state.section;
  switch (new_section) {
    case DEFINITIONS:
    case DECLARATIONS: state.section = new_section; break;
    default: fprintf(stderr, "\n*** Unknown section: %d\n\n", new_section); exit(1);
  }
  return (old_section);
}

/* Fget_state()      **********************************************/

int Fget_state() { return (state.state); }

/* Fget_last_state()    **********************************************/

int Fget_last_state() { return (state.last_state); }

/* Fadd_state()      **********************************************/

int Fadd_state(int new_state)
{
  state.last_state = state.state;
  state.state |= new_state;
  return (state.last_state);
}

/* Finit_state()    **********************************************/

void Finit_state()

{
  state.last_state    = IN_UNKNOWN;
  state.state         = IN_UNKNOWN;
  state.section       = IN_UNKNOWN;
  state.word          = IN_UNKNOWN;
  state.id_argument   = NULL;
  state.id_descriptor = NULL;
  state.id_function   = NULL;
  state.id_member     = NULL;
  state.id_paramgrp   = NULL;
  state.id_structure  = NULL;
  state.id_functiond  = NULL;
  state.id_location   = NULL;
  state.id_variable   = NULL;
}

/* Fset_state()      **********************************************/

int Fset_state(int new_state)
{
  state.last_state = state.state;
  state.state      = new_state;
  return (state.last_state);
}

/* Fsub_state()      **********************************************/

int Fsub_state(int new_state)
{
  state.last_state = state.state;
  state.state &= ~new_state;
  return (state.last_state);
}

/* Fget_word()      **********************************************/

int Fget_word() { return (state.word); }

/* Fset_word()      **********************************************/

int Fset_word(int new_word)
{
  int old_word;

  old_word   = state.word;
  state.word = new_word;
  return (old_word);
}

/* Fword_to_string()    **********************************************/

const char* Fword_to_string(int word)
{
  switch (word) {
    case ACEGR_CURV: return ("$ACEgr_Curv");
    case ACEGR_HIST: return ("$ACEgr_Hist");
    case ALLOCATION: return ("$Allocation");
    case AUXILIARY: return ("$Auxiliary");
    case C_EXP: return ("$C_exp");
    case DEC_POUND:
    case DEF_POUND: return ("#");
    case DEFAULT: return ("$Default");
    case DIR: return ("$Dir");
    case DISPLAY: return ("$Display");
    case END_FUNCTION: return ("$End_Function");
    case EXT: return ("$Ext");
    case FATHER: return ("$Father");
    case FUNCTIOND: return ("$Function");
    case GALLOCATION: return ("$Gallocation");
    case GL_DESC: return ("$Gl_desc");
    case GS_DESC: return ("$Gs_desc");
    case GVALIDF: return ("$Gvalidf");
    case HIDDEN: return ("$Hidden");
    case ICON: return ("$Icon");
    case INC_CODE: return ("$Include_code");
    case INDEX: return ("$Index");
    case KEY: return ("$Key");
    case KIND: return ("$Kind");
    case L_DESC: return ("$L_desc");
    case LENGTH: return ("$Length");
    case LOCATION: return ("$Location");
    case MANDATORY: return ("$Mandatory");
    case MAX: return ("$Max");
    case MEMBER: return ("$Member");
    case MENU: return ("$Menu");
    case MIN: return ("$Min");
    case OUTPUT: return ("$Output");
    case PARAMGRP: return ("$Paramgrp");
    case PARAMSGRP: return ("$Paramsgrp");
    case S_DESC: return ("$S_desc");
    case STRUCTURE: return ("$Structure");
    case TRAIL_ARGS: return ("$Trail_args");
    case TYPE: return ("$Type");
    case UNITS: return ("$Units");
    case VALIDF: return ("$Validf");
    case VARIABLE: return ("$Variable");
    case VECTOR: return ("$Vector");
    case VIEW: return ("$View");
    case VOLATIL: return ("$Volatil");
    default: return ("");
  }
}

/* Ffuncd_id_list()    **********************************************/

Text Ffuncd_id_list(Text text)
{
  Fset_state(IN_FUNCTIOND);
  state.id_functiond = text;
  return (text);
}

/* Floc_id_list()    **********************************************/

Text Floc_id_list(Text text)
{
  Fset_state(IN_LOCATION);
  state.id_location = text;
  return (text);
}

/* Fvar_id_list()    **********************************************/

Text Fvar_id_list(Text text)
{
  Fset_state(IN_VARIABLE);
  state.id_variable = text;
  return (text);
}

/* Farg_id()      **********************************************/

Arg* Farg_id(Arg* arg)
{
  Fadd_state(IN_ARGUMENT);
  state.id_argument = arg->str;
  return (arg);
}

/* Fdesc_id()      **********************************************/

int Fdesc_id(int word)
{
  Fset_word(word);
  Fadd_state(IN_DESCRIPTOR);
  state.id_descriptor = (char*)Fword_to_string(word);
  return (word);
}

/* Ffunc_id()      **********************************************/

char* Ffunc_id(char* string)
{
  Fadd_state(IN_FUNCTION);
  state.id_function = string;
  return (string);
}

/* Fmem_id()      **********************************************/

char* Fmem_id(char* string)
{
  Fset_state(IN_MEMBER);
  state.id_member = string;
  return (string);
}

/* Fpgrp_id()      **********************************************/

char* Fpgrp_id(char* string)
{
  Fset_state(IN_PARAMGRP);
  state.id_paramgrp = string;
  return (string);
}

/* Fstruct_id()      **********************************************/

char* Fstruct_id(char* string)
{
  Fset_state(IN_STRUCTURE);
  state.id_structure = string;
  return (string);
}

/* Fgallocation()    **********************************************/

void Fgallocation(Parameters* parameters, int allocation)
{
  if (parameters->gallocation) {
    fprintf(stderr, "Multiple definition of Gallocation\n");
    return;
  }
  parameters->gallocation = allocation;
}

/* Ffunc_declare()    **********************************************/
void Ffunc_declare(Parameters* parameters, Text id_list, Descriptor* type)
{
  Function* function;
  int       n = 0;

  while (id_list[n]) {
    function                  = (Function*)malloc(sizeof(Function));
    function->name            = id_list[n];
    function->type            = type->value;
    function->arg_list        = NULL;
    function->next            = parameters->function_list;
    parameters->function_list = function;
    n++;
  }
}

/* Fgl_desc()    ******************************************************/

void Fgl_desc(Parameters* parameters, Text text)
{
  if (parameters->gl_desc) {
    fprintf(stderr, "Multiple definition of GL_desc\n");
    return;
  }
  parameters->gl_desc = text;
}

/* Fgs_desc()    ******************************************************/

void Fgs_desc(Parameters* parameters, char* str)
{
  if (parameters->gs_desc) {
    fprintf(stderr, "Multiple definition of GS_desc\n");
    return;
  }
  parameters->gs_desc = str;
}

/* Fend_function()  ******************************************************/

void Fend_function(Parameters* parameters, Function* function)
{
  Function* f;

  for (f = function; f->next; f = f->next)
    ;
  f->next                  = parameters->end_function;
  parameters->end_function = f;
}

/* Fgvalidf()    ******************************************************/

void Fgvalidf(Parameters* parameters, Function* function)
{
  Function* f;

  for (f = function; f->next; f = f->next)
    ;
  f->next             = parameters->gvalidf;
  parameters->gvalidf = f;
}

/* Ficon()    ******************************************************/

void Ficon(Parameters* parameters, char* filename)
{
  if (parameters->icon) {
    fprintf(stderr, "Multiple definition of Icon\n");
    return;
  }
  parameters->icon = filename;
}

/* Finc_code()    ******************************************************/

void Finc_code(Parameters* parameters, Text text)
{
  if (parameters->inc_code) {
    fprintf(stderr, "Multiple definition of Inc_code\n");
    return;
  }
  parameters->inc_code = text;
}

/* Ftrail_args()  ******************************************************/

void Ftrail_args(Parameters* parameters) { parameters->trail_args = TRUE; }

/* Fline_spec()    ******************************************************/

void Fline_spec(char* line, char* file)
{
  int new_lineno;

  sscanf(line, "%d", &new_lineno);
  lineno -= 1 + last_pound_lineno - new_lineno;
  if (pdffile) free(pdffile);
  pdffile = file;
}

/* Fstructure()    ******************************************************/

void Fstructure(Parameters* parameters, char* name, Member* member_list)
{
  Structure *last, *structure;
  Member*    member;

  structure              = (Structure*)malloc(sizeof(Structure));
  structure->name        = name;
  structure->member_list = member_list;
  structure->n_members   = 0;
  structure->next        = NULL;

  member = member_list;
  while (member) {
    structure->n_members++;
    member = member->next;
  }

  /*
     The order of structure definitions must be preserved,
     so add new definition at the end of structure list
     */
  last = parameters->structure_list;
  if (last) {
    while (last->next) last = last->next;
    last->next = structure;
  } else
    parameters->structure_list = structure;
}

/* Fparamgrp()    ******************************************************/

Paramgrp* Fparamgrp(Parameters* parameters, char* name, Descriptor* descriptor_list)
{
  Paramgrp *  paramgrp, *paramsgrp, *paramsgrp_list;
  Pgrp_item*  pgrp_item;
  Variable *  location, *location_list, *textbox, *textbox_list, *variable, *variable_list;
  Descriptor* descriptor;

  paramgrp         = (Paramgrp*)calloc(1, sizeof(Paramgrp));
  paramgrp->name   = name;
  paramgrp->father = parameters->main_paramgrp;

  descriptor = descriptor_list;
  while (descriptor) {
    switch (descriptor->type) {
      case L_DESC:
        if (paramgrp->l_desc) {
          fprintf(stderr, "    Multiple l_desc definition\n");
          paramgrp->error = TRUE;
          break;
        }
        paramgrp->l_desc = (Text)(descriptor->value->data);
        break;
      case PARAMSGRP:
        paramsgrp = paramsgrp_list = (Paramgrp*)(descriptor->value->data);
        if (paramsgrp == NULL) {
          paramgrp->error = TRUE;
          break;
        }
        while (TRUE) {
          if (paramsgrp->father && strcmp(paramsgrp->father->name, "Main")) {
            fprintf(stderr, "    Subgroup %s is already included in parameter group %s\n",
                    paramsgrp->name, paramsgrp->father->name);
            paramgrp->error = TRUE;
            break;
          }
          paramsgrp->father = paramgrp;
          paramgrp->n_items++;
          parameters->main_paramgrp->n_items--;
          pgrp_item                = (Pgrp_item*)malloc(sizeof(Pgrp_item));
          pgrp_item->type          = PARAMSGRP;
          pgrp_item->item          = (char*)paramsgrp;
          pgrp_item->next          = paramgrp->pgrp_item_list;
          paramgrp->pgrp_item_list = pgrp_item;
          if (paramsgrp->next_in_sgrp)
            paramsgrp = paramsgrp->next_in_sgrp;
          else
            break;
        }
        paramsgrp->next_in_sgrp  = paramgrp->paramsgrp_list;
        paramgrp->paramsgrp_list = paramsgrp_list;
        break;
      case S_DESC:
        if (paramgrp->s_desc) {
          fprintf(stderr, "    Multiple s_desc definition\n");
          paramgrp->error = TRUE;
          break;
        }
        paramgrp->s_desc = descriptor->value->data;
        break;
      case VALIDF:
        if (paramgrp->validf) {
          fprintf(stderr, "    Multiple validf definition\n");
          paramgrp->error = TRUE;
          break;
        }
        paramgrp->validf = (Function*)(descriptor->value->data);
        break;
      case VARIABLE:
        variable = variable_list = (Variable*)(descriptor->value->data);
        while (TRUE) {
          parameters->n_variables++;
          if (variable->description->structure)
            if (variable->description->type->next)
              parameters->n_struct_arrays++;
            else
              parameters->n_structures++;
          else {
            if (variable->description->type->next) parameters->n_arrays++;
            switch (variable->description->type->type) {
              case RDIR:
              case RWDIR:
              case WDIR:
              case RFILE:
              case RWFILE:
              case WFILE:
              case STRING:
              case EXPR: parameters->n_strings++; break;
            }
            switch (variable->description->type->type) {
              case BYTE:
              case CHAR:
              case SHORT: parameters->n_short_ints++; break;
              case RDIR:
              case RWDIR:
              case WDIR: parameters->n_dirs++; break;
              case RFILE:
              case RWFILE:
              case WFILE:
                parameters->n_files++;
                if (variable->description->mandatory) parameters->n_mandatories++;
                break;
            }
          }
          if (variable->description->kind == COMPILED) parameters->n_compiled++;
          if (variable->description->volatil) parameters->n_volatils++;
          if (variable->description->output) parameters->n_outputs++;
          if (variable->description->auxiliary) parameters->n_auxiliaries++;
          paramgrp->n_items++;
          pgrp_item                = (Pgrp_item*)malloc(sizeof(Pgrp_item));
          pgrp_item->type          = VARIABLE;
          pgrp_item->item          = (char*)variable;
          pgrp_item->next          = paramgrp->pgrp_item_list;
          paramgrp->pgrp_item_list = pgrp_item;
          if (variable->next_in_pgrp)
            variable = variable->next_in_pgrp;
          else
            break;
        }
        variable->next_in_pgrp  = paramgrp->variable_list;
        paramgrp->variable_list = variable_list;
        break;
      case LOCATION:
        location = location_list = (Variable*)(descriptor->value->data);
        while (TRUE) {
          parameters->n_locations++;
          if (location->description->structure)
            if (location->description->type->next)
              parameters->n_struct_arrays++;
            else
              parameters->n_structures++;
          else {
            if (location->description->type->next) parameters->n_arrays++;
            switch (location->description->type->type) {
              case RDIR:
              case RWDIR:
              case WDIR:
              case RFILE:
              case RWFILE:
              case WFILE:
              case STRING:
              case EXPR: parameters->n_strings++; break;
            }
            switch (location->description->type->type) {
              case BYTE:
              case CHAR:
              case SHORT: parameters->n_short_ints++; break;
              case RDIR:
              case RWDIR:
              case WDIR: parameters->n_dirs++; break;
              case RFILE:
              case RWFILE:
              case WFILE:
                parameters->n_files++;
                if (location->description->mandatory) parameters->n_mandatories++;
                break;
            }
          }
          if (location->description->volatil) parameters->n_volatils++;
          if (location->description->output) parameters->n_outputs++;
          if (location->description->auxiliary) parameters->n_auxiliaries++;
          paramgrp->n_items++;
          pgrp_item                = (Pgrp_item*)malloc(sizeof(Pgrp_item));
          pgrp_item->type          = LOCATION;
          pgrp_item->item          = (char*)location;
          pgrp_item->next          = paramgrp->pgrp_item_list;
          paramgrp->pgrp_item_list = pgrp_item;
          if (location->next_in_pgrp)
            location = location->next_in_pgrp;
          else
            break;
        }
        location->next_in_pgrp  = paramgrp->location_list;
        paramgrp->location_list = location_list;
        break;
      case TEXTBOX:
        textbox = textbox_list = (Variable*)(descriptor->value->data);
        while (TRUE) {
          paramgrp->n_items++;
          pgrp_item                = (Pgrp_item*)malloc(sizeof(Pgrp_item));
          pgrp_item->type          = TEXTBOX;
          pgrp_item->item          = (char*)textbox;
          pgrp_item->next          = paramgrp->pgrp_item_list;
          paramgrp->pgrp_item_list = pgrp_item;
          if (textbox->next_in_pgrp)
            textbox = textbox->next_in_pgrp;
          else
            break;
        }
        textbox->next_in_pgrp  = paramgrp->textbox_list;
        paramgrp->textbox_list = textbox_list;
        break;
      case VIEW:
        if (paramgrp->view) {
          fprintf(stderr, "    Multiple view definition\n");
          paramgrp->error = TRUE;
          break;
        }
        paramgrp->view = descriptor->value->intdata;
        break;
      case HIDDEN:
        paramgrp->hidden = TRUE;
        variable         = paramgrp->variable_list;
        while (variable) {
          variable->description->hidden = TRUE;
          if (variable->structure) {
            Member* memb = variable->structure->member_list;
            while (memb) {
              memb->description->hidden = TRUE;
              memb                      = memb->next;
            }
          }
          variable = variable->next_in_pgrp;
        }
        break;
    }
    descriptor = descriptor->next;
  }
  paramgrp->next            = parameters->paramgrp_list;
  parameters->paramgrp_list = paramgrp;
  if (paramgrp->error) {
    fprintf(stderr, "Error in ParamGrp %s definition\n", name);
    exit(1);
  }

  return (paramgrp);
}

/* Fml_desc()    ******************************************************/

void Fml_desc(Parameters* parameters, Descriptor* descriptor)
{
  if (parameters->main_paramgrp->l_desc) {
    fprintf(stderr, "    Multiple l_desc definition\n");
    parameters->main_paramgrp->error = TRUE;
    return;
  }
  parameters->main_paramgrp->l_desc = (Text)(descriptor->value->data);
}

/* Fms_desc()    ******************************************************/

void Fms_desc(Parameters* parameters, Descriptor* descriptor)
{
  if (parameters->main_paramgrp->s_desc) {
    fprintf(stderr, "    Multiple s_desc definition\n");
    parameters->main_paramgrp->error = TRUE;
    return;
  }
  parameters->main_paramgrp->s_desc = descriptor->value->data;
}

/* Fmvalidf()    ******************************************************/

void Fmvalidf(Parameters* parameters, Descriptor* descriptor)
{
  if (parameters->main_paramgrp->validf) {
    fprintf(stderr, "    Multiple validf definition\n");
    parameters->main_paramgrp->error = TRUE;
    return;
  }
  parameters->main_paramgrp->validf = (Function*)(descriptor->value->data);
}

/* Fmview()    ******************************************************/

void Fmview(Parameters* parameters, Descriptor* descriptor)
{
  if (parameters->main_paramgrp->view) {
    fprintf(stderr, "    Multiple view definition\n");
    parameters->main_paramgrp->error = TRUE;
    return;
  }
  parameters->main_paramgrp->view = descriptor->value->intdata;
}

/* Fmhide()    ******************************************************/

void Fmhide(Parameters* parameters, Descriptor* descriptor)
{
  if (parameters->main_paramgrp->hidden) {
    fprintf(stderr, "    Multiple hidden definitions\n");
    parameters->main_paramgrp->error = TRUE;
    return;
  }
  printf("We are hiding\n");
  parameters->main_paramgrp->hidden = descriptor->value->intdata;
}

/* Fgvariable()    ******************************************************/

void Fgvariable(Parameters* parameters, Variable* variable_list)
{
  Paramgrp*  paramgrp;
  Pgrp_item* pgrp_item;
  Variable*  variable;

  paramgrp = parameters->main_paramgrp;
  variable = variable_list;
  while (TRUE) {
    parameters->n_variables++;
    if (variable->description->structure)
      if (variable->description->type->next)
        parameters->n_struct_arrays++;
      else
        parameters->n_structures++;
    else {
      if (variable->description->type->next) parameters->n_arrays++;
      switch (variable->description->type->type) {
        case RDIR:
        case RWDIR:
        case WDIR:
        case RFILE:
        case RWFILE:
        case WFILE:
        case STRING:
        case EXPR: parameters->n_strings++; break;
      }
      switch (variable->description->type->type) {
        case BYTE:
        case CHAR:
        case SHORT: parameters->n_short_ints++; break;
        case RDIR:
        case RWDIR:
        case WDIR: parameters->n_dirs++; break;
        case RFILE:
        case RWFILE:
        case WFILE:
          parameters->n_files++;
          if (variable->description->mandatory) parameters->n_mandatories++;
          break;
      }
    }
    if (variable->description->kind == COMPILED) parameters->n_compiled++;
    if (variable->description->volatil) parameters->n_volatils++;
    if (variable->description->output) parameters->n_outputs++;
    if (variable->description->auxiliary) parameters->n_auxiliaries++;
    paramgrp->n_items++;
    pgrp_item                = (Pgrp_item*)malloc(sizeof(Pgrp_item));
    pgrp_item->type          = VARIABLE;
    pgrp_item->item          = (char*)variable;
    pgrp_item->next          = paramgrp->pgrp_item_list;
    paramgrp->pgrp_item_list = pgrp_item;
    if (variable->next_in_pgrp)
      variable = variable->next_in_pgrp;
    else
      break;
  }
  variable->next_in_pgrp  = paramgrp->variable_list;
  paramgrp->variable_list = variable_list;
}

/* Fglocation()    ******************************************************/

void Fglocation(Parameters* parameters, Variable* location_list)
{
  Paramgrp*  paramgrp;
  Pgrp_item* pgrp_item;
  Variable*  location;

  paramgrp = parameters->main_paramgrp;
  location = location_list;
  while (TRUE) {
    parameters->n_locations++;
    if (location->description->structure)
      if (location->description->type->next)
        parameters->n_struct_arrays++;
      else
        parameters->n_structures++;
    else {
      if (location->description->type->next) parameters->n_arrays++;
      switch (location->description->type->type) {
        case RDIR:
        case RWDIR:
        case WDIR:
        case RFILE:
        case RWFILE:
        case WFILE:
        case STRING:
        case EXPR: parameters->n_strings++; break;
      }
      switch (location->description->type->type) {
        case BYTE:
        case CHAR:
        case SHORT: parameters->n_short_ints++; break;
        case RDIR:
        case RWDIR:
        case WDIR: parameters->n_dirs++; break;
        case RFILE:
        case RWFILE:
        case WFILE:
          parameters->n_files++;
          if (location->description->mandatory) parameters->n_mandatories++;
          break;
      }
    }
    if (location->description->volatil) parameters->n_volatils++;
    if (location->description->output) parameters->n_outputs++;
    if (location->description->auxiliary) parameters->n_auxiliaries++;
    paramgrp->n_items++;
    pgrp_item                = (Pgrp_item*)malloc(sizeof(Pgrp_item));
    pgrp_item->type          = LOCATION;
    pgrp_item->item          = (char*)location;
    pgrp_item->next          = paramgrp->pgrp_item_list;
    paramgrp->pgrp_item_list = pgrp_item;
    if (location->next_in_pgrp)
      location = location->next_in_pgrp;
    else
      break;
  }
  location->next_in_pgrp  = paramgrp->location_list;
  paramgrp->location_list = location_list;
}

/* Fgparamsgrp()    ******************************************************/

void Fgparamsgrp(Parameters* parameters, Paramgrp* paramsgrp_list)
{
  Paramgrp * paramgrp, *paramsgrp;
  Pgrp_item* pgrp_item;

  paramgrp  = parameters->main_paramgrp;
  paramsgrp = paramsgrp_list;
  while (TRUE) {
    paramgrp->n_items++;
    pgrp_item                = (Pgrp_item*)malloc(sizeof(Pgrp_item));
    pgrp_item->type          = PARAMSGRP;
    pgrp_item->item          = (char*)paramsgrp;
    pgrp_item->next          = paramgrp->pgrp_item_list;
    paramgrp->pgrp_item_list = pgrp_item;
    if (paramsgrp->next_in_sgrp)
      paramsgrp = paramsgrp->next_in_sgrp;
    else
      break;
  }
}

/* Fgtextbox()    ******************************************************/

void Fgtextbox(Parameters* parameters, Variable* textbox_list)
{
  Paramgrp*  paramgrp;
  Pgrp_item* pgrp_item;
  Variable*  textbox;

  paramgrp = parameters->main_paramgrp;
  textbox  = textbox_list;
  while (TRUE) {
    paramgrp->n_items++;
    pgrp_item                = (Pgrp_item*)malloc(sizeof(Pgrp_item));
    pgrp_item->type          = TEXTBOX;
    pgrp_item->item          = (char*)textbox;
    pgrp_item->next          = paramgrp->pgrp_item_list;
    paramgrp->pgrp_item_list = pgrp_item;
    if (textbox->next_in_pgrp)
      textbox = textbox->next_in_pgrp;
    else
      break;
  }
  textbox->next_in_pgrp  = paramgrp->textbox_list;
  paramgrp->textbox_list = textbox_list;
}

/* Ffunction()    ******************************************************/

Function* Ffunction(char* name, Arg* arg_list)
{
  Function *function, *prototype;

  function           = (Function*)malloc(sizeof(Function));
  function->name     = name;
  function->arg_list = arg_list;
  function->next     = NULL;

  prototype = function_named(name);
  if (prototype) {
    function->type = prototype->type;
  } else {
    fprintf(stderr, "Warning: function %s is undeclared, type Int is assumed\n", name);
    function->type = NULL;
  }

  return (function);
}

/* Farg_list()    ******************************************************/
Arg* Farg_list(Arg* arg_list, Arg* arg)
{
  Arg* last_arg;

  if (arg_list) {
    last_arg = arg_list;
    while (last_arg->next) last_arg = last_arg->next;
    last_arg->next = arg;
  } else
    arg_list = arg;

  return (arg_list);
}

/* Farg()    ******************************************************/
Arg* Farg(int type, char* str)
{
  Arg* arg;

  arg       = (Arg*)malloc(sizeof(Arg));
  arg->type = type;
  arg->str  = str;
  arg->next = NULL;

  return (arg);
}

/* Farray()    ******************************************************/
char* Farray(char* array_name, char* index_name)
{
  if (array_name) {
    array_name = (char*)realloc(array_name, 3 + strlen(array_name) + strlen(index_name));
    strcat(array_name, "[");
    strcat(array_name, index_name);
    strcat(array_name, "]");
  }

  return (array_name);
}

/* Fpoint()    ******************************************************/
char* Fpoint(char* structure_name, char* member_name)
{
  if (structure_name) {
    structure_name =
        (char*)realloc(structure_name, 2 + strlen(structure_name) + strlen(member_name));
    strcat(structure_name, ".");
    strcat(structure_name, member_name);
  } else {
    structure_name = member_name;
  }

  return (structure_name);
}

/* Farrow()    ******************************************************/
char* Farrow(char* structure_name, char* member_name)
{
  if (structure_name) {
    structure_name =
        (char*)realloc(structure_name, 3 + strlen(structure_name) + strlen(member_name));
    strcat(structure_name, "->");
    strcat(structure_name, member_name);
  } else {
    structure_name = member_name;
  }

  return (structure_name);
}

/* Fstring()    ******************************************************/
char* Fstring(char* string, char* str)
{
  int length;

  if (string) {
    length = strlen(string);
    if (string[length - 2] != '\\')
      string[length - 1] = '\000'; /* If not escaped, remove trailing " */
    str++;                         /* Remove leading " */
    string = (char*)realloc(string, 1 + strlen(string) + strlen(str));
    strcat(string, str);
  } else {
    string = str;
  }

  return (string);
}

/* Ftext()    ******************************************************/
Text Ftext(Text text, const char* str)
{
  if (text == NULL) text = newtext();
  text = addline(text, str);
  return (text);
}

/* Fmember_list()  ******************************************************/
Member* Fmember_list(Member* member_list, Member* member)
{
  Member* m;

  if (member->description == NULL) {
    free(member);
    return (member_list);
  }

  m = member_list;
  while (m) {
    if (0 == strcmp(m->name, member->name)) {
      fprintf(stderr, "Multiple definition of %s\n", m->name);
      return (member_list);
    }
    m = m->next;
  }
  member->next = member_list;
  member_list  = member;

  return (member_list);
}

/* Fmember()    ******************************************************/
Member* Fmember(char* name, Description* description)
{
  Member* member;

  member              = (Member*)malloc(sizeof(Member));
  member->name        = name;
  member->description = description;
  if (language == FORTRAN) {
    switch (description->type->type) {
      case BYTE:
      case RDIR:
      case RWDIR:
      case WDIR:
      case RFILE:
      case RWFILE:
      case WFILE:
      case STRING:
      case EXPR:
        fprintf(stderr, "    Members of type %s not allowed in FORTRAN\n\n",
                Type_Names[description->type->type - TYPE_BASE]);
        description->error = TRUE;
        break;
    }
  }
  if (description->allocation) {
    fprintf(stderr, "    $Allocation forbidden for members\n\n");
    description->error = TRUE;
  }
  if (description->kind == COMPILED) {
    fprintf(stderr, "    $Compiled forbidden for members\n\n");
    description->error = TRUE;
  }
  member->description->dependences = addnewline(member->description->dependences, "$Father");
  member->next                     = NULL;
  if (description->error) {
    fprintf(stderr, "Error in Member %s definition\n", name);
    exit(1);
  }

  return (member);
}

/* Fline_member()  ******************************************************/

Member* Fline_member()
{
  Member* member;

  member              = (Member*)malloc(sizeof(Member));
  member->name        = NULL;
  member->description = NULL;
  member->next        = NULL;

  return (member);
}

/* Fdescription()  ******************************************************/

Description* Fdescription(Descriptor* descriptor_list)
{
  int          default_nelem, type_nelem;
  Description* description;
  Descriptor*  descriptor;
  Value *      default_value, *default_vector, *menu_item, *value;

  description              = (Description*)calloc(1, sizeof(Description));
  description->dependences = newtext();
  description->related     = newrelated();

  descriptor = descriptor_list;
  while (descriptor) {
    switch (descriptor->type) {
      case ALLOCATION:
        if (description->allocation) {
          fprintf(stderr, "    Multiple allocation definition\n");
          description->error = TRUE;
          return (description);
        }
        description->allocation = descriptor->value->intdata;
        break;
      case AUXILIARY:
        if (description->output || description->volatil) {
          fprintf(stderr, "    $Auxiliary is incompatible with $Output or $Volatil\n");
          description->error = TRUE;
          return (description);
        }
        description->auxiliary = TRUE;
        break;
      case DEFAULT:
        if (description->default_value) {
          fprintf(stderr, "    Multiple default definition\n");
          description->error = TRUE;
          return (description);
        }
        description->default_value = descriptor->value;
        break;
      case DIR:
        if (description->dir) {
          fprintf(stderr, "    Multiple dir definition\n");
          description->error = TRUE;
          return (description);
        }
        description->dir = descriptor->value;
        break;
      case DISPLAY:
        if (description->display) {
          fprintf(stderr, "    Multiple display definition\n");
          description->error = TRUE;
          return (description);
        }
        description->display = (Function*)(descriptor->value->data);
        break;
      case EXT:
        if (description->ext) {
          fprintf(stderr, "    Multiple ext definition\n");
          description->error = TRUE;
          return (description);
        }
        description->ext = descriptor->value;
        break;
      case HIDDEN: description->hidden = TRUE; break;
      case KEY:
        if (description->key) {
          fprintf(stderr, "    Multiple key definition\n");
          description->error = TRUE;
          return (description);
        }
        description->key = descriptor->value->data;
        break;
      case KIND:
        if (description->kind) {
          fprintf(stderr, "    Multiple kind definition\n");
          description->error = TRUE;
          return (description);
        }
        description->kind = descriptor->value->intdata;
        break;
      case L_DESC:
        if (description->l_desc) {
          fprintf(stderr, "    Multiple l_desc definition\n");
          description->error = TRUE;
          return (description);
        }
        description->l_desc = (Text)(descriptor->value->data);
        break;
      case LENGTH:
        if (description->length > 0) {
          fprintf(stderr, "    Multiple length definition\n");
          description->error = TRUE;
          return (description);
        }
        if (0 == sscanf(descriptor->value->data, "%d", &description->length)) {
          fprintf(stderr, "    Incorrect length definition: %s\n", descriptor->value->data);
          description->error = TRUE;
          return (description);
        }
        if (description->length < 1) {
          fprintf(stderr, "    Invalid length definition: %d\n", description->length);
          description->error = TRUE;
          return (description);
        }
        break;
      case MANDATORY: description->mandatory = TRUE; break;
      case MAX:
        if (description->max) {
          fprintf(stderr, "    Multiple max definition\n");
          description->error = TRUE;
          return (description);
        }
        description->max = descriptor->value;
        break;
      case MENU:
        if (description->menu) {
          fprintf(stderr, "    Multiple menu definition\n");
          description->error = TRUE;
          return (description);
        }
        description->menu = descriptor->value;
        break;
      case MIN:
        if (description->min) {
          fprintf(stderr, "    Multiple min definition\n");
          description->error = TRUE;
          return (description);
        }
        description->min = descriptor->value;
        break;
      case OUTPUT:
        if (description->auxiliary || description->volatil) {
          fprintf(stderr, "    $Output is incompatible with $Auxiliary or $Volatil\n");
          description->error = TRUE;
          return (description);
        }
        if (description->output) {
          fprintf(stderr, "    Multiple output definition\n");
          description->error = TRUE;
          return (description);
        }
        description->output = descriptor->value;
        break;
      case S_DESC:
        if (description->s_desc) {
          fprintf(stderr, "    Multiple s_desc definition\n");
          description->error = TRUE;
          return (description);
        }
        description->s_desc = descriptor->value->data;
        break;
      case TYPE:
        if (description->type) {
          fprintf(stderr, "    Multiple type definition\n");
          description->error = TRUE;
          return (description);
        }
        description->type = descriptor->value;
        if (description->type->type == EXPR) { description->kind = COMPILED; }
        if (description->type->type == STR) {
          if (language == MATLAB) {
            fprintf(stderr, "    Structures forbidden with matlab\n\n");
            description->error = TRUE;
            return (description);
          }
          description->structure = structure_named(description->type->data);
          if (description->structure == NULL) {
            fprintf(stderr, "    Undefined type: %s\n", description->type->data);
            description->error = TRUE;
            return (description);
          }
        }
        if (description->type->type == VOID) {
          fprintf(stderr, "    Void type is reserved for functions\n");
          description->error = TRUE;
          return (description);
        }
        if (description->type->next) {
          /* Vector */
          if (description->type->next->type == INUM)
            description->storage = STATIC_VECTOR;
          else {
            description->storage = DYNAMIC_VECTOR;
            if (language == FORTRAN && parameters.n_compiled == 0) {
              fprintf(stderr, "    Dynamic vectors not allowed in FORTRAN\n");
              description->error = TRUE;
              return (description);
            }
          }
        } else
          description->storage = SCALAR;
        if (language == FORTRAN) {
          if (description->type->type == BYTE) {
            fprintf(stderr, "    Type %s is not allowed in FORTRAN\n\n",
                    Type_Names[value->type - TYPE_BASE]);
            description->error = TRUE;
          }
        }
        break;
      case UNITS:
        if (description->units) {
          fprintf(stderr, "    Multiple units definition\n");
          description->error = TRUE;
          return (description);
        }
        description->units = descriptor->value;
        break;
      case VALIDF:
        if (description->validf) {
          fprintf(stderr, "    Multiple validf definition\n");
          description->error = TRUE;
          return (description);
        }
        description->validf = (Function*)(descriptor->value->data);
        break;
      case VIEW:
        if (description->view) {
          fprintf(stderr, "    Multiple view definition\n");
          description->error = TRUE;
          break;
        }
        description->view = descriptor->value->intdata;
        break;
      case VOLATIL:
        if (description->auxiliary || description->output) {
          fprintf(stderr, "    $Volatil is incompatible with $Auxiliary or $Output\n");
          description->error = TRUE;
          return (description);
        }
        description->volatil = TRUE;
        break;
      default:
        fprintf(stderr, "    Invalid descriptor\n");
        description->error = TRUE;
        return (description);
    }
    merge_dependences(&description->dependences, &description->related,
                      descriptor->value->dependences, descriptor->value->related);
    descriptor = descriptor->next;
  }

  if (NULL == description->type) {
    fprintf(stderr, "    Undefined type\n");
    description->error = TRUE;
    return (description);
  }
  if (description->type->type == ANY) {
    if (!description->auxiliary) {
      fprintf(stderr, "    $Any variable, location or member must be $Auxiliary\n");
      description->error = TRUE;
    }
  }
  switch (description->type->type) {
    case RDIR:
    case RWDIR:
    case WDIR:
    case RFILE:
    case RWFILE:
    case WFILE:
    case STRING:
    case EXPR: break;
    default:
      if (description->length) {
        fprintf(stderr, "    Invalid $Length descriptor\n");
        description->error = TRUE;
        return (description);
      }
      break;
  }
  if (description->structure) {
    if (description->kind == COMPILED) {
      fprintf(stderr, "    $Compiled forbidden for structures\n");
      description->error = TRUE;
    }
    if (description->default_value) {
      fprintf(stderr, "    $Default forbidden for structures\n");
      description->error = TRUE;
    }
    if (description->menu) {
      fprintf(stderr, "    $Menu forbidden for structures\n");
      description->error = TRUE;
    }
    if (description->min) {
      fprintf(stderr, "    $Min forbidden for structures\n");
      description->error = TRUE;
    }
    if (description->max) {
      fprintf(stderr, "    $Max forbidden for structures\n");
      description->error = TRUE;
    }
    if (description->output) {
      fprintf(stderr, "    $Output forbidden for structures\n");
      description->error = TRUE;
    }
  } else {
    if (description->view) {
      fprintf(stderr, "    $View forbidden for non-structures\n");
      description->error = TRUE;
    }
  }
  if (description->storage != SCALAR) {
    if (description->kind == COMPILED) {
      fprintf(stderr, "    $Compiled forbidden for vectors\n");
      description->error = TRUE;
    }
  }
  if (description->output || description->auxiliary || description->volatil) {
    if (description->kind == COMPILED) {
      fprintf(stderr, "    $Compiled forbidden with $Output, $Auxiliary or $Volatil\n");
      description->error = TRUE;
      return (description);
    }
    if (description->storage == DYNAMIC_VECTOR) {
      fprintf(stderr, "    Dynamic vectors forbidden with $Output, $Auxiliary or $Volatil\n");
      description->error = TRUE;
      return (description);
    }
  }
  if (description->length) {
    if (description->storage == DYNAMIC_VECTOR) {
      fprintf(stderr, "    Dynamic vectors incompatible with $Length \n");
      description->error = TRUE;
      return (description);
    }
  }
  if (description->default_value) {
    /*
       Validate default value(s) if present;
       reverse default vector
       */
    if (description->output || description->auxiliary) {
      fprintf(stderr, "    $Default forbidden with $Output or $Auxiliary\n");
      description->error = TRUE;
      return (description);
    }
    default_nelem  = 0;
    default_vector = NULL;
    default_value  = description->default_value;
    while (default_value) {
      if (invalid_value(default_value, description->type->type)) {
        fprintf(stderr, "    Invalid default value %s for type %s\n", default_value->data,
                Type_Names[description->type->type - TYPE_BASE]);
        description->error = TRUE;
        return (description);
      }
      cast_type(default_value, description->type->type);
      value          = default_value;
      default_value  = default_value->next;
      value->next    = default_vector;
      default_vector = value;
      default_nelem++;
    }
    description->default_value = default_vector;
    switch (description->storage) {
      case SCALAR:
      case DYNAMIC_VECTOR:
        if (default_nelem > 1) {
          fprintf(stderr,
                  "    Invalid number of default values (%d) for scalar or dynamic vector\n",
                  default_nelem);
          description->error = TRUE;
          return (description);
        }
        break;
      case STATIC_VECTOR:
        sscanf(description->type->next->data, "%d", &type_nelem);
        if (default_nelem != 1 && default_nelem != type_nelem) {
          fprintf(stderr, "    Invalid number of default values (%d) for vector of %d elements\n",
                  default_nelem, type_nelem);
          description->error = TRUE;
          return (description);
        }
        break;
    }
  }
  if (description->menu) {
    /*
       Validate menu entries if menu present
       */
    if (description->output || description->auxiliary) {
      fprintf(stderr, "    $Menu forbidden with $Output or $Auxiliary\n");
      description->error = TRUE;
      return (description);
    }
    menu_item = description->menu;
    while (menu_item) {
      if (invalid_value(menu_item, description->type->type)) {
        fprintf(stderr, "    Invalid menu item %s for type %s\n", menu_item->data,
                Type_Names[description->type->type - TYPE_BASE]);
        description->error = TRUE;
        return (description);
      }
      cast_type(menu_item, description->type->type);
      menu_item = menu_item->next;
    }
  }
  if (description->min) {
    /*
       Validate minimum value if present
       */
    if (description->output || description->auxiliary) {
      fprintf(stderr, "    $Min forbidden with $Output or $Auxiliary\n");
      description->error = TRUE;
      return (description);
    }
    if (invalid_value(description->min, description->type->type)) {
      fprintf(stderr, "    Invalid minimum value %s for type %s\n", description->min->data,
              Type_Names[description->type->type - TYPE_BASE]);
      description->error = TRUE;
      return (description);
    }
    cast_type(description->min, description->type->type);
    switch (description->min->type) {
      case BNUM:
      case CNUM:
      case STR:
        fprintf(stderr, "    Irrelevant minimum value %s for type %s\n", description->min->data,
                Type_Names[description->type->type - TYPE_BASE]);
        description->error = TRUE;
        return (description);
    }
  }
  if (description->max) {
    /*
       Validate maximum value if present
       */
    if (description->output || description->auxiliary) {
      fprintf(stderr, "    $Max forbidden with $Output or $Auxiliary\n");
      description->error = TRUE;
      return (description);
    }
    if (invalid_value(description->max, description->type->type)) {
      fprintf(stderr, "    Invalid maximum value %s for type %s\n", description->max->data,
              Type_Names[description->type->type - TYPE_BASE]);
      description->error = TRUE;
      return (description);
    }
    cast_type(description->max, description->type->type);
    switch (description->max->type) {
      case BNUM:
      case CNUM:
      case STR:
        fprintf(stderr, "    Irrelevant maximum value %s for type %s\n", description->max->data,
                Type_Names[description->type->type - TYPE_BASE]);
        description->error = TRUE;
        return (description);
    }
  }
  if (description->dir) {
    /*
       Validate directory if present
       */
    if (description->output || description->auxiliary) {
      fprintf(stderr, "    $Dir forbidden with $Output or $Auxiliary\n");
      description->error = TRUE;
      return (description);
    }
    if (invalid_value(description->dir, RDIR)) {
      fprintf(stderr, "    Invalid directory %s for type %s\n", description->dir->data,
              Type_Names[description->type->type - TYPE_BASE]);
      description->error = TRUE;
      return (description);
    }
  }
  if (description->ext) {
    /*
       Validate extension if present
       */
    if (description->output || description->auxiliary) {
      fprintf(stderr, "    $Ext forbidden with $Output or $Auxiliary\n");
      description->error = TRUE;
      return (description);
    }
    if (invalid_value(description->ext, RDIR)) {
      fprintf(stderr, "    Invalid extension %s for type %s\n", description->dir->data,
              Type_Names[description->type->type - TYPE_BASE]);
      description->error = TRUE;
      return (description);
    }
  }
  if (description->output && description->output->data) {
    /*
       Validate output if present
       */
    if (invalid_value(description->output, description->type->type)) {
      fprintf(stderr, "    Invalid output %s for type %s\n", description->output->data,
              Type_Names[description->type->type - TYPE_BASE]);
      description->error = TRUE;
      return (description);
    }
    cast_type(description->output, description->type->type);
  }
  if (description->validf) {
    if (description->output || description->auxiliary) {
      fprintf(stderr, "    $Validf forbidden with $Output or $Auxiliary\n");
      description->error = TRUE;
      return (description);
    }
  }
  if (NULL == description->default_value && NULL == description->structure &&
      !description->output && !description->auxiliary) {
    /*
       Provide a type-dependent default value if it is missing
       */
    default_value              = (Value*)malloc(sizeof(Value));
    default_value->label       = NULL;
    default_value->sensitive   = NULL;
    default_value->dependences = NULL;
    default_value->related     = NULL;
    default_value->next        = NULL;
    if (description->menu)
      default_value = copy_value(description->menu);
    else
      switch (description->type->type) {
        case BYTE:
        case CHAR:
        case INT:
        case LONG:
        case SHORT:
          if (description->min) {
            free(default_value);
            default_value = copy_value(description->min);
          } else if (description->max) {
            free(default_value);
            default_value = copy_value(description->max);
          } else {
            default_value->type = INUM;
            default_value->data = newstr("0");
            cast_type(default_value, description->type->type);
          }
          break;
        case BOOLEAN:
        case FLAG:
          default_value->type = BNUM;
          default_value->data = newstr("PrMFALSE");
          cast_type(default_value, description->type->type);
          break;
        case DOUBLE:
        case FLOAT:
          if (description->min) {
            free(default_value);
            default_value = copy_value(description->min);
          } else if (description->max) {
            free(default_value);
            default_value = copy_value(description->max);
          } else {
            default_value->type = FNUM;
            default_value->data = newstr("0.");
            cast_type(default_value, description->type->type);
          }
          break;
        case COMPLEX:
        case D_COMPLEX:
          default_value->type = CNUM;
          default_value->data = newstr("0.+0.i");
          cast_type(default_value, description->type->type);
          break;
        case RDIR:
        case RWDIR:
        case WDIR:
          default_value->type = STR;
          default_value->data = newstr("\".\"");
          cast_type(default_value, description->type->type);
          break;
        case RFILE:
        case RWFILE:
        case WFILE:
        case STRING:
        case EXPR:
          default_value->type = STR;
          default_value->data = newstr("\"\"");
          cast_type(default_value, description->type->type);
          break;
      }
    description->default_value = default_value;
  }
  if (description->display) {
    /*
       Validate display function if present
       */
    if (0 == strcmp(description->display->name, "$ACEGR_CURV") ||
        0 == strcmp(description->display->name, "$ACEGR_HIST")) {
      if (description->storage == SCALAR) {
        fprintf(stderr, "    $ACEGR_XXXX display is only valid for vectors\n");
        description->error = TRUE;
      }
      switch (description->type->type) {
        case BYTE:
        case CHAR:
        case INT:
        case LONG:
        case SHORT:
        case FLOAT:
        case DOUBLE: break;
        default:
          fprintf(stderr, "    $ACEGR_XXXX display is only valid for numeric parameters\n");
          description->error = TRUE;
          break;
      }
      parameters.acegr = TRUE;
    }
  }

  if (description->kind == 0) description->kind = RUNTIME;

  return (description);
}

/* Fdesc_list()    ******************************************************/

Descriptor* Fdesc_list(Descriptor* descriptor_list, Descriptor* descriptor)
{
  if (descriptor->type == 0) {
    free(descriptor);
    return (descriptor_list);
  }

  descriptor->next = descriptor_list;
  descriptor_list  = descriptor;

  return (descriptor_list);
}

/* Fline_desc()  ******************************************************/

Descriptor* Fline_desc()
{
  Descriptor* descriptor;

  descriptor        = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type  = 0;
  descriptor->value = NULL;
  descriptor->next  = NULL;

  return (descriptor);
}

/* Fallocation()  ******************************************************/

Descriptor* Fallocation(int allocation)
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = ALLOCATION;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = ALLOCATION;
  descriptor->value->intdata     = allocation;
  descriptor->value->data        = NULL;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Fauxiliary()    ******************************************************/

Descriptor* Fauxiliary()
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = AUXILIARY;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = AUXILIARY;
  descriptor->value->data        = NULL;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Fhidden()    ******************************************************/

Descriptor* Fhidden()
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = HIDDEN;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = HIDDEN;
  descriptor->value->data        = NULL;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Fdefault_value()  ******************************************************/

Descriptor* Fdefault_value(Value* value)
{
  Descriptor* descriptor;

  descriptor        = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type  = DEFAULT;
  descriptor->value = value;
  descriptor->next  = NULL;

  return (descriptor);
}

/* Fdir_value()    ******************************************************/
Descriptor* Fdir_value(Value* value)
{
  Descriptor* descriptor;

  descriptor        = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type  = DIR;
  descriptor->value = value;
  descriptor->next  = NULL;

  return (descriptor);
}

/* Fdisplay()    ******************************************************/
Descriptor* Fdisplay(Function* function)
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = DISPLAY;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = DISPLAY;
  descriptor->value->data        = (char*)function;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Fext_value()    ******************************************************/
Descriptor* Fext_value(Value* value)
{
  Descriptor* descriptor;

  descriptor        = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type  = EXT;
  descriptor->value = value;
  descriptor->next  = NULL;

  return (descriptor);
}

/* Fkey()    ******************************************************/
Descriptor* Fkey(char* str)
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = KEY;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = STR;
  descriptor->value->data        = str;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Fkind()    ******************************************************/
Descriptor* Fkind(int kind)
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = KIND;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = KIND;
  descriptor->value->intdata     = kind;
  descriptor->value->data        = NULL;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Fl_desc()    ******************************************************/
Descriptor* Fl_desc(Text text)
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = L_DESC;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = TEXT;
  descriptor->value->data        = (char*)text;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Flength()    ******************************************************/
Descriptor* Flength(char* str)
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = LENGTH;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = STR;
  descriptor->value->data        = str;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Fmandatory()    ******************************************************/
Descriptor* Fmandatory()
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = MANDATORY;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = MANDATORY;
  descriptor->value->data        = NULL;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Fmax_value()    ******************************************************/
Descriptor* Fmax_value(Value* value)
{
  Descriptor* descriptor;

  descriptor        = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type  = MAX;
  descriptor->value = value;
  descriptor->next  = NULL;

  return (descriptor);
}

/* Fmenu()    ******************************************************/
Descriptor* Fmenu(Value* value_list)
{
  Descriptor* descriptor;

  descriptor        = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type  = MENU;
  descriptor->value = value_list;
  descriptor->next  = NULL;

  return (descriptor);
}

/* Fmin_value()    ******************************************************/
Descriptor* Fmin_value(Value* value)
{
  Descriptor* descriptor;

  descriptor        = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type  = MIN;
  descriptor->value = value;
  descriptor->next  = NULL;

  return (descriptor);
}

/* Foutput()    ******************************************************/
Descriptor* Foutput(Value* value)
{
  Descriptor* descriptor;

  descriptor       = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type = OUTPUT;
  if (value) {
    descriptor->value = value;
  } else {
    descriptor->value              = (Value*)malloc(sizeof(Value));
    descriptor->value->type        = OUTPUT;
    descriptor->value->data        = NULL;
    descriptor->value->sensitive   = NULL;
    descriptor->value->dependences = NULL;
    descriptor->value->related     = NULL;
    descriptor->value->next        = NULL;
  }
  descriptor->next = NULL;

  return (descriptor);
}

/* Fs_desc()    ******************************************************/
Descriptor* Fs_desc(char* str)
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = S_DESC;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = STR;
  descriptor->value->data        = str;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Ftype()    ******************************************************/
Descriptor* Ftype(Value* value)
{
  Descriptor* descriptor;

  descriptor        = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type  = TYPE;
  descriptor->value = value;
  descriptor->next  = NULL;

  return (descriptor);
}

/* Funits_value()  ******************************************************/
Descriptor* Funits_value(Value* value)
{
  Descriptor* descriptor;

  descriptor        = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type  = UNITS;
  descriptor->value = value;
  descriptor->next  = NULL;

  return (descriptor);
}

/* Fvalidf()    ******************************************************/
Descriptor* Fvalidf(Function* function)
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = VALIDF;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = VALIDF;
  descriptor->value->data        = (char*)function;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Fvolatil()    ******************************************************/
Descriptor* Fvolatil()
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = VOLATIL;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = VOLATIL;
  descriptor->value->data        = NULL;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Fparamsgrp()    ******************************************************/
Descriptor* Fparamsgrp(Text text)
{
  Descriptor* descriptor;
  Paramgrp *  first_paramgrp = NULL, *paramgrp;
  int         line           = 0;

  if (text == NULL) {
    fprintf(stderr, "\nNULL text in Fparamsgrp()\n\n");
    exit(1);
  }
  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = PARAMSGRP;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = PARAMSGRP;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;
  while (text[line]) {
    paramgrp = paramgrp_named(text[line]);
    if (paramgrp == NULL) {
      fprintf(stderr, "    Unable to find parameter subgroup %s\n", text[line]);
    } else {
      paramgrp->next_in_sgrp = first_paramgrp;
      first_paramgrp         = paramgrp;
    }
    line++;
  }
  descriptor->value->data = (char*)first_paramgrp;

  return (descriptor);
}

/* Fvariable_list()  ******************************************************/
Descriptor* Fvariable_list(Variable* variable_list)
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = VARIABLE;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = VARIABLE;
  descriptor->value->data        = (char*)variable_list;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Flocation_list()  ******************************************************/
Descriptor* Flocation_list(Variable* location_list)
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = LOCATION;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = LOCATION;
  descriptor->value->data        = (char*)location_list;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Ftextbox_list()  ******************************************************/
Descriptor* Ftextbox_list(Variable* textbox_list)
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = TEXTBOX;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = TEXTBOX;
  descriptor->value->data        = (char*)textbox_list;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* Fvariable()    ******************************************************/
Variable* Fvariable(Text name_list, Description* description)
{
  Variable *variable, *variable_prec = NULL;
  int       n = 0;

  while (name_list[n]) {
    if (description->error) {
      fprintf(stderr, "Error in Variable %s definition\n", name_list[n]);
      exit(1);
    }
    if (description->s_desc == NULL && description->l_desc == NULL) {
      fprintf(stderr, "Warning: variable %s is undocumented\n", name_list[n]);
    }
    variable               = (Variable*)calloc(1, sizeof(Variable));
    variable->name         = name_list[n];
    variable->description  = description;
    variable->structure    = copy_structure(description->structure);
    variable->next_in_pgrp = variable_prec;
    variable_prec          = variable;
    n++;
  }

  return (variable);
}

/* Flocation()    ******************************************************/
Variable* Flocation(Text name_list, Description* description)
{
  Variable *variable, *variable_prec = NULL;
  int       n = 0;

  while (name_list[n]) {
    if (description->key == NULL) {
      fprintf(stderr, "    Undefined type\n");
      description->error = TRUE;
    }
    if (description->error) {
      fprintf(stderr, "Error in Location %s definition\n", name_list[n]);
      exit(1);
    }
    if (description->s_desc == NULL && description->l_desc == NULL) {
      fprintf(stderr, "Warning: location %s is undocumented\n", name_list[n]);
    }
    variable               = (Variable*)calloc(1, sizeof(Variable));
    variable->name         = name_list[n];
    variable->description  = description;
    variable->structure    = copy_structure(description->structure);
    variable->next_in_pgrp = variable_prec;
    variable_prec          = variable;
    n++;
  }

  return (variable);
}

/* Ftextbox()    ******************************************************/
Variable* Ftextbox(Text text)
{
  Variable* textbox;

  textbox               = (Variable*)calloc(1, sizeof(Variable));
  textbox->name         = new_textbox_name();
  textbox->text         = text;
  textbox->next_in_pgrp = NULL;

  return (textbox);
}

/* Fitem()    ******************************************************/
Value* Fitem(Value* value, Value* label, Value* sensitive)
{
  if (label)
    value->label = newstr(label->data);
  else
    value->label = NULL;
  if (sensitive) {
    value->sensitive = sensitive->data;
    merge_dependences(&value->dependences, &value->related, sensitive->dependences,
                      sensitive->related);
  } else
    value->sensitive = NULL;

  return (value);
}

/* Fvalue()    ******************************************************/
Value* Fvalue(int type, char* str)
{
  Value* value;

  value            = (Value*)malloc(sizeof(Value));
  value->type      = type;
  value->data      = newstr(str);
  value->sensitive = NULL;
  if (type == IDENTIFIER || type == LVALUE) {
    value->dependences = newtext();
    value->related     = newrelated();
    value->dependences = addline(value->dependences, str);
    value->related     = addrelated(value->related, str);
  } else {
    value->dependences = NULL;
    value->related     = NULL;
  }
  value->label = NULL;
  value->next  = NULL;

  return (value);
}

/* Fvalue_func()  ******************************************************/
Value* Fvalue_func(Function* function)
{
  int    len;
  Value* value;
  Arg*   arg;

  value              = (Value*)malloc(sizeof(Value));
  value->type        = FUNCTION;
  len                = strlen(function->name) + 3; /* Add space for ( ) and terminal NULL */
  value->sensitive   = NULL;
  value->dependences = newtext();
  value->related     = newrelated();
  arg                = function->arg_list;
  while (arg) {
    len += strlen(arg->str) + 1; /* Add space for , */
    if (arg->type == IDENTIFIER || arg->type == LVALUE) {
      value->dependences = addline(value->dependences, arg->str);
      value->related     = addrelated(value->related, arg->str);
    }
    arg = arg->next;
  }
  value->data = (char*)malloc(len);
  strcpy(value->data, function->name);
  strcat(value->data, "(");
  arg = function->arg_list;
  while (arg) {
    strcat(value->data, arg->str);
    if (arg->next) strcat(value->data, ",");
    arg = arg->next;
  }
  strcat(value->data, ")");
  value->label = NULL;
  value->next  = NULL;

  return (value);
}

/* Fvalue_vector()  ******************************************************/
Value* Fvalue_vector(char* nelem_str, Value* data, Value* labels, Value* sensitive)
{
  unsigned int elem, nelem = 0;
  Value *      next = NULL, *value;
  static char  elem_str[81];

  if (nelem_str == NULL || data == NULL) return (NULL);

  sscanf(nelem_str, "%u", &nelem);
  for (elem = 0; elem < nelem; elem++) {
    sprintf(elem_str, "%u", elem);
    value              = (Value*)malloc(sizeof(Value));
    value->type        = LVALUE;
    value->data        = Farray(newstr(data->data), elem_str);
    value->dependences = newtext();
    value->related     = newrelated();
    value->dependences = addline(value->dependences, value->data);
    value->related     = addrelated(value->related, value->data);
    if (labels) {
      value->label = Farray(newstr(labels->data), elem_str);
      merge_dependences(&value->dependences, &value->related, labels->dependences, labels->related);
    } else
      value->label = NULL;
    if (sensitive) {
      value->sensitive = Farray(newstr(sensitive->data), elem_str);
      merge_dependences(&value->dependences, &value->related, sensitive->dependences,
                        sensitive->related);
    } else
      value->sensitive = NULL;
    value->next = next;
    next        = value;
  }

  return (value);
}

/* Fc_exp()    ******************************************************/
Value* Fc_exp(char* str, Value* dependents)
{
  Value* value;

  value       = (Value*)malloc(sizeof(Value));
  value->type = C_EXP;
  value->data = unquote(str);
  if (dependents) {
    value->dependences = dependents->dependences;
    value->related     = dependents->related;
  } else {
    value->dependences = NULL;
    value->related     = NULL;
  }
  value->sensitive = NULL;
  value->label     = NULL;
  value->next      = NULL;

  return (value);
}

/* Fvector()    ******************************************************/
Value* Fvector(Value* vector, Value* value)
{
  Value* last;

  if (vector)
    merge_dependences(&value->dependences, &value->related, vector->dependences, vector->related);
  for (last = value; last->next; last = last->next)
    ;
  last->next = vector;
  vector     = value;

  return (vector);
}

/* Ftype_value()  ******************************************************/
Value* Ftype_value(int etype)
{
  Value* value;

  value              = (Value*)malloc(sizeof(Value));
  value->type        = etype;
  value->data        = NULL;
  value->label       = NULL;
  value->sensitive   = NULL;
  value->dependences = NULL;
  value->related     = NULL;
  value->next        = NULL;

  return (value);
}

/* Ftype_vector()  ******************************************************/

Value* Ftype_vector(int etype, Value* nelem)
{
  Value* value;

  value              = (Value*)malloc(sizeof(Value));
  value->type        = etype;
  value->data        = NULL;
  value->label       = NULL;
  value->sensitive   = NULL;
  value->dependences = nelem->dependences;
  value->related     = nelem->related;
  value->next        = nelem;

  return (value);
}

/* Fany()    ******************************************************/
Value* Fany(int etype, char* str)
{
  Value* value;

  if (etype != ANY) {
    yyerror("Type must be $Any with $Type { \"string\" } syntaxt");
    exit(1);
  }
  value              = (Value*)malloc(sizeof(Value));
  value->type        = ANY;
  value->data        = unquote(str);
  value->label       = NULL;
  value->sensitive   = NULL;
  value->dependences = NULL;
  value->related     = NULL;
  value->next        = NULL;

  return (value);
}

/* Fstruct_value()  ******************************************************/
Value* Fstruct_value(char* str)
{
  Value* value;

  value              = (Value*)malloc(sizeof(Value));
  value->type        = STR;
  value->data        = str;
  value->label       = NULL;
  value->sensitive   = NULL;
  value->dependences = NULL;
  value->related     = NULL;
  value->next        = NULL;

  return (value);
}

/* Fstruct_vector()  ******************************************************/
Value* Fstruct_vector(char* str, Value* nelem)
{
  Value* value;

  value              = (Value*)malloc(sizeof(Value));
  value->type        = STR;
  value->data        = str;
  value->label       = NULL;
  value->sensitive   = NULL;
  value->dependences = nelem->dependences;
  value->related     = nelem->related;
  value->next        = nelem;

  return (value);
}

/* Fview()    ******************************************************/
Descriptor* Fview(int view)
{
  Descriptor* descriptor;

  descriptor                     = (Descriptor*)malloc(sizeof(Descriptor));
  descriptor->type               = VIEW;
  descriptor->value              = (Value*)malloc(sizeof(Value));
  descriptor->value->type        = VIEW;
  descriptor->value->intdata     = view;
  descriptor->value->data        = NULL;
  descriptor->value->label       = NULL;
  descriptor->value->sensitive   = NULL;
  descriptor->value->dependences = NULL;
  descriptor->value->related     = NULL;
  descriptor->value->next        = NULL;
  descriptor->next               = NULL;

  return (descriptor);
}

/* copy_structure()  ******************************************************/
Structure* copy_structure(Structure* structure)
{
  Structure* new_structure;
  Member *   member, *new_member;

  if (structure == NULL) return (NULL);

  new_structure              = (Structure*)malloc(sizeof(Structure));
  new_structure->name        = structure->name;
  new_structure->member_list = NULL;
  member                     = structure->member_list;
  while (member) {
    new_member                 = (Member*)malloc(sizeof(Member));
    new_member->name           = member->name;
    new_member->description    = member->description;
    new_member->associate_var  = member->associate_var;
    new_member->next           = new_structure->member_list;
    new_structure->member_list = new_member;
    member                     = member->next;
  }
  new_structure->n_members = structure->n_members;
  new_structure->next      = structure->next;

  return (new_structure);
}

/* copy_variable()  ******************************************************/

Variable* copy_variable(Variable* variable)
{
  Variable* new_variable;

  if (variable == NULL) return (NULL);

  new_variable           = (Variable*)calloc(1, sizeof(Variable));
  *new_variable          = *variable;
  new_variable->ancestry = NULL;
  new_variable->text     = NULL;

  return (new_variable);
}

/* copy_description()  ******************************************************/
Description* copy_description(Description* description)
{
  Description* new_description;
  Value *      new_value, *value;

  if (description == NULL) return (NULL);

  new_description = (Description*)malloc(sizeof(Description));

  new_description->allocation    = description->allocation;
  new_description->auxiliary     = description->auxiliary;
  new_description->default_value = copy_value(description->default_value);
  value                          = description->default_value;
  new_value                      = new_description->default_value;
  if (value)
    while (value->next) {
      new_value->next = copy_value(value->next);
      value           = value->next;
      new_value       = new_value->next;
    }
  new_description->dir       = copy_value(description->dir);
  new_description->display   = copy_function(description->display);
  new_description->ext       = copy_value(description->ext);
  new_description->key       = newstr(description->key);
  new_description->kind      = description->kind;
  new_description->length    = description->length;
  new_description->l_desc    = copy_text(description->l_desc);
  new_description->max       = copy_value(description->max);
  new_description->mandatory = description->mandatory;
  new_description->menu      = copy_value(description->menu);
  new_description->hidden    = description->hidden;
  value                      = description->menu;
  new_value                  = new_description->menu;
  if (value)
    while (value->next) {
      new_value->next = copy_value(value->next);
      value           = value->next;
      new_value       = new_value->next;
    }
  new_description->min       = copy_value(description->min);
  new_description->output    = copy_value(description->output);
  new_description->storage   = description->storage;
  new_description->structure = description->structure;
  new_description->s_desc    = newstr(description->s_desc);
  new_description->type      = copy_value(description->type);
  if (description->type->next) {
    new_description->type->next = copy_value(description->type->next);
  }
  new_description->units       = copy_value(description->units);
  new_description->validf      = copy_function(description->validf);
  new_description->view        = description->view;
  new_description->volatil     = description->volatil;
  new_description->dependences = copy_text(description->dependences);
  new_description->related     = copy_related(description->related);
  new_description->error       = description->error;

  return (new_description);
}

/* copy_function()  ******************************************************/

Function* copy_function(Function* function)
{
  Function* new_function;
  Arg *     new_arg, *arg;

  if (function == NULL) return (NULL);

  new_function = (Function*)malloc(sizeof(Function));

  new_function->name = newstr(function->name);
  new_function->type = copy_value(function->type);
  arg                = function->arg_list;
  if (arg) {
    new_function->arg_list = new_arg = (Arg*)malloc(sizeof(Arg));
    new_arg->str                     = newstr(arg->str);
    new_arg->type                    = arg->type;
    new_arg->next                    = NULL;
    while (arg->next) {
      new_arg->next       = (Arg*)malloc(sizeof(Arg));
      new_arg->next->str  = newstr(arg->next->str);
      new_arg->next->type = arg->next->type;
      new_arg->next->next = NULL;
      arg                 = arg->next;
      new_arg             = new_arg->next;
    }
  } else {
    new_function->arg_list = NULL;
  }
  new_function->next = function->next;

  return (new_function);
}

/* copy_value()    ******************************************************/
Value* copy_value(Value* value)
{
  Value* new_value;

  if (value == NULL) return (NULL);

  new_value = (Value*)malloc(sizeof(Value));

  new_value->type        = value->type;
  new_value->data        = newstr(value->data);
  new_value->label       = newstr(value->label);
  new_value->sensitive   = newstr(value->sensitive);
  new_value->dependences = copy_text(value->dependences);
  new_value->related     = copy_related(value->related);
  new_value->next        = NULL;

  return (new_value);
}

/* copy_related()  ******************************************************/
Boolean* copy_related(Boolean* related)
{
  Boolean* new_related;
  int      line, nlines;

  if (related == NULL) return (NULL);

  line = 0;
  while (related[line] != -1) line++;
  nlines      = (1 + line / NALLOC) * NALLOC;
  new_related = (Boolean*)malloc(nlines * sizeof(Boolean));
  for (; line >= 0; line--) new_related[line] = related[line];

  return (new_related);
}

/* copy_text()    ******************************************************/
Text copy_text(Text text)
{
  Text new_text;
  int  line, nlines;

  if (text == NULL) return (NULL);

  line = 0;
  while (text[line]) line++;
  nlines   = (1 + line / NALLOC) * NALLOC;
  new_text = (Text)malloc(nlines * sizeof(char*));
  for (; line >= 0; line--) new_text[line] = newstr(text[line]);

  return (new_text);
}

/* newtext()    ******************************************************/
Text newtext()
{
  Text text;

  text    = (Text)malloc(NALLOC * sizeof(char*));
  text[0] = NULL;

  return (text);
}

/* freetext()    ******************************************************/
void freetext(Text text)
{
  int n_lines = 0;

  if (text == NULL) return;

  while (text[n_lines]) {
    free(text[n_lines]);
    n_lines++;
  }

  free(text);
}

/* addline()    ******************************************************/
Text addline(Text text, const char* line)
{
  int i = 0, n_lines = 0;

  if (text == NULL || line == NULL) return (text);

  while (text[i++]) n_lines++;

  text[n_lines] = (char*)malloc(strlen(line) + 1);
  strcpy(text[n_lines++], line);

  if (n_lines % NALLOC == 0) text = (Text)realloc(text, (n_lines + NALLOC) * sizeof(char*));
  text[n_lines] = NULL;

  return (text);
}

/* addnewline()    ******************************************************/
/*
   Add a line to a text if it is not already present.
   */
Text addnewline(Text text, const char* line)
{
  int i = 0, n_lines = 0;

  if (text == NULL) text = newtext();
  if (line == NULL) return (text);

  while (text[i]) {
    if (0 == strcmp(text[i], line)) return (text);
    i++;
    n_lines++;
  }

  text[n_lines] = (char*)malloc(strlen(line) + 1);
  strcpy(text[n_lines++], line);

  if (n_lines % NALLOC == 0) text = (Text)realloc(text, (n_lines + NALLOC) * sizeof(char*));
  text[n_lines] = NULL;

  return (text);
}

/* newstr()    ******************************************************/
char* newstr(const char* str)
{
  char* new_str;

  if (str == NULL) return (NULL);

  new_str = (char*)calloc(1 + strlen(str), 1);
  strcpy(new_str, str);

  return (new_str);
}

/* unquote()    ******************************************************/
/*
   Removes quotes from string, except escaped ones
   */
char* unquote(char* str)
{
  char *  c_old, *c_new, *new_str;
  Boolean escaped;

  if (str == NULL) return (NULL);

  new_str = (char*)calloc(strlen(str) + 1, 1);
  escaped = FALSE;
  for (c_old = str, c_new = new_str; *c_old; c_old++) {
    if (*c_old == '\\') {
      *c_new++ = *c_old;
      escaped  = TRUE;
    } else if (*c_old == '"') {
      if (escaped) *c_new = *c_old; /* Write " over \ */
      escaped = FALSE;
    } else {
      *c_new++ = *c_old;
      escaped  = FALSE;
    }
  }

  return (new_str);
}

/* substr()    ******************************************************/
/*
   If small_string is a sub-string of large_string, return pointer to first
   occurence of small_string in large_string;
   otherwise return NULL.
   */
char* substr(const char* large_string, const char* small_string)
{
  int   small_len, large_len;
  char* ptr;

  if (large_string == NULL || small_string == NULL) return (NULL);
  small_len = strlen(small_string);
  large_len = strlen(large_string);
  if (small_len > large_len) return (NULL);

  ptr = (char*)large_string;
  while (ptr = strchr(ptr, small_string[0])) {
    /* Found first character of small_string; look for the rest */
    if (0 == strncmp(ptr, small_string, small_len))
      return (ptr);
    else
      ptr++;
  }

  return (NULL);
}

/* substitute_first()  ******************************************************/
/*
   If small_string1 is a sub-string of large_string, return pointer to a
   new string identical to large_string, except that the first occurence of
   small_string1 in large_string is replaced by small_string2;
   otherwise return large_string.
   */
char* substitute_first(char* large_string, const char* small_string1, const char* small_string2)
{
  int   large_len, new_len, small_len1, small_len2;
  char *new_ptr, *new_string, *old_ptr, *ptr;

  if (large_string == NULL || small_string1 == NULL || small_string2 == NULL) return (large_string);
  small_len1 = strlen(small_string1);
  small_len2 = strlen(small_string2);
  large_len  = strlen(large_string);
  new_len    = large_len + small_len2 - small_len1;
  if (small_len1 > large_len) return (large_string);

  ptr = large_string;
  while (ptr = strchr(ptr, small_string1[0])) {
    /* Found first character of small_string; look for the rest */
    if (0 == strncmp(ptr, small_string1, small_len1)) {
      /* Found small_string1 in large_string */
      new_string = (char*)malloc(new_len + 1);
      strncpy(new_string, large_string, new_len);
      new_ptr = new_string;
      new_ptr += ptr - large_string;
      strcpy(new_ptr, small_string2);
      new_ptr += small_len2;
      old_ptr = ptr;
      old_ptr += small_len1;
      strcpy(new_ptr, old_ptr);
      return (new_string);
    } else
      ptr++;
  }

  return (large_string);
}

/* substitute_all()  ******************************************************/
/*
   If small_string1 is a sub-string of large_string, return pointer to a
   new string identical to large_string, except that all non-overlaping
   occurences of small_string1 in large_string are replaced by small_string2;
   otherwise return large_string.
   */

char* substitute_all(char* large_string, const char* small_string1, const char* small_string2)
{
  char *new_string, *old_string;

  if (large_string == NULL || small_string1 == NULL || small_string2 == NULL) return (large_string);

  new_string = old_string = large_string;
  while (substr(old_string, small_string1)) {
    new_string = substitute_first(old_string, small_string1, small_string2);
    if (old_string != large_string) free(old_string);
    old_string = new_string;
  }

  return (new_string);
}

/* substitute_in_text()  ******************************************************/
/*
   Replace all non-overlaping occurences of small_string1 in text
   by small_string2.
   */
void substitute_in_text(Text text, const char* small_string1, const char* small_string2)
{
  int   line;
  char *new_string, *old_string;

  if (text == NULL || small_string1 == NULL || small_string2 == NULL) return;

  line = -1;
  while (old_string = text[++line]) {
    new_string = substitute_all(old_string, small_string1, small_string2);
    if (new_string != old_string) {
      free(old_string);
      text[line] = new_string;
    }
  }

  return;
}

/* merge()    ******************************************************/
/*
   Add to text2 lines of text1 which are not already in text2.
   */

Text merge(Text text2, Text text1)
{
  int l1, l2;

  if (text1 == NULL || text2 == NULL) return (text2);

  l1 = 0;
  while (text1[l1]) {
    l2 = 0;
    while (text2[l2]) {
      if (0 == strcmp(text1[l1], text2[l2])) break;
      l2++;
    }
    if (text2[l2] == NULL) text2 = addline(text2, text1[l1]);
    l1++;
  }
  return (text2);
}

/* add_dependence()  ******************************************************/
/*
   Add line to text if it is not already there;
   then add boolean to related.
   */

void add_dependence(Text* text, Boolean** related, char* line, Boolean boolean)
{
  int l;

  if (line == NULL) return;

  l = 0;
  while ((*text)[l]) {
    if (0 == strcmp(line, (*text)[l])) break;
    l++;
  }
  if ((*text)[l] == NULL) {
    *text    = addline(*text, line);
    *related = addboolean(*related, boolean);
  }
}

/* merge_dependences()  ******************************************************/
/*
   Add lines of text1 which are not already in text2 to text2;
   add the corresponding booleans of related1 to related2.
   */

void merge_dependences(Text* text2, Boolean** related2, Text text1, Boolean* related1)
{
  int l1, l2;

  if (text1 == NULL || related1 == NULL) return;
  if (*text2 == NULL) *text2 = newtext();
  if (*related2 == NULL) *related2 = newrelated();

  l1 = 0;
  while (text1[l1]) {
    l2 = 0;
    while ((*text2)[l2]) {
      if (0 == strcmp(text1[l1], (*text2)[l2])) break;
      l2++;
    }
    if ((*text2)[l2] == NULL) {
      *text2    = addline(*text2, text1[l1]);
      *related2 = addboolean(*related2, related1[l1]);
    }
    l1++;
  }
}

/* newrelated()    ******************************************************/

Boolean* newrelated()
{
  Boolean* related;

  related    = (Boolean*)malloc(NALLOC * sizeof(Boolean));
  related[0] = -1;

  return (related);
}

/* addboolean()    ******************************************************/

Boolean* addboolean(Boolean* vector, Boolean element)
{
  int i = 0, n_lines = 0;

  if (vector == NULL) return (vector);

  while (vector[i++] != -1) n_lines++;

  vector[n_lines++] = element;

  if (n_lines % NALLOC == 0)
    vector = (Boolean*)realloc(vector, (n_lines + NALLOC) * sizeof(Boolean));
  vector[n_lines] = -1;

  return (vector);
}

/* addrelated()    ******************************************************/

Boolean* addrelated(Boolean* related, const char* line)
{
  int i = 0, n_lines = 0;

  if (related == NULL || line == NULL) return (NULL);

  while (related[i++] != -1) n_lines++;

  if (substr(line, "$Father") == NULL)
    related[n_lines++] = FALSE;
  else
    related[n_lines++] = TRUE;

  if (n_lines % NALLOC == 0)
    related = (Boolean*)realloc(related, (n_lines + NALLOC) * sizeof(Boolean));
  related[n_lines] = -1;

  return (related);
}

/* add_entry()    ******************************************************/
/*
   Add an entry to a variable dictionary; display an error message if the
   entry is not new.
   */
Variable* add_entry(Variable* dictionary, Variable* newentry)
{
  Variable* variable;

  if (newentry == NULL) return (dictionary);

  for (variable = dictionary; variable; variable = variable->next) {
    if (0 == strcmp(newentry->func_name, variable->func_name) &&
        (interface == MOTIF || newentry->index != -1 && variable->index != -1)) {
      if (newentry->index == -1 && variable->index != -1) {
        fprintf(stderr, "*** Name conflict between:\n");
        fprintf(stderr, "\tmember %s of structure %s and \n", newentry->name, newentry->father);
        fprintf(stderr, "\tvariable %s", variable->name);
        if (variable->description->key) fprintf(stderr, " with key %s", variable->description->key);
        fprintf(stderr, "\n");
        parameters.error = TRUE;
      } else if (newentry->index != -1 && variable->index == -1) {
        fprintf(stderr, "*** Name conflict between:\n");
        fprintf(stderr, "\tmember %s of structure %s and \n", variable->name, variable->father);
        fprintf(stderr, "\tvariable %s", newentry->name);
        if (newentry->description->key) fprintf(stderr, " with key %s", newentry->description->key);
        fprintf(stderr, "\n");
        parameters.error = TRUE;
      } else if (newentry->index != -1 && variable->index != -1) {
        fprintf(stderr, "*** Name conflict between:\n");
        fprintf(stderr, "\tvariable %s", variable->name);
        if (variable->description->key) fprintf(stderr, " with key %s", variable->description->key);
        fprintf(stderr, " and\n");
        fprintf(stderr, "\tvariable %s", newentry->name);
        if (newentry->description->key) fprintf(stderr, " with key %s", newentry->description->key);
        fprintf(stderr, "\n");
        parameters.error = TRUE;
      }
    }
  }

  variable       = copy_variable(newentry);
  variable->next = dictionary;

  return (variable);
}

/* structure_named()  ******************************************************/

Structure* structure_named(char* name)
{
  extern Parameters parameters;
  Structure*        structure;

  if (name == NULL) return (NULL);

  structure = parameters.structure_list;
  while (structure) {
    if (0 == strcmp(structure->name, name)) return (structure);
    structure = structure->next;
  }

  return (NULL);
}

/* paramgrp_named()  ******************************************************/

Paramgrp* paramgrp_named(char* name)
{
  extern Parameters parameters;
  Paramgrp*         paramgrp;

  paramgrp = parameters.paramgrp_list;
  while (paramgrp) {
    if (0 == strcmp(paramgrp->name, name)) return (paramgrp);
    paramgrp = paramgrp->next;
  }

  return (NULL);
}

/* function_named()  ******************************************************/

Function* function_named(char* name)
{
  extern Parameters parameters;
  Function*         function;

  function = parameters.function_list;
  while (function) {
    if (0 == strcmp(function->name, name)) return (function);
    function = function->next;
  }

  return (NULL);
}

/* constant_value()  ******************************************************/
/*
   Returns TRUE if value is a contant; returns FALSE otherwise.
   */

Boolean constant_value(Value* value)
{
  if (value == NULL) return (TRUE);

  if (value->dependences != NULL) return (FALSE);

  switch (value->type) {
    case INUM:
    case BNUM:
    case FNUM:
    case CNUM:
    case STR: return (TRUE);
    default: return (FALSE);
  }
}

/* chartoint()    ******************************************************/
/*
   Converts a string with 'X' format to a string containning the corresponding
   ASCII integer value of the character between the quotes.
   */

char* chartoint(char* quoted)
{
  static char character, integer[5];
  int         octal;

  if (quoted == NULL) return (NULL);
  if (strlen(quoted) < 3) return (NULL);
  if (quoted[0] != '\'' || quoted[strlen(quoted) - 1] != '\'') return (NULL);

  if (0 == strcmp(quoted, "'\\\\'"))
    character = '\\';
  else if (0 == strcmp(quoted, "'\\\"'"))
    character = '"';
  else if (0 == strcmp(quoted, "'\\''"))
    character = '\'';
  else if (0 == strcmp(quoted, "'\\?'"))
    character = '?';
  else if (0 == strcmp(quoted, "'\\a'"))
    character = '\007';
  else if (0 == strcmp(quoted, "'\\b'"))
    character = '\b';
  else if (0 == strcmp(quoted, "'\\f'"))
    character = '\f';
  else if (0 == strcmp(quoted, "'\\n'"))
    character = '\n';
  else if (0 == strcmp(quoted, "'\\r'"))
    character = '\r';
  else if (0 == strcmp(quoted, "'\\t'"))
    character = '\t';
  else if (0 == strcmp(quoted, "'\\v'"))
    character = '\013';
  else if (0 == strncmp(quoted, "'\\x", 3) && strlen(quoted) > 4) {
    if (1 == sscanf(&quoted[3], "%x", &octal))
      character = (char)octal;
    else
      character = quoted[3];
  } else if (quoted[1] == '\\') {
    if (1 == sscanf(&quoted[2], "%o", &octal))
      character = (char)octal;
    else
      character = quoted[2];
  } else
    character = quoted[1];

  sprintf(integer, "%d", character);
  return (integer);
}

/* invalid_value()  ******************************************************/
/*
   Returns TRUE if value is invalid for the given type; returns FALSE
   otherwise;
   If type is FLOAT or DOUBLE and value is INUM, convert it to FNUM.
   For Windows/NT, replace / and \ by \\ in RFILE, WFILE, RWFILE, RDIR, WDIR
   and RWDIR.
   */

Boolean invalid_value(Value* value, int type)
{
  long inum, l;
#if (SYSTEME == WINNT)
  int   nslash;
  char *c, *n, *newdata;
#endif

  if (value == NULL) return (TRUE);

  if (value->type == IDENTIFIER || value->type == LVALUE || value->type == FUNCTION ||
      value->type == C_EXP)
    return (FALSE);

  switch (type) {
    case BYTE:
      if (value->type != INUM && value->type != INDEX) return (TRUE);
      sscanf(value->data, "%ld", &inum);
      if (inum < 0 || inum > 255) return (TRUE);
      break;
    case CHAR:
      if (value->type != INUM && value->type != INDEX) return (TRUE);
      sscanf(value->data, "%ld", &inum);
      if (inum < -128 || inum > 127) return (TRUE);
      break;
    case SHORT:
      if (value->type != INUM && value->type != INDEX) return (TRUE);
      sscanf(value->data, "%ld", &inum);
      if (inum < -32768 || inum > 32767) return (TRUE);
      break;
    case LONG:
    case INT:
      if (value->type != INUM && value->type != INDEX) return (TRUE);
      break;
    case BOOLEAN:
      if (value->type != BNUM) return (TRUE);
      break;
    case DOUBLE:
    case FLOAT:
      if (value->type == FNUM || value->type == INDEX)
        return (FALSE);
      else if (value->type == INUM) {
        value->data = (char*)realloc(value->data, 2 + strlen(value->data));
        strcat(value->data, ".");
        value->type = FNUM;
        return (FALSE);
      } else
        return (TRUE);
    case COMPLEX:
    case D_COMPLEX:
      if (value->type != CNUM) return (TRUE);
      break;
    case RDIR:
    case RWDIR:
    case WDIR:
    case RFILE:
    case RWFILE:
    case WFILE:
#if (SYSTEME == WINNT)
      if (value->data == NULL) return (TRUE);
      for (nslash = 0, c = value->data; *c; c++) {
        if (*c == '/' || *c == '\\') nslash++;
      }
      if (nslash > 0) {
        newdata = malloc(1 + nslash + strlen(value->data));
        for (n = newdata, c = value->data; *c; c++, n++) {
          if (*c == '/' || *c == '\\') {
            *n++ = '\\';
            *n   = '\\';
          } else
            *n = *c;
        }
        *n = '\000';
        free(value->data);
        value->data = newdata;
      }
#endif
    case STR:
      if (value->type != STR) return (TRUE);
      if (value->data == NULL) return (TRUE);
      l = strlen(value->data);
      if (l < 2) return (TRUE);
      if (value->data[0] != '"' || value->data[l - 1] != '"') return (TRUE);
      break;
  }

  return (FALSE);
}

/* cast_type()    ******************************************************/
/*
   Adds a cast to requested to the data of a value.
   */

void cast_type(Value* value, int type)
{
  int         length, type_length;
  char*       new_str;
  static char type_str[81];

  if (value == NULL || type < TYPE_BASE || type > TYPE_MAX) return;

  sprintf(type_str, "(%s)", Type_Names[type - TYPE_BASE]);
  type_length = strlen(type_str);

  if (0 == strncmp(type_str, value->data, type_length)) return; /* Already casted */

  length  = 5 + type_length + strlen(value->data);
  new_str = (char*)malloc(length);
  sprintf(new_str, "(%s)(%s)", Type_Names[type - TYPE_BASE], value->data);
  free(value->data);
  value->data = new_str;
}

/* match()    ******************************************************/
/*
   Returns TRUE if name and variable->match match.
   */

Boolean match(Variable* variable, char* name)
{
  int         nmatch;
  static char junk[2], string[257];

  if (variable == NULL) return (-1);
  if (variable->match == NULL) return (-1);
  if (variable->name == NULL) return (-1);
  if (name == NULL) return (-1);

  if (variable->description->key) {
    if (0 == strcmp(variable->description->key, name)) return (TRUE);
  } else {
    if (0 == strcmp(variable->name, name)) return (TRUE);
  }

  if (variable->description->storage != SCALAR) {
    if (variable->description->key)
      sprintf(string, "%s[PrMelem%d]", variable->description->key, variable->array_level);
    else
      sprintf(string, "%s[PrMelem%d]", variable->name, variable->array_level);
    if (0 == strcmp(string, name)) return (TRUE);
  }

  sprintf(string, "%s ", name);
  nmatch = sscanf(string, variable->match, junk);

  return (nmatch == 1);
}

/* variable_named()  ******************************************************/
/*
   Returns the variable in var_list whose name matches the name given as a
   parameter.
   If var_list is NULL, search in global variable vector.
   Returns NULL if no match.
   */

Variable* variable_named(Variable* var_list, char* name)
{
  Variable* variable;
  int       var;

  if (name == NULL) return (NULL);

  if (var_list) {
    variable = var_list;
    while (variable) {
      if (match(variable, name)) return (variable);
      variable = variable->next;
    }
  } else {
    for (var = 0; var < parameters.n_param; var++) {
      variable = parameters.param_vector[var];
      if (match(variable, name)) return (variable);
    }
  }

  return (NULL);
}

/* new_textbox_name()  ******************************************************/
/*
   Returns a new name for a textbox.
   */

char* new_textbox_name()
{
  char name[4];

  sprintf(name, "%.3d", parameters.n_textboxes++);
  return (newstr(name));
}

/* always()    ******************************************************/
/*
   Returns TRUE.
   */

Boolean always() { return (TRUE); }

/* never()    ******************************************************/
/*
   Returns FALSE.
   */
Boolean never() { return (FALSE); }
