include ../switches.def
include ../make.def

# Source and objects definition
SIM_SRC = \
main.cc \
sim_utils.cc \
openCARP_p.cc

BUILD_DIR = build
$(shell mkdir -p $(BUILD_DIR) >/dev/null)

SIM_OBJ = $(addprefix $(BUILD_DIR)/, $(notdir $(SIM_SRC:%.cc=%.o)))
SIM_DEP = $(SIM_OBJ:%.o=%.d)

CHECKS  = $(SIM_SRC:%.cc=%_chk)

# executable target
EXE = openCARP$(FLV)

MAIN_LIBS = \
$(NUM_DIR)/libnumerics$(FLV).a \
$(FEM_DIR)/libfem$(FLV).a \
$(PHYS_DIR)/libphysics$(FLV).a \
$(LIMPET_DIR)/liblimpet$(FLV).a


$(EXE): $(BUILD_DIR)/openCARP_p.o $(SIM_OBJ) $(MAIN_LIBS)
	@echo " * Linking binary " $@
	$(CXX) -o $(EXE) $(LDFLAGS) $(CXXFLAGS) $(SIM_OBJ) $(CVODE_LIBS) $(LIBS) $(FUNCPARSER_LIB) $(BASE_LIBS) $(OMP_LIB)

testfem: testfem.cc $(BUILD_DIR)/openCARP_p.o $(BUILD_DIR)/sim_utils.o $(MAIN_LIBS)
	@echo " * Linking binary " $@
	$(CXX) -o testfem $(LDFLAGS) $(CXXFLAGS) $< $(BUILD_DIR)/openCARP_p.o $(BUILD_DIR)/sim_utils.o  $(CVODE_LIBS) $(LIBS) $(FUNCPARSER_LIB) $(BASE_LIBS)

openCARP_p.h openCARP_d.h: openCARP.prm
	@echo " * parsing" $<
	$(CPP) $(CPPFLAGS) -traditional openCARP.prm | awk '!(/pragma/||/^# /)' | $(PRM) - openCARP -ext prm -ansi

build_info:
	@python build_info.py

$(BUILD_DIR)/openCARP_p.o: openCARP_p.h build_info
	@echo " * compiling openCARP_p.cc"
	$(CXX) -c $(PRM_INCS) $(CXXFLAGS) -O0 openCARP_p.cc -o $@

$(BUILD_DIR)/%.o: %.cc
	@echo " * compiling" $<
	$(CXX) -c $(CXXFLAGS) $< -o $@

%_chk: %.cc
	$(CHECKER) -analyze $< -- $(CHECKER_FLAGS)

clean:
	rm -rf $(BUILD_DIR)
	rm -f $(EXE)
	rm -f openCARP_[pd].h openCARP_p.cc

check: $(CHECKS)
	find . -name \*.plist -exec rm \{\} \;

format:
	$(FORMATTER) $(FORMATTER_FLAGS) *.cc *.h

all: $(EXE)

-include $(SIM_DEP)
