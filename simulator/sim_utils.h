// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file sim_utils.h
* @brief Simulator-level utility execution control functions.
* @author Aurel Neic, Gernot Plank, Edward Vigmond
* @version 
* @date 2019-10-25
*/

#ifndef _SIM_UTILS_H
#define _SIM_UTILS_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>

#include "Typedefs.h"
#include "sf_interface.h"
#include "timer_utils.h"
#include "physics_types.h"
#include "build_info.h"

#ifndef CARP_PARAMS
#define CARP_PARAMS
#include "openCARP_p.h"
#include "openCARP_d.h"
#endif

#include "IGBheader.h"

namespace opencarp {

/// The different output (directory) types
enum IO_t {INPUT, OUTPUT, POSTPROC, CURDIR};

/// tag regions types. must be in line with carp.prm
enum tagreg_t {tagreg_sphere = 1, tagreg_block = 2, tagreg_cylinder = 3, tagreg_list = 4};

namespace user_globals {
  /// Registry for the different scatter objects
  extern SF::scatter_registry<int> scatter_reg;
  /// Registry for the different meshes used in a multi-physics simulation
  extern std::map<mesh_t, sf_mesh> mesh_reg;
  /// Registriy for the inter domain mappings
  extern std::map<SF::quadruple<int>, SF::index_mapping<int> > map_reg;
  /// the physics
  extern std::map<physic_t, Basic_physic*> physics_reg;
  /// the timers manager
  extern timer_manager* tm_manager;
  /// important solution vectors from different physics
  extern std::map<datavec_t, sf_petsc_vec*> datavec_reg;
  /// petsc error are outputted to this FD
  extern FILE* petsc_error_fd;
  /// flag storing whether legacy stimuli are used
  extern bool using_legacy_stimuli;
}

/**
* @brief Initialize input parameters on a copy of the real command line parameters
*
* @param argc   The true parameter count.
* @param argv   The true cli parameters.
*/
void parse_params_cpy(int argc, char** argv);

/**
* @brief Register physics to the physics registry.
*/
void register_physics();
/**
* @brief Initialize all physics in the registry.
*/
void initialize_physics();
/**
* @brief Destroy all physics in the registry.
*/
void destroy_physics();

/**
* @brief Main simulate loop.
*/
void simulate();

/// do postprocessing
void post_process();

/**
* @brief Parse the phys_type CLI parameters and set up (empty) SF::meshdata meshes
*
* @post The mesh registry is filled with SF::meshdata structs associated to the
*       physics specified in the phys_region params.
*/
void parse_mesh_types();

/**
* @brief Read in the reference mesh and use its data to populate all meshes registered
*        in the mesh registry
*
* @post The meshes in the mesh registry are set up
*/
void setup_meshes();

/**
 *  Output the meshes specified by the user
 */
void output_meshes();

// flags to go with solution type
#define SOLV_METH(A,B) (B & A##_FLAG)
#define MONODOMAIN_FLAG   1
#define ITERATE_FLAG      2
#define CN_PARAB_FLAG     4
#define PURKINJE_FLAG     8
#define O2dT_PARAB_FLAG  16
#define PSEUDO_BIDM_FLAG 32

// simulation modes
#define MONODOMAIN         0
#define BIDOMAIN           1
#define PSEUDO_BIDM        2

// post processting options
// keep in jive with post_processing_opts in carp.prm
#define RECOVER_PHIE           1
#define OPTICAL_MAP            2
#define ACTIVATING_FUNCTION    4
#define AXIAL_CURRENTS         8
#define FILAMENTS             16
#define CURRENT_DENSITIES     32

// experiment defs, keep in jive with carp.prm
#define EXP_NORMAL       0
#define EXP_OUTPUT_FEM   1
#define EXP_LAPLACE      2
#define EXP_SETUP_MESH   3
#define EXP_POSTPROCESS  4

/** determine the solution methods
*
* \note We use the PrM variables
*
* \return integer with solution methods bits set
*/
unsigned int classify_soln_methods();

/**
* @brief Here we want to put all parameter checks, conversions and modifications that have been
*        littered throughout the codebase.
*
* Usualy people modify param globals next to the code that is affected by that change. This
* works well first, but after some time a lot of state is accumulated to the initially
* stateless functions. As such, the code works only if the order of function calls etc is
* not changed. In the long term it is better to accumulate all changes and checks of globals
* in specific functions so that the rest of the codebase can be programmed and used stateless.
*   -Aurel
*
*/
void check_and_convert_params();

/// set up error logs for PETSc, so that it doesnt print errors to stderr.
void setup_petsc_err_log();

/** set the I/O directories
 *
 * \param argc    \#words on command line
 * \param argv    the words
 * \param sim_ID  ID for naming output dir
 * \param pp_ID   ID for naming the postprocessing directory
 *
 * \pre  the output dir does not exist but all components in path up to it do
 *
 * \post output_dir is set to sim_ID, postproc_dir to pp_ID, and input_dir is set
 *       is set to the directory from which the executable was called
 *
 * \post the input parameter file is copied to the output directory
 */
void set_io_dirs(char *sim_ID, char *pp_ID, IO_t init);

/**
* @brief save the current working directory to curdir so that we can switch
*        back to it if needed.
*/
void update_cwd();
/** set the current working directory
 *
 * \param dest either INPUT, OUTPUT or POSTPROC
 *
 * \post the current working directory is changed
 */
int set_dir(IO_t dest);

/**
* @brief Here we set up the timers that we always want to have, independent of physics
*
* @param start_time Start time of the simulation
*/
void basic_timer_setup();

/** @brief plot experimental/simulation protocols by stepping through all timers
 *
 * \return  -1, if protocol output file opening failure
 */
int plot_protocols(const char *);

/**
* @brief Convinience function to get a physics.
*
* @param p  The physics type.
*
* @return    A pointer to the physics. NULL if physics is not present.
*/
Basic_physic* get_physics(physic_t p);

/// get (lowest) dimension of the mesh used in the experiment
short get_mesh_dim(mesh_t id);

/**
* @brief Register a data vector in the global registry
*
* @param dat  A pointer to the data vector.
* @param d    The identifier of the data vector.
*/
void register_data(sf_petsc_vec* dat, datavec_t d);

[[noreturn]] void cleanup_and_exit();

// return the direcotry path of a given file
char* get_file_dir(const char* file);
/**
* @brief Retrieve a petsc data vector from the data registry
*
* @param d  The identifier of the data vector.
*
* @return  A pointer to the data vector.
*/
sf_petsc_vec* get_data(datavec_t d);

//! for display execution progress and statistical data of electrical solve
struct prog_stats {
  double op;      //!< output period
  double start;   //!< output start wallclock time
  double last;    //!< last output wallclock time
  double curr;    //!< current output wallclock time
};

class igb_output_manager
{
  private:
  /**
   * Helper function to permute the parallel data back to canonical ordering and also do the
   * PetscReal -> float type converion into a buffer.
   */
  sf_petsc_vec* fill_output_buffer(const size_t idx);

  public:
  /// pointers to data registered for output
  SF::vector<sf_petsc_vec*> data;
  /// igb headers we use for output
  SF::vector<IGBheader>     igb;
  /// vector of data specs: the mesh ID and the data dpns
  SF::vector<SF::mixed_tuple<mesh_t, int>>              spec;
  /// map data spec -> PETSc vector buffer
  std::map<SF::mixed_tuple<mesh_t, int>, sf_petsc_vec*> buffmap;

  /**
  * @brief Register a data vector for output
  *
  * @param inp_data    Pointer to the data we want to output.
  * @param inp_meshid  ID of the mesh the data lives on.
  * @param dpn         d.o.f. per node used in the dataset.
  * @param name        Output filename.
  * @param unit        Units of the dataset.
  */
  void register_output(sf_petsc_vec* inp_data, const mesh_t inp_meshid,
                       const int dpn, const char* name, const char* units);

  /// write registered data to disk
  void write_data();

  /**
  * @brief Get the pointer to the igb header for a vector that
  *        was registered for output.
  *
  * @param vec Adress of petsc_vector that was registered for output.
  *
  * @return Get igb header pointer on success, else NULL.
  */
  IGBheader* get_igb_header(const sf_petsc_vec* vec);

  /// close file descriptors
  void close_files_and_cleanup();
};

/** output the command line and convert to a single parameter file
 *
 * This involves including any parameter files specified with +F
 * and stripping hyphens from command line options
 *
 * \param fname output file name
 * \param argc  number of arguments
 * \param argv  arguments
 *
 */
void output_parameter_file(const char *fname, int argc, char **argv);

/// save state and quit simulator
void savequit();


}  // namespace opencarp

#endif
