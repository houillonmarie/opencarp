// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file testfem.cc
* @brief openCARP main function
* @author Aurel Neic
* @version 
* @date 2019-10-25
*/

#include <cstdio>
#include <csignal>
#include <vector>

// we set PrMGLOBAL ONLY ONCE. this will initialize the global variables
// in only one compilation unit
#define PrMGLOBAL
#include "simulator.h"
#include "numerics.h"
#include "physics.h"
#include "fem.h"


// globals
namespace opencarp {
namespace user_globals {
  /// Registry for the different scatter objects
  SF::scatter_registry<mesh_int_t> scatter_reg;
  /// Registry for the different meshes used in a multi-physics simulation
  std::map<mesh_t, sf_mesh> mesh_reg;
  /// Registriy for the inter domain mappings
  std::map<SF::quadruple<int>, SF::index_mapping<int> > map_reg;
  /// the physics
  std::map<physic_t, Basic_physic*> physics_reg;
  /// a manager for the various physics timers
  timer_manager* tm_manager;
  /// important solution vectors from different physics
  std::map<datavec_t, sf_petsc_vec*> datavec_reg;
  /// file descriptor for petsc error output
  FILE* petsc_error_fd = NULL;
  /// flag storing whether legacy stimuli are used
  bool using_legacy_stimuli = false;
}  // namespace user_globals
}  // namespace opencarp

using namespace opencarp;

#undef  __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char** argv)
{
  char *DBfile   = NULL;  // could be used to pass in ~/.petscrc
  char *help_msg = NULL;
  parse_params_cpy(argc, argv);
  initialize_PETSc(&argc, argv, DBfile, help_msg);

  setup_petsc_err_log();
  check_and_convert_params();

  // set up meshes
  std::map<mesh_t, sf_mesh> & mesh_registry = user_globals::mesh_reg;
  mesh_registry[reference_msh] = sf_mesh();
  mesh_registry[reference_msh].comm = PETSC_COMM_WORLD;
  mesh_registry[extra_elec_msh] = sf_mesh();

  setup_meshes();

  MaterialType mtype[2];
  FILE_SPEC logger = f_open("electrics.log", param_globals::experiment != 4 ? "w" : "r");

  set_elec_tissue_properties(mtype, Electrics::extra_grid, logger);
  region_mask(intra_elec_msh, mtype[Electrics::extra_grid].regions,
              mtype[Electrics::extra_grid].regionIDs, true, "gregion_e");

  SF::vector<stimulus> stimuli;
  elliptic_solver ellip_solver;
  ellip_solver.init();
  ellip_solver.rebuild_matrices(mtype, stimuli, logger);

  cleanup_and_exit();
}

