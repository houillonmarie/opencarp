# use encoding=utf8
# ----------------------------------------------------------------------------
# openCARP is an open cardiac electrophysiology simulator.
#
# Copyright (C) 2020 openCARP project
#
# This program is licensed under the openCARP Academic Public License (APL)
# v1.0: You can use and redistribute it and/or modify it in non-commercial
# academic environments under the terms of APL as published by the openCARP
# project v1.0, or (at your option) any later version. Commercial use requires
# a commercial license (info@opencarp.org).
#
# This program is distributed without any warranty; see the openCARP APL for
# more details.
#
# You should have received a copy of the openCARP APL along with this program
# and can find it online: http://www.opencarp.org/license
# ----------------------------------------------------------------------------

"""
Generate a C header file defining some information about the build.
"""

import os
import sys
import subprocess

def run(cmd, env={}):
    """
    Run a command with subprocess and return the stdout.

    Avoiding use of subprocess.check_output to maintain backwards
    compatability.
    """
    proc = subprocess.Popen(cmd, env=env, stdout=subprocess.PIPE)
    out = proc.communicate()
    if proc.returncode != 0:
        tpl = 'execution of command "{0}" failed'
        raise Exception(tpl.format(' '.join(cmd)))
    return out[0]

def locale():
    """
    Determine the locale to use in SVN.
    """

    de = None

    for line in run(['locale', '-a']).split('\n'):

        entry = line.strip()

        if entry.startswith('en'):
            return entry

        if entry.startswith('de'):
            de = entry

    # German a good fallback as 'revision' and 'relative url' are the same
    if de is not None:
        return de

    # If all else fails
    return 'C'

SVN_NAME_MAPPING = {'Révision': 'Revision'}

def svn(directory='.'):
    """
    Get information about the SVN repository.

    Uses the --show-item syntax instead of parsing the normal svn info output
    to avoid issues with language locales.
    """

    start_wd = os.getcwd()

    os.chdir(directory)
    result = run(['svn', 'info'], env={'LC_MESSAGES': locale()})
    os.chdir(start_wd)

    info = {}

    for line in result.split('\n'):

        try:
            name, value = line.split(': ')
        except ValueError:
            continue

        # Translate where necessary
        name = SVN_NAME_MAPPING.get(name.strip(), name.strip())
        name = name.lower().replace(' ', '-')

        info[name] = value.strip()

        if name == 'revision':
            info['commit'] = info[name]

    return info

def git(directory='.'):
    start_wd = os.getcwd()
    os.chdir(directory)
    info = {}

    # get the git revision count
    result = run(['git', 'rev-list', '--all', '--count'])
    info['revision'] = str(int(result));

    # get commit hash
    result = run(['git', 'rev-parse', 'HEAD'])
    result = result[0:len(result)-1]
    info['commit'] = result.decode('ascii')

    # get the git tag
    result = run(['git', 'describe', '--tags', '--always'])
    result = result[0:len(result)-1]
    info['tag'] = result.decode('ascii')

    # get the git remote url
    result = run(['git', 'remote', '-v'])

    for line in result.decode('ascii').split('\n'):
        line = ' '.join(line.split())

        try:
            alias, path, direction = line.split(' ')
        except ValueError:
            continue

        if alias == 'origin':
            info['url'] = path

    # the subrepo is just '.'
    info['subrepo'] = '.'

    os.chdir(start_wd)

    return info



TEMPLATE = """#ifndef __BUILD_INFO__
#define __BUILD_INFO__

#define GIT_COMMIT_TAG       "{tag}"
#define GIT_COMMIT_HASH      "{commit}"
#define GIT_COMMIT_COUNT     {revision}
#define GIT_PATH             "{url}"

#define SUBREPO_REVISIONS "{subrepo}"

#define SUBREPO_COMMITS "{subrepocommit}"

#endif
"""

def generate():

    info = git(os.path.dirname(os.path.abspath(__file__)))

    # Get subrepo info
    repo_versions = []
    repo_commits  = []

    # we currently have no sub-repos. still, we leave the mechanism in here for later

    # repo_versions.append(('slimfem',       git('../fem/slimfem')['revision']))
    # repo_commits.append(( 'slimfem',       git('../fem/slimfem')['commit']))

    info['subrepo'] = ','.join(['{0[0]}={0[1]}'.format(v) for v in repo_versions])

    # Assemble commit hashes / revisions
    newline = '," \\\n' + ' ' * 24 + '"'
    info['subrepocommit'] = newline.join(['{0[0]}={0[1]}'.format(v) for v in repo_commits])

    return TEMPLATE.format(**info)

def print_build_info(filename):
    """
    print build info
    """
    git_info = generate()
    update_file = True

    # check if there's a change of the build info if the file already exists
    if os.path.isfile(filename):
        with open(filename, 'r') as myfile:
            info_h = myfile.read()
        if info_h == git_info:
           update_file = False

    # write build info file
    if update_file:
        f = open(filename, "w")
        f.write(git_info)

if __name__ == '__main__':
    if(len(sys.argv) > 1):
        filename = sys.argv[1]
    else:
        filename = 'build_info.h'    
    print_build_info(filename)