	<struct-ref name="Stim">
		<param-ref name="crct" type= "(null)" >
			<description> "Definition of setup and wiring of stimulation circuit"</description>
			<relation to-name= "$Father"  type="require" />
		</param-ref>
		<param-ref name="elec" type= "(null)" >
			<description> "Definition of electrode geometry"</description>
			<relation to-name= "$Father"  type="require" />
		</param-ref>
		<param-ref name="ptcl" type= "(null)" >
			<description> "Definition of stimulation protocol"</description>
			<relation to-name= "$Father"  type="require" />
		</param-ref>
		<param-ref name="pulse" type= "(null)" >
			<description> "Definition of stimulation pulse"</description>
			<relation to-name= "$Father"  type="require" />
		</param-ref>
		<param name="name" type= "String"  default= "(String)(\"\")" >
			<description> "Definition of the label for a single electrode"</description>
			<relation to-name= "$Father"  type="require" />
		</param>
	</struct-ref>
	
