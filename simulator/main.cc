// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file main.cc
* @brief openCARP main function
* @author Aurel Neic
* @version 
* @date 2019-10-25
*/

#include <cstdio>
#include <csignal>
#include <vector>

// we set PrMGLOBAL ONLY ONCE. this will initialize the global variables
// in only one compilation unit
#define PrMGLOBAL
#include "simulator.h"
#include "numerics.h"
#include "fem.h"


// globals
namespace opencarp {
namespace user_globals {
  /// Registry for the different scatter objects
  SF::scatter_registry<mesh_int_t> scatter_reg;
  /// Registry for the different meshes used in a multi-physics simulation
  std::map<mesh_t, sf_mesh> mesh_reg;
  /// Registriy for the inter domain mappings
  std::map<SF::quadruple<int>, SF::index_mapping<int> > map_reg;
  /// the physics
  std::map<physic_t, Basic_physic*> physics_reg;
  /// a manager for the various physics timers
  timer_manager* tm_manager;
  /// important solution vectors from different physics
  std::map<datavec_t, sf_petsc_vec*> datavec_reg;
  /// file descriptor for petsc error output
  FILE* petsc_error_fd = NULL;
  /// flag storing whether legacy stimuli are used
  bool using_legacy_stimuli = false;
}  // namespace user_globals
}  // namespace opencarp

using namespace opencarp;

#undef  __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char** argv)
{
// #ifdef __osf__
//   signal( SIGFPE, SIG_IGN );
// #endif
//   signal( SIGHUP,  savequit );
//   signal( SIGINT,  savequit );
//   signal( SIGQUIT, savequit );
//   signal( SIGIO,   savequit );

  parse_params_cpy(argc, argv);

  char *DBfile   = NULL;  // could be used to pass in ~/.petscrc
  char *help_msg = NULL;
  initialize_PETSc(&argc, argv, DBfile, help_msg);

  // set up IO dirs, this should be the first call after initialize_PETSc, so that
  // all other functions have working IO dirs
  set_io_dirs(param_globals::simID, param_globals::ppID, OUTPUT);
  output_parameter_file("parameters.par", argc, argv);

  setup_petsc_err_log();

  // accumulate check and conversions of param globals in here, so that
  // the rest of the codebase can remain stateless
  check_and_convert_params();

  // set up meshes
  parse_mesh_types();
  setup_meshes();
  output_meshes();

  // for experiment EXP_SETUP_MESH we quit after the mesh output step
  if(param_globals::experiment == EXP_SETUP_MESH)
    cleanup_and_exit();

  // convert input to internal units
  basic_timer_setup();

  // register and initialize physics
  register_physics();
  initialize_physics();

  // for experiment EXP_OUTPUT_FEM we quit after the electrics initialization
  if(param_globals::experiment == EXP_OUTPUT_FEM)
    cleanup_and_exit();

  // simulate
  if(param_globals::experiment != EXP_POSTPROCESS)
    simulate();

  post_process();

  cleanup_and_exit();
}

