// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "iir.h"

class IIR_Filt {
    public:
        IIR_Filt( ){};
        virtual ~IIR_Filt(){free(_c);free(_d);}
template<class T, class U>
        void apply(T*, U*, int);
    protected:
        int    *_c;
        long double *_d;
        int     _n;
        long double  _sf;
};

template<class T,class U>
void IIR_Filt::apply( T *x, U *y, int sz )
{
  for( int i=0; i<_n; i++ ) 
    y[i] = 0;

  for( int i=_n; i<sz; i++ ) {
    long double  sum = _sf*_c[0]*x[i];
    for( int j=1; j<_n; j++ ) {
      sum += x[i-j]*_sf*_c[j] - _d[j]*y[i-j];
    }
    y[i] = sum;
  }
}


class Butterworth : public IIR_Filt {
    public : 
        Butterworth( int, double, double h=0 );
};

void filter_all( double **in, int nr, IIR_Filt *filt, double *out, int t );

void zero_avg( double *x, int n );

