## Installation of openCARP Docker containers

First, install [Docker](https://docs.docker.com/get-started/) for your platform.

There are 2 options for the openCARP installation using Docker.

1. After the Docker installation use docker commands to pull and use the docker images.
2. Alternatively, install using our script which will take care of all installation steps for you.

### Pull the docker image (Option 1)

Pull the latest openCARP docker image:

    docker pull docker.opencarp.org/opencarp/opencarp:latest

Run the docker image using the following terminal command:

    docker run -it docker.opencarp.org/opencarp/opencarp:latest

You can now use openCARP. Head over to the [experiments](https://opencarp.org/documentation/examples) section to get started.

### Install our docker script (Option 2)

Docker provides a variety of commands to utilize docker images:

    docker --help

To help you quick start, our script wraps the most commonly used docker commands.
Tell the install script where to place the binary files (should be a diretory in your `$PATH`). Use `sudo` if admin permission is required to write there:

    cd <path-to-your-codebase>/openCARP/docker
    ./install_script.sh <path-to-your-bin>

For example, to install to `/usr/local/bin`, run:

    sudo ./install_script.sh /usr/local/bin

The script enables you to start with the keyword `opencarp-docker` followed by the appropriate command.

Make sure the script is installed correctly:

    opencarp-docker help

Pull the docker image:

    opencarp-docker pull

For usage examples and more information, please refer to our [docker/README](https://git.opencarp.org/openCARP/openCARP/blob/master/docker/README.md).

For visualization, please also refer to [installation of meshalyzer](https://opencarp.org/download/installation#meshalyzer) and locally install meshalyzer because it is not included in the Docker container.
