# Docker for openCARP

## What is shipped

The docker image contains openCARP ecosystem with the required dependencies:
* [openCARP](https://git.opencarp.org/openCARP/openCARP)
* [carputils](https://git.opencarp.org/openCARP/carputils)
* [experiments](https://git.opencarp.org/openCARP/experiments)
* [meshtool](https://bitbucket.org/aneic/meshtool/src/master/)

## Installation

See [INSTALL.md](https://git.opencarp.org/openCARP/openCARP/blob/master/docker/INSTALL.md)

## Usage

The script enables you to start with the keyword `opencarp-docker`, and followed by different commands.

You can run all openCARP programs, tools, and python codes using carputils in your working directories:

    opencarp-docker bench -help
    opencarp-docker openCARP
    opencarp-docker python <your_python_code>

You can also start a openCARP shell (based on Ubuntu 18.04) in your working directory:

    opencarp-docker shell

## For developers

The script also provides you some developer commands.

If you want to build the docker image by yourself, enter the codebase directory `openCARP` and then build it:

    cd <path-to-your-codebase>/openCARP
    opencarp-docker build

You can run all required tools for openCARP development in your working directories:

    opencarp-docker make
    opencarp-docker cmake
    opencarp-docker clang-format <source_file>

For example, enter the codebase directory `openCARP`, use cmake to compile and then run the openCARP:

    cd <path-to-your-codebase>/openCARP
    opencarp-docker cmake -S. -B_build
    opencarp-docker cmake --build _build
    opencarp-docker _build/bin/openCARP

You can also start a shell (based on Ubuntu 18.04) in the codebase directory `openCARP` and then run the tools:

    cd <path-to-your-codebase>/openCARP
    opencarp-docker shell
    cmake -S. -B_build
    cmake --build _build
    _build/bin/openCARP

## More docker commands

Our script provides a simpler way to utilize the docker images, but if you prefer, you can type docker commands by yourself.
In the following, we list some docker commands which would be useful for you.

Pull the docker images:

    docker pull docker.opencarp.org/opencarp/opencarp:latest

On the other hand, you can build your own docker images based on the Dockerfiles.
Enter the codebase directory `openCARP`:

    cd <path-to-your-codebase>/openCARP
    docker build -t opencarp . -f docker/Dockerfile

Show docker images in your machine:

    docker images

Run docker shell in an isolated container:

    docker run -it opencarp

Run docker shell with mounting files in your machine:

    docker run -it -v /path/in/your/machine:/path/in/container opencarp

Show running docker container information (including containerID):

    docker ps

Copy files between the docker container and your machine:

    docker cp <containerID>:/path/in/container/source /path/in/your/machine/target
    docker cp /path/in/your/machine/source <containerID>:/path/in/container/target

Clean dangling docker stuff:

    docker system prune
