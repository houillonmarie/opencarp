#!/bin/bash
# install opencarp docker script

PARENT_PATH=$( cd $(dirname $BASH_SOURCE) ; pwd -P )

DOCKER_SCRIPT_ABBR="opencarp-docker"

if [ -z "$1" ]; then
    echo ""
    echo "./install_script.sh <path-to-your-bin>"
    echo ""
    echo "  <path-to-your-bin>: path to your binary files (e.g. /usr/local/bin)"
    echo ""
    exit
else
    INST_DIR=$1
fi

BASH_FILE_NAME=$DOCKER_SCRIPT_ABBR

cp $PARENT_PATH/docker_script.sh $BASH_FILE_NAME
chmod a+rx $BASH_FILE_NAME
mv $BASH_FILE_NAME $INST_DIR
echo ""

if [ -f $INST_DIR/$BASH_FILE_NAME ]; then
    echo "Successfully installed the $BASH_FILE_NAME docker script in $INST_DIR"
    $BASH_FILE_NAME
else
    echo "Failed. Please make sure you have permission to access $INST_DIR. (use sudo)"
    echo "You can also manually copy the $BASH_FILE_NAME script file to your path for binary files"
fi
