#!/bin/bash
# opencarp docker script

DOCKER_SCRIPT_ABBR="opencarp-docker"

DOCKER_REGISTRY="docker.opencarp.org/opencarp"
DOCKER_IMAGE="opencarp:latest"
DOCKER_IMAGE_DEPS="opencarp/deps:latest"

DOCKER_FILE="docker/Dockerfile"
DOCKER_FILE_DEPS="docker/Dockerfile-deps"

BASH_FILE_NAME=$(basename $0)
if [ $BASH_FILE_NAME != $DOCKER_SCRIPT_ABBR ]; then
    echo "ERROR: Unexpected bash file name (expected: $DOCKER_SCRIPT_ABBR)" && exit
fi

function print_help () {
    echo ""
    echo "Usage: $BASH_FILE_NAME COMMAND"
    echo ""
    echo "Docker script for the openCARP"
    echo ""
    echo "Commands:"
    echo "  build           Build the $DOCKER_IMAGE docker image (only works in the codebase root directory: openCARP)"
    echo "  help            Show this help information"
    echo "  pull            Pull the $DOCKER_IMAGE docker image"
    echo "  shell           Start the $DOCKER_IMAGE shell in the working directory"
    echo ""
    echo "Executable Commands:"
    echo "  bench                  Run bench"
    echo "  igbapd                 Run igbapd"
    echo "  igbextract             Run igbextract"
    echo "  igbhead                Run igbhead"
    echo "  igbops                 Run igbops"
    echo "  limpet_fe.py           Run limpet_fe.py"
    echo "  make_dynamic_model.sh  Run make_dynamic_model.sh"
    echo "  mesher                 Run mesher"
    echo "  meshtool               Run meshtool"
    echo "  openCARP               Run openCARP"
    echo "  python                 Run your python code"
    echo ""
    echo "Developer Commands:"
    echo "  clang-check     Run clang-check"
    echo "  clang-format    Run clang-format"
    echo "  cmake           Run cmake"
    echo "  make            Run make"
    echo "  (built_bin)     Run the built binary files (e.g. bin/openCARP)"
    echo ""
}

function docker_build () {
    case "$1" in
        "")     docker build -t $DOCKER_IMAGE_DEPS . -f $DOCKER_FILE_DEPS
                docker build -t $DOCKER_IMAGE . -f $DOCKER_FILE
            ;;
        "deps") docker build -t $DOCKER_IMAGE_DEPS . -f $DOCKER_FILE_DEPS
            ;;
        "dist") docker build -t $DOCKER_IMAGE . -f $DOCKER_FILE
            ;;
        *) echo "The build command only works in the codebase root directory"
           echo "Build targets: deps and dist" && exit
    esac
}

function docker_pull () {
    docker pull $1
}

function check_mountable_working_dir () {
    if [ -w .. ]; then
        echo "Start running docker..."
    else
        echo "ERROR: Running docker is denied (The working directory is NOT ALLOWED to be mounted to the docker container)" && exit
    fi
}

function docker_run_program () {
    check_mountable_working_dir
    docker run --rm --user="$(id -u):$(id -g)" --volume="$(pwd):/shared:z" --workdir="/shared" $1 "${@:2}"
}

function docker_run_shell () {
    check_mountable_working_dir
    docker run -it --rm --user="$(id -u):$(id -g)" --volume="$(pwd):/shared:z" --workdir="/shared" $1
}

case "$1" in
    ""|"help") print_help
        ;;
    "build")   docker_build $2
        ;;
    "pull")    docker_pull $DOCKER_REGISTRY/$DOCKER_IMAGE
        ;;
    "shell")   docker_run_shell $DOCKER_REGISTRY/$DOCKER_IMAGE
        ;;
    *)         docker_run_program $DOCKER_REGISTRY/$DOCKER_IMAGE $@
esac
