// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file numerics.h
* @brief Top-level header file of numerics module.
* @author Aurel Neic
* @version 
* @date 2019-10-25
*/

#ifndef _NUMERICS_H
#define _NUMERICS_H

#include "petsc_compat.h"
#include "petsc_utils.h"
#include "timer_utils.h"
#include "basics.h"
#include "signals.h"
#include "solver_utils.h"
#include "short_float.h"
#include "IGBheader.h"

#endif
