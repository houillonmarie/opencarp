// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file petsc_utils.cc
* @brief Basic PETSc utilities.
* @author Gernot Plank, Edward Vigmond
* @version 
* @date 2019-10-25
*/

#include "petsc_utils.h"
#include "basics.h"

namespace opencarp {

void initialize_PETSc(int *argc, char **argv, char *rc, char *help_msg)
{
  // find indicator of PETSc options
  int PetOptInd=1;
  while ( PetOptInd<*argc && strcmp(argv[PetOptInd],DELIMIT_OPTS) )
    PetOptInd++;

  int  numPetOpt = *argc - PetOptInd;
  *argc -= numPetOpt;
  if( !numPetOpt )
    numPetOpt = 1;

  // since we pass this array to PETSc and it will be modified there, we dont go C++ here
  char **petArg = (char**) malloc( (numPetOpt)*sizeof(char *) );
  petArg[0] = dupstr( argv[0] );
  for( int i=1; i<numPetOpt; i++ )
    petArg[i] = dupstr( argv[PetOptInd+i] );

  // only send PETSc the PETSc specific options
  PetscInitialize(&numPetOpt, &petArg, rc, help_msg);

  for(int i=0; i<numPetOpt; i++) free(petArg[i]);
  free(petArg);
}

char* Petscgetline(FILE * f)
{
  size_t size = 0;
  size_t len  = 0;
  size_t last = 0;
  char * buf  = PETSC_NULL;

  if (feof(f)) return 0;
  do {
    size += 1024; /* BUFSIZ is defined as "the optimal read size for this platform" */
    buf = (char*)realloc((void *)buf,size); /* realloc(NULL,n) is the same as malloc(n) */
    /* Actually do the read. Note that fgets puts a terminal '\0' on the
    end of the string, so we make sure we overwrite this */
    if (!fgets(buf+len,size,f)) buf[len]=0;
    PetscStrlen(buf,&len);
    last = len - 1;
  } while (!feof(f) && buf[last] != '\n' && buf[last] != '\r');
  if (len) return buf;
  free(buf);
  return 0;
}

PetscErrorCode PetscOptionsClearFromString(const char in_str[])
{
  char           *first,*second;
  PetscToken     token;
  COMPAT_PetscTruth     key;

  PetscTokenCreate(in_str,' ',&token);
  PetscTokenFind(token,&first);
  while (first) {
    PetscOptionsValidKey(first,&key);
    if (key) {
      PetscTokenFind(token,&second);
      PetscOptionsValidKey(second,&key);
      if (!key) {
        COMPAT_PetscOptionsClearValue(first);
        PetscTokenFind(token,&first);
      } else {
        COMPAT_PetscOptionsClearValue(first);
        first = second;
      }
    } else {
      PetscTokenFind(token,&first);
    }
  }
  COMPAT_PetscTokenDestroy(token);
  return(0);
}


PetscErrorCode PetscOptionsClearFromFile(MPI_Comm comm, const char* file)
{
  char           *string,fname[PETSC_MAX_PATH_LEN],*first,*second,*third,*vstring = 0,*astring = 0;
  PetscErrorCode ierr;
  size_t         i,len;
  FILE           *fd;
  PetscToken     token;
  int            err;
  char           cmt[3]={'#','!','%'},*cmatch;
  PetscMPIInt    rank,cnt=0,acnt=0;

  PetscFunctionBegin;
  ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);
  if (!rank) {
    /* Warning: assume a maximum size for all options in a string */
    ierr = PetscMalloc(128000*sizeof(char),&vstring);CHKERRQ(ierr);
    vstring[0] = 0;
    ierr = PetscMalloc(64000*sizeof(char),&astring);CHKERRQ(ierr);
    astring[0] = 0;
    cnt     = 0;
    acnt    = 0;

    ierr = PetscFixFilename(file,fname);CHKERRQ(ierr);
    fd   = fopen(fname,"r");
    if (fd) {
      while ((string = Petscgetline(fd))) {
        /* eliminate comments from each line */
        for (i=0; i<3; i++){
          ierr = PetscStrchr(string,cmt[i],&cmatch); CHKERRQ(ierr);
          if (cmatch) *cmatch = 0;
        }
        ierr = PetscStrlen(string,&len); CHKERRQ(ierr);
        /* replace tabs, ^M, \n with " " */
        for (i=0; i<len; i++) {
          if (string[i] == '\t' || string[i] == '\r' || string[i] == '\n') {
            string[i] = ' ';
          }
        }
        ierr = PetscTokenCreate(string,' ',&token); CHKERRQ(ierr);

        // temporary fix, do not remove mat_pastix_* options
        void *pastix_check   = strstr(string,"-mat_pastix_check");
        void *pastix_verbose = strstr(string,"-mat_pastix_verbose");
        if(pastix_check || pastix_verbose)  {
          strcpy(string,"-ignore 0");
          ierr = PetscTokenCreate(string,' ',&token); CHKERRQ(ierr);
        }
        ierr = PetscTokenFind(token,&first); CHKERRQ(ierr);

        if (!first) {
          goto destroy;
        } else if (!first[0]) { /* if first token is empty spaces, redo first token */
          ierr = PetscTokenFind(token,&first);CHKERRQ(ierr);
        }
        ierr = PetscTokenFind(token,&second); CHKERRQ(ierr);
        if (!first) {
          goto destroy;
        }
        else if (first[0] == '-') {
          /* warning: should be making sure we do not overfill vstring */
          ierr = PetscStrcat(vstring,first);CHKERRQ(ierr);
          ierr = PetscStrcat(vstring," ");CHKERRQ(ierr);
          if (second) {
            /* protect second with quotes in case it contains strings */
            ierr = PetscStrcat(vstring,"\"");CHKERRQ(ierr);
            ierr = PetscStrcat(vstring,second);CHKERRQ(ierr);
            ierr = PetscStrcat(vstring,"\"");CHKERRQ(ierr);
          }
          ierr = PetscStrcat(vstring," ");CHKERRQ(ierr);
        }
        else {
          COMPAT_PetscTruth match;

          ierr = PetscStrcasecmp(first,"alias",&match);CHKERRQ(ierr);
          if (match) {
            ierr = PetscTokenFind(token,&third);CHKERRQ(ierr);
            if (!third) COMPAT_SETERRQ1(comm,PETSC_ERR_ARG_WRONG,"Error in options file:alias missing (%s)",second);
            ierr = PetscStrcat(astring,second);CHKERRQ(ierr);
            ierr = PetscStrcat(astring," ");CHKERRQ(ierr);
            ierr = PetscStrcat(astring,third);CHKERRQ(ierr);
            ierr = PetscStrcat(astring," ");CHKERRQ(ierr);
          }
          else {
            COMPAT_SETERRQ1(comm,PETSC_ERR_ARG_WRONG,"Unknown statement in options file: (%s)",string);
          }
        }
destroy:
        free(string);
        ierr = COMPAT_PetscTokenDestroy(token);CHKERRQ(ierr);
      }

      err = fclose(fd);
      if (err) COMPAT_SETERRQ(comm,PETSC_ERR_SYS,"fclose() failed on file");
      ierr = PetscStrlen(astring,&len);CHKERRQ(ierr);
      COMPAT_PetscMPIIntCast(len,acnt);CHKERRQ(ierr);
      ierr = PetscStrlen(vstring,&len);CHKERRQ(ierr);
      COMPAT_PetscMPIIntCast(len,cnt);CHKERRQ(ierr);
    }
  }

  ierr = MPI_Bcast(&acnt,1,MPIU_INT,0,comm);CHKERRQ(ierr);
  if (acnt) {
    //PetscToken token;
    //char       *first,*second;

    if (rank) {
      ierr = PetscMalloc((acnt+1)*sizeof(char),&astring);CHKERRQ(ierr);
    }
    ierr = MPI_Bcast(astring,acnt,MPI_CHAR,0,comm);CHKERRQ(ierr);
    astring[acnt] = 0;
    ierr = PetscTokenCreate(astring,' ',&token);CHKERRQ(ierr);
    ierr = PetscTokenFind(token,&first);CHKERRQ(ierr);
    while (first) {
      ierr = PetscTokenFind(token,&second);CHKERRQ(ierr);
      ierr = COMPAT_PetscOptionsSetAlias(first,second);CHKERRQ(ierr);
      ierr = PetscTokenFind(token,&first);CHKERRQ(ierr);
    }
    ierr = COMPAT_PetscTokenDestroy(token);CHKERRQ(ierr);
  }

  ierr = MPI_Bcast(&cnt,1,MPIU_INT,0,comm);CHKERRQ(ierr);
  if (cnt) {
    if (rank) {
      ierr = PetscMalloc((cnt+1)*sizeof(char),&vstring);CHKERRQ(ierr);
    }
    ierr = MPI_Bcast(vstring,cnt,MPI_CHAR,0,comm);CHKERRQ(ierr);
    vstring[cnt] = 0;
    ierr = PetscOptionsClearFromString(vstring); CHKERRQ(ierr);
  }
  ierr = PetscFree(astring);CHKERRQ(ierr);
  ierr = PetscFree(vstring);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

const char* petsc_get_converged_reason_str(int reason)
{
  const char* ret = "";

  switch(reason) {
    case  0  : ret = "iterating"; break;
    case  1  : ret = "relative tolerance normal"; break;
    case  2  : ret = "relative tolerance"; break;
    case  3  : ret = "absolute tolerance"; break;
    case  4  : ret = "iterations"; break;
    case  5  : ret = "cg neg curve"; break;
    case  6  : ret = "cg constrained"; break;
    case  7  : ret = "step length"; break;
    case  8  : ret = "happy breakdown"; break;
    case  9  : ret = "atol normal"; break;
    case -2  : ret = "null error"; break;
    case -3  : ret = "iterations exceeded"; break;
    case -4  : ret = "tolerance failed"; break;
    case -5  : ret = "breakdown"; break;
    case -6  : ret = "breakdown bicg"; break;
    case -7  : ret = "nonsymmetric where symmetry required"; break;
    case -8  : ret = "indefinite preconditioning"; break;
    case -9  : ret = "NaN or inf encountered"; break;
    case -10 : ret = "indefinite system matrix"; break;
    case -11 : ret = "preconditioning failed"; break;

    default: ret = "unknown";
  }

  return ret;
}

}  // namespace opencarp

