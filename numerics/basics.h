// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file basics.h
* @brief Basic utility structs and functions, mostly IO related.
* @author Aurel Neic, Edward Vigmond, Gernot Plank
* @version 
* @date 2019-10-25
*/

#ifndef _BASICS_H
#define _BASICS_H

#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <cstring>
#include <cassert>
#include <string>
#include <vector>
#include <map>
#include <unordered_set>

#include <unistd.h>
#include <sys/stat.h>
#include <mpi.h>

#include "vect.h"
#include "petsc_utils.h"
#include "SF_vector.h"

#ifndef CARP_PARAMS
#define CARP_PARAMS
#include "openCARP_p.h"
#include "openCARP_d.h"
#endif

#define STRUCT_ZERO(S) memset(&(S), 0, sizeof(S))
#define SLIST_APPEND( S, P ) sltlst_append( S, &(P), sizeof(P) )

namespace opencarp {

// This datatypes will be used in conjunction with mesh data. We might want to
// have increased / reduced precision when storing meshes, mesh related numberings, etc.
// Therefore, we will use (mesh_int_t,mesh_real_t) for data fields that scale with the mesh size,
// to be able to tweak the used datatype later. -Aurel
typedef int   mesh_int_t;
typedef float mesh_real_t;

/// saltatory list -- memory is allocated in chunks
struct Salt_list {
  void *data;    //!< the buffer
  int   chunk;   //!< allocate memory to hold this many items
  int   nitems;  //!< number of items
  int   size;    //!< size of list items
};

/** append to a list, allocating more memory if necessary
 *
 *  \param sl      the list
 *  \param p       item to add
 *  \param quantum size of items in bytes
 *
 *  \note If the chunk size is 0, it will not work so the chunk size will be set
 *        to 1 automatically.
 */
void sltlst_append(Salt_list* sl, void *p, int quantum);

/** duplicates a string
 *
 * \param old_str string that needs to be duplicated
 *
 * \return duplicated string
 */
char* dupstr(const char* old_str);

/** convert a double into a string
*
* \param r the number
*
* \return a pointer to the string
*/
char *stringify(double r);

std::string get_basename(const std::string & path);

/**
* @brief Split a string holding a character-seperated list into a vector of strings
*
* @tparam STRVEC  vector-of-string type, e.g. std::vector<std::string> or SF::vector<std::string>

* @param [in]  input    string holding list
* @param [in]  s        character delimiting the list entries
* @param [out] list     the parsed list
*/
template<class STRVEC>
void split_string(const std::string & input, const char s, STRVEC & list)
{
  size_t fnd = 0, ofnd = 0;
  int num_parts = 1, idx = 0;

  fnd = input.find(s);
  while (fnd != std::string::npos)
  {
    num_parts++;
    ofnd = fnd+1;
    fnd = input.find(s, ofnd);
  }

  list.resize(num_parts);

  fnd = input.find(s); ofnd = 0;
  while (fnd != std::string::npos)
  {
    list[idx++].assign(input.begin() + ofnd, input.begin() + fnd);
    ofnd = fnd+1;
    fnd = input.find(s, ofnd);
  }

  if(ofnd < input.size())
    list[idx].assign(input.begin() + ofnd, input.end());
}

/**
* @brief File descriptor struct
*/
struct file_desc {
  FILE* fd;
  const char* name;
};

typedef file_desc* FILE_SPEC;

/** check existence of file
 *
 * \param [in] fname  name of file
 *
 * \return true, if file exists
 *         false, otherwise
 */
bool f_exist(const char *fname);

/**
* @brief Open a FILE_SPEC
*
* @param fname  File name
* @param mode   Mode for fopen()
*
* @return The new FILE_SPEC pointer
*/
FILE_SPEC f_open(const char *fname, const char *mode);

/**
* @brief Close a FILE_SPEC
*
* @param f the FILE_SPEC to close;
*/
void f_close(FILE_SPEC & f);

/**
* @brief Parallel fread. Root reads, then broadcasts.
*
* @param ptr     Pointer to the buffer we read into
* @param size    Data type size_t
* @param nmemb   Number of data members to read
* @param stream  FILE_SPEC to use.
* @param comm    The MPI communicator to use.
*/
void f_read_par(void *ptr, size_t size, size_t nmemb, FILE_SPEC stream, MPI_Comm comm = PETSC_COMM_WORLD);

/**
* @brief Write in parallel. Data comes from one rank, rank 0 writes.
*
* @param ptr          Buffer to write. Needs only be allocated on rank source_pid.
* @param size         Size of data type.
* @param nmemb        Number of data members. Needs only be set on rank source_pid.
* @param source_pid   The PID of the rank the data is on.
* @param stream       The file we write to. Needs only be valid rank 0.
* @param comm         The MPI communicator to use.
*/
void f_write_par(void* ptr, size_t size, size_t nmemb, int source_pid, FILE_SPEC stream,
                 MPI_Comm comm = PETSC_COMM_WORLD);

/** read the length of a string followed by the string from a binary file
 *
 *  \param in the stream
 *
 *  \note memory is allocated
 *
 *  \return the string
 */
char *read_bin_string(FILE_SPEC in);

/** MPI - parallel version of read_bin_string
 *
 *  read the length of a string followed by the string from a binary file
 *
 *  \param in the stream
 *
 *  \note memory is allocated
 *
 *  \return the string
 */
char* read_bin_string_par(FILE_SPEC in);

char* f_gets_par(char* s, int size, FILE_SPEC stream, MPI_Comm comm = PETSC_COMM_WORLD);

/** write the length of a string followed by the string to a binary file
 *
 *  \param out the stream
 *  \param s   the string
 */
void write_bin_string(FILE_SPEC out, const char *s);

/**
* @brief Do a global reduction on a variable.
*
* The variable gets internally cast to a double, then reduced and then cast back.
*
* @tparam T    Variable type.
* @param in    Local input variable.
* @param OP    MPI operation to perform.
*
* @return      Globally reduced variable.
*/
template<typename T> inline
T get_global(T in, MPI_Op OP, MPI_Comm comm = PETSC_COMM_WORLD)
{
  double dat = in;
  MPI_Allreduce(MPI_IN_PLACE, &dat, 1, MPI_DOUBLE, OP, comm);
  return T(dat);
}

template<> inline
int get_global<int>(int in, MPI_Op OP, MPI_Comm comm)
{
  int dat = in;
  MPI_Allreduce(MPI_IN_PLACE, &dat, 1, MPI_INT, OP, comm);
  return dat;
}

template<> inline
long int get_global<long int>(long int in, MPI_Op OP, MPI_Comm comm)
{
  long int dat = in;
  MPI_Allreduce(MPI_IN_PLACE, &dat, 1, MPI_LONG, OP, comm);
  return dat;
}

/**
* @brief Broadcast a std::vector to all processes
*
* @tparam T    Datatype of vector entries
* @param vec   The vector we globalize
* @param sender
*/
template<typename T> inline
void get_global(std::vector<T> & vec, int sender = 0, MPI_Comm comm = PETSC_COMM_WORLD)
{
  size_t sz = vec.size();
  MPI_Bcast(&sz, sizeof(size_t), MPI_BYTE, sender, comm);
  vec.resize(sz);
  MPI_Bcast(vec.data(), sz*sizeof(T), MPI_BYTE, sender, comm);
}
/**
* @brief Broadcast a SF::vector to all processes
*
* @tparam T    Datatype of vector entries
* @param vec   The vector we globalize
* @param sender
*/
template<typename T> inline
void get_global(SF::vector<T> & vec, int sender = 0, MPI_Comm comm = PETSC_COMM_WORLD)
{
  size_t sz = vec.size();
  MPI_Bcast(&sz, sizeof(size_t), MPI_BYTE, sender, comm);
  vec.resize(sz);
  MPI_Bcast(vec.data(), sz*sizeof(T), MPI_BYTE, sender, comm);
}

/** get the rank of the process
 *
 * \return the rank of the process
 */
inline int get_rank()
{
  int rank;
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  return rank;
}

/** get the rank of the process
 *
 * \param comm communicator
 *
 * \return the rank of the process
 */
inline int get_rank_comm(MPI_Comm comm)
{
  int rank;
  MPI_Comm_rank(comm, &rank);

  return rank;
}

/** allows to determine if we are running sequentially or
    in parallel.

    \retval 0   sequential code
    \retval >=1 number of processes for parallel code
 */
inline int get_size()
{
  int size;

  MPI_Comm_size(PETSC_COMM_WORLD, &size);

  return size;
}

// flags for log messages
#define ECHO    1   // screen output
#define LOCAL   2   // output all cores
#define SYNCED  4   // synchronized
#define FLUSH   8   // flush screen output
#define NONL   16   // no new line

#define MAX_MESG_LEN 2048
#define MAX_LOG_LEVEL 5

/** output message into a log file
 *
 * \param out    output location, NULL implies ECHO
 * \param level  Severity level of message (0-5)
 * \param flags  the following may be or'ed:
 *               - ECHO: print to screen
 *               - LOCAL: every instance prints locally, must FLUSH to see output
 *               - SYNCED: output all in order on rank 0
 *               - FLUSH: flush output, this is collective
 *               - NONL: no new line at end
 *               By default, if \p out is NULL, the message is printed on the screen,
 *               and only rank 0 prints
 * \param fmt    format string for printf
 * \param ...    values for format string
 *
 * \note levels range from 0 to MAX_LOG_LEVEL with 0 being normal
 *       and MAX_LOG_LEVEL indicating catastrophic failure. Levels greater than
 *       0 are "errors" have the \p message prepended with the level.
 *
 * \note a new line will be appended to the message unless \p NONL is specified
 * \pre \p _hdf5_out must be set before this routine is called
 * \note to flush only, set \p fmt to NULL
 */
void log_msg(FILE_SPEC out, int level, unsigned char flag, const char *fmt, ...);

/// init a logger for solver iterations
void init_iterations_logger(FILE_SPEC* & logger, const char* filename);

inline void remove_preceding_char(char* buff, const int buffsize, const char c)
{
  int ridx = 0, widx=0;
  while(ridx < buffsize && buff[ridx] == c) ridx++;

  while(ridx < (buffsize - 1))
    buff[widx++] = buff[ridx++];

  buff[widx] = '\0';
}

inline void remove_char(char* buff, const int buffsize, const char c)
{
  int ridx = 0, widx=0;
  while(ridx < buffsize) {
    if(buff[ridx] != c)
      buff[widx++] = buff[ridx];
    ridx++;
  }

  buff[widx] = '\0';
}

inline bool has_char(char* buff, const int buffsize, const char c)
{
  int ridx = 0;
  while(ridx < buffsize) {
    if(buff[ridx] == '\0') return false;
    if(buff[ridx] == c)    return true;
    ridx++;
  }

  return false;
}

/// class to store shape definitions
class geom_shape {
  public:
  enum shape_t {sphere = 1, block = 2, cylinder = 3};
  shape_t type;
  Point p0;
  Point p1;
  double radius;
};

/// test if a point is inside a simple geometric shape
bool point_in_shape(const Point & p, const geom_shape & shape);

/** determine endianness of a machine
 *
 * \retval true  if machine is bigendian
 * \retval false if machine is littleendian
 */
bool is_big_endian();

/**
* @brief Check wheterh a file can be opened for reading.
*
* @param file  Path to the file to check.
*
* @return Whether the file could be opened.
*/
bool file_can_be_opened(const char* file);


/// check whether path is absolute
bool path_is_absolute(const char* path);

/* fmemopen for OSX / BSD compilations */
#ifdef __APPLE__
#define USE_FMEM_WRAPPER 1
#endif

#ifdef OS_FREEBSD
#define USE_FMEM_WRAPPER 1
#endif

#ifdef USE_FMEM_WRAPPER
struct fmem {
    size_t pos;
    size_t size;
    char *buffer;
};
typedef struct fmem fmem_t;

FILE *fmemopen_(void *, size_t, const char *);
#else
/* Else use the normal fmemopen */
#define fmemopen_ fmemopen
#endif

inline void get_time(double & tm)
{
  tm = MPI_Wtime();
}

inline double get_time()
{
  double tm = MPI_Wtime();
  return tm;
}

template<typename V> inline
V timing(V & t2, const V & t1)
{
  t2 = get_time();
  return t2 - t1;
}

}  // namespace opencarp

#endif
