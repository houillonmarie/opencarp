// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file basics.cc
* @brief Basic utility functions, mostly IO related.
* @author Aurel Neic, Edward Vigmond, Gernot Plank
* @version 
* @date 2019-10-25
*/

#include "basics.h"

namespace opencarp {

void sltlst_append( Salt_list* sl, void *p, int quantum )
{
  if( !sl->chunk ) sl->chunk = 1;

  if( !(sl->nitems%sl->chunk) )
    sl->data = realloc( sl->data, sl->chunk*(quantum*(sl->nitems/sl->chunk+1)+1) );
  memcpy( (char*)sl->data+sl->nitems*quantum, p, quantum );
  sl->nitems++;
  sl->size = quantum;
}

char* dupstr(const char* old_str) {
  if (!old_str) return NULL;

  size_t len = strlen(old_str);
  char *new_str = (char *)calloc(len+1, sizeof(char));
  strcpy(new_str, old_str);

  return new_str;
}

char* stringify(double r)
{
  char *rs = (char *)malloc(256);
  sprintf(rs, "%.1f", r);
  return rs;
}

std::string get_basename(const std::string & path)
{
   char sep = '/';
   size_t i = path.rfind(sep, path.length());
   if (i != std::string::npos) {
      return path.substr(i+1, path.length() - i);
   }

   return path;
}

void log_msg(FILE_SPEC out, int level, unsigned char flag, const char *fmt, ...)
{
  assert(level>=0 && level<=MAX_LOG_LEVEL);

  if(!out)
    flag |= ECHO;

  // create the message string
  char fmsg[MAX_MESG_LEN] = "";
  if(level)
    sprintf(fmsg, "L%d ", level );
  if((flag&LOCAL) || (flag&SYNCED))
    sprintf(fmsg+strlen(fmsg), "[P%d] ", get_rank() );
  if(level || (flag&LOCAL) || (flag&SYNCED))
    strcat(fmsg, ": ");

  if(fmt) {
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(fmsg+strlen(fmsg), MAX_MESG_LEN-strlen(fmsg)-1, fmt, ap);
    if(!(flag&NONL))
      strcat(fmsg, "\n");
  }

  // write to file
  if(out && fmt) {
    if(flag&SYNCED && out->fd)
      FPRINTF_SYNC(WORLD out->fd, fmsg);
    else if(!get_rank() || (flag&LOCAL)) {
      fprintf(out->fd, "%s", fmsg);
    }
  }

  // flush file
  if(out && flag & FLUSH)  {
    if(flag&SYNCED && out->fd)
      PRINTF_FLUSH(COMM_W, out->fd);
    else
      fflush(out->fd);
  }

  // write to screen
  if((flag&ECHO) && fmt) {
    if(flag&SYNCED )
      FPRINTF_SYNC(WORLD level ? stderr : stdout, fmsg);
    else if(!get_rank() || (flag&LOCAL))
      fprintf(level ? stderr : stdout, "%s", fmsg);
 }

  // flush screen
  if((flag&ECHO) && ((flag & FLUSH) || level == MAX_LOG_LEVEL)) {
    if(flag&SYNCED)
      PRINTF_FLUSH(COMM_W, level ? stderr : stdout);
    else
      fflush(level ? stderr : stdout);
  }
}

bool f_exist(const char *fname)
{
  return access(fname, F_OK) != -1;
}

FILE_SPEC f_open(const char *fname, const char *mode)
{
  FILE_SPEC f = new file_desc();
  f->name = dupstr(fname);

  bool openfail = false;
  int error     = 0;

  f->fd = fopen(fname,mode);
  if(f->fd == NULL) {
    openfail = true;
    error    = errno;
  }

  if(openfail) {
    char current_workdir[2048]; getcwd(current_workdir, 2048);

    fprintf(stderr, "Error, failed to open file: \"%s/%s\"!\n", current_workdir, fname);
    fprintf(stderr, "%s\n", strerror(error));

    delete f;
    return NULL;
  }

  return f;
}

void f_close(FILE_SPEC & f)
{
  if(f != NULL) {
    fclose(f->fd);
    delete f;
    f = NULL;
  }
}

void f_read_par(void *ptr, size_t size, size_t nmemb, FILE_SPEC stream, MPI_Comm comm)
{
  if(!get_rank())
    fread(ptr, size, nmemb, stream->fd);

  MPI_Bcast(ptr, size*nmemb, MPI_BYTE, 0, comm);
}

void f_write_par(void* ptr, size_t size, size_t nmemb, int source_pid, FILE_SPEC stream,
                 MPI_Comm comm)
{
  int rank = get_rank();

  if(rank == source_pid) {
    MPI_Send(&nmemb, sizeof(size_t), MPI_BYTE, 0, 100, comm);
    MPI_Send(ptr, nmemb*size, MPI_BYTE, 0, 100, comm);
  }

  if(rank == 0) {
    MPI_Status status;

    MPI_Recv(&nmemb, sizeof(size_t), MPI_BYTE, source_pid, 100, comm, &status);
    std::vector<char> wbuff(nmemb*size);
    MPI_Recv(wbuff.data(), size*nmemb, MPI_BYTE, source_pid, 100, comm, &status);
    fwrite(wbuff.data(), size, nmemb, stream->fd);
  }
}

char* f_gets_par(char* s, int size, FILE_SPEC stream, MPI_Comm comm)
{
  int rank = get_rank();

  // we want to be able to detect if a read happend by only evaluating s, not also the
  // return type of fgets. as such we make sure that strlen(s) == 0 if no read occured.
  s[0] = '\0';

  if(rank == 0) {
    fgets(s, size, stream->fd);
  }

  MPI_Bcast(s, size, MPI_CHAR, 0, comm);

  int str_len = strlen(s);

  if(str_len) return s;
  else        return NULL;
}

void write_bin_string(FILE_SPEC out, const char *s)
{
  if( !s ) return;

  int len = strlen(s);
  fwrite( &len, sizeof(int), 1, out->fd);
  fwrite( s, sizeof(char), len, out->fd);
}

char *read_bin_string(FILE_SPEC in)
{
  int len;
  fread( &len, sizeof(int), 1, in->fd );
  char *s = (char*)malloc( len+1 );
  fread( s, sizeof(char), len, in->fd );
  s[len] = '\0';

  return s;
}

char* read_bin_string_par(FILE_SPEC in)
{
  int len;
  f_read_par(&len, sizeof(int), 1, in);
  char *s = (char*) malloc(len+1);
  f_read_par(s, sizeof(char), len, in);
  s[len] = '\0';

  return s;
}

bool point_in_shape(const Point & p, const geom_shape & shape)
{
  switch(shape.type) {
    case geom_shape::block: {
      bool inside_x = p.x >= shape.p0.x && p.x <= shape.p1.x;
      bool inside_y = p.y >= shape.p0.y && p.y <= shape.p1.y;
      bool inside_z = p.z >= shape.p0.z && p.z <= shape.p1.z;
      return (inside_x && inside_y && inside_z);
    }

    case geom_shape::sphere:
      return (dist_2(p, shape.p0) <= (shape.radius * shape.radius));

    case geom_shape::cylinder:
    {
      Point height          = shape.p1 - shape.p0;
      Point center_to_point = p - shape.p0;

      double h = mag(height);
      height /= h;

      double height_projection = dot(height, center_to_point);

      if(height_projection >= 0 && height_projection <= h) {
        center_to_point -= (height * height_projection);
        return (mag2(center_to_point) <= (shape.radius * shape.radius));
      }
      else
        return false;
    }
/*
    default:
      log_msg(0,5,0, "%s error: Shape type not yet implemented.", __func__);
      EXIT(1);
*/
  }

  return false;
}

bool is_big_endian()
{
  int i = 1;
  char *p = (char *)&i;

  if (p[0] == 1)
    return false;
  else
    return true;
}

bool file_can_be_opened(const char* file)
{
  int rank = get_rank();
  int did_open = 0;

  if(rank == 0) {
    FILE* fd = fopen(file, "r");
    if(fd != NULL) {
      did_open = 1;
      fclose(fd);
    }
  }

  did_open = get_global(did_open, MPI_SUM);

  return did_open > 0;
}

bool path_is_absolute(const char* path)
{
  // we can be here more fancy / platform specific. -Aurel Jan. 27 2021
  if(strlen(path) && path[0] == '/') return true;

  return false;
}


#ifdef USE_FMEM_WRAPPER
/**
 * \brief Seek the mem file from offset and whence
 * \param handler pointer to the memfile
 * \param osffset number of bytes to move from whence
 * \param whence SEEK_SET, SEEK_CUR, SEEK_END
 * \retval pos the position by the last operation, -1 if sizes are out of bounds
 */
static fpos_t SeekFn(void *handler, fpos_t offset, int whence) {
    size_t pos = 0;
    fmem_t *mem = (fmem_t*)handler;

    switch (whence) {
        case SEEK_SET:
            if (offset > 0 && (size_t)offset <= mem->size)
                return mem->pos = offset;
        break;
        case SEEK_CUR:
            if (mem->pos + offset <= mem->size)
                return mem->pos = offset;
        break;
        case SEEK_END:
            /* must be negative */
            if (mem->size + offset <= mem->size)
                return pos = mem->size + offset;
        break;
    }

    return -1;
}

/**
 * \brief Read from the buffer looking for the available memory limits
 * \param handler pointer to the memfile
 * \param buf buffer to read from the handler
 * \param number of bytes to read
 * \retval count , the number of bytes read
 */
static int ReadFn(void *handler, char *buf, int size) {
    size_t count = 0;
    fmem_t *mem = (fmem_t*)handler;
    size_t available = mem->size - mem->pos;

    if (size < 0) return - 1;

    if ((size_t)size > available)
        size = available;

    while (count < (size_t)size)
        buf[count++] = mem->buffer[mem->pos++];

    return count;
}

/**
 * \brief Write into the buffer looking for the available memory limits
 * \param handler pointer to the memfile
 * \param buf buffer to write in the handler
 * \param number of bytes to write
 * \retval count , the number of bytes writen
 */
static int WriteFn(void *handler, const char *buf, int size) {
    size_t count = 0;
    fmem_t *mem = (fmem_t*)handler;
    size_t available = mem->size - mem->pos;

    if (size < 0) return - 1;

    if ((size_t)size > available)
        size = available;

    while (count < (size_t)size)
        mem->buffer[mem->pos++] = buf[count++];

    return count;
}

/**
 * \brief close the mem file handler
 * \param handler pointer to the memfile
 * \retval 0 on succesful
 */
static int CloseFn(void *handler) {
    free (handler);
    return 0;
}


/**
 * \brief portable version of SCFmemopen for OS X / BSD built on top of funopen()
 * \param buffer that holds the file content
 * \param size of the file buffer
 * \param mode mode of the file to open
 * \retval pointer to the file; NULL if something is wrong
 */
FILE *fmemopen_(void *buf, size_t size, const char *mode) {
    fmem_t *mem = (fmem_t *) malloc(sizeof(fmem_t));

    memset(mem, 0, sizeof(fmem_t));
    mem->size = size, mem->buffer = (char*)buf;

    return funopen(mem, ReadFn, WriteFn, SeekFn, CloseFn);
}

#endif  // USE_FMEM_WRAPPER

}  // namespace opencarp

