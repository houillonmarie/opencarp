// ----------------------------------------------------------------------------
// openCARP is an open cardiac electrophysiology simulator.
//
// Copyright (C) 2020 openCARP project
//
// This program is licensed under the openCARP Academic Public License (APL)
// v1.0: You can use and redistribute it and/or modify it in non-commercial
// academic environments under the terms of APL as published by the openCARP
// project v1.0, or (at your option) any later version. Commercial use requires
// a commercial license (info@opencarp.org).
//
// This program is distributed without any warranty; see the openCARP APL for
// more details.
//
// You should have received a copy of the openCARP APL along with this program
// and can find it online: http://www.opencarp.org/license
// ----------------------------------------------------------------------------

/**
* @file timer_utils.cc
* @brief Timers and timer manager.
* @author Gernot Plank, Edward Vigmond, Aurel Neic
* @version 
* @date 2019-10-25
*/


#include "timer_utils.h"
#include "basics.h"

#include <algorithm>

namespace opencarp {


void timer_manager::setup(double inp_dt, double inp_start, double inp_end)
{
  // pick closest index as start index
  time_step = inp_dt;
  d_time    = int(round(inp_start/time_step));
  time      = d_time * time_step;
  d_start   = d_time;
  start     = time;
  d_end     = int(round(inp_end/time_step));
  end       = d_end * time_step;
}

void timer_manager::initialize_eq_timer(double istart, double iend, int ntrig, double iintv,
                  double idur, int ID, const char *iname, const char* poolname)
{
  timer_eq* tm = new timer_eq();
  tm->initialize(this, istart, iend, ntrig, iintv, idur, iname);

  if(poolname) tm->assign_pool(poolname);

  if(int(timers.size()) <= ID)
    timers.resize(ID + 1, nullptr);

  timers[ID] = tm;
}


void timer_manager::initialize_neq_timer(const std::vector<double> & itrig, double idur, int ID,
                                         const char *iname, const char *poolname)
{
  timer_neq* tm = new timer_neq();
  tm->initialize(this, itrig, idur, iname);

  if(poolname) tm->assign_pool(poolname);

  if(int(timers.size()) <= ID)
    timers.resize(ID + 1, nullptr);

  timers[ID] = tm;
}


int timer_manager::add_eq_timer(double istart, double iend, int ntrig, double iintv,
                   double idur, const char *iname, const char *poolname)
{
  timer_eq* tm = new timer_eq();
  tm->initialize(this, istart, iend, ntrig, iintv, idur, iname);

  int ret_idx = timers.size();
  timers.push_back(tm);

  if(poolname) timers[ret_idx]->assign_pool(poolname);

  return ret_idx;
}

int timer_manager::add_neq_timer(const std::vector<double> & itrig, double idur, const char *iname,
                                 const char *poolname)
{
  timer_neq* tm = new timer_neq();
  tm->initialize(this, itrig, idur, iname);

  int ret_idx = timers.size();
  timers.push_back(tm);

  if(poolname) timers[ret_idx]->assign_pool(poolname);

  return ret_idx;
}

void timer_eq::initialize(const timer_manager* t, double istart, double iend, int ntrig,
                          double iintv, double idur, const char *iname)
{
  mng              = t;
  type             = EQUDIST;
  name             = dupstr(iname);
  trigger_count    = 0;
  trigger_dur      = idur;
  d_trigger_dur    = t->time_step > 0. ? (int)round(idur/t->time_step) : 0.;
  active           = 0;

  d_start  = t->time_step > 0. ? (int)round(istart/t->time_step) : 0.;
  d_intv   = iintv ? (int)round(iintv/t->time_step) : 1;

  if(ntrig)
    d_end = d_start + d_intv * ntrig - 1;
  else
    d_end = t->time_step > 0. ? (int)round(iend/t->time_step) : 0.;

  // move start past current time
  int num_skipped = 0;
  while ((d_start + d_trigger_dur) < t->d_time) {
    d_start += d_intv;
    num_skipped++;
  }

  // if new start is past end of trigger period of interest
  if (d_start>d_end)  {
    // disable trigger
    d_start  = t->d_end + 1;
    d_nxt    = t->d_end + 1;
    numIOs = 0;
    return;
  }

  //figure out where in the pulse cycle we are.
  if (d_start < t->d_time) {
    d_nxt  = d_start+d_intv;
    if ( d_start + d_trigger_dur > t->d_time) {
      active = d_start + d_trigger_dur - t->d_time;
    }
  } else {
    d_nxt  = d_start;
  }

  // re-determine number of trigger events
  numIOs   = (d_end-d_start)/d_intv+1;
  start    = d_start*t->time_step;
  intv     = d_intv*t->time_step;
  end      = d_end*t->time_step;
  nxt      = d_nxt*t->time_step;

  update();
}

void timer_eq::update()
{
  triggered = false;

  if (mng->d_time == d_nxt) {
    triggered = true;
    active    = d_trigger_dur;
    d_nxt += d_intv;
    if (d_nxt > d_end) d_nxt += mng->d_end;  // move beyond end of simulation
    nxt   = d_nxt * mng->time_step;
  }

  if (triggered) trigger_count++;

  // count down to end of trigger event with duration
  if (active)  {
    --active;
    triggered = true;
  }
}

void timer_eq::reset() {
  // end has been already adjusted during the first initialization. As such,
  // I dont think that we need to pass the number of trigger events again. -Aurel
  initialize(mng, start, end, 0, intv, trigger_dur, name);
}


void timer_neq::initialize(const timer_manager* t, const std::vector<double> & itrig,
                           const double idur, const char *iname)
{
  mng  = t;
  trig = itrig;
  type = NONEQUDIST;
  name = dupstr(iname);

  trigger_count   = 0;
  trigger_dur   = idur;
  d_trigger_dur = mng->time_step > 0. ? (int)round(idur/mng->time_step) : 0.;
  active   = 0;

  d_trig.resize(trig.size());
  int used_triggers = 0;
  int max_time_less_than_current_time = 0;
  int found_time_less_than_current_time = 0;

  for (size_t ii=0; ii < trig.size(); ii++)  {
    int this_d_tm = (int) round(trig[ii] / mng->time_step);
    if (this_d_tm >= mng->d_time) {
      d_trig[used_triggers++] = this_d_tm;
    }
    else {
      if (!found_time_less_than_current_time || this_d_tm > max_time_less_than_current_time) {
        found_time_less_than_current_time = 1;
        max_time_less_than_current_time = this_d_tm;
      }
    }
  }
  d_trig.resize(used_triggers);

  std::sort(d_trig.begin(), d_trig.end());
  std::unique(d_trig.begin(), d_trig.end());

  //fill the time list.
  trig.resize(d_trig.size());
  for (size_t ii=0; ii<d_trig.size(); ii++)  {
    trig[ii] = d_trig[ii] * mng->time_step;
  }
  numIOs = trig.size();

  //Find out if we are in the middle of a pulse
  if (found_time_less_than_current_time && max_time_less_than_current_time + d_trigger_dur > mng->d_time) {
    active = max_time_less_than_current_time + d_trigger_dur - mng->d_time;
  }

  update();
}

void timer_neq::update()
{
  triggered = 0;

  if (trigger_count < int(trig.size())) {
    if (mng->d_time == d_trig[trigger_count]) {
      triggered = 1;
      active    = d_trigger_dur;
    }
  }

  if (triggered) trigger_count++;

  // count down to end of trigger event with duration
  if (active) {
    --active;
    triggered = 1 ;
  }
}

void timer_neq::reset()
{
  std::vector<double> btrig(trig);
  initialize(mng, btrig, trigger_dur, name);
}

}  // namespace opencarp

