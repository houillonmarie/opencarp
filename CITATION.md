## Attribution

! If you use this software, please cite the paper describing it as below. Specific versions of the software can additionally be referenced using individual DOIs. When presenting work based on openCARP, we suggest to display our [logo](https://opencarp.org/download/logos).
! 
! *Plank, G., Loewe A., Neic. A et al. (2021). The openCARP simulation environment for cardiac electrophysiology. Computer Methods and Programs in Biomedicine 2021;208:106223. [doi:10.1016/j.cmpb.2021.106223](https://dx.doi.org/10.1016/j.cmpb.2021.106223)*

```bibtex
@article{openCARP,
author = {Gernot Plank and Axel Loewe and Aurel Neic and Christoph Augustin and Yung-Lin Huang and Matthias A.F. Gsell and Elias Karabelas and Mark Nothstein and Jorge Sanchez and Anton J Prassl and Gunnar Seemann and Edward J Vigmond},
title = {The {openCARP} Simulation Environment for Cardiac Electrophysiology},
pages = {106223},
volume = {208},
journal = {Computer Methods and Programs in Biomedicine},
doi = {10.1016/j.cmpb.2021.106223}, 
year = {2021}
}
```


