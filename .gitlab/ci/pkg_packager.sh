#!/bin/bash
# 

if [ "$(uname -s)" != "Darwin" ]; then
    echo "This script only works on macOS" && exit
fi

export PKG_PACKAGER="1"

#WORK_DIR=$HOME/code/carp
WORK_DIR=$(pwd)
BUILD_DIR=$WORK_DIR/_build_mac
INST_DIR=/usr/local/lib/opencarp

#######################################################################
#                       Xcode CommandLineTools                        #
#######################################################################
#sudo rm -rf /Library/Developer/CommandLineTools
#xcode-select --install
#sudo installer -pkg /Library/Developer/CommandLineTools/Packages/macOS_SDK_headers_for_macOS_10.14.pkg -target /

#######################################################################
#                                gcc@9                                #
#######################################################################
export GCC_DIR=$INST_DIR/lib/gcc

if [ -d $GCC_DIR ]; then
    echo "GCC is installed in $GCC_DIR"
else
    echo "Install GCC in $GCC_DIR"
    mkdir -p $GCC_DIR

    cd $WORK_DIR
    curl -L https://ftp.gnu.org/gnu/gcc/gcc-9.3.0/gcc-9.3.0.tar.xz | tar xz
    cd $(ls | grep gcc)
    ./contrib/download_prerequisites
    mkdir build && cd build
    ../configure --prefix=$GCC_DIR \
                 --disable-multilib \
                 --with-sysroot=/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk \
                 --enable-languages=c,c++,fortran
    make &>/dev/null
    make install
fi

#######################################################################
#                               openmpi                               #
#######################################################################
export MPI_DIR=$INST_DIR/lib/openmpi

if [ -d $MPI_DIR ]; then
    echo "openMPI is installed in $MPI_DIR"
else
    echo "Install openMPI in $MPI_DIR"
    mkdir -p $MPI_DIR

    cd $WORK_DIR
    curl -L https://download.open-mpi.org/release/open-mpi/v4.0/openmpi-4.0.3.tar.gz | tar xz
    cd $(ls | grep openmpi)
    mkdir build && cd build
    ../configure --prefix=$MPI_DIR --disable-dlopen --with-hwloc=internal --with-libevent=internal CC=$GCC_DIR/bin/gcc FC=$GCC_DIR/bin/gfortran
    make && make install
fi

#######################################################################
#                                petsc                                #
#######################################################################
PETSC_INSTALL_PREFIX=$INST_DIR/lib/petsc

if [ -d $PETSC_INSTALL_PREFIX ]; then
    echo "PETSc is installed in $PETSC_INSTALL_PREFIX"
else
    echo "Install PETSc in $PETSC_INSTALL_PREFIX"
    mkdir -p $PETSC_INSTALL_PREFIX

    cd $WORK_DIR
    curl -L http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-lite-3.11.4.tar.gz | tar xz
    cd $(ls | grep petsc)
    PETSC_DIR=$(pwd)

    ./configure \
        PETSC_ARCH=current-opt \
        --prefix=$PETSC_INSTALL_PREFIX \
        --with-mpi-dir=$MPI_DIR \
        --download-fblaslapack \
        --download-metis \
        --download-parmetis \
        --download-hypre \
        --with-debugging=0 \
        --with-x=0 \
        COPTFLAGS='-O2' \
        CXXOPTFLAGS='-O2' \
        FOPTFLAGS='-O2'
    make && make install
fi

export PETSC_DIR=$PETSC_INSTALL_PREFIX

#######################################################################
#                              openCARP                               #
#######################################################################
cd $WORK_DIR
cmake -S. -B$BUILD_DIR -DCMAKE_PREFIX_PATH=$MPI_DIR -DDLOPEN=ON -DBUILD_EXTERNAL=ON -DCMAKE_BUILD_TYPE=Release
cmake --build $BUILD_DIR --parallel 8
cd $BUILD_DIR
cpack -G productbuild
