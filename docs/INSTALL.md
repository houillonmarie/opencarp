## Installation of Precompiled openCARP

Before installing `openCARP`, install [Python3](https://www.python.org/downloads/) and [pip3](https://pip.pypa.io/en/stable/installing/).
Using your package manager would be preferable for most users.

Make sure they work by running the following commands.
```
python3 --version
python3 -m pip --version
```

For Ubuntu users: you might require to run the commands before installing your `python3-pip`.
```
add-apt-repository universe
apt-get update
```

### Installation Packages
We provide openCARP installation packages on our [release page](https://git.opencarp.org/openCARP/openCARP/-/releases).
Currently, they will install [openCARP](https://git.opencarp.org/openCARP/openCARP), [carputils](https://git.opencarp.org/openCARP/carputils), [meshtool](https://bitbucket.org/aneic/meshtool/src/master/), and the [examples](https://opencarp.org/documentation/examples).
[Meshalyzer](https://git.opencarp.org/openCARP/meshalyzer) is not yet included, please install it separately.

After installation, you can find the [examples](https://opencarp.org/documentation/examples) installed to `/usr/local/lib/opencarp/share/tutorials`.

#### Linux

For Ubuntu (18.04 or 20.04) and Debian (10 or later), please download the `.deb` package.
Installing the package can require to install the following packages as a prerequisite:
```
apt-get install git python3 python3-testresources python-is-python3
```

We recommand to install the package via `apt-get` like
```
cd <path-to-your-downloaded-deb-file>
apt-get update
apt-get install ./opencarp-v9.0.deb
```

For CentOS (8 or later) and Fedora (31 or later), please download the `.rpm` package.

We recommand to install the package via `dnf` like
```
cd <path-to-your-downloaded-rpm-file>
dnf install ./opencarp-v9.0.rpm
```

If the above packages doesn't work for you for some reasons (no support operating system, no superuser permissions, etc), please try the openCARP `AppImage`, which is a standalone file for any recent enough Linux-based operating system.

Make the `.AppImage` file executable and run it like
```
chmod +x openCARP-9.0-x86_64.AppImage
./openCARP-9.0-x86_64.AppImage
```

To run other openCARP binaries (`bench`, `igbapd`, `igbdft`, `igbextract`, `igbhead`, `igbops`, `mesher`, `cellml_converter.py`, `limpet_fe.py`, `make_dynamic_model.py`, `make_dynamic_mode.sh`), create a symbolic link with the corresponding name, take `bench` for example,
```
ln -s openCARP-9.0-x86_64.AppImage bench
./bench
```

The `AppImage` only supports openCARP binaries, so please check other components ([carputils](https://git.opencarp.org/openCARP/carputils), [meshtool](https://bitbucket.org/aneic/meshtool/src/master/), [examples](https://opencarp.org/documentation/examples), and [Meshalyzer](https://git.opencarp.org/openCARP/meshalyzer)) according to your needs.

#### macOS

Our current recommendation is to use Python 3.8 as some dependencies seem not to work well with Python 3.9.

For macOS (10.14 or later), please download the `.pkg` installer. Before running the installer, call `xcode-select --install` on the command line (Programs -> Utilities -> Terminal).

You may need to open the installer using the context menu (right click on the item and then select `Open`) to confirm the security warning.

### Confirm the Installation
To confirm if the package is successfully installed, open a terminal and try the following commands.

Run `openCARP` binary as follows. If it prints the building information (e.g. `GITtag: v9.0`, etc), your `openCARP` is successfully installed. Enjoy the simulator!
```
openCARP -buildinfo
```

Run `carputils` in Python as follows. If it prints the installation location, your `carputils` is successfully installed. Enjoy Python coding!
```
python3 -c "import carputils; print(carputils)"
```

### Docker
If you want to keep your host system clean but still be able to run and change openCARP, our Docker container might be most suitable for you. Docker containers should also run on Windows.
See [docker/INSTALL.md](https://git.opencarp.org/openCARP/openCARP/blob/master/docker/INSTALL.md) for detailed instructions on how to install and use ([docker/README.md](https://git.opencarp.org/openCARP/openCARP/blob/master/docker/INSTALL.md)) the openCARP docker container.

### Installation from Source
If you expect to change the openCARP source code, we suggest you follow the build instructions ([docs/BUILD.md](https://git.opencarp.org/openCARP/openCARP/-/blob/master/docs/BUILD.md)) to install from source.
