## Differences between openCARP and CARP/CARPentry
There are a few but significant differences between openCARP and CARP/CARPentry. Please find them in the following

### Things that wont be included in openCARP for now
* Mechanics
* Hemodynamics
* Purkinje system support
* Augmentation
* Eikonal and Reaction-Eikonal modes
* Generating conductivity blocks via FE mesh splitting
* Optical mapping

### Differences w.r.t. CARPentry

#### Ionic Models
openCARP is shipped only with auto-generated ionic models (via the limpet API).
If you used hand-written ionic models in CARP/CARPentry, they will not run in openCARP.
A way to deal with this is to copy the respective `.model` file, adjust it the way it to reflect the hand-written ionic model, and integrate it with [limpet_fe.py](https://opencarp.org/documentation/examples/01_ep_single_cell/04_limpet_fe).

#### Multi-Physics
openCARP is designed to support multi-physics from the ground up.
Therefore, the mesh regions are assigned to different physics.

For simple EP experiments on the whole mesh without bath, no additional
input w.r.t. CARPentry is required, as all regions are assigned by default to the
Intracellular and Extracellular domains.

With a bath present, the user needs to inform the simulator which regions form which
simulation domains.

For example, in the case of a mesh with the regions [0, 1], where region 1 is the myocard,
the region specific options are:

     -num_phys_regions 2 \
     -phys_region[0].name "Extracellular domain" \
     -phys_region[0].ptype 1 \
     -phys_region[0].num_IDs 2 \
     -phys_region[0].ID[0] 0 \
     -phys_region[0].ID[1] 1 \
     -phys_region[1].name "Intracellular domain" \
     -phys_region[1].ptype 0 \
     -phys_region[1].num_IDs 1 \
     -phys_region[1].ID[0] 1 \

By querying the openCARP help via

    ./openCARP +Help phys_type[0].ptype

one finds out that the "ptype" option is defined as:

    phys_region[0].ptype:
         type of physics

         type:    Int
         default:(Int)(0)
         menu: {
             (Int)(3)    Hemodynamics
             (Int)(2)    Mechanics
             (Int)(1)    Extracellular Electrics
             (Int)(0)    Intracellular Electrics
         }
         Depends on: {
             phys_region[PrMelem1]
         }

All `phys_type` related options can be queried via

    ./openCARP +Help | grep phys_type

You can also use this mechanism to run an experiment on sub-regions of a
mesh. For example you have a four-chamber heart in a torso, but you run
your simulation only on the atria and the atrial blood pools.

If you are using carputils, there is a helper function to generate the
`phys_region` options:

    cmd += tools.gen_physics_opts(ExtraTags=[0, 1], IntraTags=[1])
