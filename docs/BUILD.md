## Building from Source
After making sure the prerequisites are installed, you can build openCARP using CMake (recommended) or the shipped Makefile.

### Prerequisites
First install the following openCARP prerequisites using your package manager (e.g. apt or rpm on Linux, [homebrew](https://brew.sh/) or [macports](https://macports.org/) on macOS) or from source.
* [binutils](https://www.gnu.org/software/binutils/)
* C and C++ compilers (e.g. [gcc/g++](https://gcc.gnu.org) or [clang/clang++](https://clang.llvm.org). On macOS, we recommend clang provided by the homebrew package llvm to support openMP)
* [CMake](https://cmake.org) (Optional, minimum required version 3.13)
* [FFTW3](https://fftw.org) (Optional, for building `igbdft`)
* [gengetopt](https://www.gnu.org/software/gengetopt/gengetopt.html)
* [gfortran](https://gcc.gnu.org/fortran/)
* [git](https://git-scm.com)
* [make](https://git-scm.com)
* [PETSc](https://petsc.org/release/)
* [PkgConfig](https://www.freedesktop.org/wiki/Software/pkg-config/)
* [Python3](https://www.python.org) and the [pip package installer](https://pip.pypa.io/en/stable/) (current recommendation: Python 3.8 as some dependencies seem not to work well with Python 3.9)
* [zlib](https://zlib.net) development package

[Building `PETSc` from source](https://petsc.org/release/install/) would be a better practice, so you could configure it depending on your needs.
If you are not experienced with its various [configurations](https://petsc.org/release/install/install_tutorial/#configuration), please refer to the following suggestions.
Set `--prefix=` to the location you would install `PETSc`.
After installation, set the [environment variable `PETSC_DIR` and `PETSC_ARCH`](https://petsc.org/release/install/multibuild/#environmental-variables-petsc-dir-and-petsc-arch) accordingly.

```
./configure \
    --prefix=/opt/petsc \
    --download-mpich \
    --download-fblaslapack \
    --download-metis \
    --download-parmetis \
    --download-hypre \
    --with-debugging=0 \
    COPTFLAGS='-O2' \
    CXXOPTFLAGS='-O2' \
    FOPTFLAGS='-O2'
make all
make install
```

In this case, to make `mpirun` available on your machine, add `$PETSC_DIR/bin` to your `PATH`. That is, add the following line to your .bashrc.
```
export PATH=$PATH:$PETSC_DIR/bin
```

If you have all the listed dependencies, a quick way to get openCARP up and running is the following:

Clone the repository and enter the codebase folder `openCARP`
```
git clone https://git.opencarp.org/openCARP/openCARP.git
cd openCARP
```

We provide CMake files and Makefiles for building the codebase, choose one fits your workflow.

### Building using CMake

Run CMake to create the `_build` build folder, optional add `-DDLOPEN=ON` to enable ionic shared library loading, and optional add `-DCMAKE_BUILD_TYPE=Release` to optimize compilation.
You can also optionally add `-DUSE_OPENMP=ON` in order to activate OpenMP parallelization (requires that OpenMP is installed on your system).
```
cmake -S. -B_build -DDLOPEN=ON -DCMAKE_BUILD_TYPE=Release
```

Alternatively, if you would like to also build the additional tools ([meshtool](https://bitbucket.org/aneic/meshtool/src/master/), [carputils](https://git.opencarp.org/openCARP/carputils), and [examples](https://opencarp.org/documentation/examples)) locally, run:
```
cmake -S. -B_build -DDLOPEN=ON -DBUILD_EXTERNAL=ON -DCMAKE_BUILD_TYPE=Release
```

Compile the codebase, and all built executables are located in the `_build/bin` folder.
```
cmake --build _build
```

If you built the additional tools, [meshtool](https://bitbucket.org/aneic/meshtool/src/master/) executable is also located in the `_build/bin` folder, [carputils](https://git.opencarp.org/openCARP/carputils) and [experiments](https://git.opencarp.org/openCARP/experiments) (contain [examples](https://opencarp.org/documentation/examples)) are cloned to the `external` folder.
Check [carputils main page](https://git.opencarp.org/openCARP/carputils) for its installation guide.

### Building using the Makefile

The codebase can be compiled using

    make setup

This target includes updating the codebase (`make update`) and code compilation (`make all`).

Make sure to edit the `my_switches.def` file to match you requirements (e.g., to enable the use of shared libraries for ionic models).

Links to all compiled executables are located in the `bin` folder.

Please also refer to the [carputils main page](https://git.opencarp.org/openCARP/carputils), the [meshtool main page](https://bitbucket.org/aneic/meshtool/src/master/). 
Download the openCARP [examples](https://opencarp.org/documentation/examples) to the location of your choice using `git clone https://git.opencarp.org/openCARP/experiments.git`.
