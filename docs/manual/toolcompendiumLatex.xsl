<?xml version="1.0" encoding="UTF-8"?>
<!--
  toolcompendium: xsl transformation to generate documention for command line tools.
  Copyright (C) 2019  IAM-CMS

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="text"/>
  <xsl:variable name="shortname_column" select="false()" />

  <xsl:template match="/">
    <xsl:call-template name="import_latex">
      <xsl:with-param name="path" select="$preamble" />
    </xsl:call-template>

    <xsl:text>\date{</xsl:text>
    <xsl:value-of select="programs/@date"/>
    <xsl:text>}</xsl:text>
    <xsl:text>\begin{document}</xsl:text>

    <xsl:call-template name="import_latex">
      <xsl:with-param name="path" select="$titlepage" />
    </xsl:call-template>
    <xsl:call-template name="import_latex">
      <xsl:with-param name="path" select="$licensepage" />
    </xsl:call-template>
    <xsl:call-template name="import_latex">
      <xsl:with-param name="path" select="$introduction" />
    </xsl:call-template>

    <xsl:apply-templates select="programs/*[not(self::param) and not(self::struct-ref)]"/>

    <xsl:call-template name="import_latex">
      <xsl:with-param name="path" select="$numerics" />
    </xsl:call-template>
    <xsl:call-template name="import_latex">
      <xsl:with-param name="path" select="$footer" />
    </xsl:call-template>
    <xsl:text>

    </xsl:text>
  </xsl:template>

<xsl:template name="import_latex">
  <xsl:param name="path" />
  <xsl:text>\input{</xsl:text>
  <xsl:value-of select="$path" />
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="section[not(@name = 'Main' and description = ' (null) ' and count(*) = 1)]">
  <xsl:choose>
    <xsl:when test="count(ancestor::section) = 0">
      <xsl:text>\section{</xsl:text>
    </xsl:when>
    <xsl:when test="count(ancestor::section) = 1">
      <xsl:text>\subsection{</xsl:text>
    </xsl:when>
    <xsl:when test="count(ancestor::section) = 2">
      <xsl:text>\subsubsection{</xsl:text>
    </xsl:when>
    <xsl:when test="count(ancestor::section) &gt; 2">
      <xsl:text>\textbf{</xsl:text>
    </xsl:when>
  </xsl:choose>
  <xsl:call-template name="nonlatex">
    <xsl:with-param name="StringToTransform" select="@name"/>
  </xsl:call-template>
  <xsl:text>}&#10;</xsl:text>
  <xsl:apply-templates select="*[not(self::param)]"/>
  <!--<xsl:text>\clearpage&#10;</xsl:text>-->
</xsl:template>

  <xsl:template match="program">
    <xsl:param name="program_description" select="description" />
    <xsl:call-template name="programInternal">
      <xsl:with-param name="description" select="$program_description"/>
    </xsl:call-template>
  </xsl:template>

<xsl:template name="programInternal">
    <xsl:param name="description" />
    <xsl:param name="father" />
    <xsl:param name="param-ref" />

    <xsl:choose>
      <xsl:when test="not($param-ref) and $father != ''"><!-- special case for the struct-ref resolution -->
        <xsl:text>\subsection{</xsl:text>
      </xsl:when>
      <xsl:when test="$param-ref"><!-- special case for the param-ref resolution -->
        <xsl:text>\subsubsection{</xsl:text>
      </xsl:when>
      <xsl:when test="count(ancestor::section) = 0">
        <xsl:text>\section{</xsl:text>
      </xsl:when>
      <xsl:when test="count(ancestor::section) = 1">
        <xsl:text>\subsection{</xsl:text>
      </xsl:when>
      <xsl:when test="count(ancestor::section) = 2">
        <xsl:text>\subsubsection{</xsl:text>
      </xsl:when>
      <xsl:when test="count(ancestor::section) &gt; 2">
        <xsl:text>\textbf{</xsl:text>
      </xsl:when>
    </xsl:choose>
       <xsl:call-template name="nonlatex">
         <xsl:with-param name="StringToTransform" select="@name"/>
       </xsl:call-template>
    <xsl:text>}&#10;</xsl:text>
    <xsl:text>\index{</xsl:text>
       <xsl:call-template name="nonlatex">
         <xsl:with-param name="StringToTransform" select="@name"/>
       </xsl:call-template>
    <xsl:text>}&#10;</xsl:text>
    <xsl:if test="$description">
      <xsl:text>\begin{description}\itemsep2pt&#10;</xsl:text>
      <xsl:text>\item[Description]&#10;</xsl:text>
      <xsl:call-template name="nonlatex">
        <xsl:with-param name="StringToTransform" select="$description"/>
      </xsl:call-template>
      <xsl:if test="boolean(@istested)">
        <xsl:text> (\textit{test status </xsl:text><xsl:apply-templates select="@istested"/><xsl:text>})\\&#10;&#10;</xsl:text>
      </xsl:if>
      <xsl:call-template name="search-and-replace">
        <xsl:with-param name="str">
          <xsl:call-template name="nonlatex">
            <xsl:with-param name="StringToTransform" select="@example"/>
          </xsl:call-template>
        </xsl:with-param>
        <xsl:with-param name="search" select="'e.g.:'"/>
        <xsl:with-param name="replace" select="'\vspace*{-4mm}\item[Example]&#10;'"/>
      </xsl:call-template>
      <xsl:text>&#10;</xsl:text>
      <xsl:text>\end{description}&#10;</xsl:text>
    </xsl:if>
    <xsl:text>&#10;\begin{longtable}[l]{</xsl:text>
    <xsl:choose>
      <xsl:when test="$shortname_column">
        <xsl:text>p{0.181\textwidth} p{0.27\textwidth} p{0.481\textwidth}</xsl:text><!-- the sum is 0.932 -->
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>p{0.466\textwidth} p{0.566\textwidth}</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="$shortname_column">
    </xsl:if>
    <xsl:text>}&#10;\multicolumn{</xsl:text>
    <xsl:choose>
      <xsl:when test="$shortname_column">
        <xsl:text>3</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>2</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>}{l}{\hspace{-2mm}\textsf{\textbf{Parameters}}\vspace{2mm}}\\&#10;\endfirsthead&#10;\multicolumn{</xsl:text>
    <xsl:choose>
      <xsl:when test="$shortname_column">
        <xsl:text>3</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>2</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>}{l}{\hspace{-2mm}\textsf{\textbf{Parameters}} (continued)\vspace{1mm}}\\&#10;\endhead&#10;</xsl:text>
      <xsl:apply-templates select="param">
        <xsl:with-param name="father" select="$father"/>
      </xsl:apply-templates>
    <xsl:text>\end{longtable}</xsl:text>
    <xsl:apply-templates select="param-ref">
      <xsl:with-param name="father" select="$father"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="param-ref">
    <xsl:param name="father" />

    <!-- resolve the reference -->
    <xsl:variable name="struct_ref_name" select="@name" />
    <xsl:variable name="struct_ref_father_name" select="$father" />
    <xsl:variable name="struct_description" select="description" />

    <xsl:apply-templates select="/programs/struct-ref[@name=$struct_ref_name]">
      <xsl:with-param name="father_name" select="$struct_ref_father_name" />
      <xsl:with-param name="description" select="$struct_description" />
      <xsl:with-param name="param-ref" select="'true'" />
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="struct-ref">
    <xsl:param name="father_name" />
    <xsl:param name="description" />
    <xsl:param name="param-ref" />

    <xsl:text>&#xa;</xsl:text>
    <xsl:if test="parent::section">
      <!-- resolve the reference -->
      <xsl:variable name="struct_ref_name" select="@name" />
      <xsl:variable name="struct_ref_father_name" select="@father-name" />
      <xsl:variable name="struct_description" select="description" />

      <xsl:text>&#xa;</xsl:text>


      <xsl:apply-templates select="/programs/struct-ref[@name=$struct_ref_name]">
        <!-- call this template again to print the program -->
        <xsl:with-param name="father_name" select="$struct_ref_father_name" />
        <xsl:with-param name="description" select="$struct_description" />
      </xsl:apply-templates>
    </xsl:if>
    <xsl:if test="parent::programs">
      <!-- print referenced program -->
      <xsl:call-template name="programInternal">
        <!-- copying the description of the <struct-ref> over to the <program> -->
        <xsl:with-param name="description" select="$description" />
        <xsl:with-param name="father" select="$father_name" />
        <xsl:with-param name="param-ref" select="$param-ref" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="nonlatex">
    <xsl:param name="StringToTransform"/>
    <xsl:call-template name="search-and-replace">
      <xsl:with-param name="str">
      <xsl:call-template name="search-and-replace">
      <xsl:with-param name="str">
      <xsl:call-template name="search-and-replace">
      <xsl:with-param name="str">
      <xsl:call-template name="search-and-replace">
      <xsl:with-param name="str">
      <xsl:call-template name="search-and-replace">
      <xsl:with-param name="str">
        <xsl:call-template name="search-and-replace">
          <xsl:with-param name="str">
          <xsl:call-template name="search-and-replace">
            <xsl:with-param name="str">
            <xsl:call-template name="search-and-replace">
              <xsl:with-param name="str">
                <xsl:call-template name="search-and-replace">
                  <xsl:with-param name="str">
                    <xsl:call-template name="search-and-replace">
                      <xsl:with-param name="str">
                        <xsl:call-template name="search-and-replace">
                          <xsl:with-param name="str">
                            <xsl:call-template name="search-and-replace">
                              <xsl:with-param name="str">
                                <xsl:call-template name="search-and-replace">
                                  <xsl:with-param name="str">
                                    <xsl:call-template name="search-and-replace">
                                      <xsl:with-param name="str">
                                        <xsl:call-template name="search-and-replace">
                                          <xsl:with-param name="str">
                                            <xsl:call-template name="search-and-replace">
                                              <xsl:with-param name="str">
                                                <xsl:call-template name="search-and-replace">
                                                  <xsl:with-param name="str" select="$StringToTransform"/>
                                                  <xsl:with-param name="search" select="'\'"/>
                                                  <xsl:with-param name="replace" select="'\textbackslash '"/>
                                                </xsl:call-template>
                                              </xsl:with-param>
                                              <xsl:with-param name="search" select="'&#009;'"/>
                                              <xsl:with-param name="replace" select="'\quad '"/>
                                            </xsl:call-template>
                                            </xsl:with-param>
                                            <xsl:with-param name="search" select="'$'"/>
                                            <xsl:with-param name="replace" select="'\$'"/>
                                        </xsl:call-template>
                                        </xsl:with-param>
                                      <xsl:with-param name="search" select="'&lt;'"/>
                                      <xsl:with-param name="replace" select="'\textless '"/>
                                    </xsl:call-template>
                                    </xsl:with-param>
                                  <xsl:with-param name="search" select="'|'"/>
                                  <xsl:with-param name="replace" select="'\textbar '"/>
                                </xsl:call-template>
                              </xsl:with-param>
                              <xsl:with-param name="search" select="'_'"/>
                              <xsl:with-param name="replace" select="'\_'"/>
                            </xsl:call-template>
                            </xsl:with-param>
                          <xsl:with-param name="search" select="'^'"/>
                          <xsl:with-param name="replace" select="'\textasciicircum '"/>
                        </xsl:call-template>
                        </xsl:with-param>
                      <xsl:with-param name="search" select="'°'"/>
                      <xsl:with-param name="replace" select="'$^{\circ}$'"/>
                    </xsl:call-template>
                  </xsl:with-param>
                  <xsl:with-param name="search" select="'&quot;'"/>
                  <xsl:with-param name="replace" select="'``'"/>
                </xsl:call-template>
              </xsl:with-param>
              <xsl:with-param name="search" select="'&amp;'"/>
              <xsl:with-param name="replace" select="'\&amp;'"/>
            </xsl:call-template>
            </xsl:with-param>
            <xsl:with-param name="search" select="'&#xA;'"/>
            <xsl:with-param name="replace" select="'\newline&#10;'"/>
          </xsl:call-template>
          </xsl:with-param>
          <xsl:with-param name="search" select="'&gt; '"/>
          <xsl:with-param name="replace" select="'\textgreater\  '"/>
        </xsl:call-template>
        </xsl:with-param>
          <xsl:with-param name="search" select="'&gt;'"/>
          <xsl:with-param name="replace" select="'\textgreater '"/>
        </xsl:call-template>
        </xsl:with-param>
        <xsl:with-param name="search" select="'*'"/>
        <xsl:with-param name="replace" select="'$\cdot$'"/>
      </xsl:call-template>
      </xsl:with-param>
      <xsl:with-param name="search" select="'~'"/>
      <xsl:with-param name="replace" select="'$\sim$'"/>
    </xsl:call-template>
      </xsl:with-param>
      <xsl:with-param name="search" select="'#'"/>
      <xsl:with-param name="replace" select="'\#'"/>
    </xsl:call-template>
      </xsl:with-param>
      <xsl:with-param name="search" select="'%'"/>
      <xsl:with-param name="replace" select="'\%'"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="search-and-replace">
    <xsl:param name="str"/>
    <xsl:param name="search"/>
    <xsl:param name="replace"/>
    <xsl:choose>
      <xsl:when test="contains($str, $search)">
        <xsl:value-of select="substring-before($str, $search)"/>
        <xsl:value-of select="$replace"/>
        <xsl:call-template name="search-and-replace">
          <xsl:with-param name="str" select="substring-after($str, $search)"/>
          <xsl:with-param name="search" select="$search"/>
          <xsl:with-param name="replace" select="$replace"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$str"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="try-search-and-replace">
    <xsl:param name="str"/>
    <xsl:param name="search"/>
    <xsl:param name="replace"/>

    <xsl:choose>
      <xsl:when test="$replace != ''">
        <xsl:call-template name="search-and-replace">
          <xsl:with-param name="str" select="$str"/>
          <xsl:with-param name="search" select="$search"/>
          <xsl:with-param name="replace" select="$replace"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$str"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="param">
    <xsl:param name="father" />
    <xsl:variable name="name" select="@*[name() = 'name']"/>
    <xsl:variable name="type" select="@*[name() = 'type']"/>
    <xsl:variable name="short" select="@*[name() = 'char']"/>
    <xsl:if test="$shortname_column">
      <xsl:apply-templates select="@*[name() = 'char']"/>
      <xsl:apply-templates select="@*[name() = 'type']"/>
      <xsl:text>&amp;</xsl:text>
    </xsl:if>
    <xsl:variable name="namex">
     <xsl:choose>
<!--     no regular expressions supported by xslt1.0! -> arg{0-9}1-->
     <xsl:when test="$name = 'arg0'" ><xsl:value-of select="true()"/></xsl:when>
     <xsl:when test="$name = 'arg1'" ><xsl:value-of select="true()"/></xsl:when>
     <xsl:when test="$name = 'arg2'" ><xsl:value-of select="true()"/></xsl:when>
     <xsl:when test="$name = 'arg3'" ><xsl:value-of select="true()"/></xsl:when>
     <xsl:when test="$name = 'arg4'" ><xsl:value-of select="true()"/></xsl:when>
     <xsl:when test="$name = 'arg5'" ><xsl:value-of select="true()"/></xsl:when>
     <xsl:when test="$name = 'arg6'" ><xsl:value-of select="true()"/></xsl:when>
     <xsl:when test="$name = 'arg7'" ><xsl:value-of select="true()"/></xsl:when>
     <xsl:when test="$name = 'arg8'" ><xsl:value-of select="true()"/></xsl:when>
     <xsl:when test="$name = 'arg9'" ><xsl:value-of select="true()"/></xsl:when>
     <xsl:otherwise>
       <xsl:value-of select="false()"/>
     </xsl:otherwise>
    </xsl:choose>
    </xsl:variable>
    <xsl:if test="$namex = 'false'">
      <xsl:apply-templates select="@*[name() = 'name']"/>
    </xsl:if>
    <xsl:if test="($type != 'flag' and $namex = 'false') or (not($short) and $namex = 'true')">
      <xsl:apply-templates select="@*[name() = 'type']"/>
     <xsl:text>&amp;</xsl:text>

     <xsl:variable name="required" select="@*[name() = 'required']"/>
     <xsl:if test="$required = 'true'">
        <xsl:apply-templates select="@*[name() = 'required']"/>
     </xsl:if>
     <xsl:if test="not($required = 'true')">
        <xsl:if test="not(@*[name() = 'type'] = 'flag') and not(@*[name() = 'type'] = 'plot')">
          <xsl:apply-templates select="@*[name() = 'default']">
            <xsl:with-param name="father" select="$father" />
          </xsl:apply-templates>
        </xsl:if>
     </xsl:if>
     <xsl:text>\newline&#10;</xsl:text>
     <xsl:call-template name="nonlatex">
       <xsl:with-param name="StringToTransform" select="description"/>
     </xsl:call-template>
     <xsl:text>\\</xsl:text>
<!--    <xsl:apply-templates select="@*[name() = 'interval']"/>
    <xsl:apply-templates select="@*[name() = 'relations']"/>-->
    </xsl:if>
    <xsl:call-template name="validation">
      <xsl:with-param name="father" select="$father" />
    </xsl:call-template>
    <xsl:call-template name="relations">
      <xsl:with-param name="father" select="$father" />
    </xsl:call-template>
    <xsl:text>\vspace*{0.3cm}\\&#10;</xsl:text>
  </xsl:template>

  <xsl:template name="param-references" />

  <xsl:template name="validation">
    <xsl:param name="father" />
    <xsl:text>[4mm]</xsl:text>
    <xsl:if test="$shortname_column">
      <xsl:text>&amp;</xsl:text>
    </xsl:if>
    <xsl:text>&amp;</xsl:text>
    <xsl:choose>
      <xsl:when test="validation/@type = 'options'">
        <xsl:text>\textbf{Possible values are:}\\&amp;\\</xsl:text>
        <xsl:for-each select="validation/option">
          <!--
          <xsl:if test="$shortname_column">
            <xsl:text>&amp;</xsl:text>
          </xsl:if>
          -->
          <xsl:text>&amp;\hspace{10pt}</xsl:text>
          <xsl:call-template name="nonlatex">
            <xsl:with-param name="StringToTransform">
              <xsl:call-template name="try-search-and-replace">
                <xsl:with-param name="str" select="@value"/>
                <xsl:with-param name="search" select="'[[Father]]'"/>
                <xsl:with-param name="replace" select="$father" />
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
          <xsl:text>: </xsl:text>
          <xsl:call-template name="nonlatex">
            <xsl:with-param name="StringToTransform" select="@name"/>
          </xsl:call-template>
          <xsl:text>\\&#10;</xsl:text>
        </xsl:for-each>
      </xsl:when>
      <xsl:when test="validation/@type = 'range'">
        <xsl:text>Value must be</xsl:text>
        <xsl:if test="boolean(validation/min) and boolean(validation/max)">
          <xsl:text> between </xsl:text>
          <xsl:call-template name="nonlatex">
            <xsl:with-param name="StringToTransform">
              <xsl:call-template name="try-search-and-replace">
                <xsl:with-param name="str" select="validation/min"/>
                <xsl:with-param name="search" select="'[[Father]]'"/>
                <xsl:with-param name="replace" select="$father" />
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
          <xsl:text> and </xsl:text>
          <xsl:call-template name="nonlatex">
            <xsl:with-param name="StringToTransform">
              <xsl:call-template name="try-search-and-replace">
                <xsl:with-param name="str" select="validation/max"/>
                <xsl:with-param name="search" select="'[[Father]]'"/>
                <xsl:with-param name="replace" select="$father" />
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:if>
        <xsl:if test="boolean(validation/min) and not(boolean(validation/max))">
          <xsl:text> greater than </xsl:text>
          <xsl:call-template name="nonlatex">
            <xsl:with-param name="StringToTransform">
              <xsl:call-template name="try-search-and-replace">
                <xsl:with-param name="str" select="validation/min"/>
                <xsl:with-param name="search" select="'[[Father]]'"/>
                <xsl:with-param name="replace" select="$father" />
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:if>
        <xsl:if test="not(boolean(validation/min)) and boolean(validation/max)">
          <xsl:text> smaller than </xsl:text>
          <xsl:call-template name="nonlatex">
            <xsl:with-param name="StringToTransform">
              <xsl:call-template name="try-search-and-replace">
                <xsl:with-param name="str" select="validation/max"/>
                <xsl:with-param name="search" select="'[[Father]]'"/>
                <xsl:with-param name="replace" select="$father" />
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:if>
      </xsl:when>
    </xsl:choose>
    <xsl:text>\\&#10;</xsl:text>
  </xsl:template>

  <xsl:template name="relations">
    <xsl:param name="father" />
    <xsl:text>[4mm]</xsl:text>
    <xsl:if test="boolean(relation)">
      <xsl:if test="$shortname_column">
        <xsl:text>&amp;</xsl:text>
      </xsl:if>
      <xsl:text>&amp;</xsl:text>
      <!-- group all relations by type -->
      <xsl:variable name="required_parameters" select="relation[(@type = 'require' or not(boolean(@type))) and boolean(@to-name)]" />
      <xsl:variable name="required_groups" select="relation[(@type = 'require' or not(boolean(@type))) and boolean(@to-group)]" />
      <xsl:variable name="excluded_parameters" select="relation[@type = 'exclude' and boolean(@to-name)]" />
      <xsl:variable name="excluded_groups" select="relation[(@type = 'require' or not(boolean(@type))) and boolean(@to-group)]" />

      <!-- print relation groups, each with a short title -->
      <xsl:call-template name="list_relations">
        <xsl:with-param name="relations" select="$required_parameters" />
        <xsl:with-param name="title">Required parameter</xsl:with-param>
        <xsl:with-param name="father" select="$father" />
      </xsl:call-template>
      <xsl:call-template name="list_relations">
        <xsl:with-param name="relations" select="$required_groups" />
        <xsl:with-param name="title">Required group</xsl:with-param>
        <xsl:with-param name="father" select="$father" />
      </xsl:call-template>
      <xsl:call-template name="list_relations">
        <xsl:with-param name="relations" select="$excluded_parameters" />
        <xsl:with-param name="title">Excluded parameter</xsl:with-param>
        <xsl:with-param name="father" select="$father" />
      </xsl:call-template>
      <xsl:call-template name="list_relations">
        <xsl:with-param name="relations" select="$excluded_groups" />
        <xsl:with-param name="title">Excluded group</xsl:with-param>
        <xsl:with-param name="father" select="$father" />
      </xsl:call-template>

    </xsl:if>
  </xsl:template>

  <xsl:template name="list_relations">
    <xsl:param name="relations" />
    <xsl:param name="title" /> <!-- singular, plural 's' will be added if more than one relation! -->
    <xsl:param name="father" />

      <xsl:if test="count($relations) &gt; 0">
        <xsl:text>\textbf{</xsl:text>
        <xsl:value-of select="$title" />
        <xsl:choose>
          <xsl:when test="count($relations) &gt; 1">
            <xsl:text>s:&#10;}\\&amp;\\</xsl:text><!-- plural s -->
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>:&#10;}\\&amp;\\</xsl:text><!-- no plural s -->
          </xsl:otherwise>
        </xsl:choose>
        <xsl:for-each select="$relations">
          <xsl:variable name="to_name">
              <xsl:call-template name="try-search-and-replace">
                <xsl:with-param name="str" select="@to-name"/>
                <xsl:with-param name="search" select="'[[Father]]'"/>
                <xsl:with-param name="replace" select="$father"/>
              </xsl:call-template>
          </xsl:variable>


          <xsl:text>&amp;\hspace{10pt}</xsl:text>
          <xsl:call-template name="nonlatex">
            <xsl:with-param name="StringToTransform" select="$to_name" />
          </xsl:call-template>
          <xsl:text>\\&#10;</xsl:text>
        </xsl:for-each>
      </xsl:if>

    </xsl:template>

<!-- matches all attributes -->
  <xsl:template match="@*">
   <xsl:param name="father" />
   <xsl:choose>
    <xsl:when test="name() = 'char'">
     <xsl:text>\textit{-</xsl:text><xsl:value-of select="."/><xsl:text>} </xsl:text>
    </xsl:when>
    <xsl:when test="name() = 'name'">
     <xsl:text>\textit{</xsl:text><xsl:call-template name="nonlatex"><xsl:with-param name="StringToTransform" select="."/></xsl:call-template><xsl:text>} </xsl:text>
    </xsl:when>
    <xsl:when test="name() = 'type'">
     <xsl:text>\mbox{\textless </xsl:text><xsl:call-template name="nonlatex"><xsl:with-param name="StringToTransform" select="."/></xsl:call-template><xsl:text>\textgreater }</xsl:text>
    </xsl:when>
    <xsl:when test="name() = 'required'">
     <xsl:text>\textbf{(</xsl:text><xsl:value-of select="name()"/><xsl:text>)} </xsl:text>
    </xsl:when>
    <xsl:when test="name() = 'default'">
      <xsl:variable name="default_value">
        <xsl:call-template name="try-search-and-replace">
          <xsl:with-param name="str" select="."/>
          <xsl:with-param name="search" select="'[[Father]]'"/>
          <xsl:with-param name="replace" select="$father"/>
        </xsl:call-template>
      </xsl:variable>
     <xsl:text>(</xsl:text><xsl:value-of select="name()"/>=<xsl:call-template name="nonlatex"><xsl:with-param name="StringToTransform" select="$default_value"/></xsl:call-template><xsl:text>)</xsl:text>
    </xsl:when>
    <xsl:when test="name() = 'istested'">
      <xsl:if test="number(.) &gt; -1">
        <xsl:value-of select="."/><xsl:text>\%</xsl:text>
      </xsl:if>
      <xsl:if test="number(.) &lt; 0">
        <xsl:text>This tool does not work or has serious bugs!</xsl:text>
      </xsl:if>
    </xsl:when>
    <xsl:otherwise>
       <xsl:call-template name="nonlatex">
         <xsl:with-param name="StringToTransform" select="name()"/>
       </xsl:call-template>
       <xsl:text>&#10;&#10;&#10;</xsl:text>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
